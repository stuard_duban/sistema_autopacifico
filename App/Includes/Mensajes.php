<?php
class Mensajes
{
	var $_contenido_mensajes;

	function Mensajes()
	{
		$this->_contenido_mensajes = "";
	}

    /**
     * Se encarga de generar un id de mensaje que se guarda
     * en sesión para que luego sea captutador por el generador
     * de mensajes
     * DM - 2015-10-30
     **/
    static function generarIdMensaje($texto_mensaje,$tipo_mensaje)
    {
        $id = rand(10000, 99999);
        $tipo_mensaje = strtolower($tipo_mensaje);
        $tipo_mensaje = substr($tipo_mensaje, 0,1);
        $codigo_mensaje = 'msg_'.$tipo_mensaje.'_'.$id;

        $_SESSION['mensajes'][ $codigo_mensaje ] = $texto_mensaje;

        return $codigo_mensaje;
    }//Fin de generarIdMensaje


    /**
     * Se encarga de mostrar un mensaje al usuario agregandole el formato
     * de despliegue para mensajes
     * DM - 2015-02-25
     **/
    public static function generarMensaje($codigo_mensaje, $tipo_msj = 0, $usa_idiomas=true)
    {
        global $idi_despliegue;

        if( $usa_idiomas )
        {
            $mensaje = $idi_despliegue[ $codigo_mensaje ];
        }
        else
        {
            $mensaje = $codigo_mensaje;
        }
       $tipo = "warning";
       if ($tipo_msj == 1) {
            $tipo = "success";
                    //'<div class="alert alert-success">' . $mensaje . '.</div>';
        } else if ($tipo_msj == 2) {
            $tipo= "info";
                    //'<div class="alert alert-info">' . $mensaje . '.</div>';
        } else if ($tipo_msj == 3) {
            $tipo = "danger";
                    //'<div class="alert alert-danger">' . $mensaje . '.</div>';
        }
            //'<alert class="alert alert-warning">' . $mensaje . '.</alert>';

        $contenido_mensaje =
             '<alert  ng-model="alerta_inicio" close="cerrarAlerta()"  ng-hide="estaCerrado()" type="'.$tipo.'" class="alertas_flotantes">' . $mensaje . '.</alert>';
        return $contenido_mensaje;

    }//Fin de generarMensaje

    /**
     * Se encarga de generar el código html con el contenido del mensaje que se va a mostrar.
     * Tiene en cuenta el código del mensaje que se le pasa por parámetro
     * DM - 2015-09-17
     **/
	static function crearMensaje($id_mensaje,$adicional=array())
	{
		global $_PATH_IMAGENES,$idi_despliegue,$idi_alertas;

		$datos['log_nombre_proceso_actual'] = "crearMensaje";
		$datos['log_termina_proceso'] = 0;
		$datos = logProceso($datos);
		$datos_log = $datos;

        //DM - 2015-10-30
        //Identifica si el mensaje tiene un código especial para generarlo
        //porque debe cargalo de la sesion
        $codigo_id_mensaje = substr($id_mensaje,0,6);

        switch($codigo_id_mensaje)
        {
            case 'msg_e_':
                $texto = $_SESSION['mensajes'][$id_mensaje];
                unset($_SESSION['mensajes'][$id_mensaje]);
                return Mensajes::generarMensaje($texto,3,false);
                break;
            case 'msg_s_':
                $texto = $_SESSION['mensajes'][$id_mensaje];
                unset($_SESSION['mensajes'][$id_mensaje]);
                return Mensajes::generarMensaje($texto,1,false);
                break;
            case 'msg_i_':
                $texto = $_SESSION['mensajes'][$id_mensaje];
                unset($_SESSION['mensajes'][$id_mensaje]);
                return Mensajes::generarMensaje($texto,2,false);
                break;
            case 'msg_w_':
                $texto = $_SESSION['mensajes'][$id_mensaje];
                unset($_SESSION['mensajes'][$id_mensaje]);
                return Mensajes::generarMensaje($texto,4,false);
                break;
        }//Fin de switch($codigo_id_mensaje)

		if( strlen($adicional['ACCION'])>1 )
		{
			$id_mensaje = 0;
		}

		switch($adicional['ACCION'])
		{
			case 0:
				$accion = $idi_despliegue['mensaje_consultado'];
				break;
			case 1:
				$accion = $idi_despliegue['mensaje_ingresado'];
				break;
			case 2:
				$accion = $idi_despliegue['mensaje_modificado'];
				break;
			case 3:
				$accion = $idi_despliegue['mensaje_eliminado'];
				break;
			case 4:
				$accion = $idi_despliegue['mensaje_liberado'];
				break;
			case 5:
				$accion = $idi_despliegue['mensaje_enviado'];
				break;
			case 6:
				$accion = $idi_despliegue['mensaje_copiado'];
				break;
			case 7:
				$accion = $idi_despliegue['mensaje_modificada']; //En femenino
				break;
			case 8:
				$accion = $idi_despliegue['mensaje_cancelado'];
				break;
			case 9:
				$accion = $idi_despliegue['mensaje_procesado'];
				break;
		}//Fin switch($tipo_accion_mensaje)

		switch($id_mensaje)
		{
			case 1:
				$imagen = "icon_exito.jpg";
				$tipo_mensaje = 1;
				$mensaje = $idi_despliegue['1'];
				break;
			case -1:
				$imagen = "icon_exito.jpg";
				$tipo_mensaje = 0;
				$mensaje = $idi_despliegue['-1'];
				break;
			case 2:
				$imagen = "icon_error.jpg";
				$tipo_mensaje = 0;
				$mensaje = $idi_despliegue['2'];
				break;
			case 3:
				$imagen = "icon_exito.jpg";
				$tipo_mensaje = 1;
				$mensaje = $idi_despliegue['1'];
				break;
			case -1000:
				$imagen = "icon_error.jpg";
				$tipo_mensaje = 0;
				$mensaje = $idi_despliegue['-1000'];
				break;
			case -1001:
				$imagen = "icon_error.jpg";
				$tipo_mensaje = 0;
				$mensaje = $idi_despliegue['-1001'];
				break;
			case -1002:
				$imagen = "icon_error.jpg";
				$tipo_mensaje = 0;
				$mensaje = $idi_despliegue['-1002'];
				break;
			case -1003:
				$imagen = "icon_error.jpg";
				$tipo_mensaje = 0;
				$mensaje = $idi_despliegue['-1003'];
				break;
			case -1004:
				$imagen = "icon_error.jpg";
				$tipo_mensaje = 0;
				$mensaje = $idi_despliegue['-1004'];
				break;
			case -4:
				$imagen = "icon_error.jpg";
				$tipo_mensaje = 0;
				$mensaje = $idi_despliegue['-4'];
				break;
			case -1005:
				$imagen = "icon_error.jpg";
				$tipo_mensaje = 0;
				$mensaje = $idi_despliegue['-1005'];
				break;
			case -1006:
				$imagen = "icon_error.jpg";
				$tipo_mensaje = 0;
				$mensaje = $idi_despliegue['-1006'];
				break;
			case -6:
				$imagen = "icon_error.jpg";
				$tipo_mensaje = 0;
				$mensaje = $idi_despliegue['-6'];
				break;
			case -7:
				$imagen = "icon_advertencia.jpg";
				$tipo_mensaje = 0;
				$mensaje = $idi_despliegue['-7'];
				break;
			case 4:
				$imagen = "icon_exito.jpg";
				$tipo_mensaje = 1;
				$mensaje = $idi_despliegue['4'];
				break;
			//MAOH - 15 Feb 2012 - Accion con mensaje adicional
			case 10:
				$imagen = "icon_exito.jpg";
				$tipo_mensaje = 1;
				$mensaje = $idi_despliegue['10'];
        		$mensaje = str_replace("<!--adicional-->", $adicional['ADICIONAL'], $mensaje);
				break;
			case 11:
				$imagen = "icon_advertencia.jpg";
				$tipo_mensaje = 0;
				$mensaje = $idi_despliegue['11'];
        		$mensaje = str_replace("<!--adicional-->", $adicional['ADICIONAL'], $mensaje);
				break;
			case 12:
					$imagen = "icon_error.jpg";
					$tipo_mensaje = 0;
					//si pertenece a una cadena muestra mensaje correspondiente al ingresar correo repetido en Lista robinson
					if ($_SESSION['pertenece_cad'] == 0)
					{
						$mensaje = $idi_despliegue['registro_ya_existe_individual'];
					}
					else if ($_SESSION['pertenece_cad'] == 1)
					{
						$mensaje = $idi_despliegue['registro_ya_existe_para_hotel'];
					}
					else
					{
						$mensaje = $idi_despliegue['registro_ya_existe_para_cadena'];
					}
	    		$mensaje = str_replace("<!--adicional-->", $adicional['ADICIONAL'], $mensaje);
				break;
            case 'DES-1':
                $mensaje = $idi_alertas['mensaje_alerta_seleccion_destinatarios_newsletter'];
				$tipo_mensaje = 0;
                break;
            case 'CH-1':
                $mensaje = $idi_despliegue['mensaje_actualizar_chat_correcto'];
				$tipo_mensaje = 0;
                break;
            case 'CSV-3':
                $mensaje = $idi_despliegue['mensaje_csv_personalizado_error'];
				$tipo_mensaje = 0;
                break;
            case 'CSV-4':
                $mensaje = $idi_despliegue['mensaje_csv_personalizado_error_requerido'];
				$tipo_mensaje = 0;
                break;
            case 'CSV-5':
                $mensaje = $idi_despliegue['archivo_no_cargado'];
				$tipo_mensaje = 0;
                break;
            case 'CSV-6':
                $mensaje = $idi_despliegue['auxiliar_archivo_sin_lineas'];
				$tipo_mensaje = 0;
                break;
            case 'CIC-1':
                $mensaje = $idi_despliegue['mensaje_vista_previa_sin_acceso'];
				$tipo_mensaje = 0;
                break;
            case 'OFE-1':
                $mensaje = $idi_despliegue['msg_oferta_sin_imagenes'];
				$tipo_mensaje = 0;
                break;
            case 'OFE-2':
                $mensaje = $idi_despliegue['mensaje_seleccion_ofertas'];
				$tipo_mensaje = 0;
                break;
            case 'OFE-3':
                $mensaje = $idi_despliegue['sin_establecimentos_store_copiar_seleccion'];
				$tipo_mensaje = 0;
                break;
            case 'OFE-4':
                $mensaje = $idi_despliegue['mensaje_copia_seleccion_store'];
				$tipo_mensaje = 0;
                break;
            case 'PLC-1':
                $mensaje = $idi_despliegue['mensaje_envio_correo_proponer_proveedor'];
				$tipo_mensaje = 1;
                break;
            case 'PLC-2':
                $mensaje = $idi_despliegue['mensaje_error_envio_correo_proponer_proveedor'];
				$tipo_mensaje = 1;
                break;

            //JP - 2014-04-21
            case 'CAT-1':
                $mensaje = $idi_despliegue['mensaje_error_eliminar_categoria_oferta'];
				        $tipo_mensaje = 0;
                break;
            case 'CAT-2':
                $mensaje = $idi_despliegue['mensaje_error_modificar_categoria_oferta'];
				        $tipo_mensaje = 0;
                break;

            case 'TR-1':
                $mensaje = $idi_despliegue['mensaje_tr1'];
				$tipo_mensaje = 1;
                break;
            case 'DH-1':
                $mensaje = $idi_despliegue['mensaje_dh1'];
				$tipo_mensaje = 0;
                break;
            //MAOH - 23 Ene 2012
			case 'DH-2':
                $mensaje = $idi_despliegue['DH-2'];
				$tipo_mensaje = 0;
                break;
            case 'RE-1':
                $mensaje = $idi_despliegue['mensaje_re1'];
				$tipo_mensaje = 0;
                break;
            case 'S-2':
				$mensaje = $idi_despliegue['mensaje_s2'];
				$tipo_mensaje = 0;
                break;
			case 'U-0':
				$mensaje = $idi_despliegue['U-0'];
				$tipo_mensaje = 0;
				break;
			case 'U-2':
				$mensaje = $idi_despliegue['U-2'];
				$tipo_mensaje = 0;
				break;
			case 'U-3':
				$mensaje = $idi_despliegue['U-3'];
				$tipo_mensaje = 0;
				break;
			case 'U-4':
				$mensaje = $idi_despliegue['U-4'];
				$tipo_mensaje = 0;
				break;
			case 'U-5':
				$mensaje = $idi_despliegue['U-5'];
				$tipo_mensaje = 1;
				break;
			case 'U-6':
				$mensaje = $idi_despliegue['U-6'];
				$tipo_mensaje = 0;
				break;
			case 'A-2':
			     $mensaje = $idi_despliegue['A-2'];
                $mensaje = str_replace("<!--ses_minimo_dias_envio-->",$_SESSION['est_minimo_dias_envio'],$mensaje);
			     $mensaje = str_replace("<!--ses_fecha_salida-->",$_SESSION['hue_fecha_salida_existe'],$mensaje);
				$tipo_mensaje = 0;
			     break;
			case 'A-3':
			     $mensaje = $idi_despliegue['A-3'];
				$tipo_mensaje = 0;
				break;
			case 'A-4':
			     $mensaje = $idi_despliegue['A-4'];
				$tipo_mensaje = 0;
				break;
			case 'A-5':
                //se encontro un registro con el mismo numero de habitación
			     $mensaje = $idi_despliegue['A-5'];
				$tipo_mensaje = 0;
				break;
			case 'CU-1':
			     $mensaje = $idi_despliegue['CU-1'];
				$tipo_mensaje = 1;
			     break;
			case 'CU-2':
			     $mensaje = $idi_despliegue['CU-2'];
				$tipo_mensaje = 0;
			     break;
			case 'SP-1':
			     $mensaje = $idi_despliegue['SP-1'];
				$tipo_mensaje = 0;
			     break;
			case 'IN-1':
			     $mensaje = $idi_despliegue['IN-1'];
				$tipo_mensaje = 0;
			     break;
            case 'CT-1':
			     $mensaje = $idi_despliegue['CT-1'];
				$tipo_mensaje = 0;
			     break;
			case 'IN-2':
			     $mensaje = $idi_despliegue['IN-2'];
				$tipo_mensaje = 0;
			     break;
			case 'HU-2':
			     $mensaje = $idi_despliegue['HU-2'];
				$tipo_mensaje = 0;
			     break;
			case 'HU-1':
			     $mensaje = $idi_despliegue['HU-1'];
				$tipo_mensaje = 1;
			     break;
			case 'HU-3':
			     $mensaje = $idi_despliegue['HU-3'];
				$tipo_mensaje = 0;
			     break;
			case 'HU-4':
			     $mensaje = $idi_despliegue['HU-4'];
				 $tipo_mensaje = 0;
			     break;
			case 'HU-5':
			     $mensaje = $idi_despliegue['HU-5'];
				 $tipo_mensaje = 0;
			     break;
			case 'HU-6':
			     $mensaje = $idi_despliegue['HU-6'];
				 $tipo_mensaje = 0;
			     break;
            case 'HU-7':
			     $mensaje = $idi_despliegue['HU-7'];
				 $tipo_mensaje = 0;
			     break;
            case 'HU-8':
			     $mensaje = $idi_despliegue['HU-8'];
				 $tipo_mensaje = 0;
			     break;
            case 'HU-9':
			    $mensaje = $idi_despliegue['HU-9'];
				 $tipo_mensaje = 1;
			     break;
			case 'F-1':
				$mensaje = $idi_despliegue['F-1'];
				$tipo_mensaje = 0;
				break;
			case 'F-2':
				$mensaje = $idi_despliegue['F-2'];
				$tipo_mensaje = 0;
				break;
			case 'F-3':
				$mensaje = $idi_despliegue['F-3'];
				$tipo_mensaje = 0;
				break;
			case 'F-4':
				$mensaje = $idi_despliegue['F-4'];
				$tipo_mensaje = 0;
				break;
			case 'F-5':
				$mensaje = $idi_despliegue['F-5'];
				$tipo_mensaje = 0;
				break;
			case 'F-6':
				$mensaje = $idi_despliegue['F-6'];
				$tipo_mensaje = 0;
				break;
			case 'F-7':
				$mensaje = $idi_despliegue['F-7'];
				$tipo_mensaje = 0;
				break;
			case 'F-8':
				$mensaje = $idi_despliegue['F-8'];
				$tipo_mensaje = 0;
				break;
            case 'F-9':
				$mensaje = $idi_despliegue['F-9'];
				$tipo_mensaje = 0;
				break;
			//MAOH - 01 Nov 2011 - Paquetes
            case 'PQ-1':
				$mensaje = $idi_despliegue['PQ-1'];
                $mensaje = str_replace("<!--paq_nombre_existe-->", $_SESSION['paq_nombre_existe'], $mensaje);
				$tipo_mensaje = 0;
				break;
            case 'PQ-2':
				$mensaje = $idi_despliegue['PQ-2'];
				$tipo_mensaje = 0;
				break;
			//MAOH - 06 Feb 2012 - Mensaje para que se mire la tabla en la parte del csv
            case 'CSV-1':
				$mensaje = $idi_despliegue['CSV-1'];
				$tipo_mensaje = 0;
				break;
            case 'CSV-2':
				$mensaje = $idi_despliegue['CSV-2'];
				$tipo_mensaje = 1;
				break;
			//MAOH - 27 Ago 2012 - Incidencia 1803: xml para pasar comentarios de clientes a la pagina web del hotel
            case 'XML-0':
				$mensaje = $idi_despliegue['XML-0'];
				$tipo_mensaje = 0;
				break;
            case 'XML-1':
				$mensaje = $idi_despliegue['XML-1'];
				$tipo_mensaje = 1;
				break;
            case 'XML-2':
				$mensaje = $idi_despliegue['XML-2'];
				$tipo_mensaje = 0;
				break;
            case 'XML-3':
				$mensaje = $idi_despliegue['XML-3'];
				$tipo_mensaje = 1;
				break;
            case 'XML-4':
				$mensaje = $idi_despliegue['XML-4'];
				$tipo_mensaje = 0;
				break;
            case 'EST-1':
				$mensaje = $idi_despliegue['EST-1'];
				$tipo_mensaje = 0;
                break;
            case 'EST-2':
				$mensaje = $idi_despliegue['EST-2'];
				$tipo_mensaje = 0;
                break;
            case 'EST-EXISTE':
				$mensaje = $idi_despliegue['est_existe'];
				$tipo_mensaje = 0;
                break;
            case 'EST-ALTA':
				$mensaje = $idi_despliegue['EST-ALTA'];
                $mensaje = str_replace("[*correo*]",$_SESSION['EST_ALTA_CORREO'],$mensaje);
				$tipo_mensaje = 1;
                break;
            case 'CAD-ALTA':
				$mensaje = $idi_despliegue['CAD-ALTA'];
                $mensaje = str_replace("[*correo*]",$_SESSION['CAD_ALTA_CORREO'],$mensaje);
				$tipo_mensaje = 1;
                break;
            case 'CAD-1':
                $mensaje = $idi_despliegue['CAD-1'];
				$tipo_mensaje = 0;
                break;
            case 'CUE-1':
                $mensaje = $idi_despliegue['CUE-1'];
				$tipo_mensaje = 1;
                break;
            case 'CUE-2':
                $mensaje = $idi_despliegue['CUE-2'];
				$tipo_mensaje = 0;
                break;
            case 'FM-1':
                $mensaje = $idi_despliegue['FM-1'];
				$tipo_mensaje = 0;
                break;
			case 0:
				$imagen = "icon_error.jpg";
				$mensaje = $idi_despliegue['0'];
				$tipo_mensaje = 0;
				break;
			//MAOH - 27 Ago 2012 - Fin
            default:
                $id_mensaje = is_array($id_mensaje) ? 0 : $id_mensaje;
				$mensaje = $idi_despliegue[ $id_mensaje ];
				$tipo_mensaje = 0;
                break;
		}


        //DM - 2015-03-23 si no hay mensaje debe buscar si el codigo esta en idiomas
        if( $mensaje == '' && !is_array($id_mensaje) )
        {
            $mensaje = $idi_despliegue[ $id_mensaje ] ;
            $tipo_mensaje = 0;
        }//Fin de if( $mensaje == '' && !is_array($id_mensaje) )


		$mensaje = str_replace("<!--la_cadena-->",$adicional['CADENA'],$mensaje);
        $mensaje = str_replace("<!--accion-->",$accion,$mensaje);

		if($tipo_mensaje == 0)
		{
			$imagen_mensaje = "<i class='fa fa-remove'></i> ";
			$clase_mensaje = "danger";

		}

		if($tipo_mensaje == 1)
		{
			$imagen_mensaje = "<i class='fa fa-check'></i> ";
			$clase_mensaje = "success";
		}

        if( $adicional['flotante'] == 1 )
        {
            $contenido_mensaje = "<div class='alert alert-".$clase_mensaje." alert-dismissable-custom' style=\"padding-right:15px;\">
                    <div style=\"float:right;cursor:pointer;\" onClick=\"$('.alert-dismissable-custom').remove();\">
                        <img src=\"".$_PATH_IMAGENES."/cerrar_ligthbox.png\">
                    </div> 
                    $imagen_mensaje $mensaje
                    </div>";        
        }
        else
        {
            $contenido_mensaje = "<div class='alert alert-".$clase_mensaje." alert-dismissable-custom'> $imagen_mensaje $mensaje</div>";
        }
		
		if(!isset($tipo_mensaje))
		{
			$contenido_mensaje = "";
		}

		if( is_null($id_mensaje) )
		{
			$contenido_mensaje = "";
		}

        //Si solo se necesita el mensaje lo retorna
        if( $adicional['solo_mensaje'] == 1 )
        {
            return $mensaje;
        }//Fin de if( $adicional['solo_mensaje'] == 1 )

		$datos_log['log_termina_proceso'] = 1;
		$datos_log = logProceso($datos_log);

		return $contenido_mensaje;
	}

	function crearMensajes($ids_mensajes)
	{
		if(is_array($ids_mensajes) && sizeof($ids_mensajes) > 0)
		{
			foreach($ids_mensajes as $id_mensaje)
			{
				$this->_contenido_mensajes .= $this->crearMensaje($id_mensaje);
			}
		}

		return $this->_contenido_mensajes;
	}

    /**
     * Se encarga de guardar en una lista los mensajes
     * que se van a mostrar en una pantalla
     * DM - 2015-09-18
     **/
    static function agregarMensajes($mensaje)
    {
        $_SESSION['mensajes_interfaz'] = isset($_SESSION['mensajes_interfaz']) ? $_SESSION['mensajes_interfaz'] : array();

        $_SESSION['mensajes_interfaz'][] = $mensaje;
    }//Fin de agregarMensajes

    /**
     * Se encarga de obtener la lista de todos los mensajes almacenados
     * y luego borrarlos
     * DM - 2015-09-18
     **/
    static function obtenerMensajes()
    {
        if( is_array($_SESSION['mensajes_interfaz']) )
        {
            $mensaje = implode("",$_SESSION['mensajes_interfaz']);
            unset($_SESSION['mensajes_interfaz']);
            return $mensaje;
        }
        return '';
    }//Fin de obtenerMensajes

    /**
     * Se encarga de mostar un mensaje tipo alerta al ingresar
     * a una pantalla
     * DM - 2016-03-22
     **/
    static function generarMensajePantalla($datos)
    {
        if (isset($datos['recargar'])){
            $reload = 'onclick="recargar(\''.$datos['recargar'].'\')"';
        }else{
            $reload = '';
        }

        $contenido = '
            <div class="alert alert-success alert-dismissable" style="font-size:14px; text-align:center;">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" '.$reload.'>×</button>
                    '.$datos['mensaje'].'
                </div>';

        return $contenido;
    }//Fin de generarMensajePantalla
}
?>
