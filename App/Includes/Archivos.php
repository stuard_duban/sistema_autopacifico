<?php

/*
*	Nombre: Archivos.php
*	Descripci�n: Maneja todo lo referente a la subida de archivos al servidor a trav�s de un formulario HTML
*	Autor: Diego Alejandro Ruiz Tabares
*	E-mail: diegoale@gmail.com
*	Fecha de Creaci�n: 05-05-2006
*	Fecha de �ltima Modificaci�n: 08-05-2006
*/


class Archivos
{

	var $_archivos; //Los archivos que se van a subir al servidor desde un mismo formulario html

/*	******************************************	CONSTRUCTOR ARCHIVOS	*******************************************************	*/

	function Archivos ($archivos)
	{
		$this->_archivos = $archivos; //En la variable $archivos viene el arreglo $_FILES
	}


/*	******************************************	VERIFICAR CARGA ARCHIVO	*******************************************************	*/

/*
*	Utilidad:
*		Verifica si un archivo fue cargado en el servidor a trav�s del m�todo POST de un formulario
*	Par�metros de entrada:
*		nombre_campo_texto_en_formulario -> El nombre del campo en el formulario usado para la selecci�n del archivo
*	Valores de retorno:
*		1, si el archivo fue cargado en el servidor
*		-1, si el tama�o del archivo excede el permitido en el php.ini
*		-2, si el tama�o del archivo excede el valor especificado en el campo MAX_FILE_SIZE del formulario (opcional)
*		-3, si el archivo no fue cargado totalmente en el servidor
*		-4, si no se seleccion� ning�n archivo
*/

	function verificarCargaArchivo($nombre_campo_texto_en_formulario)
	{
		if(is_uploaded_file($this->_archivos[$nombre_campo_texto_en_formulario]['tmp_name']))
		{
			return 1; //El archivo fue cargado en el servidor
		}
        else if( $this->_archivos[$nombre_campo_texto_en_formulario]['error'] > 0 )
        {
            return (-1 * $this->_archivos[$nombre_campo_texto_en_formulario]['error']); //El archivo no pudo ser cargado
        }
		else
		{
            //Error al tratar de copiar el archivo
			return -6;
		}
	}


/*	******************************************	VERIFICAR TIPO ARCHIVO	*******************************************************	*/

/*
*	Utilidad:
*		Verifica si un archivo que fue cargado en el servidor es de un determinado tipo de formato. Para mayor informaci�n,
*		consulte las directivas AddType de la configuraci�n del servidor Apache.
*	Par�metros de entrada:
*		$nombre_campo_texto_en_formulario -> El nombre del campo en el formulario usado para la selecci�n del archivo
*		$tipos_archivo_esperado -> Uno de los tipos de formato que se espera que tenga el archivo
*	Valores de retorno:
*		1, si el archivo es del tipo esperado
*		-1, si el archivo no pudo ser cargado en el servidor
*		-2, si el archivo no es del tipo esperado
*/

	function verificarTipoArchivo($nombre_campo_texto_en_formulario, $tipos_archivo_esperado)
	{
		$resultado_carga_archivo = $this->verificarCargaArchivo($nombre_campo_texto_en_formulario);
		if($resultado_carga_archivo == 1)
		{
			$tipo_archivo = strtolower($this->_archivos[$nombre_campo_texto_en_formulario]['type']);
			 //DMG Cambia la funcion ereg por preg_match
			foreach($tipos_archivo_esperado as $tipo_archivo_esperado)
			{
				$tipo_archivo_esperado = strtolower($tipo_archivo_esperado);
        //	if(ereg($tipo_archivo_esperado, $tipo_archivo))
				if(preg_match("/$tipo_archivo_esperado/", $tipo_archivo))
				{
					return 1; // El archivo es del tipo esperado
				}
			}
			return -2; // El archivo no es del tipo esperado
		}
		else
		{
			return -1; // El archivo no pudo ser cargado
		}
	}

/*	******************************************	SUBIR ARCHIVO	*******************************************************	*/

/*
*	Utilidad:
*		Copia el archivo cargado a trav�s del m�todo POST de un formulario en el directorio seleccionado y con el nombre espec�fico
*	Par�metros de entrada:
*		$nombre_campo_texto_en_formulario -> El nombre del campo en el formulario usado para la selecci�n del archivo
*		$directorio_destino -> Nombre del directorio en el cual se copiar� el archivo
*		$nombre_archivo_destino -> Nuevo nombre para el archivo que va a ser copiado
*		$tipos_archivo_esperado -> Uno de los tipos de formato que se espera que tenga el archivo
*	Valores de retorno:
*		nombre_archivo_destino, si el archivo fue cargado en el servidor y copiado en el directorio seleccionado y con el nombre espec�fico
*		-1, si el tama�o del archivo excede el permitido en el php.ini
*		-2, si el tama�o del archivo excede el valor especificado en el campo MAX_FILE_SIZE del formulario (opcional)
*		-3, si el archivo no fue cargado totalmente en el servidor
*		-4, si no se seleccion� ning�n archivo
*		-5, si el archivo que se va a copiar ya existe en el servidor
*		-6, si el archivo no pudo ser copiado en el directorio seleccionado y con el nombre espec�fico
*		-7, si el archivo no tiene uno de los Tipos de Archivo esperados
*/

	function subirArchivo($nombre_campo_texto_en_formulario, $directorio_destino, $nombre_archivo_destino="", $tipos_archivo_esperado="")
	{
		$resultado_carga_archivo = $this->verificarCargaArchivo($nombre_campo_texto_en_formulario);
        if ($resultado_carga_archivo == 1) //El archivo fue cargado en el servidor
		{
			if($tipos_archivo_esperado != "") // Se debe validar el Formato del Archivo
			{
				$resultado_tipo_fichero = $this->verificarTipoArchivo($nombre_campo_texto_en_formulario, $tipos_archivo_esperado);
				if($resultado_tipo_fichero != 1) // El Archivo no tiene uno de los Tipos de Archivo esperados
				{
					return -7;
				}
			}
			if($nombre_archivo_destino == "")
			{
				$nombre_archivo_destino = $this->_archivos[$nombre_campo_texto_en_formulario]['name'];
			}
			$nombre_archivo_destino = $this->modificarEspaciosEnBlanco($nombre_archivo_destino, "_");
			$archivo_existe = $this->verificarSiArchivoExiste($directorio_destino, $nombre_archivo_destino);
			if($archivo_existe)
			{
				return -5; //El archivo que se va a copiar ya existe
			}
			else
			{
				$resultado_de_mover_archivo = move_uploaded_file($this->_archivos[$nombre_campo_texto_en_formulario]['tmp_name'], $directorio_destino.$nombre_archivo_destino);
				if($resultado_de_mover_archivo == true)
				{
					return $nombre_archivo_destino; //El archivo fue copiado exitosamente en el servidor
				}
				else
				{
					return -6; //El archivo no pudo ser copiado en el servidor
				}
			}
		}
		else
		{
			return $resultado_carga_archivo; //El archivo no pudo ser cargado en el servidor
		}
	}

/*	******************************************	VERIFICAR SI ARCHIVO EXISTE	*******************************************************	*/

/*
*	Utilidad:
*		Verifica si un archivo existe
*	Par�metros de entrada:
*		directorio -> El nombre del directorio donde est� ubicado el archivo
*		nombre_archivo -> El nombre del archivo
*	Valores de retorno:
*		true, si el archivo especificado existe
*		false, si no existe el archivo especificado
*/

	static function verificarSiArchivoExiste ($directorio, $nombre_archivo)
	{
		if(@file_exists($directorio.$nombre_archivo))
		{
			return true; //Existe el Archivo especificado
		}
		else
		{
			return false; //No existe el Archivo especificado
		}
	}

/*	******************************************	ELIMINAR ARCHIVO	*******************************************************	*/

/*
*	Utilidad:
*		Elimina un archivo del servidor
*	Par�metros de entrada:
*		directorio -> El nombre del directorio donde est� ubicado el archivo
*		nombre_archivo -> El nombre del archivo a ser eliminado
*	Valores de retorno:
*		1, si el archivo especificado fue eliminado del servidor
*		-1, si el archivo especificado no existe
*		-2, si el archivo especificado no pudo ser eliminado del servidor
*/

	function eliminarArchivo ($directorio, $nombre_archivo)
	{
		$archivo_existe = Archivos::verificarSiArchivoExiste($directorio, $nombre_archivo);
		if($archivo_existe ) //Existe el Archivo especificado
		{

         //Si es un directorio entonces no lo puede eliminar y genera error
         if( is_dir($directorio.$nombre_archivo) )
         {
            return -2;
         }//Fin de if( is_dir($directorio.$nombre_archivo) )

			if(@unlink($directorio.$nombre_archivo)) //Se Elimin� exitosamente el Archivo
			{
				return 1;
			}
			else //No se pudo Eliminar el Archivo
			{
				return -2;
			}
		}
		else //No existe el Archivo especificado
		{
			return -1;
		}
	}


/*	******************************************	AGREGAR SERIAL A NOMBRE ARCHIVO	*******************************************************	*/

/*
*	Utilidad:
*		A�ade un N�mero Serial al Nombre del Archivo especificado
*	Par�metros de entrada:
*		$nombre_campo_texto_en_formulario -> El nombre del campo en el formulario usado para la selecci�n del archivo
*		$numero_serial -> N�mero del Serial que se desea a�adir al Nombre del Archivo
*	Valores de retorno:
*		$nombre_final_archivo -> Nombre modificado del Archivo
*/

	function agregarSerialANombreArchivo($nombre_campo_texto_en_formulario, $numero_serial)
	{
		$nombre_final_archivo = "";
		$partes_nombre_archivo = explode(".", $this->_archivos[$nombre_campo_texto_en_formulario]["name"]);
		if(sizeof($partes_nombre_archivo) > 1)
		{
			for($i=0; $i<sizeof($partes_nombre_archivo)-1; $i++)
			{
				$nombre_final_archivo .= $partes_nombre_archivo[$i];
				if($i < sizeof($partes_nombre_archivo)-2)
				{
					$nombre_final_archivo .= ".";
				}
			}
			$nombre_final_archivo .= "_".$numero_serial.".".$partes_nombre_archivo[sizeof($partes_nombre_archivo)-1];
		}
		else
		{
			$nombre_final_archivo .= $nombre_archivo."_".$numero_serial;
		}
		return $nombre_final_archivo;
	}


/*	******************************************	MODIFICAR ESPACIOS EN BLANCO	*******************************************************	*/

/*
*	Utilidad:
*		Reemplaza los espacios en blanco existentes en una cadena por otra cadena
*	Par�metros de entrada:
*		cadena -> La cadena que se va a modificar
*		cadena_reemplazo -> La cadena que va a sustituir los espacios en blanco
*	Valores de retorno:
*		cadena, cuyos espacios en blanco fueron reemplazados por el contenido de cadena_reemplazo
*/

	function modificarEspaciosEnBlanco($cadena, $cadena_reemplazo)
	{
		$cadena = trim($cadena);
		$cadena = str_replace(" ", $cadena_reemplazo, $cadena);
		return $cadena;
	}


	function obtenerContenidoArchivo($ruta)
	{
		$contenido = "";

		if( file_exists($ruta) )
		{
			$file = fopen($ruta,"r");

			$contenido = fread($file, filesize($ruta));

			fclose($file);
		}//Fin de if( file_exists($ruta) )
		else
		{
			$contenido = "<br>No existe el archivo:".$ruta."<br>";
		}

		return $contenido;

	}//Fin de obtenerContenidoArchivo()

	function crearArchivo($ruta,$contenido)
    {
        $file = fopen($ruta,"a");

        if( $file )
        {
            fwrite($file,$contenido);
            fclose($file);
        }
    }//Fin de crearArchivo()

    /**
     * Funcion que se encarga de copiar la informaci�n de un directorio
     * a otro.
     * DM - 2013-08-15
     ***/
    function copiarDirectorio($src,$dst)
    {

        $dir = opendir($src);

        @mkdir($dst);

        while(false !== ( $file =  readdir($dir)) )
        {

            if (( $file  != '.' ) && ( $file != '..' ))
            {

                if (  is_dir($src . DIRECTORY_SEPARATOR . $file) )
                {
                    Archivos::copiarDirectorio($src . DIRECTORY_SEPARATOR . $file,$dst .  DIRECTORY_SEPARATOR . $file);
                }
                else
                {
                    copy($src . DIRECTORY_SEPARATOR . $file,$dst . DIRECTORY_SEPARATOR .  $file);
                }
            }
        }
        closedir($dir);
    }//Fin de copiarDirectorio


    /**
     * Funci�n que se encarga de eliminar un directorio
     * DM - 2013-08-15
     **/
    static function eliminarDirectorio($directorio)
    {
        foreach(glob($directorio . "/*") as $archivos_carpeta)
        {
            if (is_dir($archivos_carpeta))
            {
                Archivos::eliminarDirectorio($archivos_carpeta);
            }
            else
            {
                unlink($archivos_carpeta);
            }
        }
        rmdir($directorio);
    }//Fin de eliminarDirectorio()


    /**
     * Funci�n que se encarga de retornar un arreglo de los archivos con extension
     * gif, jpg o png en un directorio
     * JP - 2013-09-30
     **/
    function listarArchivosImagenes($directorio)
    {
        $imagenes = array();

        if( file_exists($directorio) && is_dir($directorio))
        {
           //i($directorio);
            $dirint = dir($directorio);
           // i($dirint);
          	while (($archivo = $dirint->read()) !== false)
          	{  //DMG Cambia la funcion eregi por preg_match
               //if (eregi("gif", $archivo) || eregi("jpg", $archivo) || eregi("png", $archivo) || eregi("jpeg", $archivo) )
               if (preg_match("/gif/", strtolower($archivo) ) || preg_match("/jpg/", strtolower($archivo) ) || preg_match("/png/", strtolower($archivo) ) || preg_match("/jpeg/", strtolower($archivo) ) )
               {
          		    $imagenes [] = $archivo;
               }
          	}
          	$dirint->close();
        }//Fin de if( file_exists($directorio) && is_dir($directorio))

        return($imagenes);
    }//Fin de listarArchivosImagenes($directorio)



}//Fin de clase Archivos
?>
