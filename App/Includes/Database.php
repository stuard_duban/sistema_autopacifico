<?php

/*
  CARLOS H. CERON M. CARCERON@HOTMAIL.COM
  JULIO 01 2008

  EN ESTE FICHERO SE INCLUYEN TODAS LAS FUNCIONES NECESARIAS PARA LA CONEXION A LA BASE DE DATOS,
  INSERTS, UPDATES, DELETES, SELECT, ASI COMO TAMBIEN OBTENER EL NUMERO DE FILAS AFECTADAS Y DEMAS.
 */

class database {

    // maneja la conexion a la base de datos
    var $_conector = '';
    // maneja la consulta arrojada a la base de datos
    var $_sql = '';
    // maneja el resultado de una operacion mysql_query
    var $_resultset = null;
    // maneja el limit de las consultas
    var $_limit = 0;
    // maneja el offset de las consultas
    var $_offset = 0;
    var $_key_db = 0;
    var $_db = array();
    var $_en_transaccion = array();
    var $_num_transacciones = array();

    function Database($host, $user, $password, $db, $key_db = 0) {
        $this->_key_db = $key_db;
        $this->_db[$this->_key_db] = $db;
        $this->_en_transaccion[$this->_key_db] = false;
        $this->_num_transacciones[$this->_key_db] = 0;

        if (!$this->_conector[$this->_key_db] = mysql_connect($host, $user, $password)) {
            $error_db = 1; //no conecta
            echo "<div class='mensaje_error'>Error conectando al host " . $host . " con usuario: " . $user . " </div>";
            exit;
        }

        $this->usarDB();
    }
    
    
    function estaConectado()
    {
        $conexion = mysql_stat( $this->_conector[$this->_key_db] ); 
        return !is_null($conexion);
    }//Fin de estaConectado

    function usarDB() {
        global $_debug_error_sql;

        $db = $this->_db[$this->_key_db];
        if ($db != '' && !mysql_select_db($db, $this->_conector[$this->_key_db])) {
            echo "<div class='mensaje_error'>La base de datos especificada no existe</div>";
            if ($_debug_error_sql) {
                echo "<div class='mensaje_error'>Base de datos:" . $db . "<br />
                    key: " . $this->_key_db . "
                    </div>";
            }//Fin de if( $_debug_error_sql )
            exit;
        }
    }

//Fin de usarDB()

    function iniciarTransaccion( $datos = array() )
    {
        if( $datos['en_transaccion'] == _TRANSACCION_PADRE )
        {
            return true;
        }
        //incrementa el contador de transacciones
        $this->_num_transacciones[$this->_key_db] ++;

        //Si una transaccion ya esta iniciada entonces solo retorna true
        if ($this->_en_transaccion[$this->_key_db]) {
            return true;
        }

        $sql = "BEGIN;";
        $resultado = $this->ejecutarSQL($sql);
        if ($resultado) {
            $this->_en_transaccion[$this->_key_db] = true;
        }
        return $resultado;
    }

    function cancelarTransaccion( $datos=array() )
    {
        if( $datos['en_transaccion'] == _TRANSACCION_PADRE )
        {
            return true;
        }
        //Si cancela la transaccion el proceso vuelve a cero
        $this->_num_transacciones[$this->_key_db] = 0;

        //Si una transaccion ya esta iniciada entonces la cancela
        if ($this->_en_transaccion[$this->_key_db]) {
            $sql = "ROLLBACK;";
            $resultado = $this->ejecutarSQL($sql);
            if ($resultado) {
                $this->_en_transaccion[$this->_key_db] = false;
            }
            return $resultado;
        }//Fin de if( $this->_en_transaccion[ $this->_key_db ] )
        return true;
    }

//Fin de cancelarTransaccion

    function terminarTransaccion($forzar_cierre_transaccion = false, $datos=array() )
    {
        if( $datos['en_transaccion'] == _TRANSACCION_PADRE )
        {
            return true;
        }

        //Si una transaccion no esta iniciada entonces solo retorna true
        if ($this->_en_transaccion[$this->_key_db] == false) {
            return true;
        }

        $this->_num_transacciones[$this->_key_db] --;

        if ($this->_num_transacciones[$this->_key_db] == 0 || $forzar_cierre_transaccion) {
            $sql = "COMMIT;";
            $resultado = $this->ejecutarSQL($sql);
            if ($resultado) {
                $this->_en_transaccion[$this->_key_db] = false;
                $this->_num_transacciones[$this->_key_db] = 0;
            }
            return $resultado;
        }//Fin de if( $this->_num_transacciones[ $this->_key_db ] == 0 )

        return true;
    }

//Fin de terminarTransaccion

    function desconectar() {
        global $_debug_error_sql;

        if (!@mysql_close($this->_conector[$this->_key_db])) {
            echo "<div class='mensaje_warning'>Error desconectando de la base de datos</div>";
            if ($_debug_error_sql) {
                echo "<div class='mensaje_error'>key: " . $this->_key_db . "
                    </div>";
            }//Fin de if( $_debug_error_sql )
            exit;
        }
    }

    //esta funcion se llama para actualizar, eliminar o insertar registros
    function ejecutarSql($sql) {
        global $_debug_error_sql, $idi_despliegue;

        //DM - 2015-02-18
        $this->registrarLog($sql);

        $this->usarDB();

        //MAOH - 19 Abr 2012 - Cambio a UTF 8
        $setCharacter = "SET character_set_client=utf8";
        mysql_query($setCharacter, $this->_conector[$this->_key_db]);
        $setCharacter = "SET character_set_connection=utf8";
        mysql_query($setCharacter, $this->_conector[$this->_key_db]);
        $codificacion = "SET character_set_results=utf8";
        mysql_query($codificacion, $this->_conector[$this->_key_db]);
        $sql_utf = "SET NAMES 'utf8'";
        mysql_query($sql_utf, $this->_conector[$this->_key_db]);


        if (!$this->_resultset = mysql_query($sql, $this->_conector[$this->_key_db])) {
            echo "<div class='mensaje_error'>" . $idi_despliegue['error_base_datos'] . "</div>";
            if ($_debug_error_sql) {
                echo "<div class='mensaje_error'>
                Metodo: EjecutarSQL <br />
                Ejecucion MYSQL =>" . mysql_error() . "<= <br />
                SQL =>" . $sql . "<=</div>";
            }//Fin de if( $_debug_error_sql )
            return null;
        }

        //DM - 2012-10-18
        //registra el sql que se esta ejecutando
        $this->almacenarOperacion($sql);

        return $this->_resultset;
    }

    function obtenerNumeroFilas() {
        if (!$this->_resultset)
            return 0;
        else
            return mysql_num_rows($this->_resultset);
    }

    function obtenerNumeroFilasAfectadas() {
        if (!$this->_resultset)
            return 0;
        else
            return mysql_affected_rows($this->_conector[$this->_key_db]);
    }

    function obtenerConsulta($sql) {
        //DM - 2015-02-18
        $this->registrarLog($sql);

        //MAOH - 19 Abr 2012 - Cambio a UTF 8
        $setCharacter = "SET character_set_client=utf8";
        mysql_query($setCharacter, $this->_conector[$this->_key_db]);
        $setCharacter = "SET character_set_connection=utf8";
        mysql_query($setCharacter, $this->_conector[$this->_key_db]);
        $codificacion = "SET character_set_results=utf8";
        mysql_query($codificacion, $this->_conector[$this->_key_db]);
        $sql_utf = "SET NAMES 'utf8'";
        mysql_query($sql_utf, $this->_conector[$this->_key_db]);

        $this->usarDB();
        return $this->_resultset = mysql_query($sql, $this->_conector[$this->_key_db]);
    }

//Fin de obtenerConsulta()
    //obtiene la primera columna de la primera fila
    function obtenerResultado($sql) {
        global $_debug_error_sql, $idi_despliegue;

        //DM - 2015-02-18
        $this->registrarLog($sql);

        $this->usarDB();

        //MAOH - 19 Abr 2012 - Cambio a UTF 8
        $setCharacter = "SET character_set_client=utf8";
        mysql_query($setCharacter, $this->_conector[$this->_key_db]);
        $setCharacter = "SET character_set_connection=utf8";
        mysql_query($setCharacter, $this->_conector[$this->_key_db]);
        $codificacion = "SET character_set_results=utf8";
        mysql_query($codificacion, $this->_conector[$this->_key_db]);
        $sql_utf = "SET NAMES 'utf8'";
        mysql_query($sql_utf, $this->_conector[$this->_key_db]);

        $_sql = $sql;
        if (!$this->_resultset = mysql_query($sql, $this->_conector[$this->_key_db])) {
            echo "<div class='mensaje_error'>" . $idi_despliegue['error_base_datos'] . "</div>";
            if ($_debug_error_sql) {
                echo "<div class='mensaje_error'>
                Metodo: obtenerResultado <br />
                Ejecucion MYSQL =>" . mysql_error() . "<= <br />
                SQL =>" . $sql . "<=</div>";
            }//Fin de if( $_debug_error_sql )
            return null;
        }
        if ($row = mysql_fetch_row($this->_resultset)) {
            $ret = $row;
        }
        //mysql_free_result( $this->_resultset );
        return $ret;
    }

    //obtiene un arreglo con los resultados de la consulta
    function obtenerRegistros($sql) {
        global $_debug_error_sql, $idi_despliegue;

        //DM - 2015-02-18
        $this->registrarLog($sql);

        $this->usarDB();

        //MAOH - 19 Abr 2012 - Cambio a UTF 8
        $setCharacter = "SET character_set_client=utf8";
        mysql_query($setCharacter, $this->_conector[$this->_key_db]);
        $setCharacter = "SET character_set_connection=utf8";
        mysql_query($setCharacter, $this->_conector[$this->_key_db]);
        $codificacion = "SET character_set_results=utf8";
        mysql_query($codificacion, $this->_conector[$this->_key_db]);
        $sql_utf = "SET NAMES 'utf8'";
        mysql_query($sql_utf, $this->_conector[$this->_key_db]);

        if (!$this->_resultset = mysql_query($sql, $this->_conector[$this->_key_db])) {
            echo "<div class='mensaje_error'>" . $idi_despliegue['error_base_datos'] . "</div>";
            if ($_debug_error_sql) {
                echo "<div class='mensaje_error'>
                Metodo: ObtenerRegistros <br />
                Ejecucion MYSQL =>" . mysql_error() . "<= <br />
                SQL =>" . $sql . "<=</div>";
            }//Fin de if( $_debug_error_sql )
            return null;
        }
        $array = array();
        while ($row = mysql_fetch_row($this->_resultset)) {
            $array[] = $row;
        }
        //mysql_free_result( $this->_resultset );
        return $array;
    }

    //obtiene un arreglo con los campos asociativos
    function obtenerRegistrosAsociativos($sql) {
        global $_debug_error_sql, $idi_despliegue;

        //DM - 2015-02-18
        $this->registrarLog($sql);

        $this->usarDB();

        //MAOH - 19 Abr 2012 - Cambio a UTF 8
        $setCharacter = "SET character_set_client=utf8";
        mysql_query($setCharacter, $this->_conector[$this->_key_db]);
        $setCharacter = "SET character_set_connection=utf8";
        mysql_query($setCharacter, $this->_conector[$this->_key_db]);
        $codificacion = "SET character_set_results=utf8";
        mysql_query($codificacion, $this->_conector[$this->_key_db]);
        $sql_utf = "SET NAMES 'utf8'";
        mysql_query($sql_utf, $this->_conector[$this->_key_db]);


        if (!$this->_resultset = mysql_query($sql, $this->_conector[$this->_key_db])) {
            echo "<div class='mensaje_error'>" . $idi_despliegue['error_base_datos'] . "</div>";
            if ($_debug_error_sql) {
                echo "<div class='mensaje_error'>
                Metodo: ObtenerRegistrosAsociativos <br />
                Ejecucion MYSQL =>" . mysql_error() . "<= <br />
                SQL =>" . $sql . "<=</div>";
            }//Fin de if( $_debug_error_sql )
            return null;
        }

        $array = array();
        while ($row = mysql_fetch_assoc($this->_resultset)) {
            $array[] = $row;
        }

        return $array;
    }

    //obtiene un arreglo con los campos asociativos
    function obtenerRegistrosAsociativosPorClave($sql, $clave, $opciones = array()) {
        global $_debug_error_sql, $idi_despliegue;

        //DM - 2015-02-18
        $this->registrarLog($sql);

        $this->usarDB();

        //MAOH - 19 Abr 2012 - Cambio a UTF 8
        $setCharacter = "SET character_set_client=utf8";
        mysql_query($setCharacter, $this->_conector[$this->_key_db]);
        $setCharacter = "SET character_set_connection=utf8";
        mysql_query($setCharacter, $this->_conector[$this->_key_db]);
        $codificacion = "SET character_set_results=utf8";
        mysql_query($codificacion, $this->_conector[$this->_key_db]);
        $sql_utf = "SET NAMES 'utf8'";
        mysql_query($sql_utf, $this->_conector[$this->_key_db]);



        if (!$this->_resultset = mysql_query($sql, $this->_conector[$this->_key_db])) {
            echo "<div class='mensaje_error'>" . $idi_despliegue['error_base_datos'] . "</div>";
            if ($_debug_error_sql) {
                echo "<div class='mensaje_error'>
                Metodo: ObtenerRegistrosAsociativosPorClave <br />
                Ejecucion MYSQL =>" . mysql_error() . "<= <br />
                SQL =>" . $sql . "<=</div>";
            }//Fin de if( $_debug_error_sql )
            return null;
        }

        $array = array();
        while ($row = mysql_fetch_assoc($this->_resultset))
        {
            if( isset( $opciones['usar_solo_clave'] ) )
            {
                $array[ $row[$clave] ] = 0;
            }
            else if( isset( $opciones['clave_valor_usar'] ) )
            {
                $array[ $row[$clave] ] = $row[ $opciones['clave_valor_usar'] ];
            }
            else
            {
                $array[ $row[$clave] ] = $row;
            }

        }

        return $array;
    }


    /**
     * Se encarga de generar un arreglo asociativo colocando como claves
     * los valores puestos en el arreglo lista_claves, si se pasa el parámetro
     * agrupados, quiere decir que los valores
     * son organizados en una lista de todos los registros que encuentra
     * y que no se sobre escribe con el ultimo valor que llega
     * DM - 2016-06-27
     ***/
    function obtenerRegistrosAsociativosAgrupados($sql,$lista_claves,$agrupado = false) {

        global $_debug_error_sql, $idi_despliegue;

        //DM - 2015-02-18
        $this->registrarLog($sql);

        $this->usarDB();

        //MAOH - 19 Abr 2012 - Cambio a UTF 8
        $setCharacter = "SET character_set_client=utf8";
        mysql_query($setCharacter, $this->_conector[$this->_key_db]);
        $setCharacter = "SET character_set_connection=utf8";
        mysql_query($setCharacter, $this->_conector[$this->_key_db]);
        $codificacion = "SET character_set_results=utf8";
        mysql_query($codificacion, $this->_conector[$this->_key_db]);
        $sql_utf = "SET NAMES 'utf8'";
        mysql_query($sql_utf, $this->_conector[$this->_key_db]);



        if (!$this->_resultset = mysql_query($sql, $this->_conector[$this->_key_db])) {
            echo "<div class='mensaje_error'>" . $idi_despliegue['error_base_datos'] . "</div>";
            if ($_debug_error_sql) {
                echo "<div class='mensaje_error'>
                Metodo: ObtenerRegistroAsociativosAgrupados <br />
                Ejecucion MYSQL =>" . mysql_error() . "<= <br />
                SQL =>" . $sql . "<=</div>";
            }//Fin de if( $_debug_error_sql )
            //var_dump($sql);
            return null;
        }

        $array = array();
        while ($row = mysql_fetch_assoc($this->_resultset))
        {

            $cadena_claves = '$array';
            foreach($lista_claves as $clave)
            {
                $cadena_claves .= '[$row['.$clave.']]';

            }//Fin de foreach($lista_claves as $clave)

            if( $agrupado )
            {
                $cadena_claves .= '[] = $row;';
            }
            else
            {
                $cadena_claves .= ' = $row;';
            }
            eval($cadena_claves);
        }

        return $array;
    }

    function obtenerRegistrosAsociativosAgrupadosPorClave($sql, $clave, $clave_nivel_2 = "", $grupo_nivel2 = false) {

        global $_debug_error_sql, $idi_despliegue;

        //DM - 2015-02-18
        $this->registrarLog($sql);

        $this->usarDB();

        //MAOH - 19 Abr 2012 - Cambio a UTF 8
        $setCharacter = "SET character_set_client=utf8";
        mysql_query($setCharacter, $this->_conector[$this->_key_db]);
        $setCharacter = "SET character_set_connection=utf8";
        mysql_query($setCharacter, $this->_conector[$this->_key_db]);
        $codificacion = "SET character_set_results=utf8";
        mysql_query($codificacion, $this->_conector[$this->_key_db]);
        $sql_utf = "SET NAMES 'utf8'";
        mysql_query($sql_utf, $this->_conector[$this->_key_db]);



        if (!$this->_resultset = mysql_query($sql, $this->_conector[$this->_key_db])) {
            echo "<div class='mensaje_error'>" . $idi_despliegue['error_base_datos'] . "</div>";
            if ($_debug_error_sql) {
                echo "<div class='mensaje_error'>
                Metodo: ObtenerRegistroAsociativosAgrupadosPorClave <br />
                Ejecucion MYSQL =>" . mysql_error() . "<= <br />
                SQL =>" . $sql . "<=</div>";
            }//Fin de if( $_debug_error_sql )
            //var_dump($sql);
            return null;
        }

        $array = array();
        while ($row = mysql_fetch_assoc($this->_resultset)) {
            if ($clave_nivel_2 != "") {
                if ($grupo_nivel2) {
                    $array[$row[$clave]][$row[$clave_nivel_2]][] = $row;
                } else {
                    $array[$row[$clave]][$row[$clave_nivel_2]] = $row;
                }
            } else {
                $array[$row[$clave]][] = $row;
            }
        }

        return $array;
    }

    function obtenerUltimoIdGenerado() {
        $this->usarDB();

        return mysql_insert_id($this->_conector[$this->_key_db]);
    }

    function obtenerProximoId($datos) {
        $sql = "select max(" . $datos['id'] . ") as id
				from " . $datos['tabla'] . " ";

        $res = $this->obtenerResultado($sql);
        if (is_array($res) && sizeof($res) > 0) {
            $res = $res[0] + 1;
        } else {
            $res = 1;
        }
        return $res;
    }

//Fin de obtenerProximoId($datos)

    function generarSQLInsertar($datos, $campos) {
        $lista_valores = array();
        foreach ($campos as $key => $valor) {
            if (is_numeric($key)) {
                $datos[$valor] = str_replace("\\'", "'", $datos[$valor]);
                $datos[$valor] = str_replace("\'", "'", $datos[$valor]);
                $datos[$valor] = str_replace("'", "\'", $datos[$valor]);
                $lista_valores[$valor] = "'" . $datos[$valor] . "'";
            } else {
                $datos[$key] = str_replace("\\'", "'", $datos[$key]);
                $datos[$key] = str_replace("\'", "'", $datos[$key]);
                $datos[$key] = str_replace("'", "\'", $datos[$key]);
                $lista_valores[$key] = $datos[$key];
            }
        }//Fin de foreach($campos as $key => $valor)

        if ($datos['sql_solo_valores'] == 1) {
            $sql = "(" . implode(",", $lista_valores) . ")";
        } else {
            $sql = "insert into " . $datos['tabla'] . " (" . implode(",", array_keys($lista_valores)) . ")
    				values (" . implode(",", $lista_valores) . ")";
        }

        return $sql;
    }

//Fin de generarSQLInsertar()

    /**
     *
     * Utilidad: metodo que se encarga de hacer un Insert compartido
     * cuando se necesita hacer valores muy grandes
     * @param type $datos arreglo con los valores a insertar
     * @param type $campos los campos de la tabla que se van a insertar
     * @return type true or false de la insercion
     */

    function insertarSQLCompartido($datos, $campos) {
        $lista_valores = array();
        $campos_valores = array();
        //recorre los valores a insertar linea a linea
        foreach ($datos['valores'] as $pos => $linea)
        {
            foreach ($campos as $key => $valor) {
                if (is_numeric($key)) {
                    $limpio = str_replace("\\'", "'", $linea[$valor]);
                    $limpio = str_replace("\'", "'", $linea[$valor]);
                    $limpio  = str_replace("'", "\'", $linea[$valor]);
                    $lista_valores[$pos][$valor] = "'" . $limpio  . "'";
                } else {
                    $limpio = str_replace("\\'", "'", $linea[$valor]);
                    $limpio = str_replace("\'", "'", $linea[$key]);
                    $limpio  = str_replace("'", "\'", $linea[$key]);
                    $lista_valores[$pos][$key] = $limpio;
                }
            }//Fin de foreach($campos as $key => $valor)
            //genera la estructura por grupos de los valores a insertar
            $campos_valores[] = "(" . implode(",", $lista_valores[$pos]) . ")";
        }

        $valores = implode(",",  $campos_valores);
        $sql = "INSERT INTO " . $datos['tabla'] . " (" . implode(",", $campos) . ") VALUES $valores";
        return $this->ejecutarSql($sql);
    }

    function generarSQLActualizar($datos, $campos) {
        $lista_valores = array();
        foreach ($campos as $key => $valor) {
            if (is_numeric($key)) {
                $datos[$valor] = str_replace("\\'", "'", $datos[$valor]);
                $datos[$valor] = str_replace("\'", "'", $datos[$valor]);
                $datos[$valor] = str_replace("'", "\'", $datos[$valor]);
                $lista_valores[] = $valor . "='" . $datos[$valor] . "'";

            } else
            {
                if( $valor['procesar_campo_sql'] != 1 )
                {
                    $datos[$key] = str_replace("\\'", "'", $datos[$key]);
                    $datos[$key] = str_replace("\'", "'", $datos[$key]);
                    $datos[$key] = str_replace("'", "\'", $datos[$key]);
                }

                $lista_valores[] = $key . "=" . $datos[$key];
            }
        }//Fin de foreach($campos as $key => $valor)

        $sql = "update " . $datos['tabla'] . "
				set " . implode(",", $lista_valores) . "
				where " . $datos['condicion'] . "";
        return $sql;
    }

//Fin de generarSQLActualizar()

    function obtenerRegistro($tabla, $condicion, $campos_select = '*') {
        $sql = "select " . $campos_select . "
					from " . $tabla . "
					where " . $condicion . "
                    limit 1
                    ";

        $valores = $this->obtenerRegistrosAsociativos($sql);
        return $valores[0];
    }

//Fin de obtenerValor

    /**
     * Funcion que se encargar de almancear los sql que se ejecutan
     * en la base de datos
     * DM - 2012-10-18
     *  */
    function almacenarOperacion($sql) {
        $key = "sql_backup" . $this->_key_db;

        //Solo registra en la variable de sesion
        //si se encuentra creada, de lo contrario no hace nada
        if (isset($_SESSION[$key])) {
            $_SESSION[$key] .= "[" . $sql . "]\n";
        }//Fin de if( isset($_SESSION[ $key ]) )
    }

//Fin de almacenarOperacion

    /**
     * Funcion que se encarga de crear y ejecutar un sql para actualizar
     * los datos de una tabla
     * DM - 2013-02-12
     * ** */
    function actualizarDatosTabla($datos, $campos) {
        $sql = $this->generarSQLActualizar($datos, $campos);
        return $this->ejecutarSql($sql);
    }

//Fin de actualizarDatosTabla

    /**
     * Funcion que se encarga de crear y ejecutar un sql para actualizar
     * los datos de una tabla
     * DM - 2016-02-10
     * ** */
    function actualizar($datos) {
        $sql = $this->generarSQLActualizar($datos, $datos['campos']);
        return $this->ejecutarSql($sql);
    }

//Fin de actualizarDatosTabla

    /**
     * Se encarga de insertar un nuevo registro en la base de datos a
     * partir de los par�metros que se pasan
     * DM - 2016-01-15
     * */
    function insertar($datos) {
        $sql = $this->generarSQLInsertar($datos, $datos['campos']);
        return $this->ejecutarSql($sql);
    }

//Fin de insertar

    /**
     * Funcion que se encarga de crear y ejecutar el sql para ingresar
     * datos a una tabla
     * DM - 2013-02-12
     * * */
    function insertarDatosTabla($datos, $campos) {
        $sql = $this->generarSQLInsertar($datos, $campos);
        return $this->ejecutarSql($sql);
    }

//Fin de insertarDatosTabla()

    /**
     * Se encarga de hacer un borrado l�gico o permanente de un registro
     * en la base de datos
     * DM - 2016-02-10
     * */
    function eliminar($datos) {
        if ($datos['borrado_permanente'] == 1) {
            $sql = "DELETE
                        FROM " . $datos['tabla'] . "
                        WHERE " . $datos['condicion'];
            return $this->ejecutarSql($sql);
        } else {
            $sql = $this->generarSQLActualizar($datos, $datos['campos']);
            return $this->ejecutarSql($sql);
        }
    }

//Fin de eliminar

    /**
     * Se encarga de almacenar todos los sql que contienen la palabra delete,
     * se pretende guardar los posibles ataques por borrado de datos
     * DM - 2015-02-18
     * */
    function registrarLog($sql) {
        global $_PATH_BASE_DATOS_LOG, $_PATH_SERVIDOR;

        //Solo se registra si el sql contiene la palabra delete
        if (strpos(strtolower($sql), 'delete') === false) {
            return;
        }

        //Trata de identificar la ip del equipo que esta ejecutando el sql
        $ip = "No detectada";
        if (isset($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        } else if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_VIA'])) {
            $ip = $_SERVER['HTTP_VIA'];
        }

        $carpeta_log = 'pdn';
        if (strpos($_PATH_SERVIDOR, '/dllo') !== false) {
            $carpeta_log = 'dllo';
        }//Fin de if( strpos($_PATH_SERVIDOR,'/dllo') !== false )
        else if (strpos($_PATH_SERVIDOR, '/demo') !== false) {
            $carpeta_log = 'demo';
        }//Fin de if( strpos($_PATH_SERVIDOR,'/demo') !== false )
        else if (strpos($_PATH_SERVIDOR, '/tool') !== false) {
            $carpeta_log = 'tool';
        }//Fin de if( strpos($_PATH_SERVIDOR,'/demo') !== false )


        $carpeta = $_PATH_BASE_DATOS_LOG . $carpeta_log;

        if (!file_exists($carpeta)) {
            //si la carpeta no existe la debe crear
            mkdir($carpeta);
        }//Fin de if ( !file_exists($carpeta) )
        //Crea un archivo por d�a
        $ruta = $carpeta . '/' . date('Y-m-d') . '.log';

        $file = fopen($ruta, "a");

        if ($file) {
            //agrega datos del Usuario y del Establecimiento que haya en la sesi�n
            $val = 'U:' . $_SESSION['usu_id'] . '_E' . $_SESSION['est_id'];

            $contenido = date('H:i:s') . ' => ' . $ip . ' => ' . $val . ' ==> ' . $sql;
            $contenido .= "\r\n";
            fwrite($file, $contenido);
            fclose($file);
        }//Fin de if( $file )
    }

//Fin de registrarLog

    /**
     * Se encarga de verificar si un campo existe en una tabla
     * DM - 2015-02-19
     * * */
    function existeCampoTabla($tabla, $campo) {
        $sql = "SHOW COLUMNS FROM " . $tabla;
        $lista_campos = $this->obtenerRegistrosAsociativosPorClave($sql, "Field");

        $campo = strtolower($campo);

        return is_array($lista_campos[$campo]);
    }

//Fin de existeCampoTabla

    /**
     * Se encarga de realizar las consultas para obtener los valores que se pasan
     * al listado de interfaz para generar una tabla
     * DM - 2015-03-16
     * */
    function obtenerValoresListadoInterfaz($datos) {
        global $_limit;

        //Asigna los valores de offset y limit teniendo en cuenta si vienen por par�metro o los carga por defecto
        $offset = ( isset($datos['offset']) && is_numeric($datos['offset']) ? $datos['offset'] : 0);
        $limit = ( isset($datos['limit']) && is_numeric($datos['limit']) ? $datos['limit'] : $_limit);

        //Si hay campos para ordernar el listado los agrega
        $orden = "";
        if ($datos['ordena'] != "") {
            $orden = "ORDER BY " . $datos['ordena'] . " " . $datos['tipo_ordena'];
        }

        $sql = $datos['sql'];
        $sql .= " " . $orden;
        $sql .= " limit " . $offset . "," . $limit;

        $listado_valores = $this->obtenerRegistrosAsociativos($datos['sql']);

        if (count($listado_valores) > 0) {
            $resultado_contador = $this->obtenerResultado($datos['sql_contador']);

            $listado_valores['limite'] = $limit;
            $listado_valores['offset'] = $offset;
            $listado_valores['num_total_registros'] = $resultado_contador[0];
            $listado_valores['usa_filtro'] = $datos['usa_filtro'];
        }//Fin de if( count($listado_valores) > 0 )

        return $listado_valores;
    }//Fin de obtenerValoresListadoInterfaz

    /**
     * Se encarga de generar una lista de registros de acuerdo al sql de consulta
     * y al sql de para paginación
     * DM - 2016-10-11
     **/
    function listar($datos)
    {
        global $_CONFIG;

        $limit = ( isset($datos['limit']) && is_numeric($datos['limit']) ? $datos['limit'] : $_CONFIG['limit_listado']);
        $offset = ( isset($datos['offset']) && is_numeric($datos['offset']) ? $datos['offset'] : 0);

        $orden = '';
        if ($datos['ordena'] != "")
        {
            $orden = "ORDER BY " . $datos['ordena'] . " " . $datos['tipo_ordena'];
        }

        $datos['sql'] .= " ".$orden;
        $datos['sql'] .= " LIMIT ".$offset.", ".$limit;

        switch( $datos['tipo_sql'] )
        {
            case 'grupo':
                $listado_registros = $this->obtenerRegistrosAsociativosAgrupadosPorClave( $datos['sql'] , $datos['clave_1']);
                break;

            case 'clave':
                $listado_registros = $this->obtenerRegistrosAsociativosPorClave( $datos['sql'] , $datos['clave_1']);
                break;

            default:
                $listado_registros = $this->obtenerRegistrosAsociativos( $datos['sql'] );
                break;
        }//Fin de switch( $datos['tipo_sql'] )

        if( count($listado_registros) > 0 )
        {
            $contador_registros = $this->obtenerRegistros( $datos['sql_paginacion'] );

            $listado_registros['limite'] = $limit;

            $listado_registros['offset'] = $offset;

            $listado_registros['num_total_registros'] = $contador_registros[0][0];

            $listado_registros['usa_filtro'] = $datos['usa_filtro'];

        }//Fin de if( count($listado_registros) > 0 )

        return $listado_registros;
    }//Fin de listar

}//class
?>
