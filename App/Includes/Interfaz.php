<?php

class Interfaz {

    var $_obj_menu;
    var $_obj_mensajes;
    var $_l_js;
    var $_l_js_footer;
    var $_listado_js_externos;
    var $_listado_js_externos_footer;
    var $_l_css;
    var $_l_css_footer;
    var $_listado_css_externos;
    var $_listado_css_externos_footer;
    var $_plantillas;
    var $_migas;
    var $_contenido;
    var $_contenido_externo;
    var $_contenido_externo_general;
    var $_contenido_popup_automatico;
    var $_seccion;
    var $_link_ayuda;
    var $_titulo;
    var $_logo;
    var $_url_logo_establecimiento;
    var $_info;
    var $_propiedades;
    var $_campos;
    var $_calendario;
    var $_print_media;

    function Interfaz() {
        global $_PATH_SERVIDOR, $_opciones;

        $this->_plantillas = $_PATH_SERVIDOR . '/Includes/Plantillas';

        $this->_l_js = array();
        $this->_l_js_footer = array();
        $this->_listado_js_externos = array();
        $this->_listado_js_externos_footer = array();
        $this->_l_css = array();
        $this->_listado_css_externos = array();
        $this->_l_css_footer = array();
        $this->_listado_css_externos_footer = array();
        $this->_obj_menu = new Menu();
        $this->_migas = array();
        $this->_contenido = '';
        $this->_contenido_externo = '';
        $this->_contenido_externo_general = '';
        $this->_calendario = array();
        $this->_pasos_labels = "";
        $this->_seccion = '';
        $this->_url_logo_establecimiento = '';
        $this->_contenido_popup_automatico = '';
        $this->_selector_encuesta = array();

        $this->_campos = array();

        //$this->adicionarCSS('general.css');
    }

//Fin de Interfaz()

    /**
     * Se encarga de agregar un campo a la lista de
     * tokens que se van a usar en la plantilla
     * de la interfaz
     * DM - 2016-09-22
     **/
    function agregarTokenInterfaz($datos)
    {
        if( is_array($datos) )
        {
            $this->_campos = array_merge( $this->_campos, $datos );
        }
    }//Fin de agregarTokenInterfaz

    function asignarCampos($campos) {
        $this->_campos = $campos;
    }

    function asignarClasePrintBody($tipo) {
        $this->_print_media = $tipo;
    }

//Fin de asignarCampos()

    /**
     * Función que se encarga de asignar el contenido de un mensaje y desplegar solo
     * ese contendio en la interfaz
     * DM - 2013-09-23
     * */
    function asignarContenidoMensaje($datos) {
        global $_PATH_IMAGENES, $idi_despliegue;

        $contenido = '<br /><br /><br />' . $datos['contenido_mensaje'];

        $datos['label_boton'] = $datos['label_boton'] == '' ? $idi_despliegue['enlace_continuar'] : $datos['label_boton'];

        $contenido .= '<br />
        <center>
        <a class="enlace" ' . $datos['accion_boton'] . ' >' . $datos['label_boton'] . '</a>
        </center>
        ';

        $this->_contenido = $contenido;
    }

//Fin de asignarContenidoMensaje

    function asignarLogo($logo) {
        $this->_logo = $logo;
    }

    /**
     * Asigna la ubicación web del logo del establecimieto
     * DM - 2015-09-24
     * */
    function asignarURLLogoEstablecimiento($url_logo) {
        $this->_url_logo_establecimiento = $url_logo;
    }

//Fin de asignarURLLogoEstablecimiento

    function asignarTitulo($titulo) {
        $this->_titulo = $titulo;
    }

//Fin de asignarCampos()

    function asignarInfo($info) {
        $this->_info = $info;
    }

    function asignarPropiedades($propiedades) {
        $this->_propiedades = $propiedades;
    }

    //amp - 12/8/15 - migas de pan interfaz
    function asignarMigas($migas) {
        $this->_migas = $migas;
    }

    //amp - 19/10/15 - calendario general
    function asignarCalendario($cal) {
        $this->_calendario = $cal;
    }

    //amp - 19/10/15 - agrega selector de encuesta
    function asignarSelectorEncuesta($sel_encuesta) {
        $this->_selector_encuesta = $sel_encuesta;
    }
     //amp - 11/07/16 - calendario general
    function asignarLabelPasos($cal) {
        $this->_pasos_labels = $cal;
    }

    function adicionarJS($script, $usar_directorio_parametro = false) {
        //DM  - 2013-09-12 issue 2971
        if ($usar_directorio_parametro)
        {
            $this->_listado_js_externos[] = $script;
        } else {
            $this->_l_js[] = $script;
        }
    }

//Fin de adicionarJS()
    /*
     * Adicionar JS en el footer
     */

    function adicionarFooterJS($script, $usar_directorio_parametro = false) {
        //DM  - 2013-09-12 issue 2971
        if ($usar_directorio_parametro) {
            $this->_listado_js_externos_footer[] = $script;
        } else {
            $this->_l_js_footer[] = $script;
        }
    }

//Fin de adicionarFooterJS()

    /**
     * Se encarga se inicializar en ceros los arreglos de js
     * para que se vuelvan a cargar de acuerdo a los nuevos
     * parámetros
     * DM - 2014-06-16
     * */
    function resetJS() {
        $this->_listado_js_externos = array();
        $this->_l_js = array();
        $this->_l_js_footer = array();
        $this->_listado_js_externos_footer = array();
    }

//Fin de resetCSS

    /**
     * Se encarga se inicializar en ceros los arreglos de css
     * para que se vuelvan a cargar de acuerdo a los nuevos
     * parámetros
     * DM - 2014-06-16
     * */
    function resetCSS() {
        $this->_listado_css_externos = array();
        $this->_l_css = array();
        $this->_l_css_footer = array();
        $this->_listado_css_externos_footer = array();
    }

//Fin de resetCSS

    function adicionarCSS($estilo, $propiedades = null, $usar_directorio_parametro = false) {
        if ($usar_directorio_parametro) {
            //DM - 2013-09-23
            if (is_array($propiedades)) {
                $propiedades['url'] = $estilo;
                $this->_listado_css_externos[] = $propiedades;
            } else {
                $this->_listado_css_externos[] = $estilo;
            }
        }//Fin de if( $usar_directorio_parametro )
        else {
            //DM - 2013-09-23
            if (is_array($propiedades)) {
                $propiedades['url'] = $estilo;
                $this->_l_css[] = $propiedades;
            } else {
                $this->_l_css[] = $estilo;
            }
        }//Fin de else de if( $usar_directorio_parametro )
    }

//Fin de adicionarCSS()

    /*
     * AMP - 5/8/15 - Adecuar CSS en eñ footer merjorar despliegue de info
     */

    function adicionarFooterCSS($estilo, $propiedades = null, $usar_directorio_parametro = false) {
        if ($usar_directorio_parametro) {
            //DM - 2013-09-23
            if (is_array($propiedades)) {
                $propiedades['url'] = $estilo;
                $this->_listado_css_externos_footer[] = $propiedades;
            } else {
                $this->_listado_css_externos_footer[] = $estilo;
            }
        }//Fin de if( $usar_directorio_parametro )
        else {
            //DM - 2013-09-23
            if (is_array($propiedades)) {
                $propiedades['url'] = $estilo;
                $this->_l_css_footer[] = $propiedades;
            } else {
                $this->_l_css_footer[] = $estilo;
            }
        }//Fin de else de if( $usar_directorio_parametro )
    }

    function asignarContenido($contenido) {
        $this->_contenido = $contenido;
    }

    function asignarContenidoExterno($contenido) {
        $this->_contenido_externo = $contenido;
    }
    function asignarContenidoExternoGeneral($contenido) {
        $this->_contenido_externo_general = $contenido;
    }

//Fin de asignarContenido()

    function asignarSeccion($seccion) {
        global $_PATH_IMAGENES;

        $this->_seccion = "<table width='100%' border='0' cellspacing='0' cellpadding='0'>
			<tr>
				<td class='titulo'>" . $seccion . "</td>
			</tr>
			<tr>
				<td height='20' valign='middle'><img src='" . $_PATH_IMAGENES . "/linea_titulo.jpg' width='760'></td>
			</tr>
			</table>";
    }

//Fin de asignarSeccion()

    function crearInterfazGrafica($datos = array()) {
        global $_PATH_WEB, $_PATH_IMAGENES, $idi_despliegue, $idi_alertas,
        $_opciones, $_PATH_PAG_WEB, $id_encuesta_hotel, $_obj_database,
        $obj_usuarios, $_google_analytics_connect, $_google_analytics_club,
        $_google_analytics_cicerone, $_PATH_SERVIDOR, $_PATH_WEB_LOGOS;

        $idioma_alertas = "";
        if (is_array($idi_alertas)) {
            //MAOH - 18 Abr 2012 - Cambio a UTF 8
            $idioma_alertas = "<script languague=\"javascript\" charset=\"utf-8\">\nvar idioma = new Array();\n";
            //$idioma_alertas = "<script languague=\"javascript\">\nvar idioma = new Array();\n";
            foreach ($idi_alertas as $key => $texto) {
                $texto = str_replace("\n", "\\n", $texto);
                $texto = str_replace("\'", "'", $texto);
                $texto = str_replace("'", "\'", $texto);
                //DM - 2015-02-19
                $idioma_alertas .= "idioma['" . $key . "'] = '" . $texto . "' ;\n";
            }//Fin de foreach($idi_alertas as $key => $texto)
            $idioma_alertas .= "</script>";
        }//Fin de if( is_array($idi_alertas) )
        //DM - 2013-09-24
        $estilos = '';
        $contenido_estilos = '';
        $propiedades_contenido_estilos = array();

        if ($datos['include_css'] == 1) {
            //$contenido_estilos .= Archivos::obtenerContenidoArchivo($_PATH_SERVIDOR.'librerias/jquery-ui-1.10.3.custom/css/smoothness/jquery-ui-1.10.3.custom.min.css');
        }//Fin de if( $datos['include_css'] == 1 )
        else {
            //$estilos .= "<link href=\"".$_PATH_WEB."/librerias/jquery-ui-1.10.3.custom/css/smoothness/jquery-ui-1.10.3.custom.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n";
        }

        foreach ($this->_listado_css_externos as $estilo) {
            //DM - 2013-09-23
            if (is_array($estilo)) {
                //DM - 2014-06-16 issue 4595
                //Si el css se debe incluir en la plantilla html carga el archivo para agregarlo
                if ($estilo['include'] == 1) {
                    //Elimina la url y el parametro de include
                    $url_estilo = $estilo['url'];
                    unset($estilo['url']);
                    unset($estilo['include']);

                    $url_archivo = $url_estilo;
                    $contenido_css = Archivos::obtenerContenidoArchivo($url_archivo);

                    $contenido_css = str_replace("\r\n", "", $contenido_css);
                    //agregar el contenido del archivo
                    $contenido_estilos .= $contenido_css;

                    foreach ($estilo as $propiedad => $val_propiedad) {
                        $propiedades_contenido_estilos[$propiedad] = $val_propiedad;
                    }
                }//Fin de if( $estilo['include'] == 1 )
                else {
                    $url_estilo = $estilo['url'];
                    unset($estilo['url']);
                    $propiedades_estilo = '';
                    foreach ($estilo as $propiedad => $val_propiedad) {
                        $propiedades_estilo = ' ' . $propiedad . '="' . $val_propiedad . '"';
                    }
                    $estilos .= "<link href=\"" . $url_estilo . "\" rel=\"stylesheet\" type=\"text/css\" " . $propiedades_estilo . " />\n";
                }//Fin de else if( $estilo['include'] == 1 )
            } else {
                $estilos .= "<link href=\"" . $estilo . "\" rel=\"stylesheet\" type=\"text/css\" />\n";
            }
        }//Fin de foreach($this->_estilos as $estilo)


        foreach ($this->_l_css as $estilo)
        {
            if ($datos['include_css'] == 1 && $estilo == 'general.css')
            {
                $estilo = array(
                    'url' => $estilo,
                    'include' => 1);
            }//Fin de if( $datos['include_css'] == 1 && $estilo == 'general.css' )
            
            //DM - 2013-09-23
            if (is_array($estilo))
            {
                //DM - 2014-06-16 issue 4595
                //Si el css se debe incluir en la plantilla html carga el archivo para agregarlo
                if ($estilo['include'] == 1)
                {
                    //Elimina la url y el parametro de include
                    $url_estilo = $estilo['url'];
                    unset($estilo['url']);
                    unset($estilo['include']);

                    $url_archivo = $_PATH_SERVIDOR . 'Includes/CSS/' . $url_estilo;
                    $contenido_css = Archivos::obtenerContenidoArchivo($url_archivo);

                    $contenido_css = str_replace("\r\n", "", $contenido_css);
                    //agregar el contenido del archivo
                    $contenido_estilos .= $contenido_css;

                    foreach ($estilo as $propiedad => $val_propiedad) {
                        $propiedades_contenido_estilos[$propiedad] = $val_propiedad;
                    }
                }//Fin de if( $estilo['include'] == 1 )
                else 
                {
                    $url_estilo = $estilo['url'];
                    unset($estilo['url']);
                    $propiedades_estilo = '';
                    foreach ($estilo as $propiedad => $val_propiedad) {
                        $propiedades_estilo = ' ' . $propiedad . '="' . $val_propiedad . '"';
                    }
                    
                    //DM - 2017-09-12 issue 10647
                    //Obtiene la ultima modificación del fichero para generar la versión
                    $src_archivo = $_PATH_SERVIDOR . "Includes/CSS/" . $url_estilo;
                    $info_archivo = stat($src_archivo);
                    $version_archivo = $info_archivo['mtime'];
                    $estilos .= "<link href=\"" . $_PATH_WEB . "/Includes/CSS/" . $url_estilo . "?v=".$version_archivo."\" rel=\"stylesheet\" type=\"text/css\" " . $propiedades_estilo . " />\n";
                }//Fin de else if( $estilo['include'] == 1 )
            } else
            {
                //DM - 2017-09-12 issue 10647
                //Obtiene la ultima modificación del fichero para generar la versión
                $src_archivo = $_PATH_SERVIDOR . "Includes/CSS/" . $estilo;
                $info_archivo = @stat($src_archivo);
                $version_archivo = $info_archivo['mtime'];
                $estilos .= "<link href=\"" . $_PATH_WEB . "/Includes/CSS/" . $estilo . "?v=".$version_archivo."\" rel=\"stylesheet\" type=\"text/css\" />\n";
            }
        }//Fin de foreach($this->_estilos as $estilo)

        /* amp - 5/8/15 - armar el html de los esilos para el footer */
        $estilos_footer = "";
        foreach ($this->_listado_css_externos_footer as $estilo) {

            $estilos_footer .= "<link href=\"" . $estilo . "\" rel=\"stylesheet\" type=\"text/css\" />\n";
        }//Fin de foreach($this->_estilos as $estilo)


        foreach ($this->_l_css_footer as $estilo)
        {
            //DM - 2017-09-12 issue 10647
            //Obtiene la ultima modificación del fichero para generar la versión
            $src_archivo = $_PATH_SERVIDOR . "Includes/CSS/" . $estilo;
            $info_archivo = @stat($src_archivo);
            $version_archivo = $info_archivo['mtime'];
            $estilos_footer .= "<link href=\"" . $_PATH_WEB . "/Includes/CSS/" . $estilo . "?v=".$version_archivo."\" rel=\"stylesheet\" type=\"text/css\" />\n";
        }//Fin de foreach($this->_estilos as $estilo)
        
        //si hay contenido para agregar lo hace
        if ($contenido_estilos != '')
        {
            $propiedades_estilo = '';
            foreach ($propiedades_contenido_estilos as $propiedad => $val_propiedad) {
                $propiedades_estilo = ' ' . $propiedad . '="' . $val_propiedad . '" ';
            }

            $estilos .= '<style type="text/css" ' . $propiedades_estilo . '>' . $contenido_estilos . '</style>' . "\n";
        }//Fin de if( $contenido_estilos != '')

        $js = $idioma_alertas;
        foreach ($this->_l_js as $script)
        {         
            //DM - 2017-09-12 issue 10647
            //Obtiene la ultima modificación del fichero para generar la versión
            $src_archivo = $_PATH_SERVIDOR . "Includes/JS/" . $script;
            $info_archivo = @stat($src_archivo);
            $version_archivo = $info_archivo['mtime'];
            $js .= "<script src=\"" .$_PATH_WEB . "/Includes/JS/" . $script. "?v=".$version_archivo."\" language=\"Javascript\" type=\"text/javascript\" charset=\"utf-8\"></script>\n";
            //$js .= "<script src=\"".$_PATH_WEB."/Includes/JS/".$script."\" language=\"Javascript\" type=\"text/javascript\"></script>\n";
        }//Fin de foreach($this->_js as $script)
        
        // amp - 5/8/15 - agrego array para mostrar links en el footer
        $js_footer = "";
        foreach ($this->_listado_js_externos_footer as $script)
        {
            $js_footer .= "<script src=\"" . $script . "\" language=\"Javascript\" type=\"text/javascript\" charset=\"utf-8\"></script>\n";
        }//Fin de foreach($this->_listado_js_externos as $script)
        
        foreach ($this->_l_js_footer as $script)
        {
            //DM - 2017-09-12 issue 10647
            //Obtiene la ultima modificación del fichero para generar la versión
            $src_archivo = $_PATH_SERVIDOR . "Includes/JS/" . $script;
            $info_archivo = @stat($src_archivo);
            $version_archivo = $info_archivo['mtime']; 
            $js_footer .= "<script src=\"" . $_PATH_WEB . "/Includes/JS/" . $script . "?v=".$version_archivo."\" language=\"Javascript\" type=\"text/javascript\" charset=\"utf-8\"></script>\n";
        }//Fin de foreach($this->_js_foter as $script)
        
        //DM  - 2013-09-12 issue 2971
        foreach ($this->_listado_js_externos as $script)
        {
            if( is_array($script) )
            {
                $js .= "<script src=\"" . $script['url'] . "\" language=\"Javascript\" type=\"text/javascript\" charset=\"utf-8\" ".$script['propiedades']." ></script>\n";
            }
            else
            {
                $js .= "<script src=\"" . $script . "\" language=\"Javascript\" type=\"text/javascript\" charset=\"utf-8\"></script>\n";
            }
            
        }//Fin de foreach($this->_listado_js_externos as $script)
        //$js .= "<script src=\"".$_PATH_WEB."/librerias/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js\" language=\"Javascript\" type=\"text/javascript\" charset=\"utf-8\"></script>\n";

        $plantilla = $datos['plantilla'];
        if ($plantilla == "") {
            $plantilla = "principal";
        }
        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/" . $plantilla . ".html");


        //JP - 2014-04-19 #issue 4167
        Interfaz::asignarToken('google_analytics_connect', $_google_analytics_connect, $contenido);
        Interfaz::asignarToken('google_analytics_club', $_google_analytics_club, $contenido);
        Interfaz::asignarToken('google_analytics_cicerone', $_google_analytics_cicerone, $contenido);

        $contenido_politicas_cookies = Herramientas::generarContenidoPoliticasCookies($datos);

        Interfaz::asignarToken('contenido_politicas_cookies', $contenido_politicas_cookies, $contenido);

        Interfaz::asignarToken('contenido_popup_automatico', $this->_contenido_popup_automatico, $contenido);
        if ($datos['despedida'] == 1) {
            $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/principal_despedida.html");

            $enlace_pie_pagina = '<a href="' . $_PATH_PAG_WEB . '" target="_blank" ><img border="0" src="' . $_PATH_IMAGENES . '/sello_hq_peq.jpg" width="75" height="100"></a>';
            //si es para tablet, entonces no agrega el enlace para no modificar la congifuración de los carrousel
            if ($datos['dispositivo_movil'] && $datos['tipo_dispositivo'] == 't') {
                $enlace_pie_pagina = '<img border="0" src="' . $_PATH_IMAGENES . '/sello_hq_peq.jpg" width="75" height="100">';
            }
            Interfaz::asignarToken('enlace_pie_pagina', $enlace_pie_pagina, $contenido);
        }

        $menu = $this->_obj_menu->crearMenu();
        Interfaz::asignarToken('menu', $menu, $contenido);

        //MAOH - 18 Abr 2012 - Cambio a UTF 8
        Interfaz::asignarToken("codificacion", "UTF-8", $contenido);

        //Interfaz::asignarToken('datos',$datos,$contenido);

        Interfaz::asignarTokenDatos("label_pie_desarrollo", $idi_despliegue, $contenido);
        Interfaz::asignarTokenDatos("label_pie", $idi_despliegue, $contenido);
        Interfaz::asignarToken("imagenes", $_PATH_IMAGENES, $contenido);
        Interfaz::asignarToken("logo", $this->_logo, $contenido);
        Interfaz::asignarToken("url_logo_establecimiento", $this->_url_logo_establecimiento, $contenido);
        Interfaz::asignarToken("titulo", $this->_titulo, $contenido);
        Interfaz::asignarToken("info", $this->_info, $contenido);
        Interfaz::asignarToken("propiedades", $this->_propiedades, $contenido);

        if (is_array($this->_campos)) {
            foreach ($this->_campos as $clave => $valor) {
                Interfaz::asignarToken($clave, $valor, $contenido);
            }
        }//Fin de if( is_array($this->_campos) )
        //amp -12/8/15 - gestioar migas de pan
        if (is_array($this->_migas)) {
            $listado_migas = "";
            $cnta = 0;
            foreach ($this->_migas as $clave => $valor) {
                $listado_migas .= "<li";
                $listado_migas .= (array_key_exists('activo', $valor)) ? " class='active' style='font-weight:bold; color:#1ab394;'>" : ">";

                if (array_key_exists('activo', $valor)) {
                    $tituloPagina = $valor['titulo'];
                }

                $listado_migas .= (array_key_exists('m', $valor)) ?
                        "<a style='text-decoration:underline;' href='index.php?m=" .
                        $valor['m'] . "&accion=" .
                        $valor['accion'] . "&" .
                        $valor['extra'] . "' >" .
                        $valor['titulo'] . "</a>" : $valor['titulo'];
                $listado_migas .= "</li>";
                $cnta++;
            }//fin de foreach
            Interfaz::asignarToken("titulo_de_pagina", $tituloPagina, $contenido);
            if ($cnta > 1) {
                Interfaz::asignarToken("migas_de_pan", $listado_migas, $contenido);
            }
        }//Fin de if( is_array($this->_migas) )

        //asignar a interfaz el calendario general
        if (!empty($this->_calendario)) {
            if (!isset($this->_calendario['tipo_cal'])) {
                $this->_calendario['tipo_cal'] = 1;
            }
            $cal = $this->obtenerCalendarioGeneral($this->_calendario['tipo_cal'], $this->_calendario['fecha_i'], $this->_calendario['fecha_f']);
            
            //JMM-2017-18-08 - Desarrollo #10555
            $calendario_general = '<div class="<!--clase_calendario_general--> col-xs-12 pull-right">'.$cal.'</div>';
            Interfaz::asignarToken('calendario_general', $calendario_general, $contenido);
        }


        if (!empty($this->_print_media)) {
            Interfaz::asignarToken("print", "print-" . $this->_print_media, $contenido);
        }

        if ($this->_pasos_labels != ""){
        	
        	//JMM-2017-08-18 - Desarrollo #10555
        	$pasos_comunicacion = '<div class="pasos-comunicacion <!--clase_ajuste_top_com--> center">'.$this->mostrarPasosLabels($this->_pasos_labels).'</div>';
            Interfaz::asignarToken("pasos_comunicacion",$pasos_comunicacion, $contenido);
        }

        //pgs - 10/08/2012 - se pone el selector de encuestas, se trae las encuestas que tiene acceso el usuario
        if ($_SESSION['autenticado']) {
            $encuestas = $this->obtenerEncuestasAsociadas($datos);

            if (sizeof($encuestas) > 0)
            {
              //asignar a interfaz el selector de encuesta
              if (!empty($this->_selector_encuesta))
              {

                  $listado_encuestas = array();
                  foreach ($encuestas as $enc_id => $encuesta) {
                      $listado_encuestas[$enc_id] = $encuesta['eni_nombre'];
                  }//Fin de foreach($encuestas as $enc)
                  //si no existe una encuesta seleccionada
                  if (empty($_SESSION ['enc_id'])) {
                      //si exite el id de la encuesta hotel se pone como predefinido, sino se pone la primera del array
                      if (array_key_exists($id_encuesta_hotel, $listado_encuestas)) {
                          $_SESSION ['enc_id'] = $id_encuesta_hotel;
                      } else {
                          $_SESSION ['enc_id'] = array_shift($listado_encuestas);
                      }//Fin de if(array_key_exists($id_encuesta_hotel, $listado_encuestas))
                  }//Fin de if(empty($_SESSION ['enc_id']))

                  $datos_sel_encuesta['array'] = $listado_encuestas;

                  //OR 2016-07-13 - se crea el nuevo selector de encuestas
                  $datos['enc_id'] = $_SESSION['enc_id'];
                  $encuesta_actual = $listado_encuestas[$datos['enc_id']];
                  $nuevo_selector_enc = '<div class="input-group-btn pull-right row" role="navigation" style="margin-top: 20px;font-size:14px !important;" dropdown>';
                  $nuevo_selector_enc .= '<div class="dropdown-toggle pull-right" dropdown-toggle  type="button" style="cursor:pointer">
                                            <i class="fa fa-file-text-o"></i>&nbsp;
                                            '.$encuesta_actual.'
                                            <button class="btn btn-white">
                                              <span>'.$idi_despliegue[label_cambiar].'</span>&nbsp;&nbsp;<span class="caret"></span></button>
                                          </div>
                                          <ul class="dropdown-menu animated m-t-md">';

                                          // <i class="fa fa-file-text-o"></i>&nbsp;
                                          //                         '.$encuesta_actual.'

                  foreach ($listado_encuestas as $id_encuesta => $nombre_encuesta)
                  {
                      if ($id_encuesta != $datos['enc_id'])
                      {
                        $nuevo_selector_enc .= '<li>
                                                 <a onclick="cambiarEncuesta('.$id_encuesta.');" style="color:#676a6c;">'.$nombre_encuesta.'</a>
                                                </li>';
                      }
                  }
                  $nuevo_selector_enc .= '</ul></div>';
                  
                  //JMM-2017-08-18 - Desarrollo #10555
                  $selector_encuestas = '<div class="selector-encuestas col-xs-4 right">'.$nuevo_selector_enc.'</div>';
                  //acomodar nav top cuando aparezca el selector de encuesta
                  Interfaz::asignarToken("clase_ajuste_top",  "col-md-5", $contenido);
                  Interfaz::asignarToken("clase_ajuste_top_com",  "col-md-3", $contenido);
                  Interfaz::asignarToken("clase_calendario_general", "col-md-3", $contenido);
                  Interfaz::asignarToken("selector_encuestas", $selector_encuestas, $contenido);
              }else if ($this->_pasos_labels != ""){//pantalla para comunicacion puntual con labels
                  Interfaz::asignarToken("clase_ajuste_top",  "col-md-4", $contenido);
                  //JMM-2017-08-18 - Desarrollo #10555
                  Interfaz::asignarToken("clase_ajuste_top_com",  "col-md-8", $contenido);
                  Interfaz::asignarToken("clase_calendario_general", "col-md-2", $contenido);
              }else{//para demas pantallas sin calendario
                  Interfaz::asignarToken("clase_ajuste_top",  "col-md-7", $contenido);
                  Interfaz::asignarToken("clase_ajuste_top_com",  "col-md-2", $contenido);
                  Interfaz::asignarToken("clase_calendario_general", "col-md-3", $contenido);
              }
            }//Fin de if(sizeof($encuestas)>0)

        }//Fin de if($_SESSION['autenticado'])
        //pgs - 10/08/2012 - fin de, se pone el selector de encuestas


        if ($_POST['accion'] != "grabar_respuestas") {
            $logo_ith = '<img src="' . $_PATH_IMAGENES . '/logo_ITH.jpg">';
            Interfaz::asignarToken("logo_ith", $logo_ith, $contenido);
        }
        Interfaz::asignarToken("seccion", $this->_seccion, $contenido);

        if ($this->_contenido == "" && $this->_contenido_externo == "" && $this->_contenido_externo_general == "" ) {
            $this->_contenido = "<center><br /><br /><br /><a href='" . $_PATH_PAG_WEB . "'>Continuar</a><br /><br /></center>"; //MAOH - 13 Jun 2012
        }

        //OR 2015-09-09 se traen datos del establecimiento para asignar en la plantilla de despedida
        $_obj_est = new establecimientos();
        //Se hace si viene el id del establecimiento unicamente
        if (is_numeric($datos['est_id']) && $datos['est_id'] != "") {
            $info_est = $_obj_est->obtenerInfoEstablecimiento($datos['est_id']);
            Interfaz::asignarToken("est_nombre", $info_est["est_nombre"], $contenido);
            Interfaz::asignarToken('est_id', $datos['est_id'], $contenido);
        } else if (isset($_SESSION['est_id']) && $_SESSION['est_id'] != "") {
            $datos['est_id'] = $_SESSION['est_id'];
            $info_est = $_obj_est->obtenerInfoEstablecimiento($datos['est_id']);
            Interfaz::asignarToken("est_nombre", $info_est["est_nombre"], $contenido);
            Interfaz::asignarToken('est_id', $datos['est_id'], $contenido);
        }

        Interfaz::asignarToken("contenido", $this->_contenido, $contenido);
        Interfaz::asignarToken("contenido_externo", $this->_contenido_externo, $contenido);
        Interfaz::asignarToken("contenido_externo_general", $this->_contenido_externo_general, $contenido);
        Interfaz::asignarToken("css", $estilos, $contenido);
        Interfaz::asignarToken("js", $js, $contenido);
        Interfaz::asignarToken("perdido_con_traduccion",$idi_despliegue['enlace_perdido_traduccion'], $contenido);
        Interfaz::asignarToken("soporte",$idi_despliegue['soporte'], $contenido);
        Interfaz::asignarToken("js_footer", $js_footer, $contenido);
        Interfaz::asignarToken("css_footer", $estilos_footer, $contenido);
        Interfaz::asignarToken("path_web", $_PATH_WEB, $contenido);
        Interfaz::asignarToken("path_imagenes", $_PATH_IMAGENES, $contenido);

        //AMP - DATOS PARA PRINCIPAL DESPEDIDA
        if (!is_null($info_est["est_logo"]) && $info_est["est_logo"] != "") {
            $logo = $_PATH_WEB_LOGOS . "/" . $info_est["est_logo"];
        } else {
            $logo = $_PATH_IMAGENES . "/hotels_quality.jpg";
        }
        if (!is_null($info_est["est_pagina_facebook"]) && $info_est["est_pagina_facebook"] != "")
            $url = $info_est["est_pagina_facebook"];


        Interfaz::asignarToken("url", $url, $contenido);
        Interfaz::asignarToken("compartido", $idi_despliegue['label_compartido_facebook_bien'], $contenido);
        Interfaz::asignarToken("no_compartido", $idi_despliegue['label_compartido_facebook_mal'], $contenido);
        Interfaz::asignarToken("logo", $logo, $contenido);
        Interfaz::asignarToken("url", $url, $contenido);
        Interfaz::asignarToken("descripcion", "", $contenido);
        Interfaz::asignarToken("es_preview", "", $contenido);

        return $contenido;
    }

//Fin de crearInterfazGrafica()

    function crearPopUp($datos = array()) {
        global $_PATH_WEB, $_PATH_IMAGENES, $idi_alertas;

        $idioma_alertas = "";
        if (is_array($idi_alertas)) {
            //MAOH - 18 Abr 2012 - Cambio a UTF 8
            $idioma_alertas = "<script languague=\"javascript\" charset=\"utf-8\">\nvar idioma = new Array();\n";
            //$idioma_alertas = "<script languague=\"javascript\">\nvar idioma = new Array();\n";

            foreach ($idi_alertas as $key => $texto) {
                $texto = str_replace("\n", "\\n", $texto);
                $texto = str_replace("\'", "'", $texto);
                $texto = str_replace("'", "\'", $texto);

                $idioma_alertas .= "idioma['" . $key . "'] = '" . $texto . "' ;\n";
            }//Fin de foreach($idi_alertas as $key => $texto)

            $idioma_alertas .= "</script>";
        }//Fin de if( is_array($idi_alertas) )

        $estilos = "";
        $js = $idioma_alertas;


        //DM - 2013-09-24
        $this->_listado_css_externos = array_unique($this->_listado_css_externos);
        foreach ($this->_listado_css_externos as $estilo) {
            //DM - 2013-09-23
            if (is_array($estilo)) {
                $url_estilo = $estilo['url'];
                unset($estilo['url']);
                $propiedades_estilo = '';
                foreach ($estilo as $propiedad => $val_propiedad) {
                    $propiedades_estilo = ' ' . $propiedad . '="' . $val_propiedad . '"';
                }
                $estilos .= "<link href=\"" . $url_estilo . "\" rel=\"stylesheet\" type=\"text/css\" " . $propiedades_estilo . " />\n";
            } else {
                $estilos .= "<link href=\"" . $estilo . "\" rel=\"stylesheet\" type=\"text/css\" />\n";
            }
        }//Fin de foreach($this->_estilos as $estilo)


        $this->_l_css = array_unique($this->_l_css);
        foreach ($this->_l_css as $estilo) {
            //DM - 2013-09-23
            if (is_array($estilo))
            {
                $url_estilo = $estilo['url'];
                unset($estilo['url']);
                $propiedades_estilo = '';
                foreach ($estilo as $propiedad => $val_propiedad) {
                    $propiedades_estilo = ' ' . $propiedad . '="' . $val_propiedad . '"';
                }
                
                //DM - 2017-09-12 issue 10647
                //Obtiene la ultima modificación del fichero para generar la versión
                $src_archivo = $_PATH_SERVIDOR . "Includes/CSS/" . $url_estilo;
                $info_archivo = @stat($src_archivo);
                $version_archivo = $info_archivo['mtime'];
                $estilos .= "<link href=\"" . $_PATH_WEB . "/Includes/CSS/" . $url_estilo . "?v=".$version_archivo."\" rel=\"stylesheet\" type=\"text/css\" " . $propiedades_estilo . " />\n";
            } else
            {
                //DM - 2017-09-12 issue 10647
                //Obtiene la ultima modificación del fichero para generar la versión
                $src_archivo = $_PATH_SERVIDOR . "Includes/CSS/" . $estilo;
                $info_archivo = @stat($src_archivo);
                $version_archivo = $info_archivo['mtime'];
                $estilos .= "<link href=\"" . $_PATH_WEB . "/Includes/CSS/" . $estilo . "?v=".$version_archivo."\" rel=\"stylesheet\" type=\"text/css\" />\n";
            }
        }//Fin de foreach($this->_estilos as $estilo)
        //  $estilos .= "<link href=\"".$_PATH_WEB."/librerias/jquery-ui-1.10.3.custom/css/smoothness/jquery-ui-1.10.3.custom.min.css\" rel=\"stylesheet\" type=\"text/css\" />\n";

        /* amp - 5/8/15 - armar el html de los estilos para el footer */
        $estilos_footer = "";
        foreach ($this->_listado_css_externos_footer as $estilo) {

            $estilos_footer .= "<link href=\"" . $estilo . "\" rel=\"stylesheet\" type=\"text/css\" />\n";
        }//Fin de foreach($this->_estilos as $estilo)


        foreach ($this->_l_css_footer as $estilo) {

            //DM - 2017-09-12 issue 10647
            //Obtiene la ultima modificación del fichero para generar la versión
            $src_archivo = $_PATH_SERVIDOR . "Includes/CSS/" . $estilo;
            $info_archivo = @stat($src_archivo);
            $version_archivo = $info_archivo['mtime'];
            $estilos_footer .= "<link href=\"" . $_PATH_WEB . "/Includes/CSS/" . $estilo . "?v=".$version_archivo."\" rel=\"stylesheet\" type=\"text/css\" />\n";
        }//Fin de foreach($this->_estilos as $estilo)
        //si hay contenido para agregar lo hace
        if ($contenido_estilos != '') {
            $propiedades_estilo = '';
            foreach ($propiedades_contenido_estilos as $propiedad => $val_propiedad) {
                $propiedades_estilo = ' ' . $propiedad . '="' . $val_propiedad . '" ';
            }

            $estilos .= '<style type="text/css" ' . $propiedades_estilo . '>' . $contenido_estilos . '</style>' . "\n";
        }//Fin de if( $contenido_estilos != '')

        $js = $idioma_alertas;

        $this->_l_js = array_unique($this->_l_js);
        foreach ($this->_l_js as $script) {
            //DM - 2017-09-12 issue 10647
            //Obtiene la ultima modificación del fichero para generar la versión
            $src_archivo = $_PATH_SERVIDOR . "Includes/JS/" . $script;
            $info_archivo = @stat($src_archivo);
            $version_archivo = $info_archivo['mtime'];
            $js .= "<script src=\"" . $_PATH_WEB . "/Includes/JS/" . $script . "?v=".$version_archivo."\" language=\"Javascript\" type=\"text/javascript\" charset=\"utf-8\"></script>\n";
            //$js .= "<script src=\"".$_PATH_WEB."/Includes/JS/".$script."\" language=\"Javascript\" type=\"text/javascript\"></script>\n";
        }//Fin de foreach($this->_js as $script)
        //DM  - 2013-09-12 issue 2971
        $this->_listado_js_externos = array_unique($this->_listado_js_externos);
        foreach ($this->_listado_js_externos as $script) {
            $js .= "<script src=\"" . $script . "\" language=\"Javascript\" type=\"text/javascript\" charset=\"utf-8\"></script>\n";
        }//Fin de foreach($this->_listado_js_externos as $script)
        //    $js .= "<script src=\"".$_PATH_WEB."/librerias/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js\" language=\"Javascript\" type=\"text/javascript\" charset=\"utf-8\"></script>\n";
        // amp - 5/8/15 - agrego array para mostrar links en el footer
        $js_footer = "";
        foreach ($this->_listado_js_externos_footer as $script) {
            $js_footer .= "<script src=\"" . $script . "\" language=\"Javascript\" type=\"text/javascript\" charset=\"utf-8\"></script>\n";
        }//Fin de foreach($this->_listado_js_externos as $script)
        foreach ($this->_l_js_footer as $script) {
            //DM - 2017-09-12 issue 10647
            //Obtiene la ultima modificación del fichero para generar la versión
            $src_archivo = $_PATH_SERVIDOR . "Includes/JS/" . $script;
            $info_archivo = @stat($src_archivo);
            $version_archivo = $info_archivo['mtime'];
            $js_footer .= "<script src=\"" . $_PATH_WEB . "/Includes/JS/" . $script . "?v=".$version_archivo."\" language=\"Javascript\" type=\"text/javascript\" charset=\"utf-8\"></script>\n";
            //$js .= "<script src=\"".$_PATH_WEB."/Includes/JS/".$script."\" language=\"Javascript\" type=\"text/javascript\"></script>\n";
        }//Fin de foreach($this->_js_foter as $script)

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/popup.html");
        if ($datos['plantilla_html'] != '') {
            $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/" . $datos['plantilla_html']);
        }

        //MAOH - 18 Abr 2012 - Cambio a UTF 8
        Interfaz::asignarToken("codificacion", "UTF-8", $contenido);

        //Si el idioma seleccionado es Chino(13), Ruso(14) o Ucraniano(15) para que reconozca caracteres extraños
        /* if (($_SESSION['idi_id'] == 10) || ($_SESSION['idi_id'] == 13) || ($_SESSION['idi_id'] == 14) || ($_SESSION['idi_id'] == 15) || ($_SESSION['idi_id'] == 9) || ($_SESSION['idi_id'] == 26))
          {
          Interfaz::asignarToken("codificacion",'UTF-8',$contenido);
          }
          else //Los otros idiomas que no tienen caracteres extraños
          {
          Interfaz::asignarToken("codificacion",'iso-8859-1',$contenido);
          } */

        Interfaz::asignarToken("propiedades", $this->_propiedades, $contenido);

        Interfaz::asignarToken("imagenes", $_PATH_IMAGENES, $contenido);

        Interfaz::asignarToken("contenido", $this->_contenido, $contenido);

        Interfaz::asignarToken("css", $estilos, $contenido);
        Interfaz::asignarToken("js", $js, $contenido);
        Interfaz::asignarToken("js_footer", $js_footer, $contenido);
        Interfaz::asignarToken("css_footer", $estilos_footer, $contenido);

        Interfaz::asignarToken("path_web", $_PATH_WEB, $contenido);


        if (is_array($this->_campos)) {
            foreach ($this->_campos as $clave => $valor) {
                Interfaz::asignarToken($clave, $valor, $contenido);
            }
        }//Fin de if( is_array($this->_campos) )

        Interfaz::asignarToken("metas", "", $contenido);

        return $contenido;
    }

//Fin de crearPopUp()

    /**
     * Se encarga de crear un listado de registros de acuerdos a los parámetros
     * que se pasan a la función.
     * DM - 2016-04-22
     **/
    static function crearListado($arreglo_datos)
    {
        global $_PATH_WEB, $idi_despliegue, $_PATH_IMAGENES;

        $contenido_inicio = "";

        //Si el istado tiene un titulo
        if ($arreglo_datos['TITULO'] != "") {
            $contenido_inicio .= "<div class='listado_titulo'><p>" . $arreglo_datos['TITULO'] . "</p></div>";
        }//Fin de if($arreglo_datos['TITULO'] != "")

        $contenido_inicio .= $arreglo_datos['MENSAJE'];

        $borde = "";
        if ($arreglo_datos["DATOS"]["exportar"] != "") {
            $borde = 'border="1"';
        }
        $contenido = "";
        //JP - 2014-09-16
        //Se le agrega la opcion de dejar el encabezado por
        //aparte de la tabla de datos
        $fijo = '';
        if ($arreglo_datos['ENCABEZADO_FIJO'] != '') {
            $fijo = 'class="head-fijo"';
            $arreglo_datos['ALTO_TABLA'] = $arreglo_datos['ALTO_TABLA'] == '' ? 500 : $arreglo_datos['ALTO_TABLA'];
            $contenido .= "<div class='container-table ' style='  max-height:" . $arreglo_datos['ALTO_TABLA'] . "px;'>";
        }//Fin de if($arreglo_datos['ENCABEZADO_FIJO'] != '')
        //DM - 2014-10-01
        //si hay un parámetro para los encabezados entonces lo asigna, de lo contrario lo inicializa
        $arreglo_datos['CAMPOS_OPCIONES_ENCABEZADO'] = is_array($arreglo_datos['CAMPOS_OPCIONES_ENCABEZADO']) ? $arreglo_datos['CAMPOS_OPCIONES_ENCABEZADO'] : array();
        //si no se va a exportar coloca la paginacion
        if ($arreglo_datos['DATOS']['exportar'] == "") {
            $paginacion = Interfaz::crearPaginacion($arreglo_datos);
//            if ($arreglo_datos['VOLVER'] == true) {
//                if ($arreglo_datos['ACCION_VOLVER'] != "")
//                    $contenido .= "<a href=\"index.php?accion=" . $arreglo_datos['ACCION_VOLVER'] . "\">Volver a Gesti&oacute;n</a>";
//            }

            $footer_pag = (is_array($paginacion)) ? $paginacion['footer'] : $paginacion;
            $top_pag = (is_array($paginacion)) ? $paginacion['top'] : "";
        }//Fin de if( $arreglo_datos[DATOS][exportar] == "")
        
        $contenido .= $top_pag;
        
        //JMM-2017-08-29 - Desarrollo #10468
        if ($arreglo_datos['LISTADO']['tabla_class_css'] != "")
        {
        	$contenido .= "<table class='footable table table-stripped crear-listado ".$arreglo_datos['LISTADO']['tabla_class_css']."' ng-controller='modalDemoCtrl' data-sort='false' data-page-size='100' syle='overflow-x: scroll;'>";
        }
        else {
        	$contenido .= "<table class='footable table table-stripped crear-listado' ng-controller='modalDemoCtrl' data-sort='false' data-page-size='100' syle='overflow-x: scroll;'>";
        }
        
        if ($arreglo_datos['cabecera'] != "") {
            $contenido .= $arreglo_datos['cabecera'];
        }

        $arreglo_datos['CAMPOS_ORDENA'] = is_array($arreglo_datos['CAMPOS_ORDENA']) ? $arreglo_datos['CAMPOS_ORDENA'] : array();

        if (count($arreglo_datos['CAMPOS']) > 0) {

            $contenido .= "<thead><tr>";

            if (isset($arreglo_datos['OPCIONES']['check'])) {
                if ($arreglo_datos['OPCIONES']['check_encabezado'] != '') {
                    $contenido .= "<th  >" . $arreglo_datos['OPCIONES']['check_encabezado'] . "</th>";
                } else {
                    $contenido .= "<th  >&nbsp;</th>";
                }
            }

            // coloca las etiquetas de las columnas en la tabla
            if (!$arreglo_datos['listado_sin_numeracion']) {
                $contenido .= "<th >No.</th>";
            }


            foreach ($arreglo_datos['CAMPOS'] as $campo => $etiqueta) {
                if ($etiqueta != "") {
                    if (in_array($campo, $arreglo_datos['CAMPOS_ORDENA'])) {
                        $ordena = $arreglo_datos['DATOS']['ordena'];
                        $tipo_ordena = $arreglo_datos['DATOS']['tipo_ordena'];
                        if (isset($arreglo_datos['TIPO_ORDENA'][$campo])) {
                            $ordena = "";
                            $tipo_ordena = $arreglo_datos['TIPO_ORDENA'][$campo];
                        }

                        $adicional = "";
                        $tipo_ordena_base = "";
                        switch ($ordena . $tipo_ordena) {
                            case $campo:
                            case $campo . "ASC":
                                $adicional = ' <i class="fa fa-sort-desc"></i>';
                                $tipo_ordena_base = "DESC";
                                break;

                            case $campo . "DESC":
                                $adicional = ' <i class="fa fa-sort-asc"></i>';
                                $tipo_ordena_base = "ASC";
                                break;
                            case "ASC-1":
                                $adicional = ' <i class="fa fa-sort-amount-desc"></i>';
                                $tipo_ordena_base = "DESC";
                                break;

                            case "DESC-1":
                                $adicional = ' <i class="fa fa-sort-amount-asc"></i>';
                                $tipo_ordena_base = "ASC";
                                break;
                            default:
                                $adicional = ' <i class="fa fa-sort"></i>';
                                $tipo_ordena_base = "";
                                break;
                        }//Fin de switch($campo)


                        $tooltip = (isset($arreglo_datos['TOOLTIP'][$campo])) ? "<i class='fa fa-info-circle' data-toggle='popover' data-trigger='hover' data-placement='top' data-content='" . $arreglo_datos['TOOLTIP'][$campo] . "'></i>" : "";

                        $enlace = "index.php?ordena=" . $campo . "&tipo_ordena=" . $tipo_ordena_base . "&offset=" . $arreglo_datos['DATOS']['offset'] . "&accion=" . $arreglo_datos['ACCION_LISTAR'];

                        $contenido_campo_adicional = '';
                        if (isset($arreglo_datos['CAMPOS_ADICIONALES'][$campo])) {
                            $contenido_campo_adicional = $arreglo_datos['CAMPOS_ADICIONALES'][$campo];
                        }

                        if ($contenido_campo_adicional != '') {
                            $etiqueta = "<span id='col_" . $campo . "'>" . $etiqueta . $contenido_campo_adicional . "</span> <a class='enlace' onclick=\"opcionesTablaAjax(" . $enlace . ")\"> " . $adicional . "</a> $tooltip";
                        } else {
                            $etiqueta = "<a  onclick=\"opcionesTablaAjax('" . $enlace . "')\">
                                        <span id='col_" . $campo . "'>" . $etiqueta . $contenido_campo_adicional . "</span> " . $adicional . "</a> $tooltip";
                        }
                    } else {
                        $contenido_campo_adicional = '';
                        if (isset($arreglo_datos['CAMPOS_ADICIONALES'][$campo])) {
                            $contenido_campo_adicional = $arreglo_datos['CAMPOS_ADICIONALES'][$campo];
                        }
                        $tooltip = (isset($arreglo_datos['TOOLTIP'][$campo])) ? "<i class='fa fa-info-circle' data-toggle='popover' data-trigger='hover' data-placement='top' data-content='" . $arreglo_datos['TOOLTIP'][$campo] . "'></i>" : "";
                        $etiqueta .= $contenido_campo_adicional . " $tooltip";
                    }

                    //DM - 2014-10-01
                    //Si se pasa un parámetro para el encabezado lo utiliza
                    $atributos_celda = '';
                    $align_celda = 'align="center"';
                    $class_celda = '';
                    if (isset($arreglo_datos['CAMPOS_OPCIONES_ENCABEZADO'][$campo])) {
                        $atributos_celda = $arreglo_datos['CAMPOS_OPCIONES_ENCABEZADO'][$campo];

                        //si se pasa una alneación como parámetro entonces no agrega la que tiene por defecto
                        if (strpos($atributos_celda, 'align') !== false) {
                            $align_celda = '';
                        }//Fin de if( strpos($atributos_celda,'align') !== false )
                        //si se pasa una clase css como parámetro entonces no agrega la que tiene por defecto
                        if (strpos($atributos_celda, 'class') !== false) {
                            $class_celda = '';
                        }//Fin de if( strpos($atributos_celda,'align') !== false )
                    }//Fin de if( isset($arreglo_datos['CAMPOS_OPCIONES_ENCABEZADO'][ $campos ]) )

                    $contenido .= "<th $atributos_celda>$etiqueta</th>";
                }
            }//Fin de foreach($arreglo_datos['CAMPOS'] as $campo => $etiqueta)

            if (isset($arreglo_datos['OPCIONES']['seleccionar'])) {
                $contenido .= "<th>&nbsp;</th>";
            }

            if (isset($arreglo_datos['OPCIONES']['registro'])) {
                $contenido .= "<th  >Registro</th>";
            }

            if (isset($arreglo_datos['OPCIONES']['delegar'])) {
                $contenido .= "<th  >Delegar</th>";
            }

            if (isset($arreglo_datos['OPCIONES']['opciones'])) {
                if (is_array($arreglo_datos['OPCIONES']['opciones'])) {
                    $contenido .= "<th>" . $arreglo_datos['OPCIONES']['opciones']['nombre_columna'] . "</th>";
                } else {
                    $contenido .= "<th>Opciones</th>";
                }
            }

            if (isset($arreglo_datos['OPCIONES']['opcion_adicional'])) {
                $contenido .= "<th  >&nbsp;</th>";
            }

            if (isset($arreglo_datos['OPCIONES']['ver'])) {
                $label = 'Historial';
                if ($arreglo_datos['OPCIONES']['ver_titulo'] != '') {
                    $label = $arreglo_datos['OPCIONES']['ver_titulo'];
                }
                $contenido .= "<th >" . $label . "</th>";
            }
            if (isset($arreglo_datos['OPCIONES']['enviar'])) {
                $contenido .= "<th >Enviar</th>";
            }
            if (isset($arreglo_datos['OPCIONES']['actualizar'])) {   //DMG 2014-10-22 Se agrega el texto de modificar por idiomas
                $modificar = $idi_despliegue['modificar'];
                $contenido .= "<th >" . $modificar . "</th>";
            }
            if (isset($arreglo_datos['OPCIONES']['eliminar']) || isset($arreglo_datos['OPCIONES']['eliminar_ajax'])) {
                //DMG 2014-10-22 Se agrega el texto de modificar por idiomas
                $eliminar = $idi_despliegue['eliminar'];
                $eliminar_todos = (isset($arreglo_datos['OPCIONES']['eliminar_todo'])) ? $arreglo_datos['OPCIONES']['eliminar_todo'] : "";
                $contenido .= "<th >$eliminar <br> $eliminar_todos</th>";
            }
            if (isset($arreglo_datos['OPCIONES']['descargar_informe'])) {
                //DMG 2014-10-22 Se agrega el texto de modificar por idiomas
                $label_descargar = $arreglo_datos['OPCIONES']['label_descargar'];
                $contenido .= "<th >$label_descargar</th>";
            }

            $contenido .= "</tr></thead>";

            if ($arreglo_datos['cabecera_datos'] != "") {
                $contenido .= $arreglo_datos['cabecera_datos'];
            }
            $contenido .= "<tbody>";

            if ($arreglo_datos['ENCABEZADO_FIJO'] != '') {
                $contenido .= "<tbody>
                    <tr><td>&nbsp;</td></tr>";
            }

            //Si existen registros en el listado
            if (is_array($arreglo_datos['LISTADO']) && sizeof($arreglo_datos['LISTADO']) > 4) {

                $num_registro = $arreglo_datos['LISTADO']['offset'];

                //El tama�o del arreglo menos los campos del limit, offset, total registros y usa_filtro
                $size = sizeof($arreglo_datos['LISTADO']) - 4;

                // para cada resultado obtenido
                for ($i = 0; $i < $size; $i++) {

                    $obj_objeto = $arreglo_datos['LISTADO'][$i];


                    //DMG 2015-03-13 $estilo_fila = ($estilo_fila=="listado_fila_2"?"listado_fila_blanca":"listado_fila_2");
                    //  $estilo_fila = ($estilo_fila == "listado_fila_new_1" ? "listado_fila_blanca" : "listado_fila_new_1");

                    $funcion = "";
                    if ($arreglo_datos["check_color"]) {
                        $funcion = "onclick=\"clickFila(this,'" . $estilo_fila . "')\"";
                    }
                    $contenido .= "<tr " . $funcion . " class='$estilo_fila<!--estilo_rojo-->'  >";


                    $estilo_rojo = "";
                    // obtiene los atributos del objeto
                    $atributos = array_keys($arreglo_datos['CAMPOS']);


                    //posicion de los campos en el listado
                    $pos_campo = 0;

                    if (isset($arreglo_datos['OPCIONES']['check'])) {
                        $est_id = $_SESSION['est_id'];

                        $checked = "";
                        if (is_numeric($obj_objeto['esb_id'])) {
                            $checked = "checked";
                        }
                        if (is_numeric($obj_objeto['esb_checked'])) {
                            $checked = "checked";
                        }

                        //deshabilita el registro
                        $habilitado = 1;
                        if ($obj_objeto['esb_cs_checked'] == "NO_CS") {
                            $checked = "";
                            $habilitado = 0;
                            $estilo_rojo = "_rojo";
                        }

                        if ($obj_objeto['esb_cs_checked'] == "LIMITE") {
                            $checked = "";
                            $habilitado = 0;
                            $estilo_rojo = "_amarillo";
                        }

                        if ($obj_objeto['est_fila_rojo'] == 1) {
                            $estilo_rojo = "_rojo";
                        }


                        $estilo_valor = '';
                        if ($pos_campo == 0) {
                            $estilo_valor = '';
                        }
                        $habilitado = (isset($arreglo_datos['LISTADO'][$i]['check_estado']) && $arreglo_datos['LISTADO'][$i]['check_estado'] == 0 ) ? 0 : 1;
                        $valor_check = $arreglo_datos['OPCIONES']['check'];
                        if ($arreglo_datos['OPCIONES']['check_habilitado'] == 1 && $habilitado == 1) {
                            if ($arreglo_datos['OPCIONES']['check_nombre'] != '') {
                                $atributos_check = $arreglo_datos['OPCIONES']['check_atributos'];


                                for ($k = 0; $k < count($atributos); $k++) {
                                    eval('$valor = $obj_objeto[' . $atributos[$k] . '];');
                                    $valor_check = str_replace("%" . $atributos[$k] . "%", $valor, $valor_check);

                                    $atributos_check = str_replace("%" . $atributos[$k] . "%", $valor, $atributos_check);
                                }//Fin de for($i=0; $i<count($atributos); $i++)

                                $checked = $obj_objeto['checked'] == 1 ? 'ng-init="check_destinatarios_' . $valor_check . '=true"' : '';

                                $contenido .= "
        						<td align=\"center\" class='" . $estilo_valor . "' valign='middle' id='div_check_" . $valor_check . "'>
        							<input " . $checked . " " . $atributos_check . " type='checkbox' icheck ng-model='check_destinatarios_" . $valor_check . "'  name='" . $arreglo_datos['OPCIONES']['check_nombre'] . "' value='" . $valor_check . "'>
        						</td>";
                            } else if ($arreglo_datos['OPCIONES']['check_funcion'] == 1) {
                                if (isset($_SESSION["ids_boletin"]) && is_array($_SESSION["ids_boletin"])) {
                                    if ($_SESSION["ids_boletin"][$obj_objeto['est_id']] > 0) {
                                        $checked = "checked";
                                    }
                                }
                                $contenido .= "
        						<td align=\"center\" class='" . $estilo_valor . "' valign='middle' id='div_" . $obj_objeto['est_id'] . "' >
        							<input " . $checked . " type='checkbox' name='est_id[]' icheck ng-model='check_destinatarios_" . $valor_check . "' value='" . $obj_objeto['est_id'] . "'>
        						</td>";
                            }//Fin de if( $arreglo_datos['OPCIONES']['check_funcion'] == 1 )
                            else {
                                $contenido .= "
        						<td align=\"center\"  class='" . $estilo_valor . "' valign='middle' id='div_" . $obj_objeto['est_id'] . "'>
        							<input " . $checked . " type='checkbox' name='est_id[]' value='" . $obj_objeto['est_id'] . "'>
        						</td>";
                            }
                        } else {
                            $contenido .= "
    						<td align=\"center\" class='" . $estilo_valor . "'  valign='middle' id='div_" . $obj_objeto['est_id'] . "'>
    							<input " . $checked . " icheck ng-model='check_destinatarios_dis" . $i . "'  type='checkbox' disabled>
    						</td>";
                        }

                        $pos_campo++;
                    }//Fin de if( isset($opciones['delegar']) )

                    $contenido = str_replace("<!--estilo_rojo-->", $estilo_rojo, $contenido);


                    if (!$arreglo_datos['listado_sin_numeracion']) {
                        $estilo_valor = '';
                        if ($pos_campo == 0) {
                            $estilo_valor = '';
                        }
                        $contenido .= "<td align='center' class='" . $estilo_valor . "'>" . ($num_registro + $i + 1) . "</td>";
                        $pos_campo++;
                    }

                    $arreglo_datos['CAMPOS_SIN_LIMITE_TEXTO'] = is_array($arreglo_datos['CAMPOS_SIN_LIMITE_TEXTO']) ? $arreglo_datos['CAMPOS_SIN_LIMITE_TEXTO'] : array();

                    //Coloca los valores de cada objeto
                    foreach ($arreglo_datos['CAMPOS'] as $campo => $etiqueta) {
                        if ($etiqueta != "") {
                            $valor = $obj_objeto[$campo];

                            //Si el texto es mayor a 58 caracteres entonces lo corta
                            if (strlen($valor) > 58 && $campo != "est_des_mes_anterior" && !in_array($campo, $arreglo_datos['CAMPOS_SIN_LIMITE_TEXTO'])
                            ) {
                                $valor = substr($valor, 0, 58);

                                $pos_ultimo_espacio = strrpos($valor, ' ');
                                if ($pos_ultimo_espacio !== false) {
                                    $valor = substr($valor, 0, $pos_ultimo_espacio);
                                }

                                $valor .= "...";
                            }

                            //DM - 2014-09-15
                            //Agrega al valor los datos de la fila en caso de que los necesite
                            for ($k = 0; $k < count($atributos); $k++) {
                                eval('$valor_atributo = $obj_objeto[' . $atributos[$k] . '];');
                                $valor = str_replace("%" . $atributos[$k] . "%", $valor_atributo, $valor);
                            }//Fin de for($i=0; $i<count($atributos); $i++)

                            $estilo_valor = '';
                            //DMG 2015-03-13 $estilo_valor = 'listado_valor';

                            if ($pos_campo == 0) {
                                $estilo_valor = '';
                                //$estilo_valor = 'listado_valor_inicial';
                            }
                            //JMM-2017-08-29 - Desarrollo #10468
                            $alineacion = (is_numeric($valor)) ? "center" : "left";
                            $contenido .= "<td style='text-align:$alineacion;' class='" . $estilo_valor . "' " . $arreglo_datos['CAMPOS_OPCIONES'][$campo] . " >" . $valor . "</td>";

                            $pos_campo++;
                        }//Fin de if($etiqueta != "")
                    }//Fin de foreach()

                    if (isset($arreglo_datos['OPCIONES']['seleccionar'])) {
                        $enlace = $arreglo_datos['OPCIONES']['seleccionar'];
                        for ($k = 0; $k < count($atributos); $k++) {
                            eval('$valor = $obj_objeto[' . $atributos[$k] . '];');
                            $valor = str_replace('\'', '\\\'', $valor);
                            $enlace = str_replace("%" . $atributos[$k] . "%", $valor, $enlace);
                        }//Fin de for($i=0; $i<count($atributos); $i++)
                        $contenido .= "
						<td align=\"center\" valign='middle' class='listado_valor'>
							<a href=\"#\" onclick=\"" . $enlace . "\" >Seleccionar</a>
						</td>";
                    }//Fin de if( isset($opciones['delegar']) )

                    if (isset($arreglo_datos['OPCIONES']['registro'])) {
                        $enlace = $arreglo_datos['OPCIONES']['registro'];
                        for ($k = 0; $k < count($atributos); $k++) {
                            eval('$valor = $obj_objeto[' . $atributos[$k] . '];');
                            $enlace = str_replace("%" . $atributos[$k] . "%", $valor, $enlace);
                        }//Fin de for($i=0; $i<count($atributos); $i++)
                        $contenido .= "
						<td align=\"center\"  valign='middle' class='listado_valor'>
							<a href=\"index.php?accion=" . $arreglo_datos['ACCION_REGISTRO'] . "&$enlace\" >
								<img src='" . $_PATH_WEB . "/Includes/Imagenes/registro.png' border=0 width=20 height=20 title='Registro de Asistencia'>
							</a>
						</td>";
                    }//Fin de if( isset($opciones['delegar']) )


                    if (isset($arreglo_datos['OPCIONES']['delegar'])) {
                        $enlace = $arreglo_datos['OPCIONES']['delegar'];
                        for ($k = 0; $k < count($atributos); $k++) {
                            eval('$valor = $obj_objeto[' . $atributos[$k] . '];');
                            $enlace = str_replace("%" . $atributos[$k] . "%", $valor, $enlace);
                        }//Fin de for($i=0; $i<count($atributos); $i++)
                        $contenido .= "
						<td align=\"center\"  valign='middle' class='listado_valor'>
							<a href=\"index.php?accion=" . $arreglo_datos['ACCION_DELEGAR'] . "&$enlace\" >
								<img src='" . $_PATH_WEB . "/Includes/Imagenes/delegar.png' border=0 width=20 height=20 title='Delegar acciones'>
							</a>
						</td>";
                    }//Fin de if( isset($opciones['delegar']) )


                    if (isset($arreglo_datos['OPCIONES']['opciones'])) {
                        $enlace = $arreglo_datos['OPCIONES']['opciones'];

                        $imagen = "<img src='" . $_PATH_WEB . "/Includes/Imagenes/eliminar.png' border=0 width=20 height=20 title='Registro de Opciones'>";

                        //Si existe un arreglo es porque tiene personalización
                        //de valores
                        if (is_array($enlace)) {
                            $imagen = $enlace['html_imagen'];
                            $enlace = $enlace['enlace'];
                        }//Fin de if( is_array($enlace) )

                        for ($k = 0; $k < count($atributos); $k++) {
                            eval('$valor = $obj_objeto[' . $atributos[$k] . '];');
                            $enlace = str_replace("%" . $atributos[$k] . "%", $valor, $enlace);
                        }//Fin de for($i=0; $i<count($atributos); $i++)

                        $contenido .= "
						<td align=\"center\" valign='middle'>
							<a href=\"index.php?accion=" . $arreglo_datos['ACCION_OPCIONES'] . "&$enlace\" >
							" . $imagen . "
							</a>
						</td>";
                    }//Fin de if( isset($opciones['delegar']) )
                    //DM - 2014-06-16
                    //Agrega la opción para campos adicionlaes
                    if (isset($arreglo_datos['OPCIONES']['opcion_adicional'])) {
                        $enlace = $arreglo_datos['OPCIONES']['opcion_adicional'];

                        $enlace_atributos = $arreglo_datos['ATRIBUTOS_OPCION_ADICIONAL'];

                        $enlace_label = $arreglo_datos['LABEL_OPCION_ADICIONAL'];
                        $enlace_label_extra = $arreglo_datos['LABEL_OPCION_ADICIONAL_EXTRA'];

                        for ($j = 0; $j < count($atributos); $j++) {

                            eval('$valor = $obj_objeto[' . $atributos[$j] . '];');
                            $enlace = str_replace("%" . $atributos[$j] . "%", $valor, $enlace);

                            $enlace_atributos = str_replace("%" . $atributos[$j] . "%", $valor, $enlace_atributos);

                            $enlace_label = str_replace("%" . $atributos[$j] . "%", $valor, $enlace_label);
                            $enlace_label_extra = str_replace("%" . $atributos[$j] . "%", $valor, $enlace_label_extra);
                        }//Fin de for($i=0; $i<count($atributos); $i++)

                        $enlace = $enlace == '' ? '' : '&' . $enlace;

                        $opcion_enlace = "<a  " . $enlace_atributos . " class=\"enlace_accion\" onclick=\"opcionesTablaAjax('" . $arreglo_datos['ACCION_OPCION_ADICIONAL'] . $enlace . "')\">
        							     " . $enlace_label . "
        						      </a>
                                      " . $enlace_label_extra;

                        //Si hay un texto que se sebe agregar en el enlace adicional lo agrega
                        $opcion_adicional = $obj_objeto['opcion_adicional'];

                        if ($opcion_adicional != '') {
                            $opcion_enlace = $opcion_adicional;
                        }

                        $contenido .= "<td align=\"center\"  >" . $opcion_enlace . "</td>";
                    }//Fin de if( isset($opciones['ver']) )
                    // para colocar el enlace de ver, actualizar y  eliminar
                    if (isset($arreglo_datos['OPCIONES']['ver'])) {
                        $enlace = $arreglo_datos['OPCIONES']['ver'];
                        for ($j = 0; $j < count($atributos); $j++) {
                            eval('$valor = $obj_objeto[' . $atributos[$j] . '];');
                            $enlace = str_replace("%" . $atributos[$j] . "%", $valor, $enlace);
                        }//Fin de for($i=0; $i<count($atributos); $i++)
                        $contenido .= "
					<td align=\"center\" >
						<a  " . $arreglo_datos['ATRIBUTOS_VER'] . " class=\"enlace_accion\" onclick=\"opcionesTablaAjax('index.php?accion=" . $arreglo_datos['ACCION_VER'] . "&$enlace')\">
							Ver
						</a>
					</td>";
                    }//Fin de if( isset($opciones['ver']) )


                    if (isset($arreglo_datos['OPCIONES']['actualizar'])) {
                        $enlace = $arreglo_datos['OPCIONES']['actualizar'];

                        for ($j = 0; $j < count($atributos); $j++) {
                            eval('$valor = $obj_objeto[' . $atributos[$j] . '];');
                            $enlace = str_replace("%" . $atributos[$j] . "%", $valor, $enlace);
                        }//Fin de for($i=0; $i<count($atributos); $i++)

                        if (isset($arreglo_datos['OPCIONES']['copiar'])) {
                            $enlace = $arreglo_datos['OPCIONES']['copiar'];
                            for ($j = 0; $j < count($atributos); $j++) {
                                eval('$valor = $obj_objeto[' . $atributos[$j] . '];');
                                $enlace = str_replace("%" . $atributos[$j] . "%", $valor, $enlace);
                            }//Fin de for($i=0; $i<count($atributos); $i++)

                            $contenido .= "<td align=\"center\" >
                                                    <a href=\"index.php?accion=" . $arreglo_datos['ACCION_COPIAR'] . "&$enlace\">
                                                            <i class='fa fa-copy' ></i>
                                                    </a>
                                            </td>";
                        }//Fin de if( isset($opciones['copiar']) )

                        if ($arreglo_datos['URL_MODIFICAR_COMPLETA']) {
                            $contenido .= "
							<td align=\"center\" >
								<a href=\"" . $arreglo_datos['ACCION_ACTUALIZAR'] . "&$enlace\" >
									<i class='fa fa-edit' ></i>
								</a>
							</td>";
                        } else {
                            $contenido .= "
							<td align=\"center\" height='38' valign='middle' >
								<a href=\"index.php?accion=" . $arreglo_datos['ACCION_ACTUALIZAR'] . "&$enlace\" >
									<i class='fa fa-edit' ></i>
								</a>
							</td>";
                        }
                    }//Fin de if( isset($opciones['actualizar']) )


                    if (isset($arreglo_datos['OPCIONES']['eliminar'])) {
                        $enlace = $arreglo_datos['OPCIONES']['eliminar'];
                        for ($k = 0; $k < count($atributos); $k++) {
                            eval('$valor = $obj_objeto[' . $atributos[$k] . '];');
                            $enlace = str_replace("%" . $atributos[$k] . "%", $valor, $enlace);
                        }//Fin de for($i=0; $i<count($atributos); $i++)

                        $contenido .= " <td align=\"center\" height='38' valign='middle' >
                                            <a href=\"index.php?accion=" . $arreglo_datos['ACCION_ELIMINAR'] . "&$enlace\" onClick=\"return confirm(idioma['mensaje_confirmar_eliminar'])\">
                                                <i class='fa fa-trash-o'></i>
                                            </a>
					                            </td>";
                    }//Fin de if( isset($opciones['eliminar']) )

                    if (isset($arreglo_datos['OPCIONES']['eliminar_ajax'])) {
                        $accion_click = $arreglo_datos['OPCIONES']['eliminar_ajax'];
                        for ($k = 0; $k < count($atributos); $k++) {
                            eval('$valor = $obj_objeto[' . $atributos[$k] . '];');
                            $accion_click = str_replace("%" . $atributos[$k] . "%", $valor, $accion_click);
                        }//Fin de for($i=0; $i<count($atributos); $i++)

                        $contenido .= "<td align=\"center\" height='38' valign='middle' >
                            					     <a onClick=\"return " . $accion_click . "\">
                                           <i class='fa fa-trash-o' ></i>
                            					    </a>
                            					</td>";
                    }//Fin de if( isset($opciones['eliminar']) )
                    if (isset($arreglo_datos['OPCIONES']['descargar_informe'])) {
                        $btn_descargar = $arreglo_datos['OPCIONES']['descargar_informe'];
                        for ($k = 0; $k < count($atributos); $k++) {
                            eval('$valor = $obj_objeto[' . $atributos[$k] . '];');
                            $btn_descargar = str_replace("%" . $atributos[$k] . "%", $valor, $btn_descargar);
                        }//Fin de for($i=0; $i<count($atributos); $i++)
                        $contenido .= "<td align=\"center\"  valign='middle'>
                            								  <a href=''>
                                                <i class='fa fa-download' onclick=" . $btn_descargar . "></i>
                                              </a>
                            						</td>";
                    }//Fin de if( isset($opciones['delegar']) )

                    $contenido .= "</tr>";
                }//Fin de for($i = 0; $i < $arreglo_datos['LISTADO']['limite']; $i++)
                $contenido .= "</tbody>";
            }//Fin de if( is_array($arreglo_datos['LISTADO']) )
            else {
                if ($arreglo_datos['LISTADO']['usa_filtro'] == 1) {
                    $contenido .= "
					<tr>
						<td colspan=" . (count($arreglo_datos['CAMPOS']) + 2) . " align=\"center\">
							<br><h3 style='padding:10px;'>" . $idi_despliegue['mensaje_listado_sin_resultados_con_filtro'] . "</h3><br>
						</td>
					</tr></tbody>";
                } else {

                    $mensaje_sin_datos = $idi_despliegue['no_registros'];
                    if ($arreglo_datos['DATOS']['mensaje_no_registros'] != '') {
                        $mensaje_sin_datos = $arreglo_datos['DATOS']['mensaje_no_registros'];
                    }
                    $contenido .= "
					<tr>
						<td colspan=" . (count($arreglo_datos['CAMPOS']) + 2) . " align=\"center\">
							<br><h3>" . $mensaje_sin_datos . "</h3><br>
						</td>
					</tr></tbody>";
                }
            }
        }//Fin de if( count($campos)>0 )

        $contenido .= "</table>";
        if ($arreglo_datos['ENCABEZADO_FIJO'] != '') {
            $contenido .= "</div>";
        }


        $contenido = $contenido_inicio . '<div class="tabla_destinatarios">' . $contenido . '</div>';


        if ($arreglo_datos['pie'] != "") {
            $contenido .= $arreglo_datos['pie'];
        }

        $contenido .= $footer_pag;



        return $contenido;
    }

//Fin crearListado()

    /*
     * Utilidad: Crear Listado para e´vío de reportes por email
     * Entrada arreglo con configuracion de tabla, cabeceras, celdas y adicionales
     * Autor: AMP
     * Fecha: 18/4/16
     *
     */

    public function crearListadoTablasCorreos($arreglo_datos) {
        global $_PATH_WEB, $idi_despliegue, $_PATH_IMAGENES, $_PATH_SERVIDOR;

        $contenido = "";
        $contenido .= '<table border="0" class="' . $arreglo_datos['TABLA']['class'] . '" cellspacing="0" cellpadding="0" ' . $arreglo_datos['TABLA']['atributos'] . ' >';
        $header = "<thead><tr>";

        foreach ($arreglo_datos['CABECERAS_TABLA'] as $cabecera) {
            $header .= '<th class="' . $cabecera['class'] . '" '
                    . $cabecera['atributos'] . ">"
                    . $cabecera['titulo']
                    . "</th>";
        }
        $header .= "</tr></thead>";
        // enlaces seguros
        $datos_enlace = array();
            $est['opciones'] = array();
            $est['opciones']['tipo_consulta'] = "info_basica";
            $establecimiento = establecimientos::obtenerInfoEstablecimiento(
                            $arreglo_datos['TABLA_ADICIONALES']['est_id'], $est);
            $datos_enlace['codigo'] = $establecimiento['est_codigo'];
        $body = "<tbody>";
        foreach ($arreglo_datos['VALORES_TABLA'] as $cuerpos) {
            $body .= "<tr>";
            foreach ($cuerpos as $campo => $cuerpo) {
                if (isset($arreglo_datos['CELDAS_TABLA'][$campo])) {

                    if (isset($arreglo_datos['CELDAS_TABLA'][$campo]['celda_indicador'])) {
                        $tipo = $arreglo_datos['CELDAS_TABLA'][$campo]['celda_tabla'];
                        $body .= '<td class="' . $arreglo_datos['CELDAS_TABLA'][$campo]['celda_indicador'][$cuerpos[$tipo]] . '" '
                                . $arreglo_datos['CELDAS_TABLA'][$campo]['atributos'] . ">";
                    } else {
                        $body .= '<td align="left" class="' . $arreglo_datos['CELDAS_TABLA'][$campo]['class'] . '" '
                                . $arreglo_datos['CELDAS_TABLA'][$campo]['atributos'] . ">";
                    }

                    //maximo caracteres
                    if (isset($arreglo_datos['CELDAS_TABLA'][$campo]['maximo_caracter'])){
                        if(strlen($cuerpos[$campo]) > $arreglo_datos['CELDAS_TABLA'][$campo]['maximo_caracter']){
                            $cuerpos[$campo] = substr($cuerpos[$campo], 0, $arreglo_datos['CELDAS_TABLA'][$campo]['maximo_caracter']) . "...";
                        }
                    }

                    //si tiene link
                    if (isset($arreglo_datos['CELDAS_TABLA'][$campo]['link'])) {
                        //concatena los parametros necesarios
                        $parametros = "";
                        if (isset($arreglo_datos['CELDAS_TABLA'][$campo]['parametros'])) {
                            foreach ($arreglo_datos['CELDAS_TABLA'][$campo]['parametros'] as $parametro) {
                                $parametros .= "&$parametro=" . $cuerpos[$parametro];
                            }
                        }
                        //crea el enlace seguro
                        $datos_enlace['enlace'] = $arreglo_datos['CELDAS_TABLA'][$campo]['link'] . $parametros;
                        $enlace_acceso = Herramientas::generarEnlaceExterno($datos_enlace);

                        $body .= '<a href="' . $enlace_acceso . '" target="_blank">' . $cuerpos[$campo] . "</a>";
                    } else {//de lo contrario texto normal
                        $body .= $cuerpos[$campo];
                    }
                    $body .= "</td>";
                }
            }
            $body .="</tr>";
        }

        $body .= "</tbody></table>";

        $contenido .= $header . $body;

        $datos = array();
        $datos['ubicacion_archivo'] = $_PATH_SERVIDOR . 'Includes/CSS/estilos_informes_tabla_correo.css';
        $datos['contenido'] = $contenido;
        $html = Interfaz::asignarEstilosDesdeArchivo($datos);
        return $html;
    }

//Fin crearListado()

    /**
     * Función que se encarga de crear los enlaces para paginación
     * DM - 2013-11-18
     * */
    static function crearPaginacion($arreglo_datos) {
        global $_PATH_WEB, $idi_despliegue;
        $contenido = "<div class='row'>";
        $offset = $arreglo_datos['LISTADO']['offset'];
        $limit = $arreglo_datos['LISTADO']['limite'] != '' ? $arreglo_datos['LISTADO']['limite'] : $arreglo_datos['LISTADO']['limit'];
        $total_registros = $arreglo_datos['LISTADO']['num_total_registros'];

        //se usa para pasar entre páginas los parametros de la paginación
        //agrega la acción para la paginación
        $enlace_datos = 'accion=' . $arreglo_datos['ACCION_LISTAR'];

        //pasa los datos de ordenamiento
        $enlace_datos .= '&ordena=' . $arreglo_datos['DATOS']['ordena'];
        $enlace_datos .= '&tipo_ordena=' . $arreglo_datos['DATOS']['tipo_ordena'];
        //agrega los valores de limit de la paginación
        $enlace_datos .= '&limit=' . $limit;

        //si es para ajax y tiene ' entonces debe cambiar para que no genere error en js
        $enlace_datos = $arreglo_datos['ajax'] == 1 ? str_replace('\'', '\\\'', $enlace_datos) : $enlace_datos;

        if ($total_registros > 0 && !isset($arreglo_datos['bloquear_paginacion_grupos'])) {
            //DMG 2014-10-22 Se agrega el texto de modificar por idiomas
            $mostrar = $idi_despliegue['mostrar'];
            $mostrando = $idi_despliegue['mostrando'];
            $a = $idi_despliegue['labela_a'];
            $de = $idi_despliegue['label_de'];
            $enlaceLimit = "index.php?ordena=" . $arreglo_datos['DATOS']['ordena'] . "&tipo_ordena=" . $arreglo_datos['DATOS']['tipo_ordena'] . "&accion=" . $arreglo_datos['ACCION_LISTAR'] . "&offset=0";
            $arr_limits = array(10, 25, 50, 100);

            $select_mostrar .= " <label id='select_mostrar' for='select_mostrar'>" . $mostrar .
                    '&nbsp; <select onchange="opcionesTablaAjax(\'' . $enlaceLimit . '&limit=\'+this.value)" class="form-control input-sm" style="width:75px; display:inline-block;" id="select_mostrar">';
            foreach ($arr_limits as $limits) {
                $select_mostrar .= "<option value='$limits' ";
                $select_mostrar .= ($limit == $limits) ? "selected" : "";
                $select_mostrar .= "> $limits </option>";
            }
            $select_mostrar .= '</select></label>';

            $limite = (($offset + $limit) > $total_registros) ? $total_registros : ($offset + $limit);
            $showing = "<div class='col-md-6' style='text-align:left;'>$mostrando " . ($offset + 1) . " $a $limite $de $total_registros </div>";
        }//Fin de if( $total_registros > 0 )
        $conte = "<div class='col-md-6 right'><ul class='pagination-aux' >";

        //Crea el enlace para pasar a la página anterior
        if ($offset > 0) {
            if ($arreglo_datos['ajax'] == 1) {
                $enlace = $enlace_datos;
                $enlace .= '&offset=' . ($offset - $limit);
            }//Fin de if( $datos['ajax'] == 1)
            else {
                $enlace = 'index.php?' . $enlace_datos;
                $enlace .= '&offset=' . ($offset - $limit);
            }//Fin de if( $datos['ajax'] == 1)
            $conte .= "<li class='paginate_button previous' style='display:inline;'> <a href='$enlace'> <i class='fa fa-angle-double-left'></i> </a></li>";
        }//Fin de if( $offset > 0 )
        //Campos select con el numero de paginas

        if ($total_registros > $limit && $limit > 0) {
            if ($arreglo_datos['ajax'] == 1) {
                $enlace = $enlace_datos;
                $enlace .= '&offset=';
            }//Fin de if( $datos['ajax'] == 1)
            else {
                $enlace = 'index.php?' . $enlace_datos;
                $enlace .= '&offset=';
            }//Fin de if( $datos['ajax'] == 1)

            $size = ceil($total_registros / $limit);
            $pagina_actual = ceil($offset / $limit) + 1;

            //DM - 2016-02-24
            //Solo debe mostrar 10 registros de paginas
            $pagina_inicial = 1;
            $pagina_final = $size;
            //Si se necesita mas de 10 paginas
            if ($size > 10) {
                $pagina_inicial = $pagina_actual - 5;
                $pagina_final = $pagina_actual + 5;
                if ($pagina_inicial <= 0) {
                    $pagina_final += abs($pagina_inicial);
                    $pagina_inicial = 1;
                }

                if ($pagina_final > $size) {
                    $diferencia = $pagina_final - $size;
                    while ($diferencia > 0 && $pagina_inicial > 0) {
                        $pagina_inicial--;
                        $diferencia--;
                    }//Fin de while($diferencia>0 && $pagina_inicial>0)
                    $pagina_final = $size;
                }
            }//Fin de if( $size > 10 )

            for ($i = $pagina_inicial; $i <= $pagina_final; $i++) {
                $selected = ( $i == $pagina_actual ? 'active' : '');
                $conte .= "<li class='paginate_button $selected'><a href='$enlace" . (($i - 1) * $limit) . "'>$i</a></li>";
            }//Fin de for ($i = 1; $i <= $size; $i++)
            //$contenido .= '</select>';
        }//Fin de if( $total_registros > $limit)
        //Crea el enlace para pasar a la página siguiente
        if ($total_registros > ($offset + $limit)) {
            if ($arreglo_datos['ajax'] == 1) {
                $enlace = $enlace_datos;
                $enlace .= '&offset=' . ($offset + $limit);
            }//Fin de if( $datos['ajax'] == 1)
            else {
                $enlace = 'index.php?' . $enlace_datos;
                $enlace .= '&offset=' . ($offset + $limit);
            }//Fin de if( $datos['ajax'] == 1)

            $conte .= "<li class='paginate_button next'> <a href='$enlace'> <i class='fa fa-angle-double-right'></i> </a></li>";
        }//Fin deif( $total_registros > ($offset+$limit))

        $conte .= "</ul></div>";
        $contenido .= $showing . $conte . "</div>";

        if (isset($arreglo_datos['DATOS']['arreglo'])) {
            return array('footer' => $contenido, 'top' => $select_mostrar);
        } else {
            return $contenido;
        }
    }

//Fin de crearPaginacion()

    static function asignarToken($clave, $reemplazo, &$contenido) {
        $contenido = str_replace("<!--" . $clave . "-->", $reemplazo, $contenido);

        return $contenido;
    }

//Fin de asignarToken()

    static function asignarTokenDatos($clave, $datos, &$contenido) {
        $contenido = str_replace("<!--" . $clave . "-->", $datos[$clave], $contenido);

        return $contenido;
    }

//Fin de asignarToken()

    function archivoExcel($archivo, $tabla) {
        global $_PATH_WEB, $_PATH_IMAGENES, $idi_alertas;

        //Se cargan los estilos disponibles
        $estilos = "";
        $this->_l_css = is_array($this->_l_css) ? $this->_l_css : array();
        foreach ($this->_l_css as $estilo) {
            $estilos .= "<link href=\"" . $_PATH_WEB . "/Includes/CSS/" . $estilo . "\" rel=\"stylesheet\" type=\"text/css\" />\n";
        }//Fin de foreach($this->_estilos as $estilo)

        $cabeceraInforme = $archivo;

        $archivo = Herramientas::eliminarCaracteresEspeciales($archivo);

        $archivo = str_replace('&aacute;', 'a', $archivo);
        $archivo = str_replace('&eacute;', 'e', $archivo);
        $archivo = str_replace('&iacute;', 'i', $archivo);
        $archivo = str_replace('&oacute;', 'o', $archivo);
        $archivo = str_replace('&oacute;', 'o', $archivo);
        $archivo = str_replace(' ', '_', $archivo);
        $archivo = $archivo . ".xls";
        header('Content-type: application/vnd.ms-excel');
        header("Content-Disposition: attachment; filename=" . $archivo);
        $documento = '	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                            <html xmlns="http://www.w3.org/1999/xhtml">
                            <head>
                            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
                            <title>Archivo Excel</title>
                            </head>
                            <body>';
        $fin = "</body></html>";
        $cabeceraInforme = "<div style ='font-size: 20px;font-weight: bold;' align='center'>" . $cabeceraInforme . "</div>";
        echo $documento . $cabeceraInforme . $tabla . $fin;
    }

//Fin de archivoExcel()

    function crearPaginacionUsuarios($arreglo_datos, $campo, $tipo_ordena_base) {
        global $_PATH_WEB, $idi_despliegue;

        $contenido = "";

        $contenido .= "<table align='center' width='80%' border='0'>
        					<tr>
        						<td align='left'>";

        $offset = ($arreglo_datos['DATOS']['offset'] == '') ? 0 : $arreglo_datos['DATOS']['offset']; //$arreglo_datos['LISTADO']['offset'];

        $limit = ($arreglo_datos['DATOS']['limit'] == '') ? 10 : $arreglo_datos['DATOS']['limit']; //$arreglo_datos['LISTADO']['limite'];
        $total_registros = $arreglo_datos['LISTADO']['num_total_registros'];

        //Enlace de anterior
        if ($offset > 0) {
            $contenido .= "<a class='enlace' href='index.php?est_id=" . $arreglo_datos['DATOS']['est_id'] . "&ordena=" . $arreglo_datos['DATOS']['ordena'] . "&tipo_ordena=" . $arreglo_datos['DATOS']['tipo_ordena'] . "&accion=" . $arreglo_datos['ACCION_LISTAR'] . "&usu_per_id=" . $arreglo_datos['DATOS']['usu_per_id'] . "&usc_codigo=" . $arreglo_datos['DATOS']['usc_codigo'] . "&offset=" . ($offset - $limit) . "&limit=" . $limit . "'>" . $idi_despliegue[paginacion_atras] . "</a>";
        }//Fin de if( $offset > 0 )
        //Campos select con el numero de paginas
        if ($total_registros > $limit) {
            $enlace = "index.php?est_id=" . $arreglo_datos['DATOS']['est_id'] . "&ordena=" . $arreglo_datos['DATOS']['ordena'] . "&tipo_ordena=" . $arreglo_datos['DATOS']['tipo_ordena'] . "&accion=" . $arreglo_datos['ACCION_LISTAR'] . "&usu_per_id=" . $arreglo_datos['DATOS']['usu_per_id'] . "&usc_codigo=" . $arreglo_datos['DATOS']['usc_codigo'] . "&limit=" . $limit . "&offset=";

            $contenido .= "<select onchange=\"location.href='" . $enlace . "'+this.value\">";

            $size = ceil($total_registros / $limit);

            $pagina_actual = ceil($offset / $limit) + 1;

            for ($i = 1; $i <= $size; $i++) {
                $selected = ( $i == $pagina_actual ? "selected" : "");
                $contenido .= "<option " . $selected . " value='" . (($i - 1) * $limit) . "'>" . $idi_despliegue[paginacion_pag] . " " . $i . "</option>";
            }
            $contenido .= "</select>";
        }//Fin de if( $total_registros > $limit)
        //Enlace siguiente
        if ($total_registros > ($offset + $limit)) {
            $contenido .= "&nbsp;&nbsp;&nbsp;<a class='enlace' href='index.php?est_id=" . $arreglo_datos['DATOS']['est_id'] . "&ordena=" . $arreglo_datos['DATOS']['ordena'] . "&tipo_ordena=" . $arreglo_datos['DATOS']['tipo_ordena'] . "&accion=" . $arreglo_datos['ACCION_LISTAR'] . "&usu_per_id=" . $arreglo_datos['DATOS']['usu_per_id'] . "&usc_codigo=" . $arreglo_datos['DATOS']['usc_codigo'] . "&offset=" . ($offset + $limit) . "&limit=" . $limit . "'>" . $idi_despliegue[paginacion_adelante] . "</a>";
            ;
        }//Fin deif( $total_registros > ($offset+$limit))

        $enlacePie = "index.php?est_id=" . $arreglo_datos['DATOS']['est_id'] . "&ordena=" . $campo . "&tipo_ordena=" . $tipo_ordena_base . "&accion=" . $arreglo_datos['ACCION_LISTAR'] . "&usu_per_id=" . $arreglo_datos['DATOS']['usu_per_id'] . "&usc_codigo=" . $arreglo_datos['DATOS']['usc_codigo'];

        $contenido .= "</td>"; //Se cierra la celda del select de las paginas

        if ($total_registros > $limit) {
            $contenido .= "<td align='right'>
                                        Mostrar:[ <a class='enlace' href='" . $enlacePie . "&limit=10'>10</a> -
                                                    <a class='enlace' href='" . $enlacePie . "&limit=25'>25</a> -
                                                    <a class='enlace' href='" . $enlacePie . "&limit=50'>50</a> -
                                                    <a class='enlace' href='" . $enlacePie . "&limit=100'>100</a> ]
                                </td>";
        }

        $contenido .= " </tr>
                        </table>";
        return $contenido;
    }

//Fin de crearPaginacionUsuarios()
    //pgs - 02/04/2012 - funcion que se encarga de crear la paginacion para la pagina de comentarios
    function crearPaginacionComenarios($arreglo_datos) {
        global $_PATH_WEB, $_PATH_IMAGENES, $idi_despliegue;

        $offset = $arreglo_datos['LISTADO']['offset'];
        $limit = $arreglo_datos['LISTADO']['limite'];
        $total_registros = $arreglo_datos['LISTADO']['num_total_registros'];

        if ($total_registros == 0) {
            return '';
        }//Fin de if($total_registros==0)

        $numero_de = ($total_registros - $offset < $limit) ? ($total_registros - $offset) + $offset : $offset + $limit;

        $contenido = "<table border='0' align='right'><tr><td valign='middle'><b>" . ($offset + 1) . " - " . $numero_de . "</b> " . $idi_despliegue['label_de'] . " <b>" . $total_registros . "</b></td><td>";

        $id_camp_offset = $arreglo_datos['LISTADO']['id_camp_offset'];
        $id_form = $arreglo_datos['LISTADO']['id_form'];

        //enlace para ir a la pagina anterior
        if ($offset > 0) {
            $contenido .= " <a href='#' onclick='$(\"#" . $id_camp_offset . "\").val(" . ($offset - $limit) . ");$(\"#" . $id_form . "\").submit();'><img src='" . $_PATH_IMAGENES . "/anterior.jpg' border='0'></a>";
        }//Fin de if( $offset > 0 )
        //Enlace siguiente
        if ($total_registros > ($offset + $limit)) {
            $contenido .= "<a href='#' onclick='$(\"#" . $id_camp_offset . "\").val(" . ($offset + $limit) . ");$(\"#" . $id_form . "\").submit();'><img src='" . $_PATH_IMAGENES . "/siguiente.jpg' border='0'></a>";
        }//Fin de if( $total_registros > ($offset+$limit))

        $contenido .= '</td></tr></table>';

        return $contenido;
    }

//Fin de crearPaginacionComenarios($arreglo_datos)

    function crearListadoUsuarios($arreglo_datos) {
        global $_PATH_WEB, $idi_despliegue, $_PATH_IMAGENES, $obj_usuarios;

        $contenido = "";

        //Si el istado tiene un titulo
        if ($arreglo_datos['TITULO'] != "") {
            $contenido .= "<div class='listado_titulo'><p>" . $arreglo_datos['TITULO'] . "</p></div>";
        }//Fin de if($arreglo_datos['TITULO'] != "")

        $contenido .= $arreglo_datos['MENSAJE'];

        $borde = 0;
        if ($arreglo_datos[DATOS][exportar] != "") {
            $borde = 1;
        }

        $contenido .= "<div " . $arreglo_datos[clase_listado] . "><table border='" . $borde . "' cellpadding='2' cellspacing='0' width='100%' align='center'>";

        if ($arreglo_datos['cabecera'] != "") {
            $contenido .= $arreglo_datos['cabecera'];
        }

        $arreglo_datos['CAMPOS_ORDENA'] = is_array($arreglo_datos['CAMPOS_ORDENA']) ? $arreglo_datos['CAMPOS_ORDENA'] : array();

        if (count($arreglo_datos['CAMPOS']) > 0) {

            $contenido .= "<tr class='listado_columna_usuarios'>";

            if (isset($arreglo_datos['OPCIONES']['check'])) {
                $contenido .= "<td width=\"30\" class='listado_columna_usuarios'>&nbsp;</td>";
            }

            // coloca las etiquetas de las columnas en la tabla
            //$contenido .= "<td width='25' align='center'>No.</td>"; //Numerador de Filas

            foreach ($arreglo_datos['CAMPOS'] as $campo => $etiqueta) {
                if ($etiqueta != "") {
                    if (in_array($campo, $arreglo_datos['CAMPOS_ORDENA'])) {
                        $ordena = $arreglo_datos['DATOS']['ordena'];
                        $tipo_ordena = $arreglo_datos['DATOS']['tipo_ordena'];

                        $adicional = "";
                        $tipo_ordena_base = "";
                        switch ($ordena . $tipo_ordena) {
                            case $campo:
                            case $campo . "ASC":
                                $adicional = "&nbsp;ASC";
                                $tipo_ordena_base = "DESC";
                                break;

                            case $campo . "DESC":
                                $adicional = "&nbsp;DESC";
                                $tipo_ordena_base = "ASC";
                                break;
                        }//Fin de switch($campo)

                        $enlace = "index.php?ordena=" . $campo . "&tipo_ordena=" . $tipo_ordena_base . "&offset=" . $arreglo_datos['DATOS']['offset'] . "&limit=" . $arreglo_datos['DATOS']['limit'] . "&accion=" . $arreglo_datos['ACCION_LISTAR'] . "&usu_per_id=" . $arreglo_datos['DATOS']['usu_per_id'] . "&usc_codigo=" . $arreglo_datos['DATOS']['usc_codigo'];

                        $etiqueta = "<a class='enlace' href=\"" . $enlace . "\">" . $etiqueta . "</a>" . $adicional;
                    }
                    $contenido .= "<td class='listado_columna_usuarios'>$etiqueta</td>";
                }
            }//Fin de foreach($arreglo_datos['CAMPOS'] as $campo => $etiqueta)

            if (isset($arreglo_datos['OPCIONES']['tipoUsuario'])) {
                $contenido .= "<td width=\"60\" class='listado_columna'>" . $arreglo_datos['OPCIONES']['tipoUsuario'] . "</td>";
            }
            if (isset($arreglo_datos['OPCIONES']['categoriaUsuario'])) {
                $contenido .= "<td width=\"60\" class='listado_columna'>" . $arreglo_datos['OPCIONES']['categoriaUsuario'] . "</td>";
            }
            if (isset($arreglo_datos['OPCIONES']['seleccionar'])) {
                $contenido .= "<td width=\"60\" class='listado_columna'>&nbsp;</td>";
            }

            if (isset($arreglo_datos['OPCIONES']['registro'])) {
                $contenido .= "<td width=\"60\" class='listado_columna'>Registro</td>";
            }

            if (isset($arreglo_datos['OPCIONES']['delegar'])) {
                $contenido .= "<td width=\"60\" class='listado_columna'>Delegar</td>";
            }

            if (isset($arreglo_datos['OPCIONES']['opciones'])) {
                $contenido .= "<td width=\"60\" class='listado_columna'>Opciones</td>";
            }

            if (isset($arreglo_datos['OPCIONES']['ver'])) {
                $contenido .= "<td width=\"70\" class='listado_columna'>Historial</td>";
            }
            if (isset($arreglo_datos['OPCIONES']['enviar'])) {
                $contenido .= "<td width=\"60\" class='listado_columna'>Enviar</td>";
            }
            if (isset($arreglo_datos['OPCIONES']['login'])) {
                $contenido .= "<td width=\"90\" class='listado_columna'>&nbsp;</td>";
            }
            if (isset($arreglo_datos['OPCIONES']['actualizar'])) {
                $contenido .= "<td width=\"60\" class='listado_columna'>&nbsp</td>";
            }
            if (isset($arreglo_datos['OPCIONES']['eliminar'])) {
                $contenido .= "<td width=\"60\" class='listado_columna'>&nbsp</td>";
            }

            $contenido .= "</tr>"; //Fin de la Fila de las cabecera de las columnas

            $total_cabeceras = sizeof($arreglo_datos['CAMPOS']) + sizeof($arreglo_datos['OPCIONES']);

            $contenido .= "<tr>
                                <td colspan=" . $total_cabeceras . " align='center'><img width='100%' height='2' src='" . $_PATH_IMAGENES . "/linea_negra.png'></td>
                            </tr>";

            if ($arreglo_datos['cabecera_datos'] != "") {
                $contenido .= $arreglo_datos['cabecera_datos'];
            }

            //Si existen registros en el listado
            if (is_array($arreglo_datos['LISTADO']) && sizeof($arreglo_datos['LISTADO']) > 4) {

                $num_registro = $arreglo_datos['LISTADO']['offset'];

                //El tama�o del arreglo menos los campos del limit, offset, total registros y usa_filtro
                $size = sizeof($arreglo_datos['LISTADO']) - 4;

                // para cada resultado obtenido
                for ($i = 0; $i < $size; $i++) {

                    $obj_objeto = $arreglo_datos['LISTADO'][$i];

                    $estilo_fila = ($estilo_fila == "listado_fila_2" ? "listado_fila_blanca" : "listado_fila_2");

                    $funcion = "";
                    if ($arreglo_datos[check_color]) {
                        $funcion = "onclick=\"clickFila(this,'" . $estilo_fila . "')\"";
                    }
                    $contenido .= "<tr " . $funcion . " class='$estilo_fila<!--estilo_rojo-->'  >";


                    $estilo_rojo = "";
                    // obtiene los atributos del objeto
                    $atributos = array_keys($arreglo_datos['CAMPOS']);


                    if (isset($arreglo_datos['OPCIONES']['check'])) {
                        $est_id = $_SESSION['est_id'];

                        $checked = "";
                        if (is_numeric($obj_objeto['esb_id'])) {
                            $checked = "checked";
                        }
                        if (is_numeric($obj_objeto['esb_checked'])) {
                            $checked = "checked";
                        }

                        //deshabilita el registro
                        $habilitado = 1;
                        if ($obj_objeto['esb_cs_checked'] == "NO_CS") {
                            $checked = "";
                            $habilitado = 0;
                            $estilo_rojo = "_rojo";
                        }

                        if ($obj_objeto['esb_cs_checked'] == "LIMITE") {
                            $checked = "";
                            $habilitado = 0;
                            $estilo_rojo = "_amarillo";
                        }

                        if ($obj_objeto['est_fila_rojo'] == 1) {
                            $estilo_rojo = "_rojo";
                        }

                        /*
                          $contenido .= "
                          <td align=\"center\"  valign='middle' id='div_".$obj_objeto['est_id']."'>
                          <input onclick=\"javascript:establecimientoCS(".$est_id.",this,'".$obj_objeto['esb_id']."')\" ".$checked." type='checkbox' name='est_id[]' value='".$obj_objeto['est_id']."'>
                          </td>";
                         */

                        if ($arreglo_datos['OPCIONES']['check_habilitado'] == 1 && $habilitado == 1) {
                            if ($arreglo_datos['OPCIONES']['check_funcion'] == 1) {
                                if (isset($_SESSION[ids_boletin]) && is_array($_SESSION[ids_boletin])) {
                                    if ($_SESSION[ids_boletin][$obj_objeto['est_id']] > 0) {
                                        $checked = "checked";
                                    }
                                }
                                $contenido .= "
        						<td align=\"center\"  valign='middle' id='div_" . $obj_objeto['est_id'] . "'>
        							<input " . $checked . " type='checkbox' name='est_id[]' value='" . $obj_objeto['est_id'] . "' onclick=\"seleccionarCheck(this)\">
        						</td>";
                            }//Fin de if( $arreglo_datos['OPCIONES']['check_funcion'] == 1 )
                            else {
                                $contenido .= "
        						<td align=\"center\"  valign='middle' id='div_" . $obj_objeto['est_id'] . "'>
        							<input " . $checked . " type='checkbox' name='est_id[]' value='" . $obj_objeto['est_id'] . "'>
        						</td>";
                            }
                        } else {
                            $contenido .= "
    						<td align=\"center\"  valign='middle' id='div_" . $obj_objeto['est_id'] . "'>
    							<input " . $checked . " type='checkbox' disabled>
    						</td>";
                        }
                    }//Fin de if( isset($opciones['delegar']) )

                    $contenido = str_replace("<!--estilo_rojo-->", $estilo_rojo, $contenido);

                    //$contenido .= "<td align='center' class='listado_valor'>".($num_registro+$i+1)."</td>"; //NUMERADOR DE FILAS

                    $pos_campo = 0;
                    //Coloca los valores de cada objeto
                    foreach ($arreglo_datos['CAMPOS'] as $campo => $etiqueta) {
                        if ($etiqueta != "") {
                            $valor = $obj_objeto[$campo];

                            //Si el texto es mayor a 58 caracteres entonces lo corta
                            if (strlen($valor) > 58 && $campo != "est_des_mes_anterior") {
                                $valor = substr($valor, 0, 55) . "...";
                            }

                            $estilo_valor = 'listado_valor';
                            if ($pos_campo == 0) {
                                $estilo_valor = 'listado_valor_inicial';
                            }
                            $contenido .= "<td class='" . $estilo_valor . "' " . $arreglo_datos['CAMPOS_OPCIONES'][$campo] . "> " . $valor . "</td>";

                            $pos_campo++;
                        }//Fin de if($etiqueta != "")
                    }//Fin de foreach()



                    if (isset($arreglo_datos['OPCIONES']['tipoUsuario'])) {
                        //Consultamos el tipo del usuario
                        $contenido .= "
					<td align=\"left\" height='38' valign='middle' class='listado_valor'>" . $arreglo_datos['LISTADO'][$i]['pid_nombre'] . "
								</td>";
                    }//Fin de if( isset($opciones['tipoUsuario']) )

                    if (isset($arreglo_datos['OPCIONES']['categoriaUsuario'])) {
                        //Consultamos la categoria del usuario
                        $contenido .= "
					<td align=\"left\" height='38' valign='middle' class='listado_valor'>" . $arreglo_datos['LISTADO'][$i]['uci_nombre'] . "
								</td>";
                    }//Fin de if( isset($opciones['tipoUsuario']) )

                    if (isset($arreglo_datos['OPCIONES']['seleccionar'])) {
                        $enlace = $arreglo_datos['OPCIONES']['seleccionar'];
                        for ($k = 0; $k < count($atributos); $k++) {
                            eval('$valor = $obj_objeto[' . $atributos[$k] . '];');
                            $enlace = str_replace("%" . $atributos[$k] . "%", $valor, $enlace);
                        }//Fin de for($i=0; $i<count($atributos); $i++)
                        $contenido .= "
						<td align=\"center\" height='38' valign='middle'
							<a class='enlace' href=\"#\" onclick=\"" . $enlace . "\" >Seleccionar</a>
						</td>";
                    }//Fin de if( isset($opciones['delegar']) )

                    if (isset($arreglo_datos['OPCIONES']['registro'])) {
                        $enlace = $arreglo_datos['OPCIONES']['registro'];
                        for ($k = 0; $k < count($atributos); $k++) {
                            eval('$valor = $obj_objeto[' . $atributos[$k] . '];');
                            $enlace = str_replace("%" . $atributos[$k] . "%", $valor, $enlace);
                        }//Fin de for($i=0; $i<count($atributos); $i++)
                        $contenido .= "
						<td align=\"center\" height='38' valign='middle' class='listado_valor'>
							<a class='enlace' href=\"index.php?accion=" . $arreglo_datos['ACCION_REGISTRO'] . "&$enlace\" >
								<img src='" . $_PATH_WEB . "/Includes/Imagenes/registro.png' border=0 width=20 height=20 title='Registro de Asistencia'>
							</a>
						</td>";
                    }//Fin de if( isset($opciones['delegar']) )


                    if (isset($arreglo_datos['OPCIONES']['delegar'])) {
                        $enlace = $arreglo_datos['OPCIONES']['delegar'];
                        for ($k = 0; $k < count($atributos); $k++) {
                            eval('$valor = $obj_objeto[' . $atributos[$k] . '];');
                            $enlace = str_replace("%" . $atributos[$k] . "%", $valor, $enlace);
                        }//Fin de for($i=0; $i<count($atributos); $i++)
                        $contenido .= "
						<td align=\"center\" height='38' valign='middle' class='listado_valor'>
							<a class='enlace' href=\"index.php?accion=" . $arreglo_datos['ACCION_DELEGAR'] . "&$enlace\" >
								<img src='" . $_PATH_WEB . "/Includes/Imagenes/delegar.png' border=0 width=20 height=20 title='Delegar acciones'>
							</a>
						</td>";
                    }//Fin de if( isset($opciones['delegar']) )


                    if (isset($arreglo_datos['OPCIONES']['opciones'])) {
                        $enlace = $arreglo_datos['OPCIONES']['opciones'];
                        for ($k = 0; $k < count($atributos); $k++) {
                            eval('$valor = $obj_objeto[' . $atributos[$k] . '];');
                            $enlace = str_replace("%" . $atributos[$k] . "%", $valor, $enlace);
                        }//Fin de for($i=0; $i<count($atributos); $i++)
                        $contenido .= "
						<td align=\"center\" height='38' valign='middle' class='listado_valor'>
							<a class='enlace' href=\"index.php?accion=" . $arreglo_datos['ACCION_OPCIONES'] . "&$enlace\" >
								<img src='" . $_PATH_WEB . "/Includes/Imagenes/eliminar.png' border=0 width=20 height=20 title='Registro de Opciones'>
							</a>
						</td>";
                    }//Fin de if( isset($opciones['delegar']) )
                    // para colocar el enlace de ver, actualizar y  eliminar
                    if (isset($arreglo_datos['OPCIONES']['ver'])) {
                        $enlace = $arreglo_datos['OPCIONES']['ver'];
                        for ($j = 0; $j < count($atributos); $j++) {
                            eval('$valor = $obj_objeto[' . $atributos[$j] . '];');
                            $enlace = str_replace("%" . $atributos[$j] . "%", $valor, $enlace);
                        }//Fin de for($i=0; $i<count($atributos); $i++)
                        $contenido .= "
					<td align=\"center\" class='listado_valor'>
						<a class='enlace' href=\"index.php?accion=" . $arreglo_datos['ACCION_VER'] . "&$enlace\">
							Ver
						</a>
					</td>";
                    }//Fin de if( isset($opciones['ver']) )


                    if (isset($arreglo_datos['OPCIONES']['login'])) {
                        $enlace = $arreglo_datos['OPCIONES']['login'];
                        for ($k = 0; $k < count($atributos); $k++) {
                            eval('$valor = $obj_objeto[' . $atributos[$k] . '];');
                            $enlace = str_replace("%" . $atributos[$k] . "%", $valor, $enlace);
                        }//Fin de for($i=0; $i<count($atributos); $i++)

                        $enlace = "index.php?accion=" . $arreglo_datos['ACCION_LOGIN'] . "&" . $enlace;
                        $enlace_js = "return confirmarAccesoLoginRoot('" . $enlace . "');";

                        $contenido .= "
						<td align=\"center\" height='38' valign='middle' class='listado_valor'>
							<a class='enlace' href=\"#\" onclick=\"" . $enlace_js . "\" >" . $idi_despliegue['listado_enlace_login'] . "</a>
						</td>";
                    }//Fin de if( isset($opciones['delegar']) )



                    if (isset($arreglo_datos['OPCIONES']['actualizar'])) {
                        $enlace = $arreglo_datos['OPCIONES']['actualizar'];

                        for ($j = 0; $j < count($atributos); $j++) {
                            eval('$valor = $obj_objeto[' . $atributos[$j] . '];');
                            $enlace = str_replace("%" . $atributos[$j] . "%", $valor, $enlace);
                        }//Fin de for($i=0; $i<count($atributos); $i++)

                        if (isset($arreglo_datos['OPCIONES']['copiar'])) {
                            $enlace = $arreglo_datos['OPCIONES']['copiar'];
                            for ($j = 0; $j < count($atributos); $j++) {
                                eval('$valor = $obj_objeto[' . $atributos[$j] . '];');
                                $enlace = str_replace("%" . $atributos[$j] . "%", $valor, $enlace);
                            }//Fin de for($i=0; $i<count($atributos); $i++)

                            $contenido .= "
							<td align=\"center\"  class='listado_valor' >
								<a class='enlace' href=\"index.php?accion=" . $arreglo_datos['ACCION_COPIAR'] . "&$enlace\">
									<img src='" . $_PATH_WEB . "/Includes/Imagenes/editar.png' border=0 >
								</a>
							</td>";
                        }//Fin de if( isset($opciones['copiar']) )

                        if ($arreglo_datos['URL_MODIFICAR_COMPLETA']) {
                            $contenido .= "
							<td align=\"center\">
								<a class='enlace' href=\"" . $arreglo_datos['ACCION_ACTUALIZAR'] . "&$enlace\">
									<img src='" . $_PATH_WEB . "/InterfazGrafica/Imagenes/icon_editar.jpg' border=0>
								</a>
							</td>";
                        } else {
                            if (($obj_usuarios->obtenerCategoriaUsuario($arreglo_datos['LISTADO'][$i]['usu_id']) == 1) &&
                                    ($obj_usuarios->obtenerCategoriaUsuario($_SESSION['usu_id']) == 2)) { //Si es un usuario secundario no puede modificar un primario
                                $contenido .= "
									<td align=\"center\" height='38' valign='middle' class='listado_valor'>
										&nbsp;
									</td>";
                            } else {
                                $contenido .= "
									<td align=\"center\" height='38' valign='middle' class='listado_valor'>
										<a class='enlace' href=\"index.php?accion=" . $arreglo_datos['ACCION_ACTUALIZAR'] . "&$enlace\">
											" . $idi_despliegue['modificar'] . "
										</a>
									</td>";
                            }
                        }
                    }//Fin de if( isset($opciones['actualizar']) )


                    if (isset($arreglo_datos['OPCIONES']['eliminar'])) {
                        $enlace = $arreglo_datos['OPCIONES']['eliminar'];
                        for ($k = 0; $k < count($atributos); $k++) {
                            eval('$valor = $obj_objeto[' . $atributos[$k] . '];');
                            $enlace = str_replace("%" . $atributos[$k] . "%", $valor, $enlace);
                        }//Fin de for($i=0; $i<count($atributos); $i++)

                        if (($_SESSION['usu_id'] == $arreglo_datos['LISTADO'][$i]['usu_id']) ||
                                ($obj_usuarios->obtenerCategoriaUsuario($arreglo_datos['LISTADO'][$i]['usu_id']) == 1) &&
                                ($obj_usuarios->obtenerCategoriaUsuario($_SESSION['usu_id']) == 2)
                        ) { //El usuario actual no se puede eliminar a el mismo, y si es secundario no puede eliminar el primario
                            $contenido .= "
							<td align=\"center\" height='38' valign='middle' class='listado_valor'>
								&nbsp;
							</td>";
                        } else {
                            $contenido .= "
							<td align=\"center\" height='38' valign='middle' class='listado_valor'>
								<a class='enlace' href=\"index.php?m=usuarios&accion=" . $arreglo_datos['ACCION_ELIMINAR'] . "&$enlace\" onClick=\"return confirm('" . $idi_despliegue['seguro_eliminar_usuario'] . "')\">
									" . $idi_despliegue['eliminar'] . "
								</a>
							</td>";
                        }
                    }//Fin de if( isset($opciones['eliminar']) )

                    $contenido .= "</tr>";
                }//Fin de for($i = 0; $i < $arreglo_datos['LISTADO']['limite']; $i++)
            }//Fin de if( is_array($arreglo_datos['LISTADO']) )
            else {
                if ($arreglo_datos['LISTADO']['usa_filtro'] == 1) {
                    $contenido .= "
					<tr>
						<td colspan=" . (count($arreglo_datos['CAMPOS']) + 2) . " align=\"center\">
							<br><h3>No existen registros para el criterio de b&uacute;squeda seleccionado</h3><br>
						</td>
					</tr>";
                } else {
                    $contenido .= "
					<tr>
						<td colspan=" . (count($arreglo_datos['CAMPOS']) + 2) . " align=\"center\">
							<br><h3>" . $idi_despliegue['no_registros'] . "</h3><br>
						</td>
					</tr>";
                }
            }
        }//Fin de if( count($campos)>0 )

        $contenido .= "</table></div>";

        if ($arreglo_datos['pie'] != "") {
            $contenido .= $arreglo_datos['pie'];
        }



        /*         * *************************** PAGINACION ALTERNA ***************************************
          $enlacePie = "index.php?ordena=".$campo."&tipo_ordena=".$tipo_ordena_base."&accion=".$arreglo_datos['ACCION_LISTAR']."&usu_per_id=".$arreglo_datos['DATOS']['usu_per_id']."&usc_codigo=".$arreglo_datos['DATOS']['usc_codigo'];
          $totalUsuarios = $arreglo_datos['LISTADO']['num_total_registros'];
          $limite = ($arreglo_datos['DATOS']['limit']=="") ? 10 : $arreglo_datos['DATOS']['limit'];
          $paginas = ceil($totalUsuarios/$limite);
          $paginate = "";
          $inicio = ($arreglo_datos['DATOS']['inicio']=="") ? 1 : $arreglo_datos['DATOS']['inicio'];
          $fin = $inicio + 9;
          if ($fin > $paginas)
          {
          $fin = $paginas;
          }
          if ($inicio > 1)
          {
          $paginate .= "<a href='".$enlacePie."'><< ".$idi_despliegue['anterior']."</a>";
          }
          for ($k = $inicio ; $k <= $fin ; $k++)
          {
          $paginate .= "&nbsp;<a href='".$enlacePie."&offset=".(($k-1)*$limite)."&inicio=".$inicio."'>".$k."</a> |";
          }
          if (($fin+1) <= $paginas)
          {
          $paginate .= "&nbsp;<a href='".$enlacePie."&offset=".($fin*$limite)."&inicio=".($fin+1)."'>".$idi_despliegue['siguiente']." >></a>";
          }

          $pieListado = "<table align='center' width='80%' border='0'>
          <tr>
          <td align='left'>|".$paginate."</td>
          <td align='right'>
          Mostrar:[ <a href='".$enlacePie."&limit=10'>10</a> -
          <a href='".$enlacePie."&limit=25'>25</a> -
          <a href='".$enlacePie."&limit=50'>50</a> -
          <a href='".$enlacePie."&limit=100'>100</a> ]
          </td>
          </tr>
          </table>";
          $contenido.=$pieListado;
         * ************************************************************************* */
        $contenido .=$this->crearPaginacionUsuarios($arreglo_datos, $campo, $tipo_ordena_base);

        $contenido .="</form>";

        return $contenido;
    }

//Fin crearListadoUsuarios()

    function crearListadoFamilias($arreglo_datos) {
        global $_PATH_WEB, $idi_despliegue, $_PATH_IMAGENES, $obj_usuarios;

        $contenido = "";

        //Si el istado tiene un titulo
        if ($arreglo_datos['TITULO'] != "") {
            $contenido .= "<div class='listado_titulo'><p>" . $arreglo_datos['TITULO'] . "</p></div>";
        }//Fin de if($arreglo_datos['TITULO'] != "")

        $contenido .= $arreglo_datos['MENSAJE'];

        $borde = 0;
        if ($arreglo_datos[DATOS][exportar] != "") {
            $borde = 1;
        }

        $contenido .= "<div " . $arreglo_datos[clase_listado] . "><table border='" . $borde . "' cellpadding='2' cellspacing='0' width='80%' align='center'>";

        if ($arreglo_datos['cabecera'] != "") {
            $contenido .= $arreglo_datos['cabecera'];
        }

        $arreglo_datos['CAMPOS_ORDENA'] = is_array($arreglo_datos['CAMPOS_ORDENA']) ? $arreglo_datos['CAMPOS_ORDENA'] : array();


        if (count($arreglo_datos['CAMPOS']) > 0) {

            $contenido .= "<tr class='listado_columna_usuarios'>";

            if (isset($arreglo_datos['OPCIONES']['check'])) {
                $contenido .= "<td width=\"30\" class='listado_columna_usuarios'>&nbsp;</td>";
            }

            // coloca las etiquetas de las columnas en la tabla
            //$contenido .= "<td width='25' align='center'>No.</td>"; //Numerador de Filas

            foreach ($arreglo_datos['CAMPOS'] as $campo => $etiqueta) {
                if ($etiqueta != "" && $etiqueta != "hidden") {
                    if (in_array($campo, $arreglo_datos['CAMPOS_ORDENA'])) {
                        $ordena = $arreglo_datos['DATOS']['ordena'];
                        $tipo_ordena = $arreglo_datos['DATOS']['tipo_ordena'];

                        $adicional = "";
                        $tipo_ordena_base = "";
                        switch ($ordena . $tipo_ordena) {
                            case $campo:
                            case $campo . "ASC":
                                $adicional = "&nbsp;ASC";
                                $tipo_ordena_base = "DESC";
                                break;

                            case $campo . "DESC":
                                $adicional = "&nbsp;DESC";
                                $tipo_ordena_base = "ASC";
                                break;
                        }//Fin de switch($campo)

                        $enlace = "index.php?ordena=" . $campo . "&tipo_ordena=" . $tipo_ordena_base . "&offset=" . $arreglo_datos['DATOS']['offset'] . "&limit=" . $arreglo_datos['DATOS']['limit'] . "&accion=" . $arreglo_datos['ACCION_LISTAR'];

                        $etiqueta = "<a class='enlace' href=\"" . $enlace . "\">" . $etiqueta . "</a>" . $adicional;
                    }
                    $contenido .= "<td class='listado_columna_usuarios'>$etiqueta</td>";
                }
            }//Fin de foreach($arreglo_datos['CAMPOS'] as $campo => $etiqueta)

            if (isset($arreglo_datos['OPCIONES']['tipoUsuario'])) {
                $contenido .= "<td width=\"60\" class='listado_columna'>" . $arreglo_datos['OPCIONES']['tipoUsuario'] . "</td>";
            }
            if (isset($arreglo_datos['OPCIONES']['categoriaUsuario'])) {
                $contenido .= "<td width=\"60\" class='listado_columna'>" . $arreglo_datos['OPCIONES']['categoriaUsuario'] . "</td>";
            }
            if (isset($arreglo_datos['OPCIONES']['seleccionar'])) {
                $contenido .= "<td width=\"60\" class='listado_columna'>&nbsp;</td>";
            }

            if (isset($arreglo_datos['OPCIONES']['registro'])) {
                $contenido .= "<td width=\"60\" class='listado_columna'>Registro</td>";
            }

            if (isset($arreglo_datos['OPCIONES']['delegar'])) {
                $contenido .= "<td width=\"60\" class='listado_columna'>Delegar</td>";
            }

            if (isset($arreglo_datos['OPCIONES']['opciones'])) {
                $contenido .= "<td width=\"60\" class='listado_columna'>Opciones</td>";
            }

            if (isset($arreglo_datos['OPCIONES']['ver'])) {
                $contenido .= "<td width=\"70\" class='listado_columna'>Historial</td>";
            }
            if (isset($arreglo_datos['OPCIONES']['enviar'])) {
                $contenido .= "<td width=\"60\" class='listado_columna'>Enviar</td>";
            }
            if (isset($arreglo_datos['OPCIONES']['actualizar'])) {
                $contenido .= "<td width=\"60\" class='listado_columna'>&nbsp</td>";
            }
            if (isset($arreglo_datos['OPCIONES']['eliminar'])) {
                $contenido .= "<td width=\"60\" class='listado_columna'>&nbsp</td>";
            }

            $contenido .= "</tr>"; //Fin de la Fila de las cabecera de las columnas

            $total_cabeceras = sizeof($arreglo_datos['CAMPOS']) + sizeof($arreglo_datos['OPCIONES']);

            $contenido .= "<tr>
								<td colspan=" . $total_cabeceras . " align='center'><img width='100%' height='2' src='" . $_PATH_IMAGENES . "/linea_negra.png'></td>
							</tr>";

            if ($arreglo_datos['cabecera_datos'] != "") {
                $contenido .= $arreglo_datos['cabecera_datos'];
            }

            //Si existen registros en el listado
            if (is_array($arreglo_datos['LISTADO']) && sizeof($arreglo_datos['LISTADO']) > 4) {

                $num_registro = $arreglo_datos['LISTADO']['offset'];

                //El tama�o del arreglo menos los campos del limit, offset, total registros y usa_filtro
                $size = sizeof($arreglo_datos['LISTADO']) - 4;

                // para cada resultado obtenido
                for ($i = 0; $i < $size; $i++) {

                    $obj_objeto = $arreglo_datos['LISTADO'][$i];

                    $estilo_fila = ($estilo_fila == "listado_fila_2" ? "listado_fila_blanca" : "listado_fila_2");

                    $funcion = "";
                    if ($arreglo_datos[check_color]) {
                        $funcion = "onclick=\"clickFila(this,'" . $estilo_fila . "')\"";
                    }
                    $contenido .= "<tr " . $funcion . " class='$estilo_fila<!--estilo_rojo-->'  >";


                    $estilo_rojo = "";
                    // obtiene los atributos del objeto
                    $atributos = array_keys($arreglo_datos['CAMPOS']);


                    if (isset($arreglo_datos['OPCIONES']['check'])) {
                        $est_id = $_SESSION['est_id'];

                        $checked = "";
                        if (is_numeric($obj_objeto['esb_id'])) {
                            $checked = "checked";
                        }
                        if (is_numeric($obj_objeto['esb_checked'])) {
                            $checked = "checked";
                        }

                        //deshabilita el registro
                        $habilitado = 1;
                        if ($obj_objeto['esb_cs_checked'] == "NO_CS") {
                            $checked = "";
                            $habilitado = 0;
                            $estilo_rojo = "_rojo";
                        }

                        if ($obj_objeto['esb_cs_checked'] == "LIMITE") {
                            $checked = "";
                            $habilitado = 0;
                            $estilo_rojo = "_amarillo";
                        }

                        if ($obj_objeto['est_fila_rojo'] == 1) {
                            $estilo_rojo = "_rojo";
                        }

                        /*
                          $contenido .= "
                          <td align=\"center\"  valign='middle' id='div_".$obj_objeto['est_id']."'>
                          <input onclick=\"javascript:establecimientoCS(".$est_id.",this,'".$obj_objeto['esb_id']."')\" ".$checked." type='checkbox' name='est_id[]' value='".$obj_objeto['est_id']."'>
                          </td>";
                         */

                        if ($arreglo_datos['OPCIONES']['check_habilitado'] == 1 && $habilitado == 1) {
                            if ($arreglo_datos['OPCIONES']['check_funcion'] == 1) {
                                if (isset($_SESSION[ids_boletin]) && is_array($_SESSION[ids_boletin])) {
                                    if ($_SESSION[ids_boletin][$obj_objeto['est_id']] > 0) {
                                        $checked = "checked";
                                    }
                                }
                                $contenido .= "
								<td align=\"center\"  valign='middle' id='div_" . $obj_objeto['est_id'] . "'>
									<input " . $checked . " type='checkbox' name='est_id[]' value='" . $obj_objeto['est_id'] . "' onclick=\"seleccionarCheck(this)\">
								</td>";
                            }//Fin de if( $arreglo_datos['OPCIONES']['check_funcion'] == 1 )
                            else {
                                $contenido .= "
								<td align=\"center\"  valign='middle' id='div_" . $obj_objeto['est_id'] . "'>
									<input " . $checked . " type='checkbox' name='est_id[]' value='" . $obj_objeto['est_id'] . "'>
								</td>";
                            }
                        } else {
                            $contenido .= "
							<td align=\"center\"  valign='middle' id='div_" . $obj_objeto['est_id'] . "'>
								<input " . $checked . " type='checkbox' disabled>
							</td>";
                        }
                    }//Fin de if( isset($opciones['delegar']) )

                    $contenido = str_replace("<!--estilo_rojo-->", $estilo_rojo, $contenido);

                    //$contenido .= "<td align='center' class='listado_valor'>".($num_registro+$i+1)."</td>"; //NUMERADOR DE FILAS
                    //Coloca los valores de cada objeto
                    foreach ($arreglo_datos['CAMPOS'] as $campo => $etiqueta) {
                        if ($etiqueta != "" && $etiqueta != "hidden") {
                            $valor = $obj_objeto[$campo];

                            //Si el texto es mayor a 58 caracteres entonces lo corta
                            if (strlen($valor) > 108 && $campo != "est_des_mes_anterior") {
                                $valor = substr($valor, 0, 105) . "...";
                            }

                            $contenido .= "<td class='listado_valor' " . $arreglo_datos['CAMPOS_OPCIONES'][$campo] . ">" . $valor . "</td>";
                        }//Fin de if($etiqueta != "")

                        if ($etiqueta == "hidden") {
                            $valor = $obj_objeto[$campo];

                            $contenido .= "<input type='hidden' value='" . $valor . "'/>";
                        }//Fin de if($etiqueta != "")
                    }//Fin de foreach()



                    if (isset($arreglo_datos['OPCIONES']['tipoUsuario'])) {
                        //Consultamos el tipo del usuario
                        $contenido .= "
					<td align=\"left\" height='38' valign='middle' class='listado_valor'>" . $arreglo_datos['LISTADO'][$i]['per_nombre'] . "
								</td>";
                    }//Fin de if( isset($opciones['tipoUsuario']) )

                    if (isset($arreglo_datos['OPCIONES']['categoriaUsuario'])) {
                        //Consultamos la categoria del usuario
                        $contenido .= "
					<td align=\"left\" height='38' valign='middle' class='listado_valor'>" . $arreglo_datos['LISTADO'][$i]['usc_nombre'] . "
								</td>";
                    }//Fin de if( isset($opciones['tipoUsuario']) )

                    if (isset($arreglo_datos['OPCIONES']['seleccionar'])) {
                        $enlace = $arreglo_datos['OPCIONES']['seleccionar'];
                        for ($k = 0; $k < count($atributos); $k++) {
                            eval('$valor = $obj_objeto[' . $atributos[$k] . '];');
                            $enlace = str_replace("%" . $atributos[$k] . "%", $valor, $enlace);
                        }//Fin de for($i=0; $i<count($atributos); $i++)
                        $contenido .= "
						<td align=\"center\" height='38' valign='middle'>
							<a class='enlace' href=\"#\" onclick=\"" . $enlace . "\" >Seleccionar</a>
						</td>";
                    }//Fin de if( isset($opciones['delegar']) )

                    if (isset($arreglo_datos['OPCIONES']['registro'])) {
                        $enlace = $arreglo_datos['OPCIONES']['registro'];
                        for ($k = 0; $k < count($atributos); $k++) {
                            eval('$valor = $obj_objeto[' . $atributos[$k] . '];');
                            $enlace = str_replace("%" . $atributos[$k] . "%", $valor, $enlace);
                        }//Fin de for($i=0; $i<count($atributos); $i++)
                        $contenido .= "
						<td align=\"center\" height='38' valign='middle'>
							<a class='enlace' href=\"index.php?accion=" . $arreglo_datos['ACCION_REGISTRO'] . "&$enlace\" >
								<img src='" . $_PATH_WEB . "/Includes/Imagenes/registro.png' border=0 width=20 height=20 title='Registro de Asistencia'>
							</a>
						</td>";
                    }//Fin de if( isset($opciones['delegar']) )


                    if (isset($arreglo_datos['OPCIONES']['delegar'])) {
                        $enlace = $arreglo_datos['OPCIONES']['delegar'];
                        for ($k = 0; $k < count($atributos); $k++) {
                            eval('$valor = $obj_objeto[' . $atributos[$k] . '];');
                            $enlace = str_replace("%" . $atributos[$k] . "%", $valor, $enlace);
                        }//Fin de for($i=0; $i<count($atributos); $i++)
                        $contenido .= "
						<td align=\"center\" height='38' valign='middle'>
							<a class='enlace' href=\"index.php?accion=" . $arreglo_datos['ACCION_DELEGAR'] . "&$enlace\" >
								<img src='" . $_PATH_WEB . "/Includes/Imagenes/delegar.png' border=0 width=20 height=20 title='Delegar acciones'>
							</a>
						</td>";
                    }//Fin de if( isset($opciones['delegar']) )


                    if (isset($arreglo_datos['OPCIONES']['opciones'])) {
                        $enlace = $arreglo_datos['OPCIONES']['opciones'];
                        for ($k = 0; $k < count($atributos); $k++) {
                            eval('$valor = $obj_objeto[' . $atributos[$k] . '];');
                            $enlace = str_replace("%" . $atributos[$k] . "%", $valor, $enlace);
                        }//Fin de for($i=0; $i<count($atributos); $i++)
                        $contenido .= "
						<td align=\"center\" height='38' valign='middle'>
							<a class='enlace' href=\"index.php?accion=" . $arreglo_datos['ACCION_OPCIONES'] . "&$enlace\" >
								<img src='" . $_PATH_WEB . "/Includes/Imagenes/eliminar.png' border=0 width=20 height=20 title='Registro de Opciones'>
							</a>
						</td>";
                    }//Fin de if( isset($opciones['delegar']) )
                    // para colocar el enlace de ver, actualizar y  eliminar
                    if (isset($arreglo_datos['OPCIONES']['ver'])) {
                        $enlace = $arreglo_datos['OPCIONES']['ver'];
                        for ($j = 0; $j < count($atributos); $j++) {
                            eval('$valor = $obj_objeto[' . $atributos[$j] . '];');
                            $enlace = str_replace("%" . $atributos[$j] . "%", $valor, $enlace);
                        }//Fin de for($i=0; $i<count($atributos); $i++)
                        $contenido .= "
					<td align=\"center\">
						<a class='enlace' href=\"index.php?accion=" . $arreglo_datos['ACCION_VER'] . "&$enlace\">
							Ver
						</a>
					</td>";
                    }//Fin de if( isset($opciones['ver']) )





                    if (isset($arreglo_datos['OPCIONES']['actualizar'])) {
                        $enlace = $arreglo_datos['OPCIONES']['actualizar'];

                        for ($j = 0; $j < count($atributos); $j++) {
                            eval('$valor = $obj_objeto[' . $atributos[$j] . '];');
                            $enlace = str_replace("%" . $atributos[$j] . "%", $valor, $enlace);
                        }//Fin de for($i=0; $i<count($atributos); $i++)

                        if (isset($arreglo_datos['OPCIONES']['copiar'])) {
                            $enlace = $arreglo_datos['OPCIONES']['copiar'];
                            for ($j = 0; $j < count($atributos); $j++) {
                                eval('$valor = $obj_objeto[' . $atributos[$j] . '];');
                                $enlace = str_replace("%" . $atributos[$j] . "%", $valor, $enlace);
                            }//Fin de for($i=0; $i<count($atributos); $i++)

                            $contenido .= "
							<td align=\"center\" >
								<a class='enlace' href=\"index.php?accion=" . $arreglo_datos['ACCION_COPIAR'] . "&$enlace\">
									<img src='" . $_PATH_WEB . "/Includes/Imagenes/editar.png' border=0 >
								</a>
							</td>";
                        }//Fin de if( isset($opciones['copiar']) )

                        if ($arreglo_datos['URL_MODIFICAR_COMPLETA']) {
                            $contenido .= "
							<td align=\"center\">
								<a class='enlace' href=\"" . $arreglo_datos['ACCION_ACTUALIZAR'] . "&$enlace\">
									<img src='" . $_PATH_WEB . "/InterfazGrafica/Imagenes/icon_editar.jpg' border=0>
								</a>
							</td>";
                        } else {

                            //Si el usuario es de segunda categoria y no tiene activa la funcionalidad 31
                            if ($obj_usuarios->obtenerCategoriaUsuario($_SESSION['usu_id']) == 2 && !$obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 31)) {
                                $mensajeContacto = $obj_usuarios->mensajeActivarFuncionalidad();
                                $accionModificarFamilia = 'onclick= "fixedtooltip(\'' . $mensajeContacto . '\',this, event, \'180px\')"  onMouseout="delayhidetip()"';
                            } else { //El usuario no puede modificar las familias
                                $accionModificarFamilia = ' onClick = "textToInput(this)"';
                            }

                            $contenido .= "
											<td align=\"center\" height='38' valign='middle' class='listado_valor'>
												<a class='enlace' style='text-decoration: underline' " . $accionModificarFamilia . ">
													" . $idi_despliegue['modificar'] . "
												</a>
											</td>";
                        }
                    }//Fin de if( isset($opciones['actualizar']) )


                    if (isset($arreglo_datos['OPCIONES']['eliminar'])) {
                        $enlace = $arreglo_datos['OPCIONES']['eliminar'];
                        for ($k = 0; $k < count($atributos); $k++) {
                            eval('$valor = $obj_objeto[' . $atributos[$k] . '];');
                            $enlace = str_replace("%" . $atributos[$k] . "%", $valor, $enlace);
                        }//Fin de for($i=0; $i<count($atributos); $i++)
                        //Si el usuario es de segunda categoria y no tiene activa la funcionalidad 31
                        if ($obj_usuarios->obtenerCategoriaUsuario($_SESSION['usu_id']) == 2 && !$obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 31)) {
                            $mensajeContacto = $obj_usuarios->mensajeActivarFuncionalidad();
                            $enlaceEliminarFamilia = "javascript:void(0)";
                            $accionEliminarFamilia = " onClick= \"fixedtooltip('" . $mensajeContacto . "',this, event, '180px')\" onMouseout=\"delayhidetip()\"";
                        } else { //El usuario no puede modificar las familias
                            $enlaceEliminarFamilia = "index.php?m=familias&accion=" . $arreglo_datos['ACCION_ELIMINAR'] . "&$enlace";
                            $accionEliminarFamilia = "onClick= \"return confirm('" . $idi_despliegue['seguro_eliminar_familia'] . "')\"";
                        }

                        $contenido .= "
					<td align=\"center\" height='38' valign='middle' class='listado_valor'>
						<a class='enlace' href=\"" . $enlaceEliminarFamilia . "\" " . $accionEliminarFamilia . "\">
							" . $idi_despliegue['eliminar'] . "
						</a>
					</td>";
                    }//Fin de if( isset($opciones['eliminar']) )

                    $contenido .= "</tr>";
                }//Fin de for($i = 0; $i < $arreglo_datos['LISTADO']['limite']; $i++)
            }//Fin de if( is_array($arreglo_datos['LISTADO']) )
            else {
                if ($arreglo_datos['LISTADO']['usa_filtro'] == 1) {
                    $contenido .= "
					<tr>
						<td colspan=" . (count($arreglo_datos['CAMPOS']) + 2) . " align=\"center\">
							<br><h3>No existen registros para el criterio de b&uacute;squeda seleccionado</h3><br>
						</td>
					</tr>";
                } else {
                    $contenido .= "
					<tr>
						<td colspan=" . (count($arreglo_datos['CAMPOS']) + 2) . " align=\"center\">
							<br><h3>" . $idi_despliegue['no_registros'] . "</h3><br>
						</td>
					</tr>";
                }
            }
        }//Fin de if( count($campos)>0 )

        $contenido .= "</table></div>";

        if ($arreglo_datos['pie'] != "") {
            $contenido .= $arreglo_datos['pie'];
        }

        $contenido .=$this->crearPaginacionUsuarios($arreglo_datos, $campo, $tipo_ordena_base);

        $contenido .="</form>";

        return $contenido;
    }

//Fin crearListadoFamilias()

    /*
     * pgs - 10/08/2012 - funcion que se encarga de obtener las encuestas asociadas a un usuario
     */

    public function obtenerEncuestasAsociadas($datos) {
        global $_obj_database;

        $datos['est_id'] = (is_numeric($datos['est_id'])) ? $datos['est_id'] : $_SESSION['est_id'];
        $datos['tipo_usuario'] = (isset($datos['tipo_usuario'])) ? $datos['tipo_usuario'] : $_SESSION['tipo_usuario'];
        if (isset($_SESSION['usu_id'])){
	       $usu[0]['usu_id']  = $_SESSION['usu_id'];
        }else{
            $sql = "SELECT usu_id FROM usuario, usuario_establecimiento ";
            $sql .= "WHERE use_establecimiento_id = " . $datos['est_id'] . " ";
            $sql .= "AND usu_usc_id = 1 AND usu_estado = 1 "
                    . "AND use_usuario_id = usu_id";
            $usu = $_obj_database->obtenerRegistrosAsociativos($sql);
        }

        $sql_encuestas = "SELECT uen_enc_id, eni_nombre
	            FROM usuario,usuario_encuesta,encuesta_idioma
	            WHERE uen_usu_id = usu_id
	            AND uen_enc_id = eni_enc_id
	            AND eni_idi_id = '" . $_SESSION['idi_id'] . "'
	            AND uen_usu_id =" . $usu[0]['usu_id'] . "
               AND uen_estado = 1
                ORDER BY eni_nombre ASC";

        $encuestas = $_obj_database->obtenerRegistrosAsociativosPorClave($sql_encuestas, 'uen_enc_id');

        //OR 2016-20-10 si es cadena y está viendo sus propias preguntas
        //$selector_primario = $datos['selector_primario'];
        $est_id_seleccionado = '';

        $est_id = $datos['est_id'] == '' ? $est_id_seleccionado : $datos['est_id'];

        if ($datos['tipo_usuario'] == 'C' && $est_id == '') {
          return $encuestas;
        }
        //En caso de que sea un hotel o una cadena consultando preguntas de un hotel
        else if($datos['tipo_usuario'] == 'H' || $datos['tipo_usuario'] == 'C') {

            //Consulta los textos asignados por el establecimientos para las encuestas
            //en todos los idiomas
            $sql_encuestas = "SELECT eei_enc_id, eei_idi_id, eei_nombre
    	            FROM establecimiento_encuesta_idioma
    	            WHERE eei_est_id = " . $datos['est_id'] . "
                        and LENGTH(eei_nombre) > 0
                           ORDER BY eei_enc_id ASC, eei_idi_id ASC
                        ";

            $listado_encuestas_idiomas = $_obj_database->obtenerRegistrosAsociativosAgrupadosPorClave($sql_encuestas, 'eei_enc_id', 'eei_idi_id');

            foreach ($encuestas as $enc_id => $val) {
                $encuesta_idiomas_establecimiento = $listado_encuestas_idiomas[$enc_id];

                //Si existe un texto para el nombre de la encuesta debe asignarlo
                if (is_array($encuesta_idiomas_establecimiento)) {
                    $encuesta_idioma = $encuesta_idiomas_establecimiento[$_SESSION['idi_id']];
                    //Si existe un texto para el idioma actual lo asigna
                    if (is_array($encuesta_idioma)) {
                        $val['eni_nombre'] = $encuesta_idioma['eei_nombre'];
                    }//Fin de if( is_array($encuesta_idioma) )
                    else {
                        $encuesta_idioma = array_shift($encuesta_idiomas_establecimiento);
                        $val['eni_nombre'] = $encuesta_idioma['eei_nombre'];
                    }
                }//Fin de if(( is_array( $encuestas_idiomas[$enc_id] ) )

                $encuestas[$enc_id] = $val;
            }//Fin de foreach($encuestas as $enc_id => $val)

            $encuestas = Herramientas::ordenarArregloPorCampo($encuestas, 'eni_nombre');
        }//Fin de if( $_SESSION['tipo_usuario'] == 'H' )

        return $encuestas;
    }

//Fin de obtenerEncuestasAsociadas($datos)

    /*
     * MAOH - 10 Sep 2012 - Funcion que se encarga de crear la paginacion de los comentarios usndo ajax
     */

    function crearPaginacionComentariosXML($arreglo_datos) {
        global $_PATH_WEB, $_PATH_IMAGENES, $idi_despliegue;

        $offset = $arreglo_datos['LISTADO']['offset'];
        $limit = $arreglo_datos['LISTADO']['limite'];
        $total_registros = $arreglo_datos['LISTADO']['num_total_registros'];

        //Si no hay registros
        if ($total_registros == 0)
            return '';

        $numero_de = ($total_registros - $offset < $limit) ? ($total_registros - $offset) + $offset : $offset + $limit;
        $contenido = "<div style='float: right;'><table border='0' align='right'><tr><td valign='middle' align='right'><b>" . ($offset + 1) . " - " . $numero_de . "</b> " . $idi_despliegue['label_de'] . " <b>" . $total_registros . "</b></td><td align='right'>";

        //Enlace para ir a la pagina anterior
        if ($offset > 0)
            $contenido .= " <a href='#' onclick='buscarComentariosXML(" . ($offset - $limit) . ");'><img src='" . $_PATH_IMAGENES . "/anterior.jpg' border='0'></a>";

        //Enlace siguiente
        if ($total_registros > ($offset + $limit))
            $contenido .= "<a href='#' onclick='buscarComentariosXML(" . ($offset + $limit) . ");'><img src='" . $_PATH_IMAGENES . "/siguiente.jpg' border='0'></a>";

        $contenido .= '</td></tr></table></div>';

        return $contenido;
    }

//Fin de crearPaginacionComentariosXML($arreglo_datos)

    /**
     * Función que se encarga de retornar el contenido de plantilla que se envia por
     * correo electronico para los mensajes que envia la aplicación
     * DM - 2013-09-23
     * */
    static function obtenerPlantillaCorreo($datos = array()) {
        global $_PATH_SERVIDOR;

        $plantillas = $_PATH_SERVIDOR . '/Includes/Plantillas';

        $contenido = Archivos::obtenerContenidoArchivo($plantillas . '/plantilla_correo.html');

        return $contenido;
    }

//Fin de obtenerPlantillaCorreo()

    /**
     * Función que se encarga de generar el html del botón que
     * utiliza en la interfaz
     * DM - 2013-09-24
     * */
    static function generarHtmlBoton($datos) {
        global $_PATH_IMAGENES;

        $datos['tipo_boton'] = $datos['tipo_boton'] == '' ? 'submit' : $datos['tipo_boton'];

        $html_boton = '
        <input ' . $datos['atributos_boton'] . ' ' . $datos['accion_boton'] . ' class="btn btn-sm btn-white" type="' . $datos['tipo_boton'] . '" value="' . $datos['label_boton'] . '">
        ';

        return $html_boton;
    }

//Fin de generarHtmlBoton

    /**
     * Función que se encarga de generar una ventana emergente de
     * manera automatica
     * DM - 2013-10-30
     * */
    function generarPopupAutomatico($datos) {
        global $_PATH_IMAGENES, $idi_despliegue;

        $this->_contenido_popup_automatico = '
          <div id="container">
           	<div id="light_popup" class="white_content">
          		<table border="0" width="100%">
          			<tr>
          				<td class="descripcion_funcionalidad_ligthbox">' . $idi_despliegue['label_informacion'] . '</td>
          				<td width="60%">&nbsp</td>
          				<td width="20"><img src="' . $_PATH_IMAGENES . '/cerrar_ligthbox.png" onclick = "document.getElementById(\'light_popup\').style.display=\'none\';document.getElementById(\'fade_popup\').style.display=\'none\'"></td>
          			</tr>
          			<tr>
          				<td align="right"><img src="' . $_PATH_IMAGENES . '/linea_gris.png" width="100%" height="1"></td>
          				<td colspan="2"></td>
          			</tr>
          			<tr>
          				<td colspan="3">' . $datos['contenido'] . '</td>
          			</tr>
          		</table>
          	</div>
          </div>
          <div id="fade_popup" class="black_overlay"></div>
          <script language="javascript" >
          var posicion_scroll = $(window).scrollTop() + 200;
          $(\'#light_popup\').show();
          $(\'#light_popup\').css(\'top\',posicion_scroll+\'px\');

          var ancho_ventana = $(window).width();
          var ancho_contenido = $(\'#light_popup\').width();
          var espacio_left = (ancho_ventana - ancho_contenido)/2;
          $(\'#light_popup\').css(\'left\',espacio_left+\'px\');
          $(\'#fade_popup\').show();
          </script>
        ';
    }

//Fin de generarPopupAutomatico()

    /**
     * Se encarga de asignar los estilos a una plantilla html
     * teniendo en cuenta un archivo CSS en donde se declararon los
     * estilos
     * DM - 2014-05-27
     * * */
    static function asignarEstilosDesdeArchivo($datos) {
        //asigna el contenido de la plantilla
        $contenido = $datos['contenido'];

        //obtiene el contenido del archivo css
        $contenido_css = Archivos::obtenerContenidoArchivo($datos['ubicacion_archivo']);

        //elimina los saltos de linea del css
        $contenido_css = str_replace("\r\n", "", $contenido_css);
        $contenido_css = str_replace("\n", "", $contenido_css);

        $contenido = str_replace('class ="', 'class="', $contenido);
        $contenido = str_replace('class = "', 'class="', $contenido);
        $contenido = str_replace('class= "', 'class="', $contenido);

        //Obtiene las clases declaradas en la plantilla para obtener del css, lo hace
        //a partir del texto class="nombre_clase"
        $datos_token = array('plantilla' => $contenido,
            'token_prefijo' => 'class="',
            'token_sufijo' => '"'
        );
        $lista_clases = obtenerTokensPlantilla($datos_token);

        //de acuerdo a los nombres de las clases, busca las declaraciones en el css
        //y extra el contenido declarado para cada clase
        foreach ($lista_clases as $clase) {

            $sublista_clases = preg_split("/,/", $clase);

            $contenido_clase = '';
            foreach ($sublista_clases as $subclase) {
                $subclase = trim($subclase);
                if ($subclase == "") {
                    continue;
                }//Fin de if( $subclase == "" )
                //Obtiene las clases declaradas en la plantilla para obtener del css
                $datos_token = array('plantilla' => $contenido_css,
                    'token_prefijo' => '.' . $subclase . '{',
                    'token_sufijo' => '}',
                    'omitir_validacion' => 1
                );
                $lista_contenido = obtenerTokensPlantillaCSS($datos_token);

                //Obtiene el contenido de la clase css
                $contenido_clase_tmp = $lista_contenido[0];

                $contenido_clase_tmp = trim($contenido_clase_tmp);
                $contenido_clase .= str_replace("\r\n", "", $contenido_clase_tmp);
            }//Fin de foreach($sublista_clases as $subclase)
            //reemplaza la declaración de la clase por el codigo css
            $contenido = str_replace('class="' . $clase . '"', 'style="' . $contenido_clase . '"', $contenido);
        }//Fin de foreach($lista_clases as $clase)

        return $contenido;
    }

//Fin de asignarEstilosDesdeArchivo

    /**
     * Se encarga de obtener un solo contenido css con la
     * lista de archivos que se envia mediante un arreglo
     * DM - 2014-10-10
     * */
    static function generarContenidoCSSExternos($datos) {

        $lista_archivos_css = $datos['lista_archivos_css'];
        $lista_texto_intercambio = $datos['lista_texto_intercambio_contenidos'];

        $lista_texto_intercambio = is_array($lista_texto_intercambio) ? $lista_texto_intercambio : array();

        $contenido_css_general = '<style type="text/css">';

        foreach ($lista_archivos_css as $archivo_css) {
            $contenido_css = Archivos::obtenerContenidoArchivo($archivo_css);
            $contenido_css = str_replace("\r\n", "", $contenido_css);
            $contenido_css = str_replace("\n", "", $contenido_css);

            $contenido_css = str_replace("}", "} ", $contenido_css);

            //Elimina todos los comentarios que tengan los css
            $pos_inicial = strpos($contenido_css, '/*');
            //Mientras existan los comentarios busca las posiciones para eliminarlos
            while ($pos_inicial !== false) {
                $pos_final = strpos($contenido_css, '*/');

                //Obtiene el contenido desde la posicion cero hasta donde se encuentra el primer comentario
                $contenido_inicial = substr($contenido_css, 0, $pos_inicial);

                //Obtiene el contenido final despues del primer comentario
                $contenido_final = substr($contenido_css, $pos_final + 2, strlen($contenido_css));

                //Genera un nuevo contenido sin los comentarios
                $contenido_css = $contenido_inicial . $contenido_final;

                //verifica si aun quedan comentarios
                $pos_inicial = strpos($contenido_css, '/*');
            }//Fin de while( $pos_inicial !== false)


            foreach ($lista_texto_intercambio as $valor_texto_intercambio) {
                $contenido_css = str_replace($valor_texto_intercambio['base'], $valor_texto_intercambio['destino'], $contenido_css);
            }//Fin de foreach($lista_texto_intercambio as $valor_texto_intercambio)

            $contenido_css_general .= $contenido_css;
        }//Fin de foreach($lista_css as $css)

        $contenido_css_general .= '</style>';

        return $contenido_css_general;
    }

//Fin generarContenidoCSSExternos

    /**
     * Funcion que genera el calendario de rangos general para los informes o  donde sea necesario
     * */
    function obtenerCalendarioGeneral($tipo_calendario, $fecha_i, $fecha_f) {
        $fecha_ini = str_replace("-", "/", $fecha_i);
        $fecha_fin = str_replace("-", "/", $fecha_f);
        switch ($tipo_calendario) {
            case 1:
                $calendario = '
                    <form action="" name="formejemplo">
                        <div class="input-group calendario-general" >
                           <span class="input-group-addon">
                               <i class="fa fa-calendar"></i>
                           </span>
                           <input date-range-picker class="form-control date-picker" readonly
                                   type="text" name="fecha_rango_general" id="fecha_rango_general"
                                   value="' . $fecha_ini . ' - ' . $fecha_fin . '"/>
                        </div>
                    </form>';
                break;
        }
        return $calendario;
    }


    function mostrarPasosLabels($seleccionado){
        global $idi_despliegue;
        if ($seleccionado == 1){
        //JMM-2017-08-18 - Desarrollo #10555
        $pasos = '<div class="row">
                    <div class="center" style="margin-top:18px;">
                        <div class="btn-group" >
                            <span class="badge badge-primary labels-pasos" id="label_paso1" >';
            $pasos .= $idi_despliegue['label_seleccionar_contactos'] . '</span>
                            <span class="badge badge-white labels-pasos" id="label_paso2" >';
            $pasos .= $idi_despliegue['label_crear_envios'] . '</span>
                            <span class="badge badge-white labels-pasos" id="label_paso3" >';
            $pasos .= $idi_despliegue['label_selecciona_plantilla'] . '</span>
                        </div>
                    </div>
                </div>';
        }else if ($seleccionado == 3){
            $pasos = '<div class="row">
                    <div class="col-md-12 center" style="margin-top:18px;">
                        <div class="btn-group" >
                            <span class="badge badge-white labels-pasos" id="label_paso1" >';
            $pasos .= $idi_despliegue['label_seleccionar_contactos'] . '</span>
                            <span class="badge badge-white labels-pasos" id="label_paso2" >';
            $pasos .= $idi_despliegue['label_crear_envios'] . '</span>
                            <span class="badge badge-primary labels-pasos" id="label_paso3" >';
            $pasos .= $idi_despliegue['label_selecciona_plantilla'] . '</span>
                        </div>
                    </div>
                </div>';
        }

        return $pasos;
    }

    /**
     * Llama las campanias según su genero para mostrarlas en HQ
     * OR 2015-12-29
     * */
    function mostrarCampania($datos) {
        global $_obj_database, $_obj_interfaz, $_obj_campanias, $_PATH_WEB;

        $idioma = isset($datos[idioma]) ? $datos[idioma] : $_SESSION[idi_id];

        //Si no llegó el idioma, se llama por defecto el que tiene definido en HQ
        if ($idioma == '') {
            $info_establecimiento = $_obj_database->obtenerRegistro("establecimiento", "est_id ='" . $datos['est_id'] . "' ");
            $idioma = $info_establecimiento['est_idi_id'];
        }

        //OR 2016-01-28 Si llegara el idioma portugués BR se toma como PT
        if ($idioma == 187) {
            $idioma = 7;
        }

        $idiomas_activos = array(1, 2, 7);
        //Si no está el idioma en los idiomas disponibles, se asigna otro por defecto ingles
        if (!in_array($idioma, $idiomas_activos)) {
            $idioma = 2;
        }

        //DM - 2016-02-26
        //Valida si el usuario actual es root usando la cuenta del hotel
        $es_root_usando_hotel = isset($_SESSION['usuario_root_id']);

        $campo_base = '';
        $campo_nombre = '';
        $campo_activo = '';
        $origen_click = '';

        switch ($datos['tipo_campania']) {
            case 'inapp':
                //Si en la sesión ya se había mostrado una campaña, no se vuelve a mostrar el modal
                if ($_SESSION[mostrar_campania] == 1) {
                    return array();
                }

                $campo_base = 'cam_inapp_contenido';
                $campo_nombre = 'contenido_inapp';
                $campo_activo = 'ccam_inapp_activo';
                $origen_click = 'inapp';

                //DM - 2016-01-12
                //Consulta los parámetros de configuración de despliegue
                $sql_config = "SELECT *
                                FROM configuracion_general
                                WHERE cog_codigo IN ('latencia_inapp','latencia_campania_inapp')
                                ";
                $lista_configuracion = $_obj_database->obtenerRegistrosAsociativosPorClave($sql_config, "cog_codigo");

                //para el despliegue en la aplicación
                $latencia_inapp = $lista_configuracion['latencia_inapp']['cog_valor'];
                //para el despliegue de la campaña
                $latencia_campania_inapp = $lista_configuracion['latencia_campania_inapp']['cog_valor'];

                $registro_impacto = $_obj_database->obtenerRegistro("reporte_impacto_campania", "
                                                ric_usu_id = " . $_SESSION['usu_id'] . "
                                                AND ric_tipo_impacto = '" . $datos['tipo_campania'] . "'
                                                AND ric_cam_id > 0
                                                ORDER BY ric_fecha DESC
                                                ");

                //Obtiene el time de despliegue de la ultima alerta
                $time_registro = strtotime($registro_impacto['ric_fecha']);

                //agrega al tiempo la cantidad de dias en los cuales puede mostrar otra vez otra alerta
                $time_registro_prox_despliegue = strtotime('+' . $latencia_inapp . ' day', $time_registro);

                //Obtiene el time a partir de la primera hora del día
                $time_registro_prox_despliegue = strtotime(date('Y-m-d 00:00:00', $time_registro_prox_despliegue));

                //Si el time actual es menor al time en el que debe mostrar la prox alerta entonces
                //no muestra nada
                if (!$_SESSION['usuario_root_id'] && strtotime(date('Y-m-d 00:00:00')) < $time_registro_prox_despliegue) {
                    //OR 2016-01-28 indico que al menos se mostró una vez o que registro el impacto
                    $_SESSION[mostrar_campania] = 1;

                    $datos['campania_id'] = 0;
                    $this->registrarImpacto($datos);
                    return array();
                }//Fin if( time() < $time_registro_prox_despliegue )
                //Obtiene la fecha para cargar todas las campañas que se han mostrado en ese tiempo
                $time_mostrar_campanias = strtotime('-' . $latencia_campania_inapp . ' day');

                //Obtiene todas las campañas que a partir de los días de latencia
                //se han mostrado para ese usuario, esas campañas no se deben mostrar al usuario
                //hasta que no pasen los dias de latencia
                $sql_registros_campanias = "SELECT ric_cam_id
                                            FROM reporte_impacto_campania
                                            WHERE ric_usu_id = " . $_SESSION['usu_id'] . "
                                                AND ric_tipo_impacto = '" . $datos['tipo_campania'] . "'
                                                AND ric_fecha >= '" . date('Y-m-d 00:00:00', $time_mostrar_campanias) . "'
                                                AND ric_cam_id > 0
                                            ";

                $lista_campanias_id = $_obj_database->obtenerRegistrosAsociativosPorClave($sql_registros_campanias, "ric_cam_id");

                $condicion_campanias = count($lista_campanias_id) > 0 ? "AND ccam_id NOT IN ( " . implode(",", array_keys($lista_campanias_id)) . ")" : "";
                break;
            case 'header':
                $campo_base = 'cam_header_contenido';
                $campo_nombre = 'contenido_header';
                $campo_activo = 'ccam_header_activo';
                $origen_click = 'header';
                break;
            case 'landing':
                $campo_base = 'cam_landing_contenido';
                $campo_nombre = 'contenido_landing';
                $campo_activo = 'ccam_landing_activo';
                break;
        }//Fin de switch($datos['tipo_campania'])
        //OR 2016-01-28 indico que al menos se mostró una vez
        $_SESSION[mostrar_campania] = 1;

        if ($datos['camp_id'] != '') {
            $condicion_campanias .= "AND ccam_id = " . $datos['camp_id'];
        }

        $sql = "SELECT ccam_titulo_campania AS titulo,
                          ccam_id AS id_campania,
                         cam_idi_id AS idioma,
                         ccam_emails_notificacion_header AS emails_notificar,
                         " . $campo_base . "  AS " . $campo_nombre . "
                  FROM campania,campania_idioma
                  WHERE " . $campo_activo . " = 1
                  AND ccam_id = cam_ccam_id
                  AND cam_idi_id = " . $idioma . "
                  " . $condicion_campanias
        ;

        $contenido = $_obj_database->obtenerRegistrosAsociativosPorClave($sql, 'id_campania');

        //Si no hay registros no continua
        if (count($contenido) == 0) {
            //DM - 2016-03-01
            //En caso de no tener disponible el contenido en el idioma del establecimiento
            //lo solicita en ingles, siempre y cuando el idioma no sea ingles
            if ($idioma != 2) {
                $sql = "SELECT ccam_titulo_campania AS titulo,
                                  ccam_id AS id_campania,
                                 cam_idi_id AS idioma,
                                 ccam_emails_notificacion_header AS emails_notificar,
                                 " . $campo_base . "  AS " . $campo_nombre . "
                          FROM campania,campania_idioma
                          WHERE " . $campo_activo . " = 1
                          AND ccam_id = cam_ccam_id
                          AND cam_idi_id = 2
                          " . $condicion_campanias;

                $contenido = $_obj_database->obtenerRegistrosAsociativosPorClave($sql, 'id_campania');
            }//Fin deif( $idioma != 2 )
            //Si no hay contenido para el idioma del establecimiento o para ingles
            //consulta español que siempre sera obligatorio cuando se ingresan los contenidos
            if (count($contenido) == 0) {
                $sql = "SELECT ccam_titulo_campania AS titulo,
                                  ccam_id AS id_campania,
                                 cam_idi_id AS idioma,
                                 ccam_emails_notificacion_header AS emails_notificar,
                                 " . $campo_base . "  AS " . $campo_nombre . "
                          FROM campania,campania_idioma
                          WHERE " . $campo_activo . " = 1
                          AND ccam_id = cam_ccam_id
                          AND cam_idi_id = 1
                          " . $condicion_campanias
                ;

                $contenido = $_obj_database->obtenerRegistrosAsociativosPorClave($sql, 'id_campania');
            }//Fin de if( $idioma != 1)
        }//Fin de if( count($contenido) == 0 )

        if (is_numeric($datos['camp_id'])) {
            //Si viene el id de una campaña por url pues se utiliza esta para luego mostrar su contenido
            $datos['campania_id'] = $datos['camp_id'];
        } else if (count($contenido) > 0) {
            //OR 2016-01-04 optiene una campaña aleatoriamente
            $pos_random = array_rand($contenido, 1);
            $datos['campania_id'] = $pos_random;
        }

        $url = $_PATH_WEB . '/index.php?m=campanias&accion=ver_campania&camp_id=' . $datos['campania_id'];

        $est_id = $datos[est_id] == '' ? $_SESSION[est_id] : $datos[est_id];

        if ($origen_click != '') {
            $url = $url . '&source=' . $origen_click . '&tipo_campania=' . $datos[tipo_campania] . '&est_id=' . $est_id;
        }
        $contenido[$datos['campania_id']][contenido_inapp] = $_obj_campanias->introducirURL($contenido[$datos['campania_id']][contenido_inapp], $url);
        $contenido[$datos['campania_id']][contenido_header] = $_obj_campanias->introducirURL($contenido[$datos['campania_id']][contenido_header], $url);

        //OR 2016-01-06 Registra el despliegue de la campaña en BD
        $this->registrarImpacto($datos);


        return $contenido[$datos['campania_id']];
    }

//fin mostrarCampania($datos);

    /**
     * Se encarga de registrar en BD el despliegue de una campaña
     * OR 2016-01-06
     * */
    function registrarImpacto($datos) {
        global $_obj_database;

        //Si es el usuario root o si el usuario root entró como usuario hotel o cadena o si es preview
        // entonces no registra impacto
        if ($_SESSION[tipo_usuario] == 'R' || isset($_SESSION['usuario_root_id']) || $datos[preview] == 1) {
            return 1;
        }

        $datos[est_id] = $_SESSION['est_id'] == '' ? $datos[est_id] : $_SESSION['est_id'];

        //OR 2016-02-19 llamo info del establecimiento para registrar el país en el impacto
        $datos['opciones']['tipo_consulta'] = 'info_establecimiento';
        $datos_est = establecimientos::obtenerInfoEstablecimiento($datos[est_id], $datos);

        $campos = array('ric_tipo_impacto', 'ric_cam_id',
            'ric_est_id', 'ric_usu_id', 'ric_procedencia', 'ric_pai_id', 'ric_correo_destinatario');


        $datos['tabla'] = "reporte_impacto_campania";
        $datos['ric_tipo_impacto'] = $datos['tipo_campania'];
        $datos['ric_cam_id'] = $datos['campania_id'];
        $datos['ric_est_id'] = $datos['est_id'];
        $datos['ric_procedencia'] = $datos['source'];
        $datos['ric_pai_id'] = $datos_est['esp_pai_id'];
        $datos['ric_correo_destinatario'] = '';
        //DM - 2016-02-16
        //Para los header no se guarda el usuario
        $datos['ric_usu_id'] = $_SESSION['usu_id'];

        if ($datos['tipo_campania'] == 'header') {
            $datos['ric_usu_id'] = 0;

            if (is_array($datos['correo_usuario']) && sizeof($datos['correo_usuario']) > 0) {
                foreach ($datos['correo_usuario'] as $key => $destinatario) {
                    if ($destinatario != '') {
                        $datos['ric_correo_destinatario'] = $destinatario;
                        $_obj_database->insertarDatosTabla($datos, $campos);
                    }
                }
            }
        }//Fin de if( $datos['tipo_campania'] == 'header' )
        else {
            $_obj_database->insertarDatosTabla($datos, $campos);
        }

        if ($_obj_database->obtenerNumeroFilasAfectadas() == 0) {
            return -1003;
        }
        return 1;
    }//fin function registrarImpacto($datos)

    /**
     * Se encarga de generar el contenido tipo json y hacer la impresión
     * en pantalla para que pueda ser enviado mediante ajax
     * DM - 2016-09-28
     **/
    function asignarContenidoAjaxJson($datos)
    {
        global $_obj_database,$_db_obj;
        
        $datos['log_nombre_proceso_actual'] = "asignarContenidoAjaxJson";
        $datos['log_nombre_subproceso_actual'] = "";
        $datos['log_termina_proceso'] = 0;
        $datos = logProceso($datos);
        $datos_log = $datos;
        
        $datos_json = json_encode($datos);
        print($datos_json);

        $_db_obj['see']->desconectar();
        $_obj_database->desconectar();
        
        //DM - 2017-11-21 issue [10936]
        //Fin de registro de log
        $datos_log['log_nombre_proceso_actual'] = "asignarContenidoAjaxJson";
        $datos_log['log_nombre_subproceso_actual'] = "";
        $datos_log['log_termina_proceso'] = 1;
        logProceso($datos_log);
        
        $datos_log['log_nombre_proceso_actual'] = "Index";
        $datos_log['log_nombre_subproceso_actual'] = "";
        $datos_log['log_termina_proceso'] = 1;
        logProceso($datos_log);
        exit;
    }//Fin de asignarContenidoAjaxJson

    /**
     * Se encarga de hacer la impresión
     * en pantalla para que pueda ser enviado mediante ajax
     * DM - 2017-02-08
     **/
    function asignarContenidoAjax($contenido)
    {
        global $_obj_database,$_db_obj;
        
        $datos['log_nombre_proceso_actual'] = "asignarContenidoAjax";
        $datos['log_nombre_subproceso_actual'] = "";
        $datos['log_termina_proceso'] = 0;
        $datos = logProceso($datos);
        $datos_log = $datos;
        
        print($contenido);

        $_db_obj['see']->desconectar();
        $_obj_database->desconectar();
        
        //DM - 2017-11-21 issue [10936]
        //Fin de registro de log
        $datos_log['log_nombre_proceso_actual'] = "asignarContenidoAjax";
        $datos_log['log_nombre_subproceso_actual'] = "";
        $datos_log['log_termina_proceso'] = 1;
        logProceso($datos_log);
        
        $datos_log['log_nombre_proceso_actual'] = "Index";
        $datos_log['log_nombre_subproceso_actual'] = "";
        $datos_log['log_termina_proceso'] = 1;
        logProceso($datos_log);
        exit;
    }//Fin de asignarContenidoAjaxJson    
    

}//class
?>
