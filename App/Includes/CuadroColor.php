<?php
//DM - 2014-02-18

$codigo = $_GET['c']; 
$valor_1 = hexdec( substr($codigo, 0, 2) ); 
$valor_2 = hexdec( substr($codigo, 2, 2) );
$valor_3 = hexdec( substr($codigo, 4, 2) );

// Crear una imagen de 200 x 200
$lienzo = imagecreatetruecolor(6, 6);
// Asignar colores
$rosa = imagecolorallocate($lienzo, $valor_1, $valor_2, $valor_3);

imagefilledrectangle($lienzo, 0, 0, 5, 5, $rosa); 

// Imprimir y liberar memoria
header('Content-Type: image/png');

imagepng($lienzo);
imagedestroy($lienzo);
?>
