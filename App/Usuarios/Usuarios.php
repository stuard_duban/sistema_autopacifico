<?php

/**
 * 	Nombre: Usuarios.php
 * 	Descripción: Maneja lo referente a operaciones basica de un Usuario del Sistema
 */
class Usuarios {

    var $_plantillas = "";

    function Usuarios() {
        global $_PATH_SERVIDOR;

        $this->_plantillas = $_PATH_SERVIDOR . "usuarios/Plantillas";
    }

//Fin de Usuarios()

    function abrirMsgSoporte($datos) {
        global $_PATH_IMAGENES, $_PATH_WEB, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/msg_soporte.html");

        Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);

        Interfaz::asignarToken("btn_cerrar", $idi_despliegue['btn_cerrar'], $contenido);

        return $contenido;
    }

//Fin de abrirMsgSoporte()

    function abrirFormularioIngformacion($datos) {
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_informacion.html");

        $boton_login = "iniciar-sesion.jpg";
        $boton_enviar = "enviar.jpg";
        $boton_enviar_2 = "enviar02.jpg";


        if ($_SESSION['idi_id'] == 2) {
            $boton_login = "iniciar-sesion-ingles.jpg";
            $boton_enviar = "enviar-ingles.jpg";
            $boton_enviar_2 = "enviar02-ingles.jpg";
        }
        Interfaz::asignarToken("boton_login", $boton_login, $contenido);
        Interfaz::asignarToken("boton_enviar", $boton_enviar, $contenido);
        Interfaz::asignarToken("boton_enviar_2", $boton_enviar_2, $contenido);



        Interfaz::asignarToken("label_usuario", $idi_despliegue['label_usuario'], $contenido);
        Interfaz::asignarToken("label_contrasenha", $idi_despliegue['label_contrasenha'], $contenido);
        Interfaz::asignarToken("label_olvido_contrasenha", $idi_despliegue['label_olvido_contrasenha'], $contenido);

        Interfaz::asignarToken("label_nombre_establecimiento", $idi_despliegue['label_nombre_establecimiento'], $contenido);
        Interfaz::asignarToken("label_nombre", $idi_despliegue['label_nombre'], $contenido);
        Interfaz::asignarToken("label_telefono", $idi_despliegue['label_telefono'], $contenido);
        Interfaz::asignarToken("label_correo", $idi_despliegue['label_correo'], $contenido);
        Interfaz::asignarToken("label_pms", $idi_despliegue['label_pms'], $contenido);
        Interfaz::asignarToken("label_captcha", $idi_despliegue['label_captcha'], $contenido);



        Interfaz::asignarToken("usu_login", $datos['usu_login'], $contenido);
        Interfaz::asignarToken("inf_nombre_establecimiento", $datos['inf_nombre_establecimiento'], $contenido);
        Interfaz::asignarToken("inf_nombre", $datos['inf_nombre'], $contenido);
        Interfaz::asignarToken("inf_correo", $datos['inf_correo'], $contenido);
        Interfaz::asignarToken("inf_telefono", $datos['inf_telefono'], $contenido);
        Interfaz::asignarToken("inf_pms", $datos['inf_pms'], $contenido);
        Interfaz::asignarToken("imagenes", $_PATH_IMAGENES, $contenido);
        Interfaz::asignarToken("path_web", $_PATH_WEB, $contenido);

        Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);

        return $contenido;
    }

//Fin de abrirFormularioIngformacion()

    function abrirFormularioAutenticacion($datos) {
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        //$boton_login = "iniciar-sesion.jpg";
        $boton_login = "Ingresar";
        $boton_informacion = "solicitar01.jpg";
        $boton_informacion_2 = "solicitar02.jpg";
        $video = '<object width="640" height="505"><param name="movie" value="http://www.youtube.com/v/MErmdd8X4hU&hl=es_ES&fs=1&"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/MErmdd8X4hU&hl=es_ES&fs=1&" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="640" height="505"></embed></object>';


        if ($_SESSION['idi_id'] == 2) {
            // $boton_login = "iniciar-sesion-ingles.jpg";
            $boton_login = "Login";
            $boton_informacion = "solicitar01-ingles.jpg";
            $boton_informacion_2 = "solicitar02-ingles.jpg";
            $video = '<object width="640" height="505"><param name="movie" value="http://www.youtube.com/v/MErmdd8X4hU&hl=es_ES&fs=1&"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/MErmdd8X4hU&hl=es_ES&fs=1&" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="640" height="505"></embed></object>';
        }

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_autenticacion.html");



        Interfaz::asignarToken("boton_login", $boton_login, $contenido);
        Interfaz::asignarToken("boton_informacion", $boton_informacion, $contenido);
        Interfaz::asignarToken("boton_informacion_2", $boton_informacion_2, $contenido);



        Interfaz::asignarToken("usu_login", $datos['usu_login'], $contenido);
        Interfaz::asignarToken("imagenes", $_PATH_IMAGENES, $contenido);

        $msg_inicio = str_replace("<br><br><br>", "</p><p>", $idi_despliegue['label_msg_inicio']);

        Interfaz::asignarToken("label_usuario", $idi_despliegue['label_usuario'], $contenido);
        Interfaz::asignarToken("label_contrasenha", $idi_despliegue['label_contrasenha'], $contenido);
        Interfaz::asignarToken("label_olvido_contrasenha", $idi_despliegue['label_olvido_contrasenha'], $contenido);
        Interfaz::asignarToken("label_msg_inicio", $msg_inicio, $contenido);



        // Interfaz::asignarToken("video",$video,$contenido);

        Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);

        return $contenido;
    }

//Fin de abrirFormularioAutenticacion()

    function abrirContenidoInicio() {
        $contenido = "<div class='bienvenido'>
                                <p>
                                    Bienvenido <b>" . $_SESSION['usu_nombre'] . "</b>
                                </p>
                              </div>";

        return $contenido;
    }

//abrirContenidoInicio()

    function abrirFormularioOlvidoClave($datos) {
        global $_PATH_IMAGENES, $_PATH_WEB, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_olvido_clave.html");

        Interfaz::asignarToken("usu_login", $datos['usu_login'], $contenido);
        Interfaz::asignarToken("usu_correo", $datos['usu_correo'], $contenido);

        Interfaz::asignarToken("label_usuario", $idi_despliegue['label_usuario'], $contenido);
        Interfaz::asignarToken("label_olvido_contrasenha", $idi_despliegue['label_olvido_contrasenha'], $contenido);
        Interfaz::asignarToken("label_volver", $idi_despliegue['volver'], $contenido);
        Interfaz::asignarToken("label_correo", $idi_despliegue['label_correo'], $contenido);
        Interfaz::asignarToken("label_enviarme_contrasenha", $idi_despliegue['label_enviarme_contrasenha'], $contenido);
        Interfaz::asignarToken("label_msg_olvido_contrasenha", $idi_despliegue['label_msg_olvido_contrasenha'], $contenido);

        Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);

        return $contenido;
    }

//Fin de abrirFormularioOlvidoClave()

    function obtenerCorreoOlvidoClave($datos, $valores) {
        global $_PATH_WEB, $_PATH_IMAGENES;

        $valor = $valores[0];

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/correo_olvido_clave.html");

        //MAOH - 18 Abr 2012 - Cambio a UTF 8
        Interfaz::asignarToken("codificacion", "UTF-8", $contenido);

        //Si el idioma seleccionado es Chino(13), Ruso(14) o Ucraniano(15) para que reconozca caracteres extraÃ±os
        /* if (($_SESSION['idi_id'] == 10) || ($_SESSION['idi_id'] == 13) || ($_SESSION['idi_id'] == 14) || ($_SESSION['idi_id'] == 15) || ($_SESSION['idi_id'] == 9) || ($_SESSION['idi_id'] == 26))
          {
          Interfaz::asignarToken("codificacion",'UTF-8',$contenido);
          }
          else //Los otros idiomas que no tienen caracteres extraÃ±os
          {
          Interfaz::asignarToken("codificacion",'iso-8859-1',$contenido);
          } */

        Interfaz::asignarToken("usuario", $valor['usu_nombre'], $contenido);

        Interfaz::asignarToken("clave", $valor['usu_clave'], $contenido);
        Interfaz::asignarToken("usu_login", $datos['usu_login'], $contenido);

        Interfaz::asignarToken("path_web", $_PATH_WEB, $contenido);
        Interfaz::asignarToken("imagenes", $_PATH_IMAGENES, $contenido);

        return $contenido;
    }

//fin de obtenerCorreoOlvidoClave()

    function abrirFormularioCambiarDatos($datos, $valores) {
        global $_PATH_IMAGENES, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_cambiar_datos.html");

        $valor = $valores[0];

        Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);
        Interfaz::asignarToken("imagenes", $_PATH_IMAGENES, $contenido);

        Interfaz::asignarToken("texto_titulo", $idi_despliegue['texto_titulo'], $contenido);
        Interfaz::asignarToken("texto_usuario", $idi_despliegue['texto_usuario'], $contenido);
        Interfaz::asignarToken("texto_correo", $idi_despliegue['texto_correo'], $contenido);
        Interfaz::asignarToken("texto_contrasenha_actual", $idi_despliegue['texto_contrasenha_actual'], $contenido);
        Interfaz::asignarToken("texto_contrasenha_nueva", $idi_despliegue['texto_contrasenha_nueva'], $contenido);
        Interfaz::asignarToken("texto_contrasenha_confirmacion", $idi_despliegue['texto_contrasenha_confirmacion'], $contenido);
        Interfaz::asignarToken("btn_guardar", $idi_despliegue['btn_guardar'], $contenido);

        Interfaz::asignarToken("usu_login", $valor['usu_login'], $contenido);
        Interfaz::asignarToken("usu_correo", $valor['usu_correo'], $contenido);
        Interfaz::asignarToken("usu_id", $valor['usu_id'], $contenido);

        return $contenido;
    }

//Fin de abrirFormularioCambiarClave()

    function abrirFormularioSoporte($datos) {
        global $_PATH_IMAGENES, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_soporte.html");

        Interfaz::asignarToken("path_imagenes", $_PATH_IMAGENES, $contenido);

        Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);

        Interfaz::asignarToken("sop_nombre", $_SESSION['usu_nombre'], $contenido);
        Interfaz::asignarToken("sop_fecha", date("Y-m-d H:i:s"), $contenido);
        Interfaz::asignarToken("sop_fecha_label", date("d-m-Y H:i:s"), $contenido);

        //MAOH - 25 Sep 2012 - Incidencia 1761
        //Se quita el captcha del formulario
        //Interfaz::asignarToken("label_captcha",$idi_despliegue[label_captcha],$contenido);

        if ($datos['sop_correo'] == "") {
            $datos['sop_correo'] = $_SESSION['usu_correo'];
        }
        Interfaz::asignarToken("sop_correo", $datos['sop_correo'], $contenido);

        Interfaz::asignarToken("sop_comentario", $datos['sop_comentario'], $contenido);

        Interfaz::asignarToken("soporte_usuario", $idi_despliegue['soporte_usuario'], $contenido);
        Interfaz::asignarToken("soporte_fecha_solicitud", $idi_despliegue['soporte_fecha_solicitud'], $contenido);
        Interfaz::asignarToken("soporte_responder_correo", $idi_despliegue['soporte_responder_correo'], $contenido);
        Interfaz::asignarToken("soporte_archivo", $idi_despliegue['soporte_archivo'], $contenido);
        Interfaz::asignarToken("soporte_comentario", $idi_despliegue['soporte_comentario'], $contenido);
        Interfaz::asignarToken("btn_cerrar", $idi_despliegue['btn_cerrar'], $contenido);
        Interfaz::asignarToken("btn_enviar", $idi_despliegue['btn_enviar'], $contenido);

        return $contenido;
    }

//Fin de abrirFormularioSoporte()

    function abrirFormularioContacto($datos) {
        global $_PATH_IMAGENES;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_contacto.html");

        Interfaz::asignarToken("path_imagenes", $_PATH_IMAGENES, $contenido);

        Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);

        return $contenido;
    }

//Fin de abrirFormularioContacto()

    function abrirFormularioRegistro($datos) {
        global $_PATH_IMAGENES;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_registro.html");

        Interfaz::asignarToken("path_imagenes", $_PATH_IMAGENES, $contenido);

        Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);

        Interfaz::asignarToken("usu_nombre", $datos['usu_nombre'], $contenido);
        Interfaz::asignarToken("usu_identificacion", $datos['usu_identificacion'], $contenido);
        Interfaz::asignarToken("usu_telefono", $datos['usu_telefono'], $contenido);
        Interfaz::asignarToken("usu_correo", $datos['usu_correo'], $contenido);
        Interfaz::asignarToken("usu_login", $datos['usu_login'], $contenido);

        return $contenido;
    }

//Fin de abrirFormularioRegistro()

    function abrirFormularioGestion($datos) {
        global $_PATH_IMAGENES, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/gestion_usuarios.html");
        if ($_SESSION[tipo_usuario] == "C") {//El usuario es de tipo cadena %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            $ids = array_keys($_SESSION[cadena_est]);
            $hotel = $_SESSION['cadena_est'][$ids[0]];
            $datos['est_id'] = $datos['est_id'] == "" ? $hotel['est_id'] : $datos['est_id'];
            $datos['adicionales'] = "onchange=\"seleccionHotelGestionUsuarios(this.value)\" ";
            $datos['atributos']['codigo'] = "est_id";
            $datos['atributos']['mostrar'] = "est_nombre";
            $datos['valor'] = $datos['est_id'];
            $datos['nombre_campo'] = "est_id";
            $datos['sql'] = "SELECT est_id , est_nombre
    						FROM establecimiento
                            WHERE est_estado = 1
                            AND est_cad_id  = '" . $_SESSION[cad_id] . "'
    						ORDER BY est_nombre ASC";
            //Se crea el selector para poder seleccionar el hotel a Gestionarle los usuarios o los usuarios de la cadena
            $hoteles = Herramientas::crearSelectHotelesGestionUsuarios($datos, $_SESSION['cad_nombre'], $_SESSION['cad_id']);
            $hotel_nombre = $_SESSION[cadena_est][$datos[est_id]][est_nombre];
        }//Fin de if( $_SESSION[tipo_usuario]=="C") el usuario es de tipo cadena %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        $botonAgregarUsuario = '';
        // Si tiene activa la funcionalidad 35 (Usuarios) puede crear nuevos usuarios dependientes de sus funcionalidadeso es el ROOT
        if ($this->verificarFuncionalidadActiva($_SESSION['usu_id'], 35) || $_SESSION['tipo_usuario'] == 'R') {
            $botonAgregarUsuario = '<input type="button" value="' . $idi_despliegue['nuevo_usuario'] . '" onclick="location.href=\'index.php?m=usuarios&accion=fm_ingresar\';"/>';
        }

        Interfaz::asignarToken("lista_hoteles", $hoteles, $contenido);
        Interfaz::asignarToken("nombre_hotel", $hotel_nombre, $contenido);
        Interfaz::asignarToken("path_imagenes", $_PATH_IMAGENES, $contenido);
        Interfaz::asignarToken("gestion_usuarios", $idi_despliegue['gestion_usuarios'], $contenido);
        Interfaz::asignarToken("texto_buscar", $idi_despliegue['texto_buscar'], $contenido);
        Interfaz::asignarToken("texto_busqueda", $datos['usu_busqueda'], $contenido);
        Interfaz::asignarToken("boton_nuevo_usuario", $botonAgregarUsuario, $contenido);
        Interfaz::asignarToken("buscar", $idi_despliegue['btn_buscar'], $contenido);
        Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);
        Interfaz::asignarToken("limite-consulta", $datos['limit'], $contenido);
        Interfaz::asignarToken("offset-consulta", $datos['offset'], $contenido);
        Interfaz::asignarToken("error_campo_vacio", $idi_despliegue['error_campo_vacio'], $contenido);

        return $contenido;
    }

//Fin de abrirFormularioGestion()

    function abrirFormularioIngresar($datos) {
        global $idi_despliegue, $_obj_database, $_obj_mensajes, $_PATH_IMAGENES;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_ingresar_usuario.html");

        if ($datos['usu_id_funcionalidades'] != '' && $datos['mensaje'] == '') {
            $opciones['ACCION'] = 6;
            $opciones['CADENA'] = $idi_despliegue['listado_funcionalidades'];
            $datos['mensaje'] = $_obj_mensajes->crearMensaje(1, $opciones);
        }

        Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);
        Interfaz::asignarToken("nombre", $idi_despliegue['label_nombre'], $contenido);
        Interfaz::asignarToken("usuario", $idi_despliegue['usuario'], $contenido);
        Interfaz::asignarToken("clave", $idi_despliegue['clave'], $contenido);
        Interfaz::asignarToken("valor_usu_clave", $datos['usu_clave'], $contenido);
        Interfaz::asignarToken("rpt_clave", $idi_despliegue['rpt_clave'], $contenido);
        Interfaz::asignarToken("valor_usu_clave_confirmacion", $datos['usu_clave_confirmacion'], $contenido);
        Interfaz::asignarToken("label_correo", $idi_despliegue['label_correo'], $contenido);
        Interfaz::asignarToken("tipo_usuario_select", $idi_despliegue['tipo_usuario_select'], $contenido);
        Interfaz::asignarToken("usu_nombre", $datos['usu_nombre'], $contenido);
        Interfaz::asignarToken("usu_apellido", $valor['usu_apellido'], $contenido);
        Interfaz::asignarToken("usu_cedula", $datos['usu_cedula'], $contenido);
        Interfaz::asignarToken("usu_correo", $datos['usu_correo'], $contenido);
        Interfaz::asignarToken("usu_login", $datos['usu_login'], $contenido);

        $datos['usu_dias_notificacion_tarea_expirar'] = isset($datos['usu_dias_notificacion_tarea_expirar']) ? $datos['usu_dias_notificacion_tarea_expirar'] : 4;
        Interfaz::asignarTokenDatos("usu_dias_notificacion_tarea_expirar", $datos, $contenido);
        Interfaz::asignarTokenDatos("label_dias_notificacion_vencimiento_tareas", $idi_despliegue, $contenido);

        //El root y los usuarios de cadena pueden crear otros usuarios de cadena y de hotel
        if ($_SESSION['tipo_usuario'] == 'R') {
            $restriccionPerfil = "";
        } else if ($_SESSION['tipo_usuario'] == 'C') {
            $restriccionPerfil = " AND (per_id = '6' OR per_id = '2')";
        }
        //Los usuarios de hotel solo pueden crear otros usuarios de hotel
        else {
            $restriccionPerfil = " AND per_id = '2'";
        }

        $onChangeTipoUsuario = ' onChange="seleccionTipoUsuarioNuevoUsuario()" ';
        //Se construye el selector de los tipo de usuario (Hotel o Cadena)
        $opcionesTipoUsuario['nombre_campo'] = "usu_per_id";
        $opcionesTipoUsuario['sql'] = "SELECT per_id , pid_nombre
								    FROM perfiles, perfiles_idioma
								    WHERE per_id = pid_per_id
                                    AND	pid_idi_id = '" . $_SESSION['idi_id'] . "'
								    $restriccionPerfil
								    ORDER BY per_nombre ASC";
        $opcionesTipoUsuario['atributos']['codigo'] = "per_id";
        $opcionesTipoUsuario['atributos']['mostrar'] = "pid_nombre";
        $opcionesTipoUsuario['valor'] = $datos['usu_per_id'];
        $opcionesTipoUsuario['adicionales'] = $onChangeTipoUsuario; //Aca va el evento Javascrip de cambiar el tipo de usuario
        $tipo_usuario = Herramientas::crearSelectFormaPersonalizada($opcionesTipoUsuario, $idi_despliegue['tipo_usuario_select']);
        //Fin Se construye el selector de los tipo de usuario (Hotel o Cadena)

        Interfaz::asignarToken("tipo_usuario", $tipo_usuario, $contenido);

        //Se construye el selector de las cadenas en caso que el usuario actual sea root y vaya a crear un nuevo usuario de cadena
        $listaCadenas = '';
        //El usuario actual es el root y seleccino crear un usuario de cadena, se debe crear el selector de las cadenas disponibles
        if ($_SESSION['tipo_usuario'] == 'R' && $datos['usu_per_id'] == '6') { //Puede crear un usuario de cadena pero debe seleccionar a que cadena se lo va a crear
            //Se construye la lista de las cadenas
            //pgs - 14/08/2012 - se añade filtro de la encuesta asociada
            $sql = "SELECT cad_id, cad_nombre
					FROM cadena
					WHERE cad_estado = '1'
                    AND cad_enc_id = '" . $_SESSION['enc_id'] . "'
					ORDER BY cad_nombre ASC";
            $resultadoCadenas = $_obj_database->obtenerRegistrosAsociativos($sql);
            $cantidadCadenas = $_obj_database->obtenerNumeroFilas();
            $listaCadenas = "<select name='cad_id' onChange='seleccionCadenaNuevoUsuario()'>";
            //Valor por defecto
            $listaCadenas .= "<option value='' align=center>- " . $idi_despliegue['seleccionar_cadena'] . " -</option>";
            for ($k = 0; $k < $cantidadCadenas; $k++) {
                $listaCadenas .= "<option ";
                if ($datos['cad_id'] == $resultadoCadenas[$k]['cad_id']) {
                    $listaCadenas .= ' selected ';
                }
                $listaCadenas .= "value='" . $resultadoCadenas[$k]['cad_id'] . "'>" . $resultadoCadenas[$k]['cad_nombre'] . "</option>";
            }//Fin de recorrer las cadenas encontradas
            $listaCadenas .= '</select>';
            $listaCadenas = '<tr>
								<td>' . $idi_despliegue['cadena'] . '</td>
								<td>' . $listaCadenas . '</td>
							</tr>';
        }//Fin es un usuario de cadena

        Interfaz::asignarToken("lista_cadenas", $listaCadenas, $contenido);

        //Fin Se construye el selector de las cadenas en caso que el usuario actual sea root y vaya a crear un nuevo usuario de cadena

        $listaHoteles = '';
        if ($_SESSION['tipo_usuario'] == 'C' || $_SESSION['tipo_usuario'] == 'R') { //Puede crear un usuario de hotel pero debe seleccionar a que hotel se lo va a crear
            if ($datos['usu_per_id'] == 2) { //Se va ingresar un usuario de hotel
                if ($_SESSION['tipo_usuario'] == 'C') { //Se restringue la lista de hoteles a los hoteles de la cadena
                    $restriccionHotelelesCadena = " and est_cad_id  = '" . $_SESSION['cad_id'] . "'  ";
                } else if ($_SESSION['tipo_usuario'] == 'R') { //No hay restriccion exhibe los hoteles activos
                    $restriccionHotelelesCadena = '';
                }
                //Se construye la lista de los hoteles
                $sql = "SELECT est_id , est_nombre
						FROM establecimiento
						WHERE est_estado = 1
						$restriccionHotelelesCadena
						ORDER BY est_nombre ASC";
                $resultadoHoteles = $_obj_database->obtenerRegistrosAsociativos($sql);
                $cantidadHoteles = $_obj_database->obtenerNumeroFilas();
                $listaHoteles = "<select name='est_id' onChange='seleccionHotelNuevoUsuario()'>";
                //Valor por defecto
                $listaHoteles .= "<option value='' align=center>- " . $idi_despliegue['seleccionar_hotel'] . " -</option>";
                for ($k = 0; $k < $cantidadHoteles; $k++) {
                    $listaHoteles .= "<option ";
                    if ($datos['est_id'] == $resultadoHoteles[$k]['est_id']) {
                        $listaHoteles .= ' selected ';
                    }
                    $listaHoteles .= "value='" . $resultadoHoteles[$k]['est_id'] . "'>" . $resultadoHoteles[$k]['est_nombre'] . "</option>";
                }//Fin de recorrer los hoteles encontrados
                $listaHoteles .= '</select>';
                $listaHoteles = '<tr>
									<td>' . $idi_despliegue['bench_hotel'] . '</td>
									<td>' . $listaHoteles . '</td>
								</tr>';
            }//Fin se va a ingresar un usuario de hotel
        } else {
            //pgs - 23/08/2012  - si es un usuario hotel el establecimiento se trae de la session
            if ($_SESSION['tipo_usuario'] == 'H') {
                $datos['est_id'] = $_SESSION['est_id'];
            }//Fin de if($_SESSION['tipo_usuario']=='C' )
        }//Fin es un usuario de cadena

        Interfaz::asignarToken("lista_hoteles", $listaHoteles, $contenido);


        $activarZonaCategoria = false;
        $primarioAsignado = '';

        //Ahora se deciden las restricciones para las categoria del nuevo usuario
        if ($datos['usu_per_id'] != '') { //Ya se selecciono el tipo de usuario (Hotel o Cadena)
            if ($_SESSION['tipo_usuario'] == 'R') { //Puede seleccionar usuarios de Hotel o Cadena
                if ($datos['usu_per_id'] == '6') { //Se va a crear un usuario de Cadena
                    //Se debe seleccionar la cadena a la que se le va a crear el usuario
                    if ($datos['cad_id'] != '') {
                        //Ya se selecciono la cadena, consultamos si ya tiene usuario primario de cadena asignado
                        $sqlUsuarioPrimario = "SELECT usu_id FROM usuario, usuario_cadena WHERE usu_id = uca_usu_id AND uca_cad_id = '" . $datos['cad_id'] . "' AND usu_usc_id = '1' AND usu_per_id = '6'";
                        $resultadoUsuarioPrimario = $_obj_database->obtenerRegistrosAsociativos($sqlUsuarioPrimario);
                        $cantidadUsuarioPrimario = $_obj_database->obtenerNumeroFilas();
                        if ($cantidadUsuarioPrimario == 0) { //Aun no se ha asignado usuario primario a la cadena
                            $restriccionCategoria = " AND usc_id ='1' "; //Debe crear el usuario primario
                            $primarioAsignado = '&nbsp;&nbsp;' . $idi_despliegue['primario_no_asignado'];
                            $datos['usc_codigo'] = 1;
                        } else { //Ya existe el usuario primario de la cadena, luego solo se pueden crear mas usuarios secundarios de cadena
                            $restriccionCategoria = " AND usc_id ='2' ";
                            $primarioAsignado = '&nbsp;&nbsp;' . $idi_despliegue['primario_asignado'];
                            $datos['usc_codigo'] = 2;
                        }
                        $activarZonaCategoria = true;
                    }//Fin ya se selecciono la cadena
                }//Fin Se va a crear un usuario de Cadena
                else if ($datos['usu_per_id'] == '2') { //Se va a crear un usuario de Hotel
                    //Se debe seleccionar el hotel a la que se le va a crear el usuario
                    if ($datos['est_id'] != '') {
                        //Ya se selecciono el hotel, consultamos si ya tiene usuario primario de hotel asignado
                        $sqlUsuarioPrimario = "SELECT usu_id FROM usuario, usuario_establecimiento WHERE usu_id = use_usuario_id AND use_establecimiento_id = '" . $datos['est_id'] . "' AND usu_usc_id = '1' AND usu_per_id = '2'";
                        $resultadoUsuarioPrimario = $_obj_database->obtenerRegistrosAsociativos($sqlUsuarioPrimario);
                        $cantidadUsuarioPrimario = $_obj_database->obtenerNumeroFilas();
                        if ($cantidadUsuarioPrimario == 0) { //Aun no se ha asignado usuario primario a el hotel
                            $restriccionCategoria = " AND usc_id ='1' "; //Se debe crear el usuario primario del hotel
                            $primarioAsignado = '&nbsp;&nbsp;' . $idi_despliegue['primario_no_asignado'];
                            $datos['usc_codigo'] = 1;
                        } else { //Ya existe el usuario primario del hotel, luego solo se pueden crear mas usuarios secundarios del hotel
                            $restriccionCategoria = " AND usc_id ='2' ";
                            $primarioAsignado = '&nbsp;&nbsp;' . $idi_despliegue['primario_asignado'];
                            $datos['usc_codigo'] = 2;
                        }
                        $activarZonaCategoria = true;
                    }//Fin ya se selecciono la cadena
                }//FIn Se va a crear un usuario de Hotel
            }//Fin es el usuario root
            else if ($_SESSION['tipo_usuario'] == 'C') { //Puede seleccionar usuarios de Hotel o Cadena
                $restriccionCategoria = " AND usc_id ='2' ";
                $activarZonaCategoria = true;
                $datos['usc_codigo'] = 2;
            }//Fin es un usuario de cadena
            else if ($_SESSION['tipo_usuario'] == 'H') { //Puede seleccionar usuarios de Hotel
                $restriccionCategoria = " AND usc_id ='2' ";
                $activarZonaCategoria = true;
                $datos['usc_codigo'] = 2;
            }//Fin es un usuario de hotel
        }//Fin ya se selecciono el tipo de usuario
        //Fin Ahora se deciden las restricciones para las categoria del nuevo usuario
        $zonaCategoria = '';
        if ($activarZonaCategoria) {
            //El select de las categorias de usuarios
            $opcionesCategoriaUsuario['nombre_campo'] = "usc_codigo";
            $opcionesCategoriaUsuario['sql'] = "select usc_id , uci_nombre
									from usuario_categoria, usuario_categoria_idioma
									where usc_id = uci_usc_id AND uci_idi_id = '" . $_SESSION['idi_id'] . "'
									" . $restriccionCategoria . " AND  usc_id != '0'
									order by usc_nombre ASC";
            $opcionesCategoriaUsuario['atributos']['codigo'] = "usc_id";
            $opcionesCategoriaUsuario['atributos']['mostrar'] = "uci_nombre";
            $opcionesCategoriaUsuario['valor'] = $datos['usc_codigo'];
            $opcionesCategoriaUsuario['adicionales'] = " onChange='seleccionCategoriaNuevoUsuario()' "; //Aca va el evento Javascrip de cambiar el Categoria de usuario

            $categoria_usuario = Herramientas::crearSelectFormaPersonalizada($opcionesCategoriaUsuario, $idi_despliegue['categoria']);

            $zonaCategoria = '<tr>
								<td>' . $idi_despliegue['categoria'] . '</td>
								<td>' . $categoria_usuario . $primarioAsignado . '</td>
							  </tr>';
        }//Fin se puede activar la zona de la categoria

        Interfaz::asignarToken("zona_categoria", $zonaCategoria, $contenido);

        if ($datos['usu_estado'] == 1 || is_null($datos['usu_estado'])) {
            $checked1 = "checked";
        } else {
            $checked2 = "checked";
        }


        $radios_estado_clientes = "<input type=radio name=usu_estado value=1 $checked1>Activo<br>
    								<input type=radio name=usu_estado value=0 $checked2>Bloqueado";
        Interfaz::asignarToken("estado_usuario", $radios_estado_clientes, $contenido);

        Interfaz::asignarToken("usu_id", $datos['usu_id'], $contenido);
        Interfaz::asignarToken("usu_nombre_cedula", $datos['usu_nombre_cedula'], $contenido);
        Interfaz::asignarToken("offset", $datos['offset'], $contenido);
        Interfaz::asignarToken("ordena", $datos['ordena'], $contenido);
        Interfaz::asignarToken("tipo_ordena", $datos['tipo_ordena'], $contenido);
        Interfaz::asignarToken("titulo", $idi_despliegue['nuevo_usuario'], $contenido);
        Interfaz::asignarToken("accion", "ingresar", $contenido);
        Interfaz::asignarToken("campo_readonly", "", $contenido);
        Interfaz::asignarToken("tipo_usuario_hidden", $_SESSION['tipo_usuario'], $contenido);
        Interfaz::asignarToken("ruta_imagenes", $_PATH_IMAGENES, $contenido);

        //Se construye la lista de los usuarios para copiar sus funcionalidades -------------------------------------------------------------

        $restriccionCategoria = '';
        $zonaFuncionalidades = '';

        //La lista de funcionalidades solo aparecera cuando ya se haya seleccionado el tipo de usuario a crear y la categoria
        if ($datos['usc_codigo'] != '') {
            //El usuario root pueder ver todos los usuarios
            if ($_SESSION['tipo_usuario'] == 'R') {
                if ($datos['usu_per_id'] == '6' && $datos['cad_id'] != '') { // Se va a crear un usuario de cadena, luego se debe consultar en la cadena seleccionada
                    $sql = "SELECT	usu_login, usu_id, usu_nombre
									FROM 	usuario, usuario_cadena
									WHERE 	usu_estado = 1 AND usu_id = uca_usu_id AND uca_cad_id = '" . $datos['cad_id'] . "'
									ORDER BY usu_login ASC";
                } else if ($datos['usu_per_id'] == '2' && $datos['est_id'] != '') { // Se va a crear un usuario de hotel, luego se debe consultar en el hotel seleccionado
                    $sql = "SELECT	usu_login, usu_id, usu_nombre
									FROM 	usuario, usuario_establecimiento
									WHERE 	usu_estado = 1 AND usu_id = use_usuario_id AND use_establecimiento_id = '" . $datos['est_id'] . "'
									ORDER BY usu_login ASC";
                } else {
                    $sql = "SELECT	usu_login, usu_id, usu_nombre
									FROM 	usuario
									WHERE 	usu_estado = 1 AND usu_per_id != '1' AND usu_per_id != '3' AND usu_per_id != '5' AND usu_per_id != '7'
									ORDER BY  usu_login ASC";
                }
                $restriccionPerfil = "";
            }
            //Los usuarios de cadena pueden ver otros usuarios de cadena y de hotel
            else if ($_SESSION['tipo_usuario'] == 'C') {
                if ($categoriaUsuarioActual == 1) {
                    $restriccionCategoria = "  AND (usu_usc_id = '1' OR usu_usc_id = '2') ";
                } else if ($categoriaUsuarioActual == 2) {
                    $restriccionCategoria = " AND usu_usc_id = '2' ";
                }

                if ($datos['usu_per_id'] == 2) { // Se va a crear un usuario de hotel, luego se debe consultar en el hotel seleccionado
                    $sql = "SELECT	usu_login, usu_id, usu_nombre
									FROM 	usuario, usuario_establecimiento
									WHERE 	usu_estado = 1 AND usu_id = use_usuario_id AND use_establecimiento_id = '" . $datos['est_id'] . "'
											" . $restriccionCategoria . "
										order by usu_login ASC";
                } else if ($datos['usu_per_id'] == 6) { //Se va crear un usuario de cadena, luego se debe consultar en usuarios de su cadena
                    $sql = "SELECT	usu_login, usu_id, usu_nombre
									FROM 	usuario, usuario_cadena
									WHERE 	usu_estado = 1 AND usu_id = uca_usu_id AND uca_cad_id = '" . $_SESSION['cad_id'] . "'
											" . $restriccionCategoria . "
										order by usu_login ASC";
                }
                $restriccionPerfil = " AND (usu_per_id = '6' OR usu_per_id = '2')";
            }
            //Los usuarios de hotel solo pueden ver otros usuarios de su hotel
            else {
                if ($categoriaUsuarioActual == 1) {
                    $restriccionCategoria = " AND (usu_usc_id = '1' OR usu_usc_id = '2') ";
                } else if ($categoriaUsuarioActual == 2) {
                    $restriccionCategoria = " AND usu_usc_id = '2' ";
                }
                $sql = "SELECT	usu_login, usu_id, usu_nombre
							FROM 	usuario, usuario_establecimiento
							WHERE 	usu_estado = 1 AND usu_id = use_usuario_id AND use_establecimiento_id = '" . $_SESSION['est_id'] . "'
									" . $restriccionCategoria . " AND usu_id <> '" . $_SESSION['usu_id'] . "'
								order by usu_login ASC";
            }

            $resultadoUsuariosCopiarFuncionalidades = $_obj_database->obtenerRegistrosAsociativos($sql);
            $cantidadUsuariosCopiarFuncionalidades = $_obj_database->obtenerNumeroFilas();

            if ($cantidadUsuariosCopiarFuncionalidades == 0) { //No hay usuarios disponibles para copiar las funcionalidades
                $listaUsuariosCopiarFuncionalidades = $idi_despliegue['no_usuarios_copiar_funcionalidades'];
            } else { //Hay por lo menos un usuario disponible para copiarle las funcionalidades
                $listaUsuariosCopiarFuncionalidades = "<select name='usu_id_funcionalidades'>";

                //Valor por defecto
                $listaUsuariosCopiarFuncionalidades .= "<option value='' align=center>- " . $idi_despliegue['seleccionar_usuario'] . " -</option>";
                for ($u = 0; $u < $cantidadUsuariosCopiarFuncionalidades; $u++) {
                    $listaUsuariosCopiarFuncionalidades .= "<option ";
                    if ($datos['usu_id_funcionalidades'] == $resultadoUsuariosCopiarFuncionalidades[$u]['usu_id']) {
                        $listaUsuariosCopiarFuncionalidades .= ' selected ';
                    }
                    $listaUsuariosCopiarFuncionalidades .= "value='" . $resultadoUsuariosCopiarFuncionalidades[$u]['usu_id'] . "'>" . $resultadoUsuariosCopiarFuncionalidades[$u]['usu_nombre'] . " (" . $resultadoUsuariosCopiarFuncionalidades[$u]['usu_login'] . ")</option>";
                }//Fin de recorrer los usuarios para copiar las funcionalidades
                $listaUsuariosCopiarFuncionalidades .= '</select>';
            }//Fin hay por lo menos un usuario disponible para copiarle las funcionalidades

            $listaFuncionalidades = $this->construirListaFuncionalidades($_SESSION['usu_id'], $datos);

            if ($listaFuncionalidades == $idi_despliegue['primario_cadena_no_asignado_no_funcionalidades'] || $listaFuncionalidades == $idi_despliegue['primario_hotel_no_asignado_no_funcionalidades']) {
                $zonaCopiarFuncionalidades = '&nbsp';
                $deshabilitarGuardar = ' disabled = "disabled" ';
            } else {
                $zonaCopiarFuncionalidades = '<table border="0" width="100%">
												<tr>
													<td>' . $idi_despliegue['copiar_funcionalidades'] . '</td>
												</tr>
												<tr>
													<td>' . $listaUsuariosCopiarFuncionalidades . '</td>
												</tr>

												<tr>
													<td><input type="button" value="' . $idi_despliegue['copiar_funcionalidades_label'] . '" onClick="copiarFuncionalidades()"></td>
												</tr>
											</table>';
                $deshabilitarGuardar = '';
            }

            //DM - 2014-07-14 issue 4924
            if (is_array($datos['encuestas'])) {
                foreach ($datos['encuestas'] as $encuesta_id) {
                    $datos['encuestas'][$encuesta_id] = array('enc_id' => $encuesta_id,
                        'uen_estado' => 1);
                }//Fin de foreach($encuestas_seleccionadas as $encuesta_id)
            }//Fin de if( is_array($encuestas_seleccionadas) )
            //pgs - 16/08/2012 - se ade la nueva seccion de encuestas asociadas
            $html_encuestas = $this->crearCheckEncuestas($datos);

            $zonaFuncionalidades = '
                                                      <tr>
                                                          <td colspan="2"><br>' . $idi_despliegue['menu_encuestas'] . '<br>' . $html_encuestas . '</td>
                                                      </tr>
                                                      <tr>
          								<td colspan="2"><br>' . $zonaCopiarFuncionalidades . '</td>
      								</tr>
									<tr>
          								<td colspan="2" class="titulo_funcionalidades"><br/>' . $idi_despliegue['funcionalidades'] . '</td>
      								</tr>
									<tr>
										<td colspan="2" align="right"><img src="' . $_PATH_IMAGENES . '/linea_gris.png" width="100%" height="1"></td>
									</tr>
									<tr>
										<td>
											' . $listaFuncionalidades . '
										</td>
										<td>
											&nbsp;
										</td>
									</tr>
									<tr>
										<td align="right" width="300" height="35">
											<input type="submit" ' . $deshabilitarGuardar . ' value="' . $idi_despliegue['guardar_cambios'] . '" class="boton_general" />
										</td>
										<td>
										</td>
									</tr>
									';
        }//Fin if ($datos['usu_per_id'] != '' && $datos['usc_codigo'] != '')
        else if ($datos['usu_per_id'] == 8) {
            //si se va a creae un super usuario
            $zonaFuncionalidades = '
									<tr>
										<td align="right" width="300" height="35">
											<input type="submit"  value="' . $idi_despliegue['guardar_cambios'] . '" class="boton_general" />
										</td>
										<td>
										</td>
									</tr>
									';
        }//Fin de else if( $datos['usu_per_id'] == 8 )

        Interfaz::asignarToken("zona_funcionalidades", $zonaFuncionalidades, $contenido);

        $enlace = "index.php?m=usuarios&accion=gestion";
        $enlace .= "&offset=" . $datos['offset'] . "&usu_busqueda=" . $datos['usu_busqueda'];
        $enlace .= "&ordena=" . $datos['ordena'] . "&tipo_ordena=" . $datos['tipo_ordena'];
        $enlace_volver = '<br /><a class="enlace" href="' . $enlace . '">' . $idi_despliegue['volver'] . '</a>';
        Interfaz::asignarToken("enlace_volver", $enlace_volver, $contenido);

        return $contenido;
    }

//Fin de abrirFormularioIngresar()

    /**
     * Función que se encarga de desplegar el formulario para editar un usuario
     * DM - 2013-08-05
     * */
    function abrirFormularioEditar($datos, $valores) {

        global $idi_despliegue, $_obj_database, $_obj_mensajes, $_PATH_IMAGENES;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_editar_usuario.html");

        if ($datos['usu_id_funcionalidades'] != '') {
            $opciones['ACCION'] = 6;
            $opciones['CADENA'] = $idi_despliegue['listado_funcionalidades'];
            $datos['mensaje'] = $_obj_mensajes->crearMensaje(1, $opciones);
        }

        Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);

        Interfaz::asignarTokenDatos("usu_busqueda", $datos, $contenido);

        $valor = $valores[0];

        Interfaz::asignarTokenDatos("label_dias_notificacion_vencimiento_tareas", $idi_despliegue, $contenido);

        Interfaz::asignarToken("nombre", $idi_despliegue['label_nombre'], $contenido);
        Interfaz::asignarToken("usuario", $idi_despliegue['usuario'], $contenido);
        Interfaz::asignarToken("clave", $idi_despliegue['clave'], $contenido);
        Interfaz::asignarToken("valor_usu_clave", $datos['usu_clave'], $contenido);
        Interfaz::asignarToken("rpt_clave", $idi_despliegue['rpt_clave'], $contenido);
        Interfaz::asignarToken("valor_usu_clave_confirmacion", $datos['usu_clave_confirmacion'], $contenido);
        Interfaz::asignarToken("label_correo", $idi_despliegue['label_correo'], $contenido);
        Interfaz::asignarToken("tipo_usuario_select", $idi_despliegue['tipo_usuario_select'], $contenido);
        Interfaz::asignarToken("guardar_cambios", $idi_despliegue['guardar_cambios'], $contenido);
        Interfaz::asignarToken("volver", $idi_despliegue['volver'], $contenido);
        Interfaz::asignarToken("usu_login", $valor['usu_login'], $contenido);
        Interfaz::asignarToken("usu_nombre", $valor['usu_nombre'], $contenido);
        Interfaz::asignarToken("usu_apellido", $valor['usu_apellido'], $contenido);
        Interfaz::asignarToken("usu_cedula", $valor['usu_cedula'], $contenido);
        Interfaz::asignarToken("usu_correo", $valor['usu_correo'], $contenido);
        Interfaz::asignarToken("usu_login", $valor['usu_login'], $contenido);
        Interfaz::asignarToken("usu_dias_notificacion_tarea_expirar", $valor['usu_dias_notificacion_tarea_expirar'], $contenido);
        //El usuario va a poder cambiar su login
        //Interfaz::asignarToken("campo_readonly","readonly",$contenido);
        //El usuario actual no podra cambiar su tipo de usuario, categoria, hotel o cadena
        $desactivarSelect = '';
        if ($datos['usu_id'] == $_SESSION['usu_id']) {
            $desactivarSelect = ' disabled = "disabled" ';
        }


        //DM - 2013-08-05
        //El usuario root puede modificar el paquete de un usuario
        //aplica solo para usuarios primarios
        $campo_paquete = '';
        if ($_SESSION['tipo_usuario'] == "R" && $valor['usu_usc_id'] == 1) {
            $datos['paquete_id'] = $datos['paquete_id'] != "" ? $datos['paquete_id'] : $valor['usu_paq_id'];
            $opciones = array();
            $opciones['nombre_campo'] = "paquete_id";
            $opciones['sql'] = " select paq_id, paq_nombre
      								from paquete
      								order by paq_nombre ASC ";
            $opciones['atributos']['codigo'] = "paq_id";
            $opciones['atributos']['mostrar'] = "paq_nombre";
            $opciones['valor'] = $datos['paquete_id'];
            $opciones['adicionales'] = 'onchange="confirmarCambioPaquete(this);" ';
            $select_paquete = Herramientas::crearSelectForma($opciones);

            $campo_paquete = '<tr>
                                <td align="left">' . $idi_despliegue['label_paquete'] . '</td>
                                <td align="left">' . $select_paquete . '
                                <input type="hidden" id="paquete_id_ant" name="paquete_id_ant" value="' . $valor['usu_paq_id'] . '" />
                                </td>
                            </tr>';
        }//Fin de if( $_SESSION['tipo_usuario'] == "R" )
        Interfaz::asignarToken("campo_paquete", $campo_paquete, $contenido);


        //Se construye la lista de tipos de usuario, pero asignada el tipo del usuario modificando
        $sql = "select per_id , pid_nombre
				from perfiles, perfiles_idioma
				where 	per_id = pid_per_id AND
						pid_idi_id = '" . $_SESSION['idi_id'] . "' AND
						per_id = '" . $valores[0]['usu_per_id'] . "'
				order by per_nombre ASC";
        ;
        $resultadoTipoUsuarios = $_obj_database->obtenerRegistrosAsociativos($sql);
        $cantidadTipoUsuarios = $_obj_database->obtenerNumeroFilas();
        $tipo_usuario = "<select name='usu_per_id' onChange='seleccionHotelEditarUsuario()' " . $desactivarSelect . ">";
        for ($k = 0; $k < $cantidadTipoUsuarios; $k++) {
            $tipo_usuario .= "<option ";
            if ($valores[0]['usu_per_id'] == $resultadoTipoUsuarios[$k]['per_id']) {
                $tipo_usuario .= ' selected ';
            }
            $tipo_usuario .= "value='" . $resultadoTipoUsuarios[$k]['per_id'] . "'>" . $resultadoTipoUsuarios[$k]['pid_nombre'] . "</option>";
        }//Fin de recorrer los tipos de usuarios encontrados
        $tipo_usuario .= '</select>';

        Interfaz::asignarToken("tipo_usuario", $tipo_usuario, $contenido);

        //Se construye el selector de las cadenas en caso que el usuario actual sea root y vaya a modificar un usuario de cadena
        $listaCadenas = '';
        //El usuario actual es el root y seleccino crear un usuario de cadena, se debe crear el selector de las cadenas disponibles
        if ($_SESSION['tipo_usuario'] == 'R' && $valores[0]['usu_per_id'] == '6') { //Puede crear un usuario de cadena pero debe seleccionar a que cadena se lo va a crear
            //Se consulta la cadena a la que pertenece el usuario actual
            $sql = "SELECT uca_cad_id FROM usuario_cadena WHERE uca_usu_id = '" . $valores[0]['usu_id'] . "'";
            $resultadoCadenaUsuario = $_obj_database->obtenerRegistrosAsociativos($sql);
            $cad_id = $resultadoCadenaUsuario[0]['uca_cad_id'];


            //Se construye la lista de las cadenas
            //pgs - 14/08/2012 - se añade filtro de la encuesta asociada
            $sql = "select cad_id , cad_nombre
							from cadena
							where cad_estado = '1' and cad_id = '" . $cad_id . "'
							order by cad_nombre ASC";
            $resultadoCadenas = $_obj_database->obtenerRegistrosAsociativos($sql);
            $cantidadCadenas = $_obj_database->obtenerNumeroFilas();
            $listaCadenas = "<select name='cad_id' onChange='seleccionCadenaNuevoUsuario()'>";
            //Valor por defecto
            //$listaCadenas .= "<option value='' align=center>- ".$idi_despliegue['seleccionar_cadena']." -</option>";
            for ($k = 0; $k < $cantidadCadenas; $k++) {
                $listaCadenas .= "<option ";
                if ($cad_id == $resultadoCadenas[$k]['cad_id']) {
                    $listaCadenas .= ' selected ';
                }
                $listaCadenas .= "value='" . $resultadoCadenas[$k]['cad_id'] . "'>" . $resultadoCadenas[$k]['cad_nombre'] . "</option>";
            }//Fin de recorrer las cadenas encontradas
            $listaCadenas .= '</select>';
            $listaCadenas = '<tr>
									<td>' . $idi_despliegue['cadena'] . '</td>
									<td>' . $listaCadenas . '</td>
								</tr>';
        }//Fin es un usuario de cadena

        Interfaz::asignarToken("lista_cadenas", $listaCadenas, $contenido);

        //Se construye el selector de los hoteles para el caso del hotel de cadena o el root
        $listaHoteles = '';
        if ($_SESSION['tipo_usuario'] == 'C' || $_SESSION['tipo_usuario'] == 'R') { //Puede crear un usuario de hotel pero debe seleccionar a que hotel se lo va a crear
            if ($valores[0]['usu_per_id'] == 2) { //Se selecciono el tipo de Usuario = hotel y va ingresar un usuario de hotel
                //Se consulta el establecimiento al que pertenece el usuario actual
                $sql = "SELECT use_establecimiento_id FROM usuario_establecimiento WHERE use_usuario_id = '" . $valores[0]['usu_id'] . "'";
                $resultadoEstablecimientoUsuario = $_obj_database->obtenerRegistrosAsociativos($sql);
                $est_id = $resultadoEstablecimientoUsuario[0]['use_establecimiento_id'];

                //Se construye la lista de los hoteles
                $sql = "select est_id , est_nombre
									from establecimiento
									where est_estado = 1
										and est_id  = '" . $est_id . "'
									order by est_nombre ASC";
                $resultadoHoteles = $_obj_database->obtenerRegistrosAsociativos($sql);
                $cantidadHoteles = $_obj_database->obtenerNumeroFilas();
                $listaHoteles = "<select name='est_id' onChange='seleccionHotelEditarUsuario()' " . $desactivarSelect . ">";
                //Valor por defecto
                //$listaHoteles .= "<option value='' align=center>- ".$idi_despliegue['seleccionar_hotel']." -</option>";
                for ($k = 0; $k < $cantidadHoteles; $k++) {
                    $listaHoteles .= "<option ";
                    if ($est_id == $resultadoHoteles[$k]['est_id']) {
                        $listaHoteles .= ' selected ';
                    }
                    $listaHoteles .= "value='" . $resultadoHoteles[$k]['est_id'] . "'>" . $resultadoHoteles[$k]['est_nombre'] . "</option>";
                }//Fin de recorrer los hoteles encontrados
                $listaHoteles .= '</select>';
                $listaHoteles = '<tr>
											<td>' . $idi_despliegue['bench_hotel'] . '</td>
											<td>' . $listaHoteles . '</td>
										</tr>';
            }//Fin se va a ingresar un usuario de hotel
        } else {
            //pgs - 23/08/2012  - si es un usuario hotel el establecimiento se trae de la session
            if ($_SESSION['tipo_usuario'] == 'H') {
                $est_id = $_SESSION['est_id'];
            }//Fin de if($_SESSION['tipo_usuario']=='C' )
        }//Fin es un usuario de cadena

        Interfaz::asignarToken("lista_hoteles", $listaHoteles, $contenido);


        //si es un usuario diferente de super usuario, debe ver la categoria
        if ($valores[0]['usu_per_id'] != 8) {
            //Consultamos la categoria del usuario actual
            $sql = "SELECT usu_usc_id FROM usuario WHERE usu_id = '" . $_SESSION['usu_id'] . "'";
            $resultadoCategoriaUsuario = $_obj_database->obtenerRegistrosAsociativos($sql);
            $categoriaUsuarioActual = $resultadoCategoriaUsuario[0]['usu_usc_id'];

            $restriccionCategoria = " AND usc_id = '" . $valores[0]['usu_usc_id'] . "'";

            //El select de las categorias de usuarios
            $sql = "select usc_id , uci_nombre
    				from usuario_categoria, usuario_categoria_idioma
    				where usc_id = uci_usc_id AND uci_idi_id = '" . $_SESSION['idi_id'] . "' AND usc_id !='0'
    				" . $restriccionCategoria . "
    				order by usc_nombre ASC";
            $resultadoCategorias = $_obj_database->obtenerRegistrosAsociativos($sql);
            $cantidadCategorias = $_obj_database->obtenerNumeroFilas();
            $categoria_usuario = "<select name='usc_codigo' onChange='seleccionHotelEditarUsuario()' " . $desactivarSelect . ">";

            for ($k = 0; $k < $cantidadCategorias; $k++) {
                $categoria_usuario .= "<option ";
                if ($valores[0]['usu_usc_id'] == $resultadoCategorias[$k]['usc_id']) {
                    $categoria_usuario .= ' selected ';
                }
                $categoria_usuario .= "value='" . $resultadoCategorias[$k]['usc_id'] . "'>" . $resultadoCategorias[$k]['uci_nombre'] . "</option>";
            }//Fin de recorrer las categorias encontradas
            $categoria_usuario .= '</select>';
        }//Fin de if( $valores[0]['usu_per_id'] != 8)

        Interfaz::asignarToken("categoria", $idi_despliegue['categoria'], $contenido);
        Interfaz::asignarToken("categoria_usuario", $categoria_usuario, $contenido);

        Interfaz::asignarToken("usu_id", $valor['usu_id'], $contenido);
        Interfaz::asignarToken("usu_per_id", $valor['usu_per_id'], $contenido);
        Interfaz::asignarToken("categoria_codigo", $valor['categoria_codigo'], $contenido);
        Interfaz::asignarToken("usu_nombre_cedula", $datos['usu_nombre_cedula'], $contenido);
        Interfaz::asignarToken("offset", $datos['offset'], $contenido);
        Interfaz::asignarToken("ordena", $datos['ordena'], $contenido);
        Interfaz::asignarToken("tipo_ordena", $datos['tipo_ordena'], $contenido);
        Interfaz::asignarToken("est_id", $datos['est_id'], $contenido);

        Interfaz::asignarToken("titulo", $idi_despliegue['titulo_modificar_usuario'], $contenido);
        Interfaz::asignarToken("accion", "editar", $contenido);
        Interfaz::asignarToken("tipo_usuario_hidden", $_SESSION['tipo_usuario'], $contenido);
        Interfaz::asignarToken("ruta_imagenes", $_PATH_IMAGENES, $contenido);

        //Se construye la lista de los usuarios para copiar sus funcionalidades -------------------------------------------------------------

        $restriccionCategoria = '';
        $zonaFuncionalidades = '';

        //La lista de funcionalidades solo aparecera cuando ya se haya seleccionado el tipo de usuario a crear y la categoria
        if ($valores[0]['usu_usc_id'] != '' && $valores[0]['usu_usc_id'] != '0') {
            //El usuario root pueder ver todos los usuarios
            if ($_SESSION['tipo_usuario'] == 'R') {
                if ($valores[0]['usu_per_id'] == '6' && $cad_id != '') { // Se va a crear un usuario de cadena, luego se debe consultar en la cadena seleccionada
                    $sql = "SELECT	usu_login, usu_id, usu_nombre
										FROM 	usuario, usuario_cadena
										WHERE 	usu_estado = 1 AND usu_id = uca_usu_id AND uca_cad_id = '" . $cad_id . "'
										ORDER BY usu_login ASC";
                } else if ($valores[0]['usu_per_id'] == '2' && $est_id != '') { // Se va a crear un usuario de hotel, luego se debe consultar en el hotel seleccionado
                    $sql = "SELECT	usu_login, usu_id, usu_nombre
										FROM 	usuario, usuario_establecimiento
										WHERE 	usu_estado = 1 AND usu_id = use_usuario_id AND use_establecimiento_id = '" . $est_id . "'
										ORDER BY usu_login ASC";
                } else {
                    $sql = "SELECT	usu_login, usu_id, usu_nombre
										FROM 	usuario
										WHERE 	usu_estado = 1 AND usu_per_id != '1' AND usu_per_id != '3' AND usu_per_id != '5' AND usu_per_id != '7'
										ORDER BY  usu_login ASC";
                }
                $restriccionPerfil = "";
            }

            //Los usuarios de cadena pueden ver otros usuarios de cadena y de hotel
            else if ($_SESSION['tipo_usuario'] == 'C') {
                if ($categoriaUsuarioActual == 1) {
                    $restriccionCategoria = "  AND (usu_usc_id = '1' OR usu_usc_id = '2') ";
                } else if ($categoriaUsuarioActual == 2) {
                    $restriccionCategoria = " AND usu_usc_id = '2' ";
                }

                if ($datos['usu_per_id'] == 2) { // Se va a crear un usuario de hotel, luego se debe consultar en el hotel seleccionado
                    $sql = "SELECT	usu_login, usu_id, usu_nombre
										FROM 	usuario, usuario_establecimiento
										WHERE 	usu_estado = 1 AND usu_id = use_usuario_id AND use_establecimiento_id = '" . $datos['est_id'] . "'
												" . $restriccionCategoria . "
											order by usu_login ASC";
                } else if ($datos['usu_per_id'] == 6) { //Se va crear un usuario de cadena, luego se debe consultar en usuarios de su cadena
                    $sql = "SELECT	usu_login, usu_id, usu_nombre
										FROM 	usuario, usuario_cadena
										WHERE 	usu_estado = 1 AND usu_id = uca_usu_id AND uca_cad_id = '" . $_SESSION['cad_id'] . "'
												" . $restriccionCategoria . "
											order by usu_login ASC";
                }
                $restriccionPerfil = " AND (usu_per_id = '6' OR usu_per_id = '2')";
            }
            //Los usuarios de hotel solo pueden ver otros usuarios de su hotel
            else {
                if ($categoriaUsuarioActual == 1) {
                    $restriccionCategoria = " AND (usu_usc_id = '1' OR usu_usc_id = '2') ";
                } else if ($categoriaUsuarioActual == 2) {
                    $restriccionCategoria = " AND usu_usc_id = '2' ";
                }
                $sql = "SELECT	usu_login, usu_id, usu_nombre
								FROM 	usuario, usuario_establecimiento
								WHERE 	usu_estado = 1 AND usu_id = use_usuario_id AND use_establecimiento_id = '" . $_SESSION['est_id'] . "'
										" . $restriccionCategoria . " AND usu_id <> '" . $_SESSION['usu_id'] . "'
									order by usu_login ASC";
            }

            $resultadoUsuariosCopiarFuncionalidades = $_obj_database->obtenerRegistrosAsociativos($sql);
            $cantidadUsuariosCopiarFuncionalidades = $_obj_database->obtenerNumeroFilas();

            if ($cantidadUsuariosCopiarFuncionalidades == 0) { //No hay usuarios disponibles para copiar las funcionalidades
                $listaUsuariosCopiarFuncionalidades = $idi_despliegue['no_usuarios_copiar_funcionalidades'];
            } else { //Hay por lo menos un usuario disponible para copiarle las funcionalidades
                $listaUsuariosCopiarFuncionalidades = "<select name='usu_id_funcionalidades'>";

                //Valor por defecto
                $listaUsuariosCopiarFuncionalidades .= "<option value='' align=center>- " . $idi_despliegue['seleccionar_usuario'] . " -</option>";
                for ($u = 0; $u < $cantidadUsuariosCopiarFuncionalidades; $u++) {
                    $listaUsuariosCopiarFuncionalidades .= "<option ";
                    if ($datos['usu_id_funcionalidades'] == $resultadoUsuariosCopiarFuncionalidades[$u]['usu_id']) {
                        $listaUsuariosCopiarFuncionalidades .= ' selected ';
                    }
                    $listaUsuariosCopiarFuncionalidades .= "value='" . $resultadoUsuariosCopiarFuncionalidades[$u]['usu_id'] . "'>" . $resultadoUsuariosCopiarFuncionalidades[$u]['usu_nombre'] . " (" . $resultadoUsuariosCopiarFuncionalidades[$u]['usu_login'] . ")</option>";
                }//Fin de recorrer los usuarios para copiar las funcionalidades
                $listaUsuariosCopiarFuncionalidades .= '</select>';
            }//Fin hay por lo menos un usuario disponible para copiarle las funcionalidades

            $datos['usu_per_id'] = $valores[0]['usu_per_id'];
            $datos['cad_id'] = $cad_id;
            $datos['est_id'] = $est_id;
            $listaFuncionalidades = $this->construirListaFuncionalidades($_SESSION['usu_id'], $datos);

            if ($listaFuncionalidades == $idi_despliegue['primario_cadena_no_asignado_no_funcionalidades'] ||
                    $listaFuncionalidades == $idi_despliegue['primario_hotel_no_asignado_no_funcionalidades']) {
                $zonaCopiarFuncionalidades = '&nbsp';
                $deshabilitarGuardar = ' disabled = "disabled" ';
            } else {
                $zonaCopiarFuncionalidades = '<table border="0" width="100%">
														<tr>
															<td>' . $idi_despliegue['copiar_funcionalidades'] . '</td>
														</tr>
														<tr>
															<td>' . $listaUsuariosCopiarFuncionalidades . '</td>
														</tr>

														<tr>
															<td><input type="button" value="' . $idi_despliegue['copiar_funcionalidades_label'] . '" onClick="copiarFuncionalidadesEditar()"></td>
														</tr>
													</table>';
                $deshabilitarGuardar = '';
            }


            //DM - 2014-07-14 issue 4924
            //almacena las encuestas seleccionas en el formulario si hay seleccionadas
            $encuestas_seleccionadas = $datos['encuestas'];


            //pgs - 16/08/2012 - se ade la nueva seccion de encuestas asociadas
            //se obtienen las encuestas asociadas al usuario
            $datos['encuestas'] = $this->obtenerEncuestasUsuario($datos);

            $datos_enc = array_merge($valores[0], $datos);

            if (is_array($encuestas_seleccionadas)) {
                foreach ($encuestas_seleccionadas as $encuesta_id) {
                    $datos_enc['encuestas'][$encuesta_id] = array('enc_id' => $encuesta_id,
                        'uen_estado' => 1);
                }//Fin de foreach($encuestas_seleccionadas as $encuesta_id)
            }//Fin de if( is_array($encuestas_seleccionadas) )

            $html_encuestas = $this->crearCheckEncuestas($datos_enc);
            //pgs - 16/08/2012 - fin de, se ade la nueva seccion de encuestas asociadas


            $enlace_gestion = 'index.php?m=usuarios&amp;accion=gestion';
            $enlace_gestion .= '&amp;offset=' . $datos['offset'] . '&amp;usu_busqueda=' . $datos['usu_busqueda'];
            $enlace_gestion .= '&amp;ordena=' . $datos['ordena'] . '&amp;tipo_ordena=' . $datos['tipo_ordena'];

            $zonaFuncionalidades = '<tr>
                                                            <td colspan="2"><br>' . $idi_despliegue['menu_encuestas'] . '<br>' . $html_encuestas . '</td>
                                                        </tr>
                                                        <tr>
            								<td colspan="2"><br>' . $zonaCopiarFuncionalidades . '</td>
        								</tr>
										<tr>
            								<td colspan="2" class="titulo_funcionalidades"><br/>' . $idi_despliegue['funcionalidades'] . '</td>
        								</tr>
										<tr>
											<td colspan="2" align="right"><img src="' . $_PATH_IMAGENES . '/linea_gris.png" width="100%" height="1"></td>
										</tr>
										<tr>
											<td>
												' . $listaFuncionalidades . '
											</td>
											<td>
												&nbsp;
											</td>
										</tr>
										<tr>
											<td align="right" width="300" height="35">
												<input type="submit"  ' . $deshabilitarGuardar . ' value="' . $idi_despliegue['guardar_cambios'] . '" class="boton_general">
											</td>
											<td>
											</td>
										</tr>
										';
        }//Fin if ($datos['usu_per_id'] != '' && $datos['usc_codigo'] != '')
        else if ($valores[0]['usu_per_id'] == 8) {
            //si es un super usuario
            $zonaFuncionalidades = '<tr>
											<td align="right" width="300" height="35">
												<input type="submit"  value="' . $idi_despliegue['guardar_cambios'] . '" class="boton_general">
											</td>
											<td>
											</td>
										</tr>
										';
        }//Fin de else if( $valores[0]['usu_per_id'] == 8 )

        Interfaz::asignarToken("zona_funcionalidades", $zonaFuncionalidades, $contenido);

        if ($valor['usu_estado'] == 1)
            $checked1 = "checked";
        else
            $checked2 = "checked";
        $radios_estado_clientes = "<input type=radio name=usu_estado value=1 $checked1>Activo<br>
    								<input type=radio name=usu_estado value=0 $checked2>Bloqueado";
        Interfaz::asignarToken("estado_usuario", $radios_estado_clientes, $contenido);
        $enlace = "index.php?m=usuarios&accion=gestion";
        $enlace .= "&offset=" . $datos['offset'] . "&usu_busqueda=" . $datos['usu_busqueda'];
        $enlace .= "&ordena=" . $datos['ordena'] . "&tipo_ordena=" . $datos['tipo_ordena'];
        $enlace_volver = '<br /><a class="enlace" href="' . $enlace . '">' . $idi_despliegue['volver'] . '</a>';
        Interfaz::asignarToken("enlace_volver", $enlace_volver, $contenido);

        return $contenido;
    }

//Fin de abrirFormularioEditar()

    function obtenerContenidoActivacion($datos) {
        global $_PATH_IMAGENES;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/correo_activacion_cuenta.html");

        Interfaz::asignarToken("path_imagenes", $_PATH_IMAGENES, $contenido);
        Interfaz::asignarToken("usuario", $datos['usu_login'], $contenido);

        return $contenido;
    }

//fin de obtenerContenidoActivacion()

    function obtenerCorreoClaveAcceso($datos) {
        global $_PATH_WEB;

        $valor = $valores[0];

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/correo_clave_acceso.html");

        Interfaz::asignarToken("usuario", $datos['usu_nombre'], $contenido);
        Interfaz::asignarToken("clave", $datos['usu_clave'], $contenido);
        Interfaz::asignarToken("usu_cedula", $datos['usu_cedula'], $contenido);

        Interfaz::asignarToken("path_web", $_PATH_WEB, $contenido);


        return $contenido;
    }

//fin de obtenerContenidoActivacion()
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //FIN DE METODOS GRAFICOS
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Funcion que se encarga de registrar la informacion de acceso de los usuarios
     * MD - 2012-09-11
     * */
    function registrarAcceso($datos) {
        global $_obj_database;

        $fecha = date("Y-m-d");
        $usu_id = $_SESSION['usu_id'];

        $sql = "select usa_usu_id
                from usuario_accesos
                where usa_fecha = '" . $fecha . "'
                    and usa_usu_id ='" . $usu_id . "' ";

        $resultado = $_obj_database->obtenerResultado($sql);

        //si el registro existe lo actualiza de lo contrario lo ingresa
        if (sizeof($resultado) > 0) {
            $sql = "update usuario_accesos
                        set usa_accesos = usa_accesos + 1
                        where usa_usu_id = '" . $usu_id . "'
                        and usa_fecha = '" . $fecha . "'";

            $_obj_database->ejecutarSql($sql);
        } else {
            $sql = "insert into usuario_accesos(usa_usu_id , usa_fecha, usa_accesos)
                        values('" . $usu_id . "','" . $fecha . "',1);";
            $_obj_database->ejecutarSql($sql);
        }
    }

//Fin de registrarAcceso

    /**
     * Funcion que se encarga de registrar en la sesion los datos del establecimiento
     * MD - 2012-09-12
     * */
    function registrarSesionUsuario($datos) {
        global $_obj_database;

        $sql = "select usu_id, usu_nombre,usu_correo, usu_logo, per_codigo as tipo_usuario
        			from usuario, perfiles
        			where usu_per_id=per_id and usu_id = '" . $datos['usu_id'] . "'";

        $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);

        if ($_obj_database->obtenerNumeroFilas() > 0) {
            //si es un usuario tipo cadena
            if ($resultado[0]['tipo_usuario'] == "C") {
                $est = "SELECT *
    					FROM cadena,usuario_cadena
    					WHERE cad_id = uca_cad_id
    					AND uca_usu_id =" . $resultado[0]['usu_id'] . "";
                $result = $_obj_database->obtenerRegistrosAsociativos($est);

                $_SESSION = array_merge($_SESSION, $resultado[0]);
                $_SESSION = array_merge($_SESSION, $result[0]);
            } else {
                $sql_est = "SELECT
    					est_id,est_nombre, est_logo, est_minimo_dias_envio,
    						est_codigo, est_tipo, est_categoria_id, est_cad_id,est_vs, est_cs, est_vs_fecha, est_cs_fecha,
                            est_idi_id, est_mar_id
    					FROM establecimiento,usuario_establecimiento
    					WHERE est_id = use_establecimiento_id
    					AND use_usuario_id =" . $resultado[0]['usu_id'] . "";

                $consulta_establecimiento = $_obj_database->obtenerRegistrosAsociativos($sql_est);

                $_SESSION = array_merge($_SESSION, $resultado[0]);

                if (sizeof($consulta_establecimiento) > 0) {

                    $_SESSION = array_merge($_SESSION, $consulta_establecimiento[0]);

                    //MD - 2012-09-11
                    //obtiene la informacion del idioma del establecimiento
                    $info_idioma = $_obj_database->obtenerRegistro("idiomas", "idi_id = '" . $_SESSION ['est_idi_id'] . "' ");


                    $_SESSION = is_array($info_idioma) ? array_merge($_SESSION, $info_idioma) : $_SESSION;


                    //Obtiene la plantilla generica del establecimiento
                    $plantilla = $_obj_database->obtenerRegistro("establecimiento_plantilla_email", "epe_est_id = '" . $_SESSION ['est_id'] . "'
                                                                and epe_generico = 1
                                                                and epe_enc_id = '" . $_SESSION['enc_id'] . "'
                                                                ");

                    $_SESSION ['est_pro_id'] = $plantilla['epe_pro_id'];
                    //MD - 2012-09-11 - Fin de cambios
                }//Fin de if( sizeof($resul)>0 )
            }//Fin de else de if( $resultado[0]['tipo_usuario'] == "C" )
            //si el establecimiento puede ver datos de cadena consulta los hoteels de la cadena
            if ($_SESSION['est_cad_id'] > 0) {

                //pgs - 14/08/2012 - se añade filtro de la encuesta asociada
                $sql = "select cad_nombre
                        from cadena
                        where cad_estado = 1
                                and cad_id = '" . $_SESSION['est_cad_id'] . "'
                                AND cad_enc_id='" . $_SESSION['enc_id'] . "'";

                $cadenas = $_obj_database->obtenerRegistrosAsociativos($sql);
                $_SESSION['cad_nombre'] = $cadenas[0]['cad_nombre'];

                $sql = "select est_id, est_nombre
                        from establecimiento
                        where est_estado = 1
                                and est_cad_id = '" . $_SESSION['est_cad_id'] . "' ";

                $establecimientos_cadenas = $_obj_database->obtenerRegistrosAsociativosPorClave($sql, "est_id");
                if (sizeof($establecimientos_cadenas) > 0) {
                    $_SESSION['cadena_est_ids'] = implode(",", array_keys($establecimientos_cadenas));

                    //MD - 2012-09-12
                    //Consulta para los hoteles el pro_id asociado
                    $establecimientos_cadenas = $this->consultarIdProyectosSEEGenericos($establecimientos_cadenas);
                    //MD - 2012-09-12 - Fin de cambios

                    $_SESSION['cadena_est'] = $establecimientos_cadenas;
                } else {
                    $_SESSION['cadena_est_ids'] = "0";
                }

                $sql = "SELECT uca_idi_id AS cad_idi_id, uca_idi_comentarios_id AS cad_comentarios_idi_id
                        FROM usuario_cadena WHERE uca_usu_id = '" . $_SESSION ['est_id'] . "'";
                $cadenasIdi = $_obj_database->obtenerRegistrosAsociativos($sql);
                //$_SESSION['usc_idi_id'] = $cadenasIdi[0]['cad_idi_id'];
            }//Fin de if( establecimientos::accesoOpcion("est_ver_cadena") )


            if (establecimientos::accesoOpcion("est_cs")) {
                $sql = "select est_id, est_nombre
                        from establecimiento, establecimiento_benchmark
                        where est_id = esb_bench_est_id
                            and esb_est_id = '" . $_SESSION['est_id'] . "'
                            and esb_estado = 1 ";

                $hoteles = $_obj_database->obtenerRegistrosAsociativosPorClave($sql, "est_id");


                //si el hotel tiene seleccionado su competitive set
                if (sizeof($hoteles) > 0) {

                    $cs_ids_todos = establecimientos::idsLimiteRespuestas();

                    if (sizeof($cs_ids_todos) >= 5) {
                        //obtiene los ids de hoteles del cs
                        $ids = array_keys($hoteles);

                        //obtiene los ids de los que tienen mas de 20 respuestas
                        $ids = array_intersect($ids, $cs_ids_todos);

                        //si hay mas de 5 hoteles
                        if (sizeof($ids) >= 5) {
                            $tmp = array();
                            foreach ($ids as $id) {
                                $tmp[$id] = $hoteles[$id];
                            }//Fin de foreach($ids as $id)
                            $hoteles = $tmp;
                        }//Fin de if( sizeof($ids) >= 5 )
                        else {
                            $_SESSION['cs_hoteles_no_respuestas'] = 1;
                        }//fin de else de if( sizeof($ids) >= 5 )
                    } else {
                        $hoteles = array();
                        $_SESSION['cs_hoteles_no_respuestas'] = 1;
                    }
                }//Fin de if( sizeof($hoteles) > 0 )
                //MD - 2012-09-12
                //Consulta para los hoteles el pro_id asociado
                $hoteles = $this->consultarIdProyectosSEEGenericos($hoteles);
                //MD - 2012-09-12 - Fin de cambios

                $_SESSION['cs_hoteles'] = $hoteles;
            }//Fin de if( establecimientos::accesoOpcion("est_cs") )


            if (cadenas::accesoOpcion("cad_ver_valoraciones") || $_SESSION['tipo_usuario'] == "C") {
                $sql = "select est_id, est_nombre, est_categoria_id
                        from establecimiento
                        where est_estado = 1
                                and est_cad_id = '" . $_SESSION['cad_id'] . "' ";

                $cadenas = $_obj_database->obtenerRegistrosAsociativosPorClave($sql, "est_id");
                if (sizeof($cadenas) > 0) {
                    $_SESSION['cadena_est_ids'] = implode(",", array_keys($cadenas));

                    //MD - 2012-09-12
                    //Consulta para los hoteles el pro_id asociado
                    $cadenas = $this->consultarIdProyectosSEEGenericos($cadenas);
                    //MD - 2012-09-12 - Fin de cambios

                    $_SESSION['cadena_est'] = $cadenas;
                } else {
                    $_SESSION['cadena_est_ids'] = "0";
                }
            }//Fin de if( cadenas::accesoOpcion("cad_ver_valoraciones") )
        }//Fin de if ($_obj_database->obtenerNumeroFilas()>0)
    }

//Fin de registrarSesionUsuario

    /**
     * Funcion que se encarga de iniciar la sesion del usuario, verificando que el nombre de
     * usurio y la contraseña sean correctos
     * MD - 2012-09-11
     * */
    function autenticarUsuario($datos) {
        global $_obj_database, $_opciones;

        //DMG - 2014-09-19 issue 4150
        $existe_invalidos = contieneCamposInvalidos($datos);
        if ($existe_invalidos) { //existen caracteres invalidos
            return 'FM-1';
        }//Fin de if( $existe_invalidos )
        //DMG - 2014-09-25 issue 4150
        $datos['codigo'] = "co_au";
        validarRegistroIP($datos);

        //Si el usuario root esta tratando de autenticar a un usuario entonces
        //no valida la activacion
        $es_root_login = false;
        $es_acceso_web = false;
        //guarda los datos del root
        $datos_root = array();
        if ($_SESSION['tipo_usuario'] == "R" && is_numeric($datos['usuario_id'])) {
            $es_root_login = true;
            $datos_root['usu_id'] = $_SESSION['usu_id'];
            //inicializar la variable de sesion
            $_SESSION = array();
        }//Fin de if( $_SESSION['tipo_usuario'] == "R" && is_numeric($datos['usuario_id']) )
        else if (is_numeric($datos['usuario_root_id']) && $datos['usuario_root_id'] == $_SESSION['usuario_root_id']) {
            $es_root_login = true;
            $datos['usuario_id'] = $datos['usuario_root_id'];
            //inicializar la variable de sesion
            $_SESSION = array();
        }//Fin de else if( is_numeric($datos['usuario_root_id']) && $datos['usuario_root_id'] == $_SESSION['usuario_root_id'] )
        else if (is_numeric($datos['usuario_id']) && md5( $datos['usuario_id'].date('YmdH') ) == $datos['usuario_cod'] ) {
            $es_acceso_web = true;
            //inicializar la variable de sesion
            $_SESSION = array();
        }//Fin de else if( is_numeric($datos['usuario_root_id']) && $datos['usuario_root_id'] == $_SESSION['usuario_root_id'] )
        //Nuevo Noviembre 6 de 2011
        //Se consulta si el usuario que esta tratando de ingresar es de
        //los usuarios que se han registrado y no han activado el hotel
        $sql = "SELECT act_id
                    FROM activacion
                    WHERE act_usuario = '" . $datos['usu_login'] . "'";
        $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
        $cantidad = $_obj_database->obtenerNumeroFilas();
        //El usuario esta dentro de la tabla activacion y no ha activado el establecimiento
        if ($cantidad > 0 && $es_root_login == false) {
            $respuesta = "U-6";
            return $respuesta;
        }//Fin El usuario esta dentro de la tabla activacion y no ha activado el establecimiento
        //usuario no existe
        $respuesta = 'U-0';

        //si es el usuario root que quiere ingresar con el usuario entonces carga los datos
        if ($es_root_login || $es_acceso_web ) {
            $sql = "select usu_id, usu_nombre,usu_clave,usu_correo,usu_logo,  per_codigo as tipo_usuario,
                        usu_usc_id
    					from usuario, perfiles
    					where usu_per_id=per_id
    					and usu_estado = 1
    					 and usu_id='{$datos[usuario_id]}'";
        }//Fin de if( $es_root_login )
        else {
            $sql = "select usu_id, usu_nombre,usu_clave,usu_correo,usu_logo,  per_codigo as tipo_usuario,
                        usu_usc_id
    					from usuario, perfiles
    					where usu_per_id=per_id
    					and usu_estado = 1
    					 and usu_login='{$datos[usu_login]}'";
        }

        $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);

        //Existe un usuario con ese login o usu_id
        if ($_obj_database->obtenerNumeroFilas() > 0) {
            //Clave incorrecta
            $respuesta = 'U-2';

            if (strcmp($resultado[0]['usu_clave'], md5($datos['usu_clave'])) == 0 || $es_root_login || $es_acceso_web) {
                //MD - 2012-09-04
                //Obtiene una de las encuestas activas que tiene asignada el usuario
                $encuesta = $_obj_database->obtenerRegistro("usuario_encuesta", "uen_usu_id = '" . $resultado[0]['usu_id'] . "'
                                                    and uen_estado = 1
                                                    order by uen_enc_id ASC ");

                if (is_array($encuesta)) {
                    $_SESSION['enc_id'] = $encuesta['uen_enc_id'];
                }//Fin de if( is_array($encuesta) )
                //MD - 2012-09-04 - Fin de cambios
                //si es un usuario tipo cadena
                if ($resultado[0]['tipo_usuario'] == "C") {
                    $est = "SELECT *
    							FROM cadena,usuario_cadena
    							WHERE cad_id = uca_cad_id
    							AND uca_usu_id =" . $resultado[0]['usu_id'] . "";
                    $result = $_obj_database->obtenerRegistrosAsociativos($est);

                    //DM - 2014-12-31 issue 5748
                    //Verifica si tiene acceso al cuestionario
                    redireccionCuentaInactiva($result[0]);

                    $_SESSION['autenticado'] = 1;
                    $_SESSION['categoria_usuario'] = $resultado[0]['usu_usc_id'];
                    $_SESSION = array_merge($_SESSION, $resultado[0]);


                    //MAOH - 22 Sep 2012 - Incidencia 1823
                    if (!is_null($result) && sizeof($result) > 0)
                        $_SESSION = array_merge($_SESSION, $result[0]);


                    $_SESSION[idi_id] = $_SESSION[uca_idi_id];
                    
                    //MD - 2012-09-11
                    //obtiene la informacion del idioma del establecimiento
                    $info_idioma = $_obj_database->obtenerRegistro("idiomas", "idi_id = '" . $_SESSION ['uca_idi_id'] . "' ");
                    
                    $_SESSION = is_array($info_idioma) ? array_merge($_SESSION, $info_idioma) : $_SESSION;

                    $respuesta = "U-1";
                }//Fin es un usuario de cadena
                else if ($resultado[0]['tipo_usuario'] == "H") { //No es un usuario cadena
                    $sql_est = "SELECT
								est_id,est_nombre, est_logo, est_minimo_dias_envio,
									est_codigo, est_tipo, est_categoria_id, est_cad_id,
                                     est_vs, est_cs, est_vs_fecha, est_cs_fecha,
									est_idi_id,est_idi_comentarios_id , est_mar_id,
                                    est_nombre_email_remite, est_email_remitente
								FROM establecimiento,usuario_establecimiento
								WHERE est_id = use_establecimiento_id
								AND use_usuario_id =" . $resultado[0]['usu_id'] . "";

                    $consulta_establecimiento = $_obj_database->obtenerRegistrosAsociativos($sql_est);

                    //DM - 2014-12-31 issue 5748
                    //verifica si tiene acceso al cuestionario
                    redireccionCuentaInactiva($consulta_establecimiento[0]);


                    $_SESSION['autenticado'] = 1;
                    $_SESSION = array_merge($_SESSION, $resultado[0]);
                    $respuesta = "U-1";

                    if (sizeof($consulta_establecimiento) > 0) {

                        $_SESSION = array_merge($_SESSION, $consulta_establecimiento[0]);

                        //MD - 2012-09-11
                        //obtiene la informacion del idioma del establecimiento
                        $info_idioma = $_obj_database->obtenerRegistro("idiomas", "idi_id = '" . $_SESSION ['est_idi_id'] . "' ");


                        $_SESSION = is_array($info_idioma) ? array_merge($_SESSION, $info_idioma) : $_SESSION;


                        //Obtiene la plantilla generica del establecimiento
                        $plantilla = $_obj_database->obtenerRegistro("establecimiento_plantilla_email", "epe_est_id = '" . $_SESSION ['est_id'] . "'
                                                                        and epe_enc_id = '" . $_SESSION['enc_id'] . "'
                                                                        and epe_generico = 1 ");

                        $_SESSION ['est_pro_id'] = $plantilla['epe_pro_id'];
                        //MD - 2012-09-11 - Fin de cambios
                    }//Fin de if( sizeof($resul)>0 )
                }//Fin else no es un usuario de cadena
                else if ($resultado[0]['tipo_usuario'] == "R" || $resultado[0]['tipo_usuario'] == "S") {
                    $_SESSION['autenticado'] = 1;
                    $_SESSION = array_merge($_SESSION, $resultado[0]);
                    $respuesta = "U-1";
                }//Fin de else if( $resultado[0]['tipo_usuario'] == "R"  )

                $this->registrarAcceso($datos);


                //si el establecimiento puede ver datos de cadena consulta los hoteels de la cadena
                if ($_SESSION['est_cad_id'] > 0) {
                    //pgs - 14/08/2012 - se añade filtro de la encuesta asociada
                    $sql = "select cad_nombre
							from cadena
							where cad_estado = 1
									and cad_id = '" . $_SESSION['est_cad_id'] . "'
                                        ";

                    $cadenas = $_obj_database->obtenerRegistrosAsociativos($sql);
                    $_SESSION['cad_nombre'] = $cadenas[0]['cad_nombre'];

                    //DM - 2012-09-12
                    //se retira el est_pro_id de la consulta y se agrega como consulta de la tabla
                    //establecimiento_plantilla_email
                    $sql = "select est_id, est_nombre, est_nombre_email_remite, est_email_remitente
							from establecimiento
							where est_estado = 1
									and est_cad_id = '" . $_SESSION['est_cad_id'] . "' ";

                    $establecimientos_cadenas = $_obj_database->obtenerRegistrosAsociativosPorClave($sql, "est_id");
                    if (sizeof($establecimientos_cadenas) > 0) {

                        $establecimientos_cadenas_ids = array_keys($establecimientos_cadenas);

                        //MD - 2012-09-12
                        //Consulta para los hoteles el pro_id asociado
                        $establecimientos_cadenas = $this->consultarIdProyectosSEEGenericos($establecimientos_cadenas);
                        //MD - 2012-09-12 - Fin de cambios

                        $_SESSION['cadena_est_ids'] = implode(",", $establecimientos_cadenas_ids);
                        $_SESSION['cadena_est'] = $establecimientos_cadenas;
                    }//Fin de if( sizeof($establecimientos_cadenas) > 0 )
                    else {
                        $_SESSION['cadena_est_ids'] = "0";
                    }//Fin de else de if( sizeof($establecimientos_cadenas) > 0 )
                    //DM - 2012-09-12 - Fin de cambios
                }//Fin de if( establecimientos::accesoOpcion("est_ver_cadena") )
                //si tiene acceso al cs selecciona los hoteles que tiene configurados
                if (establecimientos::accesoOpcion("est_cs")) {
                    $sql = "select est_id, est_nombre
							from establecimiento, establecimiento_benchmark
							where est_id = esb_bench_est_id
								and esb_est_id = '" . $_SESSION['est_id'] . "'
								and esb_estado = 1 ";

                    $hoteles = $_obj_database->obtenerRegistrosAsociativosPorClave($sql, "est_id");

                    //si el hotel tiene seleccionado su competitive set
                    if (sizeof($hoteles) > 0) {
                        //Hoteles que tienen mas de 20 respuestas
                        $cs_ids_todos = establecimientos::idsLimiteRespuestas();

                        if (sizeof($cs_ids_todos) >= 5) {
                            //obtiene los ids de hoteles del cs
                            $ids = array_keys($hoteles);

                            //obtiene los ids de los que tienen mas de 20 respuestas
                            $ids = array_intersect($ids, $cs_ids_todos);

                            //si hay mas de 5 hoteles
                            if (sizeof($ids) >= 5) {
                                $tmp = array();
                                foreach ($ids as $id) {
                                    $tmp[$id] = $hoteles[$id];
                                }//Fin de foreach($ids as $id)
                                $hoteles = $tmp;
                            }//Fin de if( sizeof($ids) >= 5 )
                            else {
                                $_SESSION['cs_hoteles_no_respuestas'] = 1;
                            }//fin de else de if( sizeof($ids) >= 5 )
                        } else {
                            $hoteles = array();
                            $_SESSION['cs_hoteles_no_respuestas'] = 1;
                        }
                    }//Fin de if( sizeof($hoteles) > 0 )
                    //MD - 2012-09-12
                    //Consulta para los hoteles el pro_id asociado
                    $hoteles = $this->consultarIdProyectosSEEGenericos($hoteles);
                    //MD - 2012-09-12 - Fin de cambios

                    $_SESSION['cs_hoteles'] = $hoteles;
                }//Fin de if( establecimientos::accesoOpcion("est_cs") )


                if ($_SESSION['tipo_usuario'] == "C") {
                    $sql = "select est_id, est_nombre, est_categoria_id
							from establecimiento
							where est_estado = 1
									and est_cad_id = '" . $_SESSION['cad_id'] . "' ";

                    $cadenas = $_obj_database->obtenerRegistrosAsociativosPorClave($sql, "est_id");
                    if (sizeof($cadenas) > 0) {
                        $_SESSION['cadena_est_ids'] = implode(",", array_keys($cadenas));

                        //MD - 2012-09-12
                        //Consulta para los hoteles el pro_id asociado
                        $cadenas = $this->consultarIdProyectosSEEGenericos($cadenas);
                        //MD - 2012-09-12 - Fin de cambios
                        $_SESSION['cadena_est'] = $cadenas;
                    } else {
                        $_SESSION['cadena_est_ids'] = "0";
                    }
                }//Fin de if( cadenas::accesoOpcion("cad_ver_valoraciones") )

                if ($es_root_login) {
                    //si el usuario root esta usando una cuenta de usuario, almacena el id del usuario
                    $_SESSION['usuario_root_id'] = $datos_root['usu_id'];
                }//Fin de if( $es_root_login )

                $this->cargarFuncionalidadesUsuario($datos);
            }//Fin de if( strcmp($resultado[0]['usu_clave'],$datos['usa_clave'])==0 )
        }//Fin de if ($_obj_database->obtenerNumeroFilas()>0)

        return $respuesta;
    }

//Fin de autenticarUsuario()

    /**
     * Se encarga de cargar en la sesion los identificadores de la funcionalidades
     * a los cuales tiene acceso el usuario
     * DM - 2014-03-05
     * */
    function cargarFuncionalidadesUsuario($datos) {
        global $_obj_database;

        //DM - 2014-03-04 issue 4183
        //si no es un super usuario entonces debe ver si tiene funcionalidades activas
        //si es un usuario root entonces no debe cargar funcionalidades
        if ($_SESSION['tipo_usuario'] == 'R' || $_SESSION['tipo_usuario'] == 'S') {
            return;
        }//Fin de if( $_SESSION['tipo_usuario'] == 'R' )

        $sql = "SELECT ufu_fun_id
                FROM usuario_funcionalidad
                WHERE ufu_usu_id =  " . $_SESSION['usu_id'];

        $lista_funcionalidades = $_obj_database->obtenerRegistrosAsociativosPorClave($sql, 'ufu_fun_id');

        $funciones = array_keys($lista_funcionalidades);

        //guarda en la sesion las funciones que tiene el usuario
        $_SESSION['funcionalidades'] = $funciones;
    }

//Fin de cargarFuncionalidadesUsuario

    /**
     * Funcion que se encarga de consultar los id de los proyectos SEE que estan
     * asociados a los hoteles, esto lo hace en la tabla de establecimiento_plantilla_email
     * MD - 2012-09-12
     * * */
    function consultarIdProyectosSEEGenericos($hoteles) {
        global $_obj_database;

        if (is_array($hoteles) && sizeof($hoteles) > 0) {
            //Consulta los proyectos del see para cada establecimiento de la cadena
            $sql_proyectos_see = "select epe_pro_id, epe_est_id
                            from establecimiento_plantilla_email
                            where epe_est_id IN ( " . implode(",", array_keys($hoteles)) . ")
                                and epe_generico = 1
                                and epe_enc_id = '" . $_SESSION['enc_id'] . "'
                                ";

            $proyectos_see = $_obj_database->obtenerRegistrosAsociativosPorClave($sql_proyectos_see, "epe_est_id");

            foreach ($hoteles as $id => $establecimiento) {
                $est_id = $establecimiento['est_id'];
                $establecimiento['est_pro_id'] = $proyectos_see[$est_id]['epe_pro_id'];
                $hoteles[$id] = $establecimiento;
            }//Fin de foreach($establecimientos_cadenas $id => $establecimiento)
        }//Fin de if( is_array($hoteles) && sizeof($hoteles) > 0 )

        return $hoteles;
    }

//Fin de consultarIdProyectosSEEGenericos()

    function solicitarRecordatorioClave($datos) {
        //usuario no existe
        $respuesta = 'U-0';

        global $_obj_database;

        //DMG - 2014-09-22 issue 4150
        $existe_invalidos = contieneCamposInvalidos($datos);
        if ($existe_invalidos) { //existen caracteres invalidos
            return 'FM-1';
        }//Fin de if( $existe_invalidos )
        //DMG - 2014-09-26 issue 4150
        $datos['codigo'] = "co_rc";
        validarRegistroIP($datos);

        $sql = "select usu_id, usu_nombre, usu_correo
					from usuario
					where usu_login='{$datos[usu_login]}'";

        $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);


        if ($_obj_database->obtenerNumeroFilas() > 0) {
            //Correo incorrecto
            $respuesta = 'U-4';

            if (strcmp($resultado[0]['usu_correo'], $datos['usu_correo']) == 0) {
                $clave = $this->cambiarClave($resultado[0]['usu_id']);
                if ($clave != "") {
                    $resultado[0]['usu_clave'] = $clave;
                    $respuesta = $resultado;
                }//Fin de if( $clave != "")
            }//Fin de if( strcmp($resultado[0]['usu_clave'],$datos['usa_clave'])==0 )
        }//Fin de if ($_obj_database->obtenerNumeroFilas()>0)

        return $respuesta;
    }

//Fin de solicitarsolicitarRecordatorioClave()

    function cambiarClave($usu_id) {
        global $_obj_database;

        $clave = rand(111111, 999999);

        $sql = "update usuario
				set usu_clave='" . md5($clave) . "'
				where usu_id=" . $usu_id;

        $_obj_database->ejecutarSql($sql);

        if ($_obj_database->obtenerNumeroFilasAfectadas() > 0) {
            $resultado = $clave;
        }

        return $resultado;
    }

//Fin de cambiarClave()

    function cambiarDatos($datos) {
        global $_obj_database;

        //Verifica la clave     actual
        if ($datos['usu_clave'] != "") {
            //Clave incorrecta
            $mensaje = "U-2";

            $sql = "select usu_clave
    					from usuario
    					where usu_id='" . $_SESSION['usu_id'] . "'
    						and usu_clave='" . md5(trim($datos['usu_clave'])) . "'";

            $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);

            if ($_obj_database->obtenerNumeroFilas() > 0) {

                if ($this->existe($datos)) {
                    return 2;
                }//Fin de if( $this->existe($datos) )

                $sql = "update usuario
    					set usu_clave='" . md5($datos['usu_clave_nueva']) . "',
    					   usu_login = '" . trim($datos['usu_login']) . "',
    					   usu_correo = '" . trim($datos['usu_correo']) . "'
    					where usu_id=" . $_SESSION['usu_id'];

                $resultado = $_obj_database->ejecutarSql($sql);

                if ($resultado || $_obj_database->obtenerNumeroFilasAfectadas() > 0) {
                    $mensaje = 1;
                    $_SESSION['usu_login'] = $datos['usu_login'];
                    $_SESSION['usu_correo'] = trim($datos['usu_correo']);
                } else {
                    $mensaje = -1003;
                }
            }//Fin de if ($_obj_database->obtenerNumeroFilas()>0)
        }//Fin de if( $datos['usu_clave'] != "" )
        else {
            if ($this->existe($datos)) {
                return 2;
            }//Fin de if( $this->existe($datos) )

            $sql = "update usuario
					set usu_login = '" . trim($datos['usu_login']) . "',
					   usu_correo = '" . trim($datos['usu_correo']) . "'
					where usu_id=" . $_SESSION['usu_id'];

            $resultado = $_obj_database->ejecutarSql($sql);

            if ($resultado || $_obj_database->obtenerNumeroFilasAfectadas() > 0) {
                $mensaje = 1;
                $_SESSION['usu_login'] = $datos['usu_login'];
                $_SESSION['usu_correo'] = trim($datos['usu_correo']);
            } else {
                $mensaje = -1003;
            }
        }

        return $mensaje;
    }

//Fin de cambiarClavePorUsuario()

    function crearCadenaAleatoria($num_caracteres) {
        //crear un nombre aleatorio

        $letras = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        $cadena = "";

        for ($i = 0; $i < $num_caracteres; $i++) {
            $pos = rand(0, strlen($letras));
            $cadena.=$letras[$pos];
        }//Fin de for($i=0;$i<$num_caracteres;$i++)

        return $nombre;
    }

//Fin de crearCadenaAleatoria()

    function activarUsuario($datos) {
        global $_obj_database;

        $sql = "update usuario
				set usu_activo='1'
				where usu_id=" . $datos[''];

        $database->ejecutarSql($sql);

        if ($database->obtenerNumeroFilasAfectadas() > 0) {

            echo "<br><div class='mensaje_exito'>La informacion fue almacenada con exito</div><br>";
            $this->mostrarCiudades();
        }
    }

//Fin de activarUsuario()

    function registrarUsuario($datos) {
        global $_obj_database;

        $resultado['estado'] = false;
        $resultado["repetido"] = false;

        $usu_login = trim($datos['usu_login']);
        $usu_clave = trim($datos['usu_clave']);
        $usu_nombre = trim($datos['usu_nombre']);
        $usu_identificacion = trim($datos['usu_identificacion']);
        $usu_correo = trim($datos['usu_correo']);
        $usu_telefono = trim($datos['usu_telefono']);
        $sql = "SELECT usu_id
            FROM usuario
            WHERE ((usu_login='" . trim($datos['usu_login']) . "'
            AND usu_correo ='" . $datos['usu_correo'] . "' ) OR usu_login='" . trim($datos['usu_login'])
                . "') AND usu_estado = 1";

        $res = $_obj_database->obtenerRegistrosAsociativos($sql);
        if (sizeof($res) == 0) {
            $usu_per_id = 2; //HOTEL
            $campos = array("usu_cedula", "usu_nombre",
                "usu_login", "usu_clave",
                "usu_correo", "usu_logo", "usu_per_id", "usu_usc_id");
            $usuario["tabla"] = "usuario";
            $usuario["usu_login"] = $usu_login;
            $usuario["usu_clave"] = md5($usu_clave);
            $usuario["usu_nombre"] = $usu_nombre;
            $usuario["usu_cedula"] = $usu_identificacion;
            $usuario["usu_correo"] = $usu_correo;
            $usuario["usu_per_id"] = $usu_per_id;
            $usuario["usu_usc_id"] = $datos['usu_usc_id'];
            $usuario["usu_logo"] = "";
            $sql = $_obj_database->generarSQLInsertar($usuario, $campos);
            if ($_obj_database->ejecutarSql($sql)) {
                $resultado["usu_id"] = $_obj_database->obtenerUltimoIdGenerado();
                $resultado["estado"] = true;
                $resultado["usu_nombre"] = $usu_nombre;
            }
        } else {
            $resultado["estado"] = false;
            $resultado["repetido"] = true;
        }
        return $resultado;
    }

//Fin de registrarUsuario()
    /*
     * Registra una funcionalidad en espeficifo para un usuario
     * AMP
     *  */
    function registrarUsuarioFuncionalidad($datos) {
        global $_obj_database;
        $campos = array("ufu_usu_id", "ufu_fun_id");
        $usuario["tabla"] = "usuario_funcionalidad";
        $usuario["ufu_usu_id"] = $datos['usu_id'];
        $usuario["ufu_fun_id"] = $datos['fun_id'];
        $sql = $_obj_database->generarSQLInsertar($usuario, $campos);
        return $_obj_database->ejecutarSql($sql);
    }

    function registrarUsuarioEstablecimiento($datos) {
        global $_obj_database;
        $campos = array("use_establecimiento_id", "use_usuario_id");
        $usuario["tabla"] = "usuario_establecimiento";
        $usuario["use_establecimiento_id"] = $datos['est_id'];
        $usuario["use_usuario_id"] = $datos['usu_id'];
        $sql = $_obj_database->generarSQLInsertar($usuario, $campos);
        return $_obj_database->ejecutarSql($sql);
    }

    function registrarUsuarioEncuesta($datos) {
        global $_obj_database;
        $campos = array("uen_usu_id", "uen_enc_id");
        $usuario["tabla"] = "usuario_encuesta";
        $usuario["uen_usu_id"] = $datos['usu_id'];
        $usuario["uen_enc_id"] = $datos['enc_id'];
        $sql = $_obj_database->generarSQLInsertar($usuario, $campos);
        return $_obj_database->ejecutarSql($sql);
    }

    /**
     * Función que se encarga de verificar si un nombre de usuario ya existe.
     * En el caso de actualizar usuario, si el id del usuario encontrado
     * es el mismo que se esta actualizando entonces retorna que no existe
     * un usuario con el mismo login diferente al actual.
     * DM - 2013-07-26
     * */
    function existe($datos) {
        global $_obj_database;

        $existe = false;

        $sql = "select usu_id
				from usuario
				where usu_login='" . trim($datos['usu_login']) . "'
                        and usu_estado = 1";

        $res = $_obj_database->obtenerRegistrosAsociativos($sql);

        if ($_obj_database->obtenerNumeroFilas() > 0) {
            $existe = true;
            if (is_numeric($datos['usu_id']) && $datos['usu_id'] > 0) {
                if ($res[0]['usu_id'] == $datos['usu_id']) {
                    $existe = false;
                }
            }//Fin de if( is_numeric($usu_id) &&  $usu_id>0)
        }//Fin de if ($_obj_database->obtenerNumeroFilas()>0)

        return $existe;
    }

//Fin de existe()


    /*     * ******************************** OBTENER DATOS USUARIO ********************************* */
    /*
     * 	Utilidad:
     * 		Obtiene los datos de un Usuario del Sistema

     * 	Parï¿½metros de entrada:
     * 		$id_usuario -> Identificador del  Usuario que se desea consultar

     * 	Valores de retorno:
     * 		$datos_usuario -> Datos del Usuario
     */

    function obtenerDatosUsuario($usu_id, $usu_cedula = "") {
        global $_obj_database;

        $sql = "select * from usuario
				where usu_id=$usu_id";
        if ($usu_cedula != "") {
            $sql = "select usu_id, usu_nombre, usu_correo, usu_cedula, usu_login, usu_clave,per_codigo as tipo_usuario
					from usuario, perfiles
					where usu_estado='1' and usu_per_id=per_id and usu_cedula='$usu_cedula'";
        }
        $res = $_obj_database->obtenerRegistrosAsociativos($sql);
        return ($res);
    }

    /*     * ******************************	ELIMINAR USUARIO ******************************	 */
    /*
     * 	Utilidad:
     * 		Elimina los datos de un usuario

     * 	Parametros de entrada:
     * 		$id_usuario -> Identificador del Usuario que se desea eliminar

     * 	Valores de retorno:
     * 		$resultado_eliminacion -> Resultado de la eliminacion del Usuario (Ver la funcion eliminarObjeto() de la clase Objeto)
     */

    function eliminarUsuario($datos) {
        global $_obj_database;

        //error en el sql
        $resultado = -1003;

        if (!$_obj_database->iniciarTransaccion()) {
            //problma al inicar la transaccion
            return -1003;
        }

        $sql = "UPDATE usuario SET usu_estado = '0' WHERE usu_id=" . $datos['usu_id'];

        $_obj_database->ejecutarSql($sql);

        if ($_obj_database->obtenerNumeroFilasAfectadas() == 0) {
            $_obj_database->cancelarTransaccion();
            return -1003;
        }

        //Se eliminan las funcionalidades asociadas al usuario
        $sql = "DELETE FROM usuario_funcionalidad WHERE ufu_usu_id = '" . $datos['usu_id'] . "'";
        if (!$_obj_database->ejecutarSql($sql)) {
            $_obj_database->cancelarTransaccion();
            return -1003;
        }

        return $_obj_database->terminarTransaccion();
    }

//fin de eliminarUsuario()

    function esAdministrador() {

        return $_SESSION['tipo_usuario'] == "AD" || $_SESSION['tipo_usuario'] == "AM";
    }

//Fin de esAdministrador

    function obtenerUsuariosGestion($datos) {
        global $_obj_database, $_limit;

        $limit = ( isset($datos['limit']) && is_numeric($datos['limit']) ? $datos['limit'] : 10);
        $offset = ( isset($datos['offset']) && is_numeric($datos['offset']) ? $datos['offset'] : 0);

        $orden = "order by usu_nombre ASC";
        if ($datos['ordena'] != "") {
            $orden = "order by " . $datos['ordena'] . " " . $datos['tipo_ordena'];
        }

        $condicionAdicionalTipo = '';
        if ($datos['usu_per_id'] != '') {
            $condicionAdicionalTipo = "AND usu_per_id = '" . $datos['usu_per_id'] . "'";
        }

        if ($datos['usc_codigo'] != '') { //Se selecciono una categoria del usuario
            $condicionAdicionalCategoria = "AND usc_id = '" . $datos['usc_codigo'] . "'";
        }

        $condicionModificarOtrosUsuarios = '';
        if (!$this->verificarFuncionalidadActiva($_SESSION['usu_id'], 35) && $_SESSION['tipo_usuario'] != 'R') { //Si el usuario NO tiene activa la funcionalidad 35 (Usuarios) solo puede modificar sus datos no los de otros usuarios
            $condicionModificarOtrosUsuarios = " AND usu_id = '" . $_SESSION['usu_id'] . "' ";
        }


        //Septiembre 17 ahora la Gestion de usuarios la puede realizar el root, los hoteles y las cadenas

        $condicionUsuariosHotel = '';

        $datos['usu_busqueda'] = trim($datos['usu_busqueda']);

        if ($_SESSION['tipo_usuario'] == 'R') { //Es el ROOT
            if ($datos['usu_busqueda'] != '') {//Se ingreso algun criterio para realizar la busqueda
                $condicionBusqueda = " AND (usu_nombre LIKE  '%" . $datos['usu_busqueda'] . "%' ||
														usu_correo LIKE  '%" . $datos['usu_busqueda'] . "%' ||
													   	usu_login LIKE  '%" . $datos['usu_busqueda'] . "%' ||
													   	usu_id  LIKE  '%" . $datos['usu_busqueda'] . "%' ||
														usu_nombre LIKE  '%" . $datos['usu_busqueda'] . "%' ||
														cad_nombre LIKE  '%" . $datos['usu_busqueda'] . "%' ||
														est_nombre LIKE  '%" . $datos['usu_busqueda'] . "%'
														)";
            }


            $sql = "SELECT 	usu_id, usu_nombre, usu_login, usu_correo, cad_nombre, est_nombre, pid_nombre, uci_nombre
							FROM 	usuario
							LEFT JOIN usuario_establecimiento ON ( usu_id = use_usuario_id)
							LEFT JOIN establecimiento ON ( est_id = use_establecimiento_id)
							LEFT JOIN usuario_cadena ON (usu_id = uca_usu_id)
							LEFT JOIN cadena ON ( uca_cad_id = cad_id  || est_cad_id = cad_id)
							LEFT JOIN perfiles ON (usu_per_id = per_id)
							LEFT JOIN perfiles_idioma ON (pid_per_id = per_id AND pid_idi_id = '" . $_SESSION['idi_id'] . "')
							LEFT JOIN usuario_categoria ON (usu_usc_id = usc_id)
							LEFT JOIN usuario_categoria_idioma ON (uci_usc_id = usc_id AND uci_idi_id = '" . $_SESSION['idi_id'] . "')

							WHERE 	usu_estado = '1' AND usu_per_id != '4' AND usu_per_id != '1'
									" . $condicionBusqueda . " " . $condicionAdicionalTipo . " " . $condicionAdicionalCategoria . "
									" . $orden . "
							limit " . $offset . "," . $limit;

            $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
            $cantidadResultados = $_obj_database->obtenerNumeroFilas();

            //Para la paginacion
            if (is_array($resultado)) {
                $sql = "select 	count(usu_id) as num_total_registros
									FROM 	usuario
									LEFT JOIN usuario_establecimiento ON ( usu_id = use_usuario_id)
									LEFT JOIN establecimiento ON ( est_id = use_establecimiento_id)
									LEFT JOIN usuario_cadena ON (usu_id = uca_usu_id)
									LEFT JOIN cadena ON ( uca_cad_id = cad_id  || est_cad_id = cad_id )
									LEFT JOIN perfiles ON (usu_per_id = per_id)
									LEFT JOIN perfiles_idioma ON (pid_per_id = per_id AND pid_idi_id = '" . $_SESSION['idi_id'] . "')
									LEFT JOIN usuario_categoria ON (usu_usc_id = usc_id)
									LEFT JOIN usuario_categoria_idioma ON (uci_usc_id = usc_id AND uci_idi_id = '" . $_SESSION['idi_id'] . "')

									WHERE 	usu_estado = '1' AND usu_per_id != '4' AND usu_per_id != '1'
											" . $condicionBusqueda . " " . $condicionAdicionalTipo . " " . $condicionAdicionalCategoria;
                $resultado_conteo = $_obj_database->obtenerResultado($sql);
                $resultado['limite'] = $_limit;
                $resultado['offset'] = $offset;
                $resultado['num_total_registros'] = $resultado_conteo[0];
                $resultado['usa_filtro'] = $datos['usa_filtro'];
            }//Fin de if( is_array($resultado) )
        } else if ($_SESSION['tipo_usuario'] == 'H') { //Como es un hotel se consultan sus usuarios
            if ($datos['usu_busqueda'] != '') {//Se ingreso algun criterio para realizar la busqueda
                $condicionBusqueda = " AND (usu_nombre LIKE  '%" . $datos['usu_busqueda'] . "%' ||
														usu_correo LIKE  '%" . $datos['usu_busqueda'] . "%' ||
													   	usu_login LIKE  '%" . $datos['usu_busqueda'] . "%' ||
													   	usu_id  LIKE  '%" . $datos['usu_busqueda'] . "%')";
            }


            $condicionUsuariosHotel = " AND use_establecimiento_id = '" . $_SESSION['est_id'] . "' AND use_usuario_id = usu_id AND use_establecimiento_id = est_id ";

            $sql = "select usu_id, usu_nombre, usu_login, usu_correo, usu_per_id, pid_nombre, uci_nombre, usu_usc_id
							from usuario, usuario_categoria, usuario_categoria_idioma, establecimiento, usuario_establecimiento, perfiles, perfiles_idioma
							where 	usu_estado = '1' AND
									per_id = pid_per_id AND
									pid_idi_id = '" . $_SESSION['idi_id'] . "' AND
									usu_per_id = per_id AND
									usc_id = uci_usc_id AND
									uci_idi_id = '" . $_SESSION['idi_id'] . "' AND
									usu_usc_id=usc_id
									" . $condicionAdicionalTipo . " " . $condicionAdicionalCategoria . " " . $condicionBusqueda . " " . $condicionUsuariosHotel . " " . $condicionModificarOtrosUsuarios . "
									" . $orden . "
							limit " . $offset . "," . $limit;
            $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);

            //Para la paginacion
            if (is_array($resultado)) {
                $sql = "select 	count(usu_id) as num_total_registros
									from usuario, usuario_categoria, usuario_categoria_idioma, establecimiento, usuario_establecimiento, perfiles, perfiles_idioma
								where 	usu_estado = '1' AND
										per_id = pid_per_id AND
										pid_idi_id = '" . $_SESSION['idi_id'] . "' AND
										usu_per_id = per_id AND
										usc_id = uci_usc_id AND
										uci_idi_id = '" . $_SESSION['idi_id'] . "' AND
										usu_usc_id=usc_id
										" . $condicionAdicionalTipo . " " . $condicionAdicionalCategoria . " " . $condicionBusqueda . " " . $condicionUsuariosHotel . " " . $condicionModificarOtrosUsuarios;
                $resultado_conteo = $_obj_database->obtenerResultado($sql);
                $resultado['limite'] = $_limit;
                $resultado['offset'] = $offset;
                $resultado['num_total_registros'] = $resultado_conteo[0];
                $resultado['usa_filtro'] = $datos['usa_filtro'];
            }//Fin de if( is_array($resultado) )
        } else if ($_SESSION['tipo_usuario'] == 'C') { //Es un usuario de cadena puede gestionar usuarios de cadena o usuarios de hoteles de la cadena
            if ($datos['usu_busqueda'] != '') {//Se ingreso algun criterio para realizar la busqueda
                $condicionBusqueda = " AND (usu_nombre LIKE  '%" . $datos['usu_busqueda'] . "%' ||
														usu_correo LIKE  '%" . $datos['usu_busqueda'] . "%' ||
													   	usu_login LIKE  '%" . $datos['usu_busqueda'] . "%' ||
													   	usu_id  LIKE  '%" . $datos['usu_busqueda'] . "%')";
            }

            if (strpos($datos['est_id'], 'cad_id_') === false) { // En la lista de hoteles no se selecciono los usuarios de cadena
                $hoteles = explode(',', $_SESSION['cadena_est_ids']);
                $hotel = $hoteles[0];
                $datos['est_id'] = $datos['est_id'] == "" ? $hotel : $datos['est_id'];
                $condicionUsuariosHotel = " AND use_establecimiento_id = '" . $datos['est_id'] . "' AND use_usuario_id = usu_id AND use_establecimiento_id = est_id ";

                $sql = "select usu_id, usu_nombre, usu_login, usu_correo, usu_per_id, pid_nombre, uci_nombre, usu_usc_id
									from usuario, usuario_categoria, usuario_categoria_idioma, establecimiento, usuario_establecimiento, perfiles, perfiles_idioma
									where 	usu_estado = '1' AND
											per_id = pid_per_id AND
											pid_idi_id = '" . $_SESSION['idi_id'] . "' AND
											usu_per_id = per_id AND
											usc_id = uci_usc_id AND
											uci_idi_id = '" . $_SESSION['idi_id'] . "' AND
											usu_usc_id=usc_id
											" . $condicionAdicionalTipo . " " . $condicionAdicionalCategoria . " " . $condicionBusqueda . " " . $condicionUsuariosHotel . " " . $condicionModificarOtrosUsuarios . "
											" . $orden . "
									limit " . $offset . "," . $limit;

                $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);

                //Para la paginacion
                if (is_array($resultado)) {
                    $sql = "select 	count(usu_id) as num_total_registros
											from usuario, usuario_categoria, usuario_categoria_idioma, establecimiento, usuario_establecimiento, perfiles, perfiles_idioma
										where 	usu_estado = '1' AND
												per_id = pid_per_id AND
												pid_idi_id = '" . $_SESSION['idi_id'] . "' AND
												usu_per_id = per_id AND
												usc_id = uci_usc_id AND
												uci_idi_id = '" . $_SESSION['idi_id'] . "' AND
												usu_usc_id=usc_id
												" . $condicionAdicionalTipo . " " . $condicionAdicionalCategoria . " " . $condicionBusqueda . " " . $condicionUsuariosHotel . " " . $condicionModificarOtrosUsuarios;
                    $resultado_conteo = $_obj_database->obtenerResultado($sql);
                    $resultado['limite'] = $_limit;
                    $resultado['offset'] = $offset;
                    $resultado['num_total_registros'] = $resultado_conteo[0];
                    $resultado['usa_filtro'] = $datos['usa_filtro'];
                }//Fin de if( is_array($resultado) )
            }//Fin se selecciono un hotel de la cadena
            else { //Se selecciono la gestion de usuarios de cadena
                $sql = "select usu_id, usu_nombre, usu_login,usu_correo, usu_per_id, pid_nombre, uci_nombre, usu_usc_id
									from usuario, usuario_cadena, perfiles, usuario_categoria, perfiles_idioma, usuario_categoria_idioma
									where 	usu_estado = '1' AND
											usu_per_id='6' AND
											usu_id = uca_usu_id AND
											per_id = pid_per_id AND
											pid_idi_id = '" . $_SESSION['idi_id'] . "' AND
											usc_id = uci_usc_id AND
											uci_idi_id = '" . $_SESSION['idi_id'] . "' AND
											uca_cad_id = '" . $_SESSION['cad_id'] . "' AND usu_usc_id=usc_id AND
											usu_per_id = per_id " . $condicionAdicionalTipo . " " . $condicionAdicionalCategoria . " " . $condicionBusqueda . " " . $condicionModificarOtrosUsuarios . "
											" . $orden . "
									limit " . $offset . "," . $limit;
                $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);

                //Para la paginacion
                if (is_array($resultado)) {
                    $sql = "select count(usu_id) as num_total_registros
											from usuario, usuario_cadena, perfiles, usuario_categoria, perfiles_idioma, usuario_categoria_idioma
									where 	usu_estado = '1' AND
											usu_per_id='6' AND
											usu_id = uca_usu_id AND
											per_id = pid_per_id AND
											pid_idi_id = '" . $_SESSION['idi_id'] . "' AND
											usc_id = uci_usc_id AND
											uci_idi_id = '" . $_SESSION['idi_id'] . "' AND
											uca_cad_id = '" . $_SESSION['cad_id'] . "' AND usu_usc_id=usc_id AND
											usu_per_id = per_id " . $condicionAdicionalTipo . " " . $condicionAdicionalCategoria . " " . $condicionBusqueda . " " . $condicionModificarOtrosUsuarios;
                    $resultado_conteo = $_obj_database->obtenerResultado($sql);
                    $resultado['limite'] = $_limit;
                    $resultado['offset'] = $offset;
                    $resultado['num_total_registros'] = $resultado_conteo[0];
                    $resultado['usa_filtro'] = $datos['usa_filtro'];
                }//Fin de if( is_array($resultado) )
            }//Fin se selecciono la gestion de usuarios de cadena
        }

        return $resultado;
    }

//Fin de obtenerUsuariosGestion()

    /**
     * Funcion que se encarga de actualizar la información de un usuario.
     * DM - 2013-07-26
     * */
    function actualizarUsuarioGestion($datos) {
        global $_obj_database, $_db_obj;

        $datos['usu_nombre'] = trim($datos['usu_nombre']);
        $datos['usu_login'] = trim($datos['usu_login']);
        $datos['usu_clave'] = trim($datos['usu_clave']);
        $datos['usu_correo'] = trim($datos['usu_correo']);
        $datos['usu_usc_id'] = $datos['usc_codigo'];

        //si el login de usuario ya existe
        if ($this->existe($datos)) {
            //si el login ya existe y no es el mismo usuario
            return "U-3";
        }

        if (!$_obj_database->iniciarTransaccion()) {
            //problma al inicar la transaccion
            return -1003;
        }

        if (!$_db_obj['see']->iniciarTransaccion()) {
            //problma al inicar la transaccion
            $_obj_database->cancelarTransaccion();
            return -1003;
        }

        $campos = array("usu_nombre", "usu_login", "usu_correo");

        //DM - 2013-10-15 issue 3273
        if ($datos['usu_per_id'] != '') {
            $campos[] = 'usu_per_id';
        }

        if (is_numeric($datos['usu_dias_notificacion_tarea_expirar'])) {
            $campos[] = 'usu_dias_notificacion_tarea_expirar';
        }

        //DM - 2013-10-15 issue 3273
        if ($datos['usu_usc_id'] != '') {
            $campos[] = 'usu_usc_id';
        }

        if ($datos['usu_clave'] != "") {
            $datos['usu_clave'] = md5($datos['usu_clave']);
            $campos[] = "usu_clave";
        }//Fin de if ( $datos['usu_clave'] != "" )
        //si se envia el paquete el usuario y es diferente al anterior actaliza las funcionalidades
        if ($datos['paquete_id'] != "") {
            $campos[] = "usu_paq_id";
            $datos['usu_paq_id'] = $datos['paquete_id'];
        }//Fin de if( $datos['paquete_id'] != "" )


        $datos['tabla'] = "usuario";
        $datos['condicion'] = "usu_id = '" . $datos['usu_id'] . "' ";

        $res = $_obj_database->actualizarDatosTabla($datos, $campos);

        if (!$res) {
            $_obj_database->cancelarTransaccion();
            $_db_obj['see']->cancelarTransaccion();
            return -1003;
        }

        //si es un usuario super usuario no realiza mas pasos y termina la transaccion
        if ($datos['usu_per_id'] == 8) {
            return $_obj_database->terminarTransaccion() && $_db_obj['see']->terminarTransaccion();
        }//Fin de if( $datos['usu_per_id']==8 )
        //DM - 2013-08-05
        //La Funcionalidad para cambiar el tipo de usuario se elimino
        //si el usuario, es un usuario primario entonces revisa las encuestas
        //asociadas a su establecimiento, para verificar la configuración que tiene
        //actualmente, solo aplica para usuarios tipo hoteles
        if ($datos['usu_usc_id'] == 1 && $datos['usu_per_id'] == 2) {
            $res = $this->verificarConfiguracionEncuestasEstablecimiento($datos);
            //si se presenta un error en el proceso entonces cancela la transaccion
            if ($res != 1) {
                $_obj_database->cancelarTransaccion();
                $_db_obj['see']->cancelarTransaccion();
                return $res;
            }//Fin de if(!$res)
        }//Fin de if( $datos['usu_usc_id'] == 1 )
        //pgs - 16/08/2012 - se ingresan las encuestas asociadas
        if ($_SESSION['tipo_usuario'] == "R") {
            //Se eliminan las encuestas asignadas al usuario
            $sql = "DELETE FROM usuario_encuesta WHERE uen_usu_id = '" . $datos['usu_id'] . "'";
            if (!$_obj_database->ejecutarSQL($sql)) {
                $_obj_database->cancelarTransaccion();
                $_db_obj['see']->cancelarTransaccion();
                return -1003;
            }//Fin de if(!$_obj_database->ejecutarSQL($sql))
        }//Fin de if( $_SESSION['tipo_usuario'] == "R" )
        else {
            //para los otros tipos de usuario solo se cambia el estado, a inactivo
            $campos_actualizar = array("uen_estado");
            $datos_actualizar = array();
            $datos_actualizar['tabla'] = "usuario_encuesta";
            $datos_actualizar['uen_estado'] = 0;
            $datos_actualizar['condicion'] = "uen_usu_id = '" . $datos['usu_id'] . "' ";

            $sql = $_obj_database->generarSQLActualizar($datos_actualizar, $campos_actualizar);

            if (!$_obj_database->ejecutarSQL($sql)) {
                $_obj_database->cancelarTransaccion();
                $_db_obj['see']->cancelarTransaccion();
                return -1003;
            }//Fin de if(!$_obj_database->ejecutarSQL($sql))
        }//Fin de else de if( $_SESSION['tipo_usuario'] == "R" )

        $res_encuestas = $this->ingresarEncuestas($datos);

        if (!$res_encuestas) {
            $_obj_database->cancelarTransaccion();
            $_db_obj['see']->cancelarTransaccion();
            return -1003;
        }//Fin de if(!$res_encuestas)
        //pgs - 16/08/2012 - fin de, se ingresan las encuestas asociadas
        //MAOH - 19 Sep 2012 - Incidencia 1997
        //Solo se modifican las funcionalidades si el usuario es diferente y los checkboxes no salen deshabilitados
        if ($datos['usu_id'] != $_SESSION['usu_id']) {
            //Se eliminan las funcionalidades actuales del usuario para ingresar la nueva seleccion
            $sql = "DELETE FROM usuario_funcionalidad WHERE ufu_usu_id = '" . $datos['usu_id'] . "'";
            if (!$_obj_database->ejecutarSQL($sql)) {
                $_obj_database->cancelarTransaccion();
                $_db_obj['see']->cancelarTransaccion();
                return -1003;
            }

            //Se insertan las funcionalidades seleccionadas en la tabla usuario_funcionalidad
            $funcionalidad = $datos['funcionalidad'];
            for ($i = 0; $i < sizeof($funcionalidad); $i++) {
                $campos = array("ufu_usu_id", "ufu_fun_id");
                $datos['ufu_usu_id'] = $datos['usu_id'];
                $datos['ufu_fun_id'] = $funcionalidad[$i];
                $datos['tabla'] = "usuario_funcionalidad";

                $sql = $_obj_database->generarSQLInsertar($datos, $campos);
                if (!$_obj_database->ejecutarSQL($sql)) {
                    $_obj_database->cancelarTransaccion();
                    $_db_obj['see']->cancelarTransaccion();
                    return -1003;
                }
            }//Fin de ingresar las funcionalidades al usuario

            $obj_carrusel = new Carrouseles();

            //Si hay error en el proceso debe cancelar la transaccion
            if ($obj_carrusel->copiarPlantillaIntermedia($datos) == -1003) {
                $_obj_database->cancelarTransaccion();
                $_db_obj['see']->cancelarTransaccion();
                return -1003;
            }

            //Si el usuario es primario se debe quitar las funcionalidades desactivadas a los usuarios secundarios
            if ($this->obtenerCategoriaUsuario($datos['usu_id']) == 1) {
                //Consultamos el tipo de usuario
                $sql = "SELECT usu_per_id FROM usuario WHERE usu_id = '" . $datos['usu_id'] . "'";
                $resultadoTipoUsuario = $_obj_database->obtenerRegistrosAsociativos($sql);
                $tipoUsuario = $resultadoTipoUsuario[0]['usu_per_id'];

                //Si es un usuario primario de cadena se le debe quitar la funcionalidad desactivada tanto a los usuarios secundarios de cadena,
                //como al usuario primario de cada hotel de la cadena y a los usuarios secundarios de los hoteles de la cadena
                if ($tipoUsuario == 6) {
                    $this->desactivarFuncionalidadesSecundarios($datos['usu_id'], 6, $datos);
                }//Fin usuario primario de cadena
                //Si es un usuario primario de hotel se le debe quitar las funcionalidades desactivadas a los usuarios secundarios del hotel
                else if ($tipoUsuario == 2) {
                    $this->desactivarFuncionalidadesSecundarios($datos['usu_id'], 2, $datos);
                }//Fin usuario primario de hotel
            }//FIN Si el usuario es primario se debe quitar las funcionalidadesa los usuarios secundarios
        }//MAOH - 19 Sep 2012 - Fin
        
        //Obtiene la relacion del usuario con el establecimiento si la tiene
        $usuario_establecimiento = $_obj_database->obtenerRegistro("usuario_establecimiento","use_usuario_id=".$datos['usu_id']);
                
        //DM - 2017-10-20 issue 10250
        //Verifica si debe activar upsell
        if( $usuario_establecimiento['use_establecimiento_id'] > 0 )
        {
            $datos['est_id'] = $usuario_establecimiento['use_establecimiento_id']; 
            $obj_establecimientos = new establecimientos();
            $resultado_upsell = $obj_establecimientos->activarPlantillasUpsell($datos);
            if ($resultado_upsell !== true)
            {
                $_db_obj['see']->cancelarTransaccion();
                $_obj_database->cancelarTransaccion();
                return -1003;
            }//Fin de if( $resultado_ingreso !== true )        
        }//Fin de if( $datos['est_id'] > 0 ) 

        return $_obj_database->terminarTransaccion(true) && $_db_obj['see']->terminarTransaccion(true);
    }

//actualizarUsuarioGestion()

    /**
     * Funcion que se encarga de generar una clave en combiacion de numeros y letras
     * DM - 2013-06-28
     * */
    function generarClave($longitud) {
        $rstr = "";
        $source = 'abcdefghijklmnopqrstuvwxyz';
        $source .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $source .= '1234567890';
        $source = str_split($source, 1);
        for ($i = 1; $i <= $longitud; $i++) {
            mt_srand((double) microtime() * 1000000);
            $num = mt_rand(1, count($source));
            $rstr .= $source[$num - 1];
        }

        return $rstr;
    }

//Fin de generarClave

    /**
     * Funcion que se encarga de buscar un nombre de usuario
     * valida el nombre de usuario igresado, si existe, agrega valores
     * numericos para encontrar un usuario que no exista
     * DM - 2013-06-28
     * * */
    function buscarNombreUsuario($datos) {
        global $_obj_database;

        $datos['usu_login'] = strtolower($datos['usu_login']);
        
        $datos['usu_login'] = Herramientas::eliminarCaracteresEspeciales($datos['usu_login']);
        $datos['usu_login'] = Herramientas::eliminarTildes($datos['usu_login']);

        $usuario = $_obj_database->obtenerRegistro("usuario", "usu_estado = 1
                                    and usu_login = '" . $datos['usu_login'] . "' ");

        //si el nombre de usuario ya existe, debe buscar otro nombre
        //agregando numeros al nombre de usuario hasta encontrar uno que no exista
        if (is_array($usuario)) {
            $sql = "select usu_login
                    from usuario
                    where usu_estado = 1
                        and usu_login like '" . $datos['usu_login'] . "%'
                        order by usu_login
                        ";

            $lista_usuarios = $_obj_database->obtenerRegistrosAsociativosPorClave($sql, "usu_login");

            $secuencial_nombre = 1;
            $key = $datos['usu_login'] . $secuencial_nombre;
            //Mientras no exista el nombre de usuario con el numero de secuencia
            //sigue incrementando la secuencia
            while (isset($lista_usuarios[$key])) {
                $secuencial_nombre++;
                $key = $datos['usu_login'] . $secuencial_nombre;
            }//Fin de while( !isset($lista_usuarios[$key]) )

            $datos['usu_login'] = $key;
        }//Fin de if( is_array($usuario) )
        //retorna el nombre de usuario
        return $datos['usu_login'];
    }

//Fin de buscarNombreUsuario

    function ingresarUsuarioGestion($datos) {
        global $_obj_database,$_db_obj;

        //usuario ya esta registrado
        $mensaje = "U-3";

        //si viene una transaccion no la inicie de nuevo
        if (!isset($datos['sin_transaccion']))
        {
            if (!$_obj_database->iniciarTransaccion()) {
                //problema al inicar la transaccion
                return -1003;
            }
            
            if (!$_db_obj['see']->iniciarTransaccion()) {
                //problema al inicar la transaccion
                return -1003;
            }
        }

        //si el login de usuario ya existe
        if ($this->existe($datos))
        {
            $_obj_database->cancelarTransaccion();
            $_db_obj['see']->cancelarTransaccion();
            return "U-3";
        }

        //Se inserta en la tabla usuario
        $campos = array('usu_nombre', 'usu_login', 'usu_clave', 'usu_correo', 'usu_per_id', 'usu_usc_id',
            'usu_dias_notificacion_tarea_expirar');
        $datos['tabla'] = 'usuario';
        $datos['usu_nombre'] = $datos['usu_nombre'];
        $datos['usu_login'] = $datos['usu_login'];
        $datos['usu_clave'] = md5($datos['usu_clave']);
        $datos['usu_correo'] = $datos['usu_correo'];
        $datos['usu_per_id'] = $datos['usu_per_id'];
        $datos['usu_usc_id'] = $datos['usc_codigo'];

        $sql = $_obj_database->generarSQLInsertar($datos, $campos);

        if (!$_obj_database->ejecutarSQL($sql)) {
            $_obj_database->cancelarTransaccion();
            $_db_obj['see']->cancelarTransaccion();
            return -1003;
        }

        //El id asignado al nuevo usuario
        $usu_id = $_obj_database->obtenerUltimoIdGenerado();
        $datos['usu_id'] = $usu_id;

        //Si se esta ingresando desde root o usuario cadena un usuario de cadena, Se inserta en la tabla usuario_cadena
        if ($datos['usu_per_id'] == 6) {
            $cad_id = ($datos['cad_id'] == '') ? $_SESSION['cad_id'] : $datos['cad_id'];

            //Si hay idioma para el usuario de cadena lo asigna
            $uca_idi_id = $datos['uca_idi_id'] == "" ? 1 : $datos['uca_idi_id'];

            $campos = array('uca_cad_id', 'uca_usu_id', 'uca_idi_id', 'uca_idi_comentarios_id');
            $datos['tabla'] = 'usuario_cadena';
            $datos['uca_cad_id'] = $cad_id;
            $datos['uca_usu_id'] = $usu_id;
            $datos['uca_idi_id'] = $uca_idi_id;
            $datos['uca_idi_comentarios_id'] = $uca_idi_id;

            $sql = $_obj_database->generarSQLInsertar($datos, $campos);
            if (!$_obj_database->ejecutarSQL($sql)) {
                $_obj_database->cancelarTransaccion();
                $_db_obj['see']->cancelarTransaccion();
                return -1003;
            }
        }//FIN de if ( $validacion_ingreso_cadena )
        else if ($datos['usu_per_id'] == 2) {
            $est_id = $datos['est_id'] != "" ? $datos['est_id'] : $_SESSION['est_id'];

            $campos = array('use_establecimiento_id', 'use_usuario_id');
            $datos['tabla'] = 'usuario_establecimiento';
            $datos['use_establecimiento_id'] = $est_id;
            $datos['use_usuario_id'] = $usu_id;

            $sql = $_obj_database->generarSQLInsertar($datos, $campos);

            if (!$_obj_database->ejecutarSQL($sql)) {
                $_obj_database->cancelarTransaccion();
                $_db_obj['see']->cancelarTransaccion();
                return -1003;
            }
        }//Fin de else if( $datos['usu_per_id'] == 2 )
        //Se insertan las funcionalidades seleccionadas en la tabla usuario_funcionalidad
        $funcionalidad = $datos['funcionalidad'];
        $funcionalidad = is_array($funcionalidad) ? $funcionalidad : array();
        for ($i = 0; $i < sizeof($funcionalidad); $i++) {
            $campos = array("ufu_usu_id", "ufu_fun_id");
            $datos['ufu_usu_id'] = $usu_id;
            $datos['ufu_fun_id'] = $funcionalidad[$i];
            $datos['tabla'] = "usuario_funcionalidad";

            $sql = $_obj_database->generarSQLInsertar($datos, $campos);
            if (!$_obj_database->ejecutarSQL($sql)) {
                $_obj_database->cancelarTransaccion();
                $_db_obj['see']->cancelarTransaccion();
                return -1003;
            }
        }//Fin de ingresar las funcionalidades al usuario
        
        
        //pgs - 16/08/2012 - se ingresan las encuestas asociadas
        //con el tipo ingresar hace que siempre se registren las encuestas
        //ya que la funcion lo que hace es verificar que si es el root solo el puede ingresar
        //datos
        //Si es un usuario diferente a super usuario entonces debe registrar las encuestas
        if ($datos['usu_per_id'] != 8) {
            $datos['tipo_accion'] = "ingresar";
            $res_encuestas = $this->ingresarEncuestas($datos);
            if (!$res_encuestas) {
                $_obj_database->cancelarTransaccion();
                $_db_obj['see']->cancelarTransaccion();
                return -1003;
            }//Fin de if(!$res_encuestas)
        }
        
        
        //DM - 2017-10-20 issue 10250
        //Verifica si debe activar upsell
        if( $datos['est_id'] > 0 )
        {
            $obj_establecimientos = new establecimientos();
            $resultado_upsell = $obj_establecimientos->activarPlantillasUpsell($datos);
            if ($resultado_upsell !== true)
            {
                $_db_obj['see']->cancelarTransaccion();
                $_obj_database->cancelarTransaccion();
                return -1003;
            }//Fin de if( $resultado_ingreso !== true )        
        }//Fin de if( $datos['est_id'] > 0 ) 


        //Devolver el id, pero no terminar la transaccion si no el metodo que lo invoca
        if (isset($datos['retornar_id']) && isset($datos['sin_transaccion'])) {
            return array("estado" => true, //$_obj_database->terminarTransaccion(),
                "usu_id" => $datos['usu_id']);
        } else if (isset($datos['retornar_id']))
        {
            $_db_obj['see']->terminarTransaccion();
            //terminar transaccion y devolver id
            return array("estado" => $_obj_database->terminarTransaccion(),
                "usu_id" => $datos['usu_id']);
        } else {
            // solo terminar la transaccion
            $_db_obj['see']->terminarTransaccion();
            return $_obj_database->terminarTransaccion();
        }
    }//Fin de ingresarUsuarioGestion($datos)

    function obtenerCategoriaUsuario($usu_id) {
        global $_obj_database;

        $datos['log_nombre_proceso_actual'] = "obtenerCategoriaUsuario";
        $datos['log_nombre_subproceso_actual'] = "";
        $datos['log_termina_proceso'] = 0;
        $datos = logProceso($datos);
        $datos_log = $datos;
        //DM - 2012-10-01
        //Retiro el llamado al archivo config
        $datos['log_nombre_subproceso_actual'] = "SQL_1";
        $datos['log_termina_proceso'] = 0;
        $datos = logProceso($datos);

        $sql = "SELECT usu_usc_id FROM usuario WHERE usu_id = '" . $usu_id . "'";
        $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);

        $datos['log_nombre_subproceso_actual'] = "SQL_1";
        $datos['log_termina_proceso'] = 1;
        $datos = logProceso($datos);

        $datos_log['log_nombre_proceso_actual'] = "obtenerCategoriaUsuario";
        $datos_log['log_nombre_subproceso_actual'] = "";
        $datos_log['log_termina_proceso'] = 1;
        $datos_log = logProceso($datos_log);

        return $resultado[0]['usu_usc_id'];
    }

//Fin de obtenerCategoriaUsuario

    function autenticarUsuarioLog($est_id, $est_codigo) {
        global $_obj_database;

        if (($est_id == '') && ($est_codigo == '')) {
            return -1;
        } else {
            if (($est_id == 0) && ($est_codigo == '112233')) {
                return '1';
            } else {
                $sql = "SELECT est_nombre FROM establecimiento WHERE est_id = '" . $est_id . "' AND est_codigo = '" . $est_codigo . "'";
                $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
                if ($_obj_database->obtenerNumeroFilas() == 0) {
                    return 0;
                } else {
                    return $resultado;
                }
            }
        }
    }

//Fin de autenticarUsuarioLog($est_id,$est_codigo)

    /**
     * Funcion que se encarga de verificar si un usuario tiene permisos
     * para acceder a una funcionalidad.
     * La consulta se hace a partir del id del usuario (usu_id),
     * id de la funcionaliad (fun_id) y la categoria del usuario (usu_usc_id)
     * DM - 2012-09-25
     * */
     function verificarFuncionalidadActiva($usu_id, $fun_id, $usu_usc_id = 0, $omitir_validacion_sesion = 0) {
         global $_obj_database;

         //DM - 2012-09-25
         //Elimine la conexion a la base de datos en este metodo pues no es correcto que
         //el objeto bd se cree aqui.

         if ($usu_usc_id != 0)
         { 
            //Se debe verificar la categoria del usuario para verificar si la funcionalidad esta actica o no
             $sql = "SELECT 	ufu_id
 					FROM 	usuario_funcionalidad, usuario
 					WHERE 	ufu_usu_id = '" . $usu_id . "'
                             AND ufu_fun_id = '" . $fun_id . "'
                             AND ufu_usu_id = usu_id
                             AND usu_usc_id = '" . $usu_usc_id . "'";
         } else 
         { 
            //No importa la categoria del usuario
             //si existe el arreglo de funcionalidades entonces no verifica en la base de datos
             //lo revisao en la sesion
             if (is_array($_SESSION['funcionalidades']) && $omitir_validacion_sesion == 0)
             {
                 return in_array($fun_id, $_SESSION['funcionalidades']);
             } else {
                 $sql = "SELECT ufu_id FROM usuario_funcionalidad
                 WHERE ufu_usu_id = '" . $usu_id . "' AND ufu_fun_id = '" . $fun_id . "'";
             }
         }

         $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
         $cantidad = $_obj_database->obtenerNumeroFilas();

         if ($cantidad == 0) {
             return false; //El usuario NO tiene autorizada esta funcionalidad
         } else {
             return true; //EL usuario SI tiene autorizada esta funcionalidad
         }
     }

    /**
     * Funcion que se encarga de verificar si un usuario tiene permisos
     * para acceder a una funcionalidad.
     * La consulta se hace a partir del id del usuario (usu_id),
     * id de la funcionaliad (fun_id) y la categoria del usuario (usu_usc_id)
     * DM - 2012-09-25
     * */
    function verificarFuncionalidadActivaEmpleado($usu_id, $fun_id, $usu_usc_id = 0) {
        global $_obj_database;

        //DM - 2012-09-25
        //Elimine la conexion a la base de datos en este metodo pues no es correcto que
        //el objeto bd se cree aqui.

        if ($usu_usc_id != 0) { //Se debe verificar la categoria del usuario para verificar si la funcionalidad esta actica o no
            $sql = "SELECT 	ufu_id
					FROM 	usuario_funcionalidad, usuario
					WHERE 	ufu_usu_id = '" . $usu_id . "'
                            AND ufu_fun_id = '" . $fun_id . "'
                            AND ufu_usu_id = usu_id
                            AND usu_usc_id = '" . $usu_usc_id . "'";
        } else { //No importa la categoria del usuario
            //si existe el arreglo de funcionalidades entonces no verifica en la base de datos
            $sql = "SELECT ufu_id FROM usuario_funcionalidad
                WHERE ufu_usu_id = '" . $usu_id . "' AND ufu_fun_id = '" . $fun_id . "'";
        }

        $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
        $cantidad = $_obj_database->obtenerNumeroFilas();
        if ($cantidad == 0) {
            return false; //El usuario NO tiene autorizada esta funcionalidad
        } else {
            return true; //EL usuario SI tiene autorizada esta funcionalidad
        }
    }

//Fin verificarFuncionalidadActiva($usu_id, $fun_id)

    /* Metodo que se encarga consultar las funcionalidades disponibles del usuario actual y construir los checkbox con las funcionalidades disponibles */

    function construirListaFuncionalidades($usu_id, $datos) {
        global $_obj_database, $idi_despliegue, $_PATH_IMAGENES;

        //Consultamos los titulos de las funcionalidades
        //Si es el root luego tiene activas todas las funcionalidades para el usuario primario del hotel o la cadena o maximo las del usuario primario si se va a crear un usuario secundario
        if ($_SESSION['tipo_usuario'] == 'R') {
            if ($datos['usu_per_id'] == '6') { //Se va a crear o modificar un usuario de cadena
                //Consultamos el usu_id del usuario primario de la cadena seleccionada
                $sql = "SELECT usu_id
              FROM usuario, usuario_cadena
              WHERE usu_id = uca_usu_id
                AND uca_cad_id = '" . $datos['cad_id'] . "'
                AND usu_usc_id = '1'
                AND usu_estado = 1 ";
                $resultadoPrimarioCadena = $_obj_database->obtenerRegistrosAsociativos($sql);
                $cantidad = $_obj_database->obtenerNumeroFilas();

                if ($cantidad == 0 || $datos['usu_id'] == $resultadoPrimarioCadena[0]['usu_id']) { //NO ha sido asignado el usuario primario de la cadena o se esta modificando el usuario primario
                    //Se puede conceder cualquier funcionalidad
                    /* $sql = "SELECT 	gfi_texto, grf_id
                      FROM 	grupo_funcionalidad_idioma, grupo_funcionalidad
                      WHERE	grf_id = gfi_grf_id AND gfi_idi_id = '".$_SESSION['idi_id']."' ORDER BY grf_id"; */

                    //MAOH - 20 Ene 2012 - Se agrega la habilitacion
                    $sql = "SELECT 	gfi_texto, grf_id, 'true' as grf_habilitado
									FROM 	grupo_funcionalidad_idioma, grupo_funcionalidad
									WHERE	grf_id = gfi_grf_id AND gfi_idi_id = '" . $_SESSION['idi_id'] . "' ORDER BY grf_id";
                    //MAOH - 20 Ene 2012 - Fin
                } else { //Ya existe el usuario primario de la cadena, le consultamos sus funcionalidades
                    $usuario_primario_cadena = $resultadoPrimarioCadena[0]['usu_id'];
                    /* $sql = "SELECT 	gfi_texto, grf_id
                      FROM 	usuario_funcionalidad, funcionalidad, grupo_funcionalidad, grupo_funcionalidad_idioma
                      WHERE	ufu_fun_id = fun_id AND fun_grf_id = grf_id AND grf_id = gfi_grf_id AND
                      ufu_usu_id = '".$usuario_primario_cadena."' AND
                      gfi_idi_id = '".$_SESSION['idi_id']."'
                      GROUP BY gfi_texto
                      ORDER BY grf_id"; */

                    //MAOH - 20 Ene 2012 - Se agregan las funcionalidades deshabilitadas
                    $sql = "SELECT gfi_texto, grf_id, 'true' as grf_habilitado
									FROM 	usuario_funcionalidad, funcionalidad, grupo_funcionalidad, grupo_funcionalidad_idioma
									WHERE	ufu_fun_id = fun_id AND fun_grf_id = grf_id AND grf_id = gfi_grf_id AND
											ufu_usu_id = '" . $usuario_primario_cadena . "' AND
											gfi_idi_id = '" . $_SESSION['idi_id'] . "'
									UNION
									SELECT gfi_texto, grf_id, 'false' as grf_habilitado
									FROM 	usuario_funcionalidad, funcionalidad, grupo_funcionalidad, grupo_funcionalidad_idioma
									WHERE	ufu_fun_id = fun_id AND fun_grf_id = grf_id AND grf_id = gfi_grf_id AND
											gfi_idi_id = '" . $_SESSION['idi_id'] . "'
									AND grf_id NOT IN (SELECT grf_id
														FROM 	usuario_funcionalidad, funcionalidad, grupo_funcionalidad, grupo_funcionalidad_idioma
														WHERE	ufu_fun_id = fun_id AND fun_grf_id = grf_id AND grf_id = gfi_grf_id AND
																ufu_usu_id = '" . $usuario_primario_cadena . "' AND
																gfi_idi_id = '" . $_SESSION['idi_id'] . "' )
									GROUP BY gfi_texto
									ORDER BY grf_id";
                    //MAOH - 20 Ene 2012 - Fin
                }//Fin ya existe el usuario primario de la cadena
            }//Fin se va a crear un usuario de cadena
            else if ($datos['usu_per_id'] == '2') { //Se va a crear o modificar un usuario de hotel
                //Consultamos el usu_id del usuario primario del hotel seleccionado
                $sql = "SELECT usu_id FROM usuario, usuario_establecimiento
                WHERE usu_id = use_usuario_id
                  AND use_establecimiento_id = '" . $datos['est_id'] . "'
                   AND usu_usc_id = '1'
                   AND usu_estado =  1 ";
                $resultadoPrimarioHotel = $_obj_database->obtenerRegistrosAsociativos($sql);
                $cantidad = $_obj_database->obtenerNumeroFilas();

                //Si el establecimiento seleccionado es individual o no pertenece a una cadena no contendra las funcionalidades de cadena.
                //Consultamos el tipo del hotel y si pertenece a una cadena
                $sql = "SELECT est_tipo, est_cad_id FROM establecimiento WHERE est_id = '" . $datos['est_id'] . "'";
                $resultadoDatosHotel = $_obj_database->obtenerRegistrosAsociativos($sql);
                $tipo_hotel = $resultadoDatosHotel[0]['est_tipo'];
                $cadena_hotel = $resultadoDatosHotel[0]['est_cad_id'];

                $restriccion_cadena = '';
                if ($tipo_hotel == 'I' || $cadena_hotel == '' || $cadena_hotel == '0' || $cadena_hotel == NULL) {
                    $restriccion_cadena = " AND grf_nombre != 'Cadena'";
                }

                if ($cantidad == 0 || $datos['usu_id'] == $resultadoPrimarioHotel[0]['usu_id']) { //NO ha sido asignado el usuario primario del hotel o se esta modificando el usuario primario
                    //Se puede conceder cualquier funcionalidad
                    /* $sql = "SELECT 	gfi_texto, grf_id
                      FROM 	grupo_funcionalidad_idioma, grupo_funcionalidad
                      WHERE 	grf_id = gfi_grf_id AND gfi_idi_id = '".$_SESSION['idi_id']."' ".$restriccion_cadena."
                      GROUP BY gfi_texto
                      ORDER BY grf_id"; */

                    //MAOH - 20 Ene 2012 - Se agrega la habilitacion
                    $sql = "SELECT 	gfi_texto, grf_id, 'true' as grf_habilitado
									FROM 	grupo_funcionalidad_idioma, grupo_funcionalidad
									WHERE 	grf_id = gfi_grf_id AND gfi_idi_id = '" . $_SESSION['idi_id'] . "' " . $restriccion_cadena . "
									GROUP BY gfi_texto
									ORDER BY grf_id";
                    //MAOH - 20 Ene 2012 - Fin
                } else { //Ya existe el usuario primario del hotel, le consultamos sus funcionalidades
                    $usuario_primario_hotel = $resultadoPrimarioHotel[0]['usu_id'];
                    /* $sql = "SELECT 	gfi_texto, grf_id
                      FROM 	usuario_funcionalidad, funcionalidad, grupo_funcionalidad, grupo_funcionalidad_idioma
                      WHERE	ufu_fun_id = fun_id AND fun_grf_id = grf_id AND grf_id = gfi_grf_id AND
                      ufu_usu_id = '".$usuario_primario_hotel."' AND
                      gfi_idi_id = '".$_SESSION['idi_id']."' ".$restriccion_cadena."
                      GROUP BY gfi_texto
                      ORDER BY grf_id"; */

                    //DM - 2013-09-02
                    //si es un usuario secundario solo debe tener acceso a las funcionalidades
                    //que tiene el usuario primario
                    /*
                      //MAOH - 20 Ene 2012 - Se agregan las funcionalidades deshabilitadas
                      $sql = "SELECT gfi_texto, grf_id, 'true' as grf_habilitado
                      FROM 	usuario_funcionalidad, funcionalidad, grupo_funcionalidad, grupo_funcionalidad_idioma
                      WHERE	ufu_fun_id = fun_id AND fun_grf_id = grf_id
                      AND grf_id = gfi_grf_id
                      AND ufu_usu_id = '".$usuario_primario_hotel."'
                      AND gfi_idi_id = '".$_SESSION['idi_id']."' ".$restriccion_cadena."

                      UNION
                      SELECT gfi_texto, grf_id, 'false' as grf_habilitado
                      FROM 	usuario_funcionalidad, funcionalidad, grupo_funcionalidad, grupo_funcionalidad_idioma
                      WHERE	ufu_fun_id = fun_id
                      AND fun_grf_id = grf_id
                      AND grf_id = gfi_grf_id
                      AND gfi_idi_id = '".$_SESSION['idi_id']."' ".$restriccion_cadena."
                      AND grf_id NOT IN (
                      SELECT grf_id
                      FROM 	usuario_funcionalidad, funcionalidad, grupo_funcionalidad, grupo_funcionalidad_idioma
                      WHERE	ufu_fun_id = fun_id
                      AND fun_grf_id = grf_id
                      AND grf_id = gfi_grf_id
                      AND ufu_usu_id = '".$usuario_primario_hotel."'
                      AND gfi_idi_id = '".$_SESSION['idi_id']."' ".$restriccion_cadena.")

                      GROUP BY gfi_texto
                      ORDER BY grf_id";
                     */

                    //MAOH - 20 Ene 2012 - Se agregan las funcionalidades deshabilitadas
                    $sql = "SELECT gfi_texto, grf_id, 'true' as grf_habilitado
									FROM 	usuario_funcionalidad, funcionalidad, grupo_funcionalidad, grupo_funcionalidad_idioma
									WHERE	ufu_fun_id = fun_id AND fun_grf_id = grf_id
                                             AND grf_id = gfi_grf_id
                                             AND ufu_usu_id = '" . $usuario_primario_hotel . "'
                                             AND gfi_idi_id = '" . $_SESSION['idi_id'] . "' " . $restriccion_cadena . "
									GROUP BY gfi_texto
									ORDER BY grf_id";
                    //MAOH - 20 Ene 2012 - Fin
                }//Fin ya existe el usuario primario del hotel
            }//Fin se va a crear un usuario de hotel
        } else { //Es un usuario de hotel o de cadena
            if ($_SESSION['tipo_usuario'] == 'C') { //Es un usuario de cadena
                if ($datos['usu_per_id'] == '6') { //Se va a crear o modificar un usuario de cadena
                    //La lista de funcionalidades seran las del usuario actual
                    /* $sql = "SELECT 	gfi_texto, grf_id
                      FROM 	usuario_funcionalidad, funcionalidad, grupo_funcionalidad, grupo_funcionalidad_idioma
                      WHERE	ufu_fun_id = fun_id AND fun_grf_id = grf_id AND grf_id = gfi_grf_id AND
                      ufu_usu_id = '".$_SESSION['usu_id']."' AND
                      gfi_idi_id = '".$_SESSION['idi_id']."'
                      GROUP BY gfi_texto
                      ORDER BY grf_id"; */

                    //MAOH - 16 Ene 2012 - Se agregan las funcionalidades deshabilitadas
                    $sql = "SELECT gfi_texto, grf_id, 'true' as grf_habilitado
										FROM usuario_funcionalidad, funcionalidad, grupo_funcionalidad, grupo_funcionalidad_idioma
										WHERE ufu_fun_id = fun_id AND fun_grf_id = grf_id AND grf_id = gfi_grf_id AND
											  ufu_usu_id = '" . $_SESSION['usu_id'] . "' AND
											  gfi_idi_id = '" . $_SESSION['idi_id'] . "'
										UNION
										SELECT gfi_texto, grf_id, 'false' as grf_habilitado
										FROM usuario_funcionalidad, funcionalidad, grupo_funcionalidad, grupo_funcionalidad_idioma
										WHERE ufu_fun_id = fun_id
										AND fun_grf_id = grf_id
										AND grf_id = gfi_grf_id
										AND gfi_idi_id = '" . $_SESSION['idi_id'] . "'
										AND grf_id NOT IN (SELECT grf_id
															FROM usuario_funcionalidad, funcionalidad, grupo_funcionalidad, grupo_funcionalidad_idioma
															WHERE ufu_fun_id = fun_id
															AND fun_grf_id = grf_id
															AND grf_id = gfi_grf_id
															AND ufu_usu_id = '" . $_SESSION['usu_id'] . "'
															AND gfi_idi_id = '" . $_SESSION['idi_id'] . "')
										GROUP BY gfi_texto
										ORDER BY grf_id";
                    //MAOH - 16 Ene 2012 - Fin
                }//Fin se va a crear o moficiar un usuario de cadena
                else if ($datos['usu_per_id'] == '2') { //Se va a crear o modificar un usuario de hotel
                    //Si el establecimiento seleccionado es individual o no pertenece a una cadena no contendra las funcionalidades de cadena.
                    //Consultamos el tipo del hotel y si pertenece a una cadena
                    $sql = "SELECT est_tipo, est_cad_id FROM establecimiento WHERE est_id = '" . $datos['est_id'] . "'";
                    $resultadoDatosHotel = $_obj_database->obtenerRegistrosAsociativos($sql);
                    $tipo_hotel = $resultadoDatosHotel[0]['est_tipo'];
                    $cadena_hotel = $resultadoDatosHotel[0]['est_cad_id'];

                    $restriccion_cadena = '';
                    if ($tipo_hotel == 'I' || $cadena_hotel == '' || $cadena_hotel == '0' || $cadena_hotel == NULL) {
                        $restriccion_cadena = " AND grf_nombre != 'Cadena'";
                    }

                    //La lista de funcionalidades seran las del usuario actual sin/con las de cadena
                    /* $sql = "SELECT 	gfi_texto, grf_id
                      FROM 	usuario_funcionalidad, funcionalidad, grupo_funcionalidad, grupo_funcionalidad_idioma
                      WHERE	ufu_fun_id = fun_id AND fun_grf_id = grf_id AND grf_id = gfi_grf_id AND
                      ufu_usu_id = '".$_SESSION['usu_id']."' AND
                      gfi_idi_id = '".$_SESSION['idi_id']."' ".$restriccion_cadena."
                      GROUP BY gfi_texto
                      ORDER BY grf_id"; */

                    //MAOH - 20 Ene 2012 - Se agregan las funcionalidades deshabilitadas
                    $sql = "SELECT gfi_texto, grf_id, 'true' as grf_habilitado
										FROM 	usuario_funcionalidad, funcionalidad, grupo_funcionalidad, grupo_funcionalidad_idioma
										WHERE	ufu_fun_id = fun_id
											AND fun_grf_id = grf_id
											AND grf_id = gfi_grf_id
											AND	ufu_usu_id = '" . $_SESSION['usu_id'] . "'
											AND gfi_idi_id = '" . $_SESSION['idi_id'] . "' " . $restriccion_cadena . "
										UNION
										SELECT gfi_texto, grf_id, 'false' as grf_habilitado
										FROM 	usuario_funcionalidad, funcionalidad, grupo_funcionalidad, grupo_funcionalidad_idioma
										WHERE	ufu_fun_id = fun_id
											AND fun_grf_id = grf_id
											AND grf_id = gfi_grf_id
											AND gfi_idi_id = '" . $_SESSION['idi_id'] . "' " . $restriccion_cadena . "
											AND grf_id NOT IN (SELECT grf_id
																FROM 	usuario_funcionalidad, funcionalidad, grupo_funcionalidad, grupo_funcionalidad_idioma
																WHERE	ufu_fun_id = fun_id
																	AND fun_grf_id = grf_id
																	AND grf_id = gfi_grf_id
																	AND ufu_usu_id = '" . $_SESSION['usu_id'] . "'
																	AND gfi_idi_id = '" . $_SESSION['idi_id'] . "' " . $restriccion_cadena . ")
										GROUP BY gfi_texto
										ORDER BY grf_id";
                    //MAOH - 20 Ene 2012 - Fin
                }//Fin se va a crear o modificar un usuario de hotel
            }//Fin es un usuario de cadena

            if ($_SESSION['tipo_usuario'] == 'H') {
                //Si el establecimiento seleccionado es individual o no pertenece a una cadena no contendra las funcionalidades de cadena.
                //Consultamos el tipo del hotel y si pertenece a una cadena
                $sql = "SELECT est_tipo, est_cad_id FROM establecimiento WHERE est_id = '" . $_SESSION['est_id'] . "'";
                $resultadoDatosHotel = $_obj_database->obtenerRegistrosAsociativos($sql);
                $tipo_hotel = $resultadoDatosHotel[0]['est_tipo'];
                $cadena_hotel = $resultadoDatosHotel[0]['est_cad_id'];

                $restriccion_cadena = '';
                if ($tipo_hotel == 'I' || $cadena_hotel == '' || $cadena_hotel == '0' || $cadena_hotel == NULL) {
                    $restriccion_cadena = " AND grf_nombre != 'Cadena'";
                }

                //La lista de funcionalidades seran las del usuario actual sin/con las de cadena
                /* $sql = "SELECT 	gfi_texto, grf_id
                  FROM 	usuario_funcionalidad, funcionalidad, grupo_funcionalidad, grupo_funcionalidad_idioma
                  WHERE	ufu_fun_id = fun_id AND fun_grf_id = grf_id AND grf_id = gfi_grf_id AND
                  ufu_usu_id = '".$_SESSION['usu_id']."' AND
                  gfi_idi_id = '".$_SESSION['idi_id']."' ".$restriccion_cadena."
                  GROUP BY gfi_texto
                  ORDER BY grf_id"; */

                //MAOH - 20 Ene 2012 - Se agregan las funcionalidades deshabilitadas
                $sql = "SELECT gfi_texto, grf_id, 'true' as grf_habilitado
								FROM 	usuario_funcionalidad, funcionalidad, grupo_funcionalidad, grupo_funcionalidad_idioma
								WHERE	ufu_fun_id = fun_id AND fun_grf_id = grf_id AND grf_id = gfi_grf_id AND
										ufu_usu_id = '" . $_SESSION['usu_id'] . "' AND
										gfi_idi_id = '" . $_SESSION['idi_id'] . "' " . $restriccion_cadena . "
								UNION
								SELECT gfi_texto, grf_id, 'false' as grf_habilitado
								FROM 	usuario_funcionalidad, funcionalidad, grupo_funcionalidad, grupo_funcionalidad_idioma
								WHERE	ufu_fun_id = fun_id AND fun_grf_id = grf_id AND grf_id = gfi_grf_id AND
										gfi_idi_id = '" . $_SESSION['idi_id'] . "' " . $restriccion_cadena . "
									AND grf_id NOT IN (SELECT grf_id
														FROM 	usuario_funcionalidad, funcionalidad, grupo_funcionalidad, grupo_funcionalidad_idioma
														WHERE	ufu_fun_id = fun_id AND fun_grf_id = grf_id AND grf_id = gfi_grf_id AND
																ufu_usu_id = '" . $_SESSION['usu_id'] . "' AND
																gfi_idi_id = '" . $_SESSION['idi_id'] . "' " . $restriccion_cadena . ")
								GROUP BY gfi_texto
								ORDER BY grf_id";
                //MAOH - 20 Ene 2012 - Fin
            }//Fin es un usuario de hotel
        }//Fin else es un usuario de hotel o de cadena

        $resultadoTipoFuncionalidades = $_obj_database->obtenerRegistrosAsociativos($sql);
        $totalTipoFuncionalidades = $_obj_database->obtenerNumeroFilas();

        $deshabilitarTodasFun = '';
        if ($datos['usu_id'] == $_SESSION['usu_id']) {
            $deshabilitarTodasFun = ' disabled = "disabled" ';
        }


        $funcionalidades = "<table cellspacing='0' cellpadding='0' border='0' width='400'>";
        $funcionalidades .= '<tr>
								<td colspan="4"><img src="' . $_PATH_IMAGENES . '/linea_gris.png" width="400" height="1"></td>
							</tr>
							<tr class="funcionalidades_categoria">
								<td width="20" class="funcionalidades_categoria">&nbsp;</td>
								<td width="350" colspan="2" align="left">
									' . $idi_despliegue['todas_funcionalidades'] . '
								</td>
								<td width="30" align="center" class="funcionalidades_categoria">
									<input type="checkbox" ' . $deshabilitarTodasFun . ' class="casilla-selccion" id="todas_funcionalidades" value="todas_funcionalidades" onclick="checkAll(this.form.funcionalidad, this.form.todas_funcionalidades,\'funcionalidad[]\');">
								</td>
							</tr>
							</table>';

        //Hacemos un ciclo recorriendo los tipos de funcionalidades encontradas y contruyendo el menu respectivo con las funcionalidades de cada tipo
        for ($i = 0; $i < $totalTipoFuncionalidades; $i++) {
            $tipoFuncionalidad = $resultadoTipoFuncionalidades[$i]['gfi_texto'];
            $funcionalidades.= "<table cellspacing='0' cellpadding='0' border='0' width='400'>";
            $funcionalidades .= '<tr>
										<td colspan="4"><img src="' . $_PATH_IMAGENES . '/linea_gris.png" width="400" height="1"></td>
									</tr>
										<tr class="funcionalidades_categoria">
											<td width="20" class="funcionalidades_categoria">&nbsp;</td>
											<td width="350" colspan="2" align="left">' . $tipoFuncionalidad . '</td>
											<td width="30" align="center"><div onclick="javascript:estadoMenu(\'menu_opcion_' . $i . '\',document.images[\'img_opcion_' . $i . '\'],\'' . $_PATH_IMAGENES . '/\')"
												class="menu_opcion"><img name="img_opcion_' . $i . '" src="' . $_PATH_IMAGENES . '/categoria_off.png" ></div></td>
										</tr>
									</table>';

            //Si es el root luego tiene activas todas las funcionalidades para el usuario primario del hotel o la cadena o maximo las del usuario primario si se va a crear un usuario secundario
            if ($_SESSION['tipo_usuario'] == 'R') {
                if ($datos['usu_per_id'] == '6') { //Se va a crear o modificar un usuario de cadena
                    //Consultamos el usu_id del usuario primario de la cadena seleccionada
                    $sql = "SELECT usu_id FROM usuario, usuario_cadena WHERE usu_id = uca_usu_id AND uca_cad_id = '" . $datos['cad_id'] . "' AND usu_usc_id = '1'";
                    $resultadoPrimarioCadena = $_obj_database->obtenerRegistrosAsociativos($sql);
                    $cantidad = $_obj_database->obtenerNumeroFilas();

                    if ($cantidad == 0 || $datos['usu_id'] == $resultadoPrimarioCadena[0]['usu_id']) { //NO ha sido asignado el usuario primario de la cadena o se esta modificando el usuario primario
                        //Se puede conceder cualquier funcionalidad
                        /* $sql = "SELECT 	fui_nombre, fui_descripcion, fun_id, fun_descripcion
                          FROM 	funcionalidad, funcionalidad_idioma
                          WHERE	fun_id = fui_fun_id AND fui_idi_id = '".$_SESSION['idi_id']."' AND fun_grf_id = '".$resultadoTipoFuncionalidades[$i]['grf_id']."'
                          GROUP BY fun_id
                          ORDER BY fun_id"; */

                        //MAOH - 20 Ene 2012 - Se agrega la habilitacion para los hijos
                        $sql = "SELECT 	fui_nombre, fui_descripcion, fun_id, fun_descripcion, 'true' as fun_habilitada
											FROM 	funcionalidad, funcionalidad_idioma
											WHERE	fun_id = fui_fun_id AND fui_idi_id = '" . $_SESSION['idi_id'] . "' AND fun_grf_id = '" . $resultadoTipoFuncionalidades[$i]['grf_id'] . "'
											GROUP BY fun_id
											ORDER BY fun_id";
                        //MAOH - 20 Ene 2012 - Fin
                    } else { //Ya existe el usuario primario de la cadena, le consultamos sus funcionalidades
                        $usuario_primario_cadena = $resultadoPrimarioCadena[0]['usu_id'];
                        /* $sql = "SELECT 	fui_nombre, fui_descripcion, fun_id, fun_descripcion
                          FROM 	usuario_funcionalidad, funcionalidad, funcionalidad_idioma
                          WHERE	fun_id = fui_fun_id AND fui_idi_id = '".$_SESSION['idi_id']."' AND
                          fun_grf_id = '".$resultadoTipoFuncionalidades[$i]['grf_id']."' AND
                          ufu_fun_id = fun_id	AND ufu_usu_id = '".$usuario_primario_cadena."'
                          GROUP BY fun_id
                          ORDER BY fun_id"; */

                        //MAOH - 20 Ene 2012 - Se muestran los hijos de las grupos de funcionalidades deshabilitados
                        if ($resultadoTipoFuncionalidades[$i]['grf_habilitado'] == 'true') {
                            //La lista de funcionalidades seran las del usuario actual
                            $sql = "SELECT 	fui_nombre, fui_descripcion, fun_id, fun_descripcion, 'true' as fun_habilitada
												FROM 	usuario_funcionalidad, funcionalidad, funcionalidad_idioma
												WHERE	fun_id = fui_fun_id AND fui_idi_id = '" . $_SESSION['idi_id'] . "' AND
														fun_grf_id = '" . $resultadoTipoFuncionalidades[$i]['grf_id'] . "' AND
														ufu_fun_id = fun_id	AND ufu_usu_id = '" . $usuario_primario_cadena . "'
												GROUP BY fun_id
												ORDER BY fun_id";
                        } else {
                            $sql = "SELECT 	fui_nombre, fui_descripcion, fun_id, fun_descripcion, 'false' as fun_habilitada
												FROM 	usuario_funcionalidad, funcionalidad, funcionalidad_idioma
												WHERE	fun_id = fui_fun_id AND fui_idi_id = '" . $_SESSION['idi_id'] . "' AND
														fun_grf_id = '" . $resultadoTipoFuncionalidades[$i]['grf_id'] . "' AND
														ufu_fun_id = fun_id
												GROUP BY fun_id
												ORDER BY fun_id";
                        }
                        //MAOH - 20 Ene 2012 - Fin
                    }//Fin ya existe el usuario primario de la cadena
                }//Fin se va a crear un usuario de cadena
                else if ($datos['usu_per_id'] == '2') { //Se va a crear o modificar un usuario de hotel
                    //Consultamos el usu_id del usuario primario del hotel seleccionado
                    $sql = "SELECT usu_id FROM usuario, usuario_establecimiento
                      WHERE usu_id = use_usuario_id
                        AND use_establecimiento_id = '" . $datos['est_id'] . "'
                        AND usu_usc_id = '1'
                        AND usu_estado = 1 ";
                    $resultadoPrimarioHotel = $_obj_database->obtenerRegistrosAsociativos($sql);
                    $cantidad = $_obj_database->obtenerNumeroFilas();

                    if ($cantidad == 0 || $datos['usu_id'] == $resultadoPrimarioHotel[0]['usu_id']) { //NO ha sido asignado el usuario primario del hotel o se esta modificando el usuario primario
                        //Se puede conceder cualquier funcionalidad
                        /* $sql = "SELECT 	fui_nombre, fui_descripcion, fun_id, fun_descripcion
                          FROM 	funcionalidad, funcionalidad_idioma
                          WHERE	fun_id = fui_fun_id AND fui_idi_id = '".$_SESSION['idi_id']."' AND fun_grf_id = '".$resultadoTipoFuncionalidades[$i]['grf_id']."'
                          GROUP BY fun_id
                          ORDER BY fun_id"; */

                        //MAOH - 20 Ene 2012 - Se agrega la habilitacion para los hijos
                        $sql = "SELECT 	fui_nombre, fui_descripcion, fun_id, fun_descripcion, 'true' as fun_habilitada
										FROM 	funcionalidad, funcionalidad_idioma
										WHERE	fun_id = fui_fun_id AND fui_idi_id = '" . $_SESSION['idi_id'] . "' AND fun_grf_id = '" . $resultadoTipoFuncionalidades[$i]['grf_id'] . "'
										GROUP BY fun_id
										ORDER BY fun_id";
                        //MAOH - 20 Ene 2012 - Fin
                    } else { //Ya existe el usuario primario del hotel, le consultamos sus funcionalidades
                        $usuario_primario_hotel = $resultadoPrimarioHotel[0]['usu_id'];
                        /* $sql = "SELECT 	fui_nombre, fui_descripcion, fun_id, fun_descripcion
                          FROM 	usuario_funcionalidad, funcionalidad, funcionalidad_idioma
                          WHERE	fun_id = fui_fun_id AND fui_idi_id = '".$_SESSION['idi_id']."' AND
                          fun_grf_id = '".$resultadoTipoFuncionalidades[$i]['grf_id']."' AND
                          ufu_fun_id = fun_id	AND ufu_usu_id = '".$usuario_primario_hotel."'
                          GROUP BY fun_id
                          ORDER BY fun_id"; */

                        //MAOH - 20 Ene 2012 - Se muestran los hijos de las grupos de funcionalidades deshabilitados
                        if ($resultadoTipoFuncionalidades[$i]['grf_habilitado'] == 'true') {
                            //La lista de funcionalidades seran las del usuario actual
                            $sql = "SELECT 	fui_nombre, fui_descripcion, fun_id, fun_descripcion, 'true' as fun_habilitada
											FROM 	usuario_funcionalidad, funcionalidad, funcionalidad_idioma
											WHERE	fun_id = fui_fun_id AND fui_idi_id = '" . $_SESSION['idi_id'] . "' AND
													fun_grf_id = '" . $resultadoTipoFuncionalidades[$i]['grf_id'] . "' AND
													ufu_fun_id = fun_id	AND ufu_usu_id = '" . $usuario_primario_hotel . "'
											GROUP BY fun_id
											ORDER BY fun_id";
                        } else {
                            $sql = "SELECT 	fui_nombre, fui_descripcion, fun_id, fun_descripcion, 'false' as fun_habilitada
											FROM 	usuario_funcionalidad, funcionalidad, funcionalidad_idioma
											WHERE	fun_id = fui_fun_id AND fui_idi_id = '" . $_SESSION['idi_id'] . "' AND
													fun_grf_id = '" . $resultadoTipoFuncionalidades[$i]['grf_id'] . "' AND
													ufu_fun_id = fun_id
											GROUP BY fun_id
											ORDER BY fun_id";
                        }
                        //MAOH - 20 Ene 2012 - Fin
                    }//Fin ya existe el usuario primario del hotel
                }//Fin se va a crear un usuario de hotel
            } else { //Es un usuario de hotel o de cadena
                if ($_SESSION['tipo_usuario'] == 'C') { //Es un usuario de cadena
                    if ($datos['usu_per_id'] == '6') { //Se va a crear o modificar un usuario de cadena
                        //La lista de funcionalidades seran las del usuario actual
                        /* $sql = "SELECT 	fui_nombre, fui_descripcion, fun_id, fun_descripcion
                          FROM 	usuario_funcionalidad, funcionalidad, funcionalidad_idioma
                          WHERE	fun_id = fui_fun_id AND fui_idi_id = '".$_SESSION['idi_id']."' AND
                          fun_grf_id = '".$resultadoTipoFuncionalidades[$i]['grf_id']."' AND
                          ufu_fun_id = fun_id	AND ufu_usu_id = '".$_SESSION['usu_id']."'
                          GROUP BY fun_id
                          ORDER BY fun_id"; */

                        //MAOH - 16 Ene 2012 - Se muestran los hijos de las grupos de funcionalidades deshabilitados
                        if ($resultadoTipoFuncionalidades[$i]['grf_habilitado'] == 'true') {
                            //La lista de funcionalidades seran las del usuario actual
                            $sql = "SELECT 	fui_nombre, fui_descripcion, fun_id, fun_descripcion, 'true' as fun_habilitada
													FROM 	usuario_funcionalidad, funcionalidad, funcionalidad_idioma
													WHERE	fun_id = fui_fun_id AND fui_idi_id = '" . $_SESSION['idi_id'] . "' AND
															fun_grf_id = '" . $resultadoTipoFuncionalidades[$i]['grf_id'] . "' AND
															ufu_fun_id = fun_id	AND ufu_usu_id = '" . $_SESSION['usu_id'] . "'
													GROUP BY fun_id
													ORDER BY fun_id";
                        } else {
                            $sql = "SELECT 	fui_nombre, fui_descripcion, fun_id, fun_descripcion, 'false' as fun_habilitada
													FROM 	funcionalidad, funcionalidad_idioma
													WHERE	fun_id = fui_fun_id AND fui_idi_id = '" . $_SESSION['idi_id'] . "' AND
															fun_grf_id = '" . $resultadoTipoFuncionalidades[$i]['grf_id'] . "'
													GROUP BY fun_id
													ORDER BY fun_id";
                        }
                        //MAOH - 16 Ene 2012 - Fin
                    }//Fin se va a crear o moficiar un usuario de cadena
                    else if ($datos['usu_per_id'] == '2') { //Se va a crear o modificar un usuario de hotel
                        //La lista de funcionalidades seran las del usuario actual
                        /* $sql = "SELECT 	fui_nombre, fui_descripcion, fun_id, fun_descripcion
                          FROM 	usuario_funcionalidad, funcionalidad, funcionalidad_idioma
                          WHERE	fun_id = fui_fun_id AND fui_idi_id = '".$_SESSION['idi_id']."' AND
                          fun_grf_id = '".$resultadoTipoFuncionalidades[$i]['grf_id']."' AND
                          ufu_fun_id = fun_id	AND ufu_usu_id = '".$_SESSION['usu_id']."'
                          GROUP BY fun_id
                          ORDER BY fun_id"; */

                        //MAOH - 20 Ene 2012 - Se muestran los hijos de las grupos de funcionalidades deshabilitados
                        if ($resultadoTipoFuncionalidades[$i]['grf_habilitado'] == 'true') {
                            //La lista de funcionalidades seran las del usuario actual
                            $sql = "SELECT 	fui_nombre, fui_descripcion, fun_id, fun_descripcion, 'true' as fun_habilitada
												FROM 	usuario_funcionalidad, funcionalidad, funcionalidad_idioma
												WHERE	fun_id = fui_fun_id AND fui_idi_id = '" . $_SESSION['idi_id'] . "' AND
														fun_grf_id = '" . $resultadoTipoFuncionalidades[$i]['grf_id'] . "' AND
														ufu_fun_id = fun_id	AND ufu_usu_id = '" . $_SESSION['usu_id'] . "'
												GROUP BY fun_id
												ORDER BY fun_id";
                        } else {
                            $sql = "SELECT 	fui_nombre, fui_descripcion, fun_id, fun_descripcion, 'false' as fun_habilitada
												FROM 	usuario_funcionalidad, funcionalidad, funcionalidad_idioma
												WHERE	fun_id = fui_fun_id AND fui_idi_id = '" . $_SESSION['idi_id'] . "' AND
														fun_grf_id = '" . $resultadoTipoFuncionalidades[$i]['grf_id'] . "' AND
														ufu_fun_id = fun_id
												GROUP BY fun_id
												ORDER BY fun_id";
                        }
                        //MAOH - 20 Ene 2012 - Fin
                    }//Fin se va a crear o modificar un usuario de hotel
                }//Fin es un usuario de cadena

                if ($_SESSION['tipo_usuario'] == 'H') {
                    //La lista de funcionalidades seran las del usuario actual
                    /* $sql = "SELECT 	fui_nombre, fui_descripcion, fun_id, fun_descripcion
                      FROM 	usuario_funcionalidad, funcionalidad, funcionalidad_idioma
                      WHERE	fun_id = fui_fun_id AND fui_idi_id = '".$_SESSION['idi_id']."' AND
                      fun_grf_id = '".$resultadoTipoFuncionalidades[$i]['grf_id']."' AND
                      ufu_fun_id = fun_id	AND ufu_usu_id = '".$_SESSION['usu_id']."'
                      GROUP BY fun_id
                      ORDER BY fun_id"; */

                    //MAOH - 20 Ene 2012 - Se muestran los hijos de las grupos de funcionalidades deshabilitados
                    if ($resultadoTipoFuncionalidades[$i]['grf_habilitado'] == 'true') {
                        //La lista de funcionalidades seran las del usuario actual
                        $sql = "SELECT 	fui_nombre, fui_descripcion, fun_id, fun_descripcion, 'true' as fun_habilitada
										FROM 	usuario_funcionalidad, funcionalidad, funcionalidad_idioma
										WHERE	fun_id = fui_fun_id AND fui_idi_id = '" . $_SESSION['idi_id'] . "' AND
												fun_grf_id = '" . $resultadoTipoFuncionalidades[$i]['grf_id'] . "' AND
												ufu_fun_id = fun_id	AND ufu_usu_id = '" . $_SESSION['usu_id'] . "'
										GROUP BY fun_id
										ORDER BY fun_id";
                    } else {
                        $sql = "SELECT 	fui_nombre, fui_descripcion, fun_id, fun_descripcion, 'false' as fun_habilitada
										FROM 	usuario_funcionalidad, funcionalidad, funcionalidad_idioma
										WHERE	fun_id = fui_fun_id AND fui_idi_id = '" . $_SESSION['idi_id'] . "' AND
												fun_grf_id = '" . $resultadoTipoFuncionalidades[$i]['grf_id'] . "' AND
												ufu_fun_id = fun_id
										GROUP BY fun_id
										ORDER BY fun_id";
                    }
                    //MAOH - 20 Ene 2012 - Fin
                }//Fin es un usuario de hotel
            }//Fin else es un usuario de hotel o de cadena

            $resultadoFuncionalidadesEsteTipo = $_obj_database->obtenerRegistrosAsociativos($sql);
            $totalFuncionalidadesEsteTipo = $_obj_database->obtenerNumeroFilas();

            //Se recorren las funcionalidades de este tipo del usuario
            //La capa oculta de la lista de funcionalidades y el inicio de la tabla
            $funcionalidades .= '<div id="menu_opcion_' . $i . '" style="display:none;">
									 <table cellspacing="0" cellpadding="0" border="0" width="400" class="funcionalidades_item">';

            for ($j = 0; $j < $totalFuncionalidadesEsteTipo; $j++) {
                $fun_id = $resultadoFuncionalidadesEsteTipo[$j]['fun_id'];
                $checkeado = '';
                if ($datos['usu_id_funcionalidades'] != '') { //Se esta copiando las funcionalidades de otro usuario
                    if ($this->verificarFuncionalidadActiva($datos['usu_id_funcionalidades'], $fun_id)) {
                        $checkeado = ' checked = "checked" ';
                    }
                } else {
                    $checkeado = $this->ponerCheckedFuncionalidad($datos['usu_id'], $fun_id);
                }

                if ($checkeado == ' checked = "checked" ') {
                    $estilo_funcionalidad = 'funcionalidades_item_checkeado';
                } else {
                    $estilo_funcionalidad = 'funcionalidades_item';
                }

                //Nuevo desde Noviembre 24 de 2011
                //Cualquier usuario (aparte del root) NO podra modificar sus propias funcionalidades.
                //El usuario primario debera solicitar el cambio al root y el usuario secundario solicitarlos al primario

                $funcionalidadDisabled = '';
                if ($datos['usu_id'] == $_SESSION['usu_id']) {
                    $funcionalidadDisabled = ' disabled = "disabled" ';
                }

                //MAOH - 16 Ene 2012 - Se deshabilita el check de la funcionalidad
                if ($resultadoFuncionalidadesEsteTipo[$j]['fun_habilitada'] == 'false') {
                    $funcionalidadDisabled = ' disabled = "disabled" ';
                }
                //MAOH - 16 Ene 2012 - Fin

                $fun_nombre = $resultadoFuncionalidadesEsteTipo[$j]['fui_nombre'];
                $fun_descripcion = $resultadoFuncionalidadesEsteTipo[$j]['fui_descripcion'];

                if (strlen($fun_descripcion) == 0) { //La funcionalidad no tiene una descripcion
                    $funcionalidades .= '<tr>
														<td colspan="3" align="right"><img src="' . $_PATH_IMAGENES . '/linea_gris.png" width="400" height="1"></td>
													</tr>
													<tr id ="funcionalidad_' . $fun_id . '" class="' . $estilo_funcionalidad . '">
														<td width="25"></td>
														<td width="345">' . $fun_nombre . '</td>
														<td width="30" align="left"><input type="checkbox" ' . $checkeado . $funcionalidadDisabled . ' name="funcionalidad[]" value="' . $fun_id . '" onClick="cambiarEstadoFuncionalidad(this, ' . $fun_id . ')"></td>
													 </tr>';
                } else if (strlen($fun_descripcion) <= 70) { //La descripcion de la funcionalidad es corta, se despliega en bocadillo
                    $funcionalidades .= '<tr>
														<td colspan="3" align="right"><img src="' . $_PATH_IMAGENES . '/linea_gris.png" width="400" height="1"></td>
													</tr>
													<tr id ="funcionalidad_' . $fun_id . '" class="' . $estilo_funcionalidad . '">
														<td width="25"></td>
														<td width="345">' . $fun_nombre . '&nbsp; <img src="' . $_PATH_IMAGENES . '/info_funcionalidad.png" onClick="mostrarBocadillo(\'' . $fun_descripcion . '\',this, event, \'200px\')" onMouseout="ocultarBocadillo()"></td>
														<td width="30" align="left"><input type="checkbox" ' . $checkeado . $funcionalidadDisabled . ' name="funcionalidad[]" value="' . $fun_id . '" onClick="cambiarEstadoFuncionalidad(this, ' . $fun_id . ')"></td>
													 </tr>';
                } else { //La descripcion de la funcionalidad es larga se despliega en lightbox
                    $funcionalidades .= '<tr>
														<td colspan="3" align="right"><img src="' . $_PATH_IMAGENES . '/linea_gris.png" width="400" height="1"></td>
													</tr>
													<tr id ="funcionalidad_' . $fun_id . '" class="' . $estilo_funcionalidad . '">
														<td width="25"></td>
														<td width="345">' . $fun_nombre . '&nbsp; <img src="' . $_PATH_IMAGENES . '/info_funcionalidad.png" onclick = "document.getElementById(\'light_' . $fun_id . '\').style.display=\'block\';document.getElementById(\'fade_' . $fun_id . '\').style.display=\'block\'">
														<div id="container">
														 	<div id="light_' . $fun_id . '" class="white_content">
																<table border="0" width="100%">
																	<tr>
																		<td class="descripcion_funcionalidad_ligthbox">' . $fun_nombre . '</td>
																		<td width="60%">&nbsp</td>
																		<td width="20"><img src="' . $_PATH_IMAGENES . '/cerrar_ligthbox.png" onclick = "document.getElementById(\'light_' . $fun_id . '\').style.display=\'none\';document.getElementById(\'fade_' . $fun_id . '\').style.display=\'none\'"></td>
																	</tr>
																	<tr>
																		<td align="right"><img src="' . $_PATH_IMAGENES . '/linea_gris.png" width="100%" height="1"></td>
																		<td colspan="2"></td>
																	</tr>
																	<tr>
																		<td colspan="3">' . $fun_descripcion . '</td>
																	</tr>
																</table>
															</div>
													 	</div>
													 	<div id="fade_' . $fun_id . '" class="black_overlay"></div>
														</td>
														<td width="30" align="left"><input type="checkbox" ' . $checkeado . $funcionalidadDisabled . ' name="funcionalidad[]" value="' . $fun_id . '" onClick="cambiarEstadoFuncionalidad(this, ' . $fun_id . ')"></td>
													 </tr>
													 ';
                }
            }//Fin de recorrer las funcionalidades de este tipo
            $funcionalidades .= '</table></div>';
        }//Fin de recorrer los tipos de funcionalidades del usuario actual
        //Construimos el checkbox para seleccionar todas las funcionalidades

        return $funcionalidades;
    }

//Fin del metodo cconstruirListaFuncionalidades($usu_id,$datos)

    function ponerCheckedFuncionalidad($usu_id, $fun_id) {
        global $_obj_database;
        $sql = "SELECT ufu_id FROM usuario_funcionalidad WHERE ufu_usu_id = '" . $usu_id . "' AND ufu_fun_id = '" . $fun_id . "'";
        $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
        $cantidad = $_obj_database->obtenerNumeroFilas();
        if ($cantidad == 0) {
            return '';
        } else {
            return ' checked = "checked" ';
        }
    }

    function validarAcceso($perfiles_autorizados, $fun_id = 0) {
        //captura el perfil del usuario
        $perfil = $_SESSION['tipo_usuario'];

        $usu_id = $_SESSION['usu_id'];

        $funcionalidadActiva = true;
        if ($fun_id != 0 && $_SESSION['tipo_usuario'] != 'R' && $_SESSION['tipo_usuario'] != 'AD') { //Si existe alguna funcionalidad que controle el acceso a esta accion
            $funcionalidadActiva = $this->verificarFuncionalidadActiva($usu_id, $fun_id);  //true el usuario actual tiene activa esta funcionalidad
        }

        if (sizeof($perfiles_autorizados) > 0) {
            //verifica si el perfil se encuentra en los autorizados
            $existe = array_search($perfil, $perfiles_autorizados);

            if (is_numeric($existe) && $_SESSION[autenticado] == 1) {
                //Asigna el perfil correspondiente
                $perfil = $_SESSION['tipo_usuario'];
            } else {
                //elimina el acceso de todos los perfiles
                $perfil = "-";
            }
        }//Fin de if( sizeof($perfiles_autorizados)>0 )

        if (!$funcionalidadActiva && $_SESSION['tipo_usuario'] != 'R' && $_SESSION['tipo_usuario'] != 'AD') {
            $perfil = "-";
        }

        return $perfil;
    }

//Fin de validarAcceso()
    //MAOH - 15 Feb 2012 - Se agrega el id de la funcionalidad
    function mensajeActivarFuncionalidad($id_funcionalidad = "") {
        //DM - 2012-10-01
        //Retiro el llamado al archivo config
        global $idi_despliegue, $_obj_database;

        $datos['log_nombre_proceso_actual'] = "mensajeActivarFuncionalidad";
        $datos['log_nombre_subproceso_actual'] = "";
        $datos['log_termina_proceso'] = 0;
        $datos = logProceso($datos);
        $datos_log = $datos;

        $nombre_funcionalidad = "";

        //MAOH - 15 Feb 2012 - Si se desea mostrar el nombre de la funcionalidad
        if ($id_funcionalidad != "") {

            $datos['log_nombre_subproceso_actual'] = "SQL_1";
            $datos['log_termina_proceso'] = 0;
            $datos = logProceso($datos);

            $sql_fun = "SELECT *
						FROM funcionalidad_idioma
						WHERE fui_fun_id = '" . $id_funcionalidad . "'
							AND fui_idi_id = '" . $_SESSION['idi_id'] . "'";
            $res_fun = $_obj_database->obtenerRegistrosAsociativos($sql_fun);

            $datos['log_nombre_subproceso_actual'] = "SQL_1";
            $datos['log_termina_proceso'] = 1;
            $datos = logProceso($datos);

            if (sizeof($res_fun) > 0) {
                $nombre_funcionalidad = " (" . $res_fun[0]["fui_nombre"] . ")";
            }
        }

        $categoriaUsuario = $_SESSION['usu_usc_id'];

        if ($categoriaUsuario == 1) { //El usuario es primario luego debe solicitar la funcionalidad a Soporte
            $mensaje = $idi_despliegue['funcionalidad_no_activa_primario'];
            Interfaz::asignarToken("nombre_fun", $nombre_funcionalidad, $mensaje); //MAOH - 15 Feb 2012
        } else if ($categoriaUsuario == 2) {
            if ($_SESSION['tipo_usuario'] == 'H') {
                //Consultamos el email del responsable del contrato
                $datos['log_nombre_subproceso_actual'] = "SQL_2";
                $datos['log_termina_proceso'] = 0;
                $datos = logProceso($datos);

                $sql = "SELECT 	ecn_correo
						FROM 	usuario_establecimiento, establecimiento_contactos
						WHERE 	use_usuario_id = '" . $_SESSION['usu_id'] . "'
                                AND ecn_est_id = use_establecimiento_id
                                AND ecn_tipo_notificacion = 'REC'
                                ";
                $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);

                $datos['log_nombre_subproceso_actual'] = "SQL_2";
                $datos['log_termina_proceso'] = 1;
                $datos = logProceso($datos);

                $email = $resultado[0]['enc_correo'];
                $mensaje = $idi_despliegue['funcionalidad_no_activa_secundario'] . ' ' . $idi_despliegue['hue_email'] . ' : ' . $email;
                Interfaz::asignarToken("nombre_fun", $nombre_funcionalidad, $mensaje); //MAOH - 15 Feb 2012
            } else if ($_SESSION['tipo_usuario'] == 'C') {
                //obtiene el usuario primario de la cadena
                $primario = $_obj_database->obtenerRegistro("usuario_cadena, usuario", "uca_cad_id = " . $_SESSION['cad_id'] . "
                                                and uca_usu_id = usu_id
                                                and usu_usc_id = 1
                                                and usu_estado = 1
                                                ");

                $email = $primario['usu_correo'];
                $mensaje = $idi_despliegue['funcionalidad_no_activa_secundario'] . ' ' . $idi_despliegue['hue_email'] . ' : ' . $email;
                Interfaz::asignarToken("nombre_fun", $nombre_funcionalidad, $mensaje); //MAOH - 15 Feb 2012
            }
        }

        $datos_log['log_nombre_proceso_actual'] = "mensajeActivarFuncionalidad";
        $datos_log['log_nombre_subproceso_actual'] = "";
        $datos_log['log_termina_proceso'] = 1;
        $datos_log = logProceso($datos_log);

        return $mensaje;
    }

//Fin mensajeActivarFuncionalidad($id_funcionalidad = "")

    function mensajeMaximoPreguntasNuevas($datos) {
        //DM - 2012-10-01
        //Retiro el llamado al archivo config
        global $_obj_database, $idi_despliegue;

        if ($_SESSION['tipo_usuario'] == 'H') {
            $est_id = $_SESSION['est_id'];
            $sql = "SELECT est_max_preguntas FROM establecimiento WHERE est_id = '" . $est_id . "'";
            $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
            $maxPreguntas = $resultado[0]['est_max_preguntas'];

            //Consultamos la cantidad de preguntas de hotel actuales del establecimiento
            //pgs - 15/08/2012 - se añade filtro de la encuesta asociada
            $sql = "select COUNT(pre_id) AS total from pregunta, pregunta_establecimiento
                                    where pee_establecimiento_id = '" . $est_id . "' and pee_pregunta_id = pre_id and pre_tipo = '3'
                                        AND pee_enc_id='" . $_SESSION['enc_id'] . "'
                                        AND pre_enc_id='" . $_SESSION['enc_id'] . "'";

            $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
            $cantidadPreguntasHotel = $resultado[0]['total'];
            if (($cantidadPreguntasHotel + 1) > $maxPreguntas) { //Si se agrega una pregunta mas de hotel se pasara del maximo
                $mensaje = $idi_despliegue['maximo_preguntas'];
            } else { //Se puede agregar la nueva pregunta
                $mensaje = '';
            }
        } else if ($_SESSION['tipo_usuario'] == 'C') {
            if (strpos($datos['est_id'], 'cad_id_') !== false) { //Se selecciono las preguntas estandar de cadena
                $cad_id = str_replace('cad_id_', '', $datos['est_id']);
                $sql = "SELECT cad_max_preguntas FROM cadena WHERE cad_id = '" . $cad_id . "'";
                $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
                $maxPreguntas = $resultado[0]['cad_max_preguntas'];

                //Consultamos los hoteles de la cadena
                $hoteles_cadena = "SELECT est_id FROM establecimiento WHERE est_estado = '1' AND est_cad_id = '" . $cad_id . "'";
                $resultado = $_obj_database->obtenerRegistrosAsociativos($hoteles_cadena);
                $est_id = $resultado[0]['est_id']; //Suponemos que todos los hoteles de la cadena tienen las mismas preguntas de cadena de su cadena
                //Consultamos la cantidad de preguntas de cadena actuales de la cadena
                //pgs - 15/08/2012 - se añade filtro de la encuesta asociada
                $sql = "select COUNT(pre_id) AS total from pregunta, pregunta_establecimiento
                                                    where pee_establecimiento_id = '" . $est_id . "' and pee_pregunta_id = pre_id and pre_tipo = '5'
                                                        AND pee_enc_id='" . $_SESSION['enc_id'] . "'
                                                         AND pre_enc_id='" . $_SESSION['enc_id'] . "'";

                $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
                $cantidadPreguntasCadena = $resultado[0]['total'];

                if (($cantidadPreguntasCadena + 1) > $maxPreguntas) { //Si se agrega una pregunta mas de cadena se pasara del maximo
                    $mensaje = $idi_despliegue['maximo_preguntas_cadena'];
                } else { //Se puede agregar la nueva pregunta de cadena
                    $mensaje = '';
                }
            }//Fin selecciono las preguntas estandar de cadena
            else { //Se esta editando un cuestionario de un hotel de la cadena
                //Consultamos los hoteles de la cadena
                $cad_id = $_SESSION['cad_id'];
                $hoteles_cadena = "SELECT est_id FROM establecimiento WHERE est_estado = '1' AND est_cad_id = '" . $cad_id . "'";
                $resultado = $_obj_database->obtenerRegistrosAsociativos($hoteles_cadena);
                $est_id = $resultado[0]['est_id']; //Suponemos que todos los hoteles de la cadena tienen las mismas preguntas de cadena de su cadena
                $sql = "SELECT est_max_preguntas FROM establecimiento WHERE est_id = '" . $est_id . "'";
                $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
                $maxPreguntas = $resultado[0]['est_max_preguntas'];

                //Consultamos la cantidad de preguntas de hotel actuales del establecimiento
                //pgs - 15/08/2012 - se añade filtro de la encuesta asociada
                $sql = "select COUNT(pre_id) AS total from pregunta, pregunta_establecimiento
                                                    where pee_establecimiento_id = '" . $est_id . "' and pee_pregunta_id = pre_id and pre_tipo = '3'
                                                        AND pee_enc_id='" . $_SESSION['enc_id'] . "'
                                                        AND pre_enc_id='" . $_SESSION['enc_id'] . "'";

                $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
                $cantidadPreguntasHotel = $resultado[0]['total'];
                if (($cantidadPreguntasHotel + 1) > $maxPreguntas) { //Si se agrega una pregunta mas de hotel se pasara del maximo
                    $mensaje = $idi_despliegue['maximo_preguntas'];
                } else { //Se puede agregar la nueva pregunta
                    $mensaje = '';
                }
            }//Se esta editando un cuestionario de un hotel de la cadena
        }

        return $mensaje;
    }

//Fin mensajeMaximoPreguntasNuevas($datos)

    function desactivarFuncionalidadesSecundarios($usu_id, $usu_per_id, $datos) { //Se pasa como parametro del id del usuario primario y su perfil
        global $_obj_database;

        //Consultamos las funcionalidades Activas para el usuario
        $funcionalidad = $datos['funcionalidad'];
        $condicion = "";
        if (is_array($funcionalidad) && sizeof($funcionalidad) > 0) {
            $funcionalidades_activas = implode(',', $funcionalidad);
            $condicion = "WHERE fun_id NOT IN (" . $funcionalidades_activas . ")";
        }

        //Consultamos las funcionalidades NO Activas para el usuario
        $sql = "SELECT fun_id
                FROM funcionalidad
                " . $condicion . "
                ORDER BY fun_id";

        $resultadoFuncionalidadesNoActivas = $_obj_database->obtenerRegistrosAsociativos($sql);
        $cantidad = $_obj_database->obtenerNumeroFilas();
        $funcionalidades_no_activas = array();
        for ($i = 0; $i < $cantidad; $i++) {
            $funcionalidades_no_activas[] = $resultadoFuncionalidadesNoActivas[$i]['fun_id'];
        }

        if (sizeof($funcionalidades_no_activas) == 0) {
            $lista_funcionalidades_no_activas = '0';
        } else {
            $lista_funcionalidades_no_activas = implode(',', $funcionalidades_no_activas);
        }

        if ($usu_per_id == 6) { //Es un usuario primario de cadena
            //Consultamos el id de la cadena
            $sql = "SELECT uca_cad_id FROM usuario_cadena WHERE uca_usu_id = '" . $usu_id . "'";
            $resultadoIdCadena = $_obj_database->obtenerRegistrosAsociativos($sql);
            $cad_id = $resultadoIdCadena[0]['uca_cad_id'];

            //Se desactivan las funcionalidades a los usuarios secundarios de la cadena
            //Consultamos los usuarios secundarios de la cadena
            $sql = "SELECT usu_id
						FROM usuario_cadena, usuario
						WHERE 	uca_cad_id = '" . $cad_id . "' AND
								uca_usu_id = usu_id AND
								usu_usc_id = '2'";

            $resultadoUsuariosSecundariosCadena = $_obj_database->obtenerRegistrosAsociativos($sql);
            $cantidadUsuariosSecundariosCadena = $_obj_database->obtenerNumeroFilas();

            //Recorremos los usuarios secundarios de la cadena desactivandole las funcionalidades que no tiene el primario de cadena
            for ($j = 0; $j < $cantidadUsuariosSecundariosCadena; $j++) {
                $usu_id_cadena_actual = $resultadoUsuariosSecundariosCadena[$j]['usu_id'];

                $sql = "DELETE FROM usuario_funcionalidad WHERE ufu_usu_id = '" . $usu_id_cadena_actual . "' AND ufu_fun_id IN (" . $lista_funcionalidades_no_activas . ")";
                $_obj_database->ejecutarSQL($sql);
            }//Fin de recorrer los usuarios secundarios de la cadena
            //Se desactivan las funcionalidades a cada uno de los usuarios de los hoteles de la cadena
            //Consultamos los usuarios de los hoteles de la cadena
            $sql = "SELECT usu_id
						FROM usuario, usuario_establecimiento, establecimiento
						WHERE 	usu_id = use_usuario_id AND
								est_id = use_establecimiento_id AND
								est_cad_id = '" . $cad_id . "'";
            $resultadoUsuariosSecundariosHotelesCadena = $_obj_database->obtenerRegistrosAsociativos($sql);
            $cantidadUsuariosSecundariosHotelesCadena = $_obj_database->obtenerNumeroFilas();
            //Recorremos los usuarios de los hoteles de la cadena desactivandole las funcionalidades que no tiene el primario de cadena
            for ($j = 0; $j < $cantidadUsuariosSecundariosHotelesCadena; $j++) {
                $usu_id_hotel_cadena_actual = $resultadoUsuariosSecundariosHotelesCadena[$j]['usu_id'];

                $sql = "DELETE FROM usuario_funcionalidad WHERE ufu_usu_id = '" . $usu_id_hotel_cadena_actual . "' AND ufu_fun_id IN (" . $lista_funcionalidades_no_activas . ")";
                $_obj_database->ejecutarSQL($sql);
            }//Fin de recorrer los usuarios secundarios de la cadena
        }//Fin es un usuario primario de cadena
        else if ($usu_per_id == 2) { //Es un usuario primario de hotel
            //Se consulta el id del hotel
            $sql = "SELECT use_establecimiento_id FROM usuario_establecimiento WHERE use_usuario_id = '" . $usu_id . "'";
            $resultadoIdEstablecimiento = $_obj_database->obtenerRegistrosAsociativos($sql);
            $est_id = $resultadoIdEstablecimiento[0]['use_establecimiento_id'];

            //Se desactivan las funcionalidades de cada uno de los usuarios secundarios del hotel
            $sql = "SELECT 	usu_id
						FROM 	usuario, usuario_establecimiento
						WHERE 	usu_id = use_usuario_id AND
								use_establecimiento_id = '" . $est_id . "' AND
								usu_usc_id = '2'";
            $resultadoUsuariosSecundariosHotel = $_obj_database->obtenerRegistrosAsociativos($sql);
            $cantidadUsuariosSecundariosHotel = $_obj_database->obtenerNumeroFilas();
            //Recorremos los usuarios secundarios del hotel desactivandole las funcionalidades que no tiene el primario del hotel
            for ($j = 0; $j < $cantidadUsuariosSecundariosHotel; $j++) {
                $usu_id_hotel_actual = $resultadoUsuariosSecundariosHotel[$j]['usu_id'];

                $sql = "DELETE FROM usuario_funcionalidad WHERE ufu_usu_id = '" . $usu_id_hotel_actual . "' AND ufu_fun_id IN (" . $lista_funcionalidades_no_activas . ")";

                $_obj_database->ejecutarSQL($sql);
            }
        }//Fin es un usuario primario de hotel
    }

//Fin desactivarFuncionalidadesSecundarios()
    //MAOH - 20 Jun 2012
    //Funcion que verifica si algun usuario del establecimiento tiene la funcionalidad activa
    function verificarFuncionalidadActivaEstablecimiento($est_id, $fun_id, $usu_usc_id = 0) {
        global $_obj_database;

        $datos['log_nombre_proceso_actual'] = "verificarFuncionalidadActivaEstablecimiento";
        $datos['log_nombre_subproceso_actual'] = "";
        $datos['log_termina_proceso'] = 0;
        $datos = logProceso($datos);
        $datos_log = $datos;

        if ($usu_usc_id != 0) { //Se debe verificar la categoria del usuario para verificar si la funcionalidad esta activa o no
            $datos['log_nombre_subproceso_actual'] = "SQL_1";
            $datos['log_termina_proceso'] = 0;
            $datos = logProceso($datos);
            $sql = "SELECT *
					FROM usuario_funcionalidad, usuario_establecimiento, usuario
					WHERE usu_id = use_usuario_id
						AND use_usuario_id = ufu_usu_id
						AND ufu_fun_id = '" . $fun_id . "'
						AND use_establecimiento_id = '" . $est_id . "'
						AND usu_usc_id = '" . $usu_usc_id . "'";
        } else { //No importa la categoria del usuario
            $datos['log_nombre_subproceso_actual'] = "SQL_1";
            $datos['log_termina_proceso'] = 0;
            $datos = logProceso($datos);
            $sql = "SELECT *
					FROM usuario_funcionalidad, usuario_establecimiento
					WHERE use_usuario_id = ufu_usu_id
						AND ufu_fun_id = '" . $fun_id . "'
						AND use_establecimiento_id = '" . $est_id . "'";
        }

        $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);

        $datos['log_nombre_subproceso_actual'] = "SQL_1";
        $datos['log_termina_proceso'] = 1;
        $datos = logProceso($datos);

        if (sizeof($resultado) == 0) {
            $datos_log['log_nombre_proceso_actual'] = "verificarFuncionalidadActivaEstablecimiento";
            $datos_log['log_nombre_subproceso_actual'] = "";
            $datos_log['log_termina_proceso'] = 1;
            $datos_log = logProceso($datos_log);
            return false; //El establecimiento NO tiene autorizada esta funcionalidad
        } else {
            $datos_log['log_nombre_proceso_actual'] = "verificarFuncionalidadActivaEstablecimiento";
            $datos_log['log_nombre_subproceso_actual'] = "";
            $datos_log['log_termina_proceso'] = 1;
            $datos_log = logProceso($datos_log);
            return true; //El establecimiento SI tiene autorizada esta funcionalidad
        }
    }

//Fin de verificarFuncionalidadActivaEstablecimiento($est_id, $fun_id, $usu_usc_id = 0)
    //MAOH - 20 Jun 2012 - Fin
    //pgs - 16/08/2012 - funcion que se encarga de traer las encuestas
    //se tiene en cuenta si es un usuario secundario
    function obtenerEncuestas($datos) {
        global $_obj_database;

        if ($datos['usc_codigo'] == 1 || $datos['usu_usc_id'] == 1) {

            if ($_SESSION['tipo_usuario'] == "R") {
                //si el usuario es primario
                $sql = "SELECT enc_id,eni_nombre
                    FROM encuesta,encuesta_idioma
                    WHERE enc_id = eni_enc_id
                        AND eni_idi_id='" . $_SESSION['idi_id'] . "' ";
            }//Fin de if( $_SESSION['tipo_usuario']=="R")
            else {

                //si el usuario es primario
                $sql = "SELECT enc_id,eni_nombre
                    FROM encuesta,encuesta_idioma,usuario_encuesta
                    WHERE enc_id = eni_enc_id
                       AND enc_id = uen_enc_id
                       AND eni_enc_id = uen_enc_id
                       AND uen_usu_id = '" . $_SESSION['usu_id'] . "'
                        AND eni_idi_id='" . $_SESSION['idi_id'] . "' ";
            }//Fin de else de if( $_SESSION['tipo_usuario']=="R")

            $encuestas = $_obj_database->obtenerRegistrosAsociativos($sql);
        }//Fin de if($datos['usc_codigo'] == 1)
        if ($datos['usc_codigo'] == 2 || $datos['usu_usc_id'] == 2) {
            //si el usuario es secundario
            //se traen las encuestas asociadas al usuario primario
            if ($datos['usu_per_id'] == 2 && ( $_SESSION['tipo_usuario'] == "R" || $_SESSION['usu_usc_id'] == 1 )) {
                //si es usuario hotel
                $sql = "SELECT usu_id
                      FROM usuario,usuario_establecimiento
                      WHERE usu_id=use_usuario_id
                          AND usu_usc_id=1
                          AND use_establecimiento_id='" . $datos['est_id'] . "'";
                $usuario_primario = $_obj_database->obtenerRegistrosAsociativos($sql);
            }//Fin de if($datos['usu_per_id'] == 2)
            else if ($datos['usu_per_id'] == 2) {
                $datos['usu_id'] = $datos['usu_id'] == '' ? $_SESSION['usu_id'] : $datos['usu_id'];
                $usuario_primario = array();
                $usuario_primario[0]['usu_id'] = $datos['usu_id'];
            }//Fin de if($datos['usu_per_id'] == 2)

            if ($datos['usu_per_id'] == 6) {
                $datos['cad_id'] = $datos['cad_id'] != "" ? $datos['cad_id'] : $_SESSION['cad_id'];

                //si es usuario cadena
                $sql = "SELECT usu_id
                      FROM usuario,usuario_cadena
                      WHERE usu_id=uca_usu_id
                          AND usu_usc_id=1
                          AND uca_cad_id='" . $datos['cad_id'] . "'";

                $usuario_primario = $_obj_database->obtenerRegistrosAsociativos($sql);
            }//Fin de if($datos['usu_per_id'] == 6)

            if (is_array($usuario_primario) && sizeof($usuario_primario) > 0) {
                //DM - 2013-01-08
                //agrega el campo de estado para filtrar solo las activas
                $sql = "SELECT distinct(enc_id),eni_nombre
                        FROM encuesta,encuesta_idioma,usuario_encuesta
                        WHERE enc_id = eni_enc_id
                            AND uen_enc_id = enc_id
                            AND uen_enc_id = eni_enc_id
                            AND uen_estado = 1
                            AND eni_idi_id='" . $_SESSION['idi_id'] . "'
                            AND uen_usu_id='" . $usuario_primario[0]['usu_id'] . "'";

                $encuestas = $_obj_database->obtenerRegistrosAsociativos($sql);
            }//Fin de if( is_array($usuario_primario) && sizeof($usuario_primario)>0)
        }//Fin de if($datos['usc_codigo'] == 2)

        return $encuestas;
    }

//Fin de obtenerEncuestas($datos)
    //pgs - 16/08/2012 - funcion que obtiene las encuestas asociadas al usuario
    function obtenerEncuestasUsuario($datos) {
        global $_obj_database;

        $sql = "SELECT enc_id, uen_estado
                        FROM encuesta,encuesta_idioma,usuario_encuesta
                        WHERE enc_id = eni_enc_id
                            AND uen_enc_id = enc_id
                            AND uen_enc_id = eni_enc_id
                            AND eni_idi_id='" . $_SESSION['idi_id'] . "'
                            AND uen_usu_id='" . $datos['usu_id'] . "'
                            ";
        $encuestas = $_obj_database->obtenerRegistrosAsociativosPorClave($sql, 'enc_id');

        return $encuestas;
    }

//Fin de obtenerEncuestasUsuario($datos)
    //pgs - 16/08/2012 - funcion que se encarga de crear los check para poner en el formulario de usuario
    function crearCheckEncuestas($datos) {


        $encuestas = $this->obtenerEncuestas($datos);

        if (!is_null($encuestas) && sizeof($encuestas) > 0) {
            $html_checks = '<table border=0>';

            foreach ($encuestas as $encuesta) {
                $checked = '';
                if (is_array($datos['encuestas'])) {
                    //if(array_key_exists($encuesta['enc_id'], $datos['encuestas']))
                    $enc_id = $encuesta['enc_id'];
                    if ($datos['encuestas'][$enc_id]['uen_estado'] == 1) {
                        $checked = 'checked';
                    }//Fin de if(array_search($encuesta['enc_id'], $datos['encuestas']))
                }//Fin de if(is_array($datos['encuestas']))

                $html_checks .= '<tr>
	                                    <td align="left"><input type="checkbox" name="encuestas[]" value="' . $encuesta['enc_id'] . '" ' . $checked . ' /></td>
	                                    <td align="left">' . $encuesta['eni_nombre'] . '</td>
	                                 </tr>';
            }//Fin de foreach($encuestas as $encuesta)

            $html_checks .= '</table>';
        } else {
            $html_checks = "-";
        }

        return $html_checks;
    }

//Fin de crearChecckEncuestas($datos)
    //pgs - 16/08/2012 - funcion que se encarga de ingresar las encuestas del usuario
    function ingresarEncuestas($datos) {
        global $_obj_database;

        //MD - 2012-09-14
        //Valida si hay encuestas para ingresar, si no existen entonces retorna false
        if (!(is_array($datos['encuestas']) && sizeof($datos['encuestas']) > 0)) {
            return false;
        }//Fin de if( !(is_array($datos['encuestas']) && sizeof($datos['encuestas'])>0) )
        //MD - 2012-09-14 - Fin de cambios
        //si es el usuario root o si se esta creando un usuario entonces si
        //ingresar registros
        if ($_SESSION['tipo_usuario'] == "R" || $datos['tipo_accion'] == "ingresar") {
            //solo el usuario ingresa registros
            $campos = array('uen_usu_id', 'uen_enc_id', 'uen_estado');

            $dato_insertar['tabla'] = 'usuario_encuesta';
            $dato_insertar['uen_usu_id'] = $datos['usu_id'];
            $dato_insertar['uen_estado'] = 1;

            foreach ($datos['encuestas'] as $encuesta) {
                $dato_insertar['uen_enc_id'] = $encuesta;

                $sql = $_obj_database->generarSQLInsertar($dato_insertar, $campos);

                $res = $_obj_database->ejecutarSQL($sql);

                if (!$res) {
                    return false;
                }//Fin de if(!$res)
            }//Fin de foreach($datos['encuestas'] as $encuesta)
        }//Fin de if( $_SESSION['tipo_usuario'] == "R" )
        else {

            $campos = array('uen_estado');
            $campos_insertar = array('uen_usu_id', 'uen_enc_id', 'uen_estado');

            $dato_actualizar['tabla'] = 'usuario_encuesta';
            $dato_actualizar['uen_estado'] = 1;

            $dato_insertar['tabla'] = "usuario_encuesta";
            $dato_insertar['uen_estado'] = 1;
            $dato_insertar['uen_usu_id'] = $datos['usu_id'];

            foreach ($datos['encuestas'] as $encuesta) {

                $dato_actualizar['condicion'] = "uen_usu_id = '" . $datos['usu_id'] . "'
                                                   and uen_enc_id = '" . $encuesta . "' ";

                $existe = $_obj_database->obtenerRegistro("usuario_encuesta", $dato_actualizar['condicion']
                );

                if (is_array($existe)) {
                    $sql = $_obj_database->generarSQLActualizar($dato_actualizar, $campos);
                }//Fin de if( is_array($existe) )
                else {
                    $dato_insertar['uen_enc_id'] = $encuesta;
                    $sql = $_obj_database->generarSQLInsertar($dato_insertar, $campos_insertar);
                }

                $res = $_obj_database->ejecutarSQL($sql);

                if (!$res) {
                    return false;
                }//Fin de if(!$res)
            }//Fin de foreach($datos['encuestas'] as $encuesta)
        }//Fin de else de if( $_SESSION['tipo_usuario'] == "R" )

        return true;
    }

//Fin de ingresarEncuestas($datos)

    /**
     * Funcion que se encarga de obtener las encuestas activas que tenga
     * el establecimiento a partir del usuario primario asociado
     * al establecimiento
     * DM - 2013-01-23
     * * */
    function obtenerEncuestasActivasPorEstablecimiento($datos) {
        global $_obj_database;

        //Obtiene los usuarios primarios asociados al establecimiento
        $sql = "SELECT usu_id
                FROM usuario,usuario_establecimiento
                WHERE usu_id = use_usuario_id
                      and use_establecimiento_id = '" . $datos['est_id'] . "'
                      and usu_usc_id =  1
                      and usu_estado = 1
                        ";

        $usuarios_primarios = $_obj_database->obtenerRegistrosAsociativosPorClave($sql, "usu_id");

        //Id de las encuestas activas para el establecimiento
        $encuestas_ids = array();

        //Si existe al menos un usuario primario
        if (sizeof($usuarios_primarios) > 0) {
            //Obtiene las encuestas activas que tengas los usuarios primarios
            $sql = "SELECT DISTINCT(uen_enc_id)
                    FROM usuario_encuesta
                    WHERE uen_estado =  1
                        and uen_usu_id IN (" . implode(array_keys($usuarios_primarios)) . ") ";

            $encuestas = $_obj_database->obtenerRegistrosAsociativosPorClave($sql, "uen_enc_id");

            //Obtiene solo los ids de las encuestas
            $encuestas_ids = array_keys($encuestas);
        }//Fin de if( sizeof($usuarios_primarios) > 0 )

        return $encuestas_ids;
    }

//Fin de obtenerEncuestasActivasPorEstablecimiento

    /**
     * Funcion que se encarga de obtener las funcionalidades asociadas a un paquete
     * DM - 2013-05-18
     * * */
    function obtenerFuncionalidadesPorPaquete($datos) {
        global $_obj_database;

        $sql = "select fpq_fun_id
                from funcionalidad_paquete
                where fpq_paq_id = '" . $datos['paq_id'] . "' ";

        $lista_funcionalidades = $_obj_database->obtenerRegistrosAsociativosPorClave($sql, "fpq_fun_id");

        return array_keys($lista_funcionalidades);
    }

//Fin de obtenerFuncionalidadesPorPaquete()

    /**
     * Funcion que se encarga de validar el acceso al formulario
     * de establecimientos o cadena
     * DM - 2013-05-20
     * */
    function obtenerAccesoFormulario($datos) {
        global $_obj_database;


        $codigo = $_obj_database->obtenerRegistro("codigos_acceso_formularios", "caf_codigo='" . $datos['cod'] . "'
                                        and caf_tipo_formulario = '" . $datos['caf_tipo_formulario'] . "'
                                        and caf_estado =  1 ");

        if (is_array($codigo)) {

            //si solicita los datos sin bloqueo entonces no realiza mas validaciones y lo retorna
            if ($datos['sin_bloqueo'] == 1) {
                return $codigo;
            }

            //si sobre paso la cantidad de accesos ya no es valido
            if ($codigo['caf_cantidad_accesos'] >= 10) {
                return false;
            }

            $campos = array("caf_cantidad_accesos");
            $datos['tabla'] = "codigos_acceso_formularios";
            $datos['condicion'] = "caf_codigo='" . $datos['cod'] . "'
                                        and caf_tipo_formulario = '" . $datos['caf_tipo_formulario'] . "'
                                        and caf_estado =  1";
            $datos['caf_cantidad_accesos'] = $codigo['caf_cantidad_accesos'] + 1;
            $_obj_database->actualizarDatosTabla($datos, $campos);

            return $codigo;
        }//Fin de if( is_array($codigo) )
        return false;
    }

//Fin de obtenerAccesoFormulario()

    /**
     * Funcion que se encargar de enviar al responsable del contrato la notificacion
     * con los datos de los usuarios creados.
     * DM - 2013-05-18
     * */
    function enviarNotificacionesDatosUsuarios($datos) {
        global $_obj_database, $_EMAIL_REMITENTE, $_CONFIG;
        
        $obj_correo = new Correo();

        //Obtiene la plantilla para notificacion usuarios
        $contenido_plantilla = $_obj_database->obtenerRegistro("contenido_plantillas", "
                                                    cpl_codigo = 'correo_notificacion_responsable_contrato'
                                                    and cpl_idi_id = '" . $datos['idi_id'] . "'
                                                    ");

        //Si no hay plantilla no envia la notificacion
        if (!is_array($contenido_plantilla)) {
            return;
        }

        $plantilla = $contenido_plantilla['cpl_contenido'];


        //Obtiene el archivo de idiomas de acuerdo al idioma del establecimiento
        $idi_despliegue = Idiomas::cargarIdiomas($datos['idi_id'], true);

        $datos_usuarios = "";
        //asigna la info de los usuarios creados
        foreach ($datos['usuarios'] as $info_usuario) {
            $perfil = $idi_despliegue['label_usuario_perfil_establecimiento'];
            if ($info_usuario['usu_per_id'] == 6) {
                $perfil = $idi_despliegue['label_usuario_perfil_cadena'];
            }
            $datos_usuarios .= "<table>
                                <tr>
                                    <td style=\"width:150px;font-family: proxima-nova,sans-serif,Tahoma, Geneva, Verdana; font-size:13px; color:#000000; text-align:left;\"><b>" . $idi_despliegue['label_usuario_perfil'] . "</b></td>
                                    <td style=\"width:200px;font-family: proxima-nova,sans-serif,Tahoma, Geneva, Verdana; font-size:13px; color:#000000; text-align:left;\">" . $perfil . "</td>
                                </tr>
                                <tr>
                                    <td style=\"font-family: proxima-nova,sans-serif,Tahoma, Geneva, Verdana; font-size:13px; color:#000000; text-align:left;\" >" . $idi_despliegue['label_usuario_nombre'] . "</td>
                                    <td style=\"font-family: proxima-nova,sans-serif,Tahoma, Geneva, Verdana; font-size:13px; color:#000000; text-align:left;\">" . $info_usuario['usu_nombre'] . "</td>
                                </tr>
                                <tr>
                                    <td style=\"font-family: proxima-nova,sans-serif,Tahoma, Geneva, Verdana; font-size:13px; color:#000000; text-align:left;\">" . $idi_despliegue['label_usuario_login'] . "</td>
                                    <td style=\"font-family: proxima-nova,sans-serif,Tahoma, Geneva, Verdana; font-size:13px; color:#000000; text-align:left;\">" . $info_usuario['usu_login'] . "</td>
                                </tr>
                                <tr>
                                    <td style=\"font-family: proxima-nova,sans-serif,Tahoma, Geneva, Verdana; font-size:13px; color:#000000; text-align:left;\">" . $idi_despliegue['label_usuario_clave'] . "</td>
                                    <td style=\"font-family: proxima-nova,sans-serif,Tahoma, Geneva, Verdana; font-size:13px; color:#000000; text-align:left;\">" . $info_usuario['usu_clave'] . "</td>
                                </tr>
                                </table>";
        }//Fin de foreach($datos['usuarios'] as $info_usuario)

        $plantilla = str_replace("[*info_usuarios*]", $datos_usuarios, $plantilla);

        $asunto = $idi_despliegue['asunto_notificacion_datos_usuarios'];

        //JMM-2017-01-30 - Validacion para datos SMTP
        //Se obtienen los datos de configuracion del hotel necesarios para envio de correos usando SMTP
        $opciones['opciones'] = establecimientos::obtenerConfiguracionGeneralEst($_SESSION['est_id']);
        //Se verifica la cantidad de emails enviados
        $cantidad_envios = establecimientos::verificarCantidadEmailsEnviados($opciones);
        if ($cantidad_envios === true) {
        	//$opciones = array();
        	$opciones['CC'] = array($_CONFIG['correo_copia_alta_establecimiento']);
        	$obj_correo->enviarCorreo($_EMAIL_REMITENTE, $datos['correo'], $asunto, $plantilla, null, $opciones);
        } 
        //$opciones = array();
        $opciones['CC'] = array($_CONFIG['correo_copia_alta_establecimiento']);
        $obj_correo->enviarCorreo($_EMAIL_REMITENTE, $datos['correo'], $asunto, $plantilla, null, $opciones);
    }

//Fin de enviarNotificacionesDatosUsuarios

    /**
     * Función que se encarga de verificar las encuestas asocidas al establecimiento
     * y activar o inactivar los parametros de encuesta para cada
     * establecimiento.
     * Para activar encuestas, la información se captura del hotel base.
     * DM - 2013-07-26
     * */
    function verificarConfiguracionEncuestasEstablecimiento($datos) {
        global $_obj_database, $_db_obj;

        //obtiene el establecimiento asociado al usuario
        $establecimiento = $_obj_database->obtenerRegistro("usuario_establecimiento, establecimiento", "use_usuario_id = '" . $datos['usu_id'] . "'
                                                and use_establecimiento_id = est_id
                                                ");

        //consulta las encuestas que tiene actualmente asociadas el usuario
        $sql = "select uen_enc_id
                from usuario_encuesta
                where uen_usu_id = '" . $datos['usu_id'] . "' ";

        $lista_encuestas = $_obj_database->obtenerRegistrosAsociativos($sql);

        foreach ($lista_encuestas as $encuesta_actual) {
            //si la encuesta, antes estaba asignada y ahora no, debe inactivarla
            //al establecimiento
            if (!in_array($encuesta_actual['uen_enc_id'], $datos['encuestas'])) {
                $respuesta = $_obj_database->obtenerRegistro("respuesta,pregunta_establecimiento", "res_pee_id = pee_id
                                                and pee_establecimiento_id = '" . $establecimiento['use_establecimiento_id'] . "'
                                                and pee_enc_id = res_enc_id
                                                and res_enc_id = " . $encuesta_actual['uen_enc_id'] . "
                                                ");

                //verifica si hay respuestas para la encuesta, si no tiene respuestas
                //entonces debe eliminar todo lo relacionado a la encuesta
                if (!is_array($respuesta)) {
                    $datos_eliminar = array();
                    $datos_eliminar['enc_id'] = $encuesta_actual['uen_enc_id'];
                    $datos_eliminar['est_id'] = $establecimiento['est_id'];
                    $res = $this->eliminarEncuestaEstablecimiento($datos_eliminar);
                    if (!$res) {
                        return -1003;
                    }
                }//Fin de if( !is_array($respuesta) )
            }//Fin de if( !in_array($encuesta_actual['uen_enc_id'], $datos['encuestas']) )
        }//Fin de foreach($lista_encuestas as $encuesta_actual)
        //funcionalidad plantillas
        $datos['tiene_funcionalidad_plantillas'] = $this->verificarFuncionalidadActiva($datos['usu_id'], 27);

        //De las encuestas seleccionadas, verifica para cuales tiene
        //las configuraciones activas, si no las tiene las agrega
        if (is_array($datos['encuestas']) && sizeof($datos['encuestas']) > 0) {
            $obj_establecimientos = new establecimientos();

            //Obtiene un de los correos que tiene el establecimiento para notificaciones
            $notificacion = $_obj_database->obtenerRegistro("establecimiento_contactos", "ecn_est_id = " . $establecimiento['est_id'] . "
                                            ");

            $correo_notificaciones = $notificacion['ecn_correo'];

            foreach ($datos['encuestas'] as $encuesta_id) {
                $datos_verificar = array();
                $datos_verificar['enc_id'] = $encuesta_id;
                $datos_verificar['est_id'] = $establecimiento['est_id'];

                if (!$this->verificarEncuestaActivaEstablecimiento($datos_verificar)) {
                    //Si el establecimiento no tiene activa la encuesta entonces
                    //activa la encuesta para el establecimiento
                    $datos_activar = array();
                    $datos_activar['tiene_funcionalidad_plantillas'] = $datos['tiene_funcionalidad_plantillas'];
                    $datos_activar['encuesta_id'] = $encuesta_id;
                    $datos_activar['est_id'] = $establecimiento['est_id'];
                    $datos_activar['est_nombre_email_remite'] = $establecimiento['est_nombre_email_remite'];
                    $datos_activar['est_email_remitente'] = $establecimiento['est_email_remitente'];
                    $datos_activar['est_email_notificaciones'] = $correo_notificaciones;
                    $datos_activar['est_cad_id'] = $establecimiento['est_cad_id'];
                    $res = $obj_establecimientos->activarEncuestaEstablecimiento($datos_activar);
                    if ($res != 1) {
                        return $res;
                    }
                }//Fin de if( !$this->verificarEncuestaActivaEstablecimiento($datos_verificar) )
            }//Fin de foreach($datos['encuestas'] as $encuesta_id)
        }//Fin de if( is_array($datos['encuestas']) && sizeof($datos['encuestas'])>0 )

        return 1;
    }

//Fin de verificarConfiguracionEncuestasEstablecimiento

    /**
     * Función que se encarga de verificar si un establecimiento tiene activa
     * una encuesta. Revisa si tiene datos asignados para el id de encuesta
     * en las plantillas de email y preguntas
     * DM - 2013-07-27
     * */
    function verificarEncuestaActivaEstablecimiento($datos) {
        global $_obj_database;

        $pregunta = $_obj_database->obtenerRegistro("pregunta_establecimiento", "pee_enc_id = " . $datos['enc_id'] . "
                                        and pee_establecimiento_id = " . $datos['est_id'] . "  ");

        $plantilla = $_obj_database->obtenerRegistro("establecimiento_plantilla_email", "epe_enc_id = " . $datos['enc_id'] . "
                                        and epe_est_id = " . $datos['est_id'] . "  ");

        //si tiene asignado un registro de pregunta o de plantilla, entonces
        //tiene activa la encuesta
        return is_array($pregunta) || is_array($plantilla);
    }

//Fin de verificarEncuestaActivaEstablecimiento()

    /**
     * Funcion que se encarga de eliminar una encuesta asociada a un establecimiento
     * DM - 2013-07-26
     * */
    function eliminarEncuestaEstablecimiento($datos) {
        global $_obj_database, $_db_obj;

        //obtiene los id de las preguntas tipo hotel a eliminar
        $sql = "select pee_pregunta_id
                from pregunta_establecimiento, pregunta
                where pee_pregunta_id = pre_id
                    and pre_tipo = 3
                    and pre_enc_id = pee_enc_id
                    and pee_enc_id = " . $datos['enc_id'] . "
                    and pee_establecimiento_id = " . $datos['est_id'] . "
                    ";

        $lista_preguntas = $_obj_database->obtenerRegistrosAsociativosPorClave($sql, "pee_pregunta_id");

        if (sizeof($lista_preguntas) > 0) {
            $lista_pre_id = array_keys($lista_preguntas);

            //elimina las preguntas de opciones idioma si tiene asignadas
            $sql = "DELETE FROM pregunta_opciones_idioma
                    WHERE poi_enc_id = " . $datos['enc_id'] . "
                        and poi_pro_id IN
                            (
                                select pro_id
                                from pregunta_opciones
                                where pro_enc_id = " . $datos['enc_id'] . "
                                and pro_pre_id IN (" . implode(",", $lista_pre_id) . ")
                            )
                    ";
            $res = $_obj_database->ejecutarSQL($sql);
            if (!$res) {
                return false;
            }//Fin de if(!$res)
            //elimina las preguntas de opciones si tiene asignadas
            $sql = "DELETE FROM pregunta_opciones
                          where pro_enc_id = " . $datos['enc_id'] . "
                          and pro_pre_id IN (" . implode(",", $lista_pre_id) . ")
                    ";
            $res = $_obj_database->ejecutarSQL($sql);
            if (!$res) {
                return false;
            }//Fin de if(!$res)
            //elimina las preguntas idioma
            $sql = "DELETE FROM pregunta_idioma
                          where pri_enc_id = " . $datos['enc_id'] . "
                          and pri_pregunta_id IN (" . implode(",", $lista_pre_id) . ")
                    ";
            $res = $_obj_database->ejecutarSQL($sql);
            if (!$res) {
                return false;
            }//Fin de if(!$res)
            //elimina las preguntas
            $sql = "DELETE FROM pregunta
                          where pre_enc_id = " . $datos['enc_id'] . "
                          and pre_id IN (" . implode(",", $lista_pre_id) . ")
                    ";
            $res = $_obj_database->ejecutarSQL($sql);
            if (!$res) {
                return false;
            }//Fin de if(!$res)
        }//Fin de if( sizeof($lista_preguntas) > 0 )
        //elimina las preguntas de establecimiento_familia si las tiene
        $sql = "DELETE FROM pregunta_establecimiento_familia
                      where pef_enc_id = " . $datos['enc_id'] . "
                      and pef_est_id = " . $datos['est_id'] . "
                ";
        $res = $_obj_database->ejecutarSQL($sql);
        if (!$res) {
            return false;
        }//Fin de if(!$res)
        //elimina las familias del establecimiento si las tiene
        $sql = "DELETE FROM familia
                      where fam_enc_id = " . $datos['enc_id'] . "
                      and fam_est_id = " . $datos['est_id'] . "
                ";
        $res = $_obj_database->ejecutarSQL($sql);
        if (!$res) {
            return false;
        }//Fin de if(!$res)
        //elimina las preguntas de establecimiento_red_social si las tiene
        $sql = "DELETE FROM pregunta_establecimiento_red_social
                      where per_enc_id = " . $datos['enc_id'] . "
                      and per_est_id = " . $datos['est_id'] . "
                ";
        $res = $_obj_database->ejecutarSQL($sql);
        if (!$res) {
            return false;
        }//Fin de if(!$res)
        //elimina las preguntas de establecimiento_terminal si las tiene
        $sql = "DELETE FROM pregunta_establecimiento_terminal
                      where pet_enc_id = " . $datos['enc_id'] . "
                      and pet_est_id = " . $datos['est_id'] . "
                ";
        $res = $_obj_database->ejecutarSQL($sql);
        if (!$res) {
            return false;
        }//Fin de if(!$res)
        //elimina las preguntas de establecimiento
        $sql = "DELETE FROM pregunta_establecimiento
                      where pee_enc_id = " . $datos['enc_id'] . "
                      and pee_establecimiento_id = " . $datos['est_id'] . "
                ";
        $res = $_obj_database->ejecutarSQL($sql);
        if (!$res) {
            return false;
        }//Fin de if(!$res)
        //Elimina los textos de la ley de proteccion
        $sql = "DELETE FROM lpd_establecimiento_idioma
                      where lei_enc_id = " . $datos['enc_id'] . "
                      and lei_est_id = " . $datos['est_id'] . "
                ";
        $res = $_obj_database->ejecutarSQL($sql);
        if (!$res) {
            return false;
        }//Fin de if(!$res)
        //Elimina los idiomas del establecimiento
        $sql = "DELETE FROM establecimiento_idioma
                      where esi_enc_id = " . $datos['enc_id'] . "
                      and esi_est_id = " . $datos['est_id'] . "
                ";
        $res = $_obj_database->ejecutarSQL($sql);
        if (!$res) {
            return false;
        }//Fin de if(!$res)
        //Elimina los correos de notificacion
        $sql = "DELETE FROM establecimiento_contactos
                      where ecn_enc_id = " . $datos['enc_id'] . "
                      and ecn_est_id = " . $datos['est_id'] . "
                ";
        $res = $_obj_database->ejecutarSQL($sql);
        if (!$res) {
            return false;
        }//Fin de if(!$res)
        //Elimina las despedidas
        $sql = "DELETE FROM establecimiento_despedida_idioma
                      where edi_enc_id = " . $datos['enc_id'] . "
                      and edi_est_id = " . $datos['est_id'] . "
                ";
        $res = $_obj_database->ejecutarSQL($sql);
        if (!$res) {
            return false;
        }//Fin de if(!$res)
        //Elimina las despedidas tracking
        $sql = "DELETE FROM establecimiento_despedida_tracking
                      where edt_enc_id = " . $datos['enc_id'] . "
                      and edt_est_id = " . $datos['est_id'] . "
                ";
        $res = $_obj_database->ejecutarSQL($sql);
        if (!$res) {
            return false;
        }//Fin de if(!$res)
        //Elimina los dias envio email
        $sql = "DELETE FROM establecimiento_dias_envio_email
                      where ede_enc_id = " . $datos['enc_id'] . "
                      and ede_est_id = " . $datos['est_id'] . "
                ";
        $res = $_obj_database->ejecutarSQL($sql);
        if (!$res) {
            return false;
        }//Fin de if(!$res)
        //Elimina los dias envio email
        $sql = "DELETE FROM establecimiento_email_agradecimiento
                      where eea_enc_id = " . $datos['enc_id'] . "
                      and eea_est_id = " . $datos['est_id'] . "
                ";
        $res = $_obj_database->ejecutarSQL($sql);
        if (!$res) {
            return false;
        }//Fin de if(!$res)
        //Obtiene los id de los proyectos que deben eliminar en el SEE
        $sql = "select epe_pro_id
                from establecimiento_plantilla_email
                where epe_enc_id = " . $datos['enc_id'] . "
                      and epe_est_id = " . $datos['est_id'] . "
                ";
        $lista_proyectos = $_obj_database->obtenerRegistrosAsociativosPorClave($sql, "epe_pro_id");

        if (is_array($lista_proyectos) && sizeof($lista_proyectos) > 0) {
            //obtiene los id de los proyectos que debe anular
            $lista_pro_id = array_keys($lista_proyectos);

            //Elimina los contenidos del correo
            $sql = "DELETE FROM contenido_mail
                        WHERE com_pro_id IN (" . implode(",", $lista_pro_id) . ")";
            $res = $_db_obj['see']->ejecutarSQL($sql);
            if (!$res) {
                return false;
            }//Fin de if(!$res)
            //Elimina los perfiles de destinatarios que tenga asociado el proyecto
            $sql = "DELETE FROM perfil_destinatario
                        WHERE pd_des_id IN
                        (
                            SELECT des_id
                            FROM  destinatario
                            WHERE des_pro_id IN (" . implode(",", $lista_pro_id) . ")
                        )
                        ";
            $res = $_db_obj['see']->ejecutarSQL($sql);
            if (!$res) {
                return false;
            }//Fin de if(!$res)
            //Elimina los destinatarios que tenga asociado el proyecto
            $sql = "DELETE FROM destinatario
                        WHERE des_pro_id IN (" . implode(",", $lista_pro_id) . ")";
            $res = $_db_obj['see']->ejecutarSQL($sql);
            if (!$res) {
                return false;
            }//Fin de if(!$res)
            //Elimina los perfiles del proyecto
            $sql = "DELETE FROM perfil_proyecto
                        WHERE pep_pro_id IN (" . implode(",", $lista_pro_id) . ")";
            $res = $_db_obj['see']->ejecutarSQL($sql);
            if (!$res) {
                return false;
            }//Fin de if(!$res)
            //Elimina los recordatorios
            $sql = "DELETE FROM recordatorio
                        WHERE rec_pro_id IN (" . implode(",", $lista_pro_id) . ")";
            $res = $_db_obj['see']->ejecutarSQL($sql);
            if (!$res) {
                return false;
            }//Fin de if(!$res)
            //Elimina los proyecto
            $sql = "DELETE FROM proyecto
                        WHERE pro_id IN (" . implode(",", $lista_pro_id) . ")";
            $res = $_db_obj['see']->ejecutarSQL($sql);
            if (!$res) {
                return false;
            }//Fin de if(!$res)
            //Elimina las plantillas de email
            $sql = "DELETE FROM establecimiento_plantilla_email
                          where epe_enc_id = " . $datos['enc_id'] . "
                          and epe_est_id = " . $datos['est_id'] . "
                        ";
            $res = $_obj_database->ejecutarSQL($sql);
            if (!$res) {
                return false;
            }//Fin de if(!$res)
        }//Fin de if( is_array($lista_proyectos) && sizeof($lista_proyectos)>0 )



        return true;
    }

//Fin de eliminarEncuestaEstablecimiento

    /**
     * Función que se encarga de obtener las funcionalidades
     * de un paquete solicitado
     * DM - 2013-08-05
     * * */
    function obtenerFuncionalidesPaqueteAjax($datos) {
        global $_obj_database;

        $sql = "select fpq_fun_id
                from funcionalidad_paquete
                where fpq_paq_id = " . $datos['paq_id'] . "
                ";
        $lista_funcionalidades = $_obj_database->obtenerRegistrosAsociativosPorClave($sql, "fpq_fun_id");

        $lista_funcionalidades_id = array_keys($lista_funcionalidades);

        return json_encode($lista_funcionalidades_id);
    }

//Fin de obtenerFuncionalidesPaqueteAjax

    /**
     * Se encarga de obtener la ur que se guardo en el archivo temporal
     * cuando se accede desde un enlace externo y debe primero autenticarse en
     * la aplicación
     * DM - 2016-04-16
     * * */
    function obtenerUrlArchivoAcceso($datos) {
        global $_ARCHIVOS_TMP_COOKIE;

        $codigo = $datos['est_codigo'];

        $ubicacion_fichero = $_ARCHIVOS_TMP_COOKIE . $codigo;

        //Verifica si existe un archivo asociado al codigo del establecimiento
        //si no existe retorna false
        if (!file_exists($ubicacion_fichero)) {
            return false;
        }//Fin de if( !file_exists($_ARCHIVOS_TMP_COOKIE.$codigo) )
        //Obtiene el tiempo desde hace cuanto se creo el fichero
        //para ver si el enlace es válido
        $info_archivo = stat($ubicacion_fichero);

        $time_minutos = (time() - $info_archivo['mtime']) / 60;

        //Si han pasado ya 10 minutos desde que se creo el enlace entonces ya no lo es valido y lo borra
        if ($time_minutos > 10) {
            Archivos::eliminarArchivo($_ARCHIVOS_TMP_COOKIE, $codigo);
            return false;
        }//Fin de
        //Obtiene el contenido del fichero y lo elimina
        $contenido_archivo = Archivos::obtenerContenidoArchivo($ubicacion_fichero);

        $contenido_archivo_partes = preg_split("/\r\n/", $contenido_archivo);

        //Si tiene un salto de linea entonces es porque debe validar el navegador y la ip remota
        if (count($contenido_archivo_partes) > 1) {
            //Valida si la información del navegador es la misma que creo el fichero
            $codigo_navegador = base64_encode($_SERVER['HTTP_USER_AGENT'] . '_' . $_SERVER['REMOTE_ADDR']);
            //Si es la misma entonces retorna la url
            if ($contenido_archivo_partes[1] == $codigo_navegador) {
                Archivos::eliminarArchivo($_ARCHIVOS_TMP_COOKIE, $codigo);
                $contenido_archivo = Herramientas::decodificarBase64CamposTextoFormulario($contenido_archivo_partes[0]);
                return $contenido_archivo;
            }//Fin deif( $contenido_archivo_partes[1] == $codigo_navegador)
            //si no es el mismo navegador no hace nada
            return false;
        }//Fin de if( count($contenido_archivo_partes) > 1 )

        Archivos::eliminarArchivo($_ARCHIVOS_TMP_COOKIE, $codigo);
        $contenido_archivo = Herramientas::decodificarBase64CamposTextoFormulario($contenido_archivo);
        return $contenido_archivo;
    }

//Fin de obtenerUrlArchivoAcceso
}

//Fin de clase  UsuarioSistema
?>