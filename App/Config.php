<?php

/*
*	ARCHIVO DE CONFIGURACION CON  LAS VARIABLES A UTILIZAR EN LA APLICACION
*/
    date_default_timezone_set('America/Bogota');
	session_name("autopacifico");
	session_start();

	ini_set("memory_limit", "128000000");
	ini_set('upload_max_filesize', '40M');
	ini_set('post_max_size', '40M');
	ini_set('max_execution_time', 0);


	$_limit = 20;

  	$_host_db = "localhost";
  	$_db = "tcbuen";
  	$_user_db="root";
  	$_password_db = "";

    //$_conexionAuto = Pagos::abrirConexionPDO("172.16.2.26, 1433", "ap_pruebas", "indicadores_taller", "qaZXsw.12*");    //Abrir conexion SQL Server

    $_PATH_WEB = "http://autopacifico.com/";
  	$_PATH_SERVIDOR =$_SERVER['DOCUMENT_ROOT'];
  	$_PATH_IMAGENES ="http://autopacifico.com/Includes/Imagenes";
  	$_PATH_UPLOADS ="uploads";
	$_FECHA_GLOBAL=fechaCastellano(date('d-m-Y H:i:s'));

  	$_EMAIL_ADMIN = "Autopacifico<ejemplogmail.com>";

  	/*$_smtp = "smtp.readyhosting.com";
  	$_login_smtp="david.males@intro-media.com";
  	$_clave_smtp="301982";*/
	$_requiere_autenticacion = true;



	//error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_ERROR );

	//Captura los datos enviados por el formulario o enlace
	$datos = array_merge($_GET,$_POST);

	function fechaCastellano ($fecha) {
		//var_dump($fecha);
		//$fecha = substr($fecha, 0, 10);
		$numeroDia = date('d', strtotime($fecha));
		$dia = date('l', strtotime($fecha));
		$mes = date('F', strtotime($fecha));
		$anio = date('Y', strtotime($fecha));
		$hora = date('H', strtotime($fecha));
		$min = date('i', strtotime($fecha));
		$sec = date('s', strtotime($fecha));
		$dias_ES = array("Lunes", "Martes", "Mi�rcoles", "Jueves", "Viernes", "S�bado", "Domingo");
		$dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
		$nombredia = str_replace($dias_EN, $dias_ES, $dia);
		$meses_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
		$meses_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
		$nombreMes = str_replace($meses_EN, $meses_ES, $mes);
		return $nombredia.", ".$numeroDia." de ".$nombreMes." de ".$anio." $hora:$min:$sec";
	}

?>
