<?php

class Correo
{

/*	******************************************	CONSTRUCTOR CORREO	*******************************************	*/

	function __construct()
	{
	}


/*	**********************************	ENVIAR CORREO	*******************************************************	*/
/*
 *	Utilidad:
 *		Verifica si un archivo fue cargado en el servidor a trav�s del m�todo POST de un formulario
 *	Par�metros de entrada:
 *		$remitente -> Remitente del Mensaje de Correo
 *		$destinatario -> Cadena con la Lista de destinatarios separados por Coma (,)
 *		$asunto -> Asunto o Subject del Mensaje de Correo
 *		$mensaje -> Contenido del Mensaje de Correo
 *		$archivo_adjuntos -> Archivos adjuntos que tendr� el Mensaje de Correo (Arreglo $_FILES)

 *	Valores de retorno:
 *		1, El Mensaje de Correo fue enviado con �xito
 *		-1, uno de los Archivos adjuntos excede el tama�o permitido en el php.ini o el valor especificado en el campo MAX_FILE_SIZE del formulario
 *		-2, uno de los Archivos adjuntos no fue cargado totalmente en el Servidor
 *		0, Se present� un error al enviar el Mensaje de Correo

 *	Notas:
 *		Para fines pr�cticos, se retornan cosas de un solo tipo, en este caso s�lo se retornan n�meros enteros.

 */

	function enviarCorreo($remitente, $destinatario, $asunto, $mensaje, $archivos_adjuntos=NULL,$opciones=NULL)
	{
    	global $_smtp,$_login_smtp,$_clave_smtp,$_requiere_autenticacion,$_puerto_smtp,$_SMTPSecure, $_obj_database, $idi_despliegue;

		include_once("PhpMailer/class.phpmailer.php");
		$mensaje = wordwrap(trim($mensaje), 70);

		if(strpos($remitente, "<") !== false)
		{
			list($remitente_nombre, $remitente_correo) = explode("<", $remitente);
			$remitente_correo = substr($remitente_correo, 0, -1);

			//MAOH - 07 Mayo 2012 - Se codifica el nombre del remitente del mail a utf8 para que lo reconozcan los servidores de correo
			//MAOH - 18 Mayo 2012 - Se traducen caracteres HTML para codificarlos
			$remitente_nombre = html_entity_decode($remitente_nombre, ENT_NOQUOTES, "UTF-8");
			$remitente_nombre = "=?UTF-8?B?".base64_encode($remitente_nombre)."?=";
		}
		else
		{
			$remitente_correo = $remitente;
			$remitente_nombre = "";
		}

        //DM - 2015-01-21
        //Identifica si el correo viene separado por comas o punto y coma para elegir un solo correo
        $destinatario = str_replace(';',',',$destinatario);
        if(strpos($destinatario, ",") != false)
        {
            //Obtiene los correos en una lista para seleccionar el primero
            $lista_destinatarios = preg_split('/,/',$destinatario);
            $lista_destinatarios_correo = array();
            foreach($lista_destinatarios as $correo_destinatario)
            {
                $correo_destinatario = trim($correo_destinatario);
                if( $correo_destinatario != '' )
                {
                    $lista_destinatarios_correo[] = $correo_destinatario;
                }
            }//Fin de foreach($lista_destinatarios as $correo_destinatario)

            //De todos los correos obtiene el primero para asignarlo como destinatario principal
            $destinatario = array_shift($lista_destinatarios_correo);

            //Si existen mas correos los agrega en copia
            if( count($lista_destinatarios_correo) > 0 )
            {
                //Si ya existe una lista de correos en copia
                //debe agregar los nuevos correos
                if( isset( $opciones['CC'] ) )
                {
                    //Si es un arreglo debe revisar que no existan correos repetidos
                    if( is_array($opciones['CC']) )
                    {
                        $opciones['CC'] = array_merge( $opciones['CC'],$lista_destinatarios_correo );
                    }//Fin de if( is_array($opciones['CC']) )
                    else
                    {
                        $lista_destinatarios_correo[] = $opciones['CC'];
                        $opciones['CC'] = $lista_destinatarios_correo;
                    }//Fin de else de if( is_array($opciones['CC']) )
                }
                else
                {
                    //si no existe, asigna la lista
                    $opciones['CC'] = $lista_destinatarios_correo;
                }
            }//Fin de if( count($lista_destinatarios_correo) > 0 )

        }//Fin de if(strpos($destinatario, ",") != false)

		if(strpos($destinatario, "<") != false)
		{
			list($destinatario_nombre, $destinatario_correo) = explode("<", $destinatario);
			$destinatario_correo = substr($destinatario_correo, 0, -1);

			//MAOH - 07 Mayo 2012 - Se codifica el nombre del destinatario del mail a utf8 para que lo reconozcan los servidores de correo
			//MAOH - 18 Mayo 2012 - Se traducen caracteres HTML para codificarlos
			$destinatario_nombre = html_entity_decode($destinatario_nombre, ENT_NOQUOTES, "UTF-8");
			$destinatario_nombre = "=?UTF-8?B?".base64_encode($destinatario_nombre)."?=";
		}
		else
		{
			$destinatario_correo = $destinatario;
			$destinatario_nombre = "";
		}

		//MAOH - 07 Mayo 2012 - Se codifica el asunto del mail a utf8 para que lo reconozcan los servidores de correo
		//MAOH - 18 Mayo 2012 - Se traducen caracteres HTML para codificarlos
		$asunto = html_entity_decode($asunto, ENT_NOQUOTES, "UTF-8");
		$asunto = "=?UTF-8?B?".base64_encode($asunto)."?=";

		//MAOH - 19 Abr 2012 - Se quitan los espacios en blanco
		$remitente_correo = trim($remitente_correo);
		$destinatario_correo = trim($destinatario_correo);

		$mail = new PHPMailer();

		$mail->isHTML(True);
		$mail->AddAddress($destinatario_correo,$destinatario_nombre);

		//JMM-2017-01-30 - Si se ha configurado el envio SMTP desde la configuracion del hotel aplica,
		//de lo contrario se usan los parametros que estan por el momento:
        $usa_smtp = $opciones['opciones']['ecg_host'] != "";
        $usa_smtp &= $opciones['opciones']['ecg_usuario_host'] != "";
        $usa_smtp &= $opciones['opciones']['ecg_clave_host'] != "";

        $usa_autenticacion = $_requiere_autenticacion;

		if ( $usa_smtp )
		{
			$mail->From = $remitente_correo;
			$mail->FromName = $remitente_nombre;
			$mail->Subject = $asunto;
			$mail->Body = $mensaje;
			$mail->SMTPAuth = true;

			$mail->Host = $opciones['opciones']['ecg_host'];
			$mail->Username = $opciones['opciones']['ecg_usuario_host'];
			$mail->Password = $opciones['opciones']['ecg_clave_host'];
            //Si el número de puerto es mayor que cero
            if( $opciones['opciones']['ecg_num_puerto'] > 0 )
            {
                $mail->Port = $opciones['opciones']['ecg_num_puerto'];
            }

            //Si hay un tipo de puerto especificado
            if( $opciones['opciones']['ecg_tipo_puerto'] != "" )
            {
                $mail->SMTPSecure = $opciones['opciones']['ecg_tipo_puerto'];
            }

			$mail->ReturnPath = $remitente_correo;
			$mail->Sender = $remitente_correo;

            $usa_autenticacion = true;
		}
		else
		{
			$mail->From = $remitente_correo;
			$mail->FromName = $remitente_nombre;
			$mail->Subject = $asunto;
			$mail->Body = $mensaje;
			$mail->SMTPAuth = $_requiere_autenticacion;

			$mail->Host = "$_smtp";
			$mail->Username = $_login_smtp;
			$mail->Password = $_clave_smtp;

	  		if($_puerto_smtp!="")
	  		  $mail->Port = $_puerto_smtp;
	  		if($_SMTPSecure!="")
	  		  $mail->SMTPSecure = $_SMTPSecure;
		}
		//Fin del bloque if ($estado != 1)

		if ($usa_autenticacion)
		{
			$mail->IsSMTP();
		}

		//MAOH - 27 Jul 2012
		$mail->SMTPDebug = false;

        //DM - 2017-03-13
        //Por defecto envia correos con utf8
        $mail->CharSet = 'UTF-8';
		if( isset( $opciones['charset'] ) )
        {
            //UTF-8
            $mail->CharSet = $opciones['charset'];
        }//Fin de if( isset( $opciones['charset'] ) )

		if( isset( $opciones['CC'] ) )
        {
            if( is_array($opciones['CC']) )
            {
                //DM - 2015-01-21
                //si hay correos repetidos los elimina
                $opciones['CC'] = array_unique( $opciones['CC'] );
                foreach($opciones['CC'] as $val)
                {
                    $mail->AddAddress( $val  );
                }//Fin de foreach($opciones['CC'] as $val)
            }//Fin de if( is_array($opciones['CC']) )
            else
            {
                $mail->AddAddress( $opciones['CC']  );
            }
        }//Fin de if( isset(['CC']) )

        if( isset( $opciones['BCC'] ) )
        {
            $opciones['BCC'] = preg_split("/,/",$opciones['BCC']);
            foreach($opciones['BCC'] as $val)
            {
                $mail->AddBCC($val);
            }
        }

		//archivos adjuntos
		if(is_array($archivos_adjuntos) && (sizeof($archivos_adjuntos)>0))
		{
			foreach($archivos_adjuntos as $archivo_adjunto)
			{
				  $mail->AddAttachment($archivo_adjunto["tmp_name"], $archivo_adjunto["name"], 'base64', $archivo_adjunto["type"]);
			}
		}

		$resultado= $mail->Send();
		if (!$resultado)
		{
		  //echo $mail->ErrorInfo; //MAOH - 27 Jul 2012
		  return 0;
		}
		else
		  return 1;
	}
}
?>
