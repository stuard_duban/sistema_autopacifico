<?php

/*
  CARLOS H. CERON M. CARCERON@HOTMAIL.COM
  JULIO 01 2008

  FUNCIONES VARIAS QUE PUEDEN SER UTILIZADAS POR CUALQUIER CLASE
 */

class Herramientas {

    //devuelve la fecha y hora actual del sistema
    function obtenerFechaYHoraActual() {
        $dato = getDate();
        $hora = $dato["hours"];
        $minutos = $dato["minutes"];
        $dia = $dato["mday"];
        $mes = $dato["mon"];
        $anho = $dato["year"];

        if ($mes < 10)
            $mes = "0$mes";

        if ($dia < 10)
            $dia = "0$dia";

        $fecha_actual = "$anho-$mes-$dia";
        $hora_actual = "$hora:$minutos";

        $info["fecha"] = $fecha_actual;
        $info["hora"] = $hora_actual;

        return $info;
    }

    static function obtenerNombreMes($mes, $datos = array()) {
        global $_SESSION;

        $mes--;

        $idi_id = $_SESSION['idi_id'];
        if ($datos['idi_id'] != "") {
            $idi_id = $datos['idi_id'];
        }//Fin de if( $datos['idi_id'] != "" )

        if ($idi_id == 2) //ingles
            $meses = array("January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December");
        else if ($idi_id == 3) //frances
            $meses = array("Janvier", "F&eacute;vrier", "Mars", "Avril", "Mai", "Juin",
                "Juillet", "Ao&ucirc;t", "Septembre", "Octobre", "Novembre", "D&eacute;cembre");
        else if ($idi_id == 4) //aleman
            $meses = array("Januar", "Februar", "M&auml;rz", "April", "Mai", "Juni",
                "Juli", "August", "September", "Oktober", "November", "Dezember");
        else if ($idi_id == 7) //portugues
            $meses = array("Janeiro", "Fevereiro", "Mar&ccedil;o", "Abril", "Maio", "Junho",
                "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro");
        else if ($idi_id == 10) //Polaco
            $meses = array("Stycznia", "Lutego", "Marca", "Kwietnia", "Maja", "Czerwca",
                "Lipca", "Sierpnia", "WrzeÃƒÆ’Ã¢â‚¬Â¦ÃƒÂ¢Ã¢â€šÂ¬Ã‚Âºnia", "PaÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Âºdziernika", "Listopada", "Grudnia");
        else if ($idi_id == 13) //Chino
            $meses = array("ÃƒÆ’Ã‚Â¤Ãƒâ€šÃ‚Â¸ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÆ’Ã‚Â¦Ãƒâ€¦Ã¢â‚¬Å“Ãƒâ€¹Ã¢â‚¬Â ", "ÃƒÆ’Ã‚Â¤Ãƒâ€šÃ‚ÂºÃƒâ€¦Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¦Ãƒâ€¦Ã¢â‚¬Å“Ãƒâ€¹Ã¢â‚¬Â ", "ÃƒÆ’Ã‚Â¤Ãƒâ€šÃ‚Â¸ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â°ÃƒÆ’Ã‚Â¦Ãƒâ€¦Ã¢â‚¬Å“Ãƒâ€¹Ã¢â‚¬Â ", "ÃƒÆ’Ã‚Â¥ÃƒÂ¢Ã¢â€šÂ¬Ã‚ÂºÃƒÂ¢Ã¢â€šÂ¬Ã‚ÂºÃƒÆ’Ã‚Â¦Ãƒâ€¦Ã¢â‚¬Å“Ãƒâ€¹Ã¢â‚¬Â ", "ÃƒÆ’Ã‚Â¤Ãƒâ€šÃ‚ÂºÃƒÂ¢Ã¢â€šÂ¬Ã¯Â¿Â½ÃƒÆ’Ã‚Â¦Ãƒâ€¦Ã¢â‚¬Å“Ãƒâ€¹Ã¢â‚¬Â ", "ÃƒÆ’Ã‚Â¥ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦Ãƒâ€šÃ‚Â­ÃƒÆ’Ã‚Â¦Ãƒâ€¦Ã¢â‚¬Å“Ãƒâ€¹Ã¢â‚¬Â ",
                "ÃƒÆ’Ã‚Â¤Ãƒâ€šÃ‚Â¸Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¦Ãƒâ€¦Ã¢â‚¬Å“Ãƒâ€¹Ã¢â‚¬Â ", "ÃƒÆ’Ã‚Â¥ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦Ãƒâ€šÃ‚Â«ÃƒÆ’Ã‚Â¦Ãƒâ€¦Ã¢â‚¬Å“Ãƒâ€¹Ã¢â‚¬Â ", "ÃƒÆ’Ã‚Â¤Ãƒâ€šÃ‚Â¹Ãƒâ€šÃ¯Â¿Â½ÃƒÆ’Ã‚Â¦Ãƒâ€¦Ã¢â‚¬Å“Ãƒâ€¹Ã¢â‚¬Â ", "ÃƒÆ’Ã‚Â¥Ãƒâ€šÃ¯Â¿Â½Ãƒâ€šÃ¯Â¿Â½ÃƒÆ’Ã‚Â¦Ãƒâ€¦Ã¢â‚¬Å“Ãƒâ€¹Ã¢â‚¬Â ", "ÃƒÆ’Ã‚Â¥Ãƒâ€šÃ¯Â¿Â½Ãƒâ€šÃ¯Â¿Â½ÃƒÆ’Ã‚Â¤Ãƒâ€šÃ‚Â¸ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÆ’Ã‚Â¦Ãƒâ€¦Ã¢â‚¬Å“Ãƒâ€¹Ã¢â‚¬Â ", "ÃƒÆ’Ã‚Â¥Ãƒâ€šÃ¯Â¿Â½Ãƒâ€šÃ¯Â¿Â½ÃƒÆ’Ã‚Â¤Ãƒâ€šÃ‚ÂºÃƒâ€¦Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¦Ãƒâ€¦Ã¢â‚¬Å“Ãƒâ€¹Ã¢â‚¬Â ");
        else if ($idi_id == 14) //	Ruso
            $meses = array("ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â¯ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â½ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â²ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â°ÃƒÆ’Ã¢â‚¬ËœÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÆ’Ã¢â‚¬ËœÃƒâ€¦Ã¢â‚¬â„¢", "ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â¤ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚ÂµÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â²ÃƒÆ’Ã¢â‚¬ËœÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â°ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â»ÃƒÆ’Ã¢â‚¬ËœÃƒâ€¦Ã¢â‚¬â„¢", "ÃƒÆ’Ã¯Â¿Â½Ãƒâ€¦Ã¢â‚¬Å“ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â°ÃƒÆ’Ã¢â‚¬ËœÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÆ’Ã¢â‚¬ËœÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡", "ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ¯Â¿Â½ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â¿ÃƒÆ’Ã¢â‚¬ËœÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚ÂµÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â»ÃƒÆ’Ã¢â‚¬ËœÃƒâ€¦Ã¢â‚¬â„¢", "ÃƒÆ’Ã¯Â¿Â½Ãƒâ€¦Ã¢â‚¬Å“ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â°ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â¹", "ÃƒÆ’Ã¯Â¿Â½Ãƒâ€¹Ã…â€œÃƒÆ’Ã¢â‚¬ËœÃƒâ€¦Ã‚Â½ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â½ÃƒÆ’Ã¢â‚¬ËœÃƒâ€¦Ã¢â‚¬â„¢",
                "ÃƒÆ’Ã¯Â¿Â½Ãƒâ€¹Ã…â€œÃƒÆ’Ã¢â‚¬ËœÃƒâ€¦Ã‚Â½ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â»ÃƒÆ’Ã¢â‚¬ËœÃƒâ€¦Ã¢â‚¬â„¢", "ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ¯Â¿Â½ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â²ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â³ÃƒÆ’Ã¢â‚¬ËœÃƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬ËœÃƒâ€šÃ¯Â¿Â½ÃƒÆ’Ã¢â‚¬ËœÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡", "ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚ÂµÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â½ÃƒÆ’Ã¢â‚¬ËœÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬ËœÃƒâ€šÃ¯Â¿Â½ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â±ÃƒÆ’Ã¢â‚¬ËœÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÆ’Ã¢â‚¬ËœÃƒâ€¦Ã¢â‚¬â„¢", "ÃƒÆ’Ã¯Â¿Â½Ãƒâ€¦Ã‚Â¾ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚ÂºÃƒÆ’Ã¢â‚¬ËœÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬ËœÃƒâ€šÃ¯Â¿Â½ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â±ÃƒÆ’Ã¢â‚¬ËœÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÆ’Ã¢â‚¬ËœÃƒâ€¦Ã¢â‚¬â„¢", "ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ¯Â¿Â½ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â¾ÃƒÆ’Ã¢â‚¬ËœÃƒâ€šÃ¯Â¿Â½ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â±ÃƒÆ’Ã¢â‚¬ËœÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÆ’Ã¢â‚¬ËœÃƒâ€¦Ã¢â‚¬â„¢", "ÃƒÆ’Ã¯Â¿Â½ÃƒÂ¢Ã¢â€šÂ¬Ã¯Â¿Â½ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚ÂµÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚ÂºÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â°ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â±ÃƒÆ’Ã¢â‚¬ËœÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÆ’Ã¢â‚¬ËœÃƒâ€¦Ã¢â‚¬â„¢");
        else if ($idi_id == 15) //Ucraniano
            $meses = array("ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã¢â‚¬ËœÃƒÂ¢Ã¢â€šÂ¬Ã¢â‚¬Å“ÃƒÆ’Ã¢â‚¬ËœÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¡ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚ÂµÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â½ÃƒÆ’Ã¢â‚¬ËœÃƒâ€¦Ã¢â‚¬â„¢", "ÃƒÆ’Ã¯Â¿Â½ÃƒÂ¢Ã¢â€šÂ¬Ã‚ÂºÃƒÆ’Ã¢â‚¬ËœÃƒâ€¦Ã‚Â½ÃƒÆ’Ã¢â‚¬ËœÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â¸ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â¹", "ÃƒÆ’Ã¯Â¿Â½ÃƒÂ¢Ã¢â€šÂ¬Ã‹Å“ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚ÂµÃƒÆ’Ã¢â‚¬ËœÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚ÂµÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â·ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚ÂµÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â½ÃƒÆ’Ã¢â‚¬ËœÃƒâ€¦Ã¢â‚¬â„¢", "ÃƒÆ’Ã¯Â¿Â½Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â²ÃƒÆ’Ã¢â‚¬ËœÃƒÂ¢Ã¢â€šÂ¬Ã¢â‚¬Å“ÃƒÆ’Ã¢â‚¬ËœÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚ÂµÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â½ÃƒÆ’Ã¢â‚¬ËœÃƒâ€¦Ã¢â‚¬â„¢", "ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã¢â‚¬ËœÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â°ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â²ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚ÂµÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â½ÃƒÆ’Ã¢â‚¬ËœÃƒâ€¦Ã¢â‚¬â„¢", "ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â§ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚ÂµÃƒÆ’Ã¢â‚¬ËœÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â²ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚ÂµÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â½ÃƒÆ’Ã¢â‚¬ËœÃƒâ€¦Ã¢â‚¬â„¢",
                "ÃƒÆ’Ã¯Â¿Â½ÃƒÂ¢Ã¢â€šÂ¬Ã‚ÂºÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â¸ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â¿ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚ÂµÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â½ÃƒÆ’Ã¢â‚¬ËœÃƒâ€¦Ã¢â‚¬â„¢", "ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚ÂµÃƒÆ’Ã¢â‚¬ËœÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â¿ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚ÂµÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â½ÃƒÆ’Ã¢â‚¬ËœÃƒâ€¦Ã¢â‚¬â„¢", "ÃƒÆ’Ã¯Â¿Â½ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚ÂµÃƒÆ’Ã¢â‚¬ËœÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚ÂµÃƒÆ’Ã¢â‚¬ËœÃƒâ€šÃ¯Â¿Â½ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚ÂµÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â½ÃƒÆ’Ã¢â‚¬ËœÃƒâ€¦Ã¢â‚¬â„¢", "ÃƒÆ’Ã¯Â¿Â½ÃƒÂ¢Ã¢â€šÂ¬Ã¢â‚¬Å“ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â¾ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â²ÃƒÆ’Ã¢â‚¬ËœÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚ÂµÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â½ÃƒÆ’Ã¢â‚¬ËœÃƒâ€¦Ã¢â‚¬â„¢", "ÃƒÆ’Ã¯Â¿Â½ÃƒÂ¢Ã¢â€šÂ¬Ã‚ÂºÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â¸ÃƒÆ’Ã¢â‚¬ËœÃƒâ€šÃ¯Â¿Â½ÃƒÆ’Ã¢â‚¬ËœÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â¾ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â¿ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â°ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â´", "ÃƒÆ’Ã¯Â¿Â½ÃƒÂ¢Ã¢â€šÂ¬Ã…â€œÃƒÆ’Ã¢â‚¬ËœÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÆ’Ã¢â‚¬ËœÃƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â´ÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚ÂµÃƒÆ’Ã¯Â¿Â½Ãƒâ€šÃ‚Â½ÃƒÆ’Ã¢â‚¬ËœÃƒâ€¦Ã¢â‚¬â„¢");
        else
            $meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
                "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

        return $meses[$mes];
    }

    //convierte una fecha en formato yyyy-mm-dd a dd-mm-yyyy
    function convertirFecha($fecha) {
        if ($fecha != "") {
            //si tiempo el campo de hora
            $partes = explode(" ", $fecha);
            $hora = $partes[1] != "" ? " " . $partes[1] : $partes[1];

            //obtiene el campo de fecha
            $partes = explode("-", $partes[0]);

            //verifica que el campo aÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢o este al inicio y sea mayor que 4 digitos
            //si no es asi indica que esta en el formato solicitado
            if (is_numeric($partes[0]) && $partes[0] > 1000) {
                return "{$partes[2]}-{$partes[1]}-{$partes[0]}" . $hora;
            }//Fin de if( is_numeric($partes[0]) && $partes[0] > 1000 )
        }
        return $fecha;
    }

    static function convertirFechaFormatoDB($fecha, $tag = "/"){
        //verifica que el formato este en dd-mm-aaaa de lo contrario deja la cadena sin modificaciones
        $partes = explode($tag, $fecha); //dd-mm-aaaa
        if (is_numeric($partes[0]) && $partes[0] < 32) {
            return "{$partes[2]}-{$partes[1]}-{$partes[0]}";
        }
        return $fecha;
    }

    // Fecha en formato dd/mm/yyyy o dd-mm-yyyy retorna la diferencia en dias
    function restaFechas($dFecIni, $dFecFin) {


        $dFecIni = str_replace("-", "", $dFecIni);
        $dFecIni = str_replace("/", "", $dFecIni);
        $dFecFin = str_replace("-", "", $dFecFin);
        $dFecFin = str_replace("/", "", $dFecFin);

        ereg("([0-9]{1,2})([0-9]{1,2})([0-9]{2,4})", $dFecIni, $aFecIni);
        ereg("([0-9]{1,2})([0-9]{1,2})([0-9]{2,4})", $dFecFin, $aFecFin);

        $date1 = mktime(0, 0, 0, $aFecIni[2], $aFecIni[1], $aFecIni[3]);
        $date2 = mktime(0, 0, 0, $aFecFin[2], $aFecFin[1], $aFecFin[3]);

        return round(($date2 - $date1) / (60 * 60 * 24));
    }

    /*     * ********************************  CREAR SELECT FORMA  ********************************* */
    /*
     * 	Utilidad:
     * 		Crea un select (html) de una consulta en la BD
     * 	Parametros de entrada:
     * 		$nombre_campo: nombre del select, es decir name=$nombre_campo
     * 		$nombre_tabla: Nombre de la tabla donde se va a consultar
     * 		$atributos: Arreglo con los atributos que se van a consultar
     * 		$condicion: Condicion de la consulta en la BD
     * 		$orden: Ordenar los datos de la consulta ORDER BY
     * 		$posicion: Identificador del valor seleccionado por defecto "si lo hay" (selected)
     * 		$opciones: opciones adicionales en el select
     * 	Valores de Retorno:
     * 		$select: el codigo html del select
     */

    function crearSelectForma($datos, $desactivada = '') {
        global $_obj_database, $idi_despliegue;

        $texto_seleccionar = $idi_despliegue['seleccionar'];

        if ($texto_seleccionar == "") {
            $texto_seleccionar = $idi_despliegue['seleccionar'];
        }

        if ($datos['texto_defecto_seleccionar'] != "") {
            $texto_seleccionar = $datos['texto_defecto_seleccionar'];
        }//Fin de if( $datos['texto_defecto_seleccionar'] != "" )

        $codigo = $datos['atributos']['codigo']; // El identificador
        $mostrar = $datos['atributos']['mostrar']; // El nombre a mostrar en el select

        $resultado = $_obj_database->obtenerRegistrosAsociativosPorClave($datos['sql'], $codigo);

        //Esto es para reducir el tamaÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â±o de la letra al select cuando se despliega en pantallas pequeÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â±as
        if ($_GET['anchoPantalla'] != '') {
            if ($_GET['anchoPantalla'] <= 800) {
                $fuenteInicio = "<font size='1'>";
                $fuenteFinal = "</font>";
            } else {
                $fuenteInicio = "";
                $fuenteFinal = "";
            }
        } else {
            $fuenteInicio = "";
            $fuenteFinal = "";
        }//Fin Esto es para reducir el tamaÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â±o de la letra al select cuando se despliega en pantallas pequeÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â±as

        //JMM-2017-08-02 - Desarrollo #10532
        if ($datos['css'] != "") {
        	$estilo_css = $datos['css'];
        }//Fin de if ($datos['css'] != "")

        //JMM-2017-09-04 - Desarrollo #10532
        if ($datos['usa_flecha_chevron'] == 1) {
        	$select = $fuenteInicio . "<select id='id_select' onClick='ShowSelected();' ".$estilo_css." name=\"" . $datos['nombre_campo'] . "\" " . $datos['adicionales'] . " data-native-menu='false' " . $desactivada . ">";
        } else {
        	//Agosto 12 de 2011: El atributo data-native-menu='false' se agrego para cuando el cuestionario es para moviles y lo reconozca jQuery Mobile
        	$select = $fuenteInicio . "<select name=\"" . $datos['nombre_campo'] . "\" " . $datos['adicionales'] . " data-native-menu='false' " . $desactivada . ">";
        }

        //DM - 18-01-2012
        // Permite ocultar el valor por defecto
        if ($datos['ocultar_valor_por_defecto'] != 1) {
            //Valor por defecto
            $select .= "<option value='".$datos['valor_opcion_por_defecto']."' ".$datos['atributos_valor_por_defecto']." align=center>" . $texto_seleccionar . "</option>";
        }

        //Calcula el tamaÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢o de resultado para llenar el select en el for.
        $numero = sizeof($resultado);

        if( is_array($datos['opciones_adicionales_inicial']) )
        {
            foreach ($datos['opciones_adicionales_inicial'] as $res) {
                $codigoFila = $res[$codigo];
                $mostrarFila = $res[$mostrar]; //MAOH - 19 Abr 2012 - Cambio a UTF 8

                if ($datos['limite_texto_campo'] != "")
                {
                    $mostrarFila = substr($mostrarFila, 0, $datos['limite_texto_campo']);
                }//Fin de if( $datos['limite_texto_campo'] != "" )

                $select .= "<option value='$codigoFila'";
                if ($datos['valor'] == $codigoFila) {
                    $select .= " selected ";
                }
                $select .= "> $mostrarFila</option>";
            } // Fin foreach($resultado as $res)
        }//Fin de if( is_array($datos['opciones_adicionales_inicial']) )

        if (is_array($resultado) && $numero > 0) {
            foreach ($resultado as $res) {
                $codigoFila = $res[$codigo];
                $mostrarFila = $res[$mostrar]; //MAOH - 19 Abr 2012 - Cambio a UTF 8

                if ($datos['limite_texto_campo'] != "") {
                    $mostrarFila = substr($mostrarFila, 0, $datos['limite_texto_campo']);
                }//Fin de if( $datos['limite_texto_campo'] != "" )

                $select .= "<option value='$codigoFila'";
                if ($datos['valor'] == $codigoFila) {
                    $select .= " selected ";
                }
                $select .= "> $mostrarFila</option>";
            } // Fin foreach($resultado as $res)
        } // Fin if ($resultado != -4)

        if( is_array($datos['opciones_adicionales_final']) )
        {
            foreach ($datos['opciones_adicionales_final'] as $res) {
                $codigoFila = $res[$codigo];
                $mostrarFila = $res[$mostrar]; //MAOH - 19 Abr 2012 - Cambio a UTF 8

                if ($datos['limite_texto_campo'] != "")
                {
                    $mostrarFila = substr($mostrarFila, 0, $datos['limite_texto_campo']);
                }//Fin de if( $datos['limite_texto_campo'] != "" )

                $select .= "<option value='$codigoFila'";
                if ($datos['valor'] == $codigoFila) {
                    $select .= " selected ";
                }
                $select .= "> $mostrarFila</option>";
            } // Fin foreach($resultado as $res)
        }//Fin de if( is_array($datos['opciones_adicionales_final']) )

        //JMM-2017-09-04 - Desarrollo #10532
        if ($datos['usa_flecha_chevron'] == 1) {
        	$select .= "</select>
        		<i id='flecha_left' class='fa fa-chevron-left'></i>" . $fuenteFinal;
        } else {
        	$select .= "</select>" . $fuenteFinal;
        }

        return $select;
    }

// Fin crearSelectForma ()


    /** crearListadoRadioForma
     * parametro: $datos
     * autor : JCB - DESARROLLO AUTOPACIFICO
     * descripcion: Crea una lista de valores con radio boton.
     **/
   static function crearListadoRadioForma($name_campo,$valuesCampo) {
        global $_obj_database, $idi_despliegue;

        $contenido = "";

        $numero = sizeof($valuesCampo);

        if (is_array($valuesCampo) && $numero > 0) {
        $valOpcion=1;
        	foreach ($valuesCampo as $val) {
		            /*$contenido = "<input type='radio' name='" . $name_campo . "' value='" . $val . "'
		                            onClick=\"OnClickRadio(this.form.name, this.form.all_" . $id_campo . ",'" . $datos['nombre_campo'] . "');\" >";
		            $contenido .= "&nbsp;" . $idi_despliegue['todas'] . "<br>\n";*/

        			$checked = ($valOpcion==1) ? "checked" : "";
		            $contenido .= "<input type='radio' name='" . $name_campo . "' value='" . $valOpcion . "'  $checked>".utf8_encode($val);
		            $contenido .= "&nbsp;<br>\n";
		            $valOpcion++;
        	}
        }

        return $contenido;
    }
    /** crearListadoCheckForma
     * parametro: $name_campo,$valuesCampo
     * autor : JCB - DESARROLLO AUTOPACIFICO
     * descripcion: Crea una lista de valores con radio boton.
     **/
    static function crearListadoCheckForma($name_campo,$valuesCampo) {
    	global $_obj_database, $idi_despliegue;

    	$contenido = "";

    	$numero = sizeof($valuesCampo);

    	if (is_array($valuesCampo) && $numero > 0) {
    		$valOpcion=1;
    		foreach ($valuesCampo as $val) {
    			/*$contenido = "<input type='radio' name='" . $name_campo . "' value='" . $val . "'
    			 onClick=\"OnClickRadio(this.form.name, this.form.all_" . $id_campo . ",'" . $datos['nombre_campo'] . "');\" >";
    			 $contenido .= "&nbsp;" . $idi_despliegue['todas'] . "<br>\n";*/

    			$checked = ($valOpcion==1) ? "checked" : "";
    			$contenido .= "<input type='checkbox' name='" . $name_campo . "' value='" . $valOpcion . "'  $checked>".utf8_encode($val);
    			$contenido .= "&nbsp;<br>\n";
    			$valOpcion++;
    		}
    	}

    	return $contenido;
    }


    /** crearListadoFacturas
     * parametro: $facturas
     * autor : Stuard Romero - DESARROLLO AUTOPACIFICO
     * descripcion: Crea una lista de valores con radio boton.
     **/
    static function crearListadoFacturas($facturas) {
    	global $_obj_database, $idi_despliegue;
        setlocale(LC_MONETARY, 'es_ES');
    	$contenido = "";
      //print_r($facturas);exit;
    	$numero = sizeof($facturas);

    	if (is_array($facturas) && $numero > 0) {
    		$valOpcion=1;
        $i = 0;
    		//foreach ($valuesCampo as $val) {
        foreach ($facturas as $factura) {
            //impar
            if($factura['estado_pse'] == 'PENDING') {
                $pendiente = "disabled";
                $mensaje = "<p>(Transacción pendiente por confirmar con la entidad bancaria)</p>";
            }

  			    if($i % 2 == 0) {
                $checked = ($valOpcion==1) ? "checked" : "";
      			    $contenido .= "<div class='col-md-10 fondo-gray ". $pendiente . "'><input type='checkbox' class='factura' name='factu[".$factura['descripcion']."][]' value='" . $factura['valor_total'] . "|". $factura['iva'] ."|". $factura['Numero']. "' $pendiente>".$factura['descripcion']." ". (isset($mensaje) ? $mensaje : '') . "</div>";
                //$contenido .= (isset($mensaje) ? $mensaje : "");
                $contenido .= "<div class='col-md-2 text-right fondo-gray ". $pendiente . "'>$ " . number_format($factura['valor_total'], 2, ',', '.') . "</div>";
      			//$contenido .= "&nbsp;<br>\n";

            }
            else {
                $checked = ($valOpcion==1) ? "checked" : "";
                $contenido .= "<div class='col-md-10 fondo-white ". $pendiente . "'><input type='checkbox' class='factura' name='factu[".$factura['descripcion']."][]' value='" . $factura['valor_total'] . "|". $factura['iva'] ."|". $factura['Numero']. "' $pendiente>".$factura['descripcion']." ". (isset($mensaje) ? $mensaje : '') . "</div>";
                //$contenido .= (isset($mensaje) ? $mensaje : "");
                $contenido .= "<div class='col-md-2 text-right fondo-white ". $pendiente . "'>$ " . number_format($factura['valor_total'], 2, ',', '.') . "</div>";
                //$contenido .= "&nbsp;<br>\n";

            }
            $pendiente = "";
            $valOpcion++;
            $i++;
          //}
    		}
        $contenido .= "<div class='col-md-9 contenedor-total'><h2 style='font-size: 18px;'><strong>Total: </strong></h2></div>
        <div class='col-md-3 text-right'><input type='number' name='displayMontoFactura' id='displayMonto' class='form-control' readonly='readonly'>
            <input type='number' name='montoFactura' id='monto' class='form-control' required='required' style='display:none;'>
        </div>";
    	}

    	return $contenido;
    }



    /** crearSelectFormaAuto
     * parametro: $datos
     * autor : JCB - DESARROLLO AUTOPACIFICO
     * descripcion: Crea una lista select consultando de bd las opciones.
     **/
    static function crearSelectFormaAuto($nombre_campo,$texto_seleccionar="Selecciona una opción",$opciones,$es_requerido,$valor_defecto='',$desactivada="") {


    	$requerido = ($es_requerido == 1) ? 'required' : '';
		$select ="<select id=\"" . $nombre_campo . "\" name=\"" . $nombre_campo . "\" " . $datos['adicionales'] . " class='form-control' data-native-menu='false' " . $desactivada . " $requerido >";

    	//Valor por defecto
    	$select .= "<option value='".$valor_defecto."'align=center>" . $texto_seleccionar . "</option>";
    	$numero = sizeof($opciones['DATA']);
    	$clave_valor=$opciones['clave_valor'];
    	if (is_array($opciones['DATA']) && $opciones['DATA'] > 0) {
    		foreach ($opciones['DATA'] as $opc) {
    			$opc=$opc[$clave_valor];
    			$select .= "<option value='".$opc."'";
    			if($valor_defecto){
    				if ($opc== utf8_encode($valor_defecto)) {
    					$select .= " selected ";
    				}
    			}

    			$select .= ">$opc</option>";
    		} // Fin foreach($resultado as $res)
    	} // Fin if ($resultado != -4)

    	//JMM-2017-09-04 - Desarrollo #10532
    	if ($datos['usa_flecha_chevron'] == 1) {
    		$select .= "</select>
        		<i id='flecha_left' class='fa fa-chevron-left'></i>" . $fuenteFinal;
    	} else {
    		$select .= "</select>" . $fuenteFinal;
    	}

    	return $select;
    }

    /** crearSelectFormaAuto
     * parametro: $datos
     * autor : JCB - DESARROLLO AUTOPACIFICO
     * descripcion: Crea una lista select consultando de bd las opciones.
     **/
    static function crearSelectFormalist($nombre_campo,$texto_seleccionar="Selecciona una opción",$opciones,$es_requerido,$valor_defecto='',$desactivada="") {


      $requerido = ($es_requerido == 1) ? 'required' : '';
    $select ="<select id=\"" . $nombre_campo . "\" name=\"" . $nombre_campo . "\" " . $datos['adicionales'] . " class='form-control' data-native-menu='false' " . $desactivada . " $requerido >";


      //Valor por defecto
      $select .= "<option value='".$valor_defecto."'align=center>" . $texto_seleccionar . "</option>";

      if (is_array($opciones) && $opciones > 0) {
        foreach ($opciones as $opc) {

          $select .= "<option value='".$opc['descripcion']."|".$opc['concepto']."'>".$opc['descripcion']."</option>";


          //$select .= ">$opc</option>";
        } // Fin foreach($resultado as $res)
      } // Fin if ($resultado != -4)

      //JMM-2017-09-04 - Desarrollo #10532
      /*if ($datos['usa_flecha_chevron'] == 1) {
        $select .= "</select>
            <i id='flecha_left' class='fa fa-chevron-left'></i>" . $fuenteFinal;
      } else {
        $select .= "</select>" . $fuenteFinal;
      }*/
      $select .= "</select>";

      return $select;
    }

//Fin de crearCheckForma()
    //MAOH - 22 Dic 2011 - Se agrega la imagen a mostrar para activar el calendario
    //MAOH - 20 Jun 2012 - Se agregan los offset (offX, offY) por si se desea mostrar el calendario en otra posicion
    function construirCalendario($objeto, $nombre_formulario, $nombre_campo, $retornar = 0, $imagen = "dynCalendar.gif", $offsetX = "", $offsetY = "") {
        global $_PATH_IMAGENES;

        if ($retornar == 0)
            echo "<script language='JavaScript' type='text/javascript'>
		   $objeto = new dynCalendar('$objeto', 'bajarFecha','$_PATH_IMAGENES/', '$imagen', '$offsetX', '$offsetY');
		   $objeto.nom_form = '$nombre_formulario';
		   $objeto.nom_campo = '$nombre_campo';
	    	   $objeto.setMonthCombo(true);
		   $objeto.setYearCombo(true);
		   </script>";
        else {
            $calendario = "&nbsp;<script language='JavaScript' type='text/javascript'>
		   $objeto = new dynCalendar('$objeto', 'bajarFecha','$_PATH_IMAGENES/', '$imagen', '$offsetX', '$offsetY');
		   $objeto.nom_form = '$nombre_formulario';
		   $objeto.nom_campo = '$nombre_campo';
	    	   $objeto.setMonthCombo(true);
		   $objeto.setYearCombo(true);
		   </script>";
            //  &nbsp;&nbsp;<input type='button' class='boton' value='Limpiar Fecha' onClick='document.$nombre_formulario.$nombre_campo.value=\"\";return false;'>";


            return $calendario;
        }
    }

    /**
     * Se encarga de validar si la cadena es o no una url valida
     * DM - 2015-05-20
     * */
    function validarURL($url) {
        $url = trim($url);

        $pattern = "/\b(?:(?:https?):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i";

        if (!preg_match($pattern, $url)) {
            return false;
        }

        return true;
    }

//Fin de validarURL

    /**
     * Valida que un correo se encuentre bien formado
     * DM - 2015-03-25
     * */
    function validarCorreo($email) {
        $email = trim($email);

        if (!preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+\.+([a-zA-Z0-9\._-]+)+$/", $email)) {
            return false;
        }

        return true;
    }

//Fin de validarCorreo

    /**
     * Funcion que se encarga de eliminar los caracteres especiales de una cadena
     * MD - 18-01-2012
     * * */
    static function eliminarCaracteresEspeciales($cadena) {

        $cadena = html_entity_decode($cadena);

        $b = array("ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¡", "ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â©", "ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â­", "ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â³", "ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Âº", "ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¤", "ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â«", "ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¯", "ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¶", "ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¼", "ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â ", "ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¨", "ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¬", "ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â²", "ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¹", "ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â±", " ", ",", ".", ";", ":", "ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¡", "!", "ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¿", "?", '"');
        $c = array("a", "e", "i", "o", "u", "a", "e", "i", "o", "u", "a", "e", "i", "o", "u", "n", "", "", "", "", "", "", "", "", "", '');

        $cadena = utf8_encode($cadena);
        $cadena = str_replace($b, $c, $cadena);

        //DM - 2015-09-17
        $cadena = Herramientas::quitarTildes($cadena);
        $cadena = Herramientas::eliminarTildes($cadena);

        return $cadena;
    }

//Fin de eliminarCaracteresEspeciales()



    static function enlaceCampos($datos, $campos) {
        $enlace = "";
        foreach ($campos as $key) {
            if (is_array($datos[$key])) {
                foreach ($datos[$key] as $valor) {
                    $enlace .= "&" . $key . "[]=" . $valor;
                }
            } else if ($datos[$key] != '') {
                $enlace .= "&" . $key . "=" . $datos[$key];
            }
        }//Fin de foreach($campos as $key)
        return $enlace;
    }

//Fin de enlaceCampos

    /**
     * Funcion que se encarga de crear los campos ocultos
     * de los campos que se pasen por parametro
     * DM - 2013-09-16
     * */
    function generarHTMLCamposOcultos($datos, $campos) {
        $campos_html = '';
        foreach ($campos as $key) {
            if (is_array($datos[$key])) {
                foreach ($datos[$key] as $valor) {
                    $campos_html .= '<input type="hidden" name="' . $key . '[]" value="' . $valor . '" >';
                }
            } else if ($datos[$key] != '') {
                $campos_html .= '<input type="hidden" name="' . $key . '" value="' . $datos[$key] . '" >';
            }
        }//Fin de foreach($campos as $key)

        return $campos_html;
    }

//Fin de generarHTMLCamposOcultos

    function crearSelectFormaPersonalizada($datos, $inicial, $desactivada = '', $valor_inicial = '') {
        global $_obj_database, $idi_despliegue;

        $inicial = $inicial == '' ? $idi_despliegue['seleccionar'] : $inicial;

        if ($inicial == '') {
            $inicial = 'Seleccionar';
        }

        $codigo = $datos['atributos']['codigo']; // El identificador
        $mostrar = $datos['atributos']['mostrar']; // El nombre a mostrar en el select

        /* if(is_array($datos['opciones'])){

          }
          if(!empty($datos['sql'])){
          $resultado = $_obj_database->obtenerRegistrosAsociativos($datos['sql']);
          } */

        $resultado = $_obj_database->obtenerRegistrosAsociativos($datos['sql']);

        $select = "<select name='" . $datos['nombre_campo'] . "' " . $datos['adicionales'] . " " . $desactivada . ">";

        if (!$datos['ultima_posicion_opcion_defecto']) {
            //Valor por defecto
            $select .= $datos['ocultar_opcion_defecto'] ? "" : "<option value='$valor_inicial' align=center> " . $inicial . " </option>";
        }
        //Calcula el tamaÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢o de resultado para llenar el select en el for.
        $numero = sizeof($resultado);

        if (is_array($resultado) && $numero > 0) {
            foreach ($resultado as $res) {
                $codigoFila = $res[$codigo];
                $mostrarFila = $res[$mostrar]; //MAOH - 19 Abr 2012 - Cambio a UTF 8

                if ($datos['limite_texto_campo'] != "") {
                    $mostrarFila = substr($mostrarFila, 0, $datos['limite_texto_campo']);
                }//Fin de if( $datos['limite_texto_campo'] != "" )

                $select .= "<option value='$codigoFila'";
                if ($datos['valor'] == $codigoFila) {
                    $select .= " selected ";
                }
                $select .= "> $mostrarFila</option>";
            } // Fin foreach($resultado as $res)
        } // Fin if ($resultado != -4)

        if ($datos['ultima_posicion_opcion_defecto']) {
            //Valor por defecto
            $select .= $datos['ocultar_opcion_defecto'] ? "" : "<option " . ($datos['valor'] == $valor_inicial ? 'selected' : '') . " value='$valor_inicial' align=center> " . $inicial . " </option>";
        }

        $select .= "</select>";

        return $select;
    }

// Fin crearSelectFormaPersonalizada ()

    /*
     * pgs - 10/08/2012
     * Funcion que crea un select desde un array
     */

    function crearSelectFormaDesdeArray($datos, $inicial = '') {
        global $_obj_database, $idi_despliegue;

        $resultado = $datos['array'];

        $inicial = $inicial == '' ? $idi_despliegue['seleccionar'] : $inicial;

        $select = "<select name=\"" . $datos['nombre_campo'] . "\" " . $datos['adicionales'] . ">";

        if ($datos['pordefecto']) {
            //Valor por defecto
            $select .= "<option value='" . $datos['valor_defecto'] . "' align=center>" . $inicial . "</option>";
        }

        //Calcula el tamaÃƒÆ’Ã‚Â±o de resultado para llenar el select en el for.
        $numero = sizeof($resultado);

        if (is_array($resultado) && $numero > 0) {

            foreach ($resultado as $key => $res) {
                $codigoFila = "$key";
                $mostrar = ( isset($datos['atributos']['mostrar']) ) ? $res[$datos['atributos']['mostrar']] : $res;

                $mostrarFila = substr($mostrar, 0, 45);

                //DM  - 2012-10-12
                //$mostrarFila = htmlentities($mostrarFila,ENT_QUOTES);
                if ($datos['mayusculas']) {
                    $mostrarFila = strtoupper($mostrarFila);
                } else {
                    if ($datos['minusculas']) {
                        $mostrarFila = strtolower($mostrarFila);
                    } else if ($datos['primera_mayus']) {
                        //MAOH - 09 Feb 2012 - Se agrega la validacion para que solo coloque la primera letra mayuscula
                        $mostrarFila = strtolower($mostrarFila);
                        $mostrarFila = ucfirst($mostrarFila);
                    } else if ($datos['normal']) {
                        //MAOH - 09 Feb 2012 - Si se envia este parametro, que no haga nada
                    } else {
                        //$mostrarFila = strtolower($mostrarFila);
                        $mostrarFila = ucwords($mostrarFila);
                    }
                }

                $select .= "<option value='$codigoFila'";
                if ($datos['valor'] == $codigoFila) {
                    $select .= " selected ";
                }
                $select .= ">$mostrarFila</option>";
            } // Fin foreach($resultado as $res)
        } // Fin if ($resultado != -4)

        $select .= "</select>";

        return $select;
    }

//Fin de crearSelectFormaDesdeArray ($datos)


// Fin crearSelectHotelesGestionUsuarios()
    //MAOH - 12 Abr 2012 - Funcion que devuelve el microtime actual
    function obtenerMicrotime() {
        $mtime = microtime();
        $mtime = explode(" ", $mtime);
        $mtime = $mtime[1] + $mtime[0];

        return $mtime;
    }

//Fin de obtenerMicrotime()
    //MAOH - 12 Abr 2012 - Fin
    //MAOH - 27 Abr 2012 - Funcion que devuelve los dias del mes y aÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â±o indicados, por defecto toma el mes y/o aÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â±o actual
    function obtenerDiasMes($mes = 0, $anho = 0) {
        //Si el mes es menos a 1 o mayor a 12, se toma el mes actual
        if ($mes < 1 || $mes > 12 || !is_numeric($mes))
            $mes = date("m");

        if ($anho < 1 || !is_numeric($anho))
            $anho = date("Y");

        //Verifico si el aÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â±o es bisiesto
        if (((fmod($anho, 4) == 0) && (fmod($anho, 100) != 0)) || (fmod($anho, 400) == 0))
            $dias_febrero = 29;
        else
            $dias_febrero = 28;

        //Cantidad de dias por mes
        $dias_mes = array(0, 31, $dias_febrero, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

        return $dias_mes[(int) $mes];
    }


// Fin crearSelectHotelesEvolucion($datos, $cadena, $cad_id)
    //MAOH - 02 Mayo 2012 - Fin
    //MAOH - 07 Mayo 2012 - Funcion que codifica todo tipo de caracteres de utf8 a entidades html
    function UTF8toEntities($string) {
        /* 	Nota: Aplicar htmlspecialchars si se desea, antes de la aplicaciÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â³n de esta funciÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â³n
          SÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â³lo se usa la conversion lenta si hay caracteres de 8 bits
          Evitar el uso de 0xA0 (\240), en rangos de ereg */
        //if(!ereg("[\200-\237]", $string) && !ereg("[\241-\377]", $string))
        if (!preg_match("/[\200-\237]/", $string) && !preg_match("/[\241-\377]/", $string))
            return $string;

        //Rechaza las secuencias demasiado cortas
        $string = preg_replace("/[\302-\375]([\001-\177])/", "&#65533;\\1", $string);
        $string = preg_replace("/[\340-\375].([\001-\177])/", "&#65533;\\1", $string);
        $string = preg_replace("/[\360-\375]..([\001-\177])/", "&#65533;\\1", $string);
        $string = preg_replace("/[\370-\375]...([\001-\177])/", "&#65533;\\1", $string);
        $string = preg_replace("/[\374-\375]....([\001-\177])/", "&#65533;\\1", $string);

        //Rechaza los bytes y secuencias ilegales
        //Caracteres de 2 bytes en rango ASCII
        $string = preg_replace("/[\300-\301]./", "&#65533;", $string);
        //Codigo ilegal de 4 bytes (RFC 3629)
        $string = preg_replace("/\364[\220-\277]../", "&#65533;", $string);
        //Codigo ilegal de 4 bytes (RFC 3629)
        $string = preg_replace("/[\365-\367].../", "&#65533;", $string);
        //Codigo ilegal de 5 bytes (RFC 3629)
        $string = preg_replace("/[\370-\373]..../", "&#65533;", $string);
        //Codigo ilegal de6 bytes (RFC 3629)
        $string = preg_replace("/[\374-\375]...../", "&#65533;", $string);
        //Bytes indefinidos
        $string = preg_replace("/[\376-\377]/", "&#65533;", $string);

        //Rechaza bytes de inicio consecutivos
        $string = preg_replace("/[\302-\364]{2,}/", "&#65533;", $string);

        //Decodifica caracteres unicode de 4 bytes
        $string = preg_replace("/([\360-\364])([\200-\277])([\200-\277])([\200-\277])/e", "'&#'.((ord('\\1')&7)<<18 | (ord('\\2')&63)<<12 | (ord('\\3')&63)<<6 | (ord('\\4')&63)).';'", $string);

        //Decodifica caracteres unicode de 3 bytes
        $string = preg_replace("/([\340-\357])([\200-\277])([\200-\277])/e", "'&#'.((ord('\\1')&15)<<12 | (ord('\\2')&63)<<6 | (ord('\\3')&63)).';'", $string);

        //Decodifica caracteres unicode de 2 bytes
        $string = preg_replace("/([\300-\337])([\200-\277])/e", "'&#'.((ord('\\1')&31)<<6 | (ord('\\2')&63)).';'", $string);

        //Rechaza los bytes faltantes
        $string = preg_replace("/[\200-\277]/", "&#65533;", $string);

        return $string;
    }

//Fin de UTF8toEntities($string)
    //MAOH - 07 Mayo 2012 - Fin
    //MAOH - 03 Jul 2012 - Funcion que cambia los puntos por comas para exportar en excel
    static function cambiarFormatoValoresAExcel($valor) {
        $valor_tmp = $valor;

        //Si es un array
        if (is_array($valor)) {
            foreach ($valor as $k => $v) {
                if (is_array($v)) //Si es un array, se llama de forma recursiva
                    $valor_tmp[$k] = Herramientas::cambiarFormatoValoresAExcel($v);
                else
                    $valor_tmp[$k] = str_replace(".", ",", $v);
            }
        }
        else {
            $valor_tmp = str_replace(".", ",", $valor);
        }

        return $valor_tmp;
    }

//Function cambiarFormatoValoresAExcel($valor)
    //MAOH - 03 Jul 2012 - Fin

    /**
     * Funcion que se encarga de eliminar las tildes de una cadena
     * MD - 01-08-2012
     * * */
    static function eliminarTildes($cadena) {

        $cadena = str_replace('ÃƒÆ’Ã‚Â¡', 'a', $cadena);
        $cadena = str_replace('ÃƒÆ’Ã‚Â©', 'e', $cadena);
        $cadena = str_replace('ÃƒÆ’Ã‚Â­', 'i', $cadena);
        $cadena = str_replace('ÃƒÆ’Ã‚Â³', 'o', $cadena);
        $cadena = str_replace('ÃƒÆ’Ã‚Âº', 'u', $cadena);
        $cadena = str_replace('ÃƒÆ’Ã‚Â±', 'n', $cadena);

        return $cadena;
    }

//Fin de eliminarTildes()

    static function quitarTildes($cadena) {

        $no_permitidas = array("ÃƒÆ’Ã‚Â¡", "ÃƒÆ’Ã‚Â©", "ÃƒÆ’Ã‚Â­", "ÃƒÆ’Ã‚Â³", "ÃƒÆ’Ã‚Âº", "ÃƒÆ’Ã¯Â¿Â½", "ÃƒÆ’Ã¢â‚¬Â°", "ÃƒÆ’Ã¯Â¿Â½", "ÃƒÆ’Ã¢â‚¬Å“", "ÃƒÆ’Ã…Â¡", "ÃƒÆ’Ã‚Â±", "ÃƒÆ’Ã¢â€šÂ¬", "ÃƒÆ’Ã†â€™", "ÃƒÆ’Ã…â€™", "ÃƒÆ’Ã¢â‚¬â„¢", "ÃƒÆ’Ã¢â€žÂ¢", "ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢", "ÃƒÆ’Ã†â€™ ", "ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¨", "ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¬", "ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â²", "ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¹", "ÃƒÆ’Ã‚Â§", "ÃƒÆ’Ã¢â‚¬Â¡", "ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢", "ÃƒÆ’Ã‚Âª", "ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â®", "ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â´", "ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â»", "ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡", "ÃƒÆ’Ã†â€™Ãƒâ€¦Ã‚Â ", "ÃƒÆ’Ã†â€™Ãƒâ€¦Ã‚Â½", "ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã¯Â¿Â½", "ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Âº", "ÃƒÆ’Ã‚Â¼", "ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¶", "ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã¢â‚¬Å“", "ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¯", "ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¤", "Ãƒâ€šÃ‚Â«", "ÃƒÆ’Ã¢â‚¬â„¢", "ÃƒÆ’Ã†â€™Ãƒâ€šÃ¯Â¿Â½", "ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾", "ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¹");
        $permitidas = array("a", "e", "i", "o", "u", "A", "E", "I", "O", "U", "n", "N", "A", "E", "I", "O", "U", "a", "e", "i", "o", "u", "c", "C", "a", "e", "i", "o", "u", "A", "E", "I", "O", "U", "u", "o", "O", "i", "a", "e", "U", "I", "A", "E");
        $texto = str_replace($no_permitidas, $permitidas, $cadena);

        return $texto;
    }

//function quitarTildes($cadena)

    /**
     * Funcion que permite identificar la codificaciÃƒÆ’Ã‚Â³n de una cadena,
     * los valores posibles a retornar son ISO_8859_1, ASCII,  UTF_8
     * Tomado de : http://www.forosdelweb.com/f18/como-detectar-codificacion-string-448344/
     * DM - 2012-10-10
     * */
    function detectarCodificacion($texto) {
        $c = 0;
        $ascii = true;
        for ($i = 0; $i < strlen($texto); $i++) {
            $byte = ord($texto[$i]);
            if ($c > 0) {
                if (($byte >> 6) != 0x2) {
                    return "ISO_8859_1";
                } else {
                    $c--;
                }
            } elseif ($byte & 0x80) {
                $ascii = false;
                if (($byte >> 5) == 0x6) {
                    $c = 1;
                } elseif (($byte >> 4) == 0xE) {
                    $c = 2;
                } elseif (($byte >> 3) == 0x14) {
                    $c = 3;
                } else {
                    return "ISO_8859_1";
                }
            }//Fin de elseif ($byte&0x80)
        }//Fin de for ($i = 0;$i<strlen($texto);$i++)

        return ($ascii) ? "ASCII" : "UTF_8";
    }

//Fin de detectarCodificacion

    /**
     * Funcion que se encarga de verificar si el correo tiene mas correos
     * separadas por (;) o (,), de ser asi, solo deja el primero y
     * no tiene en cuenta los demas
     * DM - 2012-11-01
     * */
    function depurarCorreo($correo) {
        //Cambia los ; por , para procesar solo los registros con ,
        $correo = str_replace(";", ",", $correo);
        $correo = str_replace("\\'", "", $correo);
        $correo = str_replace("'", "", $correo);
        $correo = str_replace("\\\"", "", $correo);
        $correo = str_replace("\"", "", $correo);
        $correo = str_replace("\n", "", $correo);
        $correo = str_replace("\r", "", $correo);

        //obtiene todos los correos que esten en la cadena separados por (,)
        $lista_correos = preg_split("/,/", $correo);

        foreach ($lista_correos as $valor_correo) {
            //retira los espacios en blanco y verifica que tenga algun valor
            $valor_correo = trim($valor_correo);
            if ($valor_correo != "") {
                return $valor_correo;
            }//Fin de if( $valor_correo != "")
        }//Fin de foreach($lista_correos as $valor_correo)
        //solo retorna el primer correo
        return "";
    }

//Fin de depurarCorreo()

    /**
     * Funcion que se encarga de identifica el navegador del usuario
     * DM - 2013-03-07
     * * */
    function obtenerNavegador($user_agent) {
        $navegadores = array(
            'Opera' => 'Opera',
            'Mozilla Firefox' => '(Firebird)|(Firefox)',
            'Galeon' => 'Galeon',
            'Mozilla' => 'Gecko',
            'MyIE' => 'MyIE',
            'Lynx' => 'Lynx',
            'Netscape' => '(Mozilla\/4\.75)|(Netscape6)|(Mozilla\/4\.08)|(Mozilla\/4\.5)|(Mozilla\/4\.6)|(Mozilla\/4\.79)',
            'Konqueror' => 'Konqueror',
            'MSIE 9' => '(MSIE 9\.[0-9]+)',
            'MSIE 8' => '(MSIE 8\.[0-9]+)',
            'MSIE 7' => '(MSIE 7\.[0-9]+)',
            'MSIE 6' => '(MSIE 6\.[0-9]+)',
            'MSIE 5' => '(MSIE 5\.[0-9]+)',
            'MSIE 4' => '(MSIE 4\.[0-9]+)',
        );

        foreach ($navegadores as $navegador => $pattern) {
            $coindicencias = array();
            if (preg_match('/' . $pattern . '/', $user_agent, $coindicencias))
                return $navegador;
        }//Fin de foreach($navegadores as $navegador=> $pattern)

        return 'Desconocido';
    }

//Fin de obtenerNavegador

    /**
     * Funcion que se encarga de organizar los filtros de id en
     * rangos, de tal manera que se pueda minimizar la cantidad de ids
     * que se pasan en un filtro
     * DM - 2013-03-01
     * * */
    static function obtenerFiltroPorRangos($datos) {
        //Si no hay un grupo de ids entonces no aplica el filtro
        if (!( is_array($datos['lista_ids']) && sizeof($datos['lista_ids']) > 0 )) {
            return "";
        }//Fin de if( !( is_array($datos['lista_ids']) && sizeof($datos['lista_ids']) > 0 ) )
        //Obtiene los id de pregunta_establecimiento y los ordena
        //para generar los filtros en rangos y no tener que
        //enviar todos los ids
        $lista_ids = $datos['lista_ids'];
        sort($lista_ids);
        $tamano = sizeof($lista_ids);

        //lista con los rangos obtenidos
        $rangos = array();
        //lista de valores que no tienen rangos
        $sin_rangos = array();
        //posicion para llevar la cantidad de rangos encontrados
        $pos_rango = 0;
        //indica si se encontro un rango y se tiene en cuenta en el proximo ciclo
        //para saber si se puede formar un unico rango o se debe crear otro
        $esta_en_rango = false;

        //Recorre los elementos de la lista por cada 3 posiciones para
        //saber si encuentra un rango de al menos 3 elementos
        for ($i = 0; $i < $tamano; $i+=2) {
            $posicion_1 = $i + 1;

            $valor = $lista_ids[$i];
            $valor_1 = $valor + 1;

            if (isset($lista_ids[$posicion_1])) {
                if ($esta_en_rango) {

                    //verifica que el ultimo valor del rango
                    //sea anterior al valor del rango que llega
                    $estan_en_mismo_rango = $rangos[$pos_rango - 1]['fin'] + 1 == $valor;

                    if ($estan_en_mismo_rango && $valor_1 == $lista_ids[$posicion_1]) {
                        //si esta en un rango  entonces utiliza
                        //el rango anterior y re asigna el valor final del rango
                        $rangos[$pos_rango - 1]['fin'] = $valor_1;
                        $rangos[$pos_rango - 1]['cantidad'] ++;
                        continue;
                    } else if ($valor_1 == $lista_ids[$posicion_1]) {
                        $rangos[$pos_rango] = array();
                        $rangos[$pos_rango]['ini'] = $valor;
                        $rangos[$pos_rango]['fin'] = $valor_1;
                        $rangos[$pos_rango]['cantidad'] = 2;
                        $pos_rango++;
                        continue;
                    } else if ($valor == $rangos[$pos_rango - 1]['fin'] + 1) {
                        //Si el valor forma parte del rango anterior
                        $rangos[$pos_rango - 1]['fin'] = $valor;
                        $rangos[$pos_rango - 1]['cantidad'] ++;
                        //Reduce el contador para que inicie del siguiente valor
                        //para revisar si forma parte de algun rango
                        $i--;
                        $esta_en_rango = false;
                        continue;
                    }//Fin de else if( $valor == $rangos[$pos_rango-1]['fin'] + 1)
                    else {
                        $esta_en_rango = false;
                        $sin_rangos[] = $valor;
                        //Reduce el contador para que inicie del siguiente valor
                        //para revisar si forma parte de algun rango
                        $i--;
                        continue;
                    }
                }//Fin de if( $esta_en_rango )
                else {
                    if ($valor_1 == $lista_ids[$posicion_1]) {
                        $rangos[$pos_rango] = array();
                        $rangos[$pos_rango]['ini'] = $valor;
                        $rangos[$pos_rango]['fin'] = $valor_1;
                        $rangos[$pos_rango]['cantidad'] = 2;
                        $esta_en_rango = true;
                        $pos_rango++;
                        continue;
                    } else {
                        $esta_en_rango = false;
                        $sin_rangos[] = $valor;
                        //Reduce el contador para que inicie del siguiente valor
                        //para revisar si forma parte de algun rango
                        $i--;
                        continue;
                    }
                }//Fin de else de if( $esta_en_rango )
            }//Fin de else if( isset($pee_ids[ $posicion_1 ]) )
            else {
                $esta_en_rango = false;
                $sin_rangos[] = $valor;
            }
        }//Fin de for( $i=0; $i < $tamano; $i+=2)


        $condiciones = array();
        $nombre_campo = $datos['nombre_campo'];

        if (sizeof($rangos) > 0) {
            foreach ($rangos as $valor_rango) {
                if ($valor_rango['cantidad'] > 2) {
                    $condiciones[] = $nombre_campo . " BETWEEN " . $valor_rango['ini'] . " AND " . $valor_rango['fin'] . " ";
                }//Fin de if( $valor_rango['cantidad'] > 2)
                else {
                    $sin_rangos[] = $valor_rango['ini'];
                    $sin_rangos[] = $valor_rango['fin'];
                }
            }//Fin de foreach($rangos as $valor_rango)
        }//Fin de if( sizeof($rangos) > 0 )

        if (sizeof($sin_rangos) > 0) {
            $condiciones[] = $nombre_campo . " IN (" . implode(",", $sin_rangos) . ") ";
        }//Fin de if( sizeof($sin_rangos) > 0 )

        return "(" . implode(" OR ", $condiciones) . ")";
    }

//Fin de obtenerFiltroPorRangos

    /**
     * FunciÃƒÆ’Ã‚Â³n que se encarga de aplicar trim a todos los campos que
     * llegan en el arrglo siempre y cuando no sea un arreglo el valor,
     * si es un arreglo hace un llamado recursivo
     * DM - 2013-09-23
     * * */
    static function trimCamposFormulario($datos) {

        //DM - 2017-12-06
        //Este mÃƒÆ’Ã‚Â©todo no lleva registro de log
        //$datos['log_nombre_proceso_actual'] = "trimCamposFormulario";
        //$datos['log_termina_proceso'] = 0;
        //$datos = logProceso($datos);
        //$datos_log = $datos;

        foreach ($datos as $key => $valor) {
            //si el campo no es un arreglo le aplica el trim
            if (!is_array($valor)) {
                $datos[$key] = trim($valor);
            } else {
                $valor = Herramientas::trimCamposFormulario($valor);
                $datos[$key] = $valor;
            }
        }

        //$datos_log['log_termina_proceso'] = 1;
        //$datos_log = logProceso($datos_log);

        return $datos;
    }//Fin de trimCamposFormulario

    /**
     * FunciÃƒÆ’Ã‚Â³n que se encarga de crear un campo radio
     * con las opciones de SI y NO, de acuerdo al
     * idioma de la interfaz
     * DM - 2013-11-15
     * */
    static function crearCampoSiNo($datos) {
        global $idi_despliegue;

        $checked = $datos['valor'] == '0' ? 'checked="checked"' : '';
        $contenido = $idi_despliegue['no'] . '<input ' . $datos['atributos'] . ' ' . $checked . ' type="radio" name="' . $datos['nombre_campo'] . '" value="0" />';

        $contenido .= '&nbsp;&nbsp;&nbsp;&nbsp;';

        $checked = $datos['valor'] == 1 ? 'checked="checked"' : '';
        $contenido .= $idi_despliegue['si'] . '<input ' . $datos['atributos'] . ' ' . $checked . ' type="radio" name="' . $datos['nombre_campo'] . '" value="1" />';

        return $contenido;
    }

//Fin de crearCampoSiNo

    /**
     * Se encarga de retornar la IP de conexion del usuario
     * DM - 2014-03-28
     * */
    static function obtenerIPConexion() {
        $ip = "No detectada";

        if (isset($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        } else if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_VIA'])) {
            $ip = $_SERVER['HTTP_VIA'];
        }

        return $ip;
    }

//Fin de obtenerIPConexion()

    /**
     * Se encarga de ordenar un arreglo asociativo por el campo que se pase
     * como parÃƒÆ’Ã‚Â¡metro
     * DM - 2014-09-25
     * * */
    static function ordenarArregloPorCampo($arreglo, $campo) {
        $listado_temporal = array();
        foreach ($arreglo as $key => $val) {
            //obtiene el campo por el cual se va a ordenar
            //en caso de ser texto se asigna como mayusculas
            //porque php no ordena bien si esta en minuscula con la funcion asort
            $listado_temporal[$key] = strtoupper($val[$campo]);
        }//Fin de foreach($arreglo as $key => $val)
        //ordena el listado
        asort($listado_temporal);

        //Asigna el nuevo orden de acuerdo al ordenamiento
        $nuevo_arreglo = array();
        foreach ($listado_temporal as $key => $tmp) {
            $nuevo_arreglo[$key] = $arreglo[$key];
        }//Fin de foreach($listado_temporal as $key => $tmp)

        return $nuevo_arreglo;
    }

//Fin de ordenarArregloPorCampo()

    /**
     * funcion para reemplazar caracteres html de la forma &atilde; especificamente para la generacion de xml
     * AS 2015-01-16
     * */
    function reemplazarHtmlCaracteres($string) {
        $busqueda = array('&nbsp;', '&iexcl;', '&cent;', '&pound;', '&curren;', '&yen;', '&brvbar;', '&sect;', '&uml;', '&copy;', '&ordf;', '&laquo;', '&not;[*]', '&reg;', '&macr;', '&deg;', '&plusmn;', '&sup2;', '&sup3;', '&acute;', '&micro;', '&para;', '&middot;', '&cedil;', '&sup1;', '&ordm;', '&raquo;', '&frac14;', '&frac12;', '&frac34;', '&iquest;', '&Agrave;', '&Aacute;', '&Acirc;', '&Atilde;', '&Auml;', '&Aring;', '&AElig;', '&Ccedil;', '&Egrave;', '&Eacute;', '&Ecirc;', '&Euml;', '&Igrave;', '&Iacute;', '&Icirc;', '&Iuml;', '&ETH;', '&Ntilde;', '&Ograve;', '&Oacute;', '&Ocirc;', '&Otilde;', '&Ouml;', '&times;', '&Oslash;', '&Ugrave;', '&Uacute;', '&Ucirc;', '&Uuml;', '&Yacute;', '&THORN;', '&szlig;', '&agrave;', '&aacute;', '&acirc;', '&atilde;', '&auml;', '&aring;', '&aelig;', '&ccedil;', '&egrave;', '&eacute;', '&ecirc;', '&euml;', '&igrave;', '&iacute;', '&icirc;', '&iuml;', '&eth;', '&ntilde;', '&ograve;', '&oacute;', '&ocirc;', '&otilde;', '&ouml;', '&divide;', '&oslash;', '&ugrave;', '&uacute;', '&ucirc;', '&uuml;', '&yacute;', '&thorn;', '&yuml;', '&lt;', '&gt;', '&amp;');
        $reemplazar = array(
            ' ',
            'Ãƒâ€šÃ‚Â¡',
            'Ãƒâ€šÃ‚Â¢',
            'Ãƒâ€šÃ‚Â£',
            'Ãƒâ€šÃ‚Â¤',
            'Ãƒâ€šÃ‚Â¥',
            'Ãƒâ€šÃ‚Â¦',
            'Ãƒâ€šÃ‚Â§',
            'Ãƒâ€šÃ‚Â¨',
            'Ãƒâ€šÃ‚Â©',
            'Ãƒâ€šÃ‚Âª',
            'Ãƒâ€šÃ‚Â«',
            'Ãƒâ€šÃ‚Â¬',
            'Ãƒâ€šÃ‚Â®',
            'Ãƒâ€šÃ‚Â¯',
            'Ãƒâ€šÃ‚Â°',
            'Ãƒâ€šÃ‚Â±',
            'Ãƒâ€šÃ‚Â²',
            'Ãƒâ€šÃ‚Â³',
            'Ãƒâ€šÃ‚Â´',
            'Ãƒâ€šÃ‚Âµ',
            'Ãƒâ€šÃ‚Â¶',
            'Ãƒâ€šÃ‚Â·',
            'Ãƒâ€šÃ‚Â¸',
            'Ãƒâ€šÃ‚Â¹',
            'Ãƒâ€šÃ‚Âº',
            'Ãƒâ€šÃ‚Â»',
            'Ãƒâ€šÃ‚Â¼',
            'Ãƒâ€šÃ‚Â½',
            'Ãƒâ€šÃ‚Â¾',
            'Ãƒâ€šÃ‚Â¿',
            'ÃƒÆ’Ã¢â€šÂ¬',
            'ÃƒÆ’Ã¯Â¿Â½',
            'ÃƒÆ’Ã¢â‚¬Å¡',
            'ÃƒÆ’Ã†â€™',
            'ÃƒÆ’Ã¢â‚¬Å¾',
            'ÃƒÆ’Ã¢â‚¬Â¦',
            'ÃƒÆ’Ã¢â‚¬Â ',
            'ÃƒÆ’Ã¢â‚¬Â¡',
            'ÃƒÆ’Ã‹â€ ',
            'ÃƒÆ’Ã¢â‚¬Â°',
            'ÃƒÆ’Ã…Â ',
            'ÃƒÆ’Ã¢â‚¬Â¹',
            'ÃƒÆ’Ã…â€™',
            'ÃƒÆ’Ã¯Â¿Â½',
            'ÃƒÆ’Ã…Â½',
            'ÃƒÆ’Ã¯Â¿Â½',
            'ÃƒÆ’Ã¯Â¿Â½',
            'ÃƒÆ’Ã¢â‚¬Ëœ',
            'ÃƒÆ’Ã¢â‚¬â„¢',
            'ÃƒÆ’Ã¢â‚¬Å“',
            'ÃƒÆ’Ã¢â‚¬ï¿½',
            'ÃƒÆ’Ã¢â‚¬Â¢',
            'ÃƒÆ’Ã¢â‚¬â€œ',
            'ÃƒÆ’Ã¢â‚¬â€�',
            'ÃƒÆ’Ã‹Å“',
            'ÃƒÆ’Ã¢â€žÂ¢',
            'ÃƒÆ’Ã…Â¡',
            'ÃƒÆ’Ã¢â‚¬Âº',
            'ÃƒÆ’Ã…â€œ',
            'ÃƒÆ’Ã¯Â¿Â½',
            'ÃƒÆ’Ã…Â¾',
            'ÃƒÆ’Ã…Â¸',
            'ÃƒÆ’Ã‚Â ',
            'ÃƒÆ’Ã‚Â¡',
            'ÃƒÆ’Ã‚Â¢',
            'ÃƒÆ’Ã‚Â£',
            'ÃƒÆ’Ã‚Â¤',
            'ÃƒÆ’Ã‚Â¥',
            'ÃƒÆ’Ã‚Â¦',
            'ÃƒÆ’Ã‚Â§',
            'ÃƒÆ’Ã‚Â¨',
            'ÃƒÆ’Ã‚Â©',
            'ÃƒÆ’Ã‚Âª',
            'ÃƒÆ’Ã‚Â«',
            'ÃƒÆ’Ã‚Â¬',
            'ÃƒÆ’Ã‚Â­',
            'ÃƒÆ’Ã‚Â®',
            'ÃƒÆ’Ã‚Â¯',
            'ÃƒÆ’Ã‚Â°',
            'ÃƒÆ’Ã‚Â±',
            'ÃƒÆ’Ã‚Â²',
            'ÃƒÆ’Ã‚Â³',
            'ÃƒÆ’Ã‚Â´',
            'ÃƒÆ’Ã‚Âµ',
            'ÃƒÆ’Ã‚Â¶',
            'ÃƒÆ’Ã‚Â·',
            'ÃƒÆ’Ã‚Â¸',
            'ÃƒÆ’Ã‚Â¹',
            'ÃƒÆ’Ã‚Âº',
            'ÃƒÆ’Ã‚Â»',
            'ÃƒÆ’Ã‚Â¼',
            'ÃƒÆ’Ã‚Â½',
            'ÃƒÆ’Ã‚Â¾',
            'ÃƒÆ’Ã‚Â¿',
            '<',
            '>',
            '&'
        );
        $variable = str_replace($busqueda, $reemplazar, $string);
        return $variable;
    }

    /**
     * Se encarga de codificar una cadena en base 64 para que sea interpretada por la
     * aplicaciÃƒÆ’Ã‚Â³n
     * DM - 2015-06-12
     * */
    static function codificarBase64($cadena) {
        //agrega un codigo para saber que la cadena esta en base 64
        return base64_encode($cadena) . '_b6A';
    }

//Fin de codificarBase64

    /**
     * Se encarga de revisar que campos estan en base 64 y volverlos a su valor
     * normal
     * DM - 2015-06-11
     * */
    static function decodificarBase64CamposTextoFormulario($datos) {

        //si no es un arreglo revisa si es una cadena que debe decodificar
        if (!is_array($datos)) {
            //Si es un nÃƒÆ’Ã‚Âºmero no hace nada
            if (is_numeric($datos)) {
                return $datos;
            }//Fin de if( is_numeric($datos) )
            //Verifica si es una cadena en base 64
            //verifica si tiene _b6A al final
            if (substr($datos, strlen($datos) - 4) !== '_b6A') {
                return $datos;
            }

            $datos = substr($datos, 0, strlen($datos) - 4);
            return base64_decode($datos);
        }//Fin de if( !is_array($datos) )

        foreach ($datos as $clave => $valor) {
            if (is_numeric($valor)) {
                continue;
            }

            if (is_array($valor)) {
                $valor = Herramientas::decodificarBase64CamposTextoFormulario($valor);
                $datos[$clave] = $valor;
                continue;
            }
            $valor = trim($valor);
            //Verifica si es una cadena en base 64 verificando
            //si tiene _b6A al final
            if (substr($valor, strlen($valor) - 4) !== "_b6A") {
                continue;
            }

            $valor = substr($valor, 0, strlen($valor) - 4);

            $valor = base64_decode($valor);

            $convertir_ascii = mb_detect_encoding($valor) === false;
            $convertir_ascii |= mb_detect_encoding($valor) == 'UTF-8';
            $convertir_ascii &=!isset($datos['omitir_conversion_ascii']);
            if ($convertir_ascii) {
                $valor = mb_convert_encoding($valor, 'UTF-8', 'ASCII');
            }
            $datos[$clave] = $valor;
        }//Fin de foreach($datos as $clave => $valor)

        return $datos;
    }

//Fin de decodificarBase64CamposTextoFormulario

    /*
     * AS 2015-01-21
     * funcion para verificar si existe un caracter especial al final de la cadena si fuera asi reotrnaria uno con el fin de
     * cuando se recorte una cadena no genere codigo que no se puede interpretar por el navegador
     */

    function recortarCadena($cadena, $cantidad) {

        if (preg_match("/(ÃƒÆ’Ã‚Â¡|ÃƒÆ’Ã‚Â©|ÃƒÆ’Ã‚Â­|ÃƒÆ’Ã‚Â³|ÃƒÆ’Ã‚Âº|ÃƒÆ’Ã‚Â±+)/", $cadena)) {
            return substr($cadena, 0, ($cantidad - 1)) . " ...";
        }
        return substr($cadena, 0, ($cantidad - 1)) . " ...";
    }

    /**
     * Se encarga de convertir una fecha de acuerdo a la zona
     * horaria que se pasa por parametro
     * DM - 2015-03-26
     * */
    function convertirFechaZonaHoraria($fecha, $horas_zona_horaria, $opciones = array()) {
        //Obtiene la diferencia horaria de la fecha de respuesta a partir de la hora cero
        $diferencia_hora = date('P', strtotime($fecha));
        $diferencia_hora_partes = preg_split('/:/', $diferencia_hora);

        //Cantidad de horas a partir de la hora cero GMT
        $diferencia_hora = (int) $diferencia_hora_partes[0];

        //Obtiene el time de la fecha
        $time_respuesta = strtotime($fecha);

        //Nivela la hora de respuesta a la hora cero de acuerdo a la diferencia horaria con la que se guardo
        if ($diferencia_hora < 0) {
            //Si el valor es negativo debe sumarlo para dejarlo en la hora cero
            $time_respuesta = strtotime('+' . abs($diferencia_hora) . ' hour', $time_respuesta);
        }//Fin de if( $diferencia_hora < 0 )
        else {
            //Si el valor es positivo debe restarlo para dejarlo en la hora cero
            $time_respuesta = strtotime('-' . abs($diferencia_hora) . ' hour', $time_respuesta);
        }

        //Obtiene la zona horaria configurada
        //Carga la cantidad de horas a partir de la hora cero GMT
        $zona_horaria_partes = preg_split('/\./', $horas_zona_horaria);

        //Obtiene las horas y minutos que debe sumar o restar

        $zona_singo = '+';
        $zona_horas = (int) $zona_horaria_partes[0];
        //Si la hora es negativa debe restar horas y minutos
        if ($zona_horas < 0) {
            $zona_singo = '-';
        }
        $zona_horas = abs($zona_horas);
        //Los minutos estan en un rango de 0-10, debe convertirlo a minutos de 0-60
        $zona_minutos = ((int) $zona_horaria_partes[1]) * 6;

        //A partir de esta zona horaria calcula la suma o resta de horas y minutos
        $time_respuesta_zona = strtotime($zona_singo . $zona_horas . ' hour', $time_respuesta);

        $time_respuesta_zona = strtotime($zona_singo . $zona_minutos . ' minute', $time_respuesta_zona);

        //agrega ceros iniciales en caso de que los valores sean menos que 10
        $zona_horas = $zona_horas < 10 ? '0' . $zona_horas : $zona_horas;
        $zona_minutos = $zona_minutos < 10 ? '0' . $zona_minutos : $zona_minutos;

        $formato_hora = " (h:i A)";
        if ($opciones['ocultar_formato_horas'] == 1) {
            $formato_hora = "";
        }

        if ($opciones['ocultar_diferencia_horas'] == 1) {
            return date('d/m/Y' . $formato_hora, $time_respuesta_zona);
        }
        return date('d/m/Y' . $formato_hora, $time_respuesta_zona) . ' ' . $zona_singo . $zona_horas . ':' . $zona_minutos;
    }

//Fin de convertirFechaZonaHoraria

    /*
     * Utilidad: Limpia las comillas simples presentes en el texto para
     * no tener conflictos con la BD
     * Autor: AMP
     * Fecha: 10/6/16
     *
     */
    static function limpiarCamposParaBD($cadena) {
        $a = str_replace("\\'", "'", $cadena);
        $b = str_replace("\'", "'", $a);
        return str_replace("'", "\'", $b);
    }

}//class
?>
