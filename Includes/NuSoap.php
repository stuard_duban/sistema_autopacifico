<?php

class NuSoap
{

/*	******************************************	CONSTRUCTOR Nusoap	*******************************************	*/

	function __construct()
	{
		require_once "nusoap/lib/nusoap.php";
	}


	/** crearTransaccionACH
	 * parametro: $param
	 * autor : Stuard Romero - DESARROLLO AUTOPACIFICO
	 * descripcion: Ejecuta el servicio de CREATETRANSACTIONPAYMENTHOSTING enviandole los parametros con los datos del cliente.
	 **/

	 static function crearTransaccionACH ($param) {
		require_once "nusoap/lib/nusoap.php";



		$_PATH_WEB = "http://172.16.2.33/pse/";
		header_remove('Connection');



		$cliente = new nusoap_client('https://200.1.124.118/PSEHostingWebServices/PSEHostingWS.asmx?wsdl','wsdl');

		//$cliente = new nusoap_client('https://200.1.124.65/PSEHostingWebServices/PSEHostingWS.asmx?wsdl',false);
		$cliente->useHTTPPersistentConnection();				//forzar http 1.1
		//$soapAction = "https://200.1.124.65/PSEHostingWebServices/PSEHostingWS.asmx?op=createTransactionPaymentHostinghttps://200.1.124.65/PSEHostingWebServices/PSEHostingWS.asmx?op=createTransactionPaymentHosting";
		$cliente->encode_utf8 = true;

		$namespaces = array(
		'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
		'xsd' => 'http://www.w3.org/2001/XMLSchema',
		'soap' => 'http://schemas.xmlsoap.org/soap/envelope/'
		);


		 //$cliente = new nusoap_client($url, true);
		$err = $cliente->getError();
		if($err){
			$res = "->".$err;
			return $res;
		}

		$param['entity_url'] = "http://pagos.autopacifico.com.co/pse";

		/*Cuerpo del XML*/
		$body = '<createTransactionPaymentHosting xmlns="http://www.achcolombia.com.co/PSEHostingWS"><ticketOfficeID>'.$param['ticketOfficeID'].'</ticketOfficeID>';
		$body .= '<amount>'.$param['amount'].'</amount><vatAmount>'.$param['vatAmount'].'</vatAmount><paymentID>'.$param['paymentID'].'</paymentID><paymentDescription>'.$param['paymentDescription'].'</paymentDescription><referenceNumber1>'.$param['referenceNumber1'].'</referenceNumber1><referenceNumber2>'.$param['referenceNumber2'].'</referenceNumber2><referenceNumber3>'.$param['referenceNumber3'].'</referenceNumber3>';
		$body .= '<serviceCode>'.$param['serviceCode'].'</serviceCode><email>'.$param['email'].'</email><fields><PSEHostingField><Name>'.$param['fields'][0]['Name'].'</Name><Value xsi:type="xsd:string">'.$param['fields'][0]['Value'].'</Value></PSEHostingField>';
		$body .= '<PSEHostingField><Name>'.$param['fields'][1]['Name'].'</Name><Value xsi:type="xsd:string">'.$param['fields'][1]['Value'].'</Value></PSEHostingField><PSEHostingField><Name>'.$param['fields'][2]['Name'].'</Name><Value xsi:type="xsd:string">'.$param['fields'][2]['Value'].'</Value></PSEHostingField></fields>';
		$body .= '<entity_url>'.$param['entity_url'].'</entity_url></createTransactionPaymentHosting>';


		$endpoint = "https://200.1.124.65/PSEHostingWebServices/PSEHostingWS.asmx?wsdl";
		$soapaction = "http://www.achcolombia.com.co/PSEHostingWS/createTransactionPaymentHosting";
		$msg = $cliente->serializeEnvelope($body);

		$res=$cliente->send($msg, $soapaction);


		$err = $cliente->getError();


		//Solicitud y respuesta
		/*echo '<h2>Request</h2>';
		echo '<pre>' . htmlspecialchars($cliente->request, ENT_QUOTES) . '</pre>';
		echo '<h2>Response</h2>';
		echo '<pre>' . htmlspecialchars($cliente->response, ENT_QUOTES) . '</pre>';

		//Display the debug messages
		echo '<h2>Debug</h2>';
		echo '<pre>' . htmlspecialchars($cliente->debug_str, ENT_QUOTES) . '</pre>';
		//exit;*/
		return $res;
 }

 /** obtenerInfoTransacciones
	* parametro: $paymentID
	* autor : Stuard Romero - DESARROLLO AUTOPACIFICO
	* descripcion: Ejecuta el servicio GetTransactionInformationHosting para actualizar el estado de las facturas.
	**/

	static function obtenerInfoTransacciones ($paymentID) {
		require_once "nusoap/lib/nusoap.php";

		$cliente = new nusoap_client('https://200.1.124.118/PSEHostingWebServices/PSEHostingWS.asmx?wsdl','wsdl');
		$cliente->useHTTPPersistentConnection();				//forzar http 1.1

		header_remove('Connection');

		$cliente->encode_utf8 = true;

		$namespaces = array(
		'soap' => 'http://schemas.xmlsoap.org/soap/envelope/',
		'xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
		'xsd' => 'http://www.w3.org/2001/XMLSchema'
		);

		$cliente->serializeEnvelope($namespaces);

		//$cliente = new nusoap_client($url, true);
		$err = $cliente->getError();
		if($err){
		 $res = "->".$err;
		 return $res;
		}

		$ticketOfficeID = 7110;

		$param=array('ticketOfficeID'=>(int)$ticketOfficeID,
			 'password' => '123',
			 'paymentID' => $paymentID
		);

		//Creacion del XML
		$body = '<getTransactionInformationHosting xmlns="http://www.achcolombia.com.co/PSEHostingWS"><ticketOfficeID>'.$param['ticketOfficeID'].'</ticketOfficeID>';
		$body .= '<password>'.$param['password'].'</password><paymentID>'.$param['paymentID'].'</paymentID>';
		$body .= '</getTransactionInformationHosting>';

		
		$endpoint = "https://200.1.124.65/PSEHostingWebServices/PSEHostingWS.asmx?wsdl";
		$soapaction = "http://www.achcolombia.com.co/PSEHostingWS/getTransactionInformationHosting";
		$msg = $cliente->serializeEnvelope($body);

		$res=$cliente->send($msg, $soapaction);



		$err = $cliente->getError();


		//Solicitud y respuesta
		/* echo '<h2>Request</h2>';
		echo '<pre>' . htmlspecialchars($cliente->request, ENT_QUOTES) . '</pre>';
		echo '<h2>Response</h2>';
		echo '<pre>' . htmlspecialchars($cliente->response, ENT_QUOTES) . '</pre>';

		// Display the debug messages
		echo '<h2>Debug</h2>';
		echo '<pre>' . htmlspecialchars($cliente->debug_str, ENT_QUOTES) . '</pre>';*/

		return $res;
	}


}
?>
