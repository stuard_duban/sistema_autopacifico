$(document).ready(function() {
    $(".factura").click(function(event) {
        var total = 0;
        $(".factura:checked").each(function() {
            total += parseFloat($(this).val());

        });


        if (total == 0) {
            $('#monto').val('');
            //$('#displayMonto').val('');
            $('#displayMonto').attr('placeholder', "$ 0" );
        } else {
            $('#displayMonto').attr("placeholder", "$ " + formatNumber(total));
            $('#monto').val(Math.floor(total * 100) / 100);

        }



        function formatNumber(num) {
          if (!num || num == 'NaN') return '-';
          if (num == 'Infinity') return '&#x221e;';
          num = num.toString().replace(/\$|\,/g, '');
          if (isNaN(num))
              num = "0";
          sign = (num == (num = Math.abs(num)));
          num = Math.floor(num * 100 + 0.50000000001);
          cents = num % 100;
          num = Math.floor(num / 100).toString();
          if (cents < 10)
              cents = "0" + cents;
          for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3) ; i++)
              num = num.substring(0, num.length - (4 * i + 3)) + '.' + num.substring(num.length - (4 * i + 3));
          return (((sign) ? '' : '-') + num + ',' + cents);
        }
    });
});

/*$(document).ready(function() {
    $("#concepto_pago").click(function(event) {
        //var total = 0;
        /*$(".factura:checked").each(function() {
            total += parseInt($(this).val());
        });
        var total = $('select[name=concepto_pago]').val();
        //alert(total);
        if (total == 0) {
            $('#montoConcepto').val('');
        } else {
            $('#montoConcepto').val(total);
        }
    });
});
*/

/*$(document).ready(function() {
  $("#concepto_pago").on('mousedown', function(event) {
    //alert('entro');
      var valor = $(this).val();
      alert(valor);
      var monto = valor.split('|');
      //alert(monto[1]);
      if (monto[1] == 0) {
          $('#montoConcepto').val('');
      } else {
          $('#montoConcepto').val(monto[1]);
      }
  });
});
*/

$( "#concepto_pago" )
  .change(function () {
    var valor = $( "#concepto_pago" ).val();
    var monto = valor.split('|');
    //alert(monto[1]);
    //$( "#montoConcepto" ).text( str );
    $( "#montoConcepto" ).val(monto[1]);
  })
  .change();

/*Reset Formulario*/
  $(function() {
    $('#btn-abono').click(function() {
      $('#pago-factura #monto').val('');

      $('#pago-factura #displayMonto').attr('placeholder', "$ 0" );

      $("#monto").removeAttr("required");

      /*Agregar required select*/
      $('#concepto_pago').attr('required', "true" );
      //$('#pago-concepto #montoConcepto').removeAttr('required');​​​​​

      $('#montoConcepto').attr("required", "true");
      $('#montoConcepto').attr("readonly", "true");

      $('input.boton-procesar').css('pointer-events', "auto");

      $('#formInfoPagosCedula').trigger("reset");
    });

    $('#btn-factura').click(function() {
      $('#pago-concepto #montoConcepto').val('');
      $("#montoConcepto").removeAttr("required");

      $('#monto').attr("required", "true");

      //Quitar required select concepto
      $('#concepto_pago').removeAttr("required");
      //quita required factura
      //jQuery("#monto").removeAttr("required");​​​​​jQuery("#montoConcepto").attr("required");
      $('#formInfoPagosCedula').trigger("reset");
    });

    $('.tab_sin_factura').click(function() {
      $('input.boton-procesar').css('pointer-events', "none");
    });
  });

  /*Deshabilitar input de pago si no ha sido seleccionado concepto*/
  /*$('#concepto_pago').on('change', function() {
    console.log('se toco el concepto');
    $("#montoConcepto").removeAttr('readonly');​​​​​
  });*/

  $('#concepto_pago').on('change', function(){

      console.log('se toco el concepto');
      $('#montoConcepto').removeAttr('readonly');
      console.log($(this).find(":selected").val());
      if($(this).find(":selected").val() == ""){
        $('#montoConcepto').attr("readonly", "true");
      }

  });



  function validarCheck(){
    //console.log('entro');
    var form = document.getElementById("formInfoPagosCedula");
    var active = $( "#btn-factura" ).hasClass( "active" );
  //  console.log(active);
    if($('.factura:checked').length == 0 && active) {
      alert('Debes seleccionar al menos una factura');
    }
    //active = false;
    /*else {
      form.submit();
    }*/
  }
