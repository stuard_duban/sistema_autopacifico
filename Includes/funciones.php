<?php
/**
 * Se encarga de realizar la división de 2 numeros y verificar
 * que no se haga una división por cero
 * DM - 2015-04-23
 **/
function division($valor_1,$valor_2,$decimales=1)
{
    if( !is_numeric($valor_1) )
    {
        return '-';
    }
    if( $valor_2 == 0 || $valor_2 == "")
    {
        return '-';
    }
    return number_format($valor_1 / $valor_2, $decimales);
}//Fin de division

function i($valor,$var_dump= false)
{
	echo '<pre>
	';
	if( $var_dump )
	{
		var_dump($valor);
	}
	else
	{
		print_r($valor);
	}
	echo '</pre>
	';
}

function trimCampos($datos)
{
	foreach($datos as $key => $valor)
	{
		//si el campo no es un arreglo le aplica el trim
		if( !is_array($valor) )
		{
			$datos[$key] = trim($valor);
		}
		else
		{
			$datos[$key] = trimCampos($valor);
		}
	}//Fin de foreach($datos as $key => $valor)
	return $datos;
}//Fin de trimCampos

function validarAcceso($perfiles_autorizados)
{
	//captura el perfil del usuario
	$perfil = $_SESSION['tipo_usuario'];

	if( sizeof($perfiles_autorizados)>0 )
	{
		//verifica si el perfil se encuentra en los autorizados
		$existe = array_search($perfil,$perfiles_autorizados);

		if( is_numeric($existe) && $_SESSION[autenticado]==1 )
		{
			//Asigna el perfil correspondiente
			$perfil = $_SESSION['tipo_usuario'];
		}
		else
		{
			//elimina el acceso de todos los perfiles
			$perfil = "-";
		}

	}//Fin de if( sizeof($perfiles_autorizados)>0 )

	return $perfil;
}//Fin de validarAcceso()

function tienePermiso($tipo_usuario, $acceso)
{
	if( $_SESSION['tipo_usuario'] == $tipo_usuario && !$acceso )
	{
		header('Location: index.php');
	}

}//Fin de tienePermiso



function noCache() {

  header("Expires: Tue, 01 Jul 2001 06:00:00 GMT");
  header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
  header("Cache-Control: no-store, no-cache, must-revalidate");
  header("Cache-Control: post-check=0, pre-check=0", false);
  header("Pragma: no-cache");
}

function exception_error_handler($errno, $errstr, $errfile, $errline )
{
	global $_EMAIL_ADMIN,$_host_db,$_user_db,$_password_db,$_db;

	switch($errno)
	{
		case 2:
		case 256:
		case 512:
		case 4096:
		$asunto = "Urgente - Error HQ";

		$errstr = str_replace("'","",$errstr);

		$cuerpo = 'Se genero un error en Hotels Quality :<br>
			Fecha: '.date("Y-m-d H:i:s").'<br>
			Codigo error: '.$errno.'<br>
			Archivo: '.$errfile.'<br>
			Linea: '.$errline.'<br>
			Error: '.$errstr.' <br>
			Navegador: '.$_SERVER["HTTP_USER_AGENT"].'<br>
			Host Cliente: '.$_SERVER["REMOTE_HOST"].'<br>
			IP Cliente: '.$_SERVER["REMOTE_ADDR"].'<br>
			Metodo Formulario: '.$_SERVER["REQUEST_METHOD"].'<br>
			URI Peticion : '.$_SERVER["REQUEST_URI"].'<br />
			Usuario id: '.$_SESSION['usu_id'].' <br />
			Datos en POST: '.var_export($_POST,true).' <br />
			Datos en GET: '.var_export($_GET,true).' <br />
			Datos en SESSION: '.var_export($_SESSION,true).' <br />
			';

		$cuerpo = str_replace(".",". ",$cuerpo);

		//MAOH - 18 Abr 2012 - Cambio a UTF 8
		$contenido = '
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title>HOTELS quality</title>
			</head>
			<body>'.$cuerpo.'</body></html>';

			$_obj_database = new database($_host_db,$_user_db,$_password_db,$_db);
			
			$obj_correo = new Correo();

			$fecha = date("Y-m-d");

			$sql = "select erp_id
					from errores_reportados
					where erp_codigo = '".$errno."'
							and erp_archivo =  '".$errfile."'
							and erp_linea = '".$errline."'
							and erp_fecha = '".$fecha."'
							and erp_mensaje = '".$errstr."' ";

			$resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
						
			if( is_array($resultado) )
			{
				if( sizeof($resultado) > 0 )
				{
					//si ya lo envio no hace nada
				}//Fin de if( sizeof($resultado) > 0 )
				else
				{
					$campos = array("erp_codigo",
									"erp_archivo",
									"erp_linea",
									"erp_fecha",
									"erp_mensaje");

					$datos['tabla'] = "errores_reportados";
					$datos['erp_codigo'] = $errno;
					$datos['erp_archivo'] = $errfile;
					$datos['erp_linea'] = $errline;
					$datos['erp_fecha'] = $fecha;
					$datos['erp_mensaje'] = $errstr;

					$sql = $_obj_database->generarSQLInsertar($datos,$campos);
					$_obj_database->ejecutarSQL($sql);

					$opciones = array();
                    $obj_correo->enviarCorreo($_EMAIL_ADMIN,"david.males@querytek.com", $asunto, $contenido,NULL,$opciones);
					
				}//Fin de else de if( sizeof($resultado) > 0 )

			}//fin de if( is_array($resultado) )
			else
			{
				$opciones = array();                    
                $obj_correo->enviarCorreo($_EMAIL_ADMIN,"david.males@querytek.com", $asunto, $contenido,NULL,$opciones);
				
			}

			break;
	}//Fin de switch($errno)

	return false;
}//Fin de exception_error_handler

//Si esta activa la opcion entonces envia el reporte de errores
if( $_NOTIFICACION_ERRORES == 1 )
{
	set_error_handler("exception_error_handler");
}

/**
 * Función que se encarga de redireccionar la pagina a https
 * si el llamado se hace a http
 * DM - 2014-01-10
 **/
function redireccionarHttps($datos)
{
	global $_PATH_WEB;

	$url =  $_SERVER[SERVER_NAME];

	//si no existe el protocolo de seguridad entonces debe redireccionar la pagina
	$realiza_redireccion = !isset($_SERVER['HTTPS']) ;

	//si el llamado se hace desde la ruta principal de la aplicacion
	$realiza_redireccion &=  strpos($_PATH_WEB,'http://www.hotels-quality.com') !== false  || strpos($_PATH_WEB,'http://hotels-quality.com') !== false;

	if( $realiza_redireccion )
	{
		//elimina el campo de plantilla
		unset($datos['plantilla']);

		$enlace = Herramientas::enlaceCampos($datos, array_keys($datos) );
		$enlace = substr($enlace,1,strlen($enlace));

		if(  strpos( strtolower($url),"www") !==false )
		{
			header('Location: https://www.hotels-quality.com'.$_SERVER['SCRIPT_NAME'].'?'.$enlace);
			exit;
		}
		else
		{
			header('Location: https://hotels-quality.com'.$_SERVER['SCRIPT_NAME'].'?'.$enlace);
			exit;
		}
	}//Fin de if( strpos('https',$_PATH_WEB) === false )
}//Fin de redireccionarHttps


/**
 * Función que se encarga de interpretar y obtener la lista de tokens
 * que se han agregado en la plantilla
 * DM - 2014-01-22
 **/
function obtenerTokensPlantilla($datos)
{
	$plantilla = $datos['plantilla'];
	$token_prefijo = $datos['token_prefijo'];
	$token_sufijo = $datos['token_sufijo'];

	//obtiene la posición del primer token, si existe
	$pos_token = strpos($plantilla, $token_prefijo );

	$lista_tokens = array();

	//si existe al menos un token lo obtiene
	while( $pos_token !== false )
	{
		//obtiene la posicion del token final, si existe
		$pos_final_token = strpos($plantilla, $token_sufijo , $pos_token + strlen($token_prefijo) );


		if( $pos_final_token !== false)
		{
			//obtiene la cadena retirando el sufijo y pasando la longitud de la cadena desde el
			//inicio del texto, después del token prefijo hasta la posición inicial del token sufijo
			$token = substr( $plantilla , $pos_token+strlen($token_prefijo), $pos_final_token - $pos_token - strlen($token_prefijo) );

			//verifica que sea un token y no un contenido oculto de html como javascript o comentarios
			//verifica que no tenga espacios en blanco
			$sin_espacios = strpos($token,' ') === false;

			//si no cumple con las validaciones entonces lo agrega
			if( $sin_espacios )
			{
				$lista_tokens[] = $token;
			}

			//elimina el token de la plantilla para buscar el proximo, si existe
			$plantilla = str_replace($token_prefijo.$token.$token_sufijo,'',$plantilla);

			//obtiene la posición del primer token, si existe
			$pos_token = strpos($plantilla, $token_prefijo);

		}//Fin de if( $pos_final_token !== false)
		else
		{
			//si no existe un token final entonces no tiene en cuenta el inicial y termina
			$pos_token = false;
		}
	}//Fin de while( $pos_token !== false )

	return $lista_tokens;
}//Fin de obtenerTokensPlantilla


function obtenerTokensPlantillaCSS($datos)
{
	$plantilla = $datos['plantilla'];
	$token_prefijo = $datos['token_prefijo'];
	$token_sufijo = $datos['token_sufijo'];

	//obtiene la posición del primer token, si existe
	$pos_token = strpos($plantilla, $token_prefijo );

	$lista_tokens = array();

	//si existe al menos un token lo obtiene
	while( $pos_token !== false )
	{
		//obtiene la posicion del token final, si existe a partir del token prefijo
		//pues se debe buscar la cadena mas cercana despues del prefijo
		$pos_final_token = strpos($plantilla, $token_sufijo , $pos_token + strlen($token_prefijo) );


		if( $pos_final_token !== false)
		{
			//obtiene la cadena retirando el sufijo y pasando la longitud de la cadena desde el
			//inicio del texto, después del token prefijo hasta la posición inicial del token sufijo
			$token = substr( $plantilla , $pos_token+strlen($token_prefijo), $pos_final_token - $pos_token - strlen($token_prefijo) );

			$sin_espacios = true;

			//Indica si se valida o no que los token no tengas espacios
			if( $datos['omitir_validacion'] != 1  )
			{
				//verifica que sea un token y no un contenido oculto de html como javascript o comentarios
				//verifica que no tenga espacios en blanco
				$sin_espacios = strpos($token,' ') === false;
			}//Fin de if( $datos['omitir_validacion'] !=1  )


			//si no cumple con las validaciones entonces lo agrega
			if( $sin_espacios )
			{
				$lista_tokens[] = $token;
			}

			//elimina el token de la plantilla para buscar el proximo, si existe
			$plantilla = str_replace($token_prefijo.$token.$token_sufijo,'',$plantilla);

			//obtiene la posición del primer token, si existe
			$pos_token = strpos($plantilla, $token_prefijo);

		}//Fin de if( $pos_final_token !== false)
		else
		{
			//si no existe un token final entonces no tiene en cuenta el inicial y termina
			$pos_token = false;
		}
	}//Fin de while( $pos_token !== false )

	return $lista_tokens;
}//Fin de obtenerTokensPlantilla

/**
 * Función que se encarga de asignar una lista de token a una plantilla
 * de acuerdo al arreglo de idioma
 * DM - 2014-01-23
 **/
function asignarTokensIdiomas($plantilla, $lista_variables = array() )
{
	global $idi_despliegue, $_PATH_IMAGENES,$_PATH_WEB;

	Interfaz::asignarToken('path_imagenes',$_PATH_IMAGENES,$plantilla);
	Interfaz::asignarToken('path_web',$_PATH_WEB,$plantilla);

    //Si hay variables para asignar las coloca en la plantilla
    foreach($lista_variables as $variable => $valor)
    {
        if( strpos($plantilla,'<!--'.$variable.'-->') !== false )
        {
            Interfaz::asignarToken($variable,$valor,$plantilla);
        }
    }//Fin de foreach($lista_variables as $variable => $valor)

	$datos = array();
	$datos['plantilla'] = $plantilla;
	$datos['token_prefijo'] = '<!--';
	$datos['token_sufijo'] = '-->';
	$lista_tokens = obtenerTokensPlantilla($datos);

	foreach($lista_tokens as $token)
	{
		Interfaz::asignarTokenDatos($token,$idi_despliegue,$plantilla);
	}//Fin de foreach($lista_tokens as $token)


	return $plantilla;
}//Fin de asignarTokensIdiomas()


/**
 * Identifica si uno de los parámetros que se pasan en el formulario
 * tiene caracteres invalidos como <script>, <?php...
 * DM - 2014-06-19
 **/
function contieneCamposInvalidos($datos)
{
	$contiene_invalidos = false;

	//Verifica por cada campo si es correcto
	foreach($datos as $key => $valor)
	{
		//si el valor es un arreglo entonces revisa el arreglo
		//realizando un llamado recursivo
		if( is_array($valor) )
		{
			$contiene_invalidos = contieneCamposInvalidos($valor);
			//si contiene invalidos entonces debe retornar que no es valido
			if( $contiene_invalidos )
			{
				break;
			}
		}
		else
		{
			$contiene_invalidos = contieneCaracteresInvalidos($valor);
			//si contiene invalidos entonces debe retornar que no es valido
			if( $contiene_invalidos )
			{
				break;
			}
		}
	}//Fin de foreach($datos as $key => $valor)

	return $contiene_invalidos;

}//Fin de contieneCamposInvalidos


/**
 * Verifica si la cadena tiene caracteres invalidos que hagan parte de script
 * maliciosos
 * DM - 2014-06-19
 **/
function contieneCaracteresInvalidos($cadena)
{
	$contiene_invalidos = false;


	//Verifica si la cadena esta en base64
	$cadena_base64 = base64_decode( $cadena );

	//Convierte la cadena a minusculas para hacer la busqueda de cadenas
	$cadena = strtolower($cadena);
	$cadena_base64 = strtolower($cadena_base64);



	//Elimina los espacios en blanco
	$cadena = str_replace(" ","",$cadena);
	$cadena_base64 = str_replace(" ","",$cadena_base64);

	$lista_cadenas_invalidas = array(
						'<script',
						'<?php',
						'<?',
						'insertinto',
						'updatetable',
						'deletefrom',
						'droptable',
						'dropuser',
						'dropindex',
						'truncatetable',
						'<input',
						'<file'
						);

	foreach($lista_cadenas_invalidas as $val_cadena)
	{
		//verifica si una de las cadenas invalidas se encuentra en la cadena
		if( strpos($cadena,$val_cadena) !== false )
		{
			$contiene_invalidos = true;
			break;
		}

		//verifica si una de las cadenas invalidas se encuentra en la cadena
		if( strpos($cadena_base64,$val_cadena) !== false )
		{
			$contiene_invalidos = true;
			break;
		}

	}//Fin de foreach($lista_cadenas_invalidas as $val_cadena)

	return $contiene_invalidos;
}//Fin de contieneCaracteresInvalidos


/**
 * Se encarga de registrar en un archivo por dia todas las peticiones que llegan
 * por POST
 * DM - 2014-10-06
 * */
function registrarLogPOST()
{
	global $_PATH_SERVIDOR;

	if( count($_POST) > 0 )
	{
		$cadena_post = var_export($_POST, true);

		$contenido = date('H:i:s').'::'.$_SERVER['REMOTE_ADDR'].'==>'.$cadena_post.'<==';
		$contenido .= "\r\n";

		$ruta = $_PATH_SERVIDOR.'/log_post/'.date('Y-m-d').'.txt';

		Archivos::crearArchivo($ruta, $contenido);
	}//Fin de if( count($_POST) > 0 )


}//Fin de registrarLogPOST


/**
 * Funcion que se encarga de retornar el contenido que se encuentra
 * entre el token prefijo y sufijo
 * DM - 2014-03-20
 * */
function extraerValorEntreTokens($contenido,$token_prefijo,$token_sufijo='', $cantidad_apariciones_prefijo=0,$cantidad_apariciones_sufijo=0)
{
	//si debe tener en cuenta que el prefijo debe aparecer x veces para retornar el contenido
	if( $cantidad_apariciones_prefijo > 0)
	{
		$pos_token = strpos($contenido, $token_prefijo );

		$cantidad_apariones = 0;
		while($pos_token !== false && $cantidad_apariones < $cantidad_apariciones_prefijo)
		{
			$pos_token = strpos($contenido, $token_prefijo, $pos_token + strlen($token_prefijo) );
			$cantidad_apariones++;
		}

	}//Fin de if( $cantidad_apariciones_prefijo > 0)
	else
	{
		$pos_token = strpos($contenido, $token_prefijo );
	}

	//si no encuentra el token inicial entonces no retorna nada
	if( $pos_token === false)
	{
		return false;
	}//Fin de if( $pos_token === false)

	//si debe tener en cuenta que el sufijo debe aparecer x veces para retornar el contenido
	if( $cantidad_apariciones_sufijo > 0)
	{
		//obtiene la posicion del token final, si existe
		$pos_final_token = strpos($contenido, $token_sufijo,$pos_token );

		$cantidad_apariones = 0;
		while($pos_final_token !== false && $cantidad_apariones < $cantidad_apariciones_sufijo)
		{
			$pos_final_token = strpos($contenido, $token_sufijo, $pos_final_token + strlen($token_sufijo) );
			$cantidad_apariones++;
		}

	}//Fin de if( $cantidad_apariciones_prefijo > 0)
	else
	{
		//Si no se pasa sufijo entonces retorna la posición final
		if( $token_sufijo == '')
		{
			$pos_final_token = strlen($contenido);
		}else
		{
			//obtiene la posicion del token final, si existe
			$pos_final_token = strpos($contenido, $token_sufijo,$pos_token );
		}

	}

	//si no encuentra el token final  entonces no retorna nada
	if( $pos_final_token === false)
	{
		return false;
	}//Fin de if( $pos_final_token === false)

	//obtiene la cadena retirando el sufijo y pasando la longitud de la cadena desde el
	//inicio del texto, después del token prefijo hasta la posición inicial del token sufijo
	$rango_inicial = $pos_token+strlen($token_prefijo);
	$rango_final = $pos_final_token - $pos_token - strlen($token_prefijo);
	$cadena = substr($contenido,$rango_inicial,$rango_final );

	return trim($cadena);
}//Fin de  extraerValorEntreTokens



//Función para detectar el idioma del navegador del usuario para definir idioma de encuestas, ofertas y multiofertas
//OR 2015-01-08
function detectar_idioma_navegador()
{
	$idioma_navegador = $_SERVER['HTTP_ACCEPT_LANGUAGE'];

	$idioma_navegador = substr($idioma_navegador, 0, 5);

	if ($idioma_navegador == "pt-BR")
	{
		$idioma_navegador = 'br';
	}
	else
	{
		$idioma_navegador = substr($idioma_navegador, 0, 2);
	}

	switch ($idioma_navegador)
    {
        case 'es':
            $idi_id = 1;
            break;
        case 'en':
            $idi_id = 2;
            break;
        case 'fr':
            $idi_id = 3;
            break;
        case 'de':
            $idi_id = 4;
            break;
        case 'it':
            $idi_id = 5;
            break;
        case 'pt':
            $idi_id = 7;
            break;
        case 'br':
            $idi_id = 187;
            break;
        case 'pl':
            $idi_id = 10;
            break;
        case 'ca':
            $idi_id = 12;
            break;
        case 'zh':
            $idi_id = 13;
            break;
        case 'ru':
            $idi_id = 14;
            break;
        case 'uk':
            $idi_id = 15;
            break;
        case 'az':
            $idi_id = 26;
            break;
        default:
            $idi_id = 2;
            break;
    }//fin de Switch
	return $idi_id;
}//Fin de detectar_idioma_navegador

?>