<?php
/*
*	Nombre: Menu.php
*	Modulo: Menu
*	DescripciÃ³n: Maneja todo lo referente al despliegue del menu de opciones
*/

class Menu
{
	var $_contenido_menu;

	function __construct()
    {
			$this->_contenido_menu = "";
    }//Fin de Menu()


    /**
     * Funcion que se encargar de generar el menu para usuarios que tiene acceso al bpm
     * DM - 2013-01-12
     * **/
    function crearMenuBPM()
    {
        global $_PATH_WEB,$_PATH_IMAGENES,$idi_despliegue,$datos,$obj_usuarios;


		//guarda las opciones del menu que se van a mostrar
		$menu = array();
		switch($_SESSION["tipo_usuario"]) //Dependiendo del tipo de usuario se construye el menu
		{

			case 'H'://Usuario Hotel

                //inicia el submenu de operaciones
                $menu[operaciones][ubicacion] = 'izq';
                $menu[operaciones][posicion] = 1;
                $menu[operaciones][submenu] = array();
                $menu[operaciones][nombre] = $idi_despliegue['bpm_menu_operaciones'];

                if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 3)) //Funcionalidad 3 = PestaÃ±a Mensual
                {
                  if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 4)) //Funcionalidad 4 = Menu General
                  {
                  $submenu = array();
                  $submenu[enlace] = "index.php?m=reportes&accion=hotel_mes";
                  $submenu[nombre] = $idi_despliegue['general'];
                  $menu[operaciones][submenu][] = $submenu;
                  }//Fin de if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 4))

                  $submenu = array();
                  if( $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 30) )
                  {
                  $submenu[enlace] = "index.php?m=familias&accion=familias_mensual";
                  }//Fin de if( !$obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 30) )
                  else
                  {
                  $submenu[enlace] = "javascript:void(0);";
                  $mensajeContacto = $obj_usuarios->mensajeActivarFuncionalidad();
                  $submenu[tooltip] = " onClick='fixedtooltip(\"".$mensajeContacto."\",this, event, \"180px\")' onMouseout='delayhidetip()' ";
                  }//Fin de else de if( !$obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 30) )

                  $submenu[nombre] = $idi_despliegue['familias'];
                  $menu[operaciones][submenu][] = $submenu;

                }//Fin de if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 3))

                //El usuario es de segunda categoria y tiene activa la funcionalidad se exhibe la pestaÃ±a
                if ( $_SESSION['usu_usc_id'] == 2 && $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 5)) //Funcionalidad 5 = PestaÃ±a Evolucion
    			{
                    $submenu = array();
    				$submenu[enlace] = "index.php?m=reportes&accion=evolucion";
    				$submenu[nombre] = $idi_despliegue['menu_evolucion'];
    				$menu[operaciones][submenu][] = $submenu;
    			}
                else if ( $_SESSION['usu_usc_id'] == 1)
    			{
                    $submenu = array();
    				$submenu[enlace] = "index.php?m=reportes&accion=evolucion";
    				$submenu[nombre] = $idi_despliegue['menu_evolucion'];
    				$menu[operaciones][submenu][] = $submenu;
    			}

                //Nuevo Elemento de Menu desde Julio 8 de 2011
                if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 6)) //Funcionalidad 6 = PestaÃ±a Perfil
    			{
                    $submenu = array();
    				$submenu[enlace] = "index.php?m=reportes&accion=ver_perfil";
                    $nombre = $idi_despliegue['menu_perfil'];
                    $nombre = str_replace("&nbsp;","",$nombre);
    				$submenu[nombre] = $nombre;
    				$menu[operaciones][submenu][] = $submenu;
    			}


                if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 11)) //Funcionalidad 11 = PestaÃ±a Respuestas
    			{
                    //Menu encuestas
                    $submenu = array();
                    $submenu[nombre] = $idi_despliegue[menu_encuestas];
                    if($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 12))
                    {
                        $submenu[enlace] = "index.php?m=reportes&accion=respuestas";
                    }//Fin de if($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 12))
                    else
                    {
						$mensajeContacto = $obj_usuarios->mensajeActivarFuncionalidad();
						$submenu[enlace] = "javascript:void(0);";
						$submenu[tooltip] = " onClick='fixedtooltip(\"".$mensajeContacto."\",this, event, \"180px\")' onMouseout='delayhidetip()' ";
                    }//Fin de if($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 12))

                    $menu[operaciones][submenu][] = $submenu;


                    if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 14)) //Funcionalidad 14 = Destinatarios
                    {
                        $submenu = array();
                        $submenu[enlace] = "index.php?m=auxiliar&accion=ver_destinatarios";
                        $submenu[nombre] = $idi_despliegue[menu_destinatarios];
                        $menu[operaciones][submenu][] = $submenu;
                    }

                   //DM - 2012-11-20
                   //Si tiene activa la funcionalidad de despedida (29)
                   //y compartir en redes sociales (54) o
                   //integracion con tripadvisor (55)
                   //le permite el acceso
					if( $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 29)
                      && (
                            $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 54)
                            ||
                            $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 55)
                         )
                      )
					{
						$submenu = array();
						$submenu[enlace] = "index.php?m=auxiliar&accion=ver_click_performance";
						$submenu[nombre] = $idi_despliegue['menu_click_performance'];
                        $menu[operaciones][submenu][] = $submenu;
					}

    			}//Fin de if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 11))

                if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 16)) //Funcionalidad 16 = PestaÃ±a Reputacion
    			{
                    //DM - 2015-01-15 issue 5870
					$submenu = array();
					$submenu[enlace] = "index.php?m=reputacion&accion=iframe";
					$submenu[nombre] = $idi_despliegue['menu_reputacion'];
                    $menu[operaciones][submenu][] = $submenu;
    			}

				if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 7)) //Funcionalidad 7 = PestaÃ±a Comentarios
				{

					$submenu = array();
					$submenu[enlace] = "index.php?m=comentarios&accion=ver_comentarios";
					$submenu[nombre] = $idi_despliegue['menu_ver_comentarios'];
                    $menu[operaciones][submenu][] = $submenu;

					$submenu = array();
					$submenu[enlace] = "index.php?m=comentarios&accion=publicar_comentarios";
					$submenu[nombre] = $idi_despliegue['menu_publicar_comentarios'];
                    $menu[operaciones][submenu][] = $submenu;
				}



                if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 32)) //Funcionalidad 32 = PestaÃ±a Auxiliar
    			{
                    $menu[inputs][ubicacion] = 'der';
                    $menu[inputs][posicion] = 1;
    				$menu[inputs][enlace] = "index.php?m=auxiliar&accion=fm_enviar";
                    $menu[inputs][nombre] = $idi_despliegue['bpm_menu_inputs'];
    				$menu[inputs][submenu] = array();

  					$submenu = array();
  					$submenu[enlace] = "index.php?m=auxiliar&accion=fm_enviar";
  					$submenu[nombre] = $idi_despliegue[menu_enviar];
                    $menu[inputs][submenu][] = $submenu;

    				//El usuario es de segunda categoria y tiene activa la funcionalidad Grabar, se exhibe el submenu
    				if ( $_SESSION['usu_usc_id'] == 2 && $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 34)) //Funcionalidad 34 = Grabar
                    {
						$submenu = array();
						$submenu[enlace] = "index.php?m=desplieguecuestionario&accion=fm_grabar";
						$submenu[nombre] = $idi_despliegue[menu_grabar];
                        $menu[inputs][submenu][] = $submenu;
    				}
    				else if ( $_SESSION['usu_usc_id'] == 1) //El usuario es de primera categoria, siempre vera el submenu Grabar
                    {
    					$submenu = array();
    					$submenu[enlace] = "index.php?m=desplieguecuestionario&accion=fm_grabar";
    					$submenu[nombre] = $idi_despliegue[menu_grabar];
                        $menu[inputs][submenu][] = $submenu;
    				}

					$submenu = array();
					$submenu[enlace] = "index.php?m=bpminputdata&accion=inicio";
					$submenu[nombre] = $idi_despliegue['menu_root_bpm_hotel_inputdaata'];
    				$menu[inputs][submenu][] = $submenu;
    			}//Fin de if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 32))


                // Menu Configuracion
                if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 17)) //Funcionalidad 17 = PestaÃ±a Configuracion
    			{
                    $menu[configuracion][posicion] = 0;
                    $menu[configuracion][ubicacion] = 'der';
                    $menu[configuracion][submenu] = array();
                    $menu[configuracion][nombre] = $idi_despliegue[menu_configuracion];

    				if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 18)) //Funcionalidad 18 = Cuestionario
    				{
    					$submenu = array();
    					$submenu[enlace] = "index.php?m=cuestionario&accion=ver_cuestionario";
    					$submenu[nombre] = $idi_despliegue['menu_conf_cuest'];
    					$menu[configuracion][submenu][] = $submenu;
    				}

    				$submenu = array();
    				$submenu[enlace] = "index.php?m=datoshotel&accion=fm_datoshotel";
    				$submenu[nombre] = $idi_despliegue['menu_datos_hotel'];
    				$menu[configuracion][submenu][] = $submenu;


                    //El usuario es de segunda categoria y tiene activa la funcionalidad Plantilla Email, se exhibe el submenu
                    $acceso_plantillas = $_SESSION['usu_usc_id'] == 2 && $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 25);
                    //El usuario es de primera categoria, siempre vera el submenu Plantilla Email
                    $acceso_plantillas |= $_SESSION['usu_usc_id'] == 1;

                    //El usuario es de segunda categoria y tiene activa la funcionalidad Despedida, se exhibe el submenu
                    $acceso_plantillas != ( $_SESSION['usu_usc_id'] == 2 && $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 29) );

                    //El usuario es de segunda categoria y tiene activa la funcionalidad email de agradecimiento, se exhibe el submenu
                    $acceso_plantillas != ( $_SESSION['usu_usc_id'] == 2 && $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 59) );

                    if( $acceso_plantillas )
                    {
                        $submenu = array();
					$submenu[enlace] = 'index.php?m=plantillascorreo&accion=fm_gestion_plantillas';
					$submenu[nombre] = $idi_despliegue['menu_gestion_plantillas'];
					$menu[configuracion][submenu][] = $submenu;
                    }//Fin de if( $acceso_plantillas )

    				$submenu = array();
    				$submenu[enlace] = "index.php?m=usuarios&accion=gestion";
    				$submenu[nombre] = $idi_despliegue['menu_gestion_usuarios'];
    				$menu[configuracion][submenu][] = $submenu;

                    //DM - 2013-10-28 issue 2494
    				$submenu = array();
    				$submenu[enlace] = "index.php?m=objetivos&accion=gestion";
    				$submenu[nombre] = $idi_despliegue['menu_objetivos'];
    				$menu[configuracion][submenu][] = $submenu;


      		    	//DM - 2013-08-12
      			    //Funcionalidad 61 - Recogida de datos
                    $funcionalidad_vinculos = $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 61);
      				if( $funcionalidad_vinculos )
      				{
      					$submenu = array();
                        $submenu[enlace] = "index.php?m=auxiliar&accion=fm_recogida_datos_vinculos";
      					$submenu[nombre] = $idi_despliegue['menu_recogida_datos'];
      					$menu[configuracion][submenu][] = $submenu;
      				}//Fin de if( $funcionalidad_vinculos || $funcionalidad_carrousel )

      		    	//DM - 2013-11-30
      			    //Funcionalidad 63 - Carrouseles
                    $funcionalidad_carrousel = $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 63 );
      				if( $funcionalidad_carrousel )
      				{
      					$submenu = array();
                        $submenu[enlace] = "index.php?m=carrouseles&accion=fm_recogida_datos_carrousel";
      					$submenu[nombre] = $idi_despliegue['menu_programar_carrousel'];
      					$menu[configuracion][submenu][] = $submenu;

      				}//Fin de if( $funcionalidad_carrousel )


    				$submenu = array();
                    if ( $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 30) )
                    {
    				    $submenu[enlace] = "index.php?m=familias&accion=gestion";
                    }//Fin de if ( $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 30) )
                    else
                    {
						$mensajeContacto = $obj_usuarios->mensajeActivarFuncionalidad();
						$submenu[enlace] = "javascript:void(0);";
						$submenu[tooltip] = " onClick='fixedtooltip(\"".$mensajeContacto."\",this, event, \"180px\")' onMouseout='delayhidetip()' ";
                    }//Fin de else de if ( $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 30) )

    				$submenu[nombre] = $idi_despliegue['menu_gestion_informes'];
    				$menu[configuracion][submenu][] = $submenu;


    		    	//MAOH - 11 Mayo 2012 - Submenu HQ Index
    			    //Funcionalidad 57 => HQ Index
    				if($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 57))
    				{
    					$submenu = array();
    					$submenu[enlace] = "index.php?m=reportes&accion=obtener_widget";
    					$submenu[nombre] = $idi_despliegue['hqi_menu_widget'];
    					$menu[configuracion][submenu][] = $submenu;
    				}//MAOH - 11 Mayo 2012 - Fin


    		    	//DM - 2013-08-12
    			    //Funcionalidad 61 - Recogida de datos
    				if($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 61))
    				{
    					$submenu = array();
    					$submenu[enlace] = "index.php?m=auxiliar&accion=fm_recogida_datos";
    					$submenu[nombre] = $idi_despliegue['menu_recogida_datos'];
    					$menu[configuracion][submenu][] = $submenu;
    				}//MAOH - 11 Mayo 2012 - Fin

    				/*
    				POR AHORA SE DESACTIVA LO DE COMPETITIVE SET DESACTIVADO MAYO 16/2011
    				if( $_SESSION["est_cs"]==1 || $_SESSION["est_cs"]==0)
    				{
    						$submenu = array();
    						$submenu[enlace] = "index.php?m=establecimientos&accion=fm_cs";
    						$submenu[nombre] = $idi_despliegue['menu_cs'];
    					$menu[configuracion][submenu][] = $submenu;
    				}*/
    			}//Fin de tiene activa la funcionalidad 17 = PestaÃ±a Configuracion
                // Fin Menu Configuracion

            break;

        }//Fin de switch($_SESSION["tipo_usuario"])


        //Estas opciones del menu siempre estan presentes en este tipo de Menu

        $menu[cuadro_mando][ubicacion] = 'izq';
        $menu[cuadro_mando][posicion] = 0;
        $menu[cuadro_mando][enlace] = "index.php?m=bpmperformance&accion=inicio";
        $menu[cuadro_mando][nombre] = $idi_despliegue['menu_root_bpm_hotel_performance'];


        $menu[financiera][ubicacion] = 'izq';
        $menu[financiera][posicion] = 2;
        $menu[financiera][enlace] = "index.php?m=bpmhops&accion=inicio";
        $menu[financiera][nombre] = $idi_despliegue['menu_root_bpm_hops'];
        $menu[financiera][submenu] = array();

        $submenu = array();
        $submenu[enlace] = "index.php?m=bpmhops&accion=inicio";
        $submenu[nombre] = $idi_despliegue['menu_bpm_finaciera_evolucion'];
        $menu[financiera][submenu][] = $submenu;


        $menu[marketing][ubicacion] = 'izq';
        $menu[marketing][posicion] = 3;
        $menu[marketing][nombre] = $idi_despliegue['menu_root_bpm_bpmmarketing'];
        $menu[marketing][submenu] = array();

        $submenu = array();
        $submenu[enlace] = "index.php?m=bpmmarketing&accion=hops";
        $submenu[nombre] = $idi_despliegue['menu_bpm_marketing_hops'];
        $menu[marketing][submenu][] = $submenu;

        $submenu = array();
        $submenu[enlace] = "index.php?m=bpmmarketing&accion=inicio";
        $submenu[nombre] = $idi_despliegue['menu_bpm_marketing_evolucion'];
        $menu[marketing][submenu][] = $submenu;


        $menu[aprendizaje][ubicacion] = 'izq';
        $menu[aprendizaje][posicion] = 4;
        $menu[aprendizaje][enlace] = "index.php?m=bpmlearninggrow&accion=inicio";
        $menu[aprendizaje][nombre] = $idi_despliegue['menu_root_bpm_learninggrow'];
        $menu[aprendizaje][submenu] = array();

        $submenu = array();
        $submenu[enlace] = "index.php?m=bpmlearninggrow&accion=inicio";
        $submenu[nombre] = $idi_despliegue['menu_bpm_aprendizaje_evolucion'];
        $menu[aprendizaje][submenu][] = $submenu;


        $menu[simulacion][ubicacion] = 'izq';
        $menu[simulacion][posicion] = 5;
        $menu[simulacion][enlace] = "index.php?m=breakEven&accion=list";
        $menu[simulacion][nombre] = $idi_despliegue['bpm_menu_simulacion'];
        $menu[simulacion][submenu] = array();

        $submenu = array();
        $submenu[enlace] = "index.php?m=breakEven&accion=list";
        $submenu[nombre] = $idi_despliegue['menu_bpm_simulacion_evolucion'];
        $menu[simulacion][submenu][] = $submenu;

        if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 56)) //Funcionalidad 56 = consultores
        {
          $menu[econsulting][ubicacion] = 'izq';
          $menu[econsulting][posicion] = 6;
          $menu[econsulting][enlace] = "index.php?m=bpmconsultant&accion=inicio";
          $menu[econsulting][nombre] = $idi_despliegue['bpm_menu_consulting'];
          $menu[econsulting][submenu] = array();

          $submenu = array();
          $submenu[enlace] = "index.php?m=bpmconsultant&accion=inicio";
          $submenu[nombre] = $idi_despliegue['menu_root_bpm_breakeven_consultant'];
          $menu[econsulting][submenu][] = $submenu;
        }//Fin de if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 56))



        $this->generarMenu($menu);

    }//Fin de crearMenuBPM()

    /**
     * Funcion que se encarga de generar el codigo html para las opciones
     * que se encuentran en la parte izquierda y derecha del menu
     * DM - 2013-01-14
     * */
    function generarItemMenuItem($datos)
    {
        global $_PATH_IMAGENES, $idi_despliegue;

		//obtiene el modulo activo en la apicacion
		$modulo_activo = $_SESSION['modulo_activo'];

        //Para BPM identifica el modulo activo de acuerdo al modulo seleccionado y la accion
        $datos_parametros = array_merge($_GET, $_POST);
        $modulo_parametro = $datos_parametros['m'];
        $accion_parametro = $datos_parametros['accion'];

        switch($modulo_activo)
        {
            case "hotel_mes":
                $modulo_activo = "inicio";
            case "familias_mes":
            case "evolucion":
            case "perfil":
            case "respuestas":
            case "reputacion":
                $modulo_activo_2 = "operaciones";
                break;

            case "auxiliar":
                $modulo_activo_2 = "operaciones";
                break;

            case "bpmhops":
                $modulo_activo_2 = "financiera";
                break;

            case "bpmmarketing":
                $modulo_activo_2 = "marketing";
                break;

            case "bpmlearninggrow":
                $modulo_activo_2 = "aprendizaje";
                break;

            case "breakEven":
                $modulo_activo_2 = "simulacion";
                break;


        }//Fin de switch($modulo_parametro)

        $prefijo = $datos['prefijo'];
        $modulo = $datos['modulo'];

        $activo = $modulo == $modulo_activo || $modulo == $modulo_activo_2 ? 1 : 0;
        $estilo_item = $modulo== $modulo_activo || $modulo == $modulo_activo_2 ? 'class="activo"' : '';

        $menu_opcion = $datos['menu_opcion'];

        $submenu = "";
        if( is_array($menu_opcion['submenu']) && sizeof($menu_opcion['submenu'] > 0) )
        {
            $submenu = $this->colocarOpcionesSubmenu($menu_opcion['submenu'],$prefijo,$activo) ;

            $nombre = $menu_opcion[nombre];
            if( $modulo == 'configuracion')
            {
               // $nombre = '<img alt="'.$idi_despliegue['label_menu_configuracion'].'" title="'.$idi_despliegue['label_menu_configuracion'].'" class="opcion_config" src="'.$_PATH_IMAGENES.'/menu_configuracion.jpg" >';
              $nombre = "Configuraciones";
            }

            //DM - 2014-05-01 issue 4400
            $estilo_cicerone = 'item';
            if( $modulo == 'cicerone')
            {
                $estilo_cicerone = 'item_cicerone';
            }

    		$menu_item = "<li ng-class=''>
                		 <a ".$estilo_item." ><i class='fa fa-th-large'></i> <span class='nav-label'>  ".$nombre."</span><span class='fa arrow'></span></a>	$submenu </li>";

        }//Fin de if( is_array($menu_opcion['submenu']) && sizeof($menu_opcion['submenu'] > 0)
        else
        {

    		$menu_item = "<li ng-class=''><a href='".$menu_opcion[enlace]."'><i class='fa fa-th-large'></i><span class='nav-label'> ".$menu_opcion[nombre]."</span></a></li>";


        }//Fin de if( is_array($menu_opcion['submenu']) && sizeof($menu_opcion['submenu'] > 0) )

       // AMP - Solo retornar un menu -
	   //return array("menu_item" => $menu_item,
         //           "submenu"  => $submenu);
        return array("menu_item" => $menu_item);
    }//Fin de generarItemMenuItem()


    /**
     * Funcion que se encarga de generar el codigo html para el menu
     * DM - 2013-01-14
     * */
    function generarMenu($datos)
    {
        $submenu_izq = array();
		$submenu_der = array();

		$opciones_izq = array();
		$opciones_der = array();
        $iconomenu = array();
        //$iconomenu ={ "fa-bar-chart-o", "" }
        foreach($datos as $menu_id => $menu_item)
        {
            $posicion = $menu_item['posicion'];

            $datos_opcion = array();
            $datos_opcion['prefijo'] = substr($menu_id, 0, 3);
            $datos_opcion['modulo'] = $menu_id;
            $datos_opcion['menu_opcion'] = $menu_item;

            if( $menu_item['ubicacion'] == "izq" )
            {
                $datos_opcion =  $this->generarItemMenuItem($datos_opcion);
               //AMP - QUITO SUB MENU Y LO INTEGRO AL MENU $submenu_izq[ $posicion ] = $datos_opcion['submenu'];
                $opciones_izq[ $posicion ] = $datos_opcion['menu_item'];

            }//Fin de if( $menu_item['ubicacion'] == "izq" )
            else
            {
                $datos_opcion =  $this->generarItemMenuItem($datos_opcion);
               // $submenu_der[ $posicion ] = $datos_opcion['submenu'];
                $opciones_der[ $posicion ] = $datos_opcion['menu_item'];
            }//Fin de else de if( $menu_item['ubicacion'] == "izq" )

        }//Fin de foreach($datos as $menu_id => $menu_item)

        //ordena los items del menu segun la posicion
        ksort($opciones_izq);
        ksort($submenu_izq);
        ksort($opciones_der);
        ksort($submenu_der);

		//si hay opciones del menu de izquierda habilitadas, las muestra
		if( sizeof($opciones_izq) > 0 )
		{
			$menu_izquierda_opciones .= implode('',$opciones_izq);
		}//Fin de if( sizeof($menu_reportes) > 0 )

		//si hay opciones del menu de izquierda habilitadas las muestra
		if( sizeof($opciones_der) > 0 )
		{
			$menu_derecha_opciones .= implode('',$opciones_der);
		}//Fin de if( sizeof($menu_reportes) > 0 )

		$this->contenido_menu = $menu_izquierda_opciones.$menu_derecha_opciones;

    }//Fin de generarMenu()


	function crearMenu()
    {
        global $_PATH_WEB,$_PATH_IMAGENES,$idi_despliegue,$datos,$obj_usuarios;

        $this->contenido_menu = "";

        if( $_SESSION["autenticado"]==1) //El usuario ya se ha autenticado en el sistema
        {

            $tipo_usuario = $_SESSION["tipo_usuario"];

            //si el tipo de usuario es diferente de root y tiene activo el bpm entonces
            //cambia de menu
            if( $tipo_usuario != "R" && $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 53) )
            {
                $this->crearMenuBPM();
                return $this->contenido_menu;
            }//Fin de if( $tipo_usuario != "R" && $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 58) )

    		//guarda las opciones del menu que se van a mostrar
    		$menu = array();

            switch($tipo_usuario) //Dependiendo del tipo de usuario se construye el menu
            {
				case 'R'://Usuario Root
                    $menu['reportes']['ubicacion'] = 'izq';
                    $menu['reportes']['posicion'] = 0;
                    $menu['reportes']['nombre'] = 'Reportes';

					$menu['reportes']['submenu'] = array();

					$submenu = array();
					$submenu['enlace'] = "index.php?m=establecimientos&accion=listar";
					$submenu['nombre'] = $idi_despliegue['menu_root_reportes_inicio'];
					$menu['reportes']['submenu'][] = $submenu;

					$submenu = array();
					$submenu['enlace'] = "index.php?m=establecimientos&accion=historial";
					$submenu['nombre'] = $idi_despliegue['menu_root_reportes_historial'];
					$menu['reportes']['submenu'][] = $submenu;


                    $menu['cadenas']['ubicacion'] = 'izq';
                    $menu['cadenas']['posicion'] = 1;
                    $menu['cadenas']['nombre'] = 'Cadenas';
					$menu['cadenas']['enlace'] = "index.php?m=cadenas&accion=fm_gestion";

					//MAOH - 01 Nov 2011 - Menu paquetes
                    $menu['paquetes']['ubicacion'] = 'izq';
                    $menu['paquetes']['posicion'] = 2;
                    $menu['paquetes']['nombre'] = 'Paquetes';
					$menu['paquetes']['enlace'] = "index.php?m=paquetes&accion=listar";

                    $menu['formularios']['ubicacion'] = 'izq';
                    $menu['formularios']['posicion'] = 3;
                    $menu['formularios']['nombre'] = $idi_despliegue['menu_formularios'];
                    $menu['formularios']['enlace'] = "index.php?m=auxiliar&accion=fm_formularios_codigos";

                    $menu['lista_robinson']['ubicacion'] = 'izq';
                    $menu['lista_robinson']['posicion'] = 4;
                    $menu['lista_robinson']['nombre'] = $idi_despliegue['menu_lista_robinson'];
                    $menu['lista_robinson']['enlace'] = "index.php?m=establecimientos&accion=fm_lista_robinson";

                    $menu['respuestas']['ubicacion'] = 'izq';
                    $menu['respuestas']['posicion'] = 5;
                    $menu['respuestas']['nombre'] = $idi_despliegue['menu_destinatarios'];
                    $menu['respuestas']['enlace'] = "index.php?m=auxiliar&accion=ver_destinatarios";

    		    	//DM - 2013-09-24
                    $menu['traducciones']['ubicacion'] = 'izq';
                    $menu['traducciones']['posicion'] = 6;
                    $menu['traducciones']['nombre'] = $idi_despliegue['menu_traduccion'];
					$menu['traducciones']['enlace'] = '';

                    $menu['traducciones']['submenu'] = array();

  					$submenu = array();
  					$submenu['enlace'] = 'index.php?m=auxiliar&accion=fm_traducciones';
  					$submenu['nombre'] = $idi_despliegue['submenu_traducir'];
  					$menu['traducciones']['submenu'][] = $submenu;

  					$submenu = array();
  					$submenu['enlace'] = 'index.php?m=auxiliar&accion=fm_sugerencias_traduccion';
  					$submenu['nombre'] = $idi_despliegue['submenu_sugerencias'];
  					$menu['traducciones']['submenu'][] = $submenu;

    		    	//DM - 2013-10-09
                    $menu['club']['ubicacion'] = 'izq';
                    $menu['club']['posicion'] = 7;
                    $menu['club']['nombre'] = $idi_despliegue['menu_club'];
					$menu['club']['enlace'] = '';

                    $menu['club']['submenu'] = array();

  					$submenu = array();
  					$submenu['enlace'] = 'index.php?m=club&accion=busqueda_establecimiento';
  					$submenu['nombre'] = $idi_despliegue['submenu_inventario'];
  					$menu['club']['submenu'][] = $submenu;

  					$submenu = array();
  					$submenu['enlace'] = 'index.php?m=club&accion=fm_contenidos';
  					$submenu['nombre'] = $idi_despliegue['submenu_contenidos'];
  					$menu['club']['submenu'][] = $submenu;


    		    	//DM - 2014-01-16 issue 3778
                    $menu['cicerone']['ubicacion'] = 'izq';
                    $menu['cicerone']['posicion'] = 8;
                    $menu['cicerone']['nombre'] = $idi_despliegue['menu_cicerone'];
					$menu['cicerone']['enlace'] = '';

                    $menu['cicerone']['submenu'] = array();


  					$submenu = array();
  					$submenu['enlace'] = '';
  					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_informes'];
  					$menu['cicerone']['submenu'][] = $submenu;

  					$submenu = array();
  					$submenu['enlace'] = 'index.php?m=cicerone&accion=informe_tabla_resultados';
  					$submenu['nombre'] = $idi_despliegue['submenu_tabla_resultados'];
  					$menu['cicerone']['submenu'][] = $submenu;

  					$submenu = array();
  					$submenu['enlace'] = 'index.php?m=cicerone&accion=incidencias_registros';
  					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_incidencias_registros'];
  					$menu['cicerone']['submenu'][] = $submenu;


  					$submenu = array();
  					$submenu['enlace'] = '';
  					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_store'];
  					$menu['cicerone']['submenu'][] = $submenu;

  					$submenu = array();
  					$submenu['enlace'] = 'index.php?m=cicerone&accion=fm_store_datos';
  					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_store_datos'];
  					$menu['cicerone']['submenu'][] = $submenu;

                    $submenu = array();
  					$submenu['enlace'] = 'index.php?m=cicerone&accion=fm_store_proponer_proveedor';
  					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_store_proponer_proveedor'];
  					$menu['cicerone']['submenu'][] = $submenu;

                    $submenu = array();
  					$submenu['enlace'] = 'index.php?m=cicerone&accion=fm_store_subir_proveedor';
  					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_store_subir_proveedor'];
  					$menu['cicerone']['submenu'][] = $submenu;

                    $submenu = array();
  					$submenu['enlace'] = 'index.php?m=cicerone&accion=fm_store_seleccion_ofertas';
  					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_store_seleccion_ofertas'];
  					$menu['cicerone']['submenu'][] = $submenu;

                    $submenu = array();
  					$submenu['enlace'] = 'index.php?m=cicerone&accion=fm_store_informes';
  					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_store_informes'];
  					$menu['cicerone']['submenu'][] = $submenu;

                    $submenu = array();
  					$submenu['enlace'] = 'index.php?m=cicerone&accion=destinatarios_store';
  					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_store_destinatarios'];
  					$menu['cicerone']['submenu'][] = $submenu;


  					$submenu = array();
  					$submenu['enlace'] = '';
  					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_provider'];
  					$menu['cicerone']['submenu'][] = $submenu;

  					$submenu = array();
  					$submenu['enlace'] = 'index.php?m=cicerone&accion=fm_proveedor_datos';
  					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_provider_datos'];
  					$menu['cicerone']['submenu'][] = $submenu;

  					$submenu = array();
  					$submenu['enlace'] = 'index.php?m=cicerone&accion=fm_provider_ofertas';
  					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_provider_ofertas'];
  					$menu['cicerone']['submenu'][] = $submenu;

  					$submenu = array();
  					$submenu['enlace'] = 'index.php?m=cicerone&accion=fm_provider_informes';
  					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_provider_informe'];
  					$menu['cicerone']['submenu'][] = $submenu;


                    //opciones del menu de la parte derecha
                    $menu['configuracion']['ubicacion'] = 'der';
                    $menu['configuracion']['posicion'] = 0;
                    $menu['configuracion']['nombre'] = $idi_despliegue['menu_configuracion'];
					$menu['configuracion']['enlace'] = '';

					$menu['configuracion']['submenu'] = array();

					$submenu = array();
					$submenu['enlace'] = "index.php?m=usuarios&accion=gestion";
					$submenu['nombre'] = $idi_despliegue['menu_gestion_usuarios'];
					$menu['configuracion']['submenu'][] = $submenu;

					$submenu = array();
					$submenu['enlace'] = "index.php?m=plantillascorreo&accion=fm_gestion_plantillas";
					$submenu['nombre'] = $idi_despliegue['menu_gestion_plantillas'];
					$menu['configuracion']['submenu'][] = $submenu;

          //JP - 2014-04-19
          $submenu = array();
					$submenu['enlace'] = "index.php?m=categoriaoferta&accion=fm_gestion_categoria";
					$submenu['nombre'] = $idi_despliegue['menu_gestion_categorias'];
					$menu['configuracion']['submenu'][] = $submenu;

  		    	//DM - 2013-08-12
					$submenu = array();
					$submenu['enlace'] = "index.php?m=auxiliar&accion=fm_recogida_datos_vinculos";
					$submenu['nombre'] = $idi_despliegue['menu_recogida_datos'];
					$menu['configuracion']['submenu'][] = $submenu;

					$submenu = array();
          $submenu['enlace'] = "index.php?m=carrouseles&accion=fm_recogida_datos_carrousel";
					$submenu['nombre'] = $idi_despliegue['menu_programar_carrousel'];
					$menu['configuracion']['submenu'][] = $submenu;

          /*DMG -2015-03-10*/
          $submenu = array();
          $submenu['enlace'] = "index.php?m=cicerone&accion=fm_gestion_correos_excluidos";
					$submenu['nombre'] = $idi_despliegue['menu_informe_modificacion'];
					$menu['configuracion']['submenu'][] = $submenu;
				break;

         case 'S': //super usuario
    		    	//DM - 2014-01-16 issue 3778
                    $menu['cicerone']['ubicacion'] = 'izq';
                    $menu['cicerone']['posicion'] = 8;
                    $menu['cicerone']['nombre'] = $idi_despliegue['menu_cicerone'];
					$menu['cicerone']['enlace'] = '';

                    $menu['cicerone']['submenu'] = array();

  					$submenu = array();
  					$submenu['enlace'] = '';
  					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_store'];
  					$menu['cicerone']['submenu'][] = $submenu;

  					$submenu = array();
  					$submenu['enlace'] = 'index.php?m=cicerone&accion=fm_store_datos';
  					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_store_datos'];
  					$menu['cicerone']['submenu'][] = $submenu;

                    $submenu = array();
  					$submenu['enlace'] = 'index.php?m=cicerone&accion=fm_store_proponer_proveedor';
  					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_store_proponer_proveedor'];
  					$menu['cicerone']['submenu'][] = $submenu;

                    $submenu = array();
  					$submenu['enlace'] = 'index.php?m=cicerone&accion=fm_store_subir_proveedor';
  					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_store_subir_proveedor'];
  					$menu['cicerone']['submenu'][] = $submenu;

                    $submenu = array();
  					$submenu['enlace'] = 'index.php?m=cicerone&accion=fm_store_seleccion_ofertas';
  					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_store_seleccion_ofertas'];
  					$menu['cicerone']['submenu'][] = $submenu;

                    $submenu = array();
  					$submenu['enlace'] = 'index.php?m=cicerone&accion=fm_store_informes';
  					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_store_informes'];
  					$menu['cicerone']['submenu'][] = $submenu;

                    $submenu = array();
  					$submenu['enlace'] = 'index.php?m=cicerone&accion=destinatarios_store';
  					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_store_destinatarios'];
  					$menu['cicerone']['submenu'][] = $submenu;


  					$submenu = array();
  					$submenu['enlace'] = '';
  					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_provider'];
  					$menu['cicerone']['submenu'][] = $submenu;

  					$submenu = array();
  					$submenu['enlace'] = 'index.php?m=cicerone&accion=fm_proveedor_datos';
  					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_provider_datos'];
  					$menu['cicerone']['submenu'][] = $submenu;

  					$submenu = array();
  					$submenu['enlace'] = 'index.php?m=cicerone&accion=fm_provider_ofertas';
  					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_provider_ofertas'];
  					$menu['cicerone']['submenu'][] = $submenu;

  					$submenu = array();
  					$submenu['enlace'] = 'index.php?m=cicerone&accion=fm_provider_informes';
  					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_provider_informe'];
  					$menu['cicerone']['submenu'][] = $submenu;
                    break;

         case 'H'://Usuario Hotel
          if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 3)) //Funcionalidad 3 = PestaÃ±a Mensual
					{
                        $menu['inicio']['ubicacion'] = 'izq';
                        $menu['inicio']['posicion'] = 0;
                        $menu['inicio']['nombre'] = $idi_despliegue['menu_inicio'];

                        $menu['inicio']['submenu'] = array();

						if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 4)) //Funcionalidad 4 = Menu General
						{
							$submenu = array();
							$submenu['enlace'] = "index.php?m=reportes&accion=hotel_mes";
							$submenu['nombre'] = $idi_despliegue['general'];
              $menu['inicio']['submenu'][] = $submenu;
						}

						$submenu = array();
						$submenu['enlace'] = "index.php?m=familias&accion=familias_mensual";
						$submenu['nombre'] = $idi_despliegue['familias'];
						$menu['inicio']['submenu'][] = $submenu;


             /*DMG 2015-03-16 informe Importancia Valoracion*/
            if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 68)) //Funcionalidad 68 = Menu Informe / valoracion
            {
              $submenu = array();
              $submenu['enlace'] = "index.php?m=reportes&accion=importancia_valoracion";
             	$submenu['nombre'] = $idi_despliegue['importancia_valoracion'];
              $menu['inicio']['submenu'][] = $submenu;
            }//Fin de if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 4))

					}//Fin de if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 3))


                    $mostrar_evolucion = $_SESSION['usu_usc_id'] == 2 && $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 5);
                    $mostrar_evolucion |= $_SESSION['usu_usc_id'] == 1;

					//El usuario es de segunda categoria y tiene activa la funcionalidad se exhibe la pestaÃ±a
					if ( $mostrar_evolucion ) //Funcionalidad 5 = PestaÃ±a Evolucion
					{
                        $menu['evolucion']['ubicacion'] = 'izq';
                        $menu['evolucion']['posicion'] = 1;
                        $menu['evolucion']['nombre'] = $idi_despliegue['menu_evolucion'];
						$menu['evolucion']['enlace'] = "index.php?m=reportes&accion=evolucion";

    					$accionTooltip = '';
    					//Si el usuario no tiene activa la funcionalidad pero es de categoria Primario se desplegara el mensaje de contacto a Soporte
    					if ( $_SESSION['usu_usc_id'] == 1 && !$obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 5))
    					{
    						$mensajeContacto = $obj_usuarios->mensajeActivarFuncionalidad();
    						$menu['evolucion']['enlace'] = "javascript:void(0);";
    						$accionTooltip = " onClick='fixedtooltip(\"".$mensajeContacto."\",this, event, \"180px\")' onMouseout='delayhidetip()' ";
                            $menu['evolucion']['tooltip'] = $accionTooltip;
    					}
					}


					if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 7)) //Funcionalidad 7 = PestaÃ±a Comentarios
					{
						//MAOH - 27 Ago 2012 - Incidencia 1803: xml para pasar comentarios de clientes a la pagina web del hotel
                        $menu['comentarios']['ubicacion'] = 'izq';
                        $menu['comentarios']['posicion'] = 2;
                        $menu['comentarios']['nombre'] = $idi_despliegue['menu_comentarios'];

						$menu['comentarios']['submenu'] = array();

						$submenu = array();
						$submenu['enlace'] = "index.php?m=comentarios&accion=ver_comentarios";
						$submenu['nombre'] = $idi_despliegue['menu_ver_comentarios'];
						$menu['comentarios']['submenu'][] = $submenu;

						$submenu = array();
						$submenu['enlace'] = "index.php?m=comentarios&accion=publicar_comentarios";
						$submenu['nombre'] = $idi_despliegue['menu_publicar_comentarios'];
						$menu['comentarios']['submenu'][] = $submenu;
						//MAOH - 27 Ago 2012 - Fin
					}

					//Nuevo Elemento de Menu desde Julio 8 de 2011
					if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 6)) //Funcionalidad 6 = PestaÃ±a Perfil
					{
                        $menu['perfil']['ubicacion'] = 'izq';
                        $menu['perfil']['posicion'] = 4;
                        $menu['perfil']['nombre'] = $idi_despliegue['menu_perfil'];
						$menu['perfil']['enlace'] = "index.php?m=reportes&accion=ver_perfil";
					}


					if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 11)) //Funcionalidad 11 = PestaÃ±a Respuestas
					{
                        $menu['respuestas']['ubicacion'] = 'izq';
                        $menu['respuestas']['posicion'] = 5;
                        $menu['respuestas']['nombre'] = $idi_despliegue['menu_respuestas'];

						$accionTooltip = '';
						//Si el usuario no tiene activa la funcionalidad pero es de categoria Primario se desplegara el mensaje de contacto a Soporte
						if (!$obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 12))
						{
							$mensajeContacto = $obj_usuarios->mensajeActivarFuncionalidad();
							$menu['respuestas']['enlace'] = "javascript:void(0);";
							$accionTooltip = " onClick='fixedtooltip(\"".$mensajeContacto."\",this, event, \"180px\")' onMouseout='delayhidetip()' ";
                            $menu['respuestas']['tooltip'] = $accionTooltip;
						}
                        else
                        {
                            $menu['respuestas']['submenu'] = array();

							$submenu = array();
							$submenu[enlace] = "index.php?m=reportes&accion=respuestas";
							$submenu[nombre] = $idi_despliegue[menu_encuestas];
                            $menu[respuestas][submenu][] = $submenu;

							if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 14)) //Funcionalidad 14 = Destinatarios
							{
								$submenu = array();
								$submenu[enlace] = "index.php?m=auxiliar&accion=ver_destinatarios";
								$submenu[nombre] = $idi_despliegue[menu_destinatarios];
                                $menu[respuestas][submenu][] = $submenu;
							}

                            //DM - 2012-11-20
                            //Si tiene activa la funcionalidad de despedida (29)
                            //y compartir en redes sociales (54) o
                            //integracion con tripadvisor (55)
                            //le permite el acceso
							if( $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 29)
                                && (
                                $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 54)
                                ||
                                $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 55)
                                )
                            )
							{
								$submenu = array();
								$submenu[enlace] = "index.php?m=auxiliar&accion=ver_click_performance";
								$submenu[nombre] = $idi_despliegue['menu_click_performance'];
                                $menu[respuestas][submenu][] = $submenu;
							}

                        }//Fin de else if (!$obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 12))
					}


					if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 15)) //Funcionalidad 15 = PestaÃ±a Suscriptores
					{
                        $menu['suscriptores']['ubicacion'] = 'izq';
                        $menu['suscriptores']['posicion'] = 6;
                        $menu['suscriptores']['nombre'] = $idi_despliegue['menu_vs'];
						$menu['suscriptores']['enlace'] = "index.php?m=establecimientos&accion=fm_vs";
					}

					if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 16)) //Funcionalidad 16 = PestaÃ±a Reputacion
					{
                        //DM - 2015-01-15 issue 5870
                        $menu['reputacion']['ubicacion'] = 'izq';
                        $menu['reputacion']['posicion'] = 7;
                        $menu['reputacion']['nombre'] = $idi_despliegue['menu_reputacion'];
						$menu['reputacion']['enlace'] = "index.php?m=reputacion&accion=iframe";
					}

                    $funcionalidad_provider = $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 66);
                    $funcionalidad_store = $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 67);

                    if ($funcionalidad_provider || $funcionalidad_store)
                    {
        		    	//DM - 2014-01-16 issue 3778
                        $menu['cicerone']['ubicacion'] = 'izq';
                        $menu['cicerone']['posicion'] = 8;
                        $menu['cicerone']['nombre'] = $idi_despliegue['menu_cicerone'];
    					$menu['cicerone']['enlace'] = '';

                        $menu['cicerone']['submenu'] = array();

                        if( $funcionalidad_store )
                        {
          					$submenu = array();
          					$submenu['enlace'] = '';
          					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_store'];
          					$menu['cicerone']['submenu'][] = $submenu;

          					$submenu = array();
          					$submenu['enlace'] = 'index.php?m=cicerone&accion=fm_store_datos';
          					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_store_datos'];
          					$menu['cicerone']['submenu'][] = $submenu;

                            $submenu = array();
          					$submenu['enlace'] = 'index.php?m=cicerone&accion=fm_store_proponer_proveedor';
          					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_store_proponer_proveedor'];
          					$menu['cicerone']['submenu'][] = $submenu;

                            $submenu = array();
          					$submenu['enlace'] = 'index.php?m=cicerone&accion=fm_store_subir_proveedor';
          					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_store_subir_proveedor'];
          					$menu['cicerone']['submenu'][] = $submenu;

                            $submenu = array();
          					$submenu['enlace'] = 'index.php?m=cicerone&accion=fm_store_seleccion_ofertas';
          					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_store_seleccion_ofertas'];
          					$menu['cicerone']['submenu'][] = $submenu;

                            $submenu = array();
          					$submenu['enlace'] = 'index.php?m=cicerone&accion=fm_store_informes';
          					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_store_informes'];
          					$menu['cicerone']['submenu'][] = $submenu;

                            $submenu = array();
          					$submenu['enlace'] = 'index.php?m=cicerone&accion=destinatarios_store';
          					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_destinatarios'];
          					$menu['cicerone']['submenu'][] = $submenu;
                        }//Fin de if( $funcionalidad_store )

                        if( $funcionalidad_provider )
                        {
        					$submenu = array();
        					$submenu['enlace'] = '';
        					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_provider'];
        					$menu['cicerone']['submenu'][] = $submenu;

        					$submenu = array();
        					$submenu['enlace'] = 'index.php?m=cicerone&accion=fm_proveedor_datos';
        					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_provider_datos'];
        					$menu['cicerone']['submenu'][] = $submenu;

        					$submenu = array();
        					$submenu['enlace'] = 'index.php?m=cicerone&accion=fm_provider_ofertas';
        					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_provider_ofertas'];
        					$menu['cicerone']['submenu'][] = $submenu;

        					$submenu = array();
        					$submenu['enlace'] = 'index.php?m=cicerone&accion=fm_provider_informes';
        					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_provider_informe'];
        					$menu['cicerone']['submenu'][] = $submenu;



                        }//Fin de if( $funcionalidad_provider )


                    }//Fin de if ($funcionalidad_provider || $funcionalidad_store)


                    //opciones del menu de la parte derecha

					if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 32)) //Funcionalidad 32 = PestaÃ±a Auxiliar
					{
                        $menu['auxiliar']['ubicacion'] = 'der';
                        $menu['auxiliar']['posicion'] = 0;
                        $menu['auxiliar']['nombre'] = $idi_despliegue['menu_auxiliar'];

						$menu['auxiliar']['submenu'] = array();

                        $submenu = array();
                        $submenu['enlace'] = "index.php?m=auxiliar&accion=fm_enviar";
                        $submenu['nombre'] = $idi_despliegue['menu_enviar'];
                        $menu['auxiliar']['submenu'][] = $submenu;

						//El usuario es de segunda categoria y tiene activa la funcionalidad Grabar, se exhibe el submenu
						if ( $_SESSION['usu_usc_id'] == 2 && $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 34)) //Funcionalidad 34 = Grabar
						{
							$submenu = array();
							$submenu['enlace'] = "index.php?m=desplieguecuestionario&accion=fm_grabar";
							$submenu['nombre'] = $idi_despliegue['menu_grabar'];
                            $menu['auxiliar']['submenu'][] = $submenu;
						}
						else if ( $_SESSION['usu_usc_id'] == 1) //El usuario es de primera categoria, siempre vera el submenu Grabar
						{
							$submenu = array();
							$submenu['enlace'] = "index.php?m=desplieguecuestionario&accion=fm_grabar";
							$submenu['nombre'] = $idi_despliegue['menu_grabar'];
                            $menu['auxiliar']['submenu'][] = $submenu;
						}

						$submenu = array();
						$submenu['enlace'] = "index.php?m=auxiliar&accion=fm_salidas";
						$submenu['nombre'] = $idi_despliegue['menu_salidas'];
                        $menu['auxiliar']['submenu'][] = $submenu;

						$submenu = array();
						$submenu['enlace'] = "index.php?m=cargadatos&accion=corregir_datos";
						$idi_despliegue[' ']="Corregir datos";
						$submenu['nombre'] = $idi_despliegue['menu_corregir'];
                        $menu['auxiliar']['submenu'][] = $submenu;

					}//Fin de if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 32))

                    if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 17)) //Funcionalidad 17 = PestaÃ±a configuracion
                    {

                        $menu['configuracion']['ubicacion'] = 'der';
                        $menu['configuracion']['posicion'] = 1;
                        $menu['configuracion']['nombre'] = $idi_despliegue['menu_configuracion'];
						$menu['configuracion']['enlace'] = '';

						$menu['configuracion']['submenu'] = array();

						if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 18)) //Funcionalidad 18 = Cuestionario
						{
							$submenu = array();
							$submenu[enlace] = "index.php?m=cuestionario&accion=ver_cuestionario";
							$submenu[nombre] = $idi_despliegue['menu_conf_cuest'];
							$menu[configuracion][submenu][] = $submenu;
						}

						$submenu = array();
						$submenu[enlace] = "index.php?m=datoshotel&accion=fm_datoshotel";
						$submenu[nombre] = $idi_despliegue['menu_datos_hotel'];
						$menu[configuracion][submenu][] = $submenu;



                        //El usuario es de segunda categoria y tiene activa la funcionalidad Plantilla Email, se exhibe el submenu
                        $acceso_plantillas = $_SESSION['usu_usc_id'] == 2 && $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 25);
                        //El usuario es de primera categoria, siempre vera el submenu Plantilla Email
                        $acceso_plantillas |= $_SESSION['usu_usc_id'] == 1;

                        //El usuario es de segunda categoria y tiene activa la funcionalidad Despedida, se exhibe el submenu
                        $acceso_plantillas != ( $_SESSION['usu_usc_id'] == 2 && $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 29) );

                        //El usuario es de segunda categoria y tiene activa la funcionalidad email de agradecimiento, se exhibe el submenu
                        $acceso_plantillas != ( $_SESSION['usu_usc_id'] == 2 && $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 59) );

                        if( $acceso_plantillas )
                        {
                            $submenu = array();
							$submenu[enlace] = 'index.php?m=plantillascorreo&accion=fm_gestion_plantillas';
							$submenu[nombre] = $idi_despliegue['menu_gestion_plantillas'];
							$menu[configuracion][submenu][] = $submenu;
                        }//Fin de if( $acceso_plantillas )

                        //DM - 2014-11-18 issue 5576
                        if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 35))
                        {
    						$submenu = array();
    						$submenu[enlace] = "index.php?m=usuarios&accion=gestion";
    						$submenu[nombre] = $idi_despliegue['menu_gestion_usuarios'];
    						$menu[configuracion][submenu][] = $submenu;
                        }//Fin de if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 18))



                        //DM - 2013-10-28 issue 2494
						$submenu = array();
						$submenu[enlace] = "index.php?m=objetivos&accion=gestion";
						$submenu[nombre] = $idi_despliegue['menu_objetivos'];
						$menu[configuracion][submenu][] = $submenu;

                        //DM - 2014-11-18 issue 5576
                        if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 30))
                        {
    						$submenu = array();
      						$submenu[enlace] = "index.php?m=familias&accion=gestion";
  	     					$submenu[nombre] = $idi_despliegue['menu_gestion_informes'];
  		    				$menu[configuracion][submenu][] = $submenu;
                        }//Fin de if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 18))



				    	//MAOH - 11 Mayo 2012 - Submenu HQ Index
					    //Funcionalidad 57 => HQ Index
						if($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 57))
						{
							$submenu = array();
							$submenu[enlace] = "index.php?m=reportes&accion=obtener_widget";
							$submenu[nombre] = $idi_despliegue['hqi_menu_widget'];
							$menu[configuracion][submenu][] = $submenu;
						}//MAOH - 11 Mayo 2012 - Fin

          		    	//DM - 2013-08-12
          			    //Funcionalidad 61 - Recogida de datos
                        $funcionalidad_vinculos = $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 61);
          				if( $funcionalidad_vinculos )
          				{
          					$submenu = array();
                            $submenu[enlace] = "index.php?m=auxiliar&accion=fm_recogida_datos_vinculos";
          					$submenu[nombre] = $idi_despliegue['menu_recogida_datos'];
          					$menu[configuracion][submenu][] = $submenu;
          				}//Fin de if( $funcionalidad_vinculos || $funcionalidad_carrousel )

          		    	//DM - 2013-11-30
          			    //Funcionalidad 63 - Carrouseles
                        $funcionalidad_carrousel = $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 63 );
          				if( $funcionalidad_carrousel )
          				{
          					$submenu = array();
                            $submenu[enlace] = "index.php?m=carrouseles&accion=fm_recogida_datos_carrousel";
          					$submenu[nombre] = $idi_despliegue['menu_programar_carrousel'];
          					$menu[configuracion][submenu][] = $submenu;

          				}//Fin de if( $funcionalidad_carrousel )

						/*
						POR AHORA SE DESACTIVA LO DE COMPETITIVE SET DESACTIVADO MAYO 16/2011
						if( $_SESSION["est_cs"]==1 || $_SESSION["est_cs"]==0)
						{
								$submenu = array();
								$submenu[enlace] = "index.php?m=establecimientos&accion=fm_cs";
								$submenu[nombre] = $idi_despliegue['menu_cs'];
							$menu[configuracion][submenu][] = $submenu;
						}*/
                    }//Fin de tiene activa la funcionalidad 17 = PestaÃ±a Configuracion
					/** Fin Menu Configuracion **/

					break;

				case 'C'://Usuario cadena

					if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 3)) //Funcionalidad 3 = PestaÃ±a Mensual
					{

                        $menu['inicio']['ubicacion'] = 'izq';
                        $menu['inicio']['posicion'] = 0;
                        $menu['inicio']['nombre'] = $idi_despliegue['menu_inicio'];

                        $menu['inicio']['submenu'] = array();

						if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 4)) //Funcionalidad 4 = Menu General
						{
							$menu['inicio']['enlace'] = "index.php?m=reportes&accion=hotel_mes";

							$submenu = array();
							$submenu[enlace] = "index.php?m=reportes&accion=hotel_mes";
							$submenu[nombre] = $idi_despliegue['general'];
                            $menu[inicio][submenu][] = $submenu;
						}

						$submenu = array();
						$submenu[enlace] = "index.php?m=familias&accion=familias_mensual";
						$submenu[nombre] = $idi_despliegue['familias'];
						$menu[inicio][submenu][] = $submenu;

						$submenu = array();
						$submenu[enlace] = "index.php?m=reportes&accion=cadena_tabular";
						$submenu[nombre] = $idi_despliegue['menu_mensual_cadena'];
						$menu[inicio][submenu][] = $submenu;

             /*DMG 2015-03-16 informe Importancia Valoracion*/

              $submenu = array();
              $submenu['enlace'] = "index.php?m=reportes&accion=importancia_valoracion";
             	$submenu['nombre'] = $idi_despliegue['importancia_valoracion'];
              $menu['inicio']['submenu'][] = $submenu;

					}//Fin de if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 3))


                    $mostrar_evolucion = $_SESSION['usu_usc_id'] == 2 && $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 5);
                    $mostrar_evolucion |= $_SESSION['usu_usc_id'] == 1;

					//El usuario es de segunda categoria y tiene activa la funcionalidad se exhibe la pestaÃ±a
					if ( $mostrar_evolucion ) //Funcionalidad 5 = PestaÃ±a Evolucion
					{
                        $menu['evolucion']['ubicacion'] = 'izq';
                        $menu['evolucion']['posicion'] = 1;
                        $menu['evolucion']['nombre'] = $idi_despliegue['menu_evolucion'];
						$menu['evolucion']['enlace'] = "index.php?m=reportes&accion=evolucion";

    					$accionTooltip = '';
    					//Si el usuario no tiene activa la funcionalidad pero es de categoria Primario se desplegara el mensaje de contacto a Soporte
    					if ( $_SESSION['usu_usc_id'] == 1 && !$obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 5))
    					{
    						$mensajeContacto = $obj_usuarios->mensajeActivarFuncionalidad();
    						$menu['evolucion']['enlace'] = "javascript:void(0);";
    						$accionTooltip = " onClick='fixedtooltip(\"".$mensajeContacto."\",this, event, \"180px\")' onMouseout='delayhidetip()' ";
                            $menu['evolucion']['tooltip'] = $accionTooltip;
    					}
					}

					if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 7)) //Funcionalidad 7 = PestaÃ±a Comentarios
					{
						//MAOH - 27 Ago 2012 - Incidencia 1803: xml para pasar comentarios de clientes a la pagina web del hotel
                        $menu['comentarios']['ubicacion'] = 'izq';
                        $menu['comentarios']['posicion'] = 3;
                        $menu['comentarios']['nombre'] = $idi_despliegue['menu_comentarios'];
						$menu['comentarios']['enlace'] = "index.php?m=comentarios&accion=ver_comentarios";

						$menu['comentarios']['submenu'] = array();

						$submenu = array();
						$submenu['enlace'] = "index.php?m=comentarios&accion=ver_comentarios";
						$submenu['nombre'] = $idi_despliegue['menu_ver_comentarios'];
						$menu['comentarios']['submenu'][] = $submenu;

						$submenu = array();
						$submenu['enlace'] = "index.php?m=comentarios&accion=publicar_comentarios";
						$submenu['nombre'] = $idi_despliegue['menu_publicar_comentarios'];
						$menu['comentarios']['submenu'][] = $submenu;
						//MAOH - 27 Ago 2012 - Fin
					}

					//Nuevo Elemento de Menu desde Julio 8 de 2011
					if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 6)) //Funcionalidad 6 = PestaÃ±a Perfil
					{
                        $menu['perfil']['ubicacion'] = 'izq';
                        $menu['perfil']['posicion'] = 4;
                        $menu['perfil']['nombre'] = $idi_despliegue['menu_perfil'];
						$menu['perfil']['enlace'] = "index.php?m=reportes&accion=ver_perfil";
					}


					if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 11)) //Funcionalidad 11 = PestaÃ±a Respuestas
					{
                        $menu['respuestas']['ubicacion'] = 'izq';
                        $menu['respuestas']['posicion'] = 5;
                        $menu['respuestas']['nombre'] = $idi_despliegue['menu_respuestas'];
						$menu['respuestas']['enlace'] = "index.php?m=reportes&accion=respuestas";

						$accionTooltip = '';
						//Si el usuario no tiene activa la funcionalidad pero es de categoria Primario se desplegara el mensaje de contacto a Soporte
						if (!$obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 12))
						{
							$mensajeContacto = $obj_usuarios->mensajeActivarFuncionalidad();
							$menu['respuestas']['enlace'] = "javascript:void(0);";
							$accionTooltip = " onClick='fixedtooltip(\"".$mensajeContacto."\",this, event, \"180px\")' onMouseout='delayhidetip()' ";
                            $menu['respuestas']['tooltip'] = $accionTooltip;
						}
                        else
                        {
                            $menu['respuestas']['submenu'] = array();

							$submenu = array();
							$submenu[enlace] = "index.php?m=reportes&accion=respuestas";
							$submenu[nombre] = $idi_despliegue[menu_encuestas];
                            $menu[respuestas][submenu][] = $submenu;

							if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 14)) //Funcionalidad 14 = Destinatarios
							{
								$submenu = array();
								$submenu[enlace] = "index.php?m=auxiliar&accion=ver_destinatarios";
								$submenu[nombre] = $idi_despliegue[menu_destinatarios];
                                $menu[respuestas][submenu][] = $submenu;
							}

                            //DM - 2012-11-20
                            //Si tiene activa la funcionalidad de despedida (29)
                            //y compartir en redes sociales (54) o
                            //integracion con tripadvisor (55)
                            //le permite el acceso
							if( $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 29)
                                && (
                                $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 54)
                                ||
                                $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 55)
                                )
                            )
							{
								$submenu = array();
								$submenu[enlace] = "index.php?m=auxiliar&accion=ver_click_performance";
								$submenu[nombre] = $idi_despliegue['menu_click_performance'];
                                $menu[respuestas][submenu][] = $submenu;
							}

                        }//Fin de else if (!$obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 12))
					}

                    $funcionalidad_provider = $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 66);
                    $funcionalidad_store = $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 67);

                    if ($funcionalidad_provider || $funcionalidad_store)
                    {
        		    	//DM - 2014-01-16 issue 3778
                        $menu['cicerone']['ubicacion'] = 'izq';
                        $menu['cicerone']['posicion'] = 8;
                        $menu['cicerone']['nombre'] = $idi_despliegue['menu_cicerone'];
    					$menu['cicerone']['enlace'] = '';

                        $menu['cicerone']['submenu'] = array();

                        if( $funcionalidad_store )
                        {
          					$submenu = array();
          					$submenu['enlace'] = '';
          					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_store'];
          					$menu['cicerone']['submenu'][] = $submenu;

          					$submenu = array();
          					$submenu['enlace'] = 'index.php?m=cicerone&accion=fm_store_datos';
          					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_store_datos'];
          					$menu['cicerone']['submenu'][] = $submenu;

                            $submenu = array();
          					$submenu['enlace'] = 'index.php?m=cicerone&accion=fm_store_proponer_proveedor';
          					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_store_proponer_proveedor'];
          					$menu['cicerone']['submenu'][] = $submenu;

                            $submenu = array();
          					$submenu['enlace'] = 'index.php?m=cicerone&accion=fm_store_subir_proveedor';
          					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_store_subir_proveedor'];
          					$menu['cicerone']['submenu'][] = $submenu;

                            $submenu = array();
          					$submenu['enlace'] = 'index.php?m=cicerone&accion=fm_store_seleccion_ofertas';
          					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_store_seleccion_ofertas'];
          					$menu['cicerone']['submenu'][] = $submenu;

                            $submenu = array();
          					$submenu['enlace'] = 'index.php?m=cicerone&accion=fm_store_informes';
          					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_store_informes'];
          					$menu['cicerone']['submenu'][] = $submenu;
                        }//Fin de if( $funcionalidad_store )

                        if( $funcionalidad_provider )
                        {
        					$submenu = array();
        					$submenu['enlace'] = '';
        					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_provider'];
        					$menu['cicerone']['submenu'][] = $submenu;

        					$submenu = array();
        					$submenu['enlace'] = 'index.php?m=cicerone&accion=fm_proveedor_datos';
        					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_provider_datos'];
        					$menu['cicerone']['submenu'][] = $submenu;

        					$submenu = array();
        					$submenu['enlace'] = 'index.php?m=cicerone&accion=fm_provider_ofertas';
        					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_provider_ofertas'];
        					$menu['cicerone']['submenu'][] = $submenu;

        					$submenu = array();
        					$submenu['enlace'] = 'index.php?m=cicerone&accion=fm_provider_informes';
        					$submenu['nombre'] = $idi_despliegue['submenu_cicerone_provider_informe'];
        					$menu['cicerone']['submenu'][] = $submenu;
                        }//Fin de if( $funcionalidad_provider )


                    }//Fin de if ($funcionalidad_provider || $funcionalidad_store)


                    //opciones de la parte derecha del menu


                    $menu['auxiliar']['ubicacion'] = 'der';
                    $menu['auxiliar']['posicion'] = 0;
                    $menu['auxiliar']['nombre'] = $idi_despliegue['menu_corregir'];
                    $menu['auxiliar']['enlace'] = 'index.php?m=cargadatos&accion=corregir_datos';


					//Esta autorizado a acceder a la configuracion de la cadena
					if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 17)) //Funcionalidad 17 = Pestaña Configuracion
                    {

                        $menu['configuracion']['ubicacion'] = 'der';
                        $menu['configuracion']['posicion'] = 1;
                        $menu['configuracion']['nombre'] = $idi_despliegue['menu_configuracion'];
						$menu['configuracion']['enlace'] = '';

						$menu['configuracion']['submenu'] = array();

                        if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 18)) //Funcionalidad 18 = Cuestionario
						{
							$menu[configuracion][enlace] = "index.php?m=cuestionario&accion=ver_cuestionario";

							$submenu = array();
							$submenu[enlace] = "index.php?m=cuestionario&accion=ver_cuestionario";
							$submenu[nombre] = $idi_despliegue['menu_conf_cuest'];
                            $menu[configuracion][submenu][] = $submenu;
						}

						$submenu = array();
						$submenu[enlace] = "index.php?m=datoshotel&accion=fm_datoshotel";
						$submenu[nombre] = $idi_despliegue['menu_datos_hotel'];
						$menu[configuracion][submenu][] = $submenu;

						$submenu = array();
						$submenu[enlace] = "index.php?m=cadenas&accion=fm_datoscadena";
						$submenu[nombre] = $idi_despliegue['menu_datos_cadena'];
						$menu[configuracion][submenu][] = $submenu;

                        //DM - 2014-11-18 issue 5576
                        if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 35))
                        {
    						$submenu = array();
    						$submenu[enlace] = "index.php?m=usuarios&accion=gestion";
    						$submenu[nombre] = $idi_despliegue['menu_gestion_usuarios'];
    						$menu[configuracion][submenu][] = $submenu;
                        }//Fin de if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 18))

                        //El usuario es de segunda categoria y tiene activa la funcionalidad Plantilla Email, se exhibe el submenu
                        $acceso_plantillas = $_SESSION['usu_usc_id'] == 2 && $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 25);
                        //El usuario es de primera categoria, siempre vera el submenu Plantilla Email
                        $acceso_plantillas |= $_SESSION['usu_usc_id'] == 1;

                        //El usuario es de segunda categoria y tiene activa la funcionalidad Despedida, se exhibe el submenu
                        $acceso_plantillas != ( $_SESSION['usu_usc_id'] == 2 && $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 29) );

                        //El usuario es de segunda categoria y tiene activa la funcionalidad email de agradecimiento, se exhibe el submenu
                        $acceso_plantillas != ( $_SESSION['usu_usc_id'] == 2 && $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 59) );

                        if( $acceso_plantillas )
                        {
                            $submenu = array();
							$submenu[enlace] = 'index.php?m=plantillascorreo&accion=fm_gestion_plantillas';
							$submenu[nombre] = $idi_despliegue['menu_gestion_plantillas'];
							$menu[configuracion][submenu][] = $submenu;
                        }//Fin de if( $acceso_plantillas )



                        //DM - 2014-04-08 issue 3474
						$submenu = array();
						$submenu[enlace] = "index.php?m=objetivos&accion=gestion";
						$submenu[nombre] = $idi_despliegue['menu_objetivos'];
						$menu[configuracion][submenu][] = $submenu;

                        //DM - 2014-11-18 issue 5576
                        if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 30))
                        {
    						$submenu = array();
      						$submenu[enlace] = "index.php?m=familias&accion=gestion";
  	     					$submenu[nombre] = $idi_despliegue['menu_gestion_informes'];
  		    				$menu[configuracion][submenu][] = $submenu;
                        }//Fin de if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 18))


					    //MAOH - 11 Mayo 2012 - Submenu HQ Index
					    //Funcionalidad 57 => HQ Index
						if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 57))
						{
							$submenu = array();
							$submenu[enlace] = "index.php?m=reportes&accion=obtener_widget";
							$submenu[nombre] = $idi_despliegue['hqi_menu_widget'];
							$menu[configuracion][submenu][] = $submenu;
						}//MAOH - 11 Mayo 2012 - Fin


          		    	//DM - 2013-08-12
          			    //Funcionalidad 61 - Recogida de datos
                        $funcionalidad_vinculos = $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 61);
          				if( $funcionalidad_vinculos )
          				{
          					$submenu = array();
                            $submenu[enlace] = "index.php?m=auxiliar&accion=fm_recogida_datos_vinculos";
          					$submenu[nombre] = $idi_despliegue['menu_recogida_datos'];
          					$menu[configuracion][submenu][] = $submenu;
          				}//Fin de if( $funcionalidad_vinculos || $funcionalidad_carrousel )

          		    	//DM - 2013-11-30
          			    //Funcionalidad 63 - Carrouseles
                        $funcionalidad_carrousel = $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 63 );
          				if( $funcionalidad_carrousel )
          				{
          					$submenu = array();
                            $submenu[enlace] = "index.php?m=carrouseles&accion=fm_recogida_datos_carrousel";
          					$submenu[nombre] = $idi_despliegue['menu_programar_carrousel'];
          					$menu[configuracion][submenu][] = $submenu;

          				}//Fin de if( $funcionalidad_carrousel )

					}//Fin de tiene activa la funcionalidad de la configuracion
					break;//Fin usuario Cadena

					case 'AD'://Usuario administrador = Patronato-------------------------------------------------------------------------------------------------------------------------------

						$menu[inicio][enlace] = "index.php?m=reportes&accion=hotel_mes";
						$menu[evolucion][enlace] = "index.php?m=reportes&accion=evolucion";
						$menu[comentarios][enlace] = "index.php?m=comentarios&accion=ver_comentarios";
						$menu[respuestas][enlace] = "index.php?m=reportes&accion=respuestas";
						$menu[respuestas][submenu] = array();
							$submenu = array();
							$submenu[enlace] = "index.php?m=reportes&accion=respuestas";
							$submenu[nombre] = $idi_despliegue[menu_encuestas];
						$menu[respuestas][submenu][] = $submenu;
							$submenu = array();
							$submenu[enlace] = "index.php?m=auxiliar&accion=ver_destinatarios";
							$submenu[nombre] = $idi_despliegue[menu_destinatarios];
						$menu[respuestas][submenu][] = $submenu;

						break;

					case 'AM'://Usuario aehm ------------------------------------------ OBSOLETO DESDE SEPTIEMBRE 19 DE 2011 -------------------------------------------------------------------

						$menu[inicio][enlace] = "index.php?m=reportes&accion=hotel_mes";
							$submenu = array();
							$submenu[enlace] = "index.php?m=reportes&accion=hotel_mes";
							$submenu[nombre] = $idi_despliegue['general'];
						$menu[inicio][submenu][] = $submenu;
							$submenu = array();
							$submenu[enlace] = "index.php?m=familias&accion=familias_mensual";
							$submenu[nombre] = $idi_despliegue['familias'];
						$menu[inicio][submenu][] = $submenu;
						$menu[evolucion][enlace] = "index.php?m=reportes&accion=evolucion";
						$menu[comentarios][enlace] = "index.php?m=comentarios&accion=ver_comentarios";
						$menu[respuestas][enlace] = "index.php?m=reportes&accion=respuestas";
						$menu[respuestas][submenu] = array();
							$submenu = array();
							$submenu[enlace] = "index.php?m=reportes&accion=respuestas";
							$submenu[nombre] = $idi_despliegue[menu_encuestas];
						$menu[respuestas][submenu][] = $submenu;
							$submenu = array();
							$submenu[enlace] = "index.php?m=auxiliar&accion=ver_destinatarios";
							$submenu[nombre] = $idi_despliegue[menu_destinatarios];
						$menu[respuestas][submenu][] = $submenu;
						$menu[configuracion][enlace] = "index.php?m=cuestionario&accion=ver_cuestionario";
						$menu[configuracion][submenu] = array();
							$submenu = array();
							$submenu[enlace] = "index.php?m=cuestionario&accion=ver_cuestionario";
							$submenu[nombre] = $idi_despliegue['menu_conf_cuest'];
						$menu[configuracion][submenu][] = $submenu;
							$submenu = array();
							$submenu[enlace] = "index.php?m=datoshotel&accion=fm_datoshotel";
							$submenu[nombre] = $idi_despliegue['menu_datos_hotel'];
						$menu[configuracion][submenu][] = $submenu;
							$submenu = array();
							$submenu[enlace] = "index.php?m=auxiliar&accion=fm_envios";
							$submenu[nombre] = $idi_despliegue['menu_plantilla_email'];
						$menu[configuracion][submenu][] = $submenu;
							$submenu = array();
							$submenu[enlace] = "index.php?m=familias&accion=gestion";
							$submenu[nombre] = $idi_despliegue['menu_gestion_informes'];
						$menu[configuracion][submenu][] = $submenu;
							$submenu = array();
							$submenu[enlace] = "index.php?m=auxiliar&accion=fm_despedida";
							$submenu[nombre] = $idi_despliegue['menu_despedida'];
						$menu[configuracion][submenu][] = $submenu;

						break;

					case 'AU'://Usuario auxiliar

    					if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 32)) //Funcionalidad 32 = PestaÃ±a Auxiliar
    					{
                            $menu['auxiliar']['ubicacion'] = 'der';
                            $menu['auxiliar']['nombre'] = $idi_despliegue['menu_auxiliar'];
    						$menu['auxiliar']['enlace'] = 'index.php?m=auxiliar&accion=fm_enviar';

    						$menu['auxiliar']['submenu'] = array();

                            $submenu = array();
                            $submenu['enlace'] = "index.php?m=auxiliar&accion=fm_enviar";
                            $submenu['nombre'] = $idi_despliegue['menu_enviar'];
                            $menu['auxiliar']['submenu'][] = $submenu;

    						//El usuario es de segunda categoria y tiene activa la funcionalidad Grabar, se exhibe el submenu
    						if ( $_SESSION['usu_usc_id'] == 2 && $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 34)) //Funcionalidad 34 = Grabar
    						{
    							$submenu = array();
    							$submenu['enlace'] = "index.php?m=desplieguecuestionario&accion=fm_grabar";
    							$submenu['nombre'] = $idi_despliegue['menu_grabar'];
                                $menu['auxiliar']['submenu'][] = $submenu;
    						}
    						else if ( $_SESSION['usu_usc_id'] == 1) //El usuario es de primera categoria, siempre vera el submenu Grabar
    						{
    							$submenu = array();
    							$submenu['enlace'] = "index.php?m=desplieguecuestionario&accion=fm_grabar";
    							$submenu['nombre'] = $idi_despliegue['menu_grabar'];
                                $menu['auxiliar']['submenu'][] = $submenu;
    						}

    						$submenu = array();
    						$submenu['enlace'] = "index.php?m=auxiliar&accion=fm_salidas";
    						$submenu['nombre'] = $idi_despliegue['menu_salidas'];
                            $menu['auxiliar']['submenu'][] = $submenu;

    					}//Fin de if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 32))
                        break;

					case 'RP'://Usuario reportes ------------------------------------------ OBSOLETO DESDE SEPTIEMBRE 19 DE 2011 -------------------------------------------------------------------

                        if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 3)) //Funcionalidad 3 = PestaÃ±a Mensual
    					{
                            $menu['inicio']['ubicacion'] = 'izq';
                            $menu['inicio']['nombre'] = $idi_despliegue['menu_inicio'];

                            $menu['inicio']['submenu'] = array();

    						if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 4)) //Funcionalidad 4 = Menu General
    						{
    							$menu['inicio']['enlace'] = "index.php?m=reportes&accion=hotel_mes";

    							$submenu = array();
    							$submenu['enlace'] = "index.php?m=reportes&accion=hotel_mes";
    							$submenu['nombre'] = $idi_despliegue['general'];
                                $menu['inicio']['submenu'][] = $submenu;
    						}

    						$submenu = array();
    						$submenu['enlace'] = "index.php?m=familias&accion=familias_mensual";
    						$submenu['nombre'] = $idi_despliegue['familias'];
    						$menu['inicio']['submenu'][] = $submenu;

    					}//Fin de if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 3))


                        $mostrar_evolucion = $_SESSION['usu_usc_id'] == 2 && $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 5);
                        $mostrar_evolucion |= $_SESSION['usu_usc_id'] == 1;

    					//El usuario es de segunda categoria y tiene activa la funcionalidad se exhibe la pestaÃ±a
    					if ( $mostrar_evolucion ) //Funcionalidad 5 = PestaÃ±a Evolucion
    					{
                            $menu['evolucion']['ubicacion'] = 'izq';
                            $menu['evolucion']['nombre'] = $idi_despliegue['menu_evolucion'];
    						$menu['evolucion']['enlace'] = "index.php?m=reportes&accion=evolucion";

        					$accionTooltip = '';
        					//Si el usuario no tiene activa la funcionalidad pero es de categoria Primario se desplegara el mensaje de contacto a Soporte
        					if ( $_SESSION['usu_usc_id'] == 1 && !$obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 5))
        					{
        						$mensajeContacto = $obj_usuarios->mensajeActivarFuncionalidad();
        						$menu['evolucion']['enlace'] = "javascript:void(0);";
        						$accionTooltip = " onClick='fixedtooltip(\"".$mensajeContacto."\",this, event, \"180px\")' onMouseout='delayhidetip()' ";
                                $menu['evolucion']['tooltip'] = $accionTooltip;
        					}
    					}


    					if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 7)) //Funcionalidad 7 = PestaÃ±a Comentarios
    					{
    						//MAOH - 27 Ago 2012 - Incidencia 1803: xml para pasar comentarios de clientes a la pagina web del hotel
                            $menu['comentarios']['ubicacion'] = 'izq';
                            $menu['comentarios']['nombre'] = $idi_despliegue['menu_comentarios'];
    						$menu['comentarios']['enlace'] = "index.php?m=comentarios&accion=ver_comentarios";
    					}

    					//Nuevo Elemento de Menu desde Julio 8 de 2011
    					if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 6)) //Funcionalidad 6 = PestaÃ±a Perfil
    					{
                            $menu['perfil']['ubicacion'] = 'izq';
                            $menu['perfil']['nombre'] = $idi_despliegue['menu_perfil'];
    						$menu['perfil']['enlace'] = "index.php?m=reportes&accion=ver_perfil";
    					}


    					if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 11)) //Funcionalidad 11 = PestaÃ±a Respuestas
    					{
                            $menu['respuestas']['ubicacion'] = 'izq';
                            $menu['respuestas']['nombre'] = $idi_despliegue['menu_respuestas'];
    						$menu['respuestas']['enlace'] = "index.php?m=reportes&accion=respuestas";

    						$accionTooltip = '';
    						//Si el usuario no tiene activa la funcionalidad pero es de categoria Primario se desplegara el mensaje de contacto a Soporte
    						if (!$obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 12))
    						{
    							$mensajeContacto = $obj_usuarios->mensajeActivarFuncionalidad();
    							$menu['respuestas']['enlace'] = "javascript:void(0);";
    							$accionTooltip = " onClick='fixedtooltip(\"".$mensajeContacto."\",this, event, \"180px\")' onMouseout='delayhidetip()' ";
                                $menu['respuestas']['tooltip'] = $accionTooltip;
    						}
                            else
                            {
                                $menu['respuestas']['submenu'] = array();

    							$submenu = array();
    							$submenu[enlace] = "index.php?m=reportes&accion=respuestas";
    							$submenu[nombre] = $idi_despliegue[menu_encuestas];
                                $menu[respuestas][submenu][] = $submenu;

    							if ($obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 14)) //Funcionalidad 14 = Destinatarios
    							{
    								$submenu = array();
    								$submenu[enlace] = "index.php?m=auxiliar&accion=ver_destinatarios";
    								$submenu[nombre] = $idi_despliegue[menu_destinatarios];
                                    $menu[respuestas][submenu][] = $submenu;
    							}

                                //DM - 2012-11-20
                                //Si tiene activa la funcionalidad de despedida (29)
                                //y compartir en redes sociales (54) o
                                //integracion con tripadvisor (55)
                                //le permite el acceso
    							if( $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 29)
                                    && (
                                    $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 54)
                                    ||
                                    $obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 55)
                                    )
                                )
    							{
    								$submenu = array();
    								$submenu[enlace] = "index.php?m=auxiliar&accion=ver_click_performance";
    								$submenu[nombre] = $idi_despliegue['menu_click_performance'];
                                    $menu[respuestas][submenu][] = $submenu;
    							}

                            }//Fin de else if (!$obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 12))
    					}
						break;
				}//Fin de  switch($_SESSION["tipo_usuario"]) //Dependiendo del tipo de usuario se construye el menu

                $this->generarMenu($menu);

                return $this->contenido_menu;

        }//Fin de if( $_SESSION["autenticado"]==1)

        return $this->contenido_menu;
    }//Fin de crearMenu()


    /**
     * Funcion que se encarga de generar los items del submenu
     * DM - 2013-01-14
     **/
	function colocarOpcionesSubmenu($submenu,$id,$activo="0")
    {
        global $_PATH_IMAGENES, $idi_despliegue, $obj_usuarios;

        $contenido = "";

        if( is_array($submenu) && sizeof($submenu)>0 )
        {

            $clase_adicional = '';
            if( $id == 'con')
            {
                $clase_adicional = 'configuracion';
            }
            //$contenido = "<div class='submenu ".$clase_adicional."' id='sub_".$id."' onmouseover=\"menu_over_sub('".$id."','".$_PATH_IMAGENES."')\" onmouseout=\"menu_out_sub('".$id."','".$_PATH_IMAGENES."','".$activo."')\"><table border='0' cellspacing='0' cellpadding='0'>";
            $contenido = '<ul class="nav nav-second-level">';
            foreach($submenu as $val)
            {
				if ($id == 'ini' && $val['nombre'] == $idi_despliegue['familias']) //Estamos en el Menu Inicio y en el item Familias
				{
					$accionTooltip = '';
					//Si el usuario no tiene activa la funcionalidad se desplegara el mensaje de contacto
					if (!$obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 30))
					{
						$mensajeContacto = $obj_usuarios->mensajeActivarFuncionalidad();
						$val['enlace'] = "javascript:void(0);";
						$accionTooltip = " onClick='fixedtooltip(\"".$mensajeContacto."\",this, event, \"180px\")' onMouseout='delayhidetip()' ";
					}
					$contenido .= "
								<li ui-sref-active='active' ".$accionTooltip." ><a  href=\"".$val[enlace]."\">".$val[nombre]."</a></li>";
				}
				else if ($id == 'res' && $val['nombre'] == $idi_despliegue['menu_encuestas']) //Estamos en el Menu de respuestas y en el item Encuestas
				{
					$accionTooltip = '';
					//Si el usuario no tiene activa la funcionalidad se desplegara el mensaje de contacto
					if (!$obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 12) && $_SESSION['tipo_usuario'] != 'AD')
						{
							$mensajeContacto = $obj_usuarios->mensajeActivarFuncionalidad();
							$val['enlace'] = "javascript:void(0);";
							$accionTooltip = " onClick='fixedtooltip(\"".$mensajeContacto."\",this, event, \"180px\")' onMouseout='delayhidetip()' ";
						}
					$contenido .= "<li ui-sref-active='active' ".$accionTooltip." ><a  href=\"".$val[enlace]."\">".$val[nombre]."</a></li>";
				}
				else if ($id == 'con' && $val['nombre'] == $idi_despliegue['menu_plantilla_email']) //Estamos en el Menu de Configuracion y en el item Plantilla Email
				{
					$accionTooltip = '';
					//Si el usuario no tiene activa la funcionalidad desplegara el mensaje de contacto a Soporte
					if (!$obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 25))
						{
							$mensajeContacto = $obj_usuarios->mensajeActivarFuncionalidad();
							$val['enlace'] = "javascript:void(0);";
							$accionTooltip = " onClick='fixedtooltip(\"".$mensajeContacto."\",this, event, \"180px\")' onMouseout='delayhidetip()' ";
						}
					$contenido .= "<li ui-sref-active='active' ".$accionTooltip." ><a  href=\"".$val[enlace]."\">".$val[nombre]."</a></li>";
				}
				else if ($id == 'con' && $val['nombre'] == $idi_despliegue['menu_datos_cuestionario']) //Estamos en el Menu de Configuracion y en el item Borrado de Datos
				{
					$accionTooltip = '';
					//Si el usuario no tiene activa la funcionalidad desplegara el mensaje de contacto a Soporte
					if (!$obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 28))
						{
							$mensajeContacto = $obj_usuarios->mensajeActivarFuncionalidad();
							$val['enlace'] = "javascript:void(0);";
							$accionTooltip = " onClick='fixedtooltip(\"".$mensajeContacto."\",this, event, \"180px\")' onMouseout='delayhidetip()' ";
						}
					$contenido .= "<li ui-sref-active='active' ".$accionTooltip." ><a  href=\"".$val[enlace]."\">".$val[nombre]."</a></li>";
				}
				else if ($id == 'con' && $val['nombre'] == $idi_despliegue['menu_despedida']) //Estamos en el Menu de Configuracion y en el item Despedida
				{
					$accionTooltip = '';
					//Si el usuario no tiene activa la funcionalidad desplegara el mensaje de contacto a Soporte
					if (!$obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 29))
						{
							$mensajeContacto = $obj_usuarios->mensajeActivarFuncionalidad();
							$val['enlace'] = "javascript:void(0);";
							$accionTooltip = " onClick='fixedtooltip(\"".$mensajeContacto."\",this, event, \"180px\")' onMouseout='delayhidetip()' ";
						}
					$contenido .= "<li ui-sref-active='active' ".$accionTooltip." ><a  href=\"".$val[enlace]."\">".$val[nombre]."</a></li>";
				}
				else if ($id == 'con' && $val['nombre'] == $idi_despliegue['menu_gestion_informes']) //Estamos en el Menu de Configuracion y en el item Gestion de Informes
				{
					$accionTooltip = '';
					//Si el usuario no tiene activa la funcionalidad desplegara el mensaje de contacto a Soporte
					if (!$obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 30))
						{
							$mensajeContacto = $obj_usuarios->mensajeActivarFuncionalidad();
							$val['enlace'] = "javascript:void(0);";
							$accionTooltip = " onClick='fixedtooltip(\"".$mensajeContacto."\",this, event, \"180px\")' onMouseout='delayhidetip()' ";
						}
					$contenido .= "<li ui-sref-active='active' ".$accionTooltip." ><a  href=\"".$val[enlace]."\">".$val[nombre]."</a></li>";
				}
				else if ($id == 'aux' && $val['nombre'] == $idi_despliegue['menu_grabar']) //Estamos en el Menu Auxiliar y en el item Grabar
				{
					$accionTooltip = '';
					//Si el usuario no tiene activa la funcionalidad desplegara el mensaje de contacto a Soporte
					if (!$obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 34))
						{
							$mensajeContacto = $obj_usuarios->mensajeActivarFuncionalidad();
							$val['enlace'] = "javascript:void(0);";
							$accionTooltip = " onClick='fixedtooltip(\"".$mensajeContacto."\",this, event, \"180px\")' onMouseout='delayhidetip()' ";
						}
					$contenido .= "<li ui-sref-active='active' ".$accionTooltip." ><a  href=\"".$val[enlace]."\">".$val[nombre]."</a></li>";
				}
				// RGC - BPM
				else if ($id == 'bpm' && $val['nombre'] == $idi_despliegue['menu_root_bpm_breakeven']) //Estamos en el Menu de BPM y en el item break even
				{
					$accionTooltip = '';
					//Si el usuario no tiene activa la funcionalidad desplegara el mensaje de contacto a Soporte
					if (!$obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 53))
						{
							$mensajeContacto = $obj_usuarios->mensajeActivarFuncionalidad();
							$val['enlace'] = "javascript:void(0);";
							$accionTooltip = " onClick='fixedtooltip(\"".$mensajeContacto."\",this, event, \"180px\")' onMouseout='delayhidetip()' ";
						}
					$contenido .= "<li ui-sref-active='active' ".$accionTooltip." ><a  href=\"".$val[enlace]."\">".$val[nombre]."</a></li>";
				}
				//MAOH - 19 Jun 2012 - Incidencia 1700 - Email de agradecimiento
				else if ($id == 'con' && $val['nombre'] == $idi_despliegue['menu_email_agradecimiento'])
				{
					$accionTooltip = '';

					//Si el usuario no tiene activa la funcionalidad desplegara el mensaje de contacto a Soporte
					if (!$obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 59)) //Funcionalidad 59 => Enail de agradecimiento
					{
						$mensajeContacto = $obj_usuarios->mensajeActivarFuncionalidad();
						$val['enlace'] = "javascript:void(0);";
						$accionTooltip = " onClick='fixedtooltip(\"".$mensajeContacto."\",this, event, \"180px\")' onMouseout='delayhidetip()' ";
					}

					$contenido .= "<li ui-sref-active='active' ".$accionTooltip."><a href=\"".$val['enlace']."\">".$val['nombre']."</a></li>";
				}
				//MAOH - 19 Jun 2012 - Fin

				//MAOH - 27 Ago 2012 - Incidencia 1803: xml para pasar comentarios de clientes a la pagina web del hotel
				else if ($id == 'com' && $val['nombre'] == $idi_despliegue['menu_publicar_comentarios'])
				{
					$accionTooltip = '';

					//Si el usuario no tiene activa la funcionalidad desplegara el mensaje de contacto a Soporte
					if (!$obj_usuarios->verificarFuncionalidadActiva($_SESSION['usu_id'], 60))
					{
						$mensajeContacto = $obj_usuarios->mensajeActivarFuncionalidad();
						$val['enlace'] = "javascript:void(0);";
						$accionTooltip = " onClick='fixedtooltip(\"".$mensajeContacto."\",this, event, \"180px\")' onMouseout='delayhidetip()' ";
					}

					$contenido .= "<li ui-sref-active='active' ".$accionTooltip."><a href=\"".$val['enlace']."\">".$val['nombre']."</a></li>";
				}
                else if( $val['tooltip'] != '' )
                {
                    //si existe un tooltip, quiere decir que el enlace no dede estar activo
                    //y por tanto no se muestra
                    $contenido .= "<li ui-sref-active='active' ".$val['tooltip']." ><a href=\"#\">".$val['nombre']."</a></li>";
                }//Fin de else if( $val['tooltip'] != '' )
                else if( $val['enlace'] == '' )
                {
                    //si no hay enlace, es porque es un titulo de submenu
					$contenido .= "<li style='background-color:#fff;'>".$val['nombre']."</li>";
                }//Fin de else if( $val['enlace'] == '' )
				//MAOH - 27 Ago 2012 - Fin
				else
				{
					$contenido .= "<li ui-sref-active='active'><a href=\"".$val['enlace']."\">".$val['nombre']."</a></li>";
				}
            }

            $contenido .= "</ul";

        }//Fin de if( is_array($submenu) && sizeof($submenu)>0 )

        return $contenido;

    }//Fin colocarOpcionesSubmenu

}//Fin de clase Menu
?>
