<?php

//require_once($_PATH_SERVIDOR.'establecimientos/establecimientos.php');
//require_once($_PATH_SERVIDOR.'carrouseles/Carrouseles.php');
//require_once($_PATH_SERVIDOR.'cadenas/cadenas.php');
//require_once($_PATH_SERVIDOR . "/plantillascorreo/PlantillasCorreo.php");
require_once("Usuarios.php");

$obj_usuarios = new Usuarios();

$_obj_interfaz->adicionarFooterJS("Miscelanea.js");
$_obj_interfaz->adicionarFooterJS("usuarios.js");

$datos['accion'] = $datos['accion'].$_SESSION['tipo_usuario'] ;

switch($datos['accion'])
{
    case 'url_acceso'.$obj_usuarios->validarAcceso(array()):
        //Elimina el archivo si existe uno previo
        Archivos::eliminarArchivo($_ARCHIVOS_TMP_COOKIE,$datos['cod']);

        //Si hay un hotel autenticado
        //verifica si el acceso es del mismo hotel
        $permitir_acceso = $_SESSION['tipo_usuario'] == 'H' && $_SESSION['est_codigo'] == $datos['cod'];
        $permitir_acceso |= $_SESSION['tipo_usuario'] == 'R';
        if( $permitir_acceso )
        {
            $datos['url'] = Herramientas::decodificarBase64CamposTextoFormulario($datos['url']);
            
            //DM - 2017-11-21 issue [10936]
            //Fin de registro de log
            $datos_log['log_termina_proceso'] = 1;
            logProceso($datos_log);

            header('Location: '.$datos['url']);
            exit;
        }//Fin de if( $_SESSION['tipo_usuario'] == 'H' )
        //crea un archivo con la url a la que debe acceder despues de autenticarse
        $contenido_archivo = $datos['url'];
        $contenido_archivo .= "\r\n";
        $contenido_archivo .= base64_encode($_SERVER['HTTP_USER_AGENT'].'_'.$_SERVER['REMOTE_ADDR']);
        Archivos::crearArchivo($_ARCHIVOS_TMP_COOKIE.$datos['cod'],$contenido_archivo);
        
        //DM - 2017-11-21 issue [10936]
        //Fin de registro de log
        $datos_log['log_termina_proceso'] = 1;
        logProceso($datos_log);

        //redireccion a la url de acceso
        header('Location: '.$_PATH_PAG_WEB);
        exit;
        break;
    case 'obtener_funcinalidades_paquete'.$obj_usuarios->validarAcceso(array("R")):
        $funcionalidades = $obj_usuarios->obtenerFuncionalidesPaqueteAjax($datos);
        print_r($funcionalidades);
        
        //DM - 2017-11-21 issue [10936]
        //Fin de registro de log
        $datos_log['log_termina_proceso'] = 1;
        logProceso($datos_log);

        exit;
        break;

	case 'login_usuario'.$obj_usuarios->validarAcceso(array("R","H","C","AD","S")):
		$resultado = $obj_usuarios->autenticarUsuario($datos);
        if( is_numeric($datos['usuario_root_id']) )
        {
            //DM - 2017-11-21 issue [10936]
            //Fin de registro de log
            $datos_log['log_termina_proceso'] = 1;
            logProceso($datos_log);

            header('Location: index.php?m=usuarios&accion=gestion');
            exit;
        }
        else
        {
            //DM - 2015-03-25
            //si es un tipo usuario hotel revisa si tiene info en los
            //archivos temporales
            if( $_SESSION['tipo_usuario']=='H')
            {
                $url = $obj_usuarios->obtenerUrlArchivoAcceso($_SESSION);
                if( $url !== false)
                {
                    if ($datos['auth_ajax'] != 'website_ajax')
                    {
                    
                        //DM - 2017-11-21 issue [10936]
                        //Fin de registro de log
                        $datos_log['log_termina_proceso'] = 1;
                        logProceso($datos_log);

                        //auth por pagina web si no redirecciona nomal
                        header('Location: '.$url);
                        exit;
                	}else
                    {
                        // de lo contrario devuelve url al ajax
                        $url = str_replace("http://hotels-quality.com/","",$url);
                        $url = str_replace("http://www.hotels-quality.com/","",$url);
                        $url = str_replace("https://www.hotels-quality.com/","",$url);
                        $url = str_replace("https://hotels-quality.com/","",$url);
                		echo json_encode(array('respuesta' => 1, 'url' => $url));
                        
                        //DM - 2017-11-21 issue [10936]
                        //Fin de registro de log
                        $datos_log['log_termina_proceso'] = 1;
                        logProceso($datos_log);

                        exit;
                	}
                }//Fin d if( $url !== false)
            }//Fin de if( $_SESSION['tipo_usuario']=='H')
            
            //DM - 2017-11-21 issue [10936]
            //Fin de registro de log
            $datos_log['log_termina_proceso'] = 1;
            logProceso($datos_log);

            header('Location: index.php?m=usuarios');
            exit;
        }
		break;

	case 'autenticar_usuario'.$obj_usuarios->validarAcceso(array()):
		$resultado = $obj_usuarios->autenticarUsuario($datos);
        if($resultado === "U-1" )
        {
            //DM - 2015-03-25
            //si es un tipo usuario hotel revisa si tiene info en los
            //archivos temporales u
            if( $_SESSION['tipo_usuario']=='H')
            {
                $url = $obj_usuarios->obtenerUrlArchivoAcceso($_SESSION);
                if( $url !== false)
                {
                    if ($datos['auth_ajax'] != 'website_ajax')
                    {
                        //DM - 2017-11-21 issue [10936]
                        //Fin de registro de log
                        $datos_log['log_termina_proceso'] = 1;
                        logProceso($datos_log);
                        
                        //auth por pagina web si no redirecciona nomal
                        header('Location: '.$url);
                        exit;
                	}else
                    {
                        // de lo contrario devuelve url al ajax
                        $url = str_replace("http://hotels-quality.com/","",$url);
                        $url = str_replace("http://www.hotels-quality.com/","",$url);
                        $url = str_replace("https://www.hotels-quality.com/","",$url);
                        $url = str_replace("https://hotels-quality.com/","",$url);
                		echo json_encode(array('respuesta' => 1, 'url' => $url));
                        
                        //DM - 2017-11-21 issue [10936]
                        //Fin de registro de log
                        $datos_log['log_termina_proceso'] = 1;
                        logProceso($datos_log);
                        
                        exit;
                	}
                }//Fin d if( $url !== false)
            }//Fin de if( $_SESSION['tipo_usuario']=='H')
            else if( $_SESSION['tipo_usuario']=='R')
            {
                $datos_root = array();
                $datos_root['est_codigo'] = md5('root_hq_Qu3ryt3k');
                $url = $obj_usuarios->obtenerUrlArchivoAcceso($datos_root);
                if( $url !== false)
                {
                    if ($datos['auth_ajax'] != 'website_ajax')
                    {
                    
                        //DM - 2017-11-21 issue [10936]
                        //Fin de registro de log
                        $datos_log['log_termina_proceso'] = 1;
                        logProceso($datos_log);

                        //auth por pagina web si no redirecciona nomal
                        header('Location: '.$url);
                        exit;
                	}else
                    {
                        // de lo contrario devuelve url al ajax
                        $url = str_replace("http://hotels-quality.com/","",$url);
                        $url = str_replace("http://www.hotels-quality.com/","",$url);
                		echo json_encode(array('respuesta' => 1, 'url' => $url));
                        
                        //DM - 2017-11-21 issue [10936]
                        //Fin de registro de log
                        $datos_log['log_termina_proceso'] = 1;
                        logProceso($datos_log);

                        exit;
                	}
                }//Fin d if( $url !== false)
            }//Fin de if( $_SESSION['tipo_usuario']=='H')

            $url = 'index.php?m=usuarios&usu_login='.$datos['usu_login'].'&msg='.$resultado;
            if ($datos['auth_ajax'] != 'website_ajax')
            {
                //DM - 2017-11-21 issue [10936]
                //Fin de registro de log
                $datos_log['log_termina_proceso'] = 1;
                logProceso($datos_log);
         	    header('Location: '.$url);
                exit;
        	}
            else
            {
        		echo json_encode(array('respuesta' => 1, 'url' => "index.php?usuario_id=".$_SESSION['usu_id']."&usuario_cod=".md5($_SESSION['usu_id'].date('YmdH'))));
                
                //DM - 2017-11-21 issue [10936]
                //Fin de registro de log
                $datos_log['log_termina_proceso'] = 1;
                logProceso($datos_log);
                
                exit;

        	}
        }//Fin de if( $resultado == true )
        else
        {
            //DM - 2014-05-28 nueva web
            if ($datos['auth_ajax'] != 'website_ajax'){
         	   $enlace = '?page_id=423&msg='.$resultado.'&lang='.$datos['lang'];
               
                //DM - 2017-11-21 issue [10936]
                //Fin de registro de log
                $datos_log['log_termina_proceso'] = 1;
                logProceso($datos_log);

    			header('Location: '.$_PATH_PAG_WEB.$enlace);
                exit;
        	}else{
        		echo json_encode(array('respuesta' => 2));
                
                //DM - 2017-11-21 issue [10936]
                //Fin de registro de log
                $datos_log['log_termina_proceso'] = 1;
                logProceso($datos_log);
                exit;
        	}
        }
		break;

	case 'cerrar_sesion'.$obj_usuarios->validarAcceso(array()):
        $idi_id = $_SESSION['idi_id'];
		unset($_SESSION);
		session_destroy();
        
        //DM - 2017-11-21 issue [10936]
        //Fin de registro de log
        $datos_log['log_termina_proceso'] = 1;
        logProceso($datos_log);

		header('Location: '.$_PATH_PAG_WEB);
		exit;
		break;

	case 'forma_olvido_clave'.$obj_usuarios->validarAcceso(array()):
		$datos['mensaje']= $_obj_mensajes->crearMensaje($datos['msg']);
		$contenido = $obj_usuarios->abrirFormularioOlvidoClave($datos);
		$_obj_interfaz->asignarContenido($contenido);
		break;

	case 'olvido_clave'.$obj_usuarios->validarAcceso(array()):
		$resultado = $obj_usuarios->solicitarRecordatorioClave($datos);
		if( is_array($resultado) )
		{
			$obj_correo = new Correo();

			//JMM-2017-01-30 - Validacion para datos SMTP
			//Se obtienen los datos de configuracion del hotel necesarios para envio de correos usando SMTP
			$opciones['opciones'] = establecimientos::obtenerConfiguracionGeneralEst($_SESSION['est_id']);
			//Se verifica la cantidad de emails enviados
			$cantidad_envios = establecimientos::verificarCantidadEmailsEnviados($opciones);
			if ($cantidad_envios === true) {
				$destinatario = $datos['usu_correo'];
				$remitente = $_EMAIL_ADMIN;
				$asunto = "Hotels quality indexs - Olvido de clave";
				$cuerpo = $obj_usuarios->obtenerCorreoOlvidoClave($datos,$resultado);
				$obj_correo->enviarCorreo($remitente, $destinatario, $asunto,$cuerpo, null, $opciones);
			}
			$destinatario = $datos['usu_correo'];
			$remitente = $_EMAIL_ADMIN;
			$asunto = "Hotels quality indexs - Olvido de clave";
			$cuerpo = $obj_usuarios->obtenerCorreoOlvidoClave($datos,$resultado);
			$obj_correo->enviarCorreo($remitente, $destinatario, $asunto,$cuerpo, null, $opciones);

            //DM - 2014-05-28 nueva web
            $resultado = "U-5";
            $enlace = '?page_id=434&msg='.$resultado.'&lang='.$datos['lang'];
            header('Location: '.$_PATH_PAG_WEB.$enlace);
            exit;
		}
        else
        {
            //DM - 2014-05-28 nueva web
            $enlace = '?page_id=434&msg='.$resultado.'&lang='.$datos['lang'];
            header('Location: '.$_PATH_PAG_WEB.$enlace);
            exit;
        }
		break;

	case 'forma_cambiar_datos'.$obj_usuarios->validarAcceso(array("R","H","C")):
        $_SESSION['modulo_activo']="datos_usuario";

		$valores = $obj_usuarios->obtenerDatosUsuario($_SESSION['usu_id']);
		if( !is_array($valores))
		{
			$opciones['CADENA']= $idi_despliegue['usuarios_1_cadena'];
			$opciones['ACCION']=0;
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($valores,$opciones);
		}

		if( isset($datos['msg'] ) )
        {
			$opciones['CADENA']= $idi_despliegue['usuarios_1_cadena'];
			$opciones['ACCION']=2;
			$datos['mensaje'] = $_obj_mensajes->crearMensaje( $datos['msg'] ,$opciones);
        }//Fin de if( isset($datos['msg'] )
		$datos['plantilla'] = 'login';
		$contenido = $obj_usuarios->abrirFormularioCambiarDatos($datos,$valores);
		//$datos['plantilla'] = 'login';
		$_obj_interfaz->asignarContenido($contenido);
		break;

	case 'cambiar_datos'.$obj_usuarios->validarAcceso(array("R","AD","C")):
		$resultado = $obj_usuarios->cambiarDatos($datos);
		header('Location: index.php?m=usuarios&accion=forma_cambiar_datos&msg='.$resultado);
		break;

	case 'forma_informacion'.$obj_usuarios->validarAcceso(array()):
        $_SESSION[inf_captcha] = "";
		$contenido = $obj_usuarios->abrirFormularioIngformacion($datos);
		$_obj_interfaz->asignarContenido($contenido);
		break;

	case 'enviar_informacion'.$obj_usuarios->validarAcceso(array()):
        if( strtoupper(trim($datos[inf_captcha])) != $_SESSION[inf_captcha] )
        {
            $_SESSION[inf_captcha] = "";
            $datos['mensaje'] = $_obj_mensajes->crearMensaje("CT-1",$opciones);
            $contenido = $obj_usuarios->abrirFormularioIngformacion($datos);
    		$_obj_interfaz->asignarContenido($contenido);
        }//Fin de if( trim($datos[inf_captcha]) != $_SESSION[inf_captcha] )
        else
        {
    	   $datos['inf_mensaje'] = str_replace("\n","<br>",$datos['inf_mensaje']);

    	   $obj_correo = new Correo();

    	   //JMM-2017-01-30 - Validacion para datos SMTP
    	   //Se obtienen los datos de configuracion del hotel necesarios para envio de correos usando SMTP
    	   $opciones['opciones'] = establecimientos::obtenerConfiguracionGeneralEst($_SESSION['est_id']);
    	   //Se verifica la cantidad de emails enviados
    	   $cantidad_envios = establecimientos::verificarCantidadEmailsEnviados($opciones);

    		$destinatario = $_EMAIL_ADMIN;
    		$remitente = $datos['inf_correo'];
    		$asunto = "HOTELS quality - Informacion";

    		$cuerpo = "Nombre establecimiento: ".$datos['inf_nombre_establecimiento']."<br>";
    		$cuerpo .= "Nombre: ".$datos['inf_nombre']."<br>";
            $cuerpo .= "Correo: ".$datos['inf_correo']."<br>";
    		$cuerpo .= "Telefono:".$datos['inf_telefono']."<br>";
    		$cuerpo .= "PMS:".$datos['inf_pms']."<br>";

            if( $remitente == "" )
            {
                $remitente = $_EMAIL_ADMIN;
            }

            //JMM-2017-02-01 - Validacion para datos SMTP
            if ($cantidad_envios === true) {
            	//$opciones['CC']= "pablo.fernandez@querytek.com";
            	$estado = $obj_correo->enviarCorreo($remitente, $destinatario, $asunto,$cuerpo,$_FILES,$opciones);
            }
            //$opciones['CC']= "pablo.fernandez@querytek.com";
            $estado = $obj_correo->enviarCorreo($remitente, $destinatario, $asunto,$cuerpo,$_FILES,$opciones);

            if( $estado==1)
            {
                header('Location: index.php?m=usuarios&msg=IN-1');
            }
            else
            {
        		$datos['mensaje'] = $_obj_mensajes->crearMensaje("IN-2",$opciones);
                $contenido = $obj_usuarios->abrirFormularioIngformacion($datos);
        		$_obj_interfaz->asignarContenido($contenido);
            }
        }
		break;

	case 'gestion'.$obj_usuarios->validarAcceso(array("H","R","AD","C")):    //Antes estaba asi: validarAcceso(array("C")), se cambio mientras se trabaja en la nueva gestion de usuarios

		$_SESSION['modulo_superior'] = "configuracion";
    $_SESSION['submodulo_superior'] = "";
		$_SESSION['modulo_activo'] = "conf_gestion";
		$opciones['ACCION']=$datos['tipo_gestion'];
		$opciones['CADENA']= $idi_despliegue['usuarios_1_cadena'];
		$datos['mensaje']= $_obj_mensajes->crearMensaje($datos['msg'],$opciones);
		$contenido = $obj_usuarios->abrirFormularioGestion($datos);

		$valores = $obj_usuarios->obtenerUsuariosGestion($datos);

		if ($_SESSION['tipo_usuario'] == 'R')
		{
			$campos = array("usu_id"=>$idi_despliegue['hue_id'],
							"usu_nombre"=>$idi_despliegue['label_nombre'],
							"usu_login"=>$idi_despliegue['usuario'],
							"usu_correo"=>$idi_despliegue['hue_email'],
							"cad_nombre"=>$idi_despliegue['cadena'],
							"est_nombre"=>$idi_despliegue['establecimiento'],
							"usu_per_id"=>"",
							"usu_usc_id"=>"");
			$campos_ordena = array("usu_id","usu_nombre","usu_login","usu_correo", "cad_nombre", "est_nombre");

            $opciones['login'] = "usuario_id=%usu_id%";
		}
		else
		{
			$campos = array("usu_id"=>"",
							"usu_nombre"=>$idi_despliegue['label_nombre'],
							"usu_login"=>$idi_despliegue['usuario'],
							"usu_correo"=>$idi_despliegue['hue_email'],
							"usu_per_id"=>"",
							"usu_usc_id"=>"");
			$campos_ordena = array("usu_nombre","usu_login","usu_correo");
		}

		//Septiembre 17 de 2011 YA NO Se restringe por ahora solo para perfil tipo cadena
		//$restriccionCadena = " AND per_id = '6' ";

		//El select de los tipos de usuario
		$opcionesTipoUsuario['nombre_campo']="usu_per_id";
        $opcionesTipoUsuario['sql']="select per_id , pid_nombre
								from perfiles, perfiles_idioma
								where 	per_id = pid_per_id AND
										pid_idi_id = '".$_SESSION['idi_id']."'
								".$restriccionCadena."
								AND per_id != 1 AND per_id != 3 AND per_id != 4 AND per_id != 5 AND per_id != 7
								order by per_nombre ASC";
        $opcionesTipoUsuario['atributos']['codigo']="per_id";
        $opcionesTipoUsuario['atributos']['mostrar']="pid_nombre";
        $opcionesTipoUsuario['valor']=$datos['usu_per_id'];
		$opcionesTipoUsuario['adicionales']='onChange="submit()"';
		//El select de las categorias de usuarios
		$opcionesCategoriaUsuario['nombre_campo']="usc_codigo";
        $opcionesCategoriaUsuario['sql']="select usc_id , uci_nombre
								from usuario_categoria, usuario_categoria_idioma
								where usc_id = uci_usc_id AND uci_idi_id = '".$_SESSION['idi_id']."'
								order by usc_nombre ASC";
		$opcionesCategoriaUsuario['atributos']['codigo']="usc_id";
        $opcionesCategoriaUsuario['atributos']['mostrar']="uci_nombre";
        $opcionesCategoriaUsuario['valor']=$datos['usc_codigo'];
        $opcionesCategoriaUsuario['adicionales']='onChange="submit()"';
		$opciones['tipoUsuario'] = Herramientas::crearSelectFormaPersonalizada($opcionesTipoUsuario,$idi_despliegue['tipo_usuario_select']);
		$opciones['categoriaUsuario'] = Herramientas::crearSelectFormaPersonalizada($opcionesCategoriaUsuario,$idi_despliegue['categoria']);

        if( $_SESSION[tipo_usuario]=="C")//El usuario es de tipo cadena %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		{
			$ids = array_keys( $_SESSION[cadena_est] );
			$hotel =  $_SESSION['cadena_est'][$ids[0] ];

			$datos['est_id'] = $datos['est_id'] == "" ? $hotel['est_id'] : $datos['est_id'];
		}
		$est_id = ($datos['est_id'] == '') ? $_SESSION['est_id'] : $datos['est_id'];
		$opciones['actualizar'] = "usc_codigo=%usu_usc_id%&est_id=".$est_id."&usu_per_id=%usu_per_id%&usu_id=%usu_id%&categoria_codigo=%usu_usc_id%&offset=".$datos['offset']."&usu_busqueda=".$datos['usu_busqueda']."&ordena=".$datos['ordena']."&tipo_ordena=".$datos['tipo_ordena'];
		$opciones['eliminar'] = "est_id=".$est_id."&usu_id=%usu_id%&usu_busqueda=".$datos['usu_nombre_cedula'];

		$arreglo_datos['TITULO'] = '';  //"Listado de usuarios";
		$arreglo_datos['CAMPOS_OPCIONES']=
					array("usu_cedula"=>'width="80" align="center"',
						"usu_nombre"=>'width="200" align="left"',
						"pid_nombre"=>'width="110" align="center"',
						"usu_login"=>'width="110" align="left"');

		$arreglo_datos['CAMPOS']=$campos;
		$arreglo_datos['CAMPOS_ORDENA']=$campos_ordena;
		$arreglo_datos['LISTADO']=$valores;
		$arreglo_datos['OPCIONES']=$opciones;
		$arreglo_datos['VOLVER']=true;
		$arreglo_datos['ACCION_LISTAR'] = "gestion&m=usuarios&usu_busqueda=".$datos['usu_busqueda'];
		$arreglo_datos['ACCION_ACTUALIZAR']="fm_editar";
		$arreglo_datos['ACCION_ELIMINAR']="eliminar";
        $arreglo_datos['ACCION_LOGIN']="login_usuario&m=usuarios";
		$arreglo_datos['DATOS']=$datos;


		if( isset($datos['msg']))
		{
			$opciones['ACCION']=$datos['tipo_gestion'];
			$opciones['CADENA']= $idi_despliegue['usuarios_1_cadena'];
			$arreglo_datos['MENSAJE'] = $_obj_mensajes->crearMensaje($datos['msg'],$opciones);
		}
		//amp - 12/8/15 - insertar migasd e pan
		$rastro = array( 'nivel_1' => array( "titulo"=>$idi_despliegue['label_menu_configuracion']),
						 'nivel_2' => array( "titulo"=>$idi_despliegue['gestion_usuarios'] , 'activo'=> 1));
		$_obj_interfaz->asignarMigas($rastro);

		$contenido .= $_obj_interfaz->crearListadoUsuarios($arreglo_datos);
		$_obj_interfaz->asignarContenido($contenido);
		break;

	case 'fm_ingresar'.$obj_usuarios->validarAcceso(array("H","R","AD","C")):    //Antes estaba asi: validarAcceso(array("C")), se cambio mientras se trabaja en la nueva gestion de usuarios
        $_SESSION['modulo_activo']="configuracion";
		$contenido = $obj_usuarios->abrirFormularioIngresar($datos);
		$_obj_interfaz->asignarContenido($contenido);
		break;

	case 'ingresar'.$obj_usuarios->validarAcceso(array("H","R","AD","C")):    //Antes estaba asi: validarAcceso(array("C")), se cambio mientras se trabaja en la nueva gestion de usuarios
		$resultado = $obj_usuarios->ingresarUsuarioGestion($datos);
		if( $resultado==1)
		{
            //DM - 2014-07-29 issue 5040
            if( $datos['est_id'] != '')
            {
                $resultado .= '&est_id='.$datos['est_id'];
            }
			header('Location: index.php?m=usuarios&accion=gestion&tipo_gestion=1&msg='.$resultado);
			exit;
		}
		else
		{
			$opciones['ACCION']=1;
			$opciones['CADENA']= $idi_despliegue['usuarios_1_cadena'];
			$datos['mensaje']= $_obj_mensajes->crearMensaje($resultado,$opciones);
			$contenido = $obj_usuarios->abrirFormularioIngresar($datos);
			$_obj_interfaz->asignarContenido($contenido);
		}
		break;

	case 'fm_editar'.$obj_usuarios->validarAcceso(array("H","R","AD","C")):    //Antes estaba asi: validarAcceso(array("C")), se cambio mientras se trabaja en la nueva gestion de usuarios
        $_SESSION['modulo_activo']="editar";
		$valores = $obj_usuarios->obtenerDatosUsuario($datos['usu_id']);
		if( !is_array($valores))
		{
			$opciones['CADENA']= $idi_despliegue['usuarios_1_cadena'];
			$opciones['ACCION']=0;
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($valores,$opciones);
		}
		$contenido = $obj_usuarios->abrirFormularioEditar($datos,$valores);
		$_obj_interfaz->asignarContenido($contenido);
		break;

	case 'editar'.$obj_usuarios->validarAcceso(array("C","H","R")):
		$resultado = $obj_usuarios->actualizarUsuarioGestion($datos);
		if( $resultado==1)
		{
			$enlace = "index.php?m=usuarios&accion=gestion&tipo_gestion=2&usu_busqueda=".$datos['usu_busqueda']."&msg=".$resultado;
            //DM - 2014-07-29 issue 5040
            if( $datos['est_id'] != '')
            {
                $enlace .= '&est_id='.$datos['est_id'];
            }
			header('Location: '.$enlace);
            exit;
		}
		else
		{
			$opciones['ACCION']=2;
			$opciones['CADENA']= $idi_despliegue['usuarios_1_cadena'];
			$datos['mensaje']= $_obj_mensajes->crearMensaje($resultado,$opciones);
			$valores = $obj_usuarios->obtenerDatosUsuario($datos['usu_id']);
			$contenido = $obj_usuarios->abrirFormularioEditar($datos,$valores);
			$_obj_interfaz->asignarContenido($contenido);
		}
		break;

	case 'eliminar'.$obj_usuarios->validarAcceso(array("H","R","AD","C")):    //Antes estaba asi: validarAcceso(array("C")), se cambio mientras se trabaja en la nueva gestion de usuarios
		$resultado = $obj_usuarios->eliminarUsuario($datos);

		$enlace = "index.php?m=usuarios&accion=gestion&est_id=".$datos['est_id']."";
		$enlace .= "&usu_nombre_cedula=".$datos['usu_nombre_cedula'];
		$enlace .= "&tipo_gestion=3&msg=".$resultado;
		header('Location: '.$enlace);
		break;

	case 'abrir_formulario_registro':
		$datos['mensaje']= $_obj_mensajes->crearMensaje($datos['msg']);
		$contenido = $obj_usuarios->abrirFormularioRegistro($datos);
		$_obj_interfaz->asignarContenido($contenido);
		break;

	case 'activar_usuario':
		$resultado = $obj_usuarios->activarUsuario($datos);
		break;

	//MAOH - 25 Sep 2012 - Incidencia 1761
	//Se quita el captcha del formulario y se agrega que solo puedan acceder los usuarios
	case 'abrir_formulario_soporte'.$obj_usuarios->validarAcceso(array("H", "R", "C", "AD", "AM", "AU", "RP")):
	    //$_SESSION[inf_captcha] = "";
		$_obj_interfaz->adicionarFooterJS("Miscelanea.js");
		$contenido = $obj_usuarios->abrirFormularioSoporte($datos);
    	//AMP Cambio a modal de INSPINIA
		 $_obj_interfaz->asignarContenido($contenido);
		 $contenido = $_obj_interfaz->crearPopUp();
		//print_r($pagina);
    echo $contenido;
		exit;
		break;

	//MAOH - 25 Sep 2012 - Incidencia 1761
	//Se quita el captcha del formulario y se agrega que solo puedan acceder los usuarios
	case 'soporte'.$obj_usuarios->validarAcceso(array("H", "R", "C", "AD", "AM", "AU", "RP")):
        /*if( strtoupper(trim($datos['inf_captcha'])) != $_SESSION['inf_captcha'] )
        {
            $_SESSION[inf_captcha] = "";
			$datos['mensaje'] = $_obj_mensajes->crearMensaje("CT-1");
            $contenido = $obj_usuarios->abrirFormularioSoporte($datos);
        }//Fin de if( strtoupper(trim($datos[inf_captcha])) != $_SESSION[inf_captcha] )
        else
        {*/
    	   $datos['sop_comentario'] = str_replace("\n","<br>",$datos['sop_comentario']);

    	   $obj_correo = new Correo();

    	   //JMM-2017-01-30 - Validacion para datos SMTP
    	   //Se obtienen los datos de configuracion del hotel necesarios para envio de correos usando SMTP
    	   $opciones['opciones'] = establecimientos::obtenerConfiguracionGeneralEst($_SESSION['est_id']);
    	   //Se verifica la cantidad de emails enviados
    	   $cantidad_envios = establecimientos::verificarCantidadEmailsEnviados($opciones);

    		$destinatario = $_EMAIL_ADMIN;
    		$remitente = $datos['sop_correo'];
    		$asunto = "Hotels quality indexs - Soporte";

            $cuerpo = "Hotel: ".$_SESSION['est_nombre']."<br>";
            $cuerpo .= "ID Hotel: ".$_SESSION['est_id']."<br>";
    		$cuerpo .= "Nombre: ".$datos['sop_nombre']."<br>";
            $cuerpo .= "Fecha: ".$datos['sop_fecha']."<br>";
    		$cuerpo .= "Comentario:<br>".$datos['sop_comentario'];

    		//JMM-2017-02-01 - Validacion para datos SMTP
    		if ($cantidad_envios === true) {
    			$estado = $obj_correo->enviarCorreo($remitente, $destinatario, $asunto,$cuerpo,$_FILES,$opciones);
    		} else if ($cantidad_envios === false) {
    			$estado = $obj_correo->enviarCorreo($remitente, $destinatario, $asunto,$cuerpo,$_FILES,$opciones);
    		}

            if( $estado==1)
            {
                $datos['mensaje'] = "<div class='alert alert-success'>".$idi_despliegue['msg_envio']."</div>";
                $contenido = $obj_usuarios->abrirMsgSoporte($datos);
            }
            else
            {
                $datos['mensaje'] = "<div class='alert alert-danger'>".$idi_despliegue['msg_error']."</div>";
                $contenido = $obj_usuarios->abrirFormularioSoporte($datos);
            }
        //}
        //AMP Cambio a modal de INSPINIA
	/*	$_obj_interfaz->asignarContenido($contenido);
		$pagina = $_obj_interfaz->crearPopUp();
		print_r($pagina);*/
    echo $contenido;
		exit;
	   break;

    case 'cambiar_encuesta'.$obj_usuarios->validarAcceso(array()):
        $_SESSION['enc_id'] = $datos['enc_id'];

        //MD - 2012-09-11
		//Obtiene la plantilla generica del establecimiento
        $plantilla = $_obj_database->obtenerRegistro("establecimiento_plantilla_email",
                                                    "epe_est_id = '".$_SESSION ['est_id']."'
                                                    and epe_enc_id = '".$_SESSION['enc_id']."'
                                                    and epe_generico = 1 ");

		$_SESSION ['est_pro_id'] = $plantilla['epe_pro_id'];
		//MD - 2012-09-11 - Fin de cambios
        exit;

                                                                   
	//Formulario de autenticacion
	default:
            //AM - 2015-08-11
            //carga la plantilla de inicio de sesion sin menu
            $datos['plantilla'] = 'fm_inicial';
			$datos['mensaje']= $_obj_mensajes->crearMensaje($datos['msg']);
			$contenido = $obj_usuarios->abrirFormularioAutenticacion($datos);
			$_obj_interfaz->asignarContenido($contenido);
		break;
}//Fin de switch($datos['accion'])
?>