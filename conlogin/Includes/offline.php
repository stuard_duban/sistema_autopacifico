<?php
/*
CHCM.
CARLOS H. CERON MEZA
CARCERON@HOTMAIL.COM
JULIO 01 2008

SE LLAMA CUANDO HAY ERRORES DE CONEXION A LA BASE DE DATOS
*/

switch($error_db)
{
  case "1": echo "Error conectando al host";  break;
  case "2": echo "La base de datos especificada no existe";  break;
  case "3": echo "Error desconectando de la base de datos";  break;
}
?>
