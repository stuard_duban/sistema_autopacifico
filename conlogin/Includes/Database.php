<?php
/*
CARLOS H. CERON M. CARCERON@HOTMAIL.COM
JULIO 01 2008

EN ESTE FICHERO SE INCLUYEN TODAS LAS FUNCIONES NECESARIAS PARA LA CONEXION A LA BASE DE DATOS, 
INSERTS, UPDATES, DELETES, SELECT, ASI COMO TAMBIEN OBTENER EL NUMERO DE FILAS AFECTADAS Y DEMAS.
*/

class database
{

  // maneja la conexion a la base de datos
  var $_conector='';
  
  // maneja la consulta arrojada a la base de datos
  var $_sql='';
  
  // maneja el resultado de una operacion mysql_query
  var $_resultset=null;
  
  // maneja el limit de las consultas
  var $_limit=0;
  
  // maneja el offset de las consultas
  var $_offset=0;
  
  var $_key_db = 0;
  
  var $_host = array();
  var $_user = array();
  var $_password = array();  
  var $_db = array();
  
    
  function database($host,$user,$password,$db,$key_db=0)
  {
		$this->_key_db = $key_db;
		$this->_host[ $this->_key_db ] = $host;
		$this->_user[ $this->_key_db ] = $user;
		$this->_password[ $this->_key_db ] = $password;
		$this->_db[ $this->_key_db ] = $db;
	}//Fin de database
	
	function usarDB()
	{
		$host = $this->_host[ $this->_key_db ];
		$user = $this->_user[ $this->_key_db ];
		$password = $this->_password[ $this->_key_db ];
		$db = $this->_db[ $this->_key_db ];				
		
		//si no esta conectado se conecta a la base de datos
		if( !$this->_conector[$this->_key_db] )
		{
			if (!$this->_conector[$this->_key_db] = mysql_connect( $host, $user, $password ))
			{
				$error_db = 1; //no conecta
				echo "<div class='mensaje_error'>Error conectando al host</div>";
				exit;
			}		
		}
		
		
		if ($db != '' && !mysql_select_db( $db, $this->_conector[$this->_key_db] ))
		{
			echo "<div class='mensaje_error'>La base de datos especificada no existe</div>";
			exit;
		}		
	}//Fin de usarDB()
	
	function iniciarTransaccion()
	{
		$sql = "BEGIN;";
		return $this->ejecutarSQL($sql);
	}
	
	function cancelarTransaccion()
	{
		$sql = "ROLLBACK;";
		return $this->ejecutarSQL($sql);
	}
	
	function terminarTransaccion()
	{
		$sql = "COMMIT;";
		return $this->ejecutarSQL($sql);
	}


	function desconectar ()
  	{
  
  		//si hay una conexion activa la cierra
		if( $this->_conector[$this->_key_db] )
		{
			if(!@mysql_close($this->_conector[$this->_key_db]))
			{
				 echo "<div class='mensaje_warning'>Error desconectando de la base de datos</div>";
				 exit;
			}
		}//Fin de if( $this->_conector[$this->_key_db] )
  	}
  
  //esta funcion se llama para actualizar, eliminar o insertar registros
  function ejecutarSql($sql)
  {
		$this->usarDB();
		
     if (!$this->_resultset =  @mysql_query($sql,$this->_conector[$this->_key_db]))
      {
        //echo "<div class='mensaje_error'>Ejecucion:".mysql_error()." en: $sql</div>";
        return null;
      }  
      
	  return $this->_resultset;
  }
  
  function obtenerNumeroFilas()
  {
    if (!$this->_resultset)
      return 0;
    else
      return mysql_num_rows($this->_resultset);
  }
  
  function obtenerNumeroFilasAfectadas()
  {    
    if (!$this->_resultset)
      return 0;
    else
      return mysql_affected_rows( $this->_conector[$this->_key_db] );
  }
  
  function obtenerConsulta($sql)
  {
		$this->usarDB();
		return $this->_resultset =  @mysql_query($sql,$this->_conector[$this->_key_db]);
  }//Fin de obtenerConsulta()
  
  
  //obtiene la primera columna de la primera fila
  function obtenerResultado($sql)
  {
		$this->usarDB();
		
      $_sql = $sql;
      if (!$this->_resultset =  @mysql_query($sql,$this->_conector[$this->_key_db]))
      {
        //echo "<div class='mensaje_error'>".mysql_error()." . ERROR en:  $sql</div>";
        return null;
      }  
      if ($row = mysql_fetch_row( $this->_resultset )) {
			 $ret = $row;
			 
		  }
		  //mysql_free_result( $this->_resultset );
	 	  return $ret;
  }
  
   
   //obtiene un arreglo con los resultados de la consulta
   function obtenerRegistros($sql)
   { 
		$this->usarDB();
		
      if (!$this->_resultset =  @mysql_query($sql,$this->_conector[$this->_key_db]))
      {
        //echo "<div class='mensaje_error'>Consulta:".mysql_error()."</div>";
        return null;
      }  
      $array = array();
		  while ($row = mysql_fetch_row( $this->_resultset )) {
			$array[] = $row;
		}
		  //mysql_free_result( $this->_resultset );
	 	  return $array;
   }

   //obtiene cantidad de resultados de la consulta
   function obtenerCantidadResultados($sql)
   {
   	$this->usarDB();
   
   	if (!$this->_resultset =  @mysql_query($sql,$this->_conector[$this->_key_db]))
   	{
   		//echo "<div class='mensaje_error'>Consulta:".mysql_error()."</div>";
   		return null;
   	}
   	return $this->obtenerNumeroFilas();
   }
   
   //jcb obtiene un registros para csv
   function obtenerRegistrosCsv($sql)
   {
   	$this->usarDB();
   
   	if (!$this->_resultset =  @mysql_query($sql,$this->_conector[$this->_key_db]))
   	{
   		//echo "<div class='mensaje_error'>Consulta:".mysql_error()."</div>";
   		return null;
   	}
   	$csv_export = '';
   	//$array = array();
   	$field = mysql_num_fields($this->_resultset);
   
   	// create line with field names
   	for($i = 0; $i < $field; $i++) {
   		$csv_export.= mysql_field_name($this->_resultset,$i).';';
   	}
   	// newline (seems to work both on Linux & Windows servers)
   	$csv_export.= ' 
';
   
   	// loop through database query and fill export variable
   	while($row = mysql_fetch_array($this->_resultset)) {
   		// create line with field values
   		for($i = 0; $i < $field; $i++) {
   			$csv_export.= '"'.$row[mysql_field_name($this->_resultset,$i)].'";';
   		}
   		$csv_export.= '
';
   	}
   	 
   	//mysql_free_result( $this->_resultset );
   	return $csv_export;
   }
   
   //obtiene un arreglo con los campos asociativos
   function obtenerRegistrosAsociativos($sql)
   { 
		$this->usarDB();
		
      if (!$this->_resultset =  @mysql_query($sql,$this->_conector[$this->_key_db]))
      {
        //echo "<div class='mensaje_error'>ConsultaA:".mysql_error()."</div>";
        return null;
      }  
      
      $array = array();
		  while ($row = mysql_fetch_assoc( $this->_resultset )) 
      {
			   $array[] = $row;
		  }
		  
	 	  return $array;
   }
   
   //obtiene un arreglo con los campos asociativos
   function obtenerRegistrosAsociativosPorClave($sql,$clave)
   { 
		$this->usarDB();
		
      if (!$this->_resultset =  @mysql_query($sql,$this->_conector[$this->_key_db]))
      {
        //echo "<div class='mensaje_error'>ConsultaC".mysql_error()."</div>";
        return null;
      }  
      
      $array = array();
		  while ($row = mysql_fetch_assoc( $this->_resultset )) 
      {
			   $array[ $row[$clave] ] = $row;
		  }
		  
	 	  return $array;
   }
   
   function obtenerRegistrosAsociativosAgrupadosPorClave($sql,$clave,$clave_nivel_2="")
   { 
		$this->usarDB();
		
      if (!$this->_resultset =  @mysql_query($sql,$this->_conector[$this->_key_db]))
      {
        //echo "<div class='mensaje_error'>ConsultaCG".mysql_error()."</div>";
        return null;
      }  
      
      $array = array();
		while ($row = mysql_fetch_assoc( $this->_resultset )) 
      {
	  			if( $clave_nivel_2 != "" )
				{
					$array[ $row[$clave] ][ $row[$clave_nivel_2] ] = $row;
				}
				else
				{
					$array[ $row[$clave] ][] = $row;
				}
			   
		  }
		  
	 	  return $array;
   }

   
   function obtenerUltimoIdGenerado()
   {
		$this->usarDB();
		
      return mysql_insert_id( $this->_conector[$this->_key_db] );
   }
   
   function obtenerProximoId($datos)
   {
   		$sql = "select max(".$datos['id'].") as id 
				from ".$datos['tabla']." ";
				
		$res = $this->obtenerResultado($sql);		
		if( is_array($res) && sizeof($res)>0 )
		{
			$res = $res[0]+1;
		}
		else
		{
			$res = 1;
		}
		return $res;
   }//Fin de obtenerProximoId($datos)
   
   function generarSQLInsertar($datos,$campos)
   {		
		$lista_valores = array();
		foreach($campos as $key => $valor)
		{
			if( is_numeric($key) )
			{
				$lista_valores[ $valor ] = "'".$datos[ $valor ]."'";
			}
			else
			{
				$lista_valores[ $key ] = $datos[ $key ];
			}
		}//Fin de foreach($campos as $key => $valor)
		
		//no existe el usuario en la bd
		$sql="insert into ".$datos['tabla']."(".implode(",", array_keys($lista_valores)).") 
				values (".implode(",",$lista_valores).")";
					
		return $sql;						
   }//Fin de generarSQLInsertar()
   
   
   function generarSQLActualizar($datos,$campos)
   {	
		$lista_valores = array();
		foreach($campos as $key => $valor)
		{
			if( is_numeric($key) )
			{
				$lista_valores[] = $valor."='".$datos[$valor]."'";
			}
			else
			{
				$lista_valores[] = $key."=".$datos[$key];
			}
		}//Fin de foreach($campos as $key => $valor)
		
		
		$sql="update ".$datos['tabla']." 
				set ".implode(",",$lista_valores)."
				where ".$datos['condicion']."";
				
		return $sql;		
   }//Fin de generarSQLActualizar()
   
	function obtenerRegistro($tabla,$condicion)
	{	
		$sql = "select * 
					from ".$tabla." 
					where ".$condicion;
					
		$valores = $this->obtenerRegistrosAsociativos($sql);
		return $valores[0];
	}//Fin de obtenerValor
	
  
}//class
?>
