<?php

class Correo
{

/*	******************************************	CONSTRUCTOR CORREO	*******************************************	*/

	function Correo()
	{
	}


/*	**********************************	ENVIAR CORREO	*******************************************************	*/
/*
 *	Utilidad:
 *		Verifica si un archivo fue cargado en el servidor a trav�s del m�todo POST de un formulario
 *	Par�metros de entrada:
 *		$remitente -> Remitente del Mensaje de Correo
 *		$destinatario -> Cadena con la Lista de destinatarios separados por Coma (,)
 *		$asunto -> Asunto o Subject del Mensaje de Correo
 *		$mensaje -> Contenido del Mensaje de Correo
 *		$archivo_adjuntos -> Archivos adjuntos que tendr� el Mensaje de Correo (Arreglo $_FILES)

 *	Valores de retorno:
 *		1, El Mensaje de Correo fue enviado con �xito
 *		-1, uno de los Archivos adjuntos excede el tama�o permitido en el php.ini o el valor especificado en el campo MAX_FILE_SIZE del formulario
 *		-2, uno de los Archivos adjuntos no fue cargado totalmente en el Servidor
 *		0, Se present� un error al enviar el Mensaje de Correo

 *	Notas:
 *		Para fines pr�cticos, se retornan cosas de un solo tipo, en este caso s�lo se retornan n�meros enteros.

 */

 //pgs - 14/06/2013 - se pone el responder a
	function enviarCorreo($remitente, $destinatario, $asunto, $mensaje, $archivos_adjuntos=NULL,$sender=NULL,$configuracion=array(),$replyto=null)
	{
		global  $_smtp,$_login_smtp,$_clave_smtp,$_requiere_autenticacion,$_SMTPSecure,$_puerto_smtp;
		
		include_once("PhpMailer/class.phpmailer.php");
		$mensaje = wordwrap(trim($mensaje), 70);

		if(strpos($remitente, "<") != false)
		{
			list($remitente_nombre, $remitente_correo) = explode("<", $remitente);
			$remitente_correo = substr($remitente_correo, 0, -1);
		}
		else
		{
			$remitente_correo = $remitente;
			$remitente_nombre="";
		}
    
    $mail = new PHPMailer();
    
    //pgs - 08/02/2012 - si el destinatario es un array es por que hay varios, se a�ade cada uno de ellos
    if( is_array($destinatario) && sizeof($destinatario) > 0){
      foreach($destinatario as $cor){
          $mail->AddAddress($cor);		  
      }//Fin de foreach($destinatario as $cor)
    }else{
      if(strpos($destinatario, "<") != false){
  			list($destinatario_nombre, $destinatario_correo) = explode("<", $destinatario);
  			$destinatario_correo = substr($destinatario_correo, 0, -1);
  		}else{
  			$destinatario_correo = $destinatario;
  			$destinatario_nombre="";
  		}
    
      $mail->AddAddress($destinatario_correo,$destinatario_nombre);
    }//Fin de if( is_array($destinatario) && sizeof($destinatario) > 0)
		
		$mail->IsSMTP();
		$mail->isHTML(True);	    
		$mail->From = $remitente_correo;
		$mail->FromName = $remitente_nombre;
		$mail->Subject = $asunto;
		$mail->Body = $mensaje;
		
		//chcm.  enero 31 2012.  agrego la linea para que env�e contenido en solo texto.  tomado de http://snippets.aktagon.com/snippets/129-How-to-send-both-HTML-and-text-emails-with-PHP-and-PHPMailer
		$mail->AltBody = "Version en solo texto no disponible";//$textMessage;    # This automatically sets the email to multipart/alternative. This body can be read by mail clients that do not have HTML email capability such as mutt
		
    
  
    
    
		//chcm.  enero 31 2012.  //if use_x_priority is set to ON, what should the value be? Urgent = 1, Not Urgent = 5, Disable = 0 . Default is 3
    //pgs - 16/08/2012 - se quita para que quede normal, pedicion chcm
    //$mail->Priority = 5;
		
		$mail->Host = 	$configuracion['host'];
		
    if(!empty($sender)){
		  $mail->Sender = $sender;
	  } //chcm.  Sept 11 2015
    else
      $mail->Sender = $remitente_correo;
    
    
		$mail->SMTPAuth = $configuracion['autenticacion'];
		
		//pgs - 14/06/2013 - se pone el responder a
		if(!empty($replyto)){
		  $mail->ReplyTo[0][] = $replyto;
	  }//Fin de if(!empty($replyto))
    
   //echo "<pre>"; print_r($configuracion);echo "</pre>";
    
    
		
		$mail->Username = $configuracion['cuenta'];
		$mail->Password = $configuracion['clave'];
		if(!empty($configuracion['puerto']) && $configuracion['puerto']!=0 & $configuracion['puerto']!='0'){
      $mail->Port = $configuracion['puerto'];
    }
    if(!empty($configuracion['secure'])){
		  $mail->SMTPSecure = $configuracion['secure'];
		}
     //var_dump($configuracion);exit;
		//archivos adjuntos
		if(is_array($archivos_adjuntos) && (sizeof($archivos_adjuntos)>0))
		{
			foreach($archivos_adjuntos as $archivo_adjunto)
			{
				$mail->AddAttachment($archivo_adjunto["tmp_name"], $archivo_adjunto["name"], 'base64', $archivo_adjunto["type"]);
			}
		}
    /*var_dump($mail->Host);
    var_dump($mail->Sender);
    var_dump($mail->Username);exit;*/
		$resultado= $mail->Send();
		if (!$resultado)
		{
		  echo $mail->ErrorInfo;
      
      //chcm.  
      /*
      
      $cabecera = "from: carceron@gmail.com \nMIME-Version: 1.0\nContent-Type: text/html\nX-Mailer: WebMail";
      $mensaje= "Error con cliente ".$mail->From." con asunto $asunto<br><br>.  El error es: ".$mail->ErrorInfo;
      @mail("carlos.ceron@querytek.com","error en envio de Boletines",$mensaje,$cabecera);*/
		  return 0;
		}
		else
		  return 1;

	}
	
	function leerCorreo($datos)
	{     
	
    $correos = array();
    $cont = 0;
    
    $servidor = $datos["serv"];

    switch($servidor)
    {
        /*case 'gmail':
            $authhost = "{imap.gmail.com:993/imap/ssl/novalidate-cert}";
            $user = "@gmail.com";
            $pass = "";
            break;   
        case 'hotmail':
            // Hotmail no usa IMAP
            $authhost = "{pop3.live.com:995/pop3/ssl/novalidate-cert}"; 
            $user = "@hotmail.com";
            $pass = "";
            break;
        case 'yahoo':
            // Al parecer yahoo si tiene IMAP, pero aun no o puedo acceder
            $authhost = "{pop.mail.yahoo.com:995/pop3/ssl/novalidate-cert}";
            $user = ""@yahoo.com;
            $pass = "";
            break;*/  
        case 'intromedia':
            $authhost = "{imap.readyhosting.com:110/pop3/notls}INBOX";
            $user = "pruebas@intro-media.com";
            $pass = "301982";
            break;
        default:
            $authhost = "{pop3.readyhosting.com:110/pop3/notls}INBOX";
            $user = "pruebas@intro-media.com";
            $pass = "301982";
        break;
    }

    $cant = ($datos["cant"] == "") ? 10: $datos["cant"];
  
    if( $mbox=imap_open($authhost, $user, $pass) )
    {
  
        $status = imap_status($mbox, $authhost, SA_ALL);
  
        if($status)
        {
            $total = $status->messages;
            $nuevos = $status->unseen;
    
            $cant = ($total < $cant) ? $total:$cant;  
    
            $tabla .= "<h3>&Uacute;ltimos ".$cant." Mensajes</h3>";
            $tabla .= "<b>Total mensajes: </b>".$total."<br />";  
            $tabla .= "<b>Nuevos: </b>".$nuevos."<br /><br />"; 
     
            $tabla .= "<i>(S&oacute;lo se muestran los correos con archivos adjuntos v&aacute;lidos)</i><br /><br />";   
            $tabla .= "</td></tr>";
    
            if(!is_null($total) && $total > 0)
            {
                for($i=$total; $i>$total-$cant; $i--)
                { 
                    $header = imap_header($mbox, $i);
                    $structure = imap_fetchstructure($mbox, $i);
                    $msgno = $header->Msgno; 
        
                    $attachments = array();
                    
                    if(isset($structure->parts) && count($structure->parts))
                    {
                        for($j = 0; $j < count($structure->parts); $j++)
                        {
                    	   $attachments[$j] = array(
                    			'is_attachment' => false,
                    			'filename' => '',
                    			'name' => '',
                    			'attachment' => ''
                    		);
        		
                            if($structure->parts[$j]->ifdparameters)
                            {
                                foreach($structure->parts[$j]->dparameters as $object)
                                {
                                    if(strtolower($object->attribute) == 'filename')
                                    {
                                        $attachments[$j]['is_attachment'] = true;
                                        $attachments[$j]['filename'] = $object->value;
            				        }
            			         }
                            }
        		
                            if($structure->parts[$j]->ifparameters)
                            {
                                foreach($structure->parts[$j]->parameters as $object)
                                {
                                    if(strtolower($object->attribute) == 'name')
                                    {
                    					$attachments[$j]['is_attachment'] = true;
                    					$attachments[$j]['name'] = $object->value;
        				            }
        			             }
                            }
        		
                            if($attachments[$j]['is_attachment'])
                            {
                                $attachments[$j]['attachment'] = imap_fetchbody($mbox, $msgno, $j+1);
        			
                                $arrext = explode(".", $attachments[$j]['filename']);
                                $cantext = sizeof($arrext);
                                $ext = ($cantext > 1) ? $arrext[$cantext-1]:"-";
        			
                                if($structure->parts[$j]->encoding == 3)
                                {
        				            $attachments[$j]['attachment'] = base64_decode($attachments[$j]['attachment']);
                                }
                                elseif($structure->parts[$j]->encoding == 4)
                                {
        				            $attachments[$j]['attachment'] = quoted_printable_decode($attachments[$j]['attachment']);
                                }
        			         
                                $tabla .= "<tr><td>";
                                $tabla .= "<table border='0'>";
                                $tabla .= "<tr><td><b>Asunto:</b></td><td>".$header->subject."</td></tr>"; 
                                $tabla .= "<tr><td><b>Msgno:</b></td><td>".$header->Msgno."</td></tr>";    
                                $tabla .= "<tr><td><b>Visto:</b></td><td>".$header->Unseen."</td></tr>";   
                                $tabla .= "<tr><td><b>Fecha:</b></td><td>".$header->date."</td></tr>";    
                                $tabla .= "<tr><td><b>Archivo:</b></td><td>".$attachments[$j]['filename']."</td></tr>"; 
                                $tabla .= "<tr><td><b>Extension:</b></td><td>".$ext."</td></tr>";
                                $tabla .= "<tr valign='top'><td><b>Texto adjunto:</b></td><td><textarea cols='30' rows='4' readonly='readonly'>".$attachments[1]["attachment"]."</textarea></td></tr>"; 
                                $tabla .= "</table>";
              
                                $correos[$cont]["id"] = $i; 
                                $correos[$cont]["fecha"] = $header->date;
                                $correos[$cont]["adjunto"] = $attachments[1]["attachment"];
                                
                                $cont++; 
               
                                $tabla .= "</td></tr>";
                            }
                        }
                        
                    }//Fin de if(isset($structure->parts) && count($structure->parts))
                    
                }//Fin de for($i=$total; $i>$total-$cant; $i--)
                
            }//Fin de if(!is_null($total) && $total > 0)
            else
            {           
                $tabla .= "<tr><td>No hay mensajes</td></tr>";
            }  
        }
        else
        {                     
            $tabla .= "<tr><td>Error en el status...</td></tr>";
        }
  
        imap_close($mbox);
    }
    else
    {                     
      $tabla .= "<tr><td>Error en la conexi&oacute;n!</td></tr>";
    }

    $tabla .= "</table>";

    //Interfaz::asignarToken("tabla_datos", $tabla, $contenido);

    return $correos;
    
  }//Fin de leerCorreo()
  
  function procesarCorreo($datos)
  {
    $servidor = $datos["serv"]; 
  
    switch($servidor)
    {
      /*case 'gmail':
        $authhost = "{imap.gmail.com:993/imap/ssl/novalidate-cert}";
        $user = "@gmail.com";
        $pass = "";
      break;   
      case 'hotmail':
        // Hotmail no usa IMAP
        $authhost = "{pop3.live.com:995/pop3/ssl/novalidate-cert}"; 
        $user = "@hotmail.com";
        $pass = "";
      break;
      case 'yahoo':
        // Al parecer yahoo si tiene IMAP, pero aun no o puedo acceder
        $authhost = "{pop.mail.yahoo.com:995/pop3/ssl/novalidate-cert}";
        $user = ""@yahoo.com;
        $pass = "";
      break;*/  
      case 'intromedia':
        $authhost = "{imap.readyhosting.com:110/pop3/notls}INBOX";
        $user = "pruebas@intro-media.com";
        $pass = "301982";
      break;
      default:
        $authhost = "{pop3.readyhosting.com/pop3/novalidate-cert}";
        $user = "pruebas@intro-media.com";
        $pass = "301982";
      break;
    }       
    
    if($mbox=imap_open($authhost, $user, $pass))
    {        
      $list = imap_list($mbox, $authhost, "*");
      if (is_array($list))
      {
        foreach ($list as $val)
        {
          var_dump("Folder: ".imap_utf7_decode($val) . "<br />\n");
        }
      }
      else
      {
        var_dump("imap_list failed: " . imap_last_error() . "<br />\n");
      }
    
      $status = imap_status($mbox, $authhost, SA_ALL);  
  
      if($status)
      {             
        
        if($datos["eliminar_correo"] == "SI")
        {                 
          $r = imap_delete($mbox, "".$datos["id"]);
          var_dump("del: ".$r); 
        }
        else
        {         
          $r = imap_mail_move($mbox, "".$datos["id"], "INBOX.Procesados");
          var_dump("move: ".$r);
        }
      }
    
      imap_close($mbox);
    }
  }// Fin de procesarCorreo()
}
?>
