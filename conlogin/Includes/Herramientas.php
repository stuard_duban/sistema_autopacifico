<?php
/*
CARLOS H. CERON M. CARCERON@HOTMAIL.COM
JULIO 01 2008

FUNCIONES VARIAS QUE PUEDEN SER UTILIZADAS POR CUALQUIER CLASE
*/

class Herramientas
{

	function obtenerNombreMes($mes)
	{	
		$mes--;
		
		$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio",
						"Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
						
		return $meses[$mes];
	}
 
	function obtenerIPConexion()
	{
		  $ip = "No detectada";

		if(isset($_SERVER['REMOTE_ADDR']))
		{
			$ip = $_SERVER['REMOTE_ADDR'];
		}		
		else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
		{
		  $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} 
		elseif(isset($_SERVER['HTTP_VIA']))
		{
		  $ip = $_SERVER['HTTP_VIA'];
		} 		
	
		return $ip;
	}//Fin de obtenerIPConexion()
	
	function enlaceCampos($datos,$campos)
	{
		$enlace = "";
		foreach($campos as $key)
		{
			if( is_array( $datos[$key] ) )
			{
				foreach($datos[$key] as $valor)
				{
					$enlace .= "&".$key."[]=".$valor;							
				}
			}
			else
			{
				$enlace .= "&".$key."=".$datos[$key];			
			}
		}//Fin de foreach($campos as $key)
		return $enlace;
	}//Fin de enlaceCampos
	
	
 //devuelve la fecha y hora actual del sistema
 function obtenerFechaYHoraActual()
 {
    $dato = getDate();
    $hora = $dato["hours"];
    $minutos = $dato["minutes"];
    $dia = $dato["mday"];
    $mes = $dato["mon"];
    $anho = $dato["year"];
    
    if ($mes<10)
      $mes="0$mes";
    
    if ($dia<10)
      $dia="0$dia";
    
    $fecha_actual="$anho-$mes-$dia";
    $hora_actual="$hora:$minutos";
    
    $info["fecha"]=$fecha_actual;
    $info["hora"]=$hora_actual;
    
    return $info;
 }
 
 //convierte una fecha en formato yyyy-mm-dd a dd-mm-yyyy
 function convertirFecha($fecha)
 {
     $partes = explode("-",$fecha);
     return "{$partes[2]}-{$partes[1]}-{$partes[0]}";
 }
 
  // Fecha en formato dd/mm/yyyy o dd-mm-yyyy retorna la diferencia en dias
 function restaFechas($dFecIni, $dFecFin)
  {
  
      
      $dFecIni = str_replace("-","",$dFecIni);
      $dFecIni = str_replace("/","",$dFecIni);
      $dFecFin = str_replace("-","",$dFecFin);
      $dFecFin = str_replace("/","",$dFecFin);
  
      ereg( "([0-9]{1,2})([0-9]{1,2})([0-9]{2,4})", $dFecIni, $aFecIni);
      ereg( "([0-9]{1,2})([0-9]{1,2})([0-9]{2,4})", $dFecFin, $aFecFin);
  
      $date1 = mktime(0,0,0,$aFecIni[2], $aFecIni[1], $aFecIni[3]);
      $date2 = mktime(0,0,0,$aFecFin[2], $aFecFin[1], $aFecFin[3]);
  
      return round(($date2 - $date1) / (60 * 60 * 24));
  }


	
	function construirCalendario ($objeto,$nombre_formulario,$nombre_campo,$retornar=0)
	{
		global $_PATH_IMAGENES;
		
		if ($retornar==0)
           echo "<script language='JavaScript' type='text/javascript'>
		   $objeto = new dynCalendar('$objeto', 'bajarFecha','$_PATH_IMAGENES/');
		   $objeto.nom_form = '$nombre_formulario';
		   $objeto.nom_campo = '$nombre_campo';
	    	   $objeto.setMonthCombo(true);
		   $objeto.setYearCombo(true);
		   </script>";
        else
        {
           $calendario = "&nbsp;<script language='JavaScript' type='text/javascript'>
		   $objeto = new dynCalendar('$objeto', 'bajarFecha','$_PATH_IMAGENES/');
		   $objeto.nom_form = '$nombre_formulario';
		   $objeto.nom_campo = '$nombre_campo';
	    	   $objeto.setMonthCombo(true);
		   $objeto.setYearCombo(true);
		   </script>";
		 //  &nbsp;&nbsp;<input type='button' class='boton' value='Limpiar Fecha' onClick='document.$nombre_formulario.$nombre_campo.value=\"\";return false;'>";
           
		  
		   return $calendario;
        }
	}
	

	function crearSelectForma ($datos)
	{
		global $_obj_database, $_db_obj;
		
		$codigo = $datos['codigo']; // El identificador
	 	$mostrar = $datos['mostrar']; // El nombre a mostrar en el select
		
		if( $datos[key_db] != "" )
		{
			$resultado = $_db_obj[ $datos[key_db] ]->obtenerRegistrosAsociativos($datos['sql']);	
		}
		else
		{
			$resultado = $_obj_database->obtenerRegistrosAsociativos($datos['sql']);		
		}		

	 	$select =  "<select name='".$datos['nombre_campo']."' ".$datos['adicionales'].">";
	 	
    //Valor por defecto
    if($datos['valor_defecto']!=false){
      $texto = '- Seleccionar -';
      if(!empty($datos['texto_defecto'])){
        $texto = $datos['texto_defecto'];
      }
   	  $select .= "<option value='' align=center>".$texto."</option>";
   	}
		
		if( is_array($datos[opciones]) )
		{
			foreach($datos[opciones] as $valor)
			{
				$select .= "<option value='".$valor[codigo]."' align=center>".$valor[mostrar]."</option>";
			}//Fin de foreach($datos[opciones] as $valor)
			
		}//Fin de if( is_array($datos[opciones]) )
	 	//Calcula el tama�o de resultado para llenar el select en el for.
	 	$numero = sizeof($resultado);
		
		if( is_array($resultado) && $numero>0)
		{
			foreach($resultado as $res)
			{
				$codigoFila = $res[$codigo];
				$mostrarFila = $res[$mostrar];
				$mostrarFila = htmlentities($mostrarFila,ENT_QUOTES);
				$mostrarFila = substr($mostrarFila, 0, 50);
				$mostrarFila = strtolower($mostrarFila);
				$mostrarFila = ucwords($mostrarFila);
				
	 			$select .= "<option value='$codigoFila'";
				if($datos['valor'] == $codigoFila)
				{
					$select .= " selected ";
				}
				$select .= "> $mostrarFila</option>";
			} // Fin foreach($resultado as $res)
		} // Fin if ($resultado != -4)
		
		$select .= "</select>";
		
		return $select;
		
	} // Fin crearSelectForma ()
	
	function crearSelectFormaDesdeArray ($datos)
	{
		global $_obj_database;
				
		$resultado = $datos['array'];
		
	 	$select =  "<select name='".$datos['nombre_campo']."' ".$datos['adicionales'].">";
	 	if($datos['valor_defecto']!=false){
   	  $select .= "<option value='".$datos['valor_defecto']."' align=center>- Seleccionar -</option>";
   	}
	 	//Calcula el tama�o de resultado para llenar el select en el for.
	 	$numero = sizeof($resultado);
		
		if( is_array($resultado) && $numero>0)
		{
			foreach($resultado as $key=>$res)
			{
				$codigoFila = $key;
				$mostrarFila = $res;
				$mostrarFila = htmlentities($mostrarFila,ENT_QUOTES);
				$mostrarFila = substr($mostrarFila, 0, 45);
				$mostrarFila = strtolower($mostrarFila);
				$mostrarFila = ucwords($mostrarFila);
				
	 			$select .= "<option value='$codigoFila'";
				if($datos['valor'] == $codigoFila)
				{
					$select .= " selected ";
				}
				$select .= "> $mostrarFila</option>";
			} // Fin foreach($resultado as $res)
		} // Fin if ($resultado != -4)
		
		$select .= "</select>";
		
		return $select;
		
	}
	
	function crearSelectFormaDesdeArchivo($nombre_campo, $ruta_archivo, $opcion_seleccionada='', $opciones = '', $opciones_adicionales='')
	{
		$contenido_select = "";
		
		if(file_exists($ruta_archivo))
		{      
			if(is_readable($ruta_archivo))
			{  
	 			$lineas_archivo = file($ruta_archivo);
				$contenido_select .= "<select id='".$nombre_campo."' name='$nombre_campo' $opciones>";
				$contenido_select .= "<option value='' align='center'>- Seleccionar -</option>";
				if(is_array($lineas_archivo) && sizeof($lineas_archivo) > 0)
				{
					foreach($lineas_archivo as $linea_archivo)
					{
						$linea_archivo = (trim($linea_archivo));
						$opcion = explode("|", $linea_archivo);
						if($opcion[0] === "")
						{
							continue;
						}
						
						$contenido_select .= "<option value='$opcion[0]'";
						if($opcion[0] === $opcion_seleccionada) 
						{
							$contenido_select .= " selected";
						}
						$contenido_select .= ">$opcion[1]</option>";
					}
					
					$contenido_select .= $opciones_adicionales;
					$contenido_select .= "</select>";
				}
				return $contenido_select;
			}
			else
			{
				return -2; // El Archivo especificado no tiene permisos de lectura
			}
		}
		else
		{
			return -1; // El Archivo especificado no existe
		}
	} // Fin crearSelectFormaDesdeArchivo ()
	
	function trimCamposFormulario($datos)
	{
		foreach($datos as $key => $valor)
		{
			//si el campo no es un arreglo le aplica el trim
			if( !!is_array($valor) )
			{
				$datos[$key] = trim($valor);
			}
		}
		
		return $datos;
	}//Fin de trimCamposFormulario
	
	function removerCaracteresEspeciales($cadena)
	{
		$cadena = trim($cadena);
	                                                   
		$cadena = str_replace("�", "a", $cadena);
		$cadena = str_replace("�", "a", $cadena);
		$cadena = str_replace("�", "a", $cadena);
		$cadena = str_replace("�", "a", $cadena);
		$cadena = str_replace("�", "a", $cadena);
		$cadena = str_replace("�", "a", $cadena);
	    $cadena = str_replace("�", "A", $cadena);
	    $cadena = str_replace("�", "A", $cadena);
	    $cadena = str_replace("�", "A", $cadena);
	    $cadena = str_replace("�", "A", $cadena);
	    $cadena = str_replace("�", "A", $cadena);
	                                            
	    $cadena = str_replace("�", "e", $cadena);
	    $cadena = str_replace("�", "e", $cadena);
	    $cadena = str_replace("�", "e", $cadena);
	    $cadena = str_replace("�", "e", $cadena);
	    $cadena = str_replace("�", "E", $cadena);
	    $cadena = str_replace("�", "E", $cadena);
	    $cadena = str_replace("�", "E", $cadena);
	    $cadena = str_replace("�", "E", $cadena);
	    
	    $cadena = str_replace("�", "i", $cadena);
	    $cadena = str_replace("�", "i", $cadena);
	    $cadena = str_replace("�", "i", $cadena);
	    $cadena = str_replace("�", "i", $cadena);  
	    $cadena = str_replace("�", "I", $cadena);
	    $cadena = str_replace("�", "I", $cadena);
	    $cadena = str_replace("�", "I", $cadena);
	    $cadena = str_replace("�", "I", $cadena);
	    
	    $cadena = str_replace("�", "o", $cadena);
	    $cadena = str_replace("�", "o", $cadena);
	    $cadena = str_replace("�", "o", $cadena);
	    $cadena = str_replace("�", "o", $cadena);
	    $cadena = str_replace("�", "o", $cadena);
	    $cadena = str_replace("�", "o", $cadena); 
	    $cadena = str_replace("�", "O", $cadena);
	    $cadena = str_replace("�", "O", $cadena);
	    $cadena = str_replace("�", "O", $cadena);
	    $cadena = str_replace("�", "O", $cadena);
	    $cadena = str_replace("�", "O", $cadena);
	    
	    $cadena = str_replace("�", "u", $cadena);
	    $cadena = str_replace("�", "u", $cadena);
	    $cadena = str_replace("�", "u", $cadena);
	    $cadena = str_replace("�", "u", $cadena);
	    $cadena = str_replace("�", "U", $cadena);
	    $cadena = str_replace("�", "U", $cadena);
	    $cadena = str_replace("�", "U", $cadena);
	    $cadena = str_replace("�", "U", $cadena);
	    
	    $cadena = str_replace("^", "", $cadena);
	    $cadena = str_replace("�", "", $cadena);
	    $cadena = str_replace("`", "", $cadena);
	    $cadena = str_replace("�", "", $cadena);
	    $cadena = str_replace("~", "", $cadena);
	    
	    $cadena = str_replace("�", "c", $cadena);
	    $cadena = str_replace("�", "C", $cadena);
	    $cadena = str_replace("�", "n", $cadena);
	    $cadena = str_replace("�", "N", $cadena);
	    $cadena = str_replace("�", "Y", $cadena);
	    $cadena = str_replace("�", "y", $cadena);
	    
		return $cadena;
	}//Fin de removerCaracteresEspeciales($cadena)

	function actualizarParametro($codigo,$valor)
	{
		global $_obj_database;
		
		$estado = false;
		
		//actualiza el contador
		$sql = "update parametro 
					set par_valor = '".$valor."' 
					where par_id= '".$codigo."'";
					
		$_obj_database->ejecutarSql($sql);
		
		if ($_obj_database->obtenerNumeroFilasAfectadas()>0)
		{
			$estado = true;
		}
		
		return $estado;
	}//Fin de actualizarParametro
	
	function obtenerParametro($codigo)
	{
		global $_obj_database;
		
		$id = -1;
		
		$sql = "select par_valor  
				from parametro 
				where par_id='".$codigo."' ";
		
		$resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
		
		if( sizeof($resultado) > 0 )
		{
			$id = $resultado[0]['par_valor'];
			
		}//Fin de if( sizeof($resultado) > 0 )
		
		return $id;
	}//Fin de obtenerParametro 
	
	
	function obtenerRegistroTabla($tabla,$condicion)
	{
		global $_obj_database;
		
		$sql = "select * 
					from ".$tabla." 
					where ".$condicion;
					
		$valores = $_obj_database->obtenerRegistrosAsociativos($sql);
		return $valores[0];
	}//Fin de obtenerValor
	
	function existe($texto, $texto_busca)
    {
        return !(strpos($texto, $texto_busca) === false);
    }//Fin de existe
	
	
	function obtenerCodigoBarras($barcode)
	{
		global $_PATH_WEB;
		
		$size = 12 - strlen($barcode);
		
		$ceros = "";
		if( $size > 0 )
		{
			$ceros = array_fill(0,$size,0);
			$ceros = implode("",$ceros);
		}
		
		$barcode = $ceros.$barcode;
		
		$contenido = "<img src='".$_PATH_WEB."/barCode/barcode.php?code=".$barcode."&scale=2&mode=png' alt='".$barcode."'/>";
			
		return $contenido;
		
	}//Fin de obtenerCodigoBarras()  
	
	function obtenerFiltroLineas($campo)
	{
		$filtro = "";
		if( esTipo( array(2,3,4) ) )
		{
			$lineas = array(0);
			if( is_array($_SESSION[ealinea]) && sizeof($_SESSION[ealinea])>0 )
			{
				$lineas = $_SESSION[ealinea];
			}

			$filtro = "and ".$campo." IN (".implode(",",modificar($lineas)).") ";			
		}//Fin de if( $_SESSION[tipo_usuario] == 2 || $_SESSION[tipo_usuario] == 3 || $_SESSION[tipo_usuario] == 4)
		
		return $filtro;
	}//Fin de obtenerFiltroLineas
	
	function obtenerFiltroSias($campo)
	{
		$filtro = "";
		//si es un tipo cliente
		if( esTipo( array(2,3,4) ) )
		{
			$sias= array(0);
			if( is_array($_SESSION[sia]) && sizeof($_SESSION[sia])>0 )
			{
				$sias= $_SESSION[sia];
			}//Fin de if( is_array($_SESSION[eaagadu]) && sizeof($_SESSION[eaagadu])>0 )		
			$filtro = "AND ".$campo." IN (".implode(",", modificar($sias)).")";
		}//Fin de if( esTipo( array(2,3,4) )
				
		return $filtro;	
	}//Fin de obtenerFiltroSias
	
	
	function obtenerFiltroAgentes($campo)
	{
		$filtro = "";
		//si es un tipo cliente
		if( esTipo( array(2,3,4) ) )
		{
			$agentes = array(0);
			if( is_array($_SESSION[eaagadu]) && sizeof($_SESSION[eaagadu])>0 )
			{
				$agentes = $_SESSION[eaagadu];
			}//Fin de if( is_array($_SESSION[eaagadu]) && sizeof($_SESSION[eaagadu])>0 )		
			$filtro = "AND ".$campo." IN (".implode(",", modificar($agentes)).")";
		}//Fin de if( esTipo( array(2,3,4) )
				
		return $filtro;
	}//Fin de obtenerFiltroAgentes

	
}//class
?>
