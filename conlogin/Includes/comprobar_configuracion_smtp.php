<?php
//pgs - 27/01/2012 - script que se encarga de validar si con los datos recibidos se puede hacer una conexion SMTP
error_reporting(0);//se ocultan los mensajes de error

include_once("../Config.php"); 
include_once ("Correo.php");       

$obj_correo = new Correo();

$_mensaje_conexion_leer = '<table width="100%" cellspacing="0" cellpadding="0" border="0" class="mensaje">
                        <tr>
                            <td class="mensaje_1">
                            <img border="0" width="20" src="'.$_PATH_WEB.'/Includes/Imagenes/bien.png">
                            La configuraci&oacute;n es correcta para leer la bandeja de entrada.</td>
                        </tr>
                        </table><br>';
$_mensaje_no_conexion_leer = '<table width="100%" cellspacing="0" cellpadding="0" border="0" class="mensaje">
                        <tr>
                           <td class="mensaje_0">
                           <img border="0" width="20" src="'.$_PATH_WEB.'/Includes/Imagenes/mal.png">
                           La configuraci&oacute;n no es correcta para leer la bandeja de entrada.</td>
                        </tr>
                        </table><br>';
                        
$_mensaje_conexion_enviar = '<table width="100%" cellspacing="0" cellpadding="0" border="0" class="mensaje">
                        <tr>
                            <td class="mensaje_1">
                            <img border="0" width="20" src="'.$_PATH_WEB.'/Includes/Imagenes/bien.png">
                            La configuraci&oacute;n es correcta para enviar los emails.</td>
                        </tr>
                        </table><br>';
$_mensaje_no_conexion_enviar = '<table width="100%" cellspacing="0" cellpadding="0" border="0" class="mensaje">
                        <tr>
                            <td class="mensaje_0">
                            <img border="0" width="20" src="'.$_PATH_WEB.'/Includes/Imagenes/mal.png">
                            La configuraci&oacute;n no es correcta para enviar los emails.</td>                            
                        </tr>
                        </table><br>';

$_tipo_comprobacion = trim($_POST['tipo_comprobacion']);
$_host = trim($_POST['host']);
$_usuario = trim($_POST['usuario']);
$_clave = trim($_POST['clave']);
$_req_autenticacion = trim($_POST['req_autenticacion']);
$_puerto = trim($_POST['puerto']);
$_puerto = ($_puerto==0)?'':$_puerto;
$_ssl = trim($_POST['ssl']);
$_host_lectura = trim($_POST['host_lectura']);
$_usuario_lectura = trim($_POST['usuario_lectura']);
$_clave_lectura = trim($_POST['clave_lectura']);
$_puerto_lectura = trim($_POST['puerto_lectura']);
$_puerto_lectura = ($_puerto_lectura==0)?'':$_puerto_lectura;
$_ssl_lectura = trim($_POST['ssl_lectura']);
$_tipo_conex = trim($_POST['tipo_conex']);
$_destinatario = trim($_POST['destinatario']);   
 
//se guarda la configuracion en un array para pasarcelo a la funcion de enviar correo
$conf_smtp['host'] = $_host;
$conf_smtp['cuenta'] = $_usuario;
$conf_smtp['clave'] = $_clave;
$conf_smtp['autenticacion'] = $_req_autenticacion; 
$conf_smtp['puerto'] = $_puerto;
$conf_smtp['secure'] = ($_ssl=='1')?'ssl':'';

//$conf_smtp['secure']="tls";

if($_tipo_comprobacion=='envio'){
  if(empty($_host) || empty($_usuario) || empty($_clave)){
    echo $_mensaje_no_conexion_enviar;
  }else{
    //se verifica si con los datos se pueden enviar correos
    
    //pgs - 08/02/2012 - se saca del config el destinatario a quien le llegara un correo de que estan bien los datos SMTP
    $destinatario = $_destinatario_comprobar;
    $remitente = 'Boletines.com.co <colombia@querytek.com>';
    $asunto = 'Prueba de configuracion SMTP';
    $cuerpo = '<html><body>La presente es un correo de prueba de la configuraci&oacute;n SMTP suministrada en www.boletines.com.co:<br><br>
              <b>Host:</b> '.$_host.'<br>
              <b>Usuario:</b> '.$_usuario.'<br><br>
              Por favor no responda a este correo</body></html>';
    
  	$resultado = $obj_correo->enviarCorreo($remitente, $destinatario, $asunto, $cuerpo,null,array(),$conf_smtp);
  	if($resultado){
  	 echo $_mensaje_conexion_enviar;exit;
    }else{
      echo $_mensaje_no_conexion_enviar;exit;
    }//Fin de if($resultado) 
    
  }//Fin de if(empty($_host) || empty($_host) || empty($_host)) 
}//Fin de if($_tipo_comprobacion=='envio')

if($_tipo_comprobacion=='lectura'){
  if(empty($_host_lectura) || empty($_usuario_lectura) || empty($_clave_lectura)){
    echo $_mensaje_no_conexion_leer;
  }else{
    //se verifica si con los datos se puede hacer una conexion, para leer los correos 
    
    if($_puerto_lectura!=''){
      $_puerto_lectura = ':'.$_puerto_lectura;
    }//Fin de if($_POST['puerto']!='')
    
    if($_tipo_conex!=''){
      $_tipo_conex = '/'.strtolower($_tipo_conex);
    }//Fin de if($_POST['tipo_conex']!='')
    
    if($_ssl_lectura==1){
      $_ssl_lectura = '/ssl/novalidate-cert';
    }else{
      $_ssl_lectura = '';
    }//Fin de if($_ssl_lectura==1)
    
    $authhost = "{".$_host_lectura.$_puerto_lectura.$_tipo_conex.$_ssl_lectura."}INBOX";
    
    $mbox=@imap_open($authhost, $_usuario_lectura, $_clave_lectura);
    if($mbox){
      $status = @imap_status($mbox, $authhost, SA_ALL);
      
      if($status){ 
        echo $_mensaje_conexion_leer;
      }else{
        echo $_mensaje_no_conexion_leer;
      }//Fin de if($status)
      
      imap_close($mbox);//se cierra la conexion
    }else{
      echo $_mensaje_no_conexion_leer;
    }//Fin de if($mbox=imap_open($authhost, $_usuario, $_clave))
    
    //Fin de, se verifica si con los datos se puede hacer una conexion, para leer los correos    
  }//Fin de if(empty($_host) || empty($_host) || empty($_host)) 
}//Fin de if($_tipo_comprobacion=='lectura')
?>