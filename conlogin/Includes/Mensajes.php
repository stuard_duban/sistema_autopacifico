<?php

class Mensajes
{

	var $_contenido_mensajes;
	
	function Mensajes()
	{
		$this->_contenido_mensajes = "";
	}
	
	function crearMensaje($id_mensaje,$adicional=array())
	{
		global $_PATH_IMAGENES,$_limit_des_usuario,$_limit_des_grupo,$_EMAIL_CONTACTENOS;
		
		
		if( strlen($adicional['ACCION'])>1 )
		{
			$id_mensaje = 0;
		}
		
		switch($adicional['ACCION'])
		{
			case 0:
				$accion = "consultado";
				break;

			case 1:
				$accion = "ingresado";
				break;

			case 2:
				$accion = "modificado";
				break;

			case 3:
				$accion = "eliminado";
				break;
				
			case 4:
				$accion = "liberado";
				break;
				
			case 5:
				$accion = "enviado";
				break; 
				
			case 6:
				$accion = "anulado";
				break;
				
    	case 7:
      	$accion = "cambiado de estado";
      	break;
      
      case 8:
      	$accion = "copiado";
      	break;
			
      case 9:
      	$accion = "vaciado";
      	break;	
				
		}//Fin switch($tipo_accion_mensaje)
		
		switch($id_mensaje)
		{                
			case 'U-2':
				$mensaje = "La Contrase&ntilde;a es incorrecta!";
				$tipo_mensaje = 0;
				break;
				
			case 'U-0':
				$mensaje = "El Usuario no se encuentra registrado!";
				$tipo_mensaje = 2;
				break;
				
			case 'U-3':
				$mensaje = "El usuario ya se encuentra registrado!";
				$tipo_mensaje = 0;
				break;
				
			case 'U-4':
				$mensaje = "El Correo ingresado no corresponde al usuario!";
				$tipo_mensaje = 0;
				break;
				
			case 'U-5':
				$mensaje = "Se envi&oacute; un correo con la nueva contrase&ntilde;a de acceso.";
				$tipo_mensaje = 1;
				break;
				
			case 'U-7':
				$mensaje = "La cuenta de usuario ha sido activada correctamente";
				$tipo_mensaje = 0;
				break;
				
			case 'U-8':
				$mensaje = "La cuenta de usuario ya fue activada";
				$tipo_mensaje = 0;
				break;
				
			case 'U-9':
				$mensaje = "Sus datos fueron modificados satisfactoriamente";
				$tipo_mensaje = 0;
				break;
				
			case 'U-10':
				$mensaje = "Se presento un error al actualizar la contrase&ntilde;a por favor int&eacute;ntelo nuevamente";
				$tipo_mensaje = 0;
				break;  
				
			case 'U-11':
				$mensaje = "Los caracteres ingresados en el campo de verificaci&oacute;n no son v&aacute;lidos";
				$tipo_mensaje = 0;
				break;   
				
			case 'U-12':
				$mensaje = "La Contrase&ntilde;a no se pudo cambiar. Por favor, int&eacute;ntelo de nuevo.";
				$tipo_mensaje = 0;
				break;   
				
			case 'U-13':
				$mensaje = "La Contrase&ntilde;a se ha cambiado satisfactoriamente.";
				$tipo_mensaje = 1;
				break;   
				
    case 'U-14':
        $mensaje = "El acceso a su cuenta se encuentra suspendido desde ".$_SESSION['usu_fecha_vencimiento'].".<br>Si desea adquirir de nuevo el servicio, por favor <a href='mailto:".$_EMAIL_CONTACTENOS."'>cont&aacute;ctenos</a>.";
  			$tipo_mensaje = 0;
        break;
       
       case 'U-15':
				$mensaje = "La Contrase&ntilde;a actual es incorrecta. Por favor, int&eacute;ntelo de nuevo.";
				$tipo_mensaje = 0;
				break;  
        
        case 'U-16':
				$mensaje = "La Simulaci&oacute;n de la cuenta se ha completado.";
				$tipo_mensaje = 1;
				break;
        
        case 'U-17':
				$mensaje = "La Simulaci&oacute;n de la cuenta no se ha completado, los datos del usuario son incorrectos.";
				$tipo_mensaje = 0;
				break;
       
       case 'U-18':
				$mensaje = "No puedes tratar de simular tu misma cuenta.";
				$tipo_mensaje = 0;
				break;
        
        case 'U-19':
				$mensaje = "La Simulaci&oacute;n de la cuenta se ha terminado satisfactoriamente.";
				$tipo_mensaje = 1;
				break;
        
        case 'U-20':
				$mensaje = "No tienes permisos para esta accci&oacute;n.";
				$tipo_mensaje = 0;
				break; 
				
				 case 'U-21':
				$mensaje = "Su mensaje se ha enviado satisfatoriamente, pronto nos cont&aacute;ctaremos con usted.";
				$tipo_mensaje = 1;
				break; 
				
        //pgs - 28/06/2013 - se a�ade mensaje para solicitar demo
				case 'U-22':
				$mensaje = "Su mensaje no se ha enviado, por favor intentelo de nuevo.";
				$tipo_mensaje = 0;
				break;
        
        //pgs - 28/06/2013 - se a�ade mensaje para solicitar demo
        case 'U-23':
				$mensaje = "Su solicitud se ha enviado satisfatoriamente, pronto nos cont&aacute;ctaremos con usted.";
				$tipo_mensaje = 1;
				break; 
				
				case 'U-24':
				$mensaje = "Su solicitud no se ha enviado, por favor intentelo de nuevo.";
				$tipo_mensaje = 0;
				break; 
                
			case 'S-0':
				$mensaje = "La Sesi&oacute;n se ha cerrado por inactividad. Debe volver a ingresar.";
				$tipo_mensaje = 0;
				break;
				
		case 'US-3':
				$mensaje = "El Numero de Identificaci&oacute;n ya se encuentra registrado!";
				$tipo_mensaje = 0;
				break;
				
			case 'F-1':
				$mensaje = "El tama&ntilde;o del archivo excede el permitido.";
				$tipo_mensaje = 0;
				break;
				
			case 'F-2':
				$mensaje = "El tama&ntilde;o del archivo excede el valor especificado en el campo MAX_FILE_SIZE del formulario";
				$tipo_mensaje = 0;
				break;
				
			case 'F-3':
				$mensaje = "El archivo no fue cargado en el servidor";
				$tipo_mensaje = 0;
				break;
				
			case 'F-4':
				$mensaje = "No se seleccion&oacute; ning&uacute;n archivo";
				$tipo_mensaje = 0;
				break;
				
			case 'F-5':
				$mensaje = "El archivo que se va a copiar ya existe en el servidor";
				$tipo_mensaje = 0;
				break;
				
			case 'F-6':
				$mensaje = "El archivo no pudo ser copiado en el directorio seleccionado y con el nombre espec&iacute;fico";
				$tipo_mensaje = 0;
				break;
			
			case 'F-7':
				$mensaje = "El archivo no tiene registros v&aacute;lidos";
				$tipo_mensaje = 0;
				break;
		  
		  case 'G-0':
        $mensaje = "El Grupo se regist&oacute; satisfactoriamente, ahora puedes asociarle los destinatarios en el men&uacute; Destinatarios.";
				$tipo_mensaje = 1;
				break;
		  
      case 'G-1':
        $mensaje = "El Grupo ya se encuentra registrado, por favor int&eacute;ntelo con datos nuevos.";
				$tipo_mensaje = 2;
				break;
				
			case 'G-2':
        $mensaje = "El Grupo tiene destinatarios asociados. Para eliminar el Grupo, primero debes eliminar los destinatarios.";
				$tipo_mensaje = 2;
				break;
			
      //pgs - 10/04/2012 - se a�ade un nuevo mensaje para cuando se desee vaciar un grupo y tenga un envio activo asociado	
			case 'G-3':
        $mensaje = "El Grupo tiene un env&iacute;o asociado. Para vaciar el Grupo, debes esperar a que se termine de enviar.";
				$tipo_mensaje = 2;
				break;
      
      case 'D-0':
      	if($adicional['CADENA_NUMEROS_ERRONEOS'] !=''){
      		$mensaje = "Los siguientes n&uacute;meros de contacto no se cargaron por que presentan problemas en su estructura:<br><br><i>".$adicional['CADENA_NUMEROS_ERRONEOS']."<i>";
      		$mensaje .= "<br><br>Total de n&uacute;meros con error: ".$adicional['CANTIDAD_NUMEROS_ERRONEOS'];
      		$tipo_mensaje = 2;
      		break;
      	}else{
      		$mensaje = "Los Destinatarios se registraron satisfactoriamente, ahora puedes proceder a configurar env&iacute;os en el men&uacute; Env&iacute;os.";
      		$tipo_mensaje = 1;
      	}

				break;
      
      case 'D-1':
        $mensaje = "El Destinatario ya se encuentra registrado, por favor int&eacute;ntelo con datos nuevos.";
				$tipo_mensaje = 2;
				break;
			
      //pgs - 10/04/2012 - se a�ade nuevo mensaje para cuando se desea eliminar un destinatario y tiene un envio asociado	
		  case 'D-2':
        $mensaje = "El Destinatario no se puede eliminar por que tiene un env&iacute;o asociado.<br> Para eliminar el Destinatario, debes esperar a que se termine de enviar.";
				$tipo_mensaje = 2;
				break;
				
			case 'D-3':
        $mensaje = "Ha alcanzado el limite de destinatarios por usuario, el limite de destinatarios son ".$_limit_des_usuario;
				$tipo_mensaje = 2;
				break;
			
			case 'D-4':
        $mensaje = "Ha alcanzado el limite de destinatarios por grupo, el limite de destinatarios son ".$_limit_des_grupo;
				$tipo_mensaje = 2;
				break;
				
      		case 'E-0':
        		$mensaje = "El Env&iacute;o se registr&oacute; satisfactoriamente.";
				$tipo_mensaje = 1;
				break;
      	
			case 'E-1':
        		$mensaje = "El proceso se complet&oacute; con las siguientes especificaciones:";
				$tipo_mensaje = 2;
				break;
        
         case 'E-2':
        $mensaje = "Se eliminaron los env�os de forma satisfactoria";
				$tipo_mensaje = 1;
				break;
				
			case 'B-2':
        		$mensaje = "Para crear un Bolet&iacute;n debes crear al menos un Grupo con Destinatarios asociados. Por favor int&eacute;ntelo de nuevo.";
				$tipo_mensaje = 2;
				break;
				
			case 'B-3':
        		$mensaje = "El Bolet&iacute;n se copi&oacute; satisfactoriamente, ahora puedes proceder a editar el Bolet&iacute;n y crear el Env&iacute;o.";
				$tipo_mensaje = 1;
				break;   
				
			case 'B-4':
        		$mensaje = "El Bolet&iacute;n se registr&oacute; satisfactoriamente, ahora puedes proceder a crear el Env&iacute;o. Uno o varios de los archivos adjuntos no fueron incluidos ya que superan el tama&ntilde;o permitido.";
				$tipo_mensaje = 1;
				break;    
      	
			case 'E-5':
        		$mensaje = "Se produjo un error al crear el Env&iacute;o.";
				$tipo_mensaje = 0;
				break;     
				
			case 'B-6':
        		$mensaje = "El Bolet&iacute;n se registr&oacute; satisfactoriamente, ahora puedes proceder a crear el Env&iacute;o. Uno o varios de los archivos adjuntos no fueron incluidos ya que el archivo ya fue incluido previamente.";
				$tipo_mensaje = 1;
				break;    
      	
			case 'B-7':
        		$mensaje = "Uno de los archivos que se ha subido ya existe, por favor verifique el nombre del archivo e int&eacute;ntelo de nuevo.";
				$tipo_mensaje = 2;
				break;     
      	
			case 'E-8':
        		$mensaje = "El proceso se complet&oacute; con las siguientes especificaciones:";
				$tipo_mensaje = 2;
				break;     
      	
			case 'B-9':
        		$mensaje = "El Bolet&iacute;n se ha modificado satisfactoriamente, ahora puedes proceder a crear el Env&iacute;o.".$_SESSION["adj_log"];
				$tipo_mensaje = 2;
				break;       
      	
			case 'B-10':
        		$mensaje = $_SESSION["adj_log"];
				$tipo_mensaje = 2;
				break;   
				
			case 'I-0':
        $mensaje = "El informe a&uacute;n no est&aacute; disponible ya que los correos electr&oacute;nicos est�n siendo procesados para el env&iacute;o. <br>Se han procesado hasta el momento ".$adicional['registrados']." de ".$adicional['total'];
				$tipo_mensaje = 2;
				break;
      
      case 'C-0':
        $mensaje = "El Cliente se registr&oacute; satisfactoriamente.";
				$tipo_mensaje = 1;
				break;
		  
      case 'C-1':
        $mensaje = "El Cliente ya se encuentra registrado, por favor int&eacute;ntelo con datos nuevos.";
				$tipo_mensaje = 2;
				break;
      
      case 'E-0':
        $mensaje = "El Env&iacute;o se agend&oacute; satisfactoriamente.";
				$tipo_mensaje = 1;
				break;
				
			case 'E-1':
        $mensaje = "No puede agendar un nuevo env&iacute;o para el bolet&iacute;n debido a que ya tiene un env&iacute;o sin terminar.<br> Una vez que termine el env&iacute;o podr&aacute; agendarle al bolet&iacute;n uno nuevo.";
				$tipo_mensaje = 2;
				break;
      
      case 'DB-0':
        $mensaje = "Usted se ha dado de baja satisfactoriamente.";
				$tipo_mensaje = 1;
				break;
		  
      case 'DB-1':
        $mensaje = "El destinatario no existe en nuestros registros.";
				$tipo_mensaje = 2;
				break;
				
			case 'EP-0':
        $mensaje = "El correo de prueba del Bolet&iacute;n se envi&oacute; satisfactoriamente.";
				$tipo_mensaje = 1;
				break;
				
			case 'EP-1':
        $mensaje = "El correo de prueba del Bolet&iacute;n no se envi&oacute; satisfactoriamente.<br>Por favor intentelo de nuevo.";
				$tipo_mensaje = 0;
				break;
			
			case 'DB-2':
        $mensaje = "Usted ya se dio de baja del bolet&iacute;n.";
				$tipo_mensaje = 2;
				break;
      		
			case 'F-7':
				$mensaje = "El archivo no tiene uno de los Tipos de Archivo esperados";
				$tipo_mensaje = 0;
				break;
			
      case 'SUS-1':
        $mensaje = "Se ha suscrito satisfactoriamente a la lista de distribuci&oacute;n.<br />A continuaci&oacute;n le llegar&aacute; un email con una url para activar su correo electr&oacute;nico.";
				$tipo_mensaje = 1;
        break;
      
      case 'SUS-3':
        $mensaje = "El correo elect&oacute;nico ya est&aacute; suscrito a la lista de distribuci&oacute;n.";
				$tipo_mensaje = 2;
        break;
        
      case 'SUS-4':
        $mensaje = "El correo elect&oacute;nico ya est&aacute; suscrito a la lista de distribuci&oacute;n, pero no esta activado enviaremos de nuevo el correo de activaci&oacute;n.<br />";
				$tipo_mensaje = 2;
        break;
      	
			case 1:
				$imagen = "icon_exito.jpg";
				$tipo_mensaje = 1;
				$mensaje = $adicional['CADENA']." se ha ".$accion." satisfactoriamente.";
				break;

			case 2:
				$imagen = "icon_error.jpg";
				$tipo_mensaje = 0;
				$mensaje = $adicional['CADENA']." no ha sido ".$accion.". Debido a que ya existe un elemento igual en la Base de Datos.";
				break;

			case -1000:
				$imagen = "icon_error.jpg";
				$tipo_mensaje = 0;
				$mensaje = $adicional['CADENA']." no ha sido ".$accion.". Debido a un error en la Conexi&oacute;n en la Base De Datos.";
				break;

			case -1001:
				$imagen = "icon_error.jpg";
				$tipo_mensaje = 0;
				$mensaje = $adicional['CADENA']." no ha sido ".$accion.". Debido a que no se ha asignado el nombre de la tabla.";
				break;

			case -1002:
				$imagen = "icon_error.jpg";
				$tipo_mensaje = 0;
				$mensaje = $adicional['CADENA']." no ha sido ".$accion.". Debido a que no se han asignado los atributos de la tabla.";
				break;

			case -1003:
				$imagen = "icon_error.jpg";
				$tipo_mensaje = 0;
				$mensaje = $adicional['CADENA']." no ha sido ".$accion.". Debido a un error en la consulta al ejecutarse en el SMBD";
				break;

			case -4:
				$imagen = "icon_error.jpg";
				$tipo_mensaje = 0;
				$mensaje = "La consulta no arroj&oacute; resultados";
				break;

			case -1005:
				$imagen = "icon_error.jpg";
				$tipo_mensaje = 0;
				$mensaje = $adicional['CADENA']." no ha sido ".$accion.". Debido a que no se pudo construir la instrucci�n SQL";
				break;
				
			case -1006:
				$imagen = "icon_error.jpg";
				$tipo_mensaje = 0;
				$mensaje = $adicional['CADENA']." no ha sido ".$accion.". Debido a un error en el env&iacute;o del correo";
				break; 
				
			case -1007:
				$imagen = "icon_error.jpg";
				$tipo_mensaje = 0;
				$mensaje = $adicional['CADENA']." no ha sido ".$accion." debido a un error. Int&eacute;ntalo de nuevo!";
				break;
			
			case -1008:
				$imagen = "icon_error.jpg";
				$tipo_mensaje = 0;
				$mensaje = "Debes seleccionar un d&iacute;a y una hora para crear la cita.";
				break;

			case -6:
				$imagen = "icon_error.jpg";
				$tipo_mensaje = 0;
				$mensaje = $adicional['CADENA']." no ha sido ".$accion.". Debido a que no se indic&oacute; una condici&oacute;n";
				break;

			case -7:
				$imagen = "icon_advertencia.jpg";
				$tipo_mensaje = 0;
				$mensaje = $adicional['CADENA'];
				break;
				
			case 0:
				$imagen = "icon_error.jpg";
				$mensaje = $adicional['ACCION'];
				$tipo_mensaje = 0;
				break;
		}
						
		if($tipo_mensaje == 0)
		{
			$imagen_mensaje = "<img src='$_PATH_IMAGENES/mal.png' border='0' width='20'>";
		}
		if($tipo_mensaje == 1)
		{
			$imagen_mensaje = "<img src='$_PATH_IMAGENES/bien.png' border='0' width='20'>";
		}
		if($tipo_mensaje == 2)
		{
			$imagen_mensaje = "<img src='$_PATH_IMAGENES/warning.png' border='0' width='20'>";
		}
		
		
		$contenido_mensaje = "<center><table border='0'>
            								<tr>
            									<td class='mensaje_".$tipo_mensaje."'>$imagen_mensaje $mensaje</td>
            								</tr>
                          </table><br></center>";
				
		if(!isset($tipo_mensaje))
		{
			$contenido_mensaje = "";
		}
		
		if( is_null($id_mensaje) )
		{
			$contenido_mensaje = "";
		}
		return $contenido_mensaje;
	}
	
	
	function crearMensajes($ids_mensajes)
	{
		if(is_array($ids_mensajes) && sizeof($ids_mensajes) > 0)
		{
			foreach($ids_mensajes as $id_mensaje)
			{
				$this->_contenido_mensajes .= $this->crearMensaje($id_mensaje);
			}
		}
		return $this->_contenido_mensajes;
	}
	
	
}
?>
