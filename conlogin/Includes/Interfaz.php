<?php
class Interfaz
{
	var $_obj_menu;
	var $_obj_mensajes;
	
	var $_l_js;
	var $_l_css;
	
	var $_plantillas;
	
	var $_contenido;
	var $_campos;
	
	var $_propiedades;

	function Interfaz()
	{
		global $_PATH_SERVIDOR;
		
		$this->_plantillas = $_PATH_SERVIDOR."Includes/Plantillas";
		
		$this->_l_js = array();
		$this->_l_css = array();
		
		$this->_obj_menu = new Menu();
		
//		$this->adicionarCSS("general.css");
		$this->adicionarCSS("menu.css");
		$this->adicionarCSS("tipTip.css");
					
		$this->adicionarJS("Miscelanea.js");
		$this->adicionarJS("jquery.js");
		$this->adicionarJS("jquery.tipTip.js");
		$this->adicionarJS("jquery.tipTip.minified.js");
		
		$this->_contenido = array();
		
		$this->_propiedades = "";
		
	}//Fin de Interfaz()
	
	function asignarCampos($campos)
	{
		$this->_campos = $campos;
	}//Fin de asignarCampos()
	
	function asignarPropiedades($propiedad)
	{
		$this->_propiedades = $propiedad;
	}//Fin de asignarPropiedades()
	
	function adicionarJS($script)
	{
		$this->_l_js[] = $script;
		
	}//Fin de adicionarJS()
	
	
	function adicionarCSS($estilo)
	{
		$this->_l_css[] = $estilo;
		
	}//Fin de adicionarCSS()
	
	function asignarContenido($contenido)
	{
		global $_PATH_IMAGENES,$_PATH_WEB;
		
		$this->_contenido[0] = $contenido;
		
	}//Fin de asignarContenido()
	
	function asignarContenidoAdicional($contenido,$posicion)
	{
		$this->_contenido[$posicion] = $contenido;
	}//Fin de asignarContenido()

	/*function obtener_creditos_sms (){
	
		$URL = "http://httpgateway.sitmobile.com/ClientUtilities/credit?gateway=smpp.sitmobile.com";
	
		$login = "multioav";
		$password = "7qtbAFp4";
		$clientID = "30237";
		$accountID= "59699";
	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$URL);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "Login=$login&Password=$password&IdClient=$clientID&IdCuenta=$accountID");
		curl_setopt($ch, CURLOPT_TIMEOUT, 90);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$rs = curl_exec($ch);
		curl_close ($ch);
	
		return trim($rs);
	
	}*/
		
	function crearInterfazGrafica($datos=array())
	{
		global $_PATH_WEB, $_PATH_IMAGENES, $_PATH_ACCION, $_ext_archivo_boletin,$_dias_alerta_vencimiento;
    global $_LOGIN_SMS, $_PASSWORD_SMS, $_CLIENTID_SMS,  $_ACOUNTID_SMS;
		
		$estilos = "";
		foreach($this->_l_css as $estilo)
		{
			$estilos .= "<link href=\"".$_PATH_WEB."Includes/CSS/".$estilo."\" rel=\"stylesheet\" type=\"text/css\" />\n";

		}//Fin de foreach($this->_estilos as $estilo) 

        $extensiones_archivos = "";

        //por cada una de las extensiones de archivo permitidas
        if( is_array($_ext_archivo_boletin) ){
            $extensiones_archivos = "<script languague=\"javascript\">\nvar extArray = new Array();\n";

            for($i=0; $i<sizeof($_ext_archivo_boletin); $i++)
			{
                $extensiones_archivos .= "extArray[".$i."] = '.".$_ext_archivo_boletin[$i]."';\n";
            }//Fin de for

            $extensiones_archivos .= "</script>";
        }//Fin de if( is_array($_ext_archivo_boletin) )
                                              
		$js = $extensiones_archivos;
		foreach($this->_l_js as $script)
		{
			$js .= "<script src=\"".$_PATH_WEB."Includes/JS/".$script."\" language=\"Javascript\" type=\"text/javascript\"></script>\n";

		}//Fin de foreach($this->_js as $script)
		
		$plantilla = $datos['plantilla'];
		if( $plantilla == "" )
		{
			$plantilla = "principal";
		}
		$contenido = Archivos::obtenerContenidoArchivo($this->_plantillas."/".$plantilla.".html");
		
		if( $_SESSION['autenticado']==1 ){	
		  //$cantidad_creditos= $this->obtener_creditos_sms();
		  $fecha_vencimiento = date('d-m-Y',strtotime($_SESSION['usu_fecha_vencimiento']));
		  
      //pgs - 27/07/2012 - se crea el texto con la fecha de vencimiento de la cuenta
		  $texto_vencimiento = '';
		  if($_SESSION['tipo_usuario']!='R'){//si el tipo de usuario no es el admin
		    $clase = '';
		    $img_alerta = '';
		    
		    $fecha_alerta = date("Y-m-d", strtotime("$fecha_vencimiento -$_dias_alerta_vencimiento day")); 
		    $fecha_actual = date("Y-m-d");
		    
		    //si la efcha de vencimiento estan en los dias limites se pone en rojo el texto
		    if($fecha_actual>=$fecha_alerta){
          $clase = 'class="alerta"';
          $img_alerta = '<img src="'.$_PATH_IMAGENES.'warning.png" width="15" />'; 
        }//Fin de if($fecha_actual>=$fecha_alerta)
		    
		    $texto_vencimiento = '<br><br>'.$img_alerta.' <span '.$clase.'><b>Cantidad de cr&�acute;ditos:</b> </span>';
		  }//Fin de if($_SESSION['tipo_usuario']==1)
		  
		  //pgs - 30/07/2012 - funcionalidad para volver a ser el admin
		  if($_SESSION['id_admin_inicial']){
        $cambiar_usuario = '<br><a href="index.php?m=usuarios&accion=autenticar&reset=true" class="tooltip" title="Cambiar a usuario Admin">Cambiar a usuario Admin</a>';
      }//Fin de if($_SESSION['super_admin'])
		  
      //chcm. mayo 03 2017.  Consulto la cantidad de cr�ditos disponibles
      
      $creditos_disponibles=file_get_contents("http://httpgateway.sitmobile.com/ClientUtilities/credit?gateway=smpp.sitmobile.com&Login=$_LOGIN_SMS&Password=$_PASSWORD_SMS&IdCuenta=$_ACOUNTID_SMS&IdClient=$_CLIENTID_SMS");
      $creditos_disponibles = str_replace(".00","",$creditos_disponibles);
      
      $cerrar_session = '<div id="cont_gris">
                            <b>'.$_SESSION['usu_login'].'</b>&nbsp;&nbsp;&nbsp;
                            <a href="index.php?m=usuarios&accion=forma_cambiar_clave" class="tooltip" title="Cambiar Contrase&ntilde;a">Cambiar Contrase&ntilde;a</a>&nbsp;&nbsp;&nbsp;
                            <a href="index.php?m=usuarios&accion=cerrar_sesion" class="tooltip" title="Cerrar Sesion">Cerrar Sesion</a>
                            <br>
                            <h2>Cr�ditos disponibles: '.$creditos_disponibles.'</h2>
                            
                        </div>';
		  Interfaz::asignarToken("cerrar_session",$cerrar_session,$contenido);
		
			/*$info = "<table border='0' width='100%'>
						<tr>
							<td align='right' class='label_informacion'>Esta registrado como:&nbsp;".$_SESSION['usu_login']."&nbsp;</td>
						</tr>
					</table>";
			Interfaz::asignarToken("informacion",$info,$contenido);*/		
		} 
		
		
		Interfaz::asignarToken("propiedades",$this->_propiedades,$contenido);
		Interfaz::asignarToken("path_accion",$_PATH_ACCION,$contenido);
		Interfaz::asignarToken("imagenes",$_PATH_IMAGENES,$contenido);
		
		$menu = $this->_obj_menu->crearMenu($datos);
		Interfaz::asignarToken("menu",$menu,$contenido);

        if( sizeof($this->_contenido) == 0 )
        {
            $this->_contenido[] = "<center><br><br><br><h2>No tiene permisos para acceder a la 
                        opci&oacute;n seleccionada.</h2><br><br><br><a href='index.php?m=usuarios'>Continuar</a></center>
                        <br><br><br><br>";
        }//Fin de if( sizeof($this->_contenido) == 0 )
        		
		foreach($this->_contenido as $pos => $conte)
		{
		
			Interfaz::asignarToken("contenido_".$pos,$conte,$contenido);
		}
		
		if( is_array($this->_campos) )
		{
			foreach($this->_campos as $clave => $valor)
			{
				Interfaz::asignarToken($clave,$valor,$contenido);
			}
		}//Fin de if( is_array($this->_campos) )
		
		Interfaz::asignarToken("css",$estilos,$contenido);
		Interfaz::asignarToken("js",$js,$contenido);
		
		Interfaz::asignarToken("path_web",$_PATH_WEB,$contenido);

		return $contenido; 

	}//Fin de crearInterfazGrafica()
	
	function crearListado($arreglo_datos)
	{
		global $_PATH_WEB,$_PATH_ACCION;
			
		$contenido =  "";
		
		//Si el istado tiene un titulo
		$contenido .= "<div class='listado_titulo'>&nbsp;</div>";
		
		$contenido .=  $arreglo_datos['MENSAJE'];
				
		$contenido .= "<table border='0' cellpadding='2' cellspacing='0' width=100%' id='listado'>";
		
		$arreglo_datos['CAMPOS_ORDENA'] = is_array($arreglo_datos['CAMPOS_ORDENA']) ? $arreglo_datos['CAMPOS_ORDENA'] : array() ;
			
		if(count($arreglo_datos['CAMPOS']) > 0)
		{
			// coloca las etiquetas de las columnas en la tabla
			$contenido .= "<tr class='listado_columna'><td width='25' align='center' height='30'><b>No.</b></td>";
				
			foreach($arreglo_datos['CAMPOS'] as $campo => $etiqueta)
			{
				if( $etiqueta!="" )
				{
					if( in_array($campo,$arreglo_datos['CAMPOS_ORDENA'] ) )
					{	
						$ordena = $arreglo_datos['DATOS']['ordena'];
						$tipo_ordena = $arreglo_datos['DATOS']['tipo_ordena'];
						
						$adicional = "";
						$tipo_ordena_base = "";
						switch($ordena.$tipo_ordena)
						{
							case $campo:
							case $campo."ASC":
								$adicional =  "&nbsp;<img src='Includes/Imagenes/asc.png' width='15' border='0'>";
								$tipo_ordena_base = "DESC";
								break;
								
							case $campo."DESC":
								$adicional =  "&nbsp;<img src='Includes/Imagenes/desc.png' width='15' border='0'>";
								$tipo_ordena_base = "ASC";
								break;
								
						}//Fin de switch($campo)
						
						$enlace = "index.php?ordena=".$campo."&tipo_ordena=".$tipo_ordena_base."&offset=".$arreglo_datos['DATOS']['offset']."&accion=".$arreglo_datos['ACCION_LISTAR'];

						$etiqueta = "<a class='listado_columna' href=\"".$enlace."\">".$etiqueta."</a>".$adicional;
					}
					$contenido .= "<td class='listado_columna'>$etiqueta</td>";
				}
			}//Fin de foreach($arreglo_datos['CAMPOS'] as $campo => $etiqueta)
			
			if( isset($arreglo_datos['OPCIONES']['seleccionar']) )
			{
					$contenido .= "<td width=\"60\" class='listado_columna'>Opciones</td>";
			}
			
			if( isset($arreglo_datos['OPCIONES']['registro']) )
			{
					$contenido .= "<td width=\"60\" class='listado_columna'>Registro</td>";
			}
			
			if( isset($arreglo_datos['OPCIONES']['delegar']) )
			{
					$contenido .= "<td width=\"60\" class='listado_columna'>Delegar</td>";
			}
			
			if( isset($arreglo_datos['OPCIONES']['opciones']) )
			{
					$contenido .= "<td width=\"60\" class='listado_columna'>Opciones</td>";
			}                           
			
			if( isset($arreglo_datos['OPCIONES']['ver_cita']) )
			{
					$contenido .= "<td width=\"40\" class='listado_columna'></td>";
			}
			
			if( isset($arreglo_datos['OPCIONES']['ver']) )
			{
					$contenido .= "<td width=\"30\" class='listado_columna'><b>Exportar Fallidos</b></td>";
			}
			
			if( isset($arreglo_datos['OPCIONES']['estado']) )
			{
					$contenido .= "<td width=\"50\" class='listado_columna' ><b>Estado</b></td>";
			}
			
			if( isset($arreglo_datos['OPCIONES']['enviar']) )
			{
					$contenido .= "<td width=\"50\" class='listado_columna'><b>Enviar</b></td>";
			}
			
			///pgs - 29/08/2012 - nueva funcion de enviar una prueba del env�o
			if( isset($arreglo_datos['OPCIONES']['probar']) ){
					$contenido .= "<td width=\"50\" class='listado_columna'><b>Probar</b></td>";
			}//Fin de if( isset($arreglo_datos['OPCIONES']['probar']) )
			
			if( isset($arreglo_datos['OPCIONES']['envios']) )
			{
					$contenido .= "<td width=\"50\" class='listado_columna'><b>Reporte</b></td>";
			}
			
			//pgs - 22/03/2012 - nueva opcion de consultar el numero de destinatarios
			if( isset($arreglo_datos['OPCIONES']['num_destinatarios']) ){
					$contenido .= "<td width=\"115\" class='listado_columna'><b>No. Destinatarios</b></td>";
			}
			
			if( isset($arreglo_datos['OPCIONES']['autenticar']) )
			{
					$contenido .= "<td width=\"50\" class='listado_columna'><b>Autenticar</b></td>";
			}
			
			if( isset($arreglo_datos['OPCIONES']['actualizar']) )
			{
					$contenido .= "<td width=\"50\" class='listado_columna'><b>Editar</b></td>";
			}
			
			if( isset($arreglo_datos['OPCIONES']['copiar']) )
			{
					$contenido .= "<td width=\"50\" class='listado_columna'><b>Duplicar</b></td>";
			} 
			
			if( isset($arreglo_datos['OPCIONES']['eliminar']) )
			{
					$contenido .= "<td width=\"50\" class='listado_columna'><b>Eliminar</b></td>";
			}
			
			if( isset($arreglo_datos['OPCIONES']['vaciar']) )
			{
					$contenido .= "<td width=\"50\" class='listado_columna'><b>Vaciar</b></td>";
			}
			
			if( isset($arreglo_datos['OPCIONES']['seleccionar_enlace']) )
			{
					$contenido .= "<td width=\"60\" class='listado_columna'>&nbsp;</td>";
			}         
			
			if( isset($arreglo_datos['OPCIONES']['anular']) )
			{
					$contenido .= "<td width=\"50\" class='listado_columna'></td>";
			}
			
			if( isset($arreglo_datos['OPCIONES']['crear']) )
			{
					$contenido .= "<td width=\"80\" class='listado_columna'></td>";
			} 
      		
			$contenido .= "</tr>";
			
			//Si existen registros en el listado
			if( is_array($arreglo_datos['LISTADO']) && sizeof($arreglo_datos['LISTADO'])>0)
			{
		  	  
			  	$num_registro = $arreglo_datos['LISTADO']['offset'];
				
				//El tama�o del arreglo menos los campos del limit, offset, total registros y usa_filtro
				//$size = sizeof($arreglo_datos['LISTADO'])-4;
				$size = sizeof($arreglo_datos['LISTADO']);
								
				// para cada resultado obtenido
				for($i = 0; $i < $size; $i++)
				{
				
					$obj_objeto = $arreglo_datos['LISTADO'][$i];
					
		        	$estilo_fila = ($estilo_fila=="listado_fila_2"?"listado_fila_1":"listado_fila_2");
					
					$contenido .= "<tr class='$estilo_fila'><td align='center' class='listado_valor' height='20'>".($num_registro+$i+1)."</td>";
				
				
				  $estado = '';
				   
					//Coloca los valores de cada objeto
					foreach($arreglo_datos['CAMPOS'] as $campo => $etiqueta)
					{
						if($etiqueta != "")
						{
							$valor = $obj_objeto[$campo];
							
							//Si el texto es mayor a 58 caracteres entonces lo corta						
							if (strlen($valor) > 58)
							{
								$valor = substr($valor, 0, 55)."...";
							}
							
							
							
							$contenido .= "<td class='listado_valor' ".$arreglo_datos['CAMPOS_OPCIONES'][$campo]."> ".$valor."</td>";
							
						}//Fin de if($etiqueta != "")
						if($campo=='estado'){
              $estado = $obj_objeto[$campo];
            }
					}//Fin de foreach()
					

					// obtiene los atributos del objeto
					$atributos = array_keys($arreglo_datos['CAMPOS']);
					
					
					if( isset($arreglo_datos['OPCIONES']['seleccionar']) )
					{
						$enlace = $arreglo_datos['OPCIONES']['seleccionar'];
						for($k=0; $k<count($atributos); $k++)
						{
							eval('$valor = $obj_objeto['.$atributos[$k].'];');
							$enlace = str_replace("%".$atributos[$k]."%",$valor,$enlace);
						}//Fin de for($i=0; $i<count($atributos); $i++)
						$contenido .= "
						<td align=\"center\" height='38' valign='middle'>
							<a href=\"index.php?accion=".$arreglo_datos['ACCION_SELECCIONAR']."&$enlace\" >
								Seleccionar
							</a>
						</td>";
					}//Fin de if( isset($opciones['delegar']) )
					
					
					if( isset($arreglo_datos['OPCIONES']['seleccionar_enlace']) )
					{
					  $array_campos = array();
					  $enlace = $arreglo_datos['OPCIONES']['seleccionar_enlace'];
					  for($k=0; $k<count($atributos); $k++)
						{
							eval('$valor = $obj_objeto['.$atributos[$k].'];');
							$enlace = str_replace("%".$atributos[$k]."%",$valor,$enlace);
							
						}//Fin de for($i=0; $i<count($atributos); $i++)
											  
					  $posInicial = 0;
					  $posFinal = 0;
					  $posInicialV = 0;
					  $posFinalV = 0;
					  $cadena = $enlace;
					  
						for($j=0; $j<sizeof($arreglo_datos['CAMPOS']); $j++)
						{
					    $subCadena = substr($cadena, $posInicial, strlen($cadena));
					    $posFinal = strpos($subCadena, "=");
					    $campo = substr($subCadena, $posInicial, $posFinal );
					    $posInicial = $posFinal + 1;
					    $subCadena = substr($cadena, $posInicial, strlen($cadena));				    
					    $posFinal = strpos($subCadena, "&");
					    $valor_campo = substr($subCadena, 0, $posFinal );
					    $subCadena = substr($subCadena, $posFinal+1, strlen($subCadena) );
					    $cadena = $subCadena;
					    $posInicial = 0;
					    $array_campos[$campo] = $valor_campo;					    
            }            
            
            $enlace_js = "";
            foreach($array_campos as $atributo => $valor)
            {
               $enlace_js .= "window.opener.document.".$arreglo_datos['FORMA'].".".$atributo.".value='".$valor."';";           
            }
                        
						$contenido .=
						"<td align=\"center\" height='38' valign='middle'>
							<a href=\"javascript:".$enlace_js."window.close();\" >
								Seleccionar
							</a>
						</td>";
            					
					}//Fin de if( isset($opciones['delegar']) )
					
					
					if( isset($arreglo_datos['OPCIONES']['opciones']) )
					{
						$enlace = $arreglo_datos['OPCIONES']['opciones'];
						for($k=0; $k<count($atributos); $k++)
						{
							eval('$valor = $obj_objeto['.$atributos[$k].'];');
							$enlace = str_replace("%".$atributos[$k]."%",$valor,$enlace);
						}//Fin de for($i=0; $i<count($atributos); $i++)
						$contenido .= "
						<td align=\"center\" height='38' valign='middle'>
							<a href=\"index.php?accion=".$arreglo_datos['ACCION_OPCIONES']."&$enlace\" >
								<img src='".$_PATH_WEB."/Imagenes/eliminar.png' border=0 width=20 height=20 title='Registro de Opciones'>
							</a>
						</td>";
					}//Fin de if( isset($opciones['delegar']) )     
					
					
					
						if( isset($arreglo_datos['OPCIONES']['ver']) )
					{
						$enlace = $arreglo_datos['OPCIONES']['ver'];
						for($j=0; $j<count($atributos); $j++)
						{
							eval('$valor = $obj_objeto['.$atributos[$j].'];');
							$enlace = str_replace("%".$atributos[$j]."%",$valor,$enlace);
						}//Fin de for($i=0; $i<count($atributos); $i++)
  					$contenido .= "
  					<td align=\"center\">
  						<a href=\"index.php?accion=".$arreglo_datos['ACCION_VER']."&$enlace\">
                <img src='".$_PATH_WEB."/Includes/Imagenes/informe.png' width='20' class='tooltip' border=0 title='Ver el Informe'>
              </a>
  					</td>";
					}//Fin de if( isset($opciones['ver']) )
					
					if( isset($arreglo_datos['OPCIONES']['estado']) )
					{
						$enlace = $arreglo_datos['OPCIONES']['estado'];
						
						for($j=0; $j<count($atributos); $j++)
						{
							eval('$valor = $obj_objeto['.$atributos[$j].'];');
							$enlace = str_replace("%".$atributos[$j]."%",$valor,$enlace);
						}//Fin de for($i=0; $i<count($atributos); $i++)
						
						if($estado=='1'){
						  $titulo = 'Inactivar '.$arreglo_datos['TITULO_ESTADO'];
              $img = 'activo.png';
              $url_estado = '&estado=0';  
            }else{
              $titulo = 'Activar '.$arreglo_datos['TITULO_ESTADO'];
              $img = 'inactivo.png';
              $url_estado = '&estado=1';
            }
						
						$contenido .= "
						<td align=\"center\" height='20' valign='middle'>
							<a href=\"index.php?accion=".$arreglo_datos['ACCION_ESTADO']."&".$enlace.$url_estado."\">
                <img src='".$_PATH_WEB."/Includes/Imagenes/".$img."' class='tooltip' width='20' border=0 title='".$titulo."'>
              </a>
						</td>";
					}//Fin de if( isset($opciones['estado']) )
					
						if( isset($arreglo_datos['OPCIONES']['enviar']) )
					{
						$enlace = $arreglo_datos['OPCIONES']['enviar'];
						
						for($j=0; $j<count($atributos); $j++)
						{
							eval('$valor = $obj_objeto['.$atributos[$j].'];');
							$enlace = str_replace("%".$atributos[$j]."%",$valor,$enlace);
						}//Fin de for($i=0; $i<count($atributos); $i++)
						
						if($arreglo_datos['URL_MODIFICAR_COMPLETA'])
						{
							$contenido .= "
							<td align=\"center\">
								<a href=\"".$arreglo_datos['ACCION_ENVIAR']."&$enlace\">
									<img src='".$_PATH_WEB."/Includes/Imagenes/enviar.png' class='tooltip' width='20' border=0 title='Enviar'>
								</a>
							</td>";
						}
						else
						{
							$contenido .= "
							<td align=\"center\" height='20' valign='middle'>
								<a href=\"index.php?accion=".$arreglo_datos['ACCION_ENVIAR']."&$enlace\">
                  <img src='".$_PATH_WEB."/Includes/Imagenes/enviar.png' class='tooltip' width='20' border=0 title='Enviar'>
                </a>
							</td>";
						}
					}//Fin de if( isset($opciones['enviar']) )
					
					//pgs - 29/08/2012 - nueva funcion de enviar una prueba del env�o
					if( isset($arreglo_datos['OPCIONES']['probar']) ){
						$enlace = $arreglo_datos['OPCIONES']['probar'];
						
						for($j=0; $j<count($atributos); $j++){
							eval('$valor = $obj_objeto['.$atributos[$j].'];');
							$enlace = str_replace("%".$atributos[$j]."%",$valor,$enlace);
						}//Fin de for($i=0; $i<count($atributos); $i++)
						
						$contenido .= "
							<td align=\"center\" height='20' valign='middle'>
								<a href=\"index.php?accion=".$arreglo_datos['ACCION_PROBAR']."&$enlace\" rel='gb_page_center[800, 400]'>
                  <img src='".$_PATH_WEB."/Includes/Imagenes/probar.png' width='20' class='tooltip' border=0 title='Probar el Env&iacute;o'>
                </a>
							</td>";
					}//Fin de if( isset($opciones['probar']) )
					
					
					if( isset($arreglo_datos['OPCIONES']['envios']) )
					{
						$enlace = $arreglo_datos['OPCIONES']['envios'];
						
						for($j=0; $j<count($atributos); $j++)
						{
							eval('$valor = $obj_objeto['.$atributos[$j].'];');
							$enlace = str_replace("%".$atributos[$j]."%",$valor,$enlace);
						}//Fin de for($i=0; $i<count($atributos); $i++)
						
						if($arreglo_datos['URL_MODIFICAR_COMPLETA'])
						{
							$contenido .= "
							<td align=\"center\">
								<a href=\"".$arreglo_datos['ACCION_ENVIOS']."&$enlace\">
									<img src='".$_PATH_WEB."/Includes/Imagenes/envios.png' width='20' class='tooltip' border=0 title='Ver el Reporte de env&iacute;os'>
								</a>
							</td>";
						}
						else
						{
							$contenido .= "
							<td align=\"center\" height='20' valign='middle'>
								<a href=\"index.php?accion=".$arreglo_datos['ACCION_ENVIOS']."&$enlace\">
                  <img src='".$_PATH_WEB."/Includes/Imagenes/envios.png' width='20' class='tooltip' border=0 title='Ver el Reporte de env&iacute;os'>
                </a>
							</td>";
						}
					}//Fin de if( isset($opciones['actualizar']) )
					
					//pgs - 22/03/2012 - nueva opcion de consultar el numero de destinatarios
					if( isset($arreglo_datos['OPCIONES']['num_destinatarios']) ){
						$onclick = $arreglo_datos['OPCIONES']['num_destinatarios'];
						$id_conte = $arreglo_datos['OPCIONES']['id_contenido'];
						
						
						for($j=0; $j<count($atributos); $j++){
							eval('$valor = $obj_objeto['.$atributos[$j].'];');
							$onclick = str_replace("%".$atributos[$j]."%",$valor,$onclick);
							$id_conte = str_replace("%".$atributos[$j]."%",$valor,$id_conte); 
						}//Fin de for($i=0; $i<count($atributos); $i++)
						
						$contenido .= "<td align=\"center\" class=\"listado_valor\">
								<div id=\"".$id_conte."\">
                  <a href=\"\" onclick=\"".$onclick."\" >
  									<img src='".$_PATH_WEB."/Includes/Imagenes/user_info.png' width='20' class='tooltip' border=0 title='Consultar el No. de destinatarios de este grupo'>
  								</a>
								</div>
							</td>";
					}//Fin de if( isset($opciones['num_destinatarios']) )
					//pgs - 22/03/2012 - fin de, nueva opcion de consultar el numero de destinatarios
					
					//pgs - 30/07/2012 - funcionalidad para autenticarse como otro usuario
					if( isset($arreglo_datos['OPCIONES']['autenticar']) )
					{
						$enlace = $arreglo_datos['OPCIONES']['autenticar'];
						
						for($j=0; $j<count($atributos); $j++)
						{
							eval('$valor = $obj_objeto['.$atributos[$j].'];');
							$enlace = str_replace("%".$atributos[$j]."%",$valor,$enlace);
						}//Fin de for($i=0; $i<count($atributos); $i++)
						
						$contenido .= "
						<td align=\"center\" height='20' valign='middle'>
							<a href=\"index.php?accion=".$arreglo_datos['ACCION_AUTENTICAR']."&$enlace\">
                <img src='".$_PATH_WEB."/Includes/Imagenes/autenticar.png' width='20' class='tooltip' border=0 title='Autenticarse con este usuario'>
              </a>
						</td>";
					}//Fin de if( isset($opciones['autenticar']) )
					//pgs - 30/07/2012 - fin de, funcionalidad para autenticarse como otro usuario
					
					if( isset($arreglo_datos['OPCIONES']['actualizar']) )
					{
						$enlace = $arreglo_datos['OPCIONES']['actualizar'];
						
						for($j=0; $j<count($atributos); $j++)
						{
							eval('$valor = $obj_objeto['.$atributos[$j].'];');
							$enlace = str_replace("%".$atributos[$j]."%",$valor,$enlace);
						}//Fin de for($i=0; $i<count($atributos); $i++)
						
												
						if($arreglo_datos['URL_MODIFICAR_COMPLETA'])
						{
							$contenido .= "
							<td align=\"center\">
								<a href=\"".$arreglo_datos['ACCION_ACTUALIZAR']."&$enlace\">
									<img src='".$_PATH_WEB."/Includes/Imagenes/editar.png' width='20' class='tooltip' border=0 title='Editar el registro'>
								</a>
							</td>";
						}
						else
						{
							$contenido .= "
							<td align=\"center\" height='20' valign='middle'>
								<a href=\"index.php?accion=".$arreglo_datos['ACCION_ACTUALIZAR']."&$enlace\">
                  <img src='".$_PATH_WEB."/Includes/Imagenes/editar.png' width='20' class='tooltip' border=0 title='Editar el registro'>
                </a>
							</td>";
						}
					}//Fin de if( isset($opciones['actualizar']) )
					
				  if( isset($arreglo_datos['OPCIONES']['copiar']) )
					{
						$enlace = $arreglo_datos['OPCIONES']['copiar'];
						for($j=0; $j<count($atributos); $j++)
						{
							eval('$valor = $obj_objeto['.$atributos[$j].'];');
							$enlace = str_replace("%".$atributos[$j]."%",$valor,$enlace);
						}//Fin de for($i=0; $i<count($atributos); $i++)
  					$contenido .= "
  					<td align=\"center\">
  						<a href=\"index.php?accion=".$arreglo_datos['ACCION_COPIAR']."&$enlace\">
                <img src='".$_PATH_WEB."/Includes/Imagenes/copiar.png' width='20' class='tooltip' border=0 title='Duplicar el registro'>
              </a>
  					</td>";
					}//Fin de if( isset($opciones['ver']) )
					
					if( isset($arreglo_datos['OPCIONES']['eliminar']) )
					{
						$enlace = $arreglo_datos['OPCIONES']['eliminar'];
						for($k=0; $k<count($atributos); $k++)
						{
							eval('$valor = $obj_objeto['.$atributos[$k].'];');
							$enlace = str_replace("%".$atributos[$k]."%",$valor,$enlace);
						}//Fin de for($i=0; $i<count($atributos); $i++)
						
						if( $obj_objeto[acc_id]==1 )
						{
							$contenido .= "
							<td align=\"center\" height='20' valign='middle'>&nbsp;</td>";						
						}
						else
						{
							$contenido .= "
							<td align=\"center\" height='20' valign='middle'>
								<a href=\"".$_PATH_ACCION."?accion=".$arreglo_datos['ACCION_ELIMINAR']."&$enlace\" onClick=\"return confirm('Est� seguro que desea eliminar?')\">
                  <img src='".$_PATH_WEB."/Includes/Imagenes/eliminar.png' width='20' class='tooltip' border=0 title='Eliminar el registro'>
                </a>
							</td>";						
						}

					}//Fin de if( isset($opciones['eliminar']) )
					
					if( isset($arreglo_datos['OPCIONES']['vaciar']) )
					{
						$enlace = $arreglo_datos['OPCIONES']['vaciar'];
						for($k=0; $k<count($atributos); $k++)
						{
							eval('$valor = $obj_objeto['.$atributos[$k].'];');
							$enlace = str_replace("%".$atributos[$k]."%",$valor,$enlace);
						}//Fin de for($i=0; $i<count($atributos); $i++)
						
						if( $obj_objeto[acc_id]==1 )
						{
							$contenido .= "
							<td align=\"center\" height='20' valign='middle'>&nbsp;</td>";						
						}
						else
						{
							$contenido .= "
							<td align=\"center\" height='20' valign='middle'>
								<a href=\"".$_PATH_ACCION."?accion=".$arreglo_datos['ACCION_VACIAR']."&$enlace\" onClick=\"return confirm('Est� seguro que desea vaciar?')\">
                  <img src='".$_PATH_WEB."/Includes/Imagenes/vaciar.png' width='20' class='tooltip' border=0 title='Vaciar'>
                </a>
							</td>";						
						}

					}//Fin de if( isset($opciones['vaciar']) )
					
					
					if( isset($arreglo_datos['OPCIONES']['anular']) )
					{
						$enlace = $arreglo_datos['OPCIONES']['anular'];
						for($k=0; $k<count($atributos); $k++)
						{
							eval('$valor = $obj_objeto['.$atributos[$k].'];');
							$enlace = str_replace("%".$atributos[$k]."%",$valor,$enlace);
						}
  					$contenido .= "
  					<td align=\"center\" height='20' valign='middle'>
  						<a href=\"".$_PATH_ACCION."?accion=".$arreglo_datos['ACCION_ANULAR']."&$enlace\" onClick=\"return confirm('Est� seguro que desea Anular la ".$arreglo_datos['TITULO_ANULAR']."?')\">Anular</a>
  					</td>";
					}//Fin de if(isset($opciones['anular']))     
					
					
					if( isset($arreglo_datos['OPCIONES']['crear']) )
					{
						$enlace = $arreglo_datos['OPCIONES']['crear'];
						for($j=0; $j<count($atributos); $j++)
						{
							eval('$valor = $obj_objeto['.$atributos[$j].'];');
							$enlace = str_replace("%".$atributos[$j]."%",$valor,$enlace);
						}//Fin de for($i=0; $i<count($atributos); $i++)
  					$contenido .= "
  					<td align=\"center\">
  						<a href=\"index.php?accion=".$arreglo_datos['ACCION_CREAR']."&$enlace\">Crear ".$arreglo_datos['TITULO_CREAR']."</a>
  					</td>";
					}//Fin de if( isset($opciones['ver']) )
					
					
					$contenido .= "</tr>";
				
				}//Fin de for($i = 0; $i < $arreglo_datos['LISTADO']['limite']; $i++)
				
												
			}//Fin de if( is_array($arreglo_datos['LISTADO']) )
			else
			{
				if ( $arreglo_datos['LISTADO']['usa_filtro']==1 )
				{
					$contenido .= "
					<tr>
						<td colspan=".(count($arreglo_datos['CAMPOS'])+2)." align=\"center\">
							<br><h3>No existen registros para el criterio de b&uacute;squeda seleccionado</h3><br>
						</td>
					</tr>";				
				}
				else
				{
					$contenido .= "
					<tr>
						<td colspan=".(count($arreglo_datos['CAMPOS'])+2)." align=\"center\">
							<br><h3>No existen registros </h3><br>
						</td>
					</tr>";				
				}

			}
			
		}//Fin de if( count($campos)>0 )
		
				
		$contenido .= "</table>
			<center><br>".$this->crearPaginacion($arreglo_datos)."<br><br>";
		
		if ($arreglo_datos['ACCION_VOLVER']!="")
		  $contenido .= "<a href=\"index.php?accion=".$arreglo_datos['ACCION_VOLVER']."\">Volver</a></center>";
      
    	if ($arreglo_datos['ACCION_VACIAR_TABLA_ENVIOS']!="")
		     $contenido .= "<a href=\"index.php?accion=".$arreglo_datos['ACCION_VACIAR_TABLA_ENVIOS']."\" onclick=\"return confirm('�Seguro que desea eliminar el historial de env�os?')\">Eliminar Registro de Envios</a></center>";  
		
		return $contenido;
	}//Fin crearListado()
	
	
	function crearPaginacion($arreglo_datos)
	{
		global $_PATH_WEB;
		
		$contenido = "";
		
		$offset = $arreglo_datos['LISTADO']['offset'];
		$limit = $arreglo_datos['LISTADO']['limite'];
		$total_registros = $arreglo_datos['LISTADO']['num_total_registros'];
		
		
		//Enlace de anterior
		if( $offset > 0 )
		{
			$contenido .= "<a href='index.php?ordena=".$arreglo_datos['DATOS']['ordena']."&tipo_ordena=".$arreglo_datos['DATOS']['tipo_ordena']."&accion=".$arreglo_datos['ACCION_LISTAR']."&offset=".($offset-$limit)."'>&lt;&lt; Atr&aacute;s</a>&nbsp;&nbsp;&nbsp;";
			
		}//Fin de if( $offset > 0 )
				
		
		//Campos select con el numero de paginas
		if( $total_registros > $limit)
		{
			$enlace = "index.php?ordena=".$arreglo_datos['DATOS']['ordena']."&tipo_ordena=".$arreglo_datos['DATOS']['tipo_ordena']."&accion=".$arreglo_datos['ACCION_LISTAR']."&offset=";
			
			$contenido .= "<select onchange=\"location.href='".$enlace."'+this.value\">";
			
			$size = ceil($total_registros/$limit);		
			
			$pagina_actual = ceil($offset/$limit)+1;
			
			for($i=1; $i<=$size; $i++)
			{
				$selected = (  $i==$pagina_actual ? "selected" : "");
				$contenido .= "<option ".$selected." value='".(($i-1)*$limit)."'>Pag. ".$i."</option>";
			}							
			$contenido .= "</select>";
			
			
		}//Fin de if( $total_registros > $limit)
		
		//Enlace siguiente
		if( $total_registros > ($offset+$limit))
		{
			$contenido .= "&nbsp;&nbsp;&nbsp;<a href='index.php?ordena=".$arreglo_datos['DATOS']['ordena']."&tipo_ordena=".$arreglo_datos['DATOS']['tipo_ordena']."&accion=".$arreglo_datos['ACCION_LISTAR']."&offset=".($offset+$limit)."'>Adelante &gt;&gt;</a>";
			
		}//Fin deif( $total_registros > ($offset+$limit))
				
		return $contenido;
		
	}//Fin de crearPaginacion()



	function crearPopUp()
	{
		global $_PATH_WEB,$_PATH_IMAGENES;
		
		$estilos = "";
		foreach($this->_l_css as $estilo)
		{
			$estilos .= "<link href=\"".$_PATH_WEB."/Includes/CSS/".$estilo."\" rel=\"stylesheet\" type=\"text/css\" />\n";

		}//Fin de foreach($this->_estilos as $estilo)

		$js = "";
		foreach($this->_l_js as $script)
		{
			$js .= "<script src=\"".$_PATH_WEB."/Includes/JS/".$script."\" language=\"Javascript\" type=\"text/javascript\"></script>\n";

		}//Fin de foreach($this->_js as $script)
		
		$contenido = Archivos::obtenerContenidoArchivo($this->_plantillas."/popup.html");
		
		Interfaz::asignarToken("propiedades",$this->_propiedades,$contenido);
		
		Interfaz::asignarToken("imagenes",$_PATH_IMAGENES,$contenido);
		
		foreach($this->_contenido as $pos => $conte)
		{
			Interfaz::asignarToken("contenido_".$pos,$conte,$contenido);
		}
		
		Interfaz::asignarToken("css",$estilos,$contenido);
		Interfaz::asignarToken("js",$js,$contenido);
		
		Interfaz::asignarToken("path_web",$_PATH_WEB,$contenido);

		return $contenido; 

	}//Fin de crearPopUp()      



	function crearPopUpSencillo()
	{
		global $_PATH_WEB,$_PATH_IMAGENES;
		
		$estilos = "";
		foreach($this->_l_css as $estilo)
		{
			$estilos .= "<link href=\"".$_PATH_WEB."/Includes/CSS/".$estilo."\" rel=\"stylesheet\" type=\"text/css\" />\n";

		}//Fin de foreach($this->_estilos as $estilo)

		$js = "";
		foreach($this->_l_js as $script)
		{
			$js .= "<script src=\"".$_PATH_WEB."/Includes/JS/".$script."\" language=\"Javascript\" type=\"text/javascript\"></script>\n";

		}//Fin de foreach($this->_js as $script)
		
		$contenido = Archivos::obtenerContenidoArchivo($this->_plantillas."/popup_sencillo.html");
		
		Interfaz::asignarToken("propiedades",$this->_propiedades,$contenido);
		
		Interfaz::asignarToken("imagenes",$_PATH_IMAGENES,$contenido);
		
		foreach($this->_contenido as $pos => $conte)
		{
			Interfaz::asignarToken("contenido_".$pos,$conte,$contenido);
		}
		
		Interfaz::asignarToken("css",$estilos,$contenido);
		Interfaz::asignarToken("js",$js,$contenido);
		
		Interfaz::asignarToken("path_web",$_PATH_WEB,$contenido);

		return $contenido; 

	}//Fin de crearPopUpSencillo()
	
	
	function asignarToken($clave,$reemplazo,&$contenido)
	{
		$contenido =  str_replace("<!--".$clave."-->",$reemplazo,$contenido);
				
		return $contenido;		
	}//Fin de asignarToken()
	
	function asignarTokenMsgSMS($clave,$reemplazo,&$contenido)
	{
		$contenido =  str_replace("<".$clave.">",$reemplazo,$contenido);
	
		return $contenido;
	}//Fin de asignarToken()
	
	function asignarTokenDatos($clave,$datos,&$contenido)
	{
		$contenido =  str_replace("<!--".$clave."-->",$datos[$clave],$contenido);
				
		return $contenido;		
	}//Fin de asignarTokenDatos()
	
  
	
	function crearPaginacionAjax($arreglo_datos)
	{
		global $_PATH_WEB;
		
		$contenido = "";
		
		$offset = $arreglo_datos['LISTADO']['offset'];
		$limit = $arreglo_datos['LISTADO']['limite'];
		$total_registros = $arreglo_datos['LISTADO']['num_total_registros'];
		
		
		//Enlace de anterior
		if( $offset > 0 )
		{
			$contenido .= "<a href='#' onclick='cargaProductos(\"ordena=".$arreglo_datos['DATOS']['ordena']."&tipo_ordena=".$arreglo_datos['DATOS']['tipo_ordena']."&accion=".$arreglo_datos['ACCION_LISTAR']."&offset=".($offset-$limit)."\")'>&lt;&lt; Atr&aacute;s</a>&nbsp;&nbsp;&nbsp;";
			
		}//Fin de if( $offset > 0 )
				
		
		//Campos select con el numero de paginas
		if( $total_registros > $limit)
		{
			$enlace = "ordena=".$arreglo_datos['DATOS']['ordena']."&tipo_ordena=".$arreglo_datos['DATOS']['tipo_ordena']."&accion=".$arreglo_datos['ACCION_LISTAR']."&offset=";
			
			$contenido .= "<select onchange=\"cargaProductos('".$enlace."'+this.value)\">";
			
			$size = ceil($total_registros/$limit);		
			
			$pagina_actual = ceil($offset/$limit)+1;
			
			for($i=1; $i<=$size; $i++)
			{
				$selected = (  $i==$pagina_actual ? "selected" : "");
				$contenido .= "<option ".$selected." value='".(($i-1)*$limit)."'>Pag. ".$i."</option>";
			}							
			$contenido .= "</select>";
			
			
		}//Fin de if( $total_registros > $limit)
		
		//Enlace siguiente
		if( $total_registros > ($offset+$limit))
		{
			$contenido .= "&nbsp;&nbsp;&nbsp;<a href='#' onclick='cargaProductos(\"ordena=".$arreglo_datos['DATOS']['ordena']."&tipo_ordena=".$arreglo_datos['DATOS']['tipo_ordena']."&accion=".$arreglo_datos['ACCION_LISTAR']."&offset=".($offset+$limit)."\")'>Adelante &gt;&gt;</a>";
			
		}//Fin deif( $total_registros > ($offset+$limit))
				
		return $contenido;
		
	}//Fin de crearPaginacionAjax()
	
  function crearListadoSinPaginacion($arreglo_datos)
	{
		global $_PATH_WEB,$_PATH_ACCION;
			
		$contenido =  ""; 		
		$contenido .= "<div class='listado_titulo'>&nbsp;</div>";  		
    				
		$contenido .= "<table border='0' cellpadding='2' cellspacing='0' width=100%'>"; 
		$contenido .= "<tr><td align='center'>".$arreglo_datos['MENSAJE']."</td></tr>";
		$contenido .= "</table>";  
		
    $borde = (isset($arreglo_datos['OPCIONES']['borde'])) ? $arreglo_datos['OPCIONES']['borde']:"0";
		
		$contenido .= "<table border='".$borde."' cellpadding='2' cellspacing='0' width=100%'>";
		
		if(count($arreglo_datos['CAMPOS']) > 0)
		{
		
			$contenido .= "<tr class='listado_columna'><td width='30' align='center'><b>No.</b></td>";
				
			foreach($arreglo_datos['CAMPOS'] as $campo => $etiqueta)
			{
				if($etiqueta != "")
				{
					$contenido .= "<td class='listado_columna' align='center'><b>$etiqueta</b></td>";
				}
			}
      
      $contenido .= "</tr>";
			
			if(is_array($arreglo_datos['LISTADO']) && sizeof($arreglo_datos['LISTADO'])>4)
			{
			  $num_registro = 0;
				$size = sizeof($arreglo_datos['LISTADO'])-4;
								
				for($i=0; $i<$size; $i++)
				{
				  $obj_objeto = $arreglo_datos['LISTADO'][$i];
					
		      $estilo_fila = ($estilo_fila=="listado_fila_2"?"listado_fila_1":"listado_fila_2");
					$contenido .= "<tr class='$estilo_fila'><td align='center' class='listado_valor'>".($num_registro+$i+1)."</td>";
				
					foreach($arreglo_datos['CAMPOS'] as $campo => $etiqueta)
					{
						if($etiqueta != "")
						{
							$valor = $obj_objeto[$campo];							
							$contenido .= "<td class='listado_valor' ".$arreglo_datos['CAMPOS_OPCIONES'][$campo]."> ".$valor."</td>";				
						}
					} 
      
          $contenido .= "</tr>";
        }     
      }
  		else
  		{
  			if($arreglo_datos['LISTADO']['usa_filtro']==1)
  			{
  				$contenido .= "
  				<tr>
  					<td colspan=".(count($arreglo_datos['CAMPOS'])+2)." align=\"center\">
  						<br><h3>No existen registros para el criterio de b&uacute;squeda seleccionado</h3><br>
  					</td>
  				</tr>";				
  			}
  			else
  			{
  				$contenido .= "
  				<tr>
  					<td colspan=".(count($arreglo_datos['CAMPOS'])+2)." align=\"center\">
  						<br><h3>No existen registros</h3><br>
  					</td>
  				</tr>";				
  			}
  		}
  	}
  	
  	$contenido .= "</table>";
  	
		return $contenido;
	}// Fin de crearListadoSinPaginacion()
}//class
?>
