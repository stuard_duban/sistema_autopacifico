function validarFormaIngresarDestinatariosManual(forma){
	var error=""; // Errores que se presentan
	
  if(!validarNulo(forma.des_email_1.value) && !validarNulo(forma.des_email_2.value) && !validarNulo(forma.des_email_3.value) && !validarNulo(forma.des_email_4.value) && !validarNulo(forma.des_email_5.value)){
    error += "Debes ingresar al menos un email\n"; 
  }
   
  valido(forma.des_email_1);
	if(validarNulo(forma.des_email_1.value)){
    if(!validarMail(forma.des_email_1.value)){
      invalido(forma.des_email_1);
  		error += "El campo \"Email No. 1\" debe ser un Email Valido\n"; 
    }
  }
  
  valido(forma.des_email_2);
	if(validarNulo(forma.des_email_2.value)){
    if(!validarMail(forma.des_email_2.value)){
      invalido(forma.des_email_2);
  		error += "El campo \"Email No. 2\" debe ser un Email Valido\n"; 
    }
  }
  
  valido(forma.des_email_3);
	if(validarNulo(forma.des_email_3.value)){
    if(!validarMail(forma.des_email_3.value)){
      invalido(forma.des_email_3);
  		error += "El campo \"Email No. 3\" debe ser un Email Valido\n"; 
    }
  }
  
  valido(forma.des_email_4);
	if(validarNulo(forma.des_email_4.value)){
    if(!validarMail(forma.des_email_4.value)){
      invalido(forma.des_email_4);
  		error += "El campo \"Email No. 4\" debe ser un Email Valido\n"; 
    }
  }
  
  valido(forma.des_email_5);
	if(validarNulo(forma.des_email_5.value)){
    if(!validarMail(forma.des_email_5.value)){
      invalido(forma.des_email_5);
  		error += "El campo \"Email No. 5\" debe ser un Email Valido\n"; 
    }
  }
	
	return desplegarResultadoValidacion(error);
}//Fin de validarFormaIngresarDestinatariosManual()

function validarFormaIngresarDestinatariosArchivo(forma)
{
	 var error=""; // Errores que se presentan
	 
	valido(forma.archivo);
	if(!validarNulo(forma.archivo.value)){
		invalido(forma.archivo);
		error += "El campo \"Archivo\" no puede estar vac�o\n";     
	}else{
    document.getElementById('boton_archivo').style.visibility = "hidden";
    document.getElementById('cargando').style.visibility = "visible";
  }
	
	return desplegarResultadoValidacion(error);
}//Fin de validarFormaIngresarDestinatariosArchivo()

function validarFormaDestinatario(forma){
  var error=""; // Errores que se presentan
	 
	valido(forma.des_email);
	if(!validarNulo(forma.des_email.value)){
		invalido(forma.des_email);
		error += "El campo \"Email\" no puede estar vac�o\n"; 
	}else{
    if(!validarMail(forma.des_email.value)){
      invalido(forma.des_email);
  		error += "El campo \"Email\" debe ser un Email Valido\n"; 
    }
  }
	
	return desplegarResultadoValidacion(error);
}