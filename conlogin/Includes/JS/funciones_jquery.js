var cat = "";

function cargarPagina(enlace, id_campo)
{
	/*var campo = document.getElementById(id_campo);
	campo.innerHTML = "<b>Procesando ...</b>";
  $.ajax({
    type: "POST",
    url: "index.php",
    data: enlace,
    success: function(msg){
      //sc = msg.extractScript();
      campo.innerHTML = msg;
      //sc.evalScript();
    }
  });*/
  
  $.ajax({
    type: "POST",
    url: "index.php",
    data: enlace,
    beforeSend: function()
    {
      $("#"+id_campo).html("<b>Procesando ...</b>");
    },
    success: function(msg)
    {
      $("#"+id_campo).html(msg);
    }
  });
}//Fin de cargarPagina()


function cargarPaginaAdicional(enlace, id_campo)
{
	var campo = document.getElementById(id_campo);
		campo.innerHTML = "<b>Procesando...</b>";
	$.ajax({
	   type: "POST",
	   url: "index.php",
	   data: enlace,
	   success: function(msg){
		   	sc = msg.extractScript();
	   		campo.innerHTML = msg;
			sc.evalScript();
	   }
	 });
}//Fin de cargarPaginaAdicional()

function cargarPaginaAdicional_2(enlace, id_campo)
{
	var campo = document.getElementById(id_campo);
		campo.innerHTML = "<b>Procesando...</b>";
	$.ajax({
	   type: "POST",
	   url: "index.php",
	   data: enlace,
	   success: function(msg){
		   	sc = msg.extractScript();
	   		campo.innerHTML = msg;
			sc.evalScript();
	   }
	 });
}//Fin de cargarPaginaAdicional_2()

function eliminarArchivoAdjunto(adj_id, cont_id)
{
	if(confirm("Realmente desea eliminar el archivo adjunto?"))
	{
		$.ajax({
		   type: "POST",
		   url: "index.php?m=boletines&accion=eliminar_adjunto_boletin&adj_id="+adj_id,
		   beforeSend: function()
		   {      
		   		$("#img_load_"+cont_id).css("display", "inline-block");
		   },
		   success: function(msg)
		   {                                     
				if(msg == "true")
				{  
		   			$("#fila_archivo_"+cont_id).html("El archivo ha sido eliminado exitosamente.");
				}
		        else
		        {    
		   			$("#fila_archivo_"+cont_id).append("El archivo no se pudo eliminar.");
				}
		   
		   		$("#img_load_"+cont_id).css("display", "none");
		   }
		});
		 
		return false;
	}
}