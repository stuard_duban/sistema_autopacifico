/*
Copyright (c) 2003-2010, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	//pgs - 26/12/2011 - se a�ade esta configuracion para que deje meter las etiquetas de html,body,head,title..
  config.fullPage = true;
  //pgs - 26/12/2011 - fin modificacion
  // Define changes to default configuration here. For example:
	config.defaultLanguage = 'es';
	config.language = 'es';
	config.scayt_sLang = 'es_ES';
	config.contentsLanguage = 'es';
	// config.uiColor = '#AADC6E';
	config.resize_enabled = false;
	
	config.toolbar = 'MyToolbar';
	config.height = 250;

	config.toolbar_MyToolbar =
		[
		    ['Source','-'],
		    ['Cut','Copy','Paste','PasteText','PasteFromWord','-', 'SpellChecker', 'Scayt'],
		    ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],		    
		    ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
		    ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote','CreateDiv'],
		    '/',		    
		    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
		    ['Link','Unlink'],
		    ['Image','Table','HorizontalRule','SpecialChar','PageBreak'],
		    ['Styles','Format','Font','FontSize'],
		    ['TextColor','BGColor']
		];

	config.enterMode = CKEDITOR.ENTER_BR;
	
	//config.filebrowserImageBrowseUrl : '../ckfinder/ckfinder.php?Type=Images',
	config.filebrowserImageBrowseUrl = 'Includes/JS/ckfinder/ckfinder.html?type=Images';
	//config.filebrowserImageUploadUrl : '../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
	config.filebrowserImageUploadUrl = 'Includes/JS/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
};
