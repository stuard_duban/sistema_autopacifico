/*
*	Nombre: Ajax.js
*	Descripci�n: Funciones para el Manejo de peticiones HTTP a trav�s de AJAX
*	Autor: Diego Alejandro Ruiz Tabares
*	E-mail: diegoale@gmail.com
*	Fecha de Creaci�n: 26-04-2006
*	Fecha de �ltima Modificaci�n: 22-05-2006
*/

/*	*********************************************	EJEMPLO DE USO	**************************************************************	*/

/*
*	- Se debe definir una funcion Javascript con un s�lo par�metro, para manipular la respuesta de la Petici�n. Por ejemplo, se define la funci�n
*	  miFuncionParaProcesarRespuesta encargada de imprimir la respuesta de la petici�n dentro de una etiqueta <DIV>.	
*	- Se debe realizar el llamado a la funci�n realizarPeticionHttp y pasarle como par�metro el nombre de la funci�n que manipula la 
*	  respuesta de la Petici�n, adem�s de los dem�s par�metros que deben pasarse. Por ejemplo, se define una funci�n llamada hacerPeticion
*	  que har� el respectivo llamado a realizarPeticionHttp cuyo par�metro final debe ser el nombre de la funci�n que se acaba de definir.
*	- Ver ejemplo m�s abajo
*/


/*	*********************************************	CREAR OBJETO AJAX	**********************************************************	*/

/*
*	Utilidad:
*		Crea el Objeto que manejar� as�ncronamente la petici�n HTTP
*	Par�metros de entrada:
*	Valores de retorno: 
*		Un objeto ActiveXObject, si el navegador es Internet Explorer
*		Un objeto XMLHttpRequest, si el navegador es diferente de Internet Explorer
*		false, en caso de que no se pueda crear el objeto
*/

function crearObjetoAjax()
{
	var objeto_ajax; //Objeto Ajax
	try
	{
		objeto_ajax = new ActiveXObject("Msxml2.XMLHTTP"); //Para Internet Explorer
	}
	catch(excepcion)
	{
		try
		{
			objeto_ajax = new ActiveXObject("Microsoft.XMLHTTP"); //Para Internet Explorer
		}
		catch(excepcion)
		{
			objeto_ajax = false;
		}
	}
	if (!objeto_ajax && typeof XMLHttpRequest!='undefined') 
	{
		objeto_ajax = new XMLHttpRequest(); //Otros navegadores diferentes de Internet Explorer
	}
	return objeto_ajax;
}

/*	****************************************************	REALIZAR PETICI�N HTTP	***********************************************************	*/

/*
*	Utilidad:
*		Realiza la Petici�n HTTP as�ncrona al Servidor
*	Par�metros de entrada: 
*		metodo_peticion -> El m�todo por el que se desea realizar la petici�n (GET o POST)
*		objetivo_peticion -> La p�gina o el script al cual va dirigido la petici�n
*		tipo_respuesta -> El tipo en el que se desea que se retornen los datos desde el Servidor (TEXTO o XML)
*		datos_post -> Los datos que se desean enviar si se realiza una petici�n POST. Estos datos deben ir encapsulados dentro de un
*		              String de la forma "dato1=valor1&dato2=valor2&...&daton=valorn". Si metodo_peticion no es POST, el valor de
*		              datos_post debe ser el String vac�o ""
*		funcion_procesar_respuesta -> Funci�n previamente definida utilizada para procesar la respuesta de la petici�n 
*	Valores de retorno:
*		true, en caso de que se haya hecho la petici�n HTTP as�ncrona
*		false, si no se pudo crear el objeto AJAX que realiza la petici�n
*/

function realizarPeticionHttp(metodo_peticion, objetivo_peticion, tipo_respuesta, datos_post, funcion_para_procesar_respuesta)
{
	var objeto_ajax = crearObjetoAjax(); //Objeto para la conexi�n as�ncrona al Servidor
	var respuesta_peticion; //Respuesta de la Petici�n
	
	if(objeto_ajax)
	{
		objeto_ajax.open(metodo_peticion, objetivo_peticion, true);
		if(metodo_peticion == "GET")
		{
			objeto_ajax.send(null);
		}
		else
		{ 
			if(metodo_peticion == "POST" && datos_post != "")
			{
				objeto_ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
				objeto_ajax.send(datos_post);
			}
		}
		objeto_ajax.onreadystatechange = function()
		{
			if(objeto_ajax.readyState == 4)
			{
				if(objeto_ajax.status == 200) //El objetivo de la Petici�n se encontr�
				{
					switch(tipo_respuesta)
					{
						case "TEXTO":
							respuesta_peticion = objeto_ajax.responseText;
							break;
						case "XML":
							respuesta_peticion = objeto_ajax.responseXML;
							break;
					}
				}
				else //El objetivo de la Petici�n no pudo ser encontrado
				{
					respuesta_peticion = -100;
				}
			}
			else
			{
				respuesta_peticion = "Cargando...";
			}
			funcion_para_procesar_respuesta(respuesta_peticion);
		} //Fin funci�n onreadystatechange
		return true;
	}
	else
	{
		return false;
	}	
}