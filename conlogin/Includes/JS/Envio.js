function validarFormaEnvios(forma){
  var error=""; // Errores que se presentan
	 
	valido(forma.env_nombre);
	if(!validarNulo(forma.env_nombre.value)){
		invalido(forma.env_nombre);
		error += "El campo \"Nombre del env\xEDo\" no puede estar vac\xEDo\n"; 
	}
	
	valido(forma.env_mensaje_original);
	if(!validarNulo(forma.env_mensaje_original.value)){
		invalido(forma.env_mensaje_original);
		error += "El campo \"Mensaje\" no puede estar vac\xEDo\n"; 
		
		
	}else{
		if (!validarCaracteres(forma.env_mensaje_original.value)){
			invalido(forma.env_mensaje_original);
			error += "El campo \"Mensaje\" no debe contener caracteres especiales"; 
		}
	}
	/***************************/
	
	return desplegarResultadoValidacion(error);
}//Fin de validarFormaEnvios(forma)

/*
pgs - 07/06/2012 - como la funcion por el momento solo valida que se escoja un grupo, la forma en que se muestran los grupos
ahora es con un select, entonces no se necesita validar, por este caso se comenta la funcion
function validarFormaEnviarBoletin(forma){
  var error=""; // Errores que se presentan
  
  var grupos = document.getElementsByName('bol_gru_id[]');
  var tamano = grupos.length;
  var grupo_seleccionado = false;
  for(i=0;i<tamano;i++){
    if(grupos[i].checked){
      grupo_seleccionado = true;
    }
  }
  
  if(!grupo_seleccionado){
    error += "Debes seleccionar al menos un Grupo\n";
  }
  
  return desplegarResultadoValidacion(error); 
}*/

function limpiarCampoFila(id_contenedor, id_campo)
{	
	var input = document.createElement("input");
	input.setAttribute("type", "file");
	input.setAttribute("name", id_campo);
	input.setAttribute("id", id_campo);
	
	document.getElementById(id_contenedor).innerHTML = "";
    document.getElementById(id_contenedor).appendChild(input);

	return false;
}

function validarExtensionArchivo(nombre_arch, array_ext)
{
	var strFile = nombre_arch.toLowerCase();
	var allowFile = false;
	
	for(var i=0; i<array_ext.length; i++)
	{
		var ext = strFile.substring(strFile.length - array_ext[i].length, strFile.length);
		
		if (array_ext[i] == ext)
		{
			allowFile = true;
			break;
		}
	}
	
	return allowFile;
}

function validarCaracteres(string){
	var expReg= /^[0-9a-zA-Z\.\,\:\;\-\_\|\¬\&\<\>\#\$\*\%\+\-\/\(\)\=\¿\?\!\¡\{\}\[\]\@\s]+$/;

	  if(!expReg.test(string)){
		    return false;
		  }else{
			  return true;
		  }
	  
	  
	
}
