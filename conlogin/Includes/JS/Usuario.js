/*	***************************************	VALIDAR FORMA AUTENTICACI�N	*****************************************	*/

/*
*	Utilidad:
*		Verifica que los datos ingresados a trav�s del formulario de Autenticaci�n sean correctos
*	Par�metros de entrada:
*	Valores de retorno: 
*		true, si los datos son correctos
*		false, uno o m�s de los datos del formulario son incorrectos.
*/

function validarFormaAutenticacion(forma)
{
	 var error=""; // Errores que se presentan
	 
	valido(forma.usu_login);
	if(!validarNulo(forma.usu_login.value))
	{
		invalido(forma.usu_login);
		error += "El campo \"Usuario\" no puede estar vac\xedo\n"; 
	}

	valido(forma.usu_contrasenha);
	if(!validarNulo(forma.usu_contrasenha.value))
	{
		invalido(forma.usu_contrasenha);
		error += "El campo \"Contrase\xf1a\" no puede estar vac\xedo\n";
	}
	
	return desplegarResultadoValidacion(error);
}//Fin de validarFormaAutenticacion()

function validarFormaCliente(forma,accion){
  var error=""; // Errores que se presentan
	 
	valido(forma.usu_nombre);
	if(!validarNulo(forma.usu_nombre.value)){
		invalido(forma.usu_nombre);
		error += "El campo \"Nombre\" no puede estar vac\xedo\n"; 
	}
	
	valido(forma.usu_login);
	if(!validarNulo(forma.usu_login.value)){
		invalido(forma.usu_login);
		error += "El campo \"Nombre de Usuario\" no puede estar vac\xedo\n"; 
	}
	
	if(accion=='fm_nuevo'){
  	valido(forma.usu_contrasenha);
  	if(!validarNulo(forma.usu_contrasenha.value)){
  		invalido(forma.usu_contrasenha);
  		error += "El campo \"Contrase\xf1a\" no puede estar vac\xedo\n"; 
  	}
  	
  	valido(forma.usu_confirmar_contrasenha);
  	if(!validarNulo(forma.usu_confirmar_contrasenha.value)){
  		invalido(forma.usu_confirmar_contrasenha);
  		error += "El campo \"Confirmar Contrase\xf1a\" no puede estar vac\xedo\n"; 
  	}else{
      if(forma.usu_contrasenha.value != forma.usu_confirmar_contrasenha.value){
        invalido(forma.usu_contrasenha);
        invalido(forma.usu_confirmar_contrasenha);
  		  error += "El campo \"Contrase\xf1a\" y \"Confirmar Contrase\xf1a\" deben ser iguales\n";
      }
    }
  }//Fin de if(accion=='fm_nuevo')
  
  if(accion=='fm_editar'){
  	valido(forma.usu_contrasenha);
  	valido(forma.usu_confirmar_contrasenha);
  	if(validarNulo(forma.usu_confirmar_contrasenha.value) || validarNulo(forma.usu_contrasenha.value)){
  		if(forma.usu_contrasenha.value != forma.usu_confirmar_contrasenha.value){
        invalido(forma.usu_contrasenha);
        invalido(forma.usu_confirmar_contrasenha);
  		  error += "El campo \"Contrase\xf1a\" y \"Confirmar Contrase\xf1a\" deben ser iguales\n";
      }
  	}
  }//Fin de if(accion=='fm_editar')
	
	valido(forma.usu_correo);
	if(!validarNulo(forma.usu_correo.value)){
		invalido(forma.usu_correo);
		error += "El campo \"Email\" no puede estar vac\xedo\n"; 
	}else{
	   //pgs - 14/11/2012 - validacion de multiples correos
	  var correos = forma.usu_correo.value.split(/,/g);
    for(i=0;i<correos.length;i++){
      if(!validarNulo(correos[i])){
        invalido(forma.usu_correo);
      	error += "El correo #"+(i+1)+" no puede estar vac\xedo\n"; 
      }else{
        if(!validarMail(correos[i])){
      		invalido(forma.usu_correo);
      		error += "El correo \""+correos[i]+"\" no es un correo v�lido\n"; 
      	}//Fin de if(!validarMail(correos[i]))
      }
    }//Fin de for(i=0;i<correos.length;i++)
    //pgs - 14/11/2012 - fin de modificacion
  }
  
  //pgs - 07/06/2012 - se a�ade la validacion para el nuevo campo de limite de correos a enviar
  valido(forma.usu_limite_envio);
	if(!validarNulo(forma.usu_limite_envio.value)){
		invalido(forma.usu_limite_envio);
		error += "El campo \"Limite de correos a enviar\" no puede estar vac\xedo\n"; 
	}else{
    if(!validarNumeroEntero(forma.usu_limite_envio.value)){
  		invalido(forma.usu_limite_envio);
  		error += "El campo \"Limite de correos a enviar\" debe ser un n�mero entero mayor a 0\n"; 
  	}else{
  	 if(forma.usu_limite_envio.value==0){
    		invalido(forma.usu_limite_envio);
    		error += "El campo \"Limite de correos a enviar\" debe ser un n�mero entero mayor a 0\n";
  		}
    }
  }
  
  //pgs - 27/07/2012 - se a�ade la validacion para el nuevo campo de limite de destinatarios por cliente
  valido(forma.usu_limite_destinatarios);
	if(!validarNulo(forma.usu_limite_destinatarios.value)){
		invalido(forma.usu_limite_destinatarios);
		error += "El campo \"Limite de Destinatarios\" no puede estar vac\xedo\n"; 
	}else{
    if(!validarNumeroEntero(forma.usu_limite_destinatarios.value)){
  		invalido(forma.usu_limite_destinatarios);
  		error += "El campo \"Limite de Destinatarios\" debe ser un n�mero entero mayor a 0\n"; 
  	}else{
  	 if(forma.usu_limite_destinatarios.value==0){
    		invalido(forma.usu_limite_destinatarios);
    		error += "El campo \"Limite de Destinatarios\" debe ser un n�mero entero mayor a 0\n";
  		}
    }
  }
  
  //pgs - 27/07/2012 - se a�ade la validacion para el nuevo campo de fecha de vencimiento
  valido(forma.usu_fecha_vencimiento);
  if(!validarNulo(forma.usu_fecha_vencimiento.value)){
		invalido(forma.usu_fecha_vencimiento);
		error += "El campo \"Fecha de vencimiento de la cuenta\" no puede estar vac\xedo\n"; 
	}else{
	  if(!validarFecha(forma.usu_fecha_vencimiento.value)){
      error += "El formato de la \"Fecha de vencimiento de la cuenta\" no es el correcto, debe ser YYYY-MM-DD\n";      
    }
	}
  
  /*valido(forma.usu_correo_rebotado);
  if(validarNulo(forma.usu_correo_rebotado.value)){
		if(!validarMail(forma.usu_correo_rebotado.value)){
  		invalido(forma.usu_correo_rebotado);
  		error += "El campo \"Email Para Email Rebotados\" debe ser un correo v�lido Ej: jhon@gmail.com\n"; 
  	} 
	} */
	
	valido(forma.usu_smtp_host);
	if(!validarNulo(forma.usu_smtp_host.value)){
		invalido(forma.usu_smtp_host);
		error += "El campo \"Host SMTP\" no puede estar vac\xedo\n"; 
	}
	
	valido(forma.usu_smtp_cuenta);
	if(!validarNulo(forma.usu_smtp_cuenta.value)){
		invalido(forma.usu_smtp_cuenta);
		error += "El campo \"Cuenta SMTP\" no puede estar vac\xedo\n"; 
	}
	
	valido(forma.usu_smtp_contrasenha);
	if(!validarNulo(forma.usu_smtp_contrasenha.value)){
		invalido(forma.usu_smtp_contrasenha);
		error += "El campo \"Contrase\xf1a SMTP\" no puede estar vac\xedo\n"; 
	}
	
	valido(forma.usu_smtp_confirmar_contrasenha);
	if(!validarNulo(forma.usu_smtp_confirmar_contrasenha.value)){
		invalido(forma.usu_smtp_confirmar_contrasenha);
		error += "El campo \"Confirmar Contrase\xf1a SMTP\" no puede estar vac\xedo\n"; 
	}else{
    if(forma.usu_smtp_contrasenha.value != forma.usu_smtp_confirmar_contrasenha.value){
      invalido(forma.usu_smtp_contrasenha);
      invalido(forma.usu_smtp_confirmar_contrasenha);
		  error += "El campo \"Contrase\xf1a SMTP\" y \"Confirmar Contrase\xf1a SMTP\" deben ser iguales\n";
    }
  }
  
  valido(forma.usu_smtp_puerto);
	if(validarNulo(forma.usu_smtp_puerto.value)){
		if(!validarNumeroEntero(forma.usu_smtp_puerto.value)){
  		invalido(forma.usu_smtp_puerto);
  		error += "El campo \"Puerto SMTP\" debe ser un n�mero entero\n"; 
  	} 
	}
	
	return desplegarResultadoValidacion(error);
}


function validarAlfanumerico(val)
{
  var nLet = 0, nNum = 0; 
	var tok1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz" 
	var tok2 = "0123456789"
  
  if(val.length < 6)
  {
    return false;
  }
  else
  {
    for(var i=0; i<val.length; i++)
    { 
			if(tok1.indexOf(val.charAt(i)) != -1)
      {
        nLet++
      } 
			if(tok2.indexOf(val.charAt(i)) != -1)
      {
        nNum++
      } 
		}
     
		if(nLet>0 && nNum>0)
    {
      return true;
    } 
		else 
		{
      return false; 
    }
  }
  
  return false;
}



function validarCorreo(campo)
{
	//si tiene informacion
	if( validarNulo(campo.value) )
	{
		if(!validarMail(campo.value))
		{
			if( !confirm("El correo electr�nico no tiene el formato esperado, desea continuar?") )
			{
				campo.value="";
				campo.focus();
			}
		}	
	}//Fin de if( validarNulo(campo.value) )
}//Fin de validarCorreo


/*
* pgs - 27/01/2012 - funcion que comprueba la configuracion SMTP
*/
function comprobarConfiguracionSMTP(path,div_resultado,id_destinatario,id_host,id_usuario,id_clave,id_req_autenticacion,id_puerto,id_ssl,id_host_lectura,id_usuario_lectura,id_clave_lectura,id_puerto_lectura,id_ssl_lectura,id_tipo_conexion,tipo_comprobacion){
  //se trae los valores
  var host = $('#'+id_host).val();
  var usuario = $('#'+id_usuario).val();
  var clave = $('#'+id_clave).val();
  var req_autenticacion = $('#'+id_req_autenticacion).val();
  var puerto = $('#'+id_puerto).val();
  var ssl = $('#'+id_ssl).val();
  var host_lectura = $('#'+id_host_lectura).val();
  var usuario_lectura = $('#'+id_usuario_lectura).val();
  var clave_lectura = $('#'+id_clave_lectura).val();
  var puerto_lectura = $('#'+id_puerto_lectura).val();
  var ssl_lectura = $('#'+id_ssl_lectura).val();
  var tipo_conexion = $('#'+id_tipo_conexion).val();
  var destinatario = $('#'+id_destinatario).val();
  
  
  $('#'+div_resultado).html('<img src="'+path+'/Includes/Imagenes/circle_loader.gif">');
  
  $.ajax({
    type: "POST",
    url: path+"/Includes/comprobar_configuracion_smtp.php",
    data: 'destinatario='+destinatario+'&host='+host+'&usuario='+usuario+'&clave='+clave+'&puerto='+puerto+'&ssl='+ssl+'&req_autenticacion='+req_autenticacion+'&host_lectura='+host_lectura+'&usuario_lectura='+usuario_lectura+'&clave_lectura='+clave_lectura+'&puerto_lectura='+puerto_lectura+'&ssl_lectura='+ssl_lectura+'&tipo_conex='+tipo_conexion+'&tipo_comprobacion='+tipo_comprobacion,
    success: function(msg){
      $('#'+div_resultado).html(msg);
    }
  });
}//Fin de comprobarConfiguracionSMTP(div_resultado,host,usuario,clave,req_autenticacion,puerto,ssl,tipo_conexion)

//pgs - 30/07/2012
function validarFormaCambiarClave(forma){
  var error=""; // Errores que se presentan
	 
	valido(forma.usu_clave);
	if(!validarNulo(forma.usu_clave.value))
	{
		invalido(forma.usu_clave);
		error += "El campo \"Contrase\xf1a actual\" no puede estar vac\xedo\n"; 
	}

	valido(forma.usu_clave_nueva);
	if(!validarNulo(forma.usu_clave_nueva.value))
	{
		invalido(forma.usu_clave_nueva);
		error += "El campo \"Nueva Contrase\xf1a\" no puede estar vac\xedo\n"; 
	}
	
	valido(forma.usu_rep_clave_nueva);
	if(!validarNulo(forma.usu_rep_clave_nueva.value))
	{
		invalido(forma.usu_rep_clave_nueva);
		error += "El campo \"Repita la Nueva Contrase\xf1a\" no puede estar vac\xedo\n"; 
	}
	
	if(validarNulo(forma.usu_rep_clave_nueva.value) && validarNulo(forma.usu_clave_nueva.value)){
	  valido(forma.usu_rep_clave_nueva);
    if(forma.usu_rep_clave_nueva.value != forma.usu_clave_nueva.value){
      invalido(forma.usu_rep_clave_nueva);
      error += "El campo \"Repita la Nueva Contrase\xf1a\" no coincide con el campo \"Nueva Contrase\xf1a\" \n";
    }
  }
	
	return desplegarResultadoValidacion(error);
}//Fin de validarFormaCambiarClave(forma)

//pgs - 03/11/2012 - funcion que se envarga de validar el formulario de contactenos
function validarFormaContactenos(forma){
  var error=""; // Errores que se presentan
	 
	valido(forma.co_nombre);
	if(!validarNulo(forma.co_nombre.value))
	{
		invalido(forma.co_nombre);
		error += "El campo \"Nombre\" no puede estar vac\xedo\n"; 
	}
	
	valido(forma.co_correo);
	if(!validarNulo(forma.co_correo.value))
	{
		invalido(forma.co_correo);
		error += "El campo \"Correo Electr�nico\" no puede estar vac\xedo\n"; 
	}else{
    if(!validarMail(forma.co_correo.value)){
  		invalido(forma.co_correo);
  		error += "El campo \"Correo Electr�nico\" debe ser un correo v�lido Ej: jhon@gmail.com\n"; 
  	}
  }
  
  valido(forma.co_mensaje);
	if(!validarNulo(forma.co_mensaje.value))
	{
		invalido(forma.co_mensaje);
		error += "El campo \"Mensaje\" no puede estar vac\xedo\n"; 
	}
  
  return desplegarResultadoValidacion(error);
}//Fin de validarFormaContactenos(forma)

//pgs - 28/06/2103 - funcion que se envarga de validar el formulario de solicitud de demo
function validarFormaSolicitudDemo(forma){
  var error=""; // Errores que se presentan
	 
	valido(forma.co_nombre);
	if(!validarNulo(forma.co_nombre.value))
	{
		invalido(forma.co_nombre);
		error += "El campo \"Nombre\" no puede estar vac\xedo\n"; 
	}
	
	valido(forma.co_correo);
	if(!validarNulo(forma.co_correo.value))
	{
		invalido(forma.co_correo);
		error += "El campo \"Correo Electr�nico\" no puede estar vac\xedo\n"; 
	}else{
    if(!validarMail(forma.co_correo.value)){
  		invalido(forma.co_correo);
  		error += "El campo \"Correo Electr�nico\" debe ser un correo v�lido Ej: jhon@gmail.com\n"; 
  	}
  }
  
  return desplegarResultadoValidacion(error);
}//Fin de validarFormaSolicitudDemo(forma)