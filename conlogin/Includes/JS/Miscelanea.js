var _actualizar_pagina = false;
var _forma_enviada = false;

/*
*	Nombre: Miscelanea.js
*	Descripci�n: Funciones genericas y basicas para la Validacion de los Formularios
*	Autor: Jhoan Alejandro Garc�a Trujillo
*	E-mail: jhoalejandro@gmail.com
*	Fecha de Creaci�n: 14-08-2006
*	Fecha de �ltima Modificaci�n: 31-08-2006
*/


/*	********************	VALIDAR NULO	************************	*/

/*
*	Utilidad:
*		Se encarga de validar si el texto ingresado por el Usuario es vac�o o nulo
*	P�rametros:
*		email -> Texto digitado por el Usuario
*	Valores de retorno:
*		true, si el texto no es vac�o o nulo
*		false, de lo contrario
*/

String.prototype.trim = function() {
	return this.replace(/^\s+|\s+$/g,"");
}

function valido(campo)
{
	campo.style.background = "#FFFFFF";	
}

function invalido(campo)
{
	campo.style.background = "#FFFFCC";	
}


function validarNulo(valor)
{
	valor = valor.trim();
	if(valor==null || valor=='' || valor=='null')
	{
		return false;
	}
	return true;
}	


/*	********************	VALIDAR MAIL	************************	*/

/*
*	Utilidad:
*		Se encarga de validar si el texto ingresado representa una Direcci�n de Correo Electr�nico
*	P�rametros:
*		email -> Texto que representa la Direcci�n de Correo Electr�nico digitada por el Usuario
*	Valores de retorno:
*		true, si el texto digitado por el Usuario representa una Direcci�n de Correo Electr�nico
*		false, de lo contrario
*/

function validarMail(email) 
{	
	var token = /^[a-z-_0-9\.]+@[a-z-_=>0-9\.]+\.[a-z]{2,3}$/
  return token.test(email);
}


/*	********************	VALIDAR N�MERO	************************	*/

/*
*	Utilidad:
*		Se encarga de validar si el texto ingresado representa un N�mero
*	P�rametros:
*		valor -> Texto que representa el N�mero digitado por el Usuario
*	Valores de retorno:
*		true, si el texto digitado por el Usuario representa un N�mero
*		false, de lo contrario
*/

function validarNumero(valor)
{
	if(isNaN(valor))
	{
		return false;
	}
	else
	{
		return true;
	}
}


/*	********************	VALIDAR N�MERO ENTERO	************************	*/

/*
*	Utilidad:
*		Se encarga de validar si el texto ingresado representa un N�mero Entero
*	P�rametros:
*		valor -> Texto que representa el N�mero Entero digitado por el Usuario
*	Valores de retorno:
*		true, si el texto digitado por el Usuario representa un N�mero Entero
*		false, de lo contrario
*/

function validarNumeroEntero(valor)
{
	var token = /^\d+$/;
	return token.test(valor);
}


/*	*************************	VALIDAR TAMA�O	*********************************	*/

/*
*	Utilidad:
*		Se encarga de validar que la entrada tenga cierto tama�o definido
*	P�rametros:
*		valor -> Valor de la Variable a la cual se le quiere verificar su Tama�o
*		num_caracteres_minimo -> N�mero m�nimo de caracteres que debe contener la Variable
*		num_caracteres_maximo -> N�mero m�ximo de caracteres que debe contener la Variable
*	Valores de retorno:
*		true, Si el valor de la Variable cumple con el Tama�o especificado
*		false, de lo contrario
*/

function validarTamanho(valor, num_caracteres_minimo, num_caracteres_maximo)
{
	var tamanho= valor.length;
	if(tamanho>=num_caracteres_minimo && (num_caracteres_maximo==null || tamanho<=num_caracteres_maximo))
	{
		return true;
	}
	return false;
}//Fin validarTamanho()	


/*	********************	VALIDAR FECHA	************************	*/

/*
*	Utilidad:
*		Se encarga de validar si el texto ingresado representa una Fecha en el formato YYYY-MM-DD
*	P�rametros:
*		fecha -> Texto que representa la Fecha digitada por el Usuario
*	Valores de retorno:
*		true, si el texto digitado por el Usuario representa una Fecha en el formato YYYY-MM-DD
*		false, de lo contrario
*/

function validarFecha(fecha)
{
	var token = /^\d{4}(-\d{1,2}){1,2}$/;
	return token.test(fecha);
}

/*	*****************************	VALIDAR FECHA ACTUAL	***********************************	*/
/*
*	Utilidad:
*		Se encarga de validar si la fecha ingresada (aaaa-mm-dd)
*	P�rametros:
*		fecha -> string con la fecha a validar
*	Valores de retorno:
*		true, la fecha ingresada es mayor a la actual
*		false, de lo contrario
*/

function validarFechaActual(fecha)
{
	var fecha_actual = new Date();
	var fecha_actual_conversion =fecha_actual.getFullYear()+"-"+(fecha_actual.getMonth() + 1)+"-"+fecha_actual.getDate();
	
	return validarRangoFechas(fecha, fecha_actual_conversion);
	
}//!validarFechaActual(fecha)

/*	********************	VALIDAR TIPO ARCHIVO	************************	*/

/*
*	Utilidad:
*		Se encarga de validar si el Archivo seleccionado es del Tipo de Archivo esperado
*	P�rametros:
*		archivo -> Texto que representa la ruta del Archivo seleccionado
*		tipo_archivo -> Extensi�n del Tipo de Archivo esperado
*	Valores de retorno:
*		true, si el Archivo seleccionado es del Tipo de Archivo esperado
*		false, de lo contrario
*/

function validarTipoArchivo(archivo, tipo_archivo)
{
	var partes_nombre_archivo = archivo.split(".");
	var extension_archivo = partes_nombre_archivo[partes_nombre_archivo.length-1].toLowerCase();
	if(extension_archivo == tipo_archivo)
	{
		return true;
	}
	else
	{
		return false;
	}
}


/*	********************	VALIDAR IMAGEN	************************	*/

/*
*	Utilidad:
*		Se encarga de validar si el Archivo seleccionado es una Imagen GIF, PNG, JPG o JPEG
*	P�rametros:
*		archivo -> Texto que representa la ruta del Archivo seleccionado
*	Valores de retorno:
*		true, si el Archivo seleccionado es una Imagen GIF, PNG, JPG o JPEG
*		false, de lo contrario
*/

function validarImagen(archivo)
{
	if(validarTipoArchivo(archivo, "gif") || validarTipoArchivo(archivo, "png") || validarTipoArchivo(archivo, "jpg") || validarTipoArchivo(archivo, "jpeg"))
	{
		return true;
	}
	else
	{
		return false;
	}
}



/*	********************	VALIDAR RANGO FECHAS	************************	*/

/*
*	Utilidad:
*		Se encarga de validar el rango entre dos Fechas dadas
*	P�rametros:
*		fecha_inicial -> L�mite Inferior del Rango
*		fecha_final -> L�mite Superior del Rango
*	Valores de retorno:
*		true, si el L�mite Superior del Rango es mayor o igual al L�mite Inferior
*		false, de lo contrario
*/

function validarRangoFechas(fecha_inicial, fecha_final)
{
	var datos_fecha_inicial = fecha_inicial.split("-"); // Datos de la Fecha Inicial
	var datos_fecha_final = fecha_final.split("-"); // Datos de la Fecha Final
  
	var anho_fecha_inicial = parseFloat(datos_fecha_inicial[0]); // A�o de la Fecha Inicial
	var mes_fecha_inicial = parseFloat(datos_fecha_inicial[1]); // Mes de la Fecha Inicial
	var dia_fecha_inicial = parseFloat(datos_fecha_inicial[2]); // D�a de la Fecha Inicial
	
	var anho_fecha_final = parseFloat(datos_fecha_final[0]); // A�o de la Fecha Final
	var mes_fecha_final = parseFloat(datos_fecha_final[1]); // Mes de la Fecha Final
	var dia_fecha_final = parseFloat(datos_fecha_final[2]); // D�a de la Fecha Final
	
	dias = dia_fecha_final + dia_fecha_inicial;
	
	if(anho_fecha_final == anho_fecha_inicial)
	{		
    if(mes_fecha_final == mes_fecha_inicial)
		{			
      if(dia_fecha_final > dia_fecha_inicial)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		if(mes_fecha_final > mes_fecha_inicial)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else if (anho_fecha_final > anho_fecha_inicial)
	{
		return true;
	}
	else
	{
		return false;
	}
}


/*	*******************	DESPLEGAR RESULTADO VALIDACI�N	************************	*/

/*
*	Utilidad:
*		Se encarga de desplegar un alert al usuario con el valor del string error
*	P�rametros:
*		error -> Texto con el mensaje construido con los errores que se presentaron en un Formulario determinado
*	Valores de retorno:
*		true, si no se present� ning�n error
*		false, de lo contrario. Adem�s presentar� en pantalla los errores que se presentaron
*/


function desplegarResultadoValidacion(error)
{
	if( _forma_enviada )
	{
		alert("El formulario ya fue enviado!");
		return false;	
	}
	
	if (error != "")
	{
		alert(error);
		return false;
	}
	else
	{
		_forma_enviada  = true;
		return true;
	}
}


/*	******************************	CONFIRMAR ELIMINAR	*********************************	*/

/*
*	Utilidad:
*		Despliega una ventana de confirmaci�n para una acci�n de eliminaci�n
*	P�rametros:
*	Valores de retorno:
*		true, si el Usuario elige la opci�n SI o YES
*		false, si el Usuario elige la opci�n NO
*/

function confirmarEliminar()
{
	respuesta = confirm ("� Desea eliminar el registro seleccionado ?");
	if (respuesta)
	{
		return true;
	}
	else
	{
		return false;
	}
}


function abrirPopUp(url, nombre, ancho, alto, x, y)
{
	var caracteristicas_ventana = "";
	caracteristicas_ventana += "dependent";
	caracteristicas_ventana += ",directories=no";
	caracteristicas_ventana += ",height="+alto;
	caracteristicas_ventana += ",hotkeys=no";
	//caracteristicas_ventana += ",innerHeight";
	//caracteristicas_ventana += ",innerWidth";
	caracteristicas_ventana += ",location=no";
	caracteristicas_ventana += ",menubar=no";
	//caracteristicas_ventana += ",outerHeight";
	caracteristicas_ventana += ",personalbar=no";
	caracteristicas_ventana += ",resizable=yes";
	caracteristicas_ventana += ",screenX=0";
	caracteristicas_ventana += ",screenY=0";
	caracteristicas_ventana += ",scrollbars=yes";
	caracteristicas_ventana += ",status=no";
	caracteristicas_ventana += ",titlebar=no";
	caracteristicas_ventana += ",toolbar=no";
	caracteristicas_ventana += ",width="+ancho;
	caracteristicas_ventana += ",z-lock=yes";
	caracteristicas_ventana += ",left="+x;
	caracteristicas_ventana += ",top="+y;
	var ventana=window.open(url, nombre, caracteristicas_ventana);
	
	//return ventana;
}//Fin abrirPopUp()


function transformarFecha($fecha)
{
	$arreglo_fecha=fecha.split('-');
	/*if($arreglo_fecha[1].length==1)
	{
		$arreglo_fecha[1]='0'+$arreglo_fecha[1];
	}
	if($arreglo_fecha[2].length==1)
	{
		$arreglo_fecha[2]='0'+$arreglo_fecha[2];
	}*/
	return new Date($arreglo_fecha[0],$arreglo_fecha[2]-1,$arreglo_fecha[1]);

}//Fin transformarFecha()








<!--
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->


function submenu( id )
{
	var objeto = document.getElementById("submenu");
	var submenu = "";
	switch(id )
	{
		case 'citas':
			submenu = "|&nbsp;<a href=\"index.php?m=citas&accion=listar_export\">Citas Exportaci&oacute;n</a>&nbsp;";
			submenu += "|&nbsp;<a href=\"index.php?m=citas&accion=listar_import\">Citas Importaci&oacute;n</a>&nbsp;";
			submenu += "|&nbsp;<a href=\"index.php?m=citas&accion=reportes\">Reportes</a>&nbsp;";
			submenu += "|";
			break;
		case 'citas_operador':
			submenu += "|&nbsp;<a href=\"index.php?m=citas&accion=reportes\">Reportes</a>&nbsp;";
			submenu += "|&nbsp;<a href=\"index.php?m=citas&accion=fm_operador\">Operaciones</a>&nbsp;";
			submenu += "|";
			break;
	}
	
	objeto.innerHTML = submenu;
}//Fin de submenu

function irA(enlace)
{
	location.href=enlace;
}