function validarFormaGrupos(forma)
{
	 var error=""; // Errores que se presentan
	 
	valido(forma.gru_nombre);
	if(!validarNulo(forma.gru_nombre.value)){
		invalido(forma.gru_nombre);
		error += "El campo \"Nombre\" no puede estar vac�o\n"; 
	}
	
	return desplegarResultadoValidacion(error);
}//Fin de validarFormaGrupos()

//psg - 22/03/2012 - funcion que se encarga de consultar el numero de destinatarios
function consultarNumDestinatarios(gru_id,conte_id){
  //se pone el loader mientras se calcula el numero de destinatarios
  $('#'+conte_id).html('<img src="Includes/Imagenes/loader.gif" border="0">');
  
  $.ajax({
    url:"index.php?m=grupos&accion=obtener_num_destinatarios&gru_id="+gru_id,
    success: function(msg){
      $('#'+conte_id).html(msg);
    }
  });
}//Fin de consultarNumDestinatarios(gru_id,conte_id)