<?php
require_once("Database.php");
require_once("Herramientas.php");
require_once("Archivos.php");
require_once("Menu.php");
require_once("Interfaz.php");
require_once("Mensajes.php");
require_once("Correo.php");


function modificar($lista)
{
	foreach($lista as $pos => $valor)
	{
		$lista[ $pos ] = "'$valor'";
	}
	return $lista;
}//Fin de modificar()

function i( $valor)
{
	var_dump($valor);
}

function validarTiempoSesion()
{ 
  global $_obj_database;
  
  $fecha_sesion = $_SESSION["ultimo_acceso"];
  $ahora = date("Y-n-j H:i:s");
  $tiempo_transcurrido = (strtotime($ahora)-strtotime($fecha_sesion));
		
	$sql = "SELECT par_valor FROM parametro WHERE par_id='TIEMPO_SESION'";
	$resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
	$tiempo_min = $resultado[0]["par_valor"];
	$tiempo_seg = $tiempo_min*60;
  
  if($tiempo_transcurrido > $tiempo_seg)
  {
    $_obj_database->cancelarTransaccion();
		session_destroy();
		unset($_SESSION);
		header('Location: index.php?m=usuarios&ac_user=admin&msg=S-0');
  }
  else
  {
    $_SESSION["ultimo_acceso"] = $ahora;
  }
}

function validarAcceso($perfiles_autorizados)
{
	//captura el perfil del usuario
	$perfil = $_SESSION['tipo_usuario'];
		
	if(sizeof($perfiles_autorizados)>0)
	{
		//verifica si el perfil se encuentra en los autorizados
		$existe = array_search($perfil, $perfiles_autorizados);
		
		if(is_numeric($existe))
		{
			//Asigna el perfil correspondiente
			$perfil = $_SESSION['tipo_usuario'];
		}
		else
		{
			//elimina el acceso de todos los perfiles
			$perfil = "-";		
		}
	}//Fin de if( sizeof($perfiles_autorizados)>0 )
	
	return $perfil;
}//Fin de validarAcceso()



function exception_error_handler($errno, $errstr, $errfile, $errline )
{
    global $_EMAIL_ADMIN,$_host_db,$_user_db,$_password_db,$_db;
    
    switch($errno)
    {
        case 2:
			$_obj_interfaz = new Interfaz();
			$contenido = "<center><br><br><h1><br><br>Se presento un error interno en la aplicaci&oacute;n</h1>
							<br><br></center>";
			$_obj_interfaz->asignarContenido($contenido);
			print_r($_obj_interfaz->crearPopUp($datos));
			exit;
			break;
        case 256:
        case 512:
        case 4096:
			$errstr = str_replace("'","",$errstr);
			
			$cuerpo = 'Se genero un error en PORTNET :<br>
				Fecha: '.date("Y-m-d H:i:s").'<br>
				Codigo error: '.$errno.'<br>
				Archivo: '.$errfile.'<br>
				Linea: '.$errline.'<br>
				Error: '.$errstr.' <br>
				Navegador: '.$_SERVER["HTTP_USER_AGENT"].'<br>
				Host Cliente: '.$_SERVER["REMOTE_HOST"].'<br>
				IP Cliente: '.$_SERVER["REMOTE_ADDR"].'<br>
				Metodo Formulario: '.$_SERVER["REQUEST_METHOD"].'<br>
				URI Peticion : '.$_SERVER["REQUEST_URI"].'
				';
			
			$cuerpo = str_replace(".",". ",$cuerpo);
	
            break;
    }//Fin de switch($errno)

    return false;
}//Fin de exception_error_handler

///set_error_handler("exception_error_handler");


	
function esTipo( $tipos )
{
	$estado = false;
	
	foreach($tipos as $tipo)
	{
		if( $tipo == $_SESSION[tipo_usuario] )
		{
			$estado = true;
			break;
		}//Fin de if( $tipo == $_SESSION[tipo_usuario] )
	}//Fin de foreach($tipos as $tipo)
	
	return $estado;
}//Fin de esTipo
?>
