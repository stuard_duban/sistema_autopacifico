<?php
require_once("Usuarios.php");

$obj_usuarios = new Usuarios();
                                          
$_obj_interfaz->adicionarJS("jquery.js");
$_obj_interfaz->adicionarJS("funciones_jquery.js");
$_obj_interfaz->adicionarJS("Usuario.js");

$_obj_interfaz->asignarCampos($_campos);

switch($datos['accion'])
{
	case 'autenticar_usuario'.validarAcceso(array()):
    $resultado = $obj_usuarios->autenticarUsuario($datos);
		header('Location: index.php?m=usuarios&msg='.$resultado);
		exit;
		break;		
		
	case 'cerrar_sesion'.validarAcceso(array()):
		session_destroy();
		unset($_SESSION);
		header('Location: index.php?m=usuarios');
		exit;
		break;
		
	case 'forma_olvido_clave'.validarAcceso(array()):			
		$datos['mensaje']= $_obj_mensajes->crearMensaje($datos['msg']);
		$contenido = $obj_usuarios->abrirFormularioOlvidoClave($datos);
		$_obj_interfaz->asignarContenido($contenido);
		break;
		
	case 'olvido_clave'.validarAcceso(array()):
		$resultado = $obj_usuarios->solicitarRecordatorioClave($datos);
		
		if(is_array($resultado) && sizeof($resultado)>0){
			$obj_correo = new Correo();
			$destinatario = $datos['acc_email'];
			$remitente = $_EMAIL_ADMIN;
			$asunto = "PORTNET - Olvido de contrase�a";
			$cuerpo = $obj_usuarios->obtenerCorreoOlvidoClave($datos, $resultado);		
			
			$obj_correo->enviarCorreo($remitente, $destinatario, $asunto, $cuerpo);
			$resultado = "U-5";
		}
		
		$opciones['CADENA'] = "El Olvid&oacute; de Contrase&ntilde;a";
		$opciones['ACCION'] = 2;  
		$datos["res"] = $resultado;
		$datos['mensaje'] = $_obj_mensajes->crearMensaje($resultado,$opciones);
		$contenido = $obj_usuarios->abrirMensajeOlvidoClave($datos);
		$_obj_interfaz->asignarContenido($contenido);
	break;
		 
	case 'forma_cambiar_clave'.validarAcceso(array("R",'U')):
   if(isset($_POST['POST'])){
      $resultado = $obj_usuarios->cambiarClave($datos);
      
      if($resultado=='U-13'){
        header('Location: index.php?m=usuarios&accion=forma_cambiar_clave&tipo_gestion=1&msg='.$resultado);
      }else{
        $datos['msg'] = $resultado;
      }
    }//Fin de if(isset($_POST['_POST']))
    
    if(isset($datos['msg'])){
  		$datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
  	}//Fin de if(isset($datos['msg']))
    
    $contenido = $obj_usuarios->abrirFormularioCambiarContrasenha($datos); 
    
    $_obj_interfaz->asignarContenido($contenido);
	break;         
		
	case 'cambio_clave'.validarAcceso(array("1", "2", "3","4","5","6","7","8","9")):
		validarTiempoSesion(); 	       
                
		$resultado = $obj_usuarios->solicitarCambioClave($datos);
		
		$opciones['CADENA'] = "El cambio de contrase&ntilde;a";
		$opciones['ACCION'] = 2;
		$datos['mensaje'] = $_obj_mensajes->crearMensaje($resultado, $opciones);
		$contenido = $obj_usuarios->abrirFormularioNuevaClave($datos);
		$_obj_interfaz->asignarContenido($contenido);
	break; 
		 
	case 'configuracion'.validarAcceso(array("1")):
		validarTiempoSesion();
		$contenido = $obj_usuarios->abrirFormularioConfiguracion($datos);
		$_obj_interfaz->asignarContenido($contenido);
	break;  
		 
	case 'config'.validarAcceso(array("1")):
		validarTiempoSesion();
         		        
		$resultado = $obj_usuarios->cambiarConfig($datos);
		
		$opciones['CADENA'] = "La Configuraci&oacute;n";
		$opciones['ACCION'] = 2;
		$datos['mensaje'] = $_obj_mensajes->crearMensaje($resultado, $opciones);
		$contenido = $obj_usuarios->abrirFormularioConfiguracion($datos);
		$_obj_interfaz->asignarContenido($contenido);
	break;    
		
	case 'listado_clientes'.validarAcceso(array("R")):
		if(isset($datos['msg'])){
			$opciones['ACCION'] = $datos['tipo_gestion'];
			$opciones['CADENA'] = 'El Cliente';
			$arreglo_datos['MENSAJE'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}//Fin de if(isset($datos['msg']))             
		
		$campos_busqueda = $obj_usuarios->campos_busqueda;
		$enlace_listado = Herramientas::enlaceCampos($datos,$campos_busqueda);		
		
		$campos_busqueda = $obj_usuarios->campos_busqueda_listado;
		$enlace_busqueda = Herramientas::enlaceCampos($datos,$campos_busqueda);		
		
		$contenido = "<div class='listado_titulo'>Listado de Clientes</div><br>
    <div class='contenido_acciones'>
    <br><div class='buttons' style='text-align:left;margin-left:10px;'><a class='positive' href='index.php?m=usuarios&accion=fm_nuevo'><img src='Includes/Imagenes/nuevo.png' width='30' border='0'> Nuevo Cliente</a></div>
    <br>
    </div>
    <br>
      <div class='contenido_formulario'>
        <form action='index.php' method='get'>
          <table align='center' border='0'>
            <tr>
              <td class='forma_label' valign='bottom'>Usuario:</td>
              <td valign='bottom'><input type='text' name='busq_usu_login' size='30' value='".$datos['busq_usu_login']."'></td>
              <td class='forma_label' valign='bottom'>&nbsp;&nbsp;Nombre:</td>
              <td valign='bottom'><input type='text' name='busq_usu_nombre' size='30' value='".$datos['busq_usu_nombre']."'></td>
              <td class='forma_label' valign='bottom'>&nbsp;&nbsp;Email:</td>
              <td valign='bottom'><input type='text' name='busq_usu_correo' size='30' value='".$datos['busq_usu_correo']."'></td>
              <td align='center' class='buttons'><br>
                <button class='positive' type='submit'>
                    <img alt='' src='Includes/Imagenes/buscar.png' width='15'> 
                    Buscar
                </button>
                <input type='hidden' name='m' value='usuarios'>
                <input type='hidden' name='accion' value='listado_clientes'>
              </td>
            </tr>        
          </table>
        </form>
        <br>
      </div>";
		
		$valores = $obj_usuarios->obtenerListado($datos);
		
		$campos = array(
      "usu_id" => "Cliente ID", 
      "usu_login" => "Cliente",
      "usu_nombre" => "Nombre",
      "usu_correo" => "Email", 
      "per_nombre" => "Perfil"
    );  
                       
		$opciones['actualizar'] = "usu_id=%usu_id%";  
		$opciones['eliminar'] = "usu_id=%usu_id%";
		//pgs - 30/07/2012 - funcionalidad para autenticarse con otro usuario
    $opciones['autenticar'] = "usu_id=%usu_id%"; 
		
		$arreglo_datos['TITULO'] = "";
    
		$campos_ordena = array("usu_login","usu_nombre", "usu_correo", "per_nombre");
						
		$arreglo_datos['CAMPOS'] = $campos;
		$arreglo_datos['CAMPOS_ORDENA'] = $campos_ordena;
		$arreglo_datos['LISTADO'] = $valores;
		$arreglo_datos['OPCIONES'] = $opciones;  
		
		$arreglo_datos['ACCION_LISTAR'] = "listado_clientes&m=usuarios";
		$arreglo_datos['ACCION_LISTAR'] .= $enlace_listado;
		
		$arreglo_datos['ACCION_ACTUALIZAR'] = "fm_editar&m=usuarios";
		$arreglo_datos['ACCION_ACTUALIZAR'] .= $enlace_busqueda;
		
		$arreglo_datos['ACCION_ELIMINAR'] = "eliminar&m=usuarios";
		$arreglo_datos['ACCION_ELIMINAR'] .= $enlace_busqueda;
		
		//pgs - 30/07/2012 - funcionalidad para autenticarse con otro usuario
		$arreglo_datos['ACCION_AUTENTICAR'] = "autenticar&m=usuarios";
		$arreglo_datos['ACCION_AUTENTICAR'] .= $enlace_busqueda;
		
		$arreglo_datos['ACCION_VOLVER'] = "";
		$arreglo_datos['DATOS'] = $datos;
		
		$contenido .= $_obj_interfaz->crearListado($arreglo_datos);
		$_obj_interfaz->asignarContenido($contenido);
	break;		 
		 
	case 'fm_nuevo'.validarAcceso(array("R")):
	//pgs - 27/07/2012 - se a�ade libreria jquery para seleccionar fechas
	 $_obj_interfaz->adicionarJS("ui.datepicker.js");
	 $_obj_interfaz->adicionarCSS("ui.datepicker.css");
	 $_obj_interfaz->adicionarCSS("ui.theme.css");
	
	 if(isset($_POST['POST'])){
      $resultado = $obj_usuarios->ingresarCliente($datos);
      
      if($resultado=='C-0'){
        header('Location: index.php?m=usuarios&accion=listado_clientes&tipo_gestion=1&msg='.$resultado);
      }else{
        $datos['msg'] = $resultado;
      }
    }//Fin de if(isset($_POST['_POST']))
    
    if(isset($datos['msg'])){
			$opciones['ACCION'] = 1;
			$opciones['CADENA'] = 'El Cliente';
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}//Fin de if(isset($datos['msg']))
    
    $contenido = $obj_usuarios->obtenerFormularioNuevo($datos); 
    
    $_obj_interfaz->asignarContenido($contenido);
	break;
  
	case 'fm_editar'.validarAcceso(array('R')):
	  //pgs - 27/07/2012 - se a�ade libreria jquery para seleccionar fechas
	  $_obj_interfaz->adicionarJS("ui.datepicker.js");
	  $_obj_interfaz->adicionarCSS("ui.datepicker.css");
	  $_obj_interfaz->adicionarCSS("ui.theme.css");
	  
    if(isset($_POST['POST'])){
    
      $resultado = $obj_usuarios->editarCliente($datos);
      
      if($resultado==1){
        header('Location: index.php?m=usuarios&accion=listado_clientes&tipo_gestion=2&msg='.$resultado);
      }else{
        $datos['msg'] = $resultado;
      }
    }//Fin de if(isset($_POST['_POST']))
    
    if(isset($datos['msg'])){
			$opciones['ACCION'] = 2;
			$opciones['CADENA'] = 'El Cliente';
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}//Fin de if(isset($datos['msg']))

    $valores = $obj_usuarios->obtenerCliente($datos);    
    $contenido = $obj_usuarios->obtenerFormularioEditar($datos,$valores); 
    
    $_obj_interfaz->asignarContenido($contenido);
	 break;
	 
   case 'eliminar'.validarAcceso(array('R')):
    $resultado = $obj_usuarios->eliminarCliente($datos);
    header('Location: index.php?m=usuarios&accion=listado_clientes&tipo_gestion=3&msg='.$resultado);
   break;     
				
	//pgs - 30/07/2012 - funcion que se encarga de simular la cuenta de un cliente
	 case 'autenticar'.validarAcceso(array())://la validacion se hace internamente
    $resultado = $obj_usuarios->simularCliente($datos);
    header('Location: index.php?m=usuarios&msg='.$resultado);
   break;
	
  case 'precios'.validarAcceso(array()):
	   $contenido = $obj_usuarios->obtenerPrecios($datos);
	   $_obj_interfaz->asignarContenido($contenido);
	 break;
	 
	 //pgs - 03/11/2012 - nueva funcion de contactenos
	 case 'contactenos'.validarAcceso(array()):	  
	   if(isset($_POST['POST'])){
      $resultado = $obj_usuarios->enviarContactenos($datos);
      
      if($resultado==1){
        //se envi� satisfactoriamente
        header('Location: index.php?m=usuarios&accion=contactenos&msg=U-21');
      }else{
        $datos['msg'] = 'U-22';//no se pudo enviar
      }
    }//Fin de if(isset($_POST['_POST']))
    
    if(isset($datos['msg'])){      
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg']);
		}//Fin de if(isset($datos['msg']))
	   
	   $contenido = $obj_usuarios->obtenerFormularioContactenos($datos);
	   $_obj_interfaz->asignarContenido($contenido);
	 break;
	 
	 //pgs - 28/06/2013 - nueva funcion de solicitar demo
	 case 'solicitar_demo'.validarAcceso(array()):	  
	   if(isset($_POST['POST'])){
      $resultado = $obj_usuarios->enviarSolicitudDemo($datos);
      
      if($resultado==1){
        //se envi� satisfactoriamente
        header('Location: index.php?m=usuarios&accion=solicitar_demo&msg=U-23');
      }else{
        $datos['msg'] = 'U-24';//no se pudo enviar
      }
    }//Fin de if(isset($_POST['_POST']))
    
    if(isset($datos['msg'])){      
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg']);
		}//Fin de if(isset($datos['msg']))
	   
	   $contenido = $obj_usuarios->obtenerFormularioSolicitudDemo($datos);
	   $_obj_interfaz->asignarContenido($contenido);
	 break;
  	case 'clientes'	. validarAcceso(array()):	
		$datos['meta-titulo'] = "Nuestros Clientes | Boletines QUERYTEK";
		$datos['meta_descripcion'] = "";
		$datos['meta_keywords'] = "";
		
		$contenido = $obj_usuarios->obtenerLogosClientes($datos);
		$_obj_interfaz->asignarContenido($contenido);
	break;
	//Formulario de autenticacion
	default:    	       
		if($_SESSION['autenticado']==1){
      //pgs - 30/07/2012 - se crea el mensaje para mostrar en el formulario inicial
      if($datos['msg']!=1){//1=> cuando se loguea
        $datos['mensaje']= $_obj_mensajes->crearMensaje($datos['msg']);
      }//Fin de if($datos['msg']!=1) 
      
			$contenido = $obj_usuarios->abrirContenidoInicio($datos);
			$_obj_interfaz->asignarContenido($contenido);
			
		}else{
		    $_obj_interfaz->adicionarJS("jquery.carousel.min.js");
            $_obj_interfaz->asignarCampos($_campos);
			$datos['mensaje']= $_obj_mensajes->crearMensaje($datos['msg']);
			//pgs - 02/11/2012 - se cambia la funcion por la que despliega el home
			//$contenido = $obj_usuarios->abrirFormularioAutenticacion($datos);
			$contenido = $obj_usuarios->abrirHome($datos);
			$_obj_interfaz->asignarContenido($contenido);
		}
		break;
}//Fin de switch($datos['accion'])
?>