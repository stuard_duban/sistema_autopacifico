<?php

class Usuarios{

	var $_plantillas = "";

	function Usuarios(){
		global $_PATH_SERVIDOR;
		
		$this->_plantillas = $_PATH_SERVIDOR."Usuarios/Plantillas/";
		
		$this->campos_busqueda = array("busq_usu_login","busq_usu_nombre","busq_usu_correo",
					"offset","tipo_ordena","ordena");
					
		$this->campos_busqueda_listado = array("busq_usu_login","busq_usu_nombre","busq_usu_correo");
	}//Fin de Usuarios()
	
	/*
	* Funcion que se encarga de mostrar el formulario para crear un usuario nuevo
	*/	
  function obtenerFormularioNuevo($datos){
    global $_PATH_IMAGENES,$_PATH_WEB;
    
    $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas."/fm_nuevo.html");
    
    $datos['codigo'] = "per_id";
		$datos['mostrar'] = "per_nombre";		
		$datos['sql'] = "select per_id , per_nombre 
						from perfil order by per_nombre ASC";
		if(empty($datos['usu_per_id'])){
		  //2 id del perfil cliente
      $datos['valor'] = 2;
    }else{
      $datos['valor'] = $datos['usu_per_id'];
    }//Fin de if(empty($datos['usu_per_id'])) 
       
		$datos['nombre_campo'] = "usu_per_id";
		$select_perfil = Herramientas::crearSelectForma($datos);
		Interfaz::asignarToken("select_perfil", $select_perfil, $contenido);
    
    Interfaz::asignarToken("usu_nombre", $datos['usu_nombre'], $contenido);
    Interfaz::asignarToken("usu_login", $datos['usu_login'], $contenido);
    Interfaz::asignarToken("usu_correo", $datos['usu_correo'], $contenido);
    Interfaz::asignarToken("usu_correo_rebotado", $datos['usu_correo_rebotado'], $contenido);
    Interfaz::asignarToken("usu_smtp_host", $datos['usu_smtp_host'], $contenido);
    Interfaz::asignarToken("usu_smtp_cuenta", $datos['usu_smtp_cuenta'], $contenido);
    Interfaz::asignarToken("usu_smtp_puerto", $datos['usu_smtp_puerto'], $contenido);
    Interfaz::asignarToken("usu_smtp_contrasenha", $datos['usu_smtp_contrasenha'], $contenido);
    Interfaz::asignarToken("usu_smtp_confirmar_contrasenha", $datos['usu_smtp_confirmar_contrasenha'], $contenido);
    
    //pgs - 28/01/2012 - se a�aden los nuevos campos para la lectura de email
    Interfaz::asignarToken("usu_lectura_host", $datos['usu_lectura_host'], $contenido);
    Interfaz::asignarToken("usu_lectura_usuario", $datos['usu_lectura_usuario'], $contenido);
    Interfaz::asignarToken("usu_lectura_clave", $datos['usu_lectura_clave'], $contenido);
    Interfaz::asignarToken("usu_lectura_puerto", $datos['usu_lectura_puerto'], $contenido);
    
    //pgs - 07/06/2012 - se a�ade campo para el nuevo campo de limite de correos a enviar
    Interfaz::asignarToken("usu_limite_envio", $datos['usu_limite_envio'], $contenido);
    
    //chcm - 16/07/2012 - se a�ade campo para el nuevo campo de limite de destinatarios por usuario
    Interfaz::asignarToken("usu_limite_destinatarios", $datos['usu_limite_destinatarios'], $contenido);
    
    //pgs - 27/07/2012 - se a�ade campo para el nuevo campo de fecha de vencimiento
    Interfaz::asignarToken("usu_fecha_vencimiento", $datos['usu_fecha_vencimiento'], $contenido);
    
    $datos['adicionales'] = 'id="usu_lectura_tipo_conex"';
    $datos['array'] = array('IMAP'=>'IMAP','POP3'=>'POP3');
    $datos['valor'] = $datos['usu_lectura_tipo_conex'];
		$datos['nombre_campo'] = "usu_lectura_tipo_conex";
		$select_tipo_conex = Herramientas::crearSelectFormaDesdeArray($datos);
		Interfaz::asignarToken("select_tipo_conexion", $select_tipo_conex, $contenido);
    
    $datos['adicionales'] = 'id="usu_lectura_ssl"';
    $datos['array'] = array(0=>'No',1=>'Si');
    $datos['valor'] = $datos['usu_lectura_ssl'];
		$datos['nombre_campo'] = "usu_lectura_ssl";
		$select_ssl_lectura = Herramientas::crearSelectFormaDesdeArray($datos);
		Interfaz::asignarToken("select_ssl_lectura", $select_ssl_lectura, $contenido);
    
    $datos['array'] = array(0=>'No',1=>'Si');
    
    if(empty($datos['usu_smtp_autenticacion'])){
      $datos['valor'] = 1;
    }else{
      $datos['valor'] = $datos['usu_smtp_autenticacion'];
    }//Fin de if(empty($datos['usu_smtp_autenticacion']))
    
    $datos['adicionales'] = 'id="usu_smtp_autenticacion"';
		$datos['nombre_campo'] = "usu_smtp_autenticacion";
		$select_autenticacion = Herramientas::crearSelectFormaDesdeArray($datos);
		Interfaz::asignarToken("select_autenticacion", $select_autenticacion, $contenido);
    
    $datos['adicionales'] = 'id="usu_smtp_ssl"';
    $datos['array'] = array(0=>'No',1=>'Si');
    $datos['valor'] = $datos['usu_smtp_ssl'];
		$datos['nombre_campo'] = "usu_smtp_ssl";
		$select_ssl = Herramientas::crearSelectFormaDesdeArray($datos);
		Interfaz::asignarToken("select_ssl", $select_ssl, $contenido);
    
    $requerido = '<span class="requerido">*</span>';
    Interfaz::asignarToken("requerido", $requerido, $contenido);
    
    Interfaz::asignarToken("accion", 'fm_nuevo', $contenido);
    Interfaz::asignarToken("titulo", 'Nuevo Cliente', $contenido);
    Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);
    Interfaz::asignarToken("path_web", $_PATH_WEB, $contenido);
    
    return $contenido;
  }//Fin de obtenerFormularioNuevo($datos)
  
  
  function obtenerLogosClientes($datos) {
        global $_PATH_IMAGENES, $_PATH_WEB, $_PATH_ACCION;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/clientes.html");

        $listado_clientes = $this->generarListadoClientes();
        
        Interfaz::asignarToken("listado_clientes", $listado_clientes, $contenido);
        Interfaz::asignarToken("path_imagenes", $_PATH_IMAGENES, $contenido);

        return $contenido;
    }
	
  /*
	* Funcion que se encarga de mostrar el formulario para editar un usuario
	*/
  function obtenerFormularioEditar($datos,$cliente){
    global $_PATH_IMAGENES,$_PATH_WEB;
    
    $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas."/fm_nuevo.html");
    
    $datos['codigo'] = "per_id";
		$datos['mostrar'] = "per_nombre";		
		$datos['sql'] = "select per_id , per_nombre 
						from perfil order by per_nombre ASC";
		if(empty($cliente['usu_per_id'])){
		  //id del perfil cliente
      $datos['valor'] = 2;
    }else{
      $datos['valor'] = $cliente['usu_per_id'];
    }//Fin de if(empty($datos['usu_per_id']))    
		$datos['nombre_campo'] = "usu_per_id";
		$select_perfil = Herramientas::crearSelectForma($datos);
		Interfaz::asignarToken("select_perfil", $select_perfil, $contenido);
    
    Interfaz::asignarToken("usu_nombre", $cliente['usu_nombre'], $contenido);
    Interfaz::asignarToken("usu_login", $cliente['usu_login'], $contenido);
    Interfaz::asignarToken("usu_correo", $cliente['usu_correo'], $contenido);
    Interfaz::asignarToken("usu_correo_rebotado", $cliente['usu_correo_rebotado'], $contenido);
    Interfaz::asignarToken("usu_smtp_host", $cliente['usu_smtp_host'], $contenido);
    Interfaz::asignarToken("usu_smtp_cuenta", $cliente['usu_smtp_cuenta'], $contenido);
    Interfaz::asignarToken("usu_smtp_contrasenha", $cliente['usu_smtp_contrasenha'], $contenido);
    Interfaz::asignarToken("usu_smtp_confirmar_contrasenha", $cliente['usu_smtp_contrasenha'], $contenido);
    
    //pgs - 28/01/2012 - se a�aden los nuevos campos para la lectura de email
    Interfaz::asignarToken("usu_lectura_host", $cliente['usu_lectura_host'], $contenido);
    Interfaz::asignarToken("usu_lectura_usuario", $cliente['usu_lectura_usuario'], $contenido);
    Interfaz::asignarToken("usu_lectura_clave", $cliente['usu_lectura_clave'], $contenido);
    
    //pgs - 07/06/2012 - se a�ade campo para el nuevo campo de limite de correos a enviar
    Interfaz::asignarToken("usu_limite_envio", $cliente['usu_limite_envio'], $contenido);
    
    //chcm.  16/07/2012 - se a�ade campo para el nuevo campo de limite de destinatarios por usuario
    Interfaz::asignarToken("usu_limite_destinatarios", $cliente['usu_limite_destinatarios'], $contenido);
    
    //pgs - 27/07/2012 - se a�ade campo para el nuevo campo de fecha de vencimiento
    Interfaz::asignarToken("usu_fecha_vencimiento", date('Y-m-d',strtotime($cliente['usu_fecha_vencimiento'])), $contenido);
    
    if($cliente['usu_lectura_puerto']==0){
      $cliente['usu_lectura_puerto'] = '';
    }//Fin de if($cliente['usu_lectura_puerto']==0)
    
    Interfaz::asignarToken("usu_lectura_puerto", $cliente['usu_lectura_puerto'], $contenido);
    
    $datos['adicionales'] = 'id="usu_lectura_tipo_conex"';
    $datos['array'] = array('IMAP'=>'IMAP','POP3'=>'POP3');
    $datos['valor'] = $cliente['usu_lectura_tipo_conex'];
		$datos['nombre_campo'] = "usu_lectura_tipo_conex";
		$select_tipo_conex = Herramientas::crearSelectFormaDesdeArray($datos);
		Interfaz::asignarToken("select_tipo_conexion", $select_tipo_conex, $contenido);
    
    $datos['adicionales'] = 'id="usu_lectura_ssl"';
    $datos['array'] = array(0=>'No',1=>'Si');
    $datos['valor'] = $cliente['usu_lectura_ssl'];
		$datos['nombre_campo'] = "usu_lectura_ssl";
		$select_ssl_lectura = Herramientas::crearSelectFormaDesdeArray($datos);
		Interfaz::asignarToken("select_ssl_lectura", $select_ssl_lectura, $contenido);
    
    if($cliente['usu_smtp_puerto']==0){
      $cliente['usu_smtp_puerto'] = '';
    }//Fin de if($cliente['usu_smtp_puerto']==0)
    Interfaz::asignarToken("usu_smtp_puerto", $cliente['usu_smtp_puerto'], $contenido);
    
    $datos['adicionales'] = 'id="usu_smtp_autenticacion"';
    $datos['array'] = array(0=>'No',1=>'Si');    
    $datos['valor'] = $cliente['usu_smtp_autenticacion'];
		$datos['nombre_campo'] = "usu_smtp_autenticacion";
		$select_autenticacion = Herramientas::crearSelectFormaDesdeArray($datos);
		Interfaz::asignarToken("select_autenticacion", $select_autenticacion, $contenido);
    
    $datos['adicionales'] = 'id="usu_smtp_ssl"';
    $datos['array'] = array(0=>'No',1=>'Si');
    $datos['valor'] = $cliente['usu_smtp_ssl'];
		$datos['nombre_campo'] = "usu_smtp_ssl";
		$select_ssl = Herramientas::crearSelectFormaDesdeArray($datos);
		Interfaz::asignarToken("select_ssl", $select_ssl, $contenido);
    
    $requerido = '';
    Interfaz::asignarToken("requerido", $requerido, $contenido);
    
    Interfaz::asignarToken("accion", 'fm_editar', $contenido);
    Interfaz::asignarToken("titulo", 'Datos B&aacute;sicos del Cliente', $contenido);
    Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);
    Interfaz::asignarToken("usu_id", $datos['usu_id'], $contenido);
    
    Interfaz::asignarToken("path_web", $_PATH_WEB, $contenido);
    
    return $contenido;
  }//Fin de obtenerFormularioEditar($datos,$cliente)
  
  /*
  * Funcion que se encarga de mostrar el contenido del inicio despues de loguearse 
  */  
  function abrirContenidoInicio($datos){
    //global $_limit_des_usuario,$_PATH_WEB;
    
		//$num_des_disponibles = $this->obtenerNumDestinatariosDisponibles($datos);
    
    //pgs - 30/07/2012 - se pone el mensaje para mostrar en el formulario inicial
    $contenido = "
		<table width='70%' align='center'>
			<tr>
			 <td>".$datos['mensaje']."</td>
			</tr>
      <tr>
				<td align='center' valign='middle'>
				<br><br><b>Bienvenido al Gestor de Env&iacute;os SMS</b>
			 	<br><br>	
				<br>Fecha de Acceso:<br>".date("d-m-Y h:i:s A",time())."</b>
				</td>
			</tr>
		</table><br><br>";
		
		return $contenido;
	}//Fin de abrirContenidoInicio()	  
  
  //pgs - 02/11/2012 - funcion que despliega el home
  function abrirHome($datos){
    global $_PATH_IMAGENES,$_PATH_ACCION;
		
		$contenido = Archivos::obtenerContenidoArchivo($this->_plantillas."/home.html");

		Interfaz::asignarToken("usu_login",$datos['usu_login'],$contenido);		
		
		Interfaz::asignarToken("path_imagenes",$_PATH_IMAGENES,$contenido);		
		
		Interfaz::asignarToken("mensaje",$datos['mensaje'],$contenido);		
		
		Interfaz::asignarToken("path_accion",$_PATH_ACCION,$contenido);		
		
		return $contenido;
  }//Fin de abrirHome($datos)
  
  /*
  * Funcion que se encarga de mostrar el formulario para autenticarse
  */  
  function abrirFormularioAutenticacion($datos){
		global $_PATH_IMAGENES,$_PATH_ACCION;
		
		$contenido = Archivos::obtenerContenidoArchivo($this->_plantillas."/fm_autenticacion.html");

		Interfaz::asignarToken("usu_login",$datos['usu_login'],$contenido);		
		
		Interfaz::asignarToken("path_imagenes",$_PATH_IMAGENES,$contenido);		
		
		Interfaz::asignarToken("mensaje",$datos['mensaje'],$contenido);		
		
		Interfaz::asignarToken("path_accion",$_PATH_ACCION,$contenido);		
		
		return $contenido;
	}//Fin de abrirFormularioAutenticacion()
  	
	function abrirMensajeOlvidoClave($datos){
		global $_PATH_ACCION;
		
		$contenido = Archivos::obtenerContenidoArchivo($this->_plantillas."/msg_olvido_clave.html");
		
		$enlace = "Continuar";
		$accion = "";
		
		if($datos["res"] == "U-0" || $datos["res"] == "U-4" || $datos["res"] == "U-10"){
      $enlace = "Volver";
      $accion = "&accion=forma_olvido_clave&ac_user=".$datos["ac_user"]."&ac_email=".$datos["ac_email"];
    }//Fin de if($datos["res"] == "U-0" || $datos["res"] == "U-4" || $datos["res"] == "U-10")
		                                                                  
		Interfaz::asignarToken("enlace", $enlace, $contenido);
		Interfaz::asignarToken("accion", $accion, $contenido);
		Interfaz::asignarToken("path_accion", $_PATH_ACCION, $contenido);
		Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);
		
		return $contenido;
	}//fin de abrirMensajeOlvidoClave()
	
	function obtenerCorreoOlvidoClave($datos,$valores){
		global $_PATH_WEB, $_PATH_ACCION;
		
		$valor = $valores[0];
		
		$contenido = Archivos::obtenerContenidoArchivo($this->_plantillas."/correo_olvido_clave.html");
		
		Interfaz::asignarToken("path_accion", $_PATH_ACCION, $contenido);
		Interfaz::asignarToken("usuario", $valor['acc_user'], $contenido);				
		Interfaz::asignarToken("clave", $valor['acc_pass'], $contenido);
		Interfaz::asignarToken("path_web", $_PATH_WEB, $contenido);			
		
		return $contenido;
	}//fin de obtenerContenidoActivacion()
	
	function abrirFormularioOlvidoClave($datos){
		global $_PATH_IMAGENES,$_PATH_WEB,$_PATH_ACCION;
		
		$contenido = Archivos::obtenerContenidoArchivo($this->_plantillas."/fm_olvido_clave.html");
		
		Interfaz::asignarToken("path_accion",$_PATH_ACCION,$contenido);		

		Interfaz::asignarToken("acc_user", $datos['acc_user'], $contenido);
		Interfaz::asignarToken("acc_email", $datos['acc_email'], $contenido);
		Interfaz::asignarToken("mensaje", $datos['mensaje'] ,$contenido);	
		Interfaz::asignarToken("path_imagenes", $_PATH_IMAGENES, $contenido);	
		
		return $contenido;
	}//Fin de abrirFormularioOlvidoClave()
  
  
  /*
  * pgs - 30/07/2012
  * Funcion que se encarga de mostrar el formulario para cambiar contrase�a  
  */  	
  function abrirFormularioCambiarContrasenha($datos){
    global $_PATH_IMAGENES,$_PATH_WEB;
    
    $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas."/fm_cambiar_contrasenha.html");
    
    
    Interfaz::asignarToken("accion", 'forma_cambiar_clave', $contenido);
    Interfaz::asignarToken("titulo", 'Cambiar Contrase&ntilde;a', $contenido);
    Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);
    Interfaz::asignarToken("path_web", $_PATH_WEB, $contenido);
		Interfaz::asignarToken("path_imagenes", $_PATH_IMAGENES, $contenido);
		
		return $contenido;
  }//Fin de abrirFormularioCambiarContrasenha($datos)
	
	//pgs - 01/08/2012 - funcion que muestra los precios vigentes del aplicativo
  function obtenerPrecios($datos){
    global $_PATH_IMAGENES,$_PATH_WEB,$_PATH_ACCION;
		
		$contenido = Archivos::obtenerContenidoArchivo($this->_plantillas."/precios.html");
		
		Interfaz::asignarToken("path_imagenes", $_PATH_IMAGENES, $contenido);	
		
		return $contenido;
  }//Fin de obtenerPrecios($datos)	
	
	//pgs - 28/06/2013 - funcion que se encarga de desplegar el formulario de solicitar demo
  function obtenerFormularioSolicitudDemo($datos){
    global $_PATH_IMAGENES,$_PATH_WEB,$_PATH_ACCION;
		
		$contenido = Archivos::obtenerContenidoArchivo($this->_plantillas."/fm_solicita_demo.html");
		
		Interfaz::asignarToken("path_imagenes", $_PATH_IMAGENES, $contenido);
    Interfaz::asignarToken("accion", 'solicitar_demo', $contenido);
    Interfaz::asignarToken("titulo", 'Solicita un demo', $contenido);
    Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);
    	
		
		return $contenido;
  }//Fin de obtenerFormularioSolicitudDemo($datos)	
	
	//pgs - 03/11/2012 - funcion que se encarga de desplegar el formulario de contactenos
  function obtenerFormularioContactenos($datos){
    global $_PATH_IMAGENES,$_PATH_WEB,$_PATH_ACCION;
		
		$contenido = Archivos::obtenerContenidoArchivo($this->_plantillas."/fm_contactenos.html");
		
		Interfaz::asignarToken("path_imagenes", $_PATH_IMAGENES, $contenido);
    Interfaz::asignarToken("accion", 'contactenos', $contenido);
    Interfaz::asignarToken("titulo", 'Cont&aacute;ctenos', $contenido);
    Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);
    	
		
		return $contenido;
  }//Fin de obtenerFormularioContactenos($datos)	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						 //FIN DE METODOS GRAFICOS
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
  
  //pgs - 03/11/2012 - funcion que se encarga de enviar el correo de contactenos
  function enviarContactenos($datos){
    global $_EMAIL_CONTACTENOS,$_EMAIL_FROM,$_NOMBRE_FROM,$_PATH_IMAGENES,$_PATH_WEB;
    
    $para = $_EMAIL_CONTACTENOS;
    //$para = 'pablo.gomez@querytek.com';
    $asunto = 'Contacto de la p�gina de Boletines.com.co';
    
    $mensaje = Archivos::obtenerContenidoArchivo($this->_plantillas."/msg_contactenos.html");
		
		Interfaz::asignarToken("nombre", $datos['co_nombre'], $mensaje);
    Interfaz::asignarToken("telefono_celular", $datos['co_telefono_celular'], $mensaje);
    Interfaz::asignarToken("correo", $datos['co_correo'], $mensaje);
    Interfaz::asignarToken("ciudad", $datos['co_ciudad'], $mensaje);
    Interfaz::asignarToken("mensaje", nl2br($datos['co_mensaje']), $mensaje);    
    
    $cabecera = "from:".$_NOMBRE_FROM." <".$_EMAIL_FROM."> \r\nMIME-Version: 1.0\nContent-Type: text/html\nX-Mailer: WebMail\nReply-To:".$datos['co_correo'];
    if (mail($para,$asunto,$mensaje,$cabecera)){
      $para = $datos['co_correo'];
      $cabecera = "from:".$_NOMBRE_FROM." <".$_EMAIL_FROM."> \r\nMIME-Version: 1.0\nContent-Type: text/html\nX-Mailer: WebMail";
      $mensaje = Archivos::obtenerContenidoArchivo($this->_plantillas."/msg_gracias_contactenos.html");
		
		  Interfaz::asignarToken("nombre", $datos['co_nombre'], $mensaje);
		  Interfaz::asignarToken("path_imagenes", $_PATH_IMAGENES, $mensaje);
      Interfaz::asignarToken("path_web", $_PATH_WEB, $mensaje);
      
      //se envia un mensaje de agradecimiento al contacto
      if (mail($para,$asunto,$mensaje,$cabecera)){
        return 1;
      }else{
        return 0;
      }//Fin de if (mail($para,$asunto,$mensaje,$cabecera))
    }else{
      return 0;
    }//Fin de if (mail($to,$subject,$mensaje,$cabecera))
  }//Fin de enviarContactenos($datos)
  	
	function autenticarUsuario($datos){                        
		global $_obj_database;
		
		$datos = Herramientas::trimCamposFormulario($datos);
		
		//codigo usuario no existe
		$respuesta = 'U-0';
		
		$sql = "select usu_correo, usu_contrasenha
  					from usuario_sistema  
  					where usu_correo='".$datos['usu_login']."'";
    
		$resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
		   
		if ($_obj_database->obtenerNumeroFilas()>0){		
        //Clave incorrecta
  			$respuesta='U-2';			
  			if(strcmp($resultado[0]['usu_contrasenha'], md5($datos['usu_contrasenha']))==0){
  				$_SESSION = array_merge($_SESSION,$resultado[0]);				                
  				//elimina la variable de clave para que no este en la sesion
  				unset($_SESSION[usu_contrasenha]);
  				$_SESSION['autenticado'] = 1;
  		   $_SESSION['tipo_usuario'] = 'U'; // se asigna tipo de usuario para visualizacion de elementos en la pagina como el men�
  				$respuesta=1;
  			}//Fin de if( strcmp($resultado[0]['usu_clave'],$datos['usa_clave'])==0 )  
		}//Fin de if ($_obj_database->obtenerNumeroFilas()>0)   
     
		return $respuesta;
	}//Fin de autenticarUsuario()
	
	function solicitarRecordatorioClave($datos){                   
		global $_obj_database;
		
		$datos = Herramientas::trimCamposFormulario($datos);
		
		//usuario no existe
		$respuesta = 'U-0';
		
		$sql = "select acc_user, acc_email 
					from acceso  
					where acc_estado = 1 
						and acc_user='{$datos[acc_user]}'";
		$resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
		   
		if($_obj_database->obtenerNumeroFilas()>0){
			//Correo incorrecto
			$respuesta = 'U-4';
						
			if(strcmp($resultado[0]['acc_email'], $datos['acc_email'])==0){
				$clave = $this->cambiarClave($resultado[0]['acc_user']);
				
				if($clave != ""){
					$resultado[0]['acc_pass'] = $clave;
					$respuesta = $resultado;
				}else{
					//error al actualizar contrase�a
					$respuesta = "U-10";
				}

			}//Fin de if( strcmp($resultado[0]['ac_email'],$datos['ac_email'])==0 )     
    }//Fin de if ($_obj_database->obtenerNumeroFilas()>0)    
				
		return $respuesta;
	}//Fin de solicitarsolicitarRecordatorioClave()
	
	function abrirFormularioNuevaClave($datos){
		$contenido = Archivos::obtenerContenidoArchivo($this->_plantillas."/fm_cambiar_clave.html");                       
    
		if( esTipo( array(1)) ){
			$datos[acc_id] = $datos[acc_id]=="" ? $_SESSION[acc_id] : $datos[acc_id];
			
			$datos['codigo'] = "acc_id";
			$datos['mostrar'] = "acc_nombre";
			$datos['valor'] = $datos[acc_id];
			$datos['nombre_campo'] = "acc_id";
			$datos['valor_defecto'] = "";
			$datos['sql'] = "SELECT acc_id, acc_nombre 
								FROM acceso 
								WHERE acc_estado = 1 ";
			$acc_id = Herramientas::crearSelectForma($datos);
		}else{
			$acc_id = "<b>".$_SESSION["acc_user"]."</b>";
			$clave_actual = '<tr>
							<td class="forma_label"><span class="forma_oblig">*</span>&nbsp;Contrase&ntilde;a actual:</td>
							<td><input type="password" name="acc_pass_actual" value=""></td>
						</tr>';
		}        
    
		Interfaz::asignarToken("acc_id", $acc_id, $contenido);
		Interfaz::asignarToken("clave_actual", $clave_actual, $contenido);
		Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);
		
		return $contenido;
  }// Fin de abrirFormularioNuevaClave()
  
	function solicitarCambioClave($datos){
		global $_obj_database; 
		
		$datos = Herramientas::trimCamposFormulario($datos);
		
		$acc_id = $_SESSION[acc_id];
		if( $datos[acc_id] != "" && esTipo(array(1)) ){
			$acc_id = $datos[acc_id];
		}
		
		$valor = $_obj_database->obtenerRegistro("acceso","acc_id='".$acc_id."'");

		$respuesta = "U-2";
		//contrase�a incorrecta
				
		//la contrase�a actual y la ingresada son correctas
		if(  $valor[acc_pass] == $datos[acc_pass_actual] || esTipo(array(1)) ){
			$sql = "UPDATE acceso 
					SET acc_pass='".$datos['acc_pass']."' 
					WHERE acc_id='".$acc_id."'";
					
			$resultado = $_obj_database->ejecutarSql($sql);
				
			if ($resultado || $_obj_database->obtenerNumeroFilasAfectadas()>0){
				$respuesta = "U-13";
			}else{
				$respuesta = "U-12";
			}
			
		}//Fin de if(  $valor[acc_pass] == $datos[acc_pass_actual] )
						
		return $respuesta;
	}// Fin de solicitarCambioClave()
  
	function abrirFormularioConfiguracion($datos){        
    global $_obj_database;  
         
    $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas."/fm_configuracion.html");
    
    $sql = "SELECT * FROM parametro";
		$resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
		
		$tabla = "";
		$i = 0;
    
		foreach($resultado as $res){		
			$tabla .= "<tr>
			  <td class='forma_label' align='left'><b>".$res["par_id"]."</b></td>
			  <td class='forma_mensaje' align='left'>".$res["par_desc"]."</td>
			  ";
			
			  if($res["par_modificar"] == "1"){           
  				$tabla .= "<td align='left'><input size='40' type='text' name='conf_".$res["par_id"]."' value='".$res["par_valor"]."' /></td>";
  				$i++;
			  }else{
				  $tabla .= "<td align='left' class='forma_mensaje'>".$res["par_valor"]."</td>";
			  }   
			  
			$tabla .= "</tr>";
		}//Fin de foreach($resultado as $res)  
		
		$boton = "";
		if($i > 0){
		  $boton = "<tr>
				<td colspan='3' align='left'><input class='boton' type='submit' value='Guardar'></td>
			</tr>";
		} 
    
		Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);
		Interfaz::asignarToken("tabla_config", $tabla, $contenido);
		Interfaz::asignarToken("boton", $boton, $contenido);
		
		return $contenido;
	}// Fin de abrirFormularioConfiguracion()
	
  function cambiarConfig($datos){              
    global $_obj_database;
    
    $resultado = -1003;
    $i = 0; 
    
    foreach($datos as $key=>$val){                    
      if(strpos($key, "conf_") !== false){
        $pm_id = substr($key, 5);
        $sql = "UPDATE parametro SET par_valor='".$val."' WHERE par_id='".$pm_id."'";
        $res = $_obj_database->ejecutarSql($sql);
        
        if($res==true || $_obj_database->obtenerNumeroFilasAfectadas()>0){
          $i++;
        }
      }//Fin de if(strpos($key, "conf_") !== false)
    }//Fin de foreach($datos as $key=>$val)
    
    if($i > 0){
      $resultado = 1;
    }
    
    return $resultado; 
  }// Fin de cambiarConfig()
  
  /*	***********************************	INGRESAR CLIENTE	*************************************	*/
	/*
	*	Utilidad:
	*		Registra los datos de un cliente
	*	Paraetros de entrada:
	*		$datos -> Datos del cliente
	*	Valores de retorno:
	*		$resultado_registro -> Resultado del registro del cliente
	*/
  function ingresarCliente($datos){
    global $_obj_database;
    
    $datos = Herramientas::trimCamposFormulario($datos);
    
    $existe = $this->existe($datos);
    
    if(!$existe){
      $datos['usu_contrasenha'] = md5($datos['usu_contrasenha']);
      $datos['usu_fecha_registro'] = date('Y-m-d');  
      //pgs - 07/06/2012 - se a�ade nuevo campo para el limite de envios usu_limite_envio   
      //chcm - 16/07/2012 - se a�ade nuevo campo para el limite de destinatarios por usuario
      //pgs - 27/07/2012 - se a�ade nuevo campo para la fecha de vencimiento
      
    	$campos  = array("usu_per_id",
                        "usu_nombre",
                        "usu_login",
                        "usu_contrasenha",
                        "usu_correo",
                        "usu_smtp_host",
                        "usu_smtp_cuenta",
                        "usu_smtp_contrasenha",
                        "usu_smtp_autenticacion",
                        "usu_smtp_ssl",
                        "usu_fecha_registro",
                        "usu_lectura_tipo_conex",
                        "usu_lectura_host",
                        "usu_lectura_usuario",
                        "usu_lectura_clave",
                        "usu_lectura_puerto",
                        "usu_lectura_ssl",
                        "usu_limite_envio",
                        "usu_limite_destinatarios",
                        "usu_fecha_vencimiento");
    	
      if(!empty($datos['usu_smtp_puerto'])){
        $campos[] = "usu_smtp_puerto";
      }
      				
    	$datos['tabla'] = "usuario";
    	$sql = $_obj_database->generarSQLInsertar($datos,$campos);
    	
    	$resultado = $_obj_database->ejecutarSql($sql);
    	
    	if($_obj_database->obtenerNumeroFilasAfectadas()>0 && $resultado){
    		//el cliente se registro satisfactoriamente
        return 'C-0';
    	}else{
        return $resultado;
      }//Fin de if($_obj_database->obtenerNumeroFilasAfectadas()>0 && $resultado)
    }else{
      //El cliente ya existe
      return 'C-1'; 
    }
  }//Fin de ingresarCliente($datos)
  
  /*	***********************************	EDITAR CLIENTE	*************************************	*/
	/*
	*	Utilidad:
	*		Edita la informacion de un cliente
	*	Paraetros de entrada:
	*	  $datos -> datos del cliente	
	*		$datos[usu_id] -> id del cliente
	*	Valores de retorno:
	*		$res -> resultado de ingresar un cliente
	*/
  function editarCliente($datos){       
		global $_obj_database;
		
		if($datos['usu_login_anterior'] != $datos['usu_login']){
       $existe = $this->existe($datos);
    
      if($existe){
        //El cliente ya existe
        return 'C-1';
      }//Fin de if($existe)
    }//Fin de if($datos['usu_login_anterior'] != $datos['usu_login'])
		
		$datos = Herramientas::trimCamposFormulario($datos);
		
		$datos["tabla"] = "usuario"; 
		$datos["condicion"] = "usu_id='".$datos["usu_id"]."' 
								and usu_estado = 1";
    
    //pgs - 07/06/2012 - se a�ade nuevo campo para el limite de envios usu_limite_envio      
    
    //chcm - 16/07/2012 - se a�ade nuevo campo para el limite de envios usu_limite_destinatarios
    //pgs - 27/07/2012 - se a�ade nuevo campo para la fecha de vencimiento
		$campos  = array("usu_per_id",
                      "usu_nombre",
                      "usu_login",
                      "usu_correo",
                      "usu_smtp_host",
                      "usu_smtp_cuenta",
                      "usu_smtp_contrasenha",
                      "usu_smtp_autenticacion",
                      "usu_smtp_ssl",
                      "usu_smtp_puerto",
                      "usu_lectura_tipo_conex",
                      "usu_lectura_host",
                      "usu_lectura_usuario",
                      "usu_lectura_clave",
                      "usu_lectura_puerto",
                      "usu_lectura_ssl",
                      "usu_limite_envio",
                      "usu_limite_destinatarios",
                      "usu_fecha_vencimiento");
    	
    /*if(!empty($datos['usu_smtp_puerto'])){     
      $campos[] = "usu_smtp_puerto";
    }*/
    
    if(!empty($datos['usu_contrasenha']) && !empty($datos['usu_confirmar_contrasenha'])){
      $datos['usu_contrasenha'] = md5($datos['usu_contrasenha']);
      $campos[] = "usu_contrasenha";
    }
	   
		$sql = $_obj_database->generarSQLActualizar($datos, $campos);
		
		$resultado = $_obj_database->ejecutarSql($sql);
		
		if($resultado){
  		return 1;
  	}else{
      return $resultado;
    }//Fin de if($resultado)
  }//Fin de editarCliente($datos)
  
	function obtenerListado($datos){
		global $_obj_database, $_limit;
		
		$datos = Herramientas::trimCamposFormulario($datos);
		
		$offset = (isset($datos['offset']) && is_numeric($datos['offset']) ? $datos['offset'] : 0);
		$condicion = "";      
		
		if($datos['busq_usu_nombre'] != ""){
			$condicion .= " AND usu_nombre LIKE '%".$datos['busq_usu_nombre']."%' ";
			$datos['usa_filtro'] = "1";
		}       
		
		if($datos['busq_usu_login'] != ""){
			$condicion .= " AND usu_login LIKE '%".$datos['busq_usu_login']."%' ";
			$datos['usa_filtro'] = "1";
		} 
    
    if($datos['busq_usu_correo'] != ""){
			$condicion .= " AND usu_correo LIKE '%".$datos['busq_usu_correo']."%' ";
			$datos['usa_filtro'] = "1";
		}      
		
		$orden = "ORDER BY usu_id ASC";
		if( $datos['ordena'] != ""){
			$orden = "ORDER BY ".$datos['ordena']." ".$datos['tipo_ordena'];
		}
		
		$sql = "SELECT usu_id, usu_login, usu_nombre, usu_correo,per_nombre 
					FROM usuario,perfil 
					WHERE usu_per_id=per_id and usu_estado = 1 
					  ".$condicion."
					".$orden." 
					LIMIT ".$offset.",".$_limit;
		$resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
		
		if(is_array($resultado)){
			$sql = "SELECT count(usu_id) as num_total_registros 
					FROM usuario,perfil 
					WHERE usu_per_id=per_id and usu_estado = 1 
					  ".$condicion;
					  
			$resultado_conteo = $_obj_database->obtenerResultado($sql);
			$resultado['limite']=$_limit;
			$resultado['offset']=$offset;
			$resultado['num_total_registros']= $resultado_conteo[0];
			$resultado['usa_filtro'] = $datos['usa_filtro'];
		}//Fin de if(is_array($resultado))
		
		return $resultado;
	}//Fin de obtenerListado() 
	
	/*	***********************************	EXISTE	*************************************	*/
	/*
	*	Utilidad:
	*		verifica si ya existe el nomnbre de usuario en la base de datos 
	*	Paraetros de entrada:
	*		$datos[usu_login] -> login del usuario
	*	Valores de retorno:
	*		$estado -> false=> si no existe, true=>si existe
	*/
	function existe($datos){
		global $_obj_database;	
		
		$estado = false;
		
		$sql = "select usu_id 
					from usuario 
					where usu_login = '".$datos[usu_login]."' 
						and usu_estado = 1 ";
						
		$res = $_obj_database->obtenerRegistrosAsociativos($sql);
		
		if( sizeof($res) > 0 ){
			$estado = true;
			$datos[id] = $res[0][acc_id];
		}//Fin de if( sizeof($res) > 0 )
		
		return $estado;
	}//Fin de existe()
  
  /*	***********************************	OBTENER CLIENTE	*************************************	*/
	/*
	*	Utilidad:
	*		Obtiene la informacion de un cliente
	*	Paraetros de entrada:
	*		$datos[usu_id] -> id del cliente
	*	Valores de retorno:
	*		$res -> informacion del cliente
	*/
  function obtenerCliente($datos){
		global $_obj_database;	
		
		$sql = "select * 
				from usuario 
				where usu_id = '".$datos['usu_id']."' 
					and usu_estado = 1";	
					
		$res = $_obj_database->obtenerRegistrosAsociativos($sql);
		
		return $res[0];
	}//Fin de obtenerCliente($datos)
	
	/*	***********************************	ELIMINAR CLIENTE	*************************************	*/
	/*
	*	Utilidad:
	*		eliminado logico del cliente
	*	Paraetros de entrada:
	*		$datos[usu_id] -> id del cliente
	*	Valores de retorno:
	*		$mensaje -> resultado de eliminar logicamente el cliente
	*/
	function eliminarCliente($datos){           
		global $_obj_database;
    
		$sql = "UPDATE usuario 
				SET usu_estado = 0 
				WHERE usu_id='".$datos["usu_id"]."' "; 
				
		$res = $_obj_database->ejecutarSql($sql);
    
		if( $res == true || $_obj_database->obtenerNumeroFilasAfectadas() > 0 ){
			$mensaje = 1;
		}else{
     	$mensaje = $res;
    }//Fin de if( $res == true || $_obj_database->obtenerNumeroFilasAfectadas() > 0 )
                
		return $mensaje;
	}// Fin de eliminarUsuario()
  
  /*
  * pgs
  *   
  * Funcion que se encarga de obtener el numero de destiantarios que le quedan para inrgesar 
  */  
  function obtenerNumDestinatariosDisponibles($datos){
    global $_limit_des_usuario,$_obj_database;
    
    $sql = "SELECT count(desg_id) as total
            FROM destinatario_grupo
            WHERE desg_usu_id='".$_SESSION['usu_id']."'";
    
    $res = $_obj_database->obtenerRegistrosAsociativos($sql);
    
    if(sizeof($res)>0){
      $disponibles = $_limit_des_usuario - $res[0]['total'];
      
      return $disponibles;
    }//Fin de if(sizeof($res)>0)
    
    return $_limit_des_usuario;
  }//Fin de obtenerNumDestinatariosLibres($datos)
  
  
  /*
  * chcm.  julio 16 2012
  *   
  * Funcion que se encarga de obtener el limite de destiantarios de un cliente 
  */  
  function obtenerLimiteDestinatariosCliente($datos){
    global $_limit_des_usuario,$_obj_database;
    
    $sql = "SELECT usu_limite_destinatarios as limite
            FROM usuario
            WHERE usu_id = ".$datos['usu_id'];
    
    $res = $_obj_database->obtenerRegistrosAsociativos($sql);
    
      return $res[0]['limite'];
    
  }//Fin de if(sizeof($res)>0)
    
  /*
  * pgs - 30/07/2012
  * Funcion que se encarga de cambiar la clave   
  */   
  function cambiarClave($datos){
    global $_obj_database;
    
    $clave = md5($datos['usu_clave']);
    
    $sql = "SELECT usu_correo
          FROM usuario_sistema
          WHERE usu_contrasenha='".$clave."' AND
            usu_correo='".$_SESSION['usu_correo']."'";
    $usuario = $_obj_database->obtenerRegistrosAsociativos($sql);
    
    if(sizeof($usuario)>0){    
      $sql = "UPDATE usuario_sistema 
      			SET usu_contrasenha='".md5($datos['usu_clave_nueva'])."' 
      			WHERE usu_correo='".$_SESSION['usu_correo']."'";
      $resultado = $_obj_database->ejecutarSql($sql);
        
      if($resultado || $_obj_database->obtenerNumeroFilasAfectadas()>0){
        return 'U-13';//se cambio satisfactoriamente
      }else{
        return 'U-12';//no se pudo cambiar
      }//Fin de if($resultado || $_obj_database->obtenerNumeroFilasAfectadas()>0)     
    }else{
      return 'U-15';//la contrase�a actual es incorrecta
    }//Fin de if(sizeof($usuario)>0)
  }//Fin de cambiarClave($datos)
  
  //pgs - 30/07/2012 - funcion que simula estar autenticado como un cliente
  function simularCliente($datos){
    global $_obj_database;

    //si no se esta tratando de simular a si mismo  
    if($datos['usu_id']!=$_SESSION['usu_id']){
      //si el tipo de usuario es admin o si existe como autenticado como otro usuario
      if($_SESSION['tipo_usuario'] == 'R' || !empty($_SESSION['id_admin_inicial'])){
        if(!isset($datos['reset'])){//si no se desea resetear la simulacion de autenticacion
          //se guarda el id inicial del admin
          $id_admin_inicial = $_SESSION['usu_id'];
        }else{
          //si desea resetear la simulacion, se envia su id inicial para traer sus datos
          $datos['usu_id'] = $_SESSION['id_admin_inicial'];
          unset($_SESSION['id_admin_inicial']);
        }//Fin de if(!isset($datos['reset']))
        
        $sql = "select usu_id,usu_per_id, usu_login, usu_contrasenha,usu_correo,usu_per_id as tipo_usuario,
                  usu_smtp_cuenta,usu_fecha_vencimiento
      					from usuario  
      					where usu_id='".$datos['usu_id']."' and usu_estado=1 ";
        $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
        
        if(sizeof($resultado)>0){
            $_SESSION = array_merge($_SESSION,$resultado[0]);				                
    				//elimina la variable de clave para que no este en la sesion
    				unset($_SESSION[usu_contrasenha]);
    				$_SESSION['autenticado'] = 1;
      				
            /*chcm.  julio 16/07/2012.  Obtengo el limite de destinatarios por cliente*/
            $datos['usu_id']= $_SESSION['usu_id'];
            global $_limit_des_usuario;
            $_limit_des_usuario = $this->obtenerLimiteDestinatariosCliente($datos);
            $_SESSION['$_limit_des_usuario']=$_limit_des_usuario;
            
            switch($_SESSION['tipo_usuario']){
              case 1:
              case '1':
                $_SESSION['tipo_usuario'] = 'R';
              break;
              
              case 2:
              case '2':
                $_SESSION['tipo_usuario'] = 'U';
              break;
            }
            
            if(!isset($datos['reset'])){//si no se desea resetear la simulacion de autenticacion
              $_SESSION['id_admin_inicial'] = $id_admin_inicial;
              return 'U-16';//simulacion completada
            }else{
              return 'U-19';//simulacion terminada satisfactoriamente              
            }//Fin de if(!isset($datos['reset']))
        }else{
          return 'U-17';//los datos de usuario no existen
        }//Fin de if(sizeof($resultado)>0)
      }else{
          return 'U-20';//No tienes permisos
      }//Fin de if($_SESSION['tipo_usuario'] == 'R' || !empty($_SESSION['id_admin_inicial'])
    }else{
      return 'U-18';//no puedes simular tu cuenta
    }//Fin de if($datos['usu_id']!=$_SESSION['usu_id'])    
  }//Find e simularCliente($datos)   

function generarListadoClientes()
  {
   $despliegue_clientes = "";
   
    $directory="Includes/Imagenes/clientes";
    $dirint = dir($directory);
    
    $despliegue_clientes .= "<ul class='nuestros-clientes'>";
    while (($archivo = $dirint->read()) !== false)
    {     
        if (eregi("gif", $archivo) || eregi("jpg", $archivo) || eregi("png", $archivo)){
            $despliegue_clientes .= '<li><img src="'.$directory."/".$archivo.'"></li>';
        }
    }
    $dirint->close();
   $despliegue_clientes .= "</ul>";
   return $despliegue_clientes;
  }
}//Fin de clase  Usuario
?>
