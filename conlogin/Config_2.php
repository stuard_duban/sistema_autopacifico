<?php
/*
*	ARCHIVO DE CONFIGURACION CON  LAS VARIABLES A UTILIZAR EN LA APLICACION
*/
	session_name("msm_qtk");
	session_start();
	
	putenv('TZ=America/Bogota');
	ini_set("memory_limit", "128000000");	
	set_time_limit(0);
	
	//limite de numero de registros en los listados
	$_limit = 50;
	//numero de correos a enviar cada vez que se ejecuta el crontab
	$_limite_correos = 200;
	//cada cuanto se puede correr el daemon en minutos
	$_limite_daemon_tiempo = 4;
  //dias que estara disponible totalmente el informe de envio
  //si pasados x dias el informe solo mostrara los valores y no se podra ver el detalle
  $_limite_informe = 30;

  $_host_db = "localhost";
	$_db = "app_sms";
	$_user_db = "root";
	$_password_db = "";

	$_PATH_WEB = "http://localhost/smsApp/";
	$_PATH_IMAGENES = $_PATH_WEB."Includes/Imagenes/";
	$_PATH_IMAGENES_WEB = $_PATH_WEB."Imagenes";
  $_PATH_ANIMACIONES = $_PATH_WEB."InterfazGrafica/Animaciones";   
  $_PATH_ADJUNTOS = $_PATH_WEB."Adjuntos";
	
  $_PATH_LOCAL = "C://xampp\htdocs\smsApp"; 
  $_PATH_LOCAL_ADJUNTOS = $_PATH_LOCAL."/Adjuntos";

	$_TITULO_SITIO_WEB = "Sistema de env&iacute;o de SMS";
	$_ANCHO_SITIO_WEB = 768;	 
	
	$_EMAIL_FROM = "colombia@querytek.com";
	$_NOMBRE_FROM = "Sistema de env&iacute;o SMS";
	
	//psg - 28/07/2012 - email de contactenos
  $_EMAIL_CONTACTENOS = 'colombia@querytek.com';
	
	$_limite_correos_envio = 200;
	
	//Tama�o maximo archivos adjuntos
	//Ejemplo: 2 megas = 2097152, se multiplica la cantidad de megas por 1048576
	//3MB - 3145728
	
  //2MB
  $_MAX_FILE_SIZE_ADJUNTOS = 2097152;
  
  //3MB
  $_MAX_FILE_SIZE_ADJUNTOS = 3145728;
  
  $_LIMIT_ENVIOS = 2;
  /*----------------------------------------         IMPORTANTE        ---------------------------------------------*/
  /*----------------------------------------         SE UTILIZA PARA LA CONFIGURACION DEL API DE ENVIO SMS        ---------------------------------------------*/
  	//DATOS DE PRUEBA
    $_URL_SMS = "http://httpgateway.sitmobile.com/HTTPConnector/SendMessage";
    $_LOGIN_SMS = "multioav";
    $_PASSWORD_SMS = "334UrtYy";
    $_CLIENTID_SMS = "30237";
    $_ACOUNTID_SMS= "59699";
    $_SEQ_SMS = "123";
    $_SOURCE_SMS = "TestRI";
    
    //CUENTAS DE USUARIO EN PRODUCCION
    /*$_URL_SMS = "http://httpgateway.sitmobile.com/HTTPConnector/SendMessage";
    $_LOGIN_SMS = "SIIGCOM";
    $_PASSWORD_SMS = "2vLqmu7g";
    $_CLIENTID_SMS = "30894";
    $_ACOUNTID_SMS= "60355";
    $_SEQ_SMS = "123";
    $_SOURCE_SMS = "TestRI";*/
    
   
  

	
	$_requiere_autenticacion = true;
	
	/*---------------------------------------- ---------------------------------------------------------  ---------------------------------------------*/
	
	$_tipos_archivo = array("jpg","jpeg","bmp","png","gif");
	
	//Extensiones de archivo validas para adjuntar en los boletines
	$_ext_archivo_boletin = array("jpg", "jpeg", "gif", "png", "pdf", "ppt", "doc", "docx", "xls", "xlsx", "pptx", "csv");
	
	$datos = array_merge($_GET,$_POST);
	
	$datos['accion'] = $datos['accion'].$_SESSION['tipo_usuario'];

	//limite de registros a procesar para los envios
	$_limit_lotes = 800;
	
  //limite de registros a procesar para los envios
  $_limit_procesar_envios = 20000;

  //pgs - 30/01/2012 - limite de correos rebotados a procesar;
  $_limit_procesar_rebotados = 200;
  $_correo_blando = 1;//dureza del correo blando
  $_correo_duro = 2;//dureza del correo duro
  //pgs - 08/02/2012 - destinatario a quien le llegara un correo de que estan bien los datos SMTP
  $_destinatario_comprobar = array("carlos.ceron@querytek.com","david.males@querytek.com");

  //pgs - 10/03/2012 - limite de destinatarios con dureza rebotado = 2 (duro) a limpiar
  $_limit_procesar_rebotados_duros = 200;
  
  //pgs - 10/03/2012 - limite de destinatarios a verificar sus correos
  $_limit_verificar_correos = 10;
  
  //pgs - 07/06/2012 - limite de destinatarios por usuario
  $_limit_des_usuario = 1000;
  
  //pgs - 07/06/2012 - limite de destinatarios por grupo
  $_limit_des_grupo = 70000;
  
  //pgs - 27/07/2012 - alertar x dias antes de la fecha de vencimiento de la cuenta
  $_dias_alerta_vencimiento = 7;
  //pgs - 19/07/2013 - segunda alerta de x dias antes de la fecha de vencimiento de la cuenta
  $_dias_alerta_vencimiento2 = 2;
?>