<?php
require_once("Envios.php");
require_once("Includes/Correo.php");

$obj_envios = new Envios();
                                          
$_obj_interfaz->adicionarJS("Envio.js");

$_obj_interfaz->asignarCampos($_campos);

switch($datos['accion'])
{
	
	case 'index'.validarAcceso(array('R','U')):
		if(isset($datos['msg'])){
			$opciones['ACCION'] = 1;
			$opciones['CADENA'] = 'El Env&iacute;o';
			$datos['mensaje_ok'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}//Fin de if(isset($datos['msg']))
		//jcb - 8/06/2017 extraemos numeros erroneos
		if(isset($datos['msg_archivo'])){
			$opciones['ACCION'] = 1;
			$opciones['CADENA'] = 'Los Destinatarios';
			$opciones['CADENA_NUMEROS_ERRONEOS'] = $_SESSION['numeros_erroneos']; //jcb - 8/06/2017 mandamos numeros erroneos a mensajes
			$opciones['CANTIDAD_NUMEROS_ERRONEOS'] = $_SESSION['cantidad_numeros_erroneos']; //jcb - 8/06/2017 mandamos numeros erroneos a mensajes
			unset($_SESSION['numeros_erroneos']);
        	unset($_SESSION['cantidad_numeros_erroneos']);
			$datos['mensaje_archivo'] = $_obj_mensajes->crearMensaje($datos['msg_archivo'], $opciones);
		}//Fin de if(isset($datos['msg_archivo']))
		// fin jcb 08/06/2017
		$contenido = $obj_envios->obtenerIndexEnvios($datos);
	
		$_obj_interfaz->asignarContenido($contenido);
	
		break;
		
	case 'listado_envios'.validarAcceso(array('R','U')):
	 //pgs - 29/08/2012 - se incluyen los archivos para el lightbox
   $_obj_interfaz->adicionarJS("greybox/AJS.js");
	 $_obj_interfaz->adicionarJS("greybox/AJS_fx.js");
	 $_obj_interfaz->adicionarJS("greybox/gb_scripts.js");
	 $_obj_interfaz->adicionarCSS("greybox/gb_styles.css");
	
		$contenido = '';
		
    $valores = $obj_envios->listarEnvios();
		
		if(isset($datos['msg'])){
			$opciones['ACCION'] = $datos['tipo_gestion'];
			$opciones['CADENA'] = 'El Bolet&iacute;n';
			$arreglo_datos['MENSAJE'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}//Fin de if(isset($datos['msg'])) 
		
		$campos = array(
      "env_id" => "Cons",
      "env_nombre" => "Nombre",
      "env_mensaje_original" => "Mensaje",       
      "env_fecha_envio" => "Fecha de env&iacute;o",
      "pendientes" => "Pendientes",       
      "enviados" => "Enviados",
	    "fallidos" => "Fallidos"
    );  

                       
		$opciones['ver'] = "env_id=%env_id%"; 
     
		
		$arreglo_datos['TITULO'] = "";
    
		$campos_ordena = array("env_id","env_nombre","env_mensaje_original","env_fecha_envio","pendientes","enviados","fallidos");
						
		$arreglo_datos['CAMPOS'] = $campos;
		$arreglo_datos['CAMPOS_ORDENA'] = $campos_ordena;
		$arreglo_datos['LISTADO'] = $valores;
		$arreglo_datos['OPCIONES'] = $opciones;  
		
		$arreglo_datos['ACCION_LISTAR'] = "listado_envios&m=envios";
		$arreglo_datos['ACCION_LISTAR'] .= $enlace_listado;
		
		$arreglo_datos['ACCION_VER'] = "ver_envios_fallidos&m=envios";
		$arreglo_datos['ACCION_VER'] .= $enlace_listado;
		/*$arreglo_datos['ACCION_ENVIAR'] = "fm_enviar&m=boletines&campo_busqueda=".$datos['campo_busqueda'];
		$arreglo_datos['ACCION_ENVIAR'] .= $enlace_busqueda;
		
		//pgs - 29/08/2012 - se a�ade la accion de probar un env�o
		$arreglo_datos['ACCION_PROBAR'] = "fm_probar&m=boletines&campo_busqueda=".$datos['campo_busqueda'];
		$arreglo_datos['ACCION_PROBAR'] .= $enlace_busqueda;
		
		$arreglo_datos['ACCION_ENVIOS'] = "listado_envios&m=boletines&campo_busqueda=".$datos['campo_busqueda'];
		$arreglo_datos['ACCION_ENVIOS'] .= $enlace_busqueda;
		

		
		$arreglo_datos['ACCION_COPIAR'] = "copiar_boletin&m=boletines&campo_busqueda=".$datos['campo_busqueda'];
		$arreglo_datos['ACCION_COPIAR'] .= $enlace_busqueda;
		
		$arreglo_datos['ACCION_ELIMINAR'] = "eliminar&m=boletines&campo_busqueda=".$datos['campo_busqueda'];
		$arreglo_datos['ACCION_ELIMINAR'] .= $enlace_busqueda;
		
		$arreglo_datos['ACCION_VOLVER'] = "";
		$arreglo_datos['DATOS'] = $datos;*/
    
    $arreglo_datos['ACCION_VACIAR_TABLA_ENVIOS']="vaciar_tabla_envios&m=envios";
		
   
    //chcm.  Mayo 03 2017.  Reemplazo los caracteres "<" y ">" por el ascii correspondiente para que salga en pantalla
    for($i=0;$i<sizeof($arreglo_datos['LISTADO']);$i++)
    {
        $mensaje_original=$arreglo_datos['LISTADO'][$i]['env_mensaje_original'];
        $mensaje_original = str_replace("<","&#60;",$mensaje_original);
        $mensaje_original = str_replace(">","&#62;",$mensaje_original);
        $arreglo_datos['LISTADO'][$i]['env_mensaje_original']=$mensaje_original;
    }
    
   
    
		$contenido .= $_obj_interfaz->crearListado($arreglo_datos);
    $_obj_interfaz->asignarContenido($contenido);
    
		break;
		case 'ver_envios_fallidos'.validarAcceso(array('R','U')):
		
		
			$contenido = $obj_envios->obtenerEnviosFallidos($datos);
			if(count($contenido)>0){
				//jcb-27/04/2017- descarga de archivo csv con contenido
				$csv_filename = 'envios_fallidos_'.date('Y-m-d').'.csv';
				// Export the data and prompt a csv file for download
				header("Content-type: text/x-csv");
				header("Content-Disposition: attachment; filename=".$csv_filename."");
				echo($contenido);
				die();
		
			} // fin if(count($contenido)>0)
			//$_obj_interfaz->asignarContenido($contenido);
		
			break;
	case 'ver_registros'.validarAcceso(array('R','U')):
		//pgs - 24/06/2011 - se pone mas detallado el listado de destinatarios
    //nombre del listado de destinatarios Ej: Listado de Destinatarios de los enviados
    $tipo_lista = '';
		switch($datos['tipo']){
      case 'enviados':
        $tipo_lista = ' que se les enviar&oacute;n el correo.';
      break;
      
      case 'no_enviados':
        $tipo_lista = ' que no se les envi&oacute; el correo.';
      break;
      
      case 'pendientes':
        $tipo_lista = ' que estan pendientes por enviar.';
      break;
      
      case 'leidos':
        $tipo_lista = ' que leyeron el correo.';
      break;
      
      case 'baja':
        $tipo_lista = ' que se dieron de baja.';
      break;
    }//Fin de switch($datos['tipo'])
		
		
    $contenido = '<div class="listado_titulo">Listado de destinatarios '.$tipo_lista.'</div>';
    
    $info_bol_envio = $obj_envios->obtenerEnvioBoletin($datos);
    $contenido .= '<br><div class="listado_subtitulo">Env&iacute;o "'.$info_bol_envio['enb_fecha_inicio'].'" para el Bolet&iacute;n "'.$info_bol_envio['bol_nombre'].'"</div>';
		
		
    $valores = $obj_envios->listarDestinatarios($datos);
		
		if(isset($datos['msg'])){
			$opciones['ACCION'] = $datos['tipo_gestion'];
			$opciones['CADENA'] = 'El Destinatario';
			$arreglo_datos['MENSAJE'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}//Fin de if(isset($datos['msg'])) 
		
		$campos = array(
      "des_id" => "",
      "des_email" => "Email",
      "des_nombre" => "Nombre", 
      "gru_nombre" => "Grupo"
    );  
                       
		//$opciones['actualizar'] = "des_id=%des_id%&informe=true&tipo=".$datos['tipo']."&enb_id=".$datos['enb_id'];  
		//$opciones['eliminar'] = "des_id=%des_id%&informe=true&tipo=".$datos['tipo']."&enb_id=".$datos['enb_id']; 
		
		$arreglo_datos['TITULO'] = "";
    
		$campos_ordena = array("des_email","des_nombre","gru_nombre");
						
		$arreglo_datos['CAMPOS'] = $campos;
		$arreglo_datos['CAMPOS_ORDENA'] = $campos_ordena;
		$arreglo_datos['LISTADO'] = $valores;
		$arreglo_datos['OPCIONES'] = $opciones;  
		
		$arreglo_datos['ACCION_LISTAR'] = "ver_registros&m=boletines&campo_busqueda=".$datos['campo_busqueda']."&enb_id=".$datos['enb_id']."&bol_id=".$datos['bol_id']."&tipo=".$datos['tipo'];
		$arreglo_datos['ACCION_LISTAR'] .= $enlace_listado;
		
		$arreglo_datos['ACCION_ACTUALIZAR'] = "fm_editar&m=destinatarios&campo_busqueda=".$datos['campo_busqueda']."&grupo=".$datos['grupo'];
		$arreglo_datos['ACCION_ACTUALIZAR'] .= $enlace_busqueda;
		
		$arreglo_datos['ACCION_ELIMINAR'] = "eliminar&m=destinatarios&campo_busqueda=".$datos['campo_busqueda']."&grupo=".$datos['grupo'];
		$arreglo_datos['ACCION_ELIMINAR'] .= $enlace_busqueda;
		
		$arreglo_datos['ACCION_VOLVER'] = "";
		$arreglo_datos['DATOS'] = $datos;
		
		$contenido .= $_obj_interfaz->crearListado($arreglo_datos);
		$contenido .= '<div class="contenido_acciones">
                  <br>
                    <div class="buttons" style="text-align:right;margin-right:10px;"><a class="positive" href="index.php?m=boletines&accion=ver_informe&enb_id='.$datos['enb_id'].'&bol_id='.$datos['bol_id'].'"><img src="Includes/Imagenes/listado.png" width="30" border="0"> Volver al Informe</a></div>
                  <br>
                </div>';
    $_obj_interfaz->asignarContenido($contenido);
		break;
	
	case 'fm_nuevo'.validarAcceso(array('R','U')):
	    $_obj_interfaz->adicionarJS("ckeditor/ckeditor.js");
	    $_obj_interfaz->adicionarJS("ckeditor/ckfinder.js");
	  
	    $_obj_interfaz->adicionarJS("jquery.bgiframe.js");
	    $_obj_interfaz->adicionarJS("jquery.dimensions.js");
	    $_obj_interfaz->adicionarJS("jquery.tooltip.js");
	    
	    $_obj_interfaz->adicionarCSS("jquery.tooltip.css");
		
	    if(isset($_POST['POST'])){
	      $resultado = $obj_envios->ingresarEnvios($datos);
	      
	      if($resultado=='E-0' || $resultado=='E-8'){
	        header('Location: index.php?m=envios&accion=index&tipo_gestion=1&msg='.$resultado);
	      }else{
	        $datos['msg'] = $resultado;
	      }
	    }//Fin de if(isset($_POST['_POST']))
	    
		if(isset($datos['msg'])){
			$opciones['ACCION'] = 1;
			$opciones['CADENA'] = 'El Env&iacute;o';
			$datos['mensaje_ok'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}//Fin de if(isset($datos['msg']))
	    
	    $contenido = $obj_envios->obtenerFormularioNuevo($datos); 
	    $_obj_interfaz->asignarContenido($contenido);
	    
	 break;
	 
	 case 'fm_editar'.validarAcceso(array('R','U')):
    $_obj_interfaz->adicionarJS("ckeditor/ckeditor.js");
	  $_obj_interfaz->adicionarJS("ckeditor/ckfinder.js");
   
    $_obj_interfaz->adicionarJS("jquery.bgiframe.js");
    $_obj_interfaz->adicionarJS("jquery.dimensions.js");
    $_obj_interfaz->adicionarJS("jquery.tooltip.js");   
   $_obj_interfaz->adicionarJS("funciones_jquery.js");
    
    $_obj_interfaz->adicionarCSS("jquery.tooltip.css");
   
    if(isset($_POST['POST'])){
      $resultado = $obj_envios->editarBoletin($datos);
      
      if($resultado == 1)
	  {
        header('Location: index.php?m=boletines&accion=listado_boletines&tipo_gestion=2&msg='.$resultado);
      }else{
        $datos['msg'] = $resultado;
      }
    }//Fin de if(isset($_POST['_POST']))
    
    if(isset($datos['msg'])){
			$opciones['ACCION'] = 2;
			$opciones['CADENA'] = 'El Bolet&iacute;n';
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}//Fin de if(isset($datos['msg']))

    $valores = $obj_envios->obtenerBoletin($datos);    
    $contenido = $obj_envios->obtenerFormularioEditar($datos,$valores); 
    
    $_obj_interfaz->asignarContenido($contenido);
	 break;
	 
	 case 'fm_enviar'.validarAcceso(array('R','U')):
    //revisamos si tiene envios por hacer, si tiene no dejara agendar otro env�o hasta que termine el anterior 17-05-2011 - pablo
    $estado_enviar = $obj_envios->estadoEnviar($datos);
    
    //si no puede agendar un env�o le informa al usuario porque no puede 17-05-2011 - pablo
    if(!$estado_enviar){
      header('Location: index.php?m=boletines&accion=listado_boletines&msg=E-1');
      exit;
    }//Fin de if(!$estado_enviar)
    
    if(isset($_POST['POST'])){
      $resultado = $obj_envios->registrarEnvio($datos);
      
      if($resultado=='E-0'){
        header('Location: index.php?m=boletines&accion=listado_boletines&tipo_gestion=5&msg='.$resultado);
      }else{
        $datos['msg'] = $resultado;
      }
    }//Fin de if(isset($_POST['_POST']))
    
    if(isset($datos['msg'])){
			$opciones['ACCION'] = 5;
			$opciones['CADENA'] = 'El Env&iacute;o';
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}//Fin de if(isset($datos['msg']))

    $valores = $obj_envios->obtenerBoletin($datos);   
    $contenido = $obj_envios->obtenerFormularioEnviar($datos,$valores); 
    
    $_obj_interfaz->asignarContenido($contenido);
	 break;
	 
	 //pgs - 29/08/2012 - funcion que se encarga de desplegar el formulario para enviar un correo de prueba
	 case 'fm_probar'.validarAcceso(array('R','U')):
    if(isset($_POST['POST'])){
      //se traen los datos del boletin y del cliente para enviar el correo de prueba
      $datos_boletin = $obj_envios->obtenerDatosBoletinCliente($datos);
      $resultado = $obj_envios->enviarCorreoPrueba($datos,$datos_boletin);
      $datos['msg'] = $resultado;
    }//Fin de if(isset($_POST['_POST']))
    
    if(isset($datos['msg'])){
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
		}//Fin de if(isset($datos['msg']))
   
    $contenido = $obj_envios->obtenerFormularioProbar($datos,$valores); 
    $_obj_interfaz->asignarContenido($contenido);
    $contenido = $_obj_interfaz->crearPopUp();
    print_r($contenido);exit;
	 break;
	 
	 case 'eliminar'.validarAcceso(array('R','U')):
    $resultado = $obj_envios->eliminarBoletin($datos);
    header('Location: index.php?m=boletines&accion=listado_boletines&tipo_gestion=3&msg='.$resultado);
	 break;
	 
	 case 'cambiar_estado'.validarAcceso(array('R','U')):
	  $resultado = $obj_envios->cambiarEstadoBoletin($datos);
    header('Location: index.php?m=boletines&accion=listado_boletines&tipo_gestion=7&msg='.$resultado);
	 break;
	 
	 case 'copiar_boletin'.validarAcceso(array('R','U')):
	  $resultado = $obj_envios->copiarBoletin($datos);
    header('Location: index.php?m=boletines&accion=listado_boletines&tipo_gestion=8&msg='.$resultado);
	 break;
	 
	 case 'ver_informe'.validarAcceso(array('R','U')):
	  $estado_informe = $obj_envios->estadoInformeBoletin($datos);
	  
    //pgs - 24/09/2011 - se envia el nuevo parametro de si ya se creo el informe en la bd
    $datos['informe_creado'] = $estado_informe['informe_creado'];
    
    //pgs - 07/06/2012 - se envia el nuevo parametro de si ya se termino de procesar el envio
    $datos['fin_procesado'] = $estado_informe['fin_procesado'];
	  
	  //pgs - 07/06/2012 - ya no se valida de que pueda ver o no el informe, teniendo en cuenta las modificaciones 
	  //del 21 de marzo de 2012
    //if($estado_informe['resultado']){
  	  $valores = $obj_envios->informeBoletin($datos);
      
      
  	  $envio_boletin = $obj_envios->obtenerEnvioBoletin($datos);
  	  $valores = array_merge($valores,$envio_boletin);
  	  $contenido = $obj_envios->obtenerInforme($datos,$valores); 
      
      $_obj_interfaz->asignarContenido($contenido);
    /*}else{
      header('Location: index.php?accion=listado_envios&m=boletines&campo_busqueda='.$datos['campo_busqueda'].'&bol_id='.$datos['bol_id'].'&registrados='.$estado_informe['registrados'].'&total='.$estado_informe['total'].'&msg=I-0');
    }//Fin de if($estado_informe['resultado'])*/
	 break;
	 
	 case 'pruebas'.validarAcceso(array('R')):
    $resultado = $obj_envios->pruebaCorreosProgramados($datos);
    $_obj_interfaz->asignarContenido($resultado);
	 break;
	 
	 case 'enviar_pendientes'.validarAcceso(array('R','U')):
    $resultado = $obj_envios->enviarPendientes($datos);
    /*if(isset($datos['msg']) && $datos['msg']=='E-1'){
    	$opciones['ACCION'] = 5;
    	$opciones['CADENA'] = 'El Env&iacute;o';
    	$datos['mensaje_ok'] = $_obj_mensajes->crearMensaje($datos['msg'], $opciones);
    }*///Fin de if(isset($datos['msg']))
    $_obj_interfaz->asignarContenido($resultado);
	 break;
	 
	 case 'baja'.validarAcceso(array()):
	  $resultado = $obj_envios->darBaja($datos);
	  
	  if(isset($resultado)){
			$opciones['ACCION'] = $datos['tipo_gestion'];
			$opciones['CADENA'] = 'El Destinatario';
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($resultado, $opciones);
		}//Fin de if(isset($datos['msg'])) 
	  
	  $contenido = $obj_envios->obtenerFormularioDarBaja($datos);
	  $_obj_interfaz->asignarContenido($contenido);
	 break;
	 
	 case 'imagen'.validarAcceso(array()):
	  $resultado = $obj_envios->registrarLecturaCorreo($datos);
    
    $im = file_get_contents($_PATH_IMAGENES.'qtl.png');
    header('content-type: image/jpeg');
    echo $im;exit;
	 break;
	 
	case 'eliminar_adjunto_boletin'.validarAcceso(array('R','U')):
    	$resultado = $obj_envios->eliminarArchivoAdjuntoBoletin($datos);
    	echo $resultado;
    	exit;
	break;
  
  	case 'vaciar_tabla_envios'.validarAcceso(array('R','U')):
    	$contenido = $obj_envios->vaciarTablaEnvios();
    	$_obj_interfaz->asignarContenido($contenido);
	break;
  
  
  
	 
}//Fin de switch($datos['accion'])
?>