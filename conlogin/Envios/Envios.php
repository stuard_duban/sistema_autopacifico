<?php

class Envios
{

	var $_plantillas = "";

	function Envios()
	{
		global $_PATH_LOCAL;
		
		$this->_plantillas = $_PATH_LOCAL."/Envios/Plantillas";
		
		$this->campos_busqueda = array("busq_acc_user","busq_acc_nombre",
					"busq_acc_per_id","offset","tipo_ordena","ordena");
					
		$this->campos_busqueda_listado = array("busq_acc_user","busq_acc_nombre",
					"busq_acc_per_id");
		
	}//Fin de Boletines()
	/*	***********************************	OBTENER INDEX ENVIOS	*************************************	*/
	/*
	 *	Utilidad:
	 *		la pagina index
	 *	Paraetros de entrada:
	 *
	 *	Valores de retorno:
	 *		$res -> contenido del index
	 */
	function obtenerIndexEnvios($datos){
		global $_obj_database;
		$sql = "select *
				from destinatario";
			
		$num_des_disponibles = $_obj_database->obtenerCantidadResultados($sql);
		$num_envios_pendientes = $this-> obtenerCountEnviosPendientes();
		if($num_des_disponibles<=0){
			
			$contenido = Archivos::obtenerContenidoArchivo($this->_plantillas."/msg_crear_destinatarios.html");
			
		}else{
			$contenido = Archivos::obtenerContenidoArchivo($this->_plantillas."/index.html");
		}
		if(isset($num_envios_pendientes[0])){
			Interfaz::asignarToken("cantidad_envios_pendientes", $num_envios_pendientes[0]['pendientes'], $contenido);
		}else{
			Interfaz::asignarToken("cantidad_envios_pendientes", 0, $contenido);
		}
		Interfaz::asignarToken("mensaje_ok", $datos['mensaje_ok'], $contenido);
		Interfaz::asignarToken("mensaje_archivo", $datos['mensaje_archivo'], $contenido);
		Interfaz::asignarToken("cantidad_destinatarios", $num_des_disponibles, $contenido);
		
		return $contenido;
	}
   //chcm.  Feb 27 2015
  /**
 * Funci�n que se encarga de obtener el % de espacio libre en disco
 * duro
 * DM - 2013-11-09
 ***/
function obtenerPorcentajeEspacioDisponible()
{
    //Comando para consultar la cantidad de espacio libre en disco
    $comando = 'df -h /dev/mapper/VolGroup00-LogVol00';
    //ejecuta el comando en el shell
    $resultado_comando = trim( exec($comando) );
    //retira los / de la cadena que retorna
    $resultado_comando = str_replace('/','',$resultado_comando);
    //parte el comando en espacios en blanco para obtener el n�mero del porcentaje disponible
    $resultado_partes = preg_split('/ /',$resultado_comando);
    $porcentaje_libre = $resultado_partes[7];
    $porcentaje_libre = str_replace('%','',$porcentaje_libre);
    
    return $porcentaje_libre;
}//Fin de obtenerPorcentajeEspacioDisponible


	
	function obtenerFormularioNuevo($datos){
    global $_PATH_WEB, $_PATH_ACCION, $_ext_archivo_boletin, $_MAX_FILE_SIZE_ADJUNTOS;
		
    $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas."/fm_nuevo.html");
    
    //Interfaz::asignarToken("bol_nombre", $datos['bol_nombre'], $contenido);
    //Interfaz::asignarToken("bol_asunto", $datos['bol_asunto'], $contenido);
    //Interfaz::asignarToken("bol_nombre_remitente", $datos['bol_nombre_remitente'], $contenido);
    //pgs - 04/10/2011 - para evitar el spam no se le permitira al usuario poner un remitente de correo diferente
    //al correo con el cual se esta autenticando para enviar sus boletines
    //Interfaz::asignarToken("bol_correo_remitente", $_SESSION['usu_smtp_cuenta'], $contenido);
    //Interfaz::asignarToken("bol_correo_remitente", $datos['bol_correo_remitente'], $contenido);
    //Interfaz::asignarToken("bol_correo_rebotado", $datos['bol_correo_rebotado'], $contenido);
    //pgs - 04/10/2011 - fin de la modificacion
    
    //Interfaz::asignarToken("bol_html", $datos['bol_html'], $contenido);
    
    //$datos['array'] = array(1=>'Activo',0=>'Inactivo');
    //$datos['nombre_campo'] = 'bol_estado';
    //$datos['valor_defecto'] = false;
    /*if(empty($datos['bol_estado'])){
      $datos['bol_estado'] = 1;
    }*/ 
    //$datos['valor'] = $datos['bol_estado'];
    //$bol_estado = Herramientas::crearSelectFormaDesdeArray($datos);
    //Interfaz::asignarToken("bol_estado", $bol_estado, $contenido);
    
    //$extensiones = implode(", ", $_ext_archivo_boletin); 
    //Interfaz::asignarToken("extensiones", $extensiones, $contenido);
                                                 			  
    //Interfaz::asignarToken("archivos_previos", "", $contenido);
    
    Interfaz::asignarToken("accion", 'fm_nuevo', $contenido);
    Interfaz::asignarToken("titulo", 'Crear Nuevo Env&iacute;o', $contenido);
    Interfaz::asignarToken("mensaje_ok", $datos['mensaje_ok'], $contenido);
    //Interfaz::asignarToken("campo_busqueda", $datos['campo_busqueda'], $contenido);
    
    //Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);   
    //Interfaz::asignarToken("max_file_size", $_MAX_FILE_SIZE_ADJUNTOS, $contenido);
    
    //$tam_megas = round(($_MAX_FILE_SIZE_ADJUNTOS/1048576), 1);     
    //Interfaz::asignarToken("tam_megas", $tam_megas, $contenido);
    
    return $contenido;
  }//Fin de obtenerFormularioNuevo($datos)
  
  function obtenerFormularioEditar($datos,$valores){
    global $_PATH_WEB, $_PATH_IMAGENES, $_PATH_ACCION, $_ext_archivo_boletin, $_PATH_ADJUNTOS, $_MAX_FILE_SIZE_ADJUNTOS;
		
    $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas."/fm_nuevo.html");
    
    Interfaz::asignarToken("bol_nombre", $valores['bol_nombre'], $contenido);
    Interfaz::asignarToken("bol_asunto", $valores['bol_asunto'], $contenido);
    
    Interfaz::asignarToken("bol_nombre_remitente", $valores['bol_nombre_remitente'], $contenido);
    
    //pgs - 04/10/2011 - para evitar el spam no se le permitira al usuario poner un remitente de correo diferente
    //al correo con el cual se esta autenticando para enviar sus boletines
    Interfaz::asignarToken("bol_correo_remitente", $_SESSION['usu_smtp_cuenta'], $contenido);
    //Interfaz::asignarToken("bol_correo_remitente", $valores['bol_correo_remitente'], $contenido);
    //Interfaz::asignarToken("bol_correo_rebotado", $valores['bol_correo_rebotado'], $contenido);
    //pgs - 04/10/2011 - fin de la modificacion
        
    Interfaz::asignarToken("bol_html", $valores['bol_html'], $contenido);
    Interfaz::asignarToken("bol_id", $valores['bol_id'], $contenido);
    
    $datos['array'] = array(1=>'Activo',0=>'Inactivo');
    $datos['nombre_campo'] = 'bol_estado';
    $datos['valor_defecto'] = false;
    if(empty($datos['bol_estado'])){
      $datos['bol_estado'] = 1;
    } 
    $datos['valor'] = $valores['bol_estado'];
    $bol_estado = Herramientas::crearSelectFormaDesdeArray($datos);
    Interfaz::asignarToken("bol_estado", $bol_estado, $contenido);      
    
    $extensiones = implode(", ", $_ext_archivo_boletin); 
    Interfaz::asignarToken("extensiones", $extensiones, $contenido);
    
    $enviar_boletin = "<a class='positive' href='index.php?m=boletines&accion=fm_enviar&bol_id=".$valores['bol_id']."'><img src='Includes/Imagenes/editar.png' width='30' border='0'> Enviar Bolet&iacute;n</a>";
    Interfaz::asignarToken("enviar_boletin", $enviar_boletin, $contenido);
    
    $archivos_adjuntos = $this->obtenerArchivosAdjuntosBoletin($valores);
  	/*$archivos_previos = '<tr>
				<td colspan="2" align="left" class="forma_label"><br>Archivos subidos previamente:</td>					
			</tr>
			<tr>
			  <td colspan="2" align="left" >
			  	<div class="nota_azul">
				  	<img src="Includes/Imagenes/informacion.png" width="15" border="0"> Recuerde que dispone de hasta 4 archivos adjuntos para el bolet&iacute;n, asi que si desea subir m&aacute;s archivos teniendo ya la capacidad m&aacute;xima, debe eliminar alguno de los archivos previamente subidos. 
				</div>
	          </td>
			</tr>';*/
    
    if(sizeof($archivos_adjuntos) > 0)
    {		
		$cont_adj = 0;
		foreach($archivos_adjuntos as $adjunto)
		{			
			$archivos_previos .= '<tr>
					<td class="forma_label" align="right">Archivo "'.$adjunto["adj_nombre_archivo"].'": </td>
					<td align="left">
						<div id="fila_archivo_'.$cont_adj.'" style="display: inline-block;">
						<a href="'.$_PATH_ADJUNTOS.'/'.$valores['bol_id'].'/'.$adjunto["adj_nombre_archivo"].'" target="_blank">Ver archivo</a>&nbsp;&nbsp;&nbsp;
						<a class="no_href" onclick="eliminarArchivoAdjunto(\''.$adjunto["adj_id"].'\', \''.$cont_adj.'\');">Eliminar archivo</a>&nbsp;&nbsp;&nbsp;
						</div>
						<div id="img_load_'.$cont_adj.'" style="display: none;"><img src="'.$_PATH_IMAGENES.'ajax-loader.gif" align="absmiddle" /></div>
					</td>
				</tr>';
				
			$cont_adj++;
		} 
	}
	else
	{
	/*	$archivos_previos .= '<tr>
				<td align="center" colspan="2">
					<br />No hay archivos adjuntos subidos previamente.
				</td>
			</tr>';*/
	}
                                                                           
    Interfaz::asignarToken("archivos_previos", $archivos_previos, $contenido);
    
    Interfaz::asignarToken("accion", 'fm_editar', $contenido);
    Interfaz::asignarToken("titulo", 'Editar Bolet&iacute;n', $contenido);
    Interfaz::asignarToken("campo_busqueda", $datos['campo_busqueda'], $contenido);
    
    Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);   
    Interfaz::asignarToken("max_file_size", $_MAX_FILE_SIZE_ADJUNTOS, $contenido);  
    
    $tam_megas = round(($_MAX_FILE_SIZE_ADJUNTOS/1048576), 1);     
    Interfaz::asignarToken("tam_megas", $tam_megas, $contenido);
    
    return $contenido;
  }//Fin de obtenerFormularioEditar($datos)
  
  function obtenerFormularioEnviar($datos,$valores){
    global $_PATH_WEB,$_PATH_IMAGENES, $_PATH_ACCION;
		
    $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas."/fm_enviar.html");
    
    Interfaz::asignarToken("bol_nombre", $valores['bol_nombre'], $contenido);
    Interfaz::asignarToken("bol_asunto", $valores['bol_asunto'], $contenido);
    Interfaz::asignarToken("bol_nombre_remitente", $valores['bol_nombre_remitente'], $contenido);
    Interfaz::asignarToken("bol_correo_remitente", $valores['bol_correo_remitente'], $contenido);
    Interfaz::asignarToken("bol_correo_rebotado", $valores['bol_correo_rebotado'], $contenido);
    Interfaz::asignarToken("bol_html", $valores['bol_html'], $contenido);
    Interfaz::asignarToken("bol_id", $valores['bol_id'], $contenido);
    
    $datos['array'] = array(1=>'Activo',0=>'Inactivo');
    $datos['nombre_campo'] = 'bol_estado';
    $datos['valor_defecto'] = false;
    if(empty($datos['bol_estado'])){
      $datos['bol_estado'] = 1;
    } 
    $datos['valor'] = $valores['bol_estado'];
    $bol_estado = Herramientas::crearSelectFormaDesdeArray($datos);
    Interfaz::asignarToken("bol_estado", $bol_estado, $contenido);
    
    $grupos = $this->obtenerGrupos($datos);
    
    if(sizeof($grupos)<=0){
      header('Location: index.php?m=boletines&accion=listado_boletines&msg=B-2');exit;
    }//Fin de if(sizeof($grupos)<=0)
    
    //pgs - 07/06/2012 - se cambia la forma en que se muestran los grupos, ahora solo se puede escoger un grupo
    //mediante un select
    $array_grupos = array();
    foreach($grupos as $key=>$grupo){
      $array_grupos[$key] = $grupo['gru_nombre'];
    }//Fin de foreach($grupos as $grupo)
    
    $datos_grupos['array'] = $array_grupos;
    $datos_grupos['nombre_campo'] = 'bol_gru_id';
    $datos_grupos['valor'] = $datos['bol_gru_id'];
    $select_grupos = Herramientas::crearSelectFormaDesdeArray($datos_grupos);
    Interfaz::asignarToken("grupos", $select_grupos, $contenido);
    
    /*pgs - 07/06/2012 - se cambia la forma en que se muestran los grupos, ahora solo se puede escoger un grupo
    $html_grupos = '';
    $salto = 1;
    foreach($grupos as $grupo){
      $checked = '';
      
      $html_grupos .= '<input type="checkbox" name="bol_gru_id[]" value="'.$grupo['gru_id'].'" '.$checked.'> '.$grupo['gru_nombre'].'&nbsp;&nbsp;';
      if($salto%6 == 0){
        $html_grupos .= '<br>';
      }
      $salto++;  
    }//Fin de foreach($grupos as $grupo)
    
    Interfaz::asignarToken("grupos", $html_grupos, $contenido);*/
    
    $dias = array();
    for($i=1;$i<=31;$i++){
      $dias[$i]=$i;
    }
    $datos['array'] = $dias;
    $datos['nombre_campo'] = 'bol_dia';
    $datos['valor_defecto'] = false;
    if(empty($datos['bol_dia'])){
      $datos['bol_dia'] = date('d');
    } 
    $datos['valor'] = $datos['bol_dia'];
    $bol_dia = Herramientas::crearSelectFormaDesdeArray($datos);
    
    $meses = array(1=>"Enero",2=>"Febrero",3=>"Marzo",4=>"Abril",5=>"Mayo",6=>"Junio",7=>"Julio",8=>"Agosto",9=>"Septiembre",10=>"Octubre",11=>"Noviembre",12=>"Diciembre");
    $datos['array'] = $meses;
    $datos['nombre_campo'] = 'bol_mes';
    $datos['valor_defecto'] = false;
    if(empty($datos['bol_mes'])){
      $datos['bol_mes'] = date('m');
    } 
    $datos['valor'] = $datos['bol_mes'];
    $bol_mes = Herramientas::crearSelectFormaDesdeArray($datos);
    
    $anhos = array();
    $anho_actual = date('Y');
    for($i=$anho_actual;$i<=$anho_actual+5;$i++){
      $anhos[$i]=$i;
    }
    $datos['array'] = $anhos;
    $datos['nombre_campo'] = 'bol_anho';
    $datos['valor_defecto'] = false;
    if(empty($datos['bol_anho'])){
      $datos['bol_anho'] = date('Y');
    } 
    $datos['valor'] = $datos['bol_anho'];
    $bol_anho = Herramientas::crearSelectFormaDesdeArray($datos);
    
    $bol_fecha_envio =  $bol_dia.$bol_mes.$bol_anho;
    Interfaz::asignarToken("bol_fecha_envio", $bol_fecha_envio, $contenido);
    
    $horas = array();
		for($i = 1;$i<=24;$i++){
			$tmp = date('H:i:s',mktime($i,0,0));
			$horas[$tmp] = $tmp;
		}
    $datos['array'] = $horas;
    $datos['nombre_campo'] = 'bol_hora';
    $datos['valor_defecto'] = false;
    if(empty($datos['bol_hora'])){
      //chcm.  Marzo 8 2015. quito que el env�o se haga una hora despu�s a la actual, sino que se haga sobre la misma hora
      //$datos['bol_hora'] = (date('H')+1).':00:00';
        $datos['bol_hora'] = (date('H')).':00:00';
    } 
    $datos['valor'] = $datos['bol_hora'];
    $bol_hora = Herramientas::crearSelectFormaDesdeArray($datos); 
    Interfaz::asignarToken("bol_hora_envio", $bol_hora, $contenido);
    
    $datos['array'] = array(1=>'Activo',0=>'Inactivo');
    $datos['nombre_campo'] = 'bol_estado';
    $datos['valor_defecto'] = false;
    if(empty($datos['bol_estado'])){
      $datos['bol_estado'] = 1;
    } 
    $datos['valor'] = $valores['bol_estado'];
    $bol_estado = Herramientas::crearSelectFormaDesdeArray($datos);
    Interfaz::asignarToken("bol_estado", $bol_estado, $contenido);
    
    //pgs - 27/20/2011 - se muestran los archivos adjuntos
    if(sizeof($valores['archivos_adjuntos'])>0){
      $html = '<table border="0" cellspacing="0" cellpading="0">';
      foreach($valores['archivos_adjuntos'] as $adjunto){
        $html .= '<tr>
                      <td align="left" class="forma_label">'.$adjunto['adj_nombre_archivo'].'</td>
                      <td align="right" width="100"><a href="'.$_PATH_WEB.'Adjuntos/'.$datos['bol_id'].'/'.$adjunto['adj_nombre_archivo'].'" target="_blank">Ver archivo</a></td>
                  <tr>';
      }//Fin de foreach($valores['archivos_adjuntos'] as $adjunto)
      $html .= '</table>';
      Interfaz::asignarToken("archivos_adjuntos", $html, $contenido);
    }else{
      Interfaz::asignarToken("archivos_adjuntos", '<span class="nota">No hay archivos adjuntos asociados al boletin.</span>', $contenido);
    }//Fin de if(sizeof($valores['archivos_adjuntos'])>0)
    //fin modificacion pgs - 27/20/2011
    
    Interfaz::asignarToken("accion", 'fm_enviar', $contenido);
    Interfaz::asignarToken("titulo", 'Enviar Bolet&iacute;n', $contenido);
    Interfaz::asignarToken("campo_busqueda", $datos['campo_busqueda'], $contenido);
    
    Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);    
    
    return $contenido;
  }//Fin de obtenerFormularioEnviar($datos)
  
  function obtenerInforme($datos,$valores){
    global $_PATH_WEB, $_PATH_ACCION,$_limite_informe;
		
    $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas."/informe.html");
    
    //pgs - 24/09/2011 - si tiene informe creado en la bd se muestra la nota y se deshabilitan los botones de ver registros
    if($datos['informe_creado']=='1'){
        Interfaz::asignarToken('mostrar','display:none;',$contenido);
        Interfaz::asignarToken('nota','<b>Nota:</b> El env&iacute;o se realizo hace mas de '.$_limite_informe.' d&iacute;as por lo cual no podra ver el detalle de cada uno de los items.',$contenido);
    }else{
        Interfaz::asignarToken('mostrar','',$contenido);
        Interfaz::asignarToken('nota','',$contenido);
    }//Fin de if($datos['informe_creado'])
    
    //pgs - 30/08/2012 - se a�ade al tiitulo el nombre del grupo a quien envi� el bolet�n
    $titulo_pag = 'Informe del Env&iacute;o "'.$valores['enb_fecha_inicio'].'" para el Bolet&iacute;n "'.$valores['bol_nombre'].'"<br>
    <br>Grupo al que se le envi&oacute; este Bolet&iacute;n: '.$valores['gru_nombre'];
    Interfaz::asignarToken("titulo",$titulo_pag , $contenido);
    
    Interfaz::asignarToken("enviados", $valores['enviados'], $contenido);
    //pgs - 12/03/2012 - si el calculo de los pendientes es < 0 se muestra 0, es por que el total cambio, debido a destinatarios que se dieron de baaj o fueron eliminados 
    $valores['pendientes'] = $valores['total_enviar'] - $valores['enviados'];
    $valores['pendientes'] = ($valores['pendientes']<=0)?0:$valores['pendientes'];
    
    //pgs - 12/03/2012 - si ya termino de enviar y hay pendientes es por que el total cambio, debido a destinatarios que se dieron de baaj o fueron eliminados
    if($valores['termino_enviar']==1 && $valores['pendientes']>0){
      $valores['pendientes'] = 0;
    }//Fin de if($valores['termino_enviar']==1 && $valores['pendientes']>0)
    
    //pgs - 07/06/2012 - si no se ha terminado de procesar el envio (poner en cola) se pone como no disponible los pendientes
    if($datos['fin_procesado']==0){
      $valores['pendientes'] = 'No disponible';
      
      //pgs - 25/06/2012 - se pone un mensaje informativo indicando el por que de no disponible
      $msj = '<div class="nota_azul"><img src="Includes/Imagenes/informacion.png" border="0" width="15"> Algunos de los datos del informe a&uacute;n no est&aacute;n disponibles porque se est&aacute;n procesando los correos a enviar.</div><br>';
      Interfaz::asignarToken("msj_no_disponible", $msj, $contenido);
    }//Fin de if($datos['fin_procesado']==0)
    
    Interfaz::asignarToken("pendientes", $valores['pendientes'], $contenido);
    
    Interfaz::asignarToken("no_enviados", $valores['no_enviados'], $contenido);
    Interfaz::asignarToken("leidos", $valores['leidos'], $contenido);
    Interfaz::asignarToken("baja", $valores['baja'], $contenido);
    Interfaz::asignarToken("baja_otros", $valores['baja_otros'], $contenido);
    Interfaz::asignarToken("rebotados_blandos", $valores['rebote_blando'], $contenido);
    Interfaz::asignarToken("rebotados_duros", $valores['rebote_duro'], $contenido);
    
    Interfaz::asignarToken("enb_id", $datos['enb_id'], $contenido);
    Interfaz::asignarToken("bol_id", $datos['bol_id'], $contenido);
    
    return $contenido;
  }//Fin de obtenerInforme($datos)
  
  function obtenerFormularioDarBaja($datos){
    global $_PATH_WEB, $_PATH_ACCION;
		
    $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas."/dar_baja.html");
    
    Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);
    
    return $contenido;
  }//Fin de obtenerFormularioDarBaja($datos)
  
  //pgs - 29/08/2012 - funcion que se encarga de desplegar el formulario para enviar un correo de prueba
  function obtenerFormularioProbar($datos){
    global $_PATH_WEB, $_PATH_ACCION;
		
    $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas."/fm_probar.html");
    
    Interfaz::asignarToken("accion", 'fm_probar', $contenido);
    Interfaz::asignarToken("titulo", 'Enviar correo de prueba', $contenido);
    
    Interfaz::asignarToken("envp_correo", $datos['envp_correo'], $contenido);
    Interfaz::asignarToken("bol_id", $datos['bol_id'], $contenido);
    
    Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);   
    
    return $contenido;
  }//Fin de obtenerFormularioProbar($datos)
  
  
	/*******************************************	 FIN DE FUNCIONES GRAFICAS	*************************************************	*/
	
	
  /*	***********************************	OBTENER ENVIOS FALLIDOS	*************************************	*/
  /*
   *	Utilidad:
   *		Obtiene numero de celular y nombre de todos los destinatarios destinatario
   *	Paraetros de entrada:
   *
   *	Valores de retorno:
   *		$res -> informacion de destinatarios
   */
  function obtenerEnviosFallidos($datos){
  	global $_obj_database;
  	
  	$env_id=$datos['env_id'];
  	$sql = "select  b.envs_numero_destinatario, b.envs_intentos_fallidos 
from envio_sms b 
where b.envs_env_id = $env_id and b.envs_intentos_fallidos = 3";
  		
  	$res = $_obj_database->obtenerRegistrosCsv($sql);
  
  	return $res;
  }//Fin de obtenerEnviosFallidos()
  
	/*	*******************************	LISTAR ENVIOS ******************************	*/
	/*
	*	Utilidad:
	*		Lista los envios del sistema

	*	Par�metros de entrada:
	*		$datos -> array con los datos de offser y limit

	*	Valores de retorno:
	*		$listado -> array con el listado de los boletines
	*/
	function listarEnvios(){
		global $_obj_database, $_limit;                   
		
    
		$sql = "select a.env_id , a.env_nombre , a.env_mensaje_original, a.env_fecha_envio,
(select count(*) from envio_sms b where b.envs_env_id = a.env_id  and b.envs_estado=0 ) pendientes,
(select count(*) from envio_sms b where b.envs_env_id = a.env_id and b.envs_estado=1 ) enviados, 
(select count(*) from envio_sms b where b.envs_env_id = a.env_id and b.envs_intentos_fallidos=3 ) fallidos
from envio as a"; 
					
		$resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
		
		
		return $resultado;
	}//Fin de listarEnvios()
	

	
	/*	*******************************	OBTENER ENVIOS PENDIENTES ******************************	*/
	/*
	 *	Utilidad:
	 *		Lista los envios del sistema
	
	 *	Par�metros de entrada:
	 *		$datos -> array con los datos de offser y limit
	
	 *	Valores de retorno:
	 *		$listado -> array con el listado de los boletines
	 */
	function obtenerEnviosPendientes(){
		global $_obj_database, $_limit,$_LIMIT_ENVIOS;
	
	
		$sql = "select * from envio_sms where envs_estado=0 and envs_intentos_fallidos < 3 limit $_LIMIT_ENVIOS";
			
		$resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
	
	
		return $resultado;
	}//Fin de obtenerEnviosPendientes()
	/*	*******************************	OBTENER COUNT PENDIENTES ******************************	*/
	/*
	 *	Utilidad:
	 *		Lista los envios del sistema
	
	 *	Par�metros de entrada:
	 *		$datos -> array con los datos de offser y limit
	
	 *	Valores de retorno:
	 *		$listado -> array con el listado de los boletines
	 */
	function obtenerCountEnviosPendientes(){
		global $_obj_database, $_limit;
	
	
		$sql = "select count(*) pendientes from envio_sms where envs_estado=0 and envs_intentos_fallidos < 3";
			
		$resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
	
	
		return $resultado;
	}//Fin de obtenerCountEnviosPendientes()
	/*	*******************************	LISTAR ENVIOS BOLETIN ******************************	*/
	/*
	*	Utilidad:
	*		Lista los envios asociados a un boletin del sistema

	*	Par�metros de entrada:
	*		$datos -> array con los datos de offser y limit

	*	Valores de retorno:
	*		$listado -> array con el listado de los envios asociados a un boletin
	*/
	function listarEnviosBoletin($datos){
		global $_obj_database, $_limit;
		
		$datos = Herramientas::trimCamposFormulario($datos);
		
		$offset = (isset($datos['offset']) && is_numeric($datos['offset']) ? $datos['offset'] : 0);
		$condicion = "";      
		
		/*if($datos['campo_busqueda'] != ''){
      $condicion .= " and bol_nombre like('%".$datos['campo_busqueda']."%')";
    } */
		
		$orden = "ORDER BY enb_fecha_inicio DESC";
		if( $datos['ordena'] != "")
		{
			$orden = "ORDER BY ".$datos['ordena']." ".$datos['tipo_ordena'];
		}
		
		//pgs - 30/08/2012 - se trae el nombre del grupo a quien se le envia
		$sql = "SELECT enb_id,enb_fecha_inicio,enb_fecha_termino,enb_bol_id as bol_id, gru_nombre 
					FROM envio_boletin,boletin,envio_grupo,grupos 
					WHERE enb_bol_id=bol_id
            and eng_enb_id=enb_id
            and eng_gru_id=gru_id 
            and bol_usu_id='".$_SESSION['usu_id']."' 
            and bol_id='".$datos['bol_id']."' 
					  ".$condicion."
					".$orden." 
					LIMIT ".$offset.",".$_limit; 
					
		$resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
		
		if(is_array($resultado))
		{
			$sql = "SELECT count(enb_id) as num_total_registros 
					FROM envio_boletin,boletin 
					WHERE enb_bol_id=bol_id and bol_usu_id='".$_SESSION['usu_id']."' and bol_id='".$datos['bol_id']."'
					  ".$condicion;
            		  
			$resultado_conteo = $_obj_database->obtenerResultado($sql);
			$resultado['limite']=$_limit;
			$resultado['offset']=$offset;
			$resultado['num_total_registros']= $resultado_conteo[0];
			$resultado['usa_filtro'] = $datos['usa_filtro'];
		}//Fin de if(is_array($resultado))
		
		return $resultado;
	}//Fin de listarEnviosBoletin($datos)
	  
  /*	***********************************	INGRESAR ENVIOS	*************************************	*/
	/*
	*	Utilidad:
	*		Registra los datos de un envi
	*	Paraetros de entrada:
	*		$datos -> Datos del envio
	*	Valores de retorno:
	*		$resultado-> Resultado del registro del boletin
	*/
	function ingresarEnvios($datos){
      global $_obj_database, $_PATH_LOCAL_ADJUNTOS;
    
      //$datos = Herramientas::trimCamposFormulario($datos);
   

      $datos['tabla'] = "envio";
      $datos['id'] = 'env_id';
      $datos['env_id'] = $_obj_database->obtenerProximoId($datos);
  
       $_obj_database->ejecutarSql('BEGIN;'); 
      
	   $return_envs = ""; 
      
    	$campos  = array("env_id",
    	                 "env_nombre",
                         "env_mensaje_original"
                      );
    	//jcb 11-05-2017 se corrigen tildes al guardar
    	$datos ["env_mensaje_original"]=Herramientas::removerCaracteresEspeciales(utf8_decode($datos ["env_mensaje_original"]));

    	$sql = $_obj_database->generarSQLInsertar($datos,$campos);
    	$resultado = $_obj_database->ejecutarSql($sql);
		  	
    	
    	if($_obj_database->obtenerNumeroFilasAfectadas()>0 && $resultado)
		{
			
			$datos2['envs_env_id']=$datos['env_id'];
			$datos2['envs_mensaje']=$datos['env_mensaje_original'];
			$return_envs=$this->ingresarEnviosSms($datos2);
			
			
		}
		else
		{
	        $_obj_database->ejecutarSql('ROLLBACK;');
	        return $resultado;
	    }//Fin de if($_obj_database->obtenerNumeroFilasAfectadas()>0 && $resultado)
      
      $_obj_database->ejecutarSql('COMMIT;');
      //el boletin se registro satisfactoriamente
                                      
	  
      $retu = ($return_envs == "") ? "E-5" : $return_envs;
      return $retu;
	}//Fin de ingresarEnvios($datos)
	
	
	/*	***********************************	INGRESAR ENVIOS SMS	*************************************	*/
	/*
	 *	Utilidad:
	 *		Registra los datos de envios sms
	 *	Paraetros de entrada:
	 *		$datos -> Datos de envio
	 *	Valores de retorno:
	 *		$resultado -> Resultado del registro de los destinatarios
	 */
	function ingresarEnviosSms($datos){
		global $_obj_database,$_limit_des_usuario,$_limit_des_grupo,$_limit_lotes;
	
		
		$sql="select des_numero_celular,des_nombre from destinatario";
		$contenido=$_obj_database->obtenerRegistrosAsociativos($sql);
	
		if(is_array($contenido)){
			
			//cada $num_ejecutar registros se ejecuta el script, numero maximo de registros a procesar por lote
			//$num_ejecutar = 800;
	        $num_ejecutar=$_limit_lotes;
			//pgs - 07/06/2012 - flag temporal de cuantos registros lleva por el archivo
			$num_tmp_procesados_archivo = 0;
	
			//flag temporal de cuantos registros lleva por lote
			$num_tmp_procesados_lote = 0;
	
			$sql_envios_sms = "INSERT IGNORE INTO envio_sms(envs_numero_destinatario,envs_env_id,envs_mensaje) VALUES";
	
			//en la posicion 0 el numero destinatario y en la posicion 1 el nombre
			foreach($contenido as $destinatario){
				
				//pgs - 07/06/2102 - se va calculando el numero de destiantarios procesados por lote
				$num_tmp_procesados_archivo++;
				$num_tmp_procesados_lote++;
				
				$datos['envs_numero_destinatario']=$destinatario['des_numero_celular'];
				$contenido_mensaje=$datos['envs_mensaje'];
				Interfaz::asignarTokenMsgSMS("nombre", $destinatario['des_nombre'], $contenido_mensaje);
				
				$sql_envios_sms .= "('".$datos['envs_numero_destinatario']."','".$datos['envs_env_id']."','".$contenido_mensaje."'),";
				//}//Fin de if(preg_match($patron, $correo))
	
				//si ya se cumplieron los $num_ejecutar registros se ejecutan y se vuelve a crear otro lote de inserts comunes
				if($num_tmp_procesados_lote == $num_ejecutar){
					//se quita el ultimo caracter que es la coma sobrante
					$sql_envios_sms = substr ($sql_envios_sms, 0, strlen($sql_envios_sms) - 1);
	
					$resultado1 = $_obj_database->ejecutarSql($sql_envios_sms);
	
					//pgs - 07/06/2012 - se obtiene el numero de registros ingresados a�l ingresar en la tabla destinatario
					$num_registros = $_obj_database->obtenerNumeroFilasAfectadas();
	
					//pgs - 07/06/2012 - se van sumando el numero de destinatarios por lote al total
					$num_tmp_des_total += $num_registros;
	
					//se reinician las variables
					$num_tmp_procesados_lote=0;
					$sql_envios_sms = "INSERT IGNORE INTO envio_sms(envs_numero_destinatario,envs_env_id,envs_mensaje) VALUES";
				}//Fin de if($flag == $num_ejecutar)
			}//Fin de foreach($contenido as $destinatario)
	
			if($num_tmp_procesados_lote<$num_ejecutar && $num_tmp_procesados_lote>0){
				//se quita el ultimo caracter que es la coma sobrante
				$sql_envios_sms = substr ($sql_envios_sms, 0, strlen($sql_envios_sms) - 1);
				$resultado1 = $_obj_database->ejecutarSql($sql_envios_sms);
	
				//pgs - 07/06/2012 - se obtiene el numero de registros ingresados a�l ingresar en la tabla destinatario
				$num_registros = $_obj_database->obtenerNumeroFilasAfectadas();
	
				//pgs - 07/06/2012 - se van sumando el numero de destinatarios por lote al total
				$num_tmp_des_total += $num_registros;
				$num_tmp_des_total_grupo += $num_registros;
	
			}//Fin de if($num_ejecutar>$num_filas)
	
			//Los destinatarios se registraron satisfactoriamente
			if($resultado1){
				$resultado = 'E-0';
			}else{
				$resultado = 'E-5';
			}
	
			return $resultado;
		}else{
			//error al crear envio
			return 'E-5';
		}//Fin de if(is_array($contenido))
	}//Fin de ingresarEnviosSMS($datos)
	
	/*	***********************************	EDITAR BOLETIN	*************************************	*/
	/*
	*	Utilidad:
	*		Registra los datos de un boletin
	*	Paraetros de entrada:
	*		$datos -> Datos del boletin
	*	Valores de retorno:
	*		$resultado-> Resultado del registro del boletin
	*/
	function editarBoletin($datos){
    global $_obj_database, $_PATH_LOCAL_ADJUNTOS;
    
    if($datos['bol_nombre_anterior'] != $datos['bol_nombre']){
      $existe = $this->existeBoletin($datos);
    }else{
      $existe = false;
    }//Fin de if($datos['bol_nombre_anterior'] != $datos['bol_nombre'])
    
    if(!$existe){
      $_obj_database->ejecutarSql('BEGIN;');
      
      $result = "";
      $adj_log = "";
      
      $datos['tabla'] = "boletin";
      $datos['condicion'] = 'bol_id='.$datos['bol_id']; 
    	
	    //Creo el directorio si no existe
	    $directorio_adj = $_PATH_LOCAL_ADJUNTOS."/".$datos['bol_id'];
	    	
	    if(!file_exists($directorio_adj))
	    	mkdir($directorio_adj);
         
    	$campos  = array("bol_nombre",
                      "bol_asunto",
                      "bol_nombre_remitente",
                      "bol_correo_remitente",
                      "bol_correo_rebotado",
                      "bol_html",
                      "bol_estado"
                      );
    					
    	$sql = $_obj_database->generarSQLActualizar($datos,$campos);
    	$resultado = $_obj_database->ejecutarSql($sql);    	
    	
    	if($resultado)
		{
			$obj_archivos = new Archivos($_FILES);
			$arch_adjuntos = $this->obtenerArchivosAdjuntosBoletin($datos);
			$cantidad_archivos = sizeof($arch_adjuntos);
			
			//Subo los archivos
		    for($i=1; $i<=4; $i++)
			{
				//Si hay archivos subidos previamente y quieren subir mas, solo permite subir hasta 4
				if($cantidad_archivos < 4)
				{
					$nom_adjunto = $_FILES["bol_adjunto_".$i]["name"];
					$nom_adjunto = $obj_archivos->modificarEspaciosEnBlanco($nom_adjunto, "_");   
					$nom_adjunto = Herramientas::removerCaracteresEspeciales($nom_adjunto);   
					$tam_adjunto = $_FILES["bol_adjunto_".$i]["size"];
					
					if($nom_adjunto != "" && $tam_adjunto > 0)
					{       
						if($tam_adjunto <= $datos["MAX_FILE_SIZE"])
						{
							$res_arch_adj = $obj_archivos->subirArchivo("bol_adjunto_".$i, $directorio_adj."/", $nom_adjunto);
							
							//Si subio el archivo
							if($nom_adjunto == $res_arch_adj)	
							{
								$datos_arch = array();     
							    $datos_arch['tabla'] = "adjuntos_boletin";             
							    $datos_arch['adj_bol_id'] = $datos['bol_id'];
							    $datos_arch['adj_nombre_archivo'] = $nom_adjunto;
							    $datos_arch['adj_fecha_subida'] = date('Y-m-d H:i:s');
							  	$campos_arch = array("adj_bol_id",
								  	                 "adj_nombre_archivo",
								                     "adj_fecha_subida"
								                    );
							  
							  	$sql_arch = $_obj_database->generarSQLInsertar($datos_arch, $campos_arch);
							  	$resultado_adjunto = $_obj_database->ejecutarSql($sql_arch);
							  	
							  	if($_obj_database->obtenerNumeroFilasAfectadas()>0 && $resultado_adjunto)
							  	{
							  		$cantidad_archivos++;
								}
								else
							  	{
			        				$_obj_database->ejecutarSql('ROLLBACK;');
									return "B-5";
								}
						  	}
						  	elseif($res_arch_adj == -5)//El archivo ya existe
						  	{             
								$adj_log .= "- El archivo '".$_FILES["bol_adjunto_".$i]["name"]."' no se ha inclu&iacute;do ya que el archivo ya fue incluido previamente.<br />";
								$result = "B-9";
							}
						  	else
						  	{             
		        				$_obj_database->ejecutarSql('ROLLBACK;');
								return "B-5";  
							}
					    }
					    else
						{
							$adj_log .= "- El archivo '".$_FILES["bol_adjunto_".$i]["name"]."' no se ha inclu&iacute;do ya que supera el tama&ntilde;o permitido.<br />"; 
							$result = "B-9";
						}
				    }
				}
				else
				{
					if(!is_null($_FILES["bol_adjunto_".$i]) && $_FILES["bol_adjunto_".$i]["name"] != "")
					{
						$adj_log .= "- El archivo '".$_FILES["bol_adjunto_".$i]["name"]."' no se ha inclu&iacute;do ya que se ha llegado al l&iacute;mite de archivos permitidos.<br />"; 
						$result = "B-9";
					}
				}
			}
    	}
		else
		{
        	$_obj_database->ejecutarSql('ROLLBACK;');
        	return $resultado;
      	}//Fin de if($_obj_database->obtenerNumeroFilasAfectadas()>0 && $resultado)
      
      $_obj_database->ejecutarSql('COMMIT;');
                   
	  $_SESSION["adj_log"] = ($adj_log != "") ? "<br />".$adj_log : "";
      $result = ($result != "") ? $result : 1;
      return $result;
    }
	else
	{
      //El boletin ya existe
      return 'B-1'; 
    }//Fin de if(!$existe)
	}//Fin de ingresarGrupo($datos)
	
	function registrarEnvio($datos){
    global $_obj_database;
    
    $datos['tabla'] = "envio_boletin";
    $datos['id'] = 'enb_id';
  	$datos['enb_id'] = $_obj_database->obtenerProximoId($datos);
    
    $_obj_database->iniciarTransaccion();
    
    $datos['enb_fecha_inicio'] = $datos['bol_anho'].'-'.$datos['bol_mes'].'-'.$datos['bol_dia'].' '.$datos['bol_hora'];
    $datos['enb_bol_id'] = $datos['bol_id'];
    $datos['enb_fecha_creacion'] = date('Y-m-d H:i:s');    
  	$campos  = array("enb_id",
                    "enb_bol_id",
                    "enb_fecha_inicio",
                    "enb_fecha_creacion"
                    );
    
  	$sql = $_obj_database->generarSQLInsertar($datos,$campos);
  	
  	$resultado = $_obj_database->ejecutarSql($sql);    	
  	
  	if($_obj_database->obtenerNumeroFilasAfectadas()>0 && $resultado){
  	}else{
      $_obj_database->cancelarTransaccion();
      return $resultado;
    }//Fin de if($_obj_database->obtenerNumeroFilasAfectadas()>0 && $resultado)
    
    //pgs - 07/06/2012 - se ingresa el grupo seleccionado para el env�o
    $datos['tabla'] = "envio_grupo";
    $datos['eng_gru_id'] = $datos['bol_gru_id'];
    $datos['eng_enb_id'] = $datos['enb_id'];    
  	$campos  = array("eng_enb_id",
                    "eng_gru_id"
                    );
    
  	$sql = $_obj_database->generarSQLInsertar($datos,$campos);
  	
  	$resultado = $_obj_database->ejecutarSql($sql);    	
  	
  	if($_obj_database->obtenerNumeroFilasAfectadas()>0 && $resultado){
  	}else{
      $_obj_database->cancelarTransaccion();
      return $resultado;
    }//Fin de if($_obj_database->obtenerNumeroFilasAfectadas()>0 && $resultado)
    
    /*
    pgs - 07/06/2012 - se comenta el codigo de ingresar varios grupos
    ya que ahora solo puede seleccionar un grupo
    foreach($datos['bol_gru_id'] as $key_grupo){
      $datos['tabla'] = "envio_grupo";
      $datos['eng_gru_id'] = $key_grupo;
      $datos['eng_enb_id'] = $datos['enb_id'];    
    	$campos  = array("eng_enb_id",
                      "eng_gru_id"
                      );
      
    	$sql = $_obj_database->generarSQLInsertar($datos,$campos);
    	
    	$resultado = $_obj_database->ejecutarSql($sql);    	
    	
    	if($_obj_database->obtenerNumeroFilasAfectadas()>0 && $resultado){
    	}else{
        $_obj_database->cancelarTransaccion();
        return $resultado;
      }//Fin de if($_obj_database->obtenerNumeroFilasAfectadas()>0 && $resultado)
    }//Fin de foreach($grupos as $key_grupo => $grupo) */
    
    $datos['tabla'] = "informe_envio";
    $campos  = array("infv_enb_id");
    $datos['infv_enb_id'] = $datos['enb_id'];
    
    $sql = $_obj_database->generarSQLInsertar($datos,$campos);    	
  	$resultado = $_obj_database->ejecutarSql($sql);    	
  	
  	if($_obj_database->obtenerNumeroFilasAfectadas()>0 && $resultado){
  	}else{
      $_obj_database->cancelarTransaccion();
      return $resultado;
    }//Fin de if($_obj_database->obtenerNumeroFilasAfectadas()>0 && $resultado)
    
    $_obj_database->terminarTransaccion();
     $resultado = 'E-0';
    
    return $resultado;
  }//Fin de registrarEnvio($datos)
	
	/*	***********************************	ELIMINAR BOLETIN	*************************************	*/
	/*
	*	Utilidad:
	*		eliminado logico de un boletin
	*	Paraetros de entrada:
	*		$datos -> Datos del boletin
	*	Valores de retorno:
	*		$resultado-> Resultado de eliminar el boletin
	*/
	function eliminarBoletin($datos){
    global $_obj_database;
    
    $datos['tabla'] = "boletin";
    $datos['condicion'] = 'bol_id='.$datos['bol_id'];
    $datos['bol_activo'] = 0;
    $datos['bol_estado'] = 0;
  	$campos  = array("bol_activo",'bol_estado');
  					
  	$sql = $_obj_database->generarSQLActualizar($datos,$campos);
  	
  	$resultado = $_obj_database->ejecutarSql($sql);    	
  	
  	if($resultado){
  	 return 1;
  	}else{
      return $resultado;
    }//Fin de if($resultado)
	}//Fin de eliminarBoletin($datos)
	
	/*	***********************************	CAMBIAR ESTADO BOLETIN	*************************************	*/
	/*
	*	Utilidad:
	*		cambia el estado de un boletin
	*	Paraetros de entrada:
	*		$datos -> Datos del boletin
	*		$datos['estado'] => el nuevo estado del boletin	
	*	Valores de retorno:
	*		$resultado-> Resultado de cambiar el estado del boletin
	*/
	function cambiarEstadoBoletin($datos){
    global $_obj_database;
    
    $datos['tabla'] = "boletin";
    $datos['condicion'] = 'bol_id='.$datos['bol_id'];
    $datos['bol_estado'] = $datos['estado'];
  	$campos  = array("bol_estado");
  					
  	$sql = $_obj_database->generarSQLActualizar($datos,$campos);
  	
  	$resultado = $_obj_database->ejecutarSql($sql);    	
  	
  	if($resultado){
  	 return 1;
  	}else{
      return $resultado;
    }//Fin de if($resultado)
	}//Fin de cambiarEstadoBoletin($datos)
	
	/*	***********************************	DAR DE BAJA	*************************************	*/
	/*
	*	Utilidad:
	*		se da de baja un destinatario del cliente
	*	Paraetros de entrada:
	*		$datos -> Datos
	*		$datos['env_id'] => id del envio que se le hizo al destinatario
	*	Valores de retorno:
	*		$resultado-> Resultado de cambiar el estado del boletin
	*/
	function darBaja($datos){
    global $_obj_database;
    
    //consultamos los datos del destinatario y del cliente
    //pgs - 10/03/2012 - se trae el id del envio del boletin (enb_id)
    $sql = "SELECT bol_usu_id,des_id,enb_id,env_baja 
            FROM envios,destinatarios,envio_boletin,boletin
            WHERE env_des_id=des_id AND env_enb_id=enb_id AND enb_bol_id=bol_id AND env_id='".$datos['env_id']."'";
    
    $info_baja = $_obj_database->obtenerRegistrosAsociativos($sql);
    
    if(sizeof($info_baja)<=0){
      return 'DB-1';
    }//Fin de if(sizeof($info_baja)<=0)
    
    $usu_id = $info_baja[0]['bol_usu_id'];
    $correo = $info_baja[0]['des_email'];
    $des_id = $info_baja[0]['des_id'];
    
    //consultamos si el destinatario ya se dio de baja
    $sql = "SELECT des_id 
            FROM destinatarios
            WHERE des_id='".$des_id."' and des_baja='1'";
    
    $existe = $_obj_database->obtenerRegistrosAsociativos($sql);
    
    if(sizeof($existe)>0){
      return 'DB-2';
    }//Fin de if(sizeof($existe)>0)
    
    $resultado = $_obj_database->ejecutarSql('BEGIN;');
    
    $datos['tabla'] = "envios";
    $datos['condicion'] = 'env_id='.$datos['env_id'];
    $datos['env_baja'] = 1;
  	$campos  = array("env_baja");
  					
  	$sql = $_obj_database->generarSQLActualizar($datos,$campos);
  	
  	$resultado = $_obj_database->ejecutarSql($sql);    	
  	
  	if(!$resultado){
      $_obj_database->ejecutarSql('ROLLBACK;');
      return $resultado;
    }//Fin de if(!$resultado)
  	
  	$datos['tabla'] = "destinatarios"; 
    $datos['condicion'] = "des_id='".$des_id."'";
    $datos['des_baja'] = 1;      
  	$campos  = array("des_baja");
    
  	$sql = $_obj_database->generarSQLActualizar($datos,$campos);
  	
  	$resultado = $_obj_database->ejecutarSql($sql);    	
  	
  	if( $resultado){
    }else{
      $_obj_database->ejecutarSql('ROLLBACK;');
      return $resultado;
    }//Fin de if($resultado)
  	
  	//se eliminan los envios
    $datos["tabla"] = "envios"; 
		$datos["condicion"] = "env_des_id='".$des_id."'";
    
    $datos['env_eliminado'] = 1;
		$campos = array("env_eliminado");
	   
		$sql = $_obj_database->generarSQLActualizar($datos, $campos);
		$resultado = $_obj_database->ejecutarSql($sql);
  	
  	if($resultado!=true){
      $_obj_database->ejecutarSql('ROLLBACK;');
      return $resultado;
    }//Fin de if($resultado)
  	
  	if($info_baja[0]['env_baja']==0){  	
    	//pgs - 10/03/2012 - se actualiza el informe del envio
    	//el sistema base no permite actualizacion de la siguiente manera: infv_baja=infv_baja+1
    	$sql = "UPDATE informe_envio
              SET infv_baja=infv_baja+1
              WHERE infv_enb_id='".$info_baja[0]['enb_id']."'";
              
  		$resultado = $_obj_database->ejecutarSql($sql);
    	
    	if($resultado!=true){
        $_obj_database->ejecutarSql('ROLLBACK;');
        return $resultado;
      }//Fin de if($resultado)
    }//Fin de if($info_baja[0]['env_baja'])
  	
  	$_obj_database->ejecutarSql('COMMIT;');
    return 'DB-0';
	}//Fin de darBaja($datos)
	
	/*	***********************************	OBTENER GRUPOS	*************************************	*/
	/*
	*	Utilidad:
	*		obtiene los grupos existentes en el sistema
	*	Paraetros de entrada:
	*		$datos -> Datos del boletin
	*	Valores de retorno:
	*		$resultado = array con los datos de los grupos
	*/
  function obtenerGrupos($datos){
		global $_obj_database;	
		
		$estado = false;
		
		/*$sql = "select gru_id,gru_nombre  
					from grupos,destinatario_grupo,destinatarios
					where desg_gru_id=gru_id and desg_des_correo=des_email and desg_estado=1 and gru_activo = 1 and des_activo = 1 and des_usu_id='".$_SESSION['usu_id']."' and gru_usu_id='".$_SESSION['usu_id']."'
          group by desg_gru_id";
						
		$resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
		
		return $resultado;*/
		
		$sql = "select gru_id,gru_nombre  
					from grupos
					where gru_activo = 1 and gru_usu_id='".$_SESSION['usu_id']."'
          group by gru_nombre";
						
		$grupos = $_obj_database->obtenerRegistrosAsociativosPorClave($sql,'gru_id');
		
		
    //CHCM.  DIC 06 2011. COMENTO EL SIGUI0ENTE FRAGMENTO PARA QUE NO CONSULTE LOS GRUPOS QUE TIENEN DESTINATARIOS
    /*foreach($grupos as $grupo){
		  $sql = "select des_id from destinatario_grupo,destinatarios where desg_des_correo=des_email and desg_estado=1 and des_activo = 1 and des_baja=0 and desg_gru_id='".$grupo['gru_id']."' and des_usu_id='".$_SESSION['usu_id']."' limit 1";
		  $des = $_obj_database->obtenerRegistrosAsociativos($sql);
		  
		  if(sizeof($des)<=0){
        unset($grupos[$grupo['gru_id']]);
      }
    }//Fin de foreach($grupos as $grupo)*/
		
		return $grupos;
		
	}//Fin de obtenerGrupos()
	
	/*	***********************************	OBTENER DESTINATARIOS GRUPOS	*************************************	*/
	/*
	* En el momento no se esta utilizando
	*	Utilidad:
	*		obtiene los destinatariosasociados a un grupo
	*	Paraetros de entrada:
	*		$datos -> Datos del grupo
	*	Valores de retorno:
	*		$resultado = array con los datos de los destinatarios
	*/
  function obtenerDestinatariosGrupo($datos){
		global $_obj_database;	
		
		$estado = false;
		
		$sql = "select *  
					from destinatarios,destinatario_grupo
					where desg_des_correo=des_email and des_activo = 1 and desg_estado=1 and desg_gru_id='".$datos['gru_id']."'";
						
		$resultado = $_obj_database->obtenerRegistrosAsociativosPorClave($sql,'des_id');
		
		return $resultado;
	}//Fin de obtenerDestinatariosGrupo()
	
	/*	***********************************	COPIAR BOLETIN	*************************************	*/
	/*
	*	Utilidad:
	*		copia la informacion de un boletin
	*	Paraetros de entrada:
	*		$datos[bol_id] -> id del boletin
	*	Valores de retorno:
	*		$res -> informacion del boletin
	*/
  function copiarBoletin($datos){
		global $_obj_database;	
		
		$datos = $this->obtenerBoletin($datos);
		
		$datos['tabla'] = 'boletin';
		$datos['bol_fecha'] = date('Y-m-d');
    $datos['bol_usu_id'] = $_SESSION['usu_id'];
    $datos['bol_nombre'] = "Copia - ".$datos['bol_nombre'];
       
  	$campos  = array("bol_usu_id",
                    "bol_nombre",
                    "bol_asunto",
                    "bol_nombre_remitente",
                    "bol_correo_remitente",
                    "bol_correo_rebotado",
                    "bol_html",
                    "bol_fecha"
                    );
  	
    //pgs - 15/05/2012 - se reemplazan las comillas simples por \' para que no hayan inconvenientes al insertar				
    $datos['bol_html'] = str_replace("'","\'",$datos['bol_html']);				
  
  	$sql = $_obj_database->generarSQLInsertar($datos,$campos);
  	
  	$resultado = $_obj_database->ejecutarSql($sql);    	
  	
  	if($_obj_database->obtenerNumeroFilasAfectadas()>0 && $resultado){
  	  return 'B-3';
  	}else{
      return $resultado;
    }
	}//Fin de obtenerBoletin($datos)
	
	/*	***********************************	OBTENER BOLETIN	*************************************	*/
	/*
	*	Utilidad:
	*		Obtiene la informacion de un boletin
	*	Paraetros de entrada:
	*		$datos[bol_id] -> id del boletin
	*	Valores de retorno:
	*		$res -> informacion del boletin
	*/
  function obtenerBoletin($datos){
		global $_obj_database;	
		
		$sql = "select * 
				from boletin 
				where bol_id = '".$datos['bol_id']."' 
					and bol_activo = 1";	
					
		$res = $_obj_database->obtenerRegistrosAsociativos($sql);
		
		//pgs - 27-10-2011 - se obtienen los archivos adjuntos
		$adjuntos = $this->obtenerArchivosAdjuntosBoletin($datos);
		//fin modificacion pgs - 27/20/2011
		
		$res[0]['archivos_adjuntos'] = $adjuntos;
		
		return $res[0];
	}//Fin de obtenerBoletin($datos)
	                                     
	/*	***********************************	OBTENER CLIENTES	*************************************	*/
	/*
	*	Utilidad:
	*		Obtiene los clientes activos con correos sin enviar
	*	Paraetros de entrada:
	*		$datos
	*	Valores de retorno:
	*		$res -> informacion de los clientes
	*/
  function obtenerClientes($datos){
		global $_obj_database;	
		
    $fecha_actual = date('Y-m-d H:i:s');
    $fecha_actual_sin_his = date('Y-m-d');
		
		$usu_id='';
		if(isset($datos['usuario_id'])){
      $usu_id = " and usu_id='".$datos['usuario_id']."'";
    }//Fin de if(isset($datos['usuario_id']))
		
		//pgs - 16/01/2012 - se obtiene el nombre del cliente
		//pgs - 07/06/2012 - se obtiene el limite de envios usu_limite_envio
		//pgs - 28/07/2012 - se a�ade validacion para que traiga los datos del cliente que no se le ha vencido su cuenta
		$sql = "select enb_id,usu_id,usu_correo,usu_nombre,usu_correo_rebotado,usu_smtp_host,
              usu_smtp_cuenta,usu_smtp_contrasenha,usu_smtp_autenticacion,usu_smtp_puerto,
              usu_smtp_ssl,usu_limite_envio
            from usuario,boletin, envio_boletin
            where usu_id=bol_usu_id 
              and bol_id=enb_bol_id  
              and enb_termino_enviar=0 
              and enb_estado=1 
              and enb_eliminado=0 
              and enb_inicio_procesado=1
              AND enb_informe_creado=0 
              and usu_estado=1 
              and bol_estado=1 
              and bol_activo=1 
              and enb_fecha_inicio<='".$fecha_actual."'
              and usu_fecha_vencimiento>='".$fecha_actual_sin_his."'
               ".$usu_id." group by usu_id";	
					
          //echo $sql;     
            
                                      
		$res = $_obj_database->obtenerRegistrosAsociativosPorClave($sql,'usu_id');
		
		return $res;
	}//Fin de obtenerClientes($datos)
	
	/*	***********************************	OBTENER ENVIOS	*************************************	*/
	/*
	*	Utilidad:
	*		Obtiene la informacion de un envio
	*	Paraetros de entrada:
	*		$datos
	*	Valores de retorno:
	*		$res -> informacion del envio
	*/
  function obtenerEnvios($datos){
		global $_obj_database;	
		
		/*--------------------------------- DESTINATARIOS PENDIENTES POR ENVIAR ----------------------------------*/
		
		$sql = "SELECT env_id,env_enb_id as enb_id,des_id,des_nombre,des_email 
  					FROM destinatarios,envios
  					WHERE des_id=env_des_id and des_usu_id='".$datos['usu_id']."' 
              and env_enb_id='".$datos['enb_id']."' and des_baja=0 
              AND des_activo=1 and env_enviado=0 and env_eliminado=0 
              and env_procesado=0
            limit ".$datos['limite'];
            
     
             
            
		
		$des_pendientes = $_obj_database->obtenerRegistrosAsociativosPorClave($sql,'des_id');
		
		$sql = "UPDATE envios SET env_procesado=1 WHERE env_des_id IN(".implode(',',array_keys($des_pendientes)).") AND env_enb_id='".$datos['enb_id']."'";
		$_obj_database->ejecutarSql($sql);            
		
		/*--------------------------------- INFORMACION DEL BOLETIN ----------------------------------*/
		$sql = "SELECT bol_id,enb_id,bol_nombre_remitente,bol_correo_remitente,bol_correo_rebotado,bol_asunto,bol_html 
  					FROM envio_boletin,boletin
  					WHERE enb_bol_id=bol_id and enb_id='".$datos['enb_id']."' and bol_usu_id='".$datos['usu_id']."' and enb_estado=1 and enb_eliminado=0 and bol_estado=1  
            limit 1";
       
            
    $boletin = $_obj_database->obtenerRegistrosAsociativos($sql);
		
    
    
		$res['envios'] = $des_pendientes;
		$res['boletin'] = $boletin;
		return $res;
	}//Fin de obtenerEnvios($datos)
	
	/*	***********************************	EXISTE BOLETIN	*************************************	*/
	/*
	*	Utilidad:
	*		Revisa si el nombre de un boletin ya existe
	*	Paraetros de entrada:
	*		$datos -> Datos del boletin
	*	Valores de retorno:
	*		$estado -> true=>existe false=>no existe
	*/
  function existeBoletin($datos){
		global $_obj_database;	
		
		$estado = false;
		
		$sql = "select bol_id 
					from boletin
					where bol_nombre = '".$datos['bol_nombre']."'
          and bol_activo = 1 and bol_usu_id='".$_SESSION['usu_id']."'";
						
		$res = $_obj_database->obtenerRegistrosAsociativos($sql);
		
		if( sizeof($res) > 0 ){
			$estado = true;
		}//Fin de if( sizeof($res) > 0 )
		
		return $estado;
	}//Fin de existeBoletin()
	
	/*	***********************************	EXISTE DESTINATARIO BOLETIN	*************************************	*/
	/*
	*	Utilidad:
	*		Revisa si existe un envio para un boletin al mismo destinatario
	*	Paraetros de entrada:
	*		$datos -> Datos del boletin y el desinatario
	*	Valores de retorno:
	*		$estado -> true=>existe false=>no existe
	*/
  function existeDestinatarioBoletin($datos){
		global $_obj_database;	
		
		$estado = false;
		
		$sql = "select env_id 
					from envios
					where env_des_id = '".$datos['env_des_id']."'
          and env_enb_id = '".$datos['env_enb_id']."'
          and env_enviado = 0";
						
		$res = $_obj_database->obtenerRegistrosAsociativos($sql);
		
		if( sizeof($res) > 0 ){
			$estado = true;
		}//Fin de if( sizeof($res) > 0 )
		
		return $estado;
	}//Fin de existeDestinatarioBoletin()
	
	/*	***********************************	DESTINATARIO SE DIO DE BAJA	*************************************	*/
	/*
	*	Utilidad:
	*		Revisa si el destinatario se dio de baja para el cliente
	*	Paraetros de entrada:
	*		$datos -> Datos
	*		$dato['usu_id'] = id del cliente
	*		$dato['correo'] = correo del destinatario     
	*	Valores de retorno:
	*		$estado -> true=>existe false=>no existe
	*/
  function destinatarioDioBaja($datos){
		global $_obj_database;	
		
		$estado = false;
		
		$sql = "select baja_id 
					from baja
					where baja_usu_id = '".$datos['usu_id']."'
          and baja_correo = '".$datos['correo']."'";
						
		$res = $_obj_database->obtenerRegistrosAsociativos($sql);
		
		if( sizeof($res) > 0 ){
			$estado = true;
		}//Fin de if( sizeof($res) > 0 )
		
		return $estado;
	}//Fin de destinatarioDioBaja()
	
	/*	***********************************	REGISTRAR LECTURA DEL CORREOS	*************************************	*/
	/*
	*	Utilidad:
	*		Se encarga de registrar la lectura de un correo
	*	Paraetros de entrada:
	*		$datos -> datos del destinatario
	*	Valores de retorno:
	*		$resultado - resultado de registrar la lectura del correo
	*/
	function registrarLecturaCorreo($datos){
	  global $_obj_database;	
		
		if(!empty($datos['env_id'])){
  		//pgs - 10/03/2012 - se trae la informacion del envio del boletin
  		$sql = "SELECT env_enb_id,env_leido
            FROM envios
            WHERE env_id='".$datos['env_id']."'";
    
      $info_leido = $_obj_database->obtenerRegistrosAsociativos($sql);
      
      if($info_leido[0]['env_leido']==0){      
        $_obj_database->iniciarTransaccion();
        
        $datos['tabla'] = "envios";
    		$datos['condicion'] = "env_id='".$datos['env_id']."'";
        $datos['env_leido'] = 1;
        
        //pgs - 25/07/2012 - se actualiza la fecha en que se envio
        $datos['env_fecha_leido'] = date('Y-m-d H:i:s');
            
      	$campos  = array("env_leido","env_fecha_leido");
        
      	$sql = $_obj_database->generarSQLActualizar($datos,$campos);
      	
      	$resultado = $_obj_database->ejecutarSql($sql);
      	
      	if($resultado!=true){
          $_obj_database->cancelarTransaccion();
          return $resultado;
        }//Fin de if($resultado)
      	
      	//pgs - 10/03/2012 - se actualiza el informe del envio
      	//el sistema base no permite actualizacion de la siguiente manera: infv_leidos=infv_leidos+1
      	$sql = "UPDATE informe_envio
                SET infv_leidos=infv_leidos+1
                WHERE infv_enb_id='".$info_leido[0]['env_enb_id']."'";
                
    		$resultado = $_obj_database->ejecutarSql($sql);
      	
      	if($resultado!=true){
          $_obj_database->cancelarTransaccion();
          return $resultado;
        }//Fin de if($resultado)
      	
      	$resultado = $_obj_database->terminarTransaccion();    
      }//Fin de if($info_leido[0]['env_leido']==0)
    }//Fin de if(!empty($datos['env_id']))    	
  			
		return $resultado;
  }//Fin de registrarLecturaCorreo($datos)
	
	/*	*******************************	INFORME BOLETIN ******************************	*/
	/*
	*	Utilidad:
	*		obtiene los datos necesarios para mostrar el informe de un envio

	*	Par�metros de entrada:
	*		$datos['enb_id'] -> id del envio_boletin

	*	Valores de retorno:
	*		$res_info_1 -> array con los datos para generar el informe
	*/
	function informeBoletin($datos){
	  global $_obj_database;	
		
		//pgs - 24/09/2011 - si ya se le creo el informe se traen los datos de la tabla informe creado sino se calculan los datos de la tabla envios
    //if($datos['informe_creado']){
        //pgs - 04/01/2012 - se a�ade el campo de pendientes
        //pgs - 12/03/2012 - se quitan los pendientes y se traen el total, que a partir de este se calculara los pendientes
        $sql = "SELECT infv_enviados as enviados,infv_total_enviar as total_enviar,
                  infv_leidos as leidos, infv_baja as baja,infv_noenviados as no_enviados,
                  infv_baja_otros as baja_otros,infv_rebotado_blando as rebote_blando,
                  infv_rebotado_duro as rebote_duro, enb_termino_enviar as termino_enviar
                FROM informe_envio,envio_boletin
                WHERE infv_enb_id=enb_id AND 
                infv_enb_id = '".$datos['enb_id']."' ";
                
                
        $informe = $_obj_database->obtenerRegistrosAsociativos($sql);
        
          
        return $informe[0];
    //}//Fin de if($datos['informe_creado'])

    //pgs - 12/03/2012 - ya no se calculan los datos para el informe, sino que se muestra el informe
    /*$sql = "SELECT count(env_enviado) as enviados
            FROM envios
            WHERE env_enb_id = '".$datos['enb_id']."' AND env_enviado=1 AND env_error_envio=0";
    $enviados = $_obj_database->obtenerRegistrosAsociativos($sql);

    $res['enviados'] = $enviados[0]['enviados'];

    $sql = "SELECT count(env_leido) as leidos
            FROM envios
            WHERE env_enb_id = '".$datos['enb_id']."' AND env_leido=1 AND env_error_envio=0";
    $leidos = $_obj_database->obtenerRegistrosAsociativos($sql);
    $res['leidos'] = $leidos[0]['leidos'];

    $sql = "SELECT count(env_baja) as baja
            FROM envios
            WHERE env_enb_id = '".$datos['enb_id']."' AND env_baja=1 AND env_error_envio=0";
    $baja = $_obj_database->obtenerRegistrosAsociativos($sql);
    $res['baja'] = $baja[0]['baja'];

    $sql = "SELECT count(env_error_envio) as no_enviados
            FROM envios
            WHERE env_enb_id = '".$datos['enb_id']."' and env_error_envio=1";

    $noenviados = $_obj_database->obtenerRegistrosAsociativos($sql);
    $res['no_enviados'] = $noenviados[0]['no_enviados'];

    $sql = "SELECT count(env_id) as pendientes
            FROM envios,destinatarios
            WHERE env_des_id=des_id AND des_baja=0 AND des_activo=1 AND env_enviado=0 AND env_enb_id = '".$datos['enb_id']."' and env_eliminado =0";

    $pendientes = $_obj_database->obtenerRegistrosAsociativos($sql);
    $res['pendientes'] = $pendientes[0]['pendientes'];

    //pgs - 26/06/2011 - se calculan los destinatarios que se dieron de baja en otros boletines
    //se traen los grupos que pertenecen al envio
    $sql = "SELECT eng_gru_id FROM envio_grupo WHERE eng_enb_id='".$datos['enb_id']."'";
    $grupos = $_obj_database->obtenerRegistrosAsociativosPorClave($sql,'eng_gru_id');


    $sql = "SELECT COUNT(des_baja) as baja_otros
            FROM destinatarios,destinatario_grupo
            WHERE des_email=desg_des_correo AND desg_estado=1 AND des_activo=1 AND des_usu_id='".$_SESSION['usu_id']."' AND desg_gru_id IN(".implode(',',array_keys($grupos)).") AND des_baja=1";
    
    $baja_otros = $_obj_database->obtenerRegistrosAsociativos($sql);
    $res['baja_otros'] = $baja_otros[0]['baja_otros'] - $baja[0]['baja'];
    if($res['baja_otros']<0){
        $res['baja_otros']=0;
    }//Fin de if($res['baja_otros']<0) */

    return $res;
  }//Fin de informeBoletin($datos)
  
  /*	*******************************	ESTADO INFORME BOLETIN ******************************	*/
	/*
	*	Utilidad:
	*		consulta si al boletin se le puede consultar el informe si no muestra un mensaje con los registros que lleva procesados
	*		hasta el momento de cuantos en total
	*			
	*	Par�metros de entrada:
	*		$datos['enb_id'] -> id del envio_boletin
	*			
	*	Valores de retorno:
	*		$res['informe_creado'] -> true=>ya se cr,false=>no se puede consultar
	*	  
	*/
	function estadoInformeBoletin($datos){
	  global $_obj_database,$_correo_blando,$_correo_duro;	
		
		//pgs - 24/09/2011 - se a�ade el campo del informe creado
		$sql = "SELECT enb_fin_procesado,enb_informe_creado
            FROM envio_boletin
            WHERE enb_id = '".$datos['enb_id']."'";
						
		$envio = $_obj_database->obtenerRegistrosAsociativos($sql);
		
    
		//pgs - 07/06/2012 - se envia la info del envio del boletin 
    $res['informe_creado'] = $envio[0]['enb_informe_creado'];
    $res['fin_procesado'] = $envio[0]['enb_fin_procesado'];
		
		/*
    pgs - 07/06/2012 - se comenta el siguiente codigo ya que siempre se mostrara el informe,
    esta funcion solo se utilizara para saber si ya termino de procesar los destinatarios
		if($envio[0]['enb_fin_procesado']==1 || $envio[0]['enb_fin_procesado']=='1'){
		  $res['resultado'] = true;
		  $res['informe_creado'] = $envio[0]['enb_informe_creado'];
    }else{
      $res['resultado'] = false;
      
      $sql = "SELECT count(env_id) as registrados
            FROM envios
            WHERE env_enb_id = '".$datos['enb_id']."' and env_eliminado =0";
  						
  		$num_pendientes = $_obj_database->obtenerRegistrosAsociativos($sql);
  		$res['registrados'] = $num_pendientes[0]['registrados'];
  		
  		//obtengo los grupos a quienes se les agendo el envio
  		$sql = "SELECT eng_gru_id FROM envio_grupo WHERE eng_enb_id='".$datos['enb_id']."'";
      $grupos = $_obj_database->obtenerRegistrosAsociativosPorClave($sql,'eng_gru_id');
      
      //se cuentan los destinatarios asociados a los grupos
      //pgs - 26/05/2011 
      //$sql = "SELECT count(desg_id) as total FROM destinatario_grupo WHERE desg_estado=1 AND desg_gru_id IN (".implode(",",array_keys($grupos)).")";
      $sql = "SELECT COUNT(DISTINCT(des_email)) as total 
              FROM destinatarios,destinatario_grupo 
              WHERE des_email=desg_des_correo AND des_baja=0 
              AND des_activo=1 AND desg_estado=1 
              AND desg_gru_id IN (".implode(",",array_keys($grupos)).") 
              AND des_usu_id='".$_SESSION['usu_id']."'
              AND des_dureza_rebotado!=".$_correo_duro."";
      //var_dump($sql);
      $num_destinatarios = $_obj_database->obtenerRegistrosAsociativos($sql);
      //var_dump($sql);
      $res['total'] = $num_destinatarios[0]['total'];
    }//Fin de if($envio['enb_fin_procesado']==1 || $envio['enb_fin_procesado']=='1') */
		
		return $res;
  }//Fin de estadoInformeBoletin($datos)
  
  /*	*******************************	ENVIO BOLETIN ******************************	*/
	/*
	*	Utilidad:
	*		trae la informacion del envio y la informacion del boletin asociado al envio
	*			
	*	Par�metros de entrada:
	*		$datos['bol_id'] -> id del boletin
	*			
	*	Valores de retorno:
	*		$res['resultado'] -> informacion del envio y del boletin asociado
	*/
	function obtenerEnvioBoletin($datos){
	  global $_obj_database;	
		
		//pgs - 30/08/2012 - se trae el nombre del grupo a quien se le envia
		$sql = "SELECT enb_fecha_inicio,bol_nombre,gru_nombre
            FROM envio_boletin,boletin,envio_grupo,grupos 
            WHERE bol_id=enb_bol_id
              and eng_enb_id=enb_id
              and eng_gru_id=gru_id 
              and bol_id = '".$datos['bol_id']."' 
              and enb_id='".$datos['enb_id']."'";
		
		$res = $_obj_database->obtenerRegistrosAsociativos($sql);
    
    
		
		return $res[0];
  }//Fin de obtenerEnvioBoletin($datos)
	
	/*	*******************************	ESTADO ENVIAR ******************************	*/ 
	/*17-05-2011 - pablo
	*	Utilidad:
	*		analiza si puede agendar un nuevo envio, revisando si tiene env�os por hacer para este boletin, si ya tiene un env�o no
	*		dejara agendar uno nuevo, hasta que termine el anterior	
	*			
	*	Par�metros de entrada:
	*		$datos['bol_id'] -> id del boletin
	*			
	*	Valores de retorno:
	*		$res['resultado'] -> true=> puede agendar un nuevo env�o, false => no puede agendar un nuevo env�o
	*/
	function estadoEnviar($datos){
	 global $_obj_database;	
		
		$sql = "SELECT enb_id
            FROM envio_boletin
            WHERE enb_bol_id = '".$datos['bol_id']."' 
              AND enb_termino_enviar=0 
              AND enb_estado=1 
              AND enb_eliminado=0
              AND enb_informe_creado=0";
		
		$res = $_obj_database->obtenerRegistrosAsociativos($sql);
		
		if(sizeof($res)>0){
      return false;
    }else{
      return true;
    }//Fin de if(sizeof($res)>0)
  }//Fin de estadoEnviar($datos)
	
	/*	*******************************	LISTAR DESTINATARIOS ******************************	*/
	/*
	*	Utilidad:
	*		Lista los destinatarios del sistema

	*	Par�metros de entrada:
	*		$datos -> array con los datos de offser y limit

	*	Valores de retorno:
	*		$listado -> array con el listado de los destinatarios
	*/
	function listarDestinatarios($datos){
		global $_obj_database, $_limit;
		
		$datos = Herramientas::trimCamposFormulario($datos);
		
		$offset = (isset($datos['offset']) && is_numeric($datos['offset']) ? $datos['offset'] : 0);
		$condicion = "";      
		
		switch($datos['tipo']){
      case 'enviados':
        $condicion .= ' and env_enviado=1 and env_error_envio=0 and env_eliminado=0';
      break;
      
      case 'no_enviados':
        $condicion .= ' and env_error_envio=1 and env_eliminado=0';
      break;
      
      case 'pendientes':
        $condicion .= ' and env_enviado=0 and env_eliminado=0';
      break;
      
      case 'leidos':
        $condicion .= ' and env_leido=1 and env_eliminado=0';
      break;
      
      case 'baja':
        $condicion .= ' and env_baja=1';
      break;
    }//Fin de switch($datos['tipo'])
		
		$orden = "ORDER BY des_email ASC";
		if( $datos['ordena'] != ""){
			$orden = "ORDER BY ".$datos['ordena']." ".$datos['tipo_ordena'];
		}
		
    //se traen los ids de los grupos a quienes se les agendo el envio
		$sql = "SELECT eng_gru_id FROM envio_grupo WHERE eng_enb_id='".$datos['enb_id']."'";
		$grupos_envio = $_obj_database->obtenerRegistrosAsociativosPorClave($sql,'eng_gru_id');
    	
		$sql = "SELECT des_id,des_email,des_nombre, gru_nombre 
  					FROM destinatarios,destinatario_grupo,grupos,envios 
  					WHERE des_id=env_des_id and desg_des_correo=des_email and desg_gru_id=gru_id and desg_estado=1 and gru_activo = 1 and env_enb_id='".$datos['enb_id']."' and des_usu_id='".$_SESSION['usu_id']."' and gru_usu_id='".$_SESSION['usu_id']."' AND desg_gru_id IN (".implode(',',array_keys($grupos_envio)).")
  					  ".$condicion."
  					".$orden." 
  					LIMIT ".$offset.",".$_limit;
  	
    $resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
    
    
		
		if(is_array($resultado)){
     
  	 //mejora del sql anterior 23/05/2011
  	 $sql = "SELECT count(des_id) as num_total_registros 
  					FROM destinatarios,envios 
  					WHERE des_id=env_des_id and env_enb_id='".$datos['enb_id']."' and des_usu_id='".$_SESSION['usu_id']."' 
  					  ".$condicion;
            		  
			$resultado_conteo = $_obj_database->obtenerResultado($sql);
			$resultado['limite']=$_limit;
			$resultado['offset']=$offset;
			$resultado['num_total_registros']= $resultado_conteo[0];
			$resultado['usa_filtro'] = $datos['usa_filtro'];
		}//Fin de if(is_array($resultado))
		
		return $resultado;
	}//Fin de listarDestinatarios($datos)
	
	function enviarCorreosProgramados($datos){
    global $_obj_database,$_limite_correos,$_EMAIL_FROM,$_NOMBRE_FROM,$_PATH_WEB,$_limite_daemon_tiempo,$_PATH_LOCAL, $_PATH_LOCAL_ADJUNTOS;
    
    
    $porcentaje_libre = $this->obtenerPorcentajeEspacioDisponible();
    if( $porcentaje_libre > 80 )
    {
        echo "<br /><br /><h3>El sistema est� ocupando el $porcentaje_libre% de espacio lleno en el servidor. No se ejecuta el daemon de env�o hasta que el % sea menor a 80%</h3>";
        return;  
    }

      
    $obj_correo = new Correo();
    
    //pgs - 07/06/2012 - el limite de correos a enviar se traen de la tabla usuarios
    //$datos['limite'] = $_limite_correos;
    
    $res_html = '';
    
    $ultima_fecha_envio = Herramientas::obtenerParametro('ultima_fecha_envio');
    
    $tiempo_actual = strtotime(date('Y-m-d H:i'));
    $tiempo_ultima_ejec = strtotime($ultima_fecha_envio);
    
    $tiempo_diff = $tiempo_actual - $tiempo_ultima_ejec;
    $tiempo_diff = (int) date('i',$tiempo_diff);
    
    if($tiempo_diff < $_limite_daemon_tiempo){
      //return 'El daemon ya se ejecut&oacute;';
    }else{
      //se actualiza la hora en que se ejecuto el daemon
      Herramientas::actualizarParametro('ultima_fecha_envio',date('Y-m-d H:i').':00');
    }//Fin de if($tiempo_diff < $_limite_daemon_tiempo)
    
    $estado_envios = array();
    
    //Se obtienen los clientes activos y con envios por hacer 
    $clientes = $this->obtenerClientes($datos);
    //var_dump($clientes);exit;
    foreach($clientes as $cliente){      
      $smtp = trim($cliente['usu_smtp_host']);
      $cuenta = trim($cliente['usu_smtp_cuenta']);
      $clave = trim($cliente['usu_smtp_contrasenha']);
      $autenticacion = trim($cliente['usu_smtp_autenticacion']);
      $puerto = trim($cliente['usu_smtp_puerto']);
      $ssl = trim($cliente['usu_smtp_ssl']);
      
      //pgs - 14/06/2013 - se crea la variable del correo del cliente
      $usu_correo = trim($cliente['usu_correo']);
      
      //pgs - 07/06/2012 - el limite de correos a enviar se traen de la tabla usuarios
      $datos['limite'] = $cliente['usu_limite_envio'];
      
      $configuracion['host'] = $smtp;
      $configuracion['cuenta'] = $cuenta;
      $configuracion['clave'] = $clave;
      $configuracion['autenticacion'] = $autenticacion;
      $configuracion['puerto'] = $puerto;       
      
      if($ssl==1){
        $configuracion['secure'] = 'ssl';
        
        /*chcm.  Nov 10 2014*/
		
		//AMP - 16/03/16 - se reemplaza a tls  debido a que sslv3 es deprecated.
        if ($cuenta=="AKIAIAG3MHEXYS63PO4Q") //si es la cuenta de CUTIS , la configuraci�n va por sslv3
          $configuracion['secure'] = 'tls';       
       
         if ($cuenta=="AKIAINL3H4YOXFLRDHAA") //si es la cuenta de CUTIS , la configuraci�n va por sslv3
          $configuracion['secure'] = 'tls';
       
         /*chcm.  Ene 17 2015*/
        if ($cuenta=="AKIAJINRDXID3VWBOYQA") //si es la cuenta de THECOLORWEAR , la configuraci�n va por sslv3
          $configuracion['secure'] = 'tls';   
          
        /*chcm.  Mar 18 2015*/
        if ($cuenta=="AKIAI3EWXFJCOUXWRSPQ") //si es la cuenta de ELA , la configuraci�n va por sslv3
          $configuracion['secure'] = 'tls';
        
           /*chcm.  Abr 24 2015*/
        if ($cuenta=="AKIAJRSF6ZS24CKAHQYA") //si es la cuenta de CLAUDIA TOVAR  , la configuraci�n va por sslv3
          $configuracion['secure'] = 'tls';
          
          /*chcm  Sept 09 2015*/
           if ($cuenta=="AKIAJMRQWQ4WCYBPMG3Q") //si es la cuenta de JUANCHOCORRELON  , la configuraci�n va por sslv3
              $configuracion['secure'] = 'tls';
     
     
          
      }//Fin de if($ssl==1)   
      
      $datos['usu_id'] = $cliente['usu_id'];
      
      $count_env_error = 0;
      $count_env = 0;
      $num_pendientes = 0;
      
      if(!empty($smtp) && !empty($cuenta) && !empty($clave)){
        //pgs - 07/06/2012 - el limite de correos a enviar se traen de la tabla usuarios
        //$datos['limite'] = $_limite_correos;
        $datos['enb_id'] = $cliente['enb_id'];
        $envios = $this->obtenerEnvios($datos);     
        
        
        $info_boletin = $envios['boletin'][0];
        
        /*echo "<pre>";
        print_r($envios);
        echo "</pre>";*/
        if(sizeof($envios['envios'])>0){
        
          foreach($envios['envios'] as $key => $envio){
            //array con los id de los envios
            $estado_envios[$envio['enb_id']] = $info_boletin['enb_id'];
            
            $cuerpo = Archivos::obtenerContenidoArchivo($this->_plantillas."/msj_boletin.html");
            
            $destinatario = $envio['des_nombre'].' <'.$envio['des_email'].'>';
            
             switch($cuenta)
             {
               /*SI EL CLIENTE ES CUTIS CAMBIO EL NOMBRE DEL CORREO DEL REMITENTE*/
               case "AKIAIAG3MHEXYS63PO4Q":
               case "AKIAINL3H4YOXFLRDHAA": $remitente = $info_boletin['bol_nombre_remitente'].' <publicidad@cutis.com.co>';
               break;
               
               /*SI EL CLIENTE ES THECOLORWEAR CAMBIO EL NOMBRE DEL CORREO DEL REMITENTE*/
               case "AKIAJINRDXID3VWBOYQA":   $remitente = $info_boletin['bol_nombre_remitente'].' <boletines@thecolorwear.com>';
             	 break; 
               
               /*SI EL CLIENTE ES ELA CAMBIO EL NOMBRE DEL CORREO DEL REMITENTE*/
               case "AKIAI3EWXFJCOUXWRSPQ":   $remitente = $info_boletin['bol_nombre_remitente'].' <info@ela.com.co>';
             	 break;
               
               /*SI EL CLIENTE ES ELA CAMBIO EL NOMBRE DEL CORREO DEL REMITENTE*/
               case "AKIAJRSF6ZS24CKAHQYA":   $remitente = $info_boletin['bol_nombre_remitente'].' <contacto@morenoconsultores.com.co>';
             	 break;
               
                 /*SI EL CLIENTE ES JUANCHOCORRELON CAMBIO EL NOMBRE DEL CORREO DEL REMITENTE*/
               case "AKIAJMRQWQ4WCYBPMG3Q":   $remitente = $info_boletin['bol_nombre_remitente'].' <envios@boletines.juanchocorrelon.com>';
             	 break;
               
                  
                //si no es cutis ni thecolorwear ni ela ni juanchocorrelon entonces traigo el remitente definido en el formulario
                default:  $remitente = $info_boletin['bol_nombre_remitente'].' <'.$info_boletin['bol_correo_remitente'].'>';
             
             }            
              
            
            $buscar = array('[nombre]','[NOMBRE]');
            $reemplazar = $envio['des_nombre'];
            
            $asunto = str_replace($buscar,$reemplazar,$info_boletin['bol_asunto']);	            
            $bol_html = str_replace($buscar,$reemplazar,$info_boletin['bol_html']);
                        
            Interfaz::asignarToken("bol_html", $bol_html, $cuerpo);
            Interfaz::asignarToken("env_id", $envio['env_id'], $cuerpo);
            Interfaz::asignarToken("path_web", $_PATH_WEB, $cuerpo);
            
            //pgs - 30/01/2012 - se pone el des_id en la plantilla
            //tambien se cambia en la plantilla y se pone el proyecto, el des_id y el env_id
            Interfaz::asignarToken("des_id", $envio['des_id'], $cuerpo);
            Interfaz::asignarToken("env_id", $envio['env_id'], $cuerpo);
            
			//Se adjuntan los archivos            
            $archivos_adjuntos = $this->obtenerArchivosAdjuntosBoletin($info_boletin);
            $array_adjuntos = array();
            $cont_adj = 0;
            if(sizeof($archivos_adjuntos) > 0)
            {
            	foreach($archivos_adjuntos as $adjunto)
            	{
            		$ruta_adj = $_PATH_LOCAL_ADJUNTOS."/".$info_boletin['bol_id']."/".$adjunto["adj_nombre_archivo"];
            		
            		$array_adjuntos["archivo_".$cont_adj] = array();
					$array_adjuntos["archivo_".$cont_adj]["name"] = $adjunto["adj_nombre_archivo"]; // the name of the uploaded file
					$array_adjuntos["archivo_".$cont_adj]["type"] = filetype($ruta_adj); // the type of the uploaded file
					$array_adjuntos["archivo_".$cont_adj]["size"] = filesize($ruta_adj); // the size in bytes of the uploaded file
					$array_adjuntos["archivo_".$cont_adj]["tmp_name"] = $ruta_adj; // the name of the temporary copy of the file stored on the server
					$array_adjuntos["archivo_".$cont_adj]["error"] = 0; // the error code resulting from the file upload
					        
            		$cont_adj++;
				}
			}
            	
            //$cuerpo = $envio['bol_html'];
            $sender = $cliente['usu_correo_rebotado'];
            //var_dump($cliente);
            
          
            
            
            
            //pgs - 14/06/2013 - se pone el responder a
            $replyto = $usu_correo; 
            $resultado = $obj_correo->enviarCorreo($remitente, $destinatario, $asunto, $cuerpo, $array_adjuntos, $sender, $configuracion,$replyto);
            //$resultado=true;
            //$resultado=false;
            
            /*$fp = fopen($_PATH_LOCAL.'/Logs/envios_'.$datos['enb_id'].'.txt', "a");
            $string = $envio['des_email']."  Fecha: ".date('d-m-Y H:i:s')." \n";
            $write = fputs($fp, $string);
            fclose($fp);*/
            
            $env['tabla'] = "envios";
            $env['condicion'] = "env_id='".$envio['env_id']."'";
            $env['env_enviado'] = 1;
            
            //pgs - 25/07/2012 - se actualiza la fecha en que se envio
            $env['env_fecha_enviado'] = date('Y-m-d H:i:s');
               
          	$campos  = array("env_enviado","env_fecha_enviado");
          	
          	if($resultado!=1){
          	 $campos[] = 'env_error_envio';
          	 $env['env_error_envio'] = 1;
             $count_env_error++; 
            }//Fin de if($resultado!=1)
          	
          	$count_env++;
            				
          	$sql = $_obj_database->generarSQLActualizar($env,$campos);
          	
          	
          	
          	$resultado = $_obj_database->ejecutarSql($sql); 
          	if(!$resultado){
            	$fp = fopen($_PATH_LOCAL.'/Logs/no_envios_'.$datos['enb_id'].'.txt', "a");
              $string = $sql." \n";
              $write = fputs($fp, $string);
              fclose($fp);
            }//Fin de if(!$resultado)
          	$res_html .= 'Correo enviado a: '.$envio['des_email'].'<br>';
          }//Fin de foreach($envios as $key => $envio)
        }else{
          $res_html .= 'No hay correos por enviar para el usuario '.$cliente['usu_id'].'<br>';
        }//Fin de if(sizeof($envios)>0)
      }//Fin de if(!empty($stmp) && !empty($cuenta) && !empty($clave))
      
      //Se revisa si se termino de enviar el boletin del envio
      foreach($estado_envios as $envio){
        $fecha_actual = date('Y-m-d H:i:s');
        
        //pgs - 26/0672011 - se debe hacer una asociacion con destinatario para traer los que no se han dado de baja
        /*$sql = "SELECT count(env_id) as pendientes
                FROM envios
                WHERE env_enviado=0 and env_enb_id = '".$envio."'";*/
        
        $sql = "SELECT count(env_id) as pendientes
                FROM envios,destinatarios
                WHERE env_des_id=des_id AND des_baja=0 AND des_activo=1 AND env_enviado=0 and env_eliminado=0 and env_enb_id = '".$envio."'";
    		
           
    		$res = $_obj_database->obtenerRegistrosAsociativos($sql);
    		
    		$num_pendientes =  $res[0]['pendientes'];
    		
    		//pgs - 12/01/2012 - se verifica si se termino de procesar, se podra dar como terminado de enviar.
    		$sql_termino_procesar = "SELECT enb_fin_procesado
                FROM envio_boletin
                WHERE enb_id = '".$envio."'";
    		
    		$res_termino_procesar = $_obj_database->obtenerRegistrosAsociativos($sql_termino_procesar);
    		//pgs - 12/01/2012 - fin de se verifica si se termino de procesar, se podra dar como terminado de enviar.
    		
    		//Si no hay registros entonces el envio termino
    		//pgs - 12/01/2012 - se a�ade que si se ha terminado de procesar el envio
    		if($num_pendientes <= 0 && $res_termino_procesar[0]['enb_fin_procesado']==1){
    			$datos['tabla'] = "envio_boletin";
          $datos['condicion'] = 'enb_id='.$envio;
          $datos['enb_fecha_termino'] = date('Y-m-d H:i:s');
          $datos['enb_termino_enviar'] = 1;
        	$campos  = array("enb_termino_enviar",
                          "enb_fecha_termino"
                          );
        					
        	$sql = $_obj_database->generarSQLActualizar($datos,$campos);
        	
        	$resultado = $_obj_database->ejecutarSql($sql);
          
          //pgs - 16/01/2012 - se obtiene el nombre del cliente
          $sql = "select env_id,usu_correo,usu_nombre,bol_nombre,enb_fecha_inicio,enb_fecha_termino 
  				from envios,envio_boletin,boletin,usuario
  				where env_enb_id=enb_id and enb_bol_id=bol_id and bol_usu_id=usu_id and env_enb_id = '".$envio."' group by env_enb_id";
  					
    		  $res_env = $_obj_database->obtenerRegistrosAsociativos($sql);
          
          //se envia el correo al creador del boletin
          $remitente = $_NOMBRE_FROM." <".$_EMAIL_FROM.">";
          $destinatario = $res_env[0]['usu_correo'];
          $asunto = 'El envio del boletin '.$res_env[0]['bol_nombre'].' ha finalizado';
          $fecha_inicio = date('d-m-Y',strtotime($res_env[0]['enb_fecha_inicio']));
          $fecha_termino = date('d-m-Y',strtotime($res_env[0]['enb_fecha_termino']));
          $hora_termino = date('H:i',strtotime($res_env[0]['enb_fecha_termino']));
          
          //pgs - 16/01/2012 - se obtiene el contenido del correo de notificacion de que termino un boletin
          $info_cuerpo['usu_nombre'] = $res_env[0]['usu_nombre'];
          $info_cuerpo['bol_nombre'] = $res_env[0]['bol_nombre'];
          $info_cuerpo['fecha_inicio'] = $fecha_inicio;
          $info_cuerpo['fecha_termino'] = $fecha_termino;
          $info_cuerpo['hora_termino'] = $hora_termino;
          
          $cuerpo = $this->obtenerCorreoNotificacion($info_cuerpo);
          
          /*$cuerpo = 'Este es un aviso para notificarle que su bolet&iacute;n <b>'.$res_env[0]['bol_nombre'].'</b>';                     
          $cuerpo .= ' enviado el <b>'.$fecha_inicio.'</b> ha terminado de enviarse completamente el <b>'.$fecha_termino.'</b>';
          $cuerpo .= ' a las <b>'.$hora_termino.'</b>.<br><br>';
          $cuerpo .= 'Cordialmente<br><br>';
          $cuerpo .= 'Querytek Colombia';*/
          
          $from = 'colombia@querytek.com';
          $cabeceras = "From: Boletines Colombia <colombia@querytek.com>"."\n"."X-Mailer: PHP/".phpversion()."\n"."MIME-Version: 1.0"."\n"."Content-Type: text/html; charset=ISO-8859-1";  
          mail($destinatario,$asunto,$cuerpo,$cabeceras);
          //pgs - 04/10/2011 - me notifico cada vez que terminade enviar un boletin
          //mail("pablo.gomez@querytek.com,blogo86@gmail.com",$asunto,$cuerpo,$cabeceras);
          mail("david.males@querytek.com,carlos.ceron@querytek.com",$asunto,$cuerpo,$cabeceras);
          //$resultado = $obj_correo->enviarCorreo($remitente, $destinatario, $asunto, $cuerpo,null,null,$configuracion); 
    		}//Fin de if( sizeof($res) > 0 )
      }//Fin de foreach($estado_envios as $envio)
      
      echo 'Cliente: <b>'.$cliente['usu_nombre'].'</b> con id '.$cliente['usu_id'].':<br> enviados: '.$count_env.' <br>enviados con error: '.$count_env_error.' <br> envios pendientes '.$num_pendientes.'<br>';
      
      //pgs - 30/09/2011 - notificacion de error de envio en los lotes
      //si el envio supera mas de 100 correos
      if($count_env>100){ 
        //si el porcentaje de error del lote es mayor a 30, enva notificacion a carlos y a pablo      
        $porcentaje_error = ($count_env_error/$count_env)*100;
        if($porcentaje_error>30){
          //se verifica si ya se notifico
          $notifico_error = Herramientas::obtenerParametro('notifico_error');
          
          //si no se ha notificado
          if($notifico_error==0){
            //se actualiza el parametro
            Herramientas::actualizarParametro('notifico_error',1);
            
            $cabeceras = "From: Boletines Colombia <colombia@querytek.com>"."\n"."X-Mailer: PHP/".phpversion()."\n"."MIME-Version: 1.0"."\n"."Content-Type: text/html; charset=ISO-8859-1";  
            //$destinatario_notificacion = "pablo.gomez@querytek.com,blogo86@gmail.com,carlos.ceron@querytek.com,david.males@querytek.com";
            //DM - 2013-10-08
            //cambio el destinatario por el de colombia
            $destinatario_notificacion = "colombia@querytek.com";
            $asunto_notificacion = "Alerta: error de mas del 30% al enviar";
            $cuerpo_notificacion = "Para el cliente <b>".$cliente['usu_nombre']."</b> con id: ".$cliente['usu_id'].' ha habido un <b>%'.$porcentaje_error.'</b> de error al enviar los correos<br>
                                    Numero de correos procesados:'.$count_env.'<br>
                                    Numero de correos no enviados:'.$count_env_error.'<br>
                                    Numero de correos enviados:'.($count_env-$count_env_error).'<br><br>
                                    <b>Nota: Por favor no se olvide de cambiar el valor del registro "notifico_error" de la tabla parametro a 0, para que vuelva a enviar esta notificacion.</b>';
            //echo $cuerpo_notificacion;                                  
            mail($destinatario_notificacion,$asunto_notificacion,$cuerpo_notificacion,$cabeceras);
          }//Fin de if($notifico_error==0) 
        }//Fin de if($porcentaje_error>30)
      }//Fin de if($count_env>100)
      
    }//Fin de foreach($clientes as $cliente)
    
    if(sizeof($clientes)<=0){
      echo 'No hay envios para el cliente';
    }//Fin de if(sizeof($clientes)<=0)
     
    return $res_html;	
  }//Fin de pruebaCorreosProgramados($datos)
  
  /*
  * pgs - 16/01/2012 
  * Funcion que se encarga de obtener el correo para notificarle al usuario que se termino de enviar un boletin  
  */  
  function obtenerCorreoNotificacion($datos){
    $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas."/correo_notificacion.html");
    
    Interfaz::asignarToken("usu_nombre", $datos['usu_nombre'], $contenido);
    Interfaz::asignarToken("bol_nombre", $datos['bol_nombre'], $contenido);
    Interfaz::asignarToken("fecha_inicio", $datos['fecha_inicio'], $contenido);
    Interfaz::asignarToken("fecha_termino", $datos['fecha_termino'], $contenido);
    Interfaz::asignarToken("hora_termino", $datos['hora_termino'], $contenido);
    
    return $contenido;
  }//Fin de obtenerCorreoNotificacion($datos)
  
  function obtenerArchivosAdjuntosBoletin($datos)
  {
  	global $_obj_database;
  	
  	$sql = "SELECT *
	  		FROM adjuntos_boletin
			WHERE adj_bol_id = '".$datos["bol_id"]."'
				AND adj_estado = 1";  
    $res_adj = $_obj_database->obtenerRegistrosAsociativos($sql);
  
  	return $res_adj;  
  }//Fin de obtenerArchivosAdjuntosBoletin($datos)
  
  function eliminarArchivoAdjuntoBoletin($datos)
  {
  	global $_obj_database, $_PATH_LOCAL_ADJUNTOS; 
  	
  	if($datos["adj_id"] != "" && is_numeric($datos["adj_id"]))
  	{
	  	$sql = "SELECT *
		  		FROM adjuntos_boletin
				WHERE adj_id = '".$datos["adj_id"]."'";  
	    $res_adj = $_obj_database->obtenerRegistrosAsociativos($sql);
	    
		if(sizeof($res_adj) > 0)
		{
			$sql = "DELETE FROM adjuntos_boletin WHERE adj_id = '".$datos["adj_id"]."'";
			$res = $_obj_database->ejecutarSql($sql);
			
			if($res != true)
			{
				return "false"; 
			}
			else
			{
				@unlink($_PATH_LOCAL_ADJUNTOS.'/'.$res_adj[0]['adj_bol_id'].'/'.$res_adj[0]["adj_nombre_archivo"]);
				return "true";
			}
		}
		else
		{	                    
			return "false"; 
		}
	}
	else
	{
		return "false";
	}
  }
  
  /*
  * pgs - 30/08/2012
  * Funcion que se encarga de obtener losd atos del boletin y del cliente(contenido boletin, asunto, configuracion smtp)  
  */
  function obtenerDatosBoletinCliente($datos){
    global $_obj_database;
    
    $sql = "select 	bol_nombre_remitente,bol_correo_remitente,bol_correo_rebotado,
              bol_asunto,bol_html,usu_smtp_host,usu_smtp_cuenta,usu_smtp_contrasenha,
              usu_smtp_autenticacion,usu_smtp_puerto,usu_smtp_ssl
            from usuario,boletin
            where usu_id=bol_usu_id
              and bol_id='".$datos['bol_id']."'
              and usu_id='".$_SESSION['usu_id']."'";	
		                                  
		$resultado = $_obj_database->obtenerRegistrosAsociativos($sql);
		
		//se obtienel os archivos adjuntos
    $resultado[0]['archivos_adjuntos'] = $this->obtenerArchivosAdjuntosBoletin($datos); 

    return $resultado[0];		
  }//Fin de obtenerDatosBoletinCliente($datos)  
  
  /*
  * pgs - 30/08/2012 
  * Funcion que se encarga en enviar un correo de prueba de un boletin  
  */  
  function enviarCorreoPrueba($datos,$datos_boletin){
    global $_PATH_LOCAL_ADJUNTOS,$_PATH_WEB;
  
    $obj_correo = new Correo();
  
    //se obtiene la plantilla del boletin
    $cuerpo = Archivos::obtenerContenidoArchivo($this->_plantillas."/msj_boletin_prueba.html");
            
    $destinatario = $datos['envp_correo'];
    $remitente = $datos_boletin['bol_nombre_remitente'].' <'.$datos_boletin['bol_correo_remitente'].'>';
    
    $buscar = array('[nombre]','[NOMBRE]');
    $reemplazar = $datos_boletin['bol_nombre_remitente'];
    
    $asunto = '[Prueba] '.str_replace($buscar,$reemplazar,$datos_boletin['bol_asunto']);	            
    $bol_html = str_replace($buscar,$reemplazar,$datos_boletin['bol_html']);
                
    Interfaz::asignarToken("bol_html", $bol_html, $cuerpo);
    Interfaz::asignarToken("path_web", $_PATH_WEB, $cuerpo);
    
    $smtp = trim($datos_boletin['usu_smtp_host']);
    $cuenta = trim($datos_boletin['usu_smtp_cuenta']);
    $clave = trim($datos_boletin['usu_smtp_contrasenha']);
    $autenticacion = trim($datos_boletin['usu_smtp_autenticacion']);
    $puerto = trim($datos_boletin['usu_smtp_puerto']);
    $ssl = trim($datos_boletin['usu_smtp_ssl']);
    
    $configuracion['host'] = $smtp;
    $configuracion['cuenta'] = $cuenta;
    $configuracion['clave'] = $clave;
    $configuracion['autenticacion'] = $autenticacion;
    $configuracion['puerto'] = $puerto;      
    
    if($ssl==1){
      $configuracion['secure'] = 'ssl';   
      
      //chcm.   Noviembre 10 2014
      if ($cuenta=="AKIAIAG3MHEXYS63PO4Q") //si es la cuenta de CUTIS , la configuraci�n va por tls
        $configuracion['secure'] = 'tls';
        
       if ($cuenta=="AKIAINL3H4YOXFLRDHAA") //si es la cuenta de CUTIS , la configuraci�n va por tls
        $configuracion['secure'] = 'tls';
        
      //chcm.   Enero 17 2015
      if ($cuenta=="AKIAJINRDXID3VWBOYQA") //si es la cuenta de THECOLORWEAR , la configuraci�n va por tls
        $configuracion['secure'] = 'tls';
    
    
      //chcm.   Marzo 18 2015
      if ($cuenta=="AKIAI3EWXFJCOUXWRSPQ") //si es la cuenta de ELA , la configuraci�n va por tls
        $configuracion['secure'] = 'tls';
    
    
        /*chcm.  Abr 24 2015*/
        if ($cuenta=="AKIAJRSF6ZS24CKAHQYA") //si es la cuenta de CLAUDIA TOVAR  , la configuraci�n va por sslv3
          $configuracion['secure'] = 'tls';
    
        /*chcm.  Sept  09 2015*/
        if ($cuenta=="AKIAJMRQWQ4WCYBPMG3Q") //si es la cuenta de JUANCHOCORRELON  , la configuraci�n va por sslv3
          $configuracion['secure'] = 'tls';
          
        
    }//Fin de if($ssl==1)
    
    //Se adjuntan los archivos            
    $archivos_adjuntos = $datos_boletin['archivos_adjuntos'];
    $array_adjuntos = array();
    $cont_adj = 0;
    if(sizeof($archivos_adjuntos) > 0){
    	foreach($archivos_adjuntos as $adjunto){
    		$ruta_adj = $_PATH_LOCAL_ADJUNTOS."/".$datos['bol_id']."/".$adjunto["adj_nombre_archivo"];
    		
    		$array_adjuntos["archivo_".$cont_adj] = array();
      	$array_adjuntos["archivo_".$cont_adj]["name"] = $adjunto["adj_nombre_archivo"]; // the name of the uploaded file
      	$array_adjuntos["archivo_".$cont_adj]["type"] = filetype($ruta_adj); // the type of the uploaded file
      	$array_adjuntos["archivo_".$cont_adj]["size"] = filesize($ruta_adj); // the size in bytes of the uploaded file
      	$array_adjuntos["archivo_".$cont_adj]["tmp_name"] = $ruta_adj; // the name of the temporary copy of the file stored on the server
      	$array_adjuntos["archivo_".$cont_adj]["error"] = 0; // the error code resulting from the file upload
	        
    		$cont_adj++;
      }//Fin de foreach($archivos_adjuntos as $adjunto)
    }//Fin de if(sizeof($archivos_adjuntos) > 0)
    	
    $sender = $datos_boletin['usu_correo_rebotado'];
    
    
    
    if ($cuenta=="AKIAIAG3MHEXYS63PO4Q" || $cuenta=="AKIAINL3H4YOXFLRDHAA") //si es la cuenta de CUTIS , pongo el remitente hard coded
               $remitente = $info_boletin['bol_nombre_remitente'].'<publicidad@cutis.com.co>';
            
    if ($cuenta=="AKIAJINRDXID3VWBOYQA") //si es la cuenta de THECOLORWEAR , pongo el remitente hard coded
               $remitente = $info_boletin['bol_nombre_remitente'].'<boletines@thecolorwear.com>';
      
     if ($cuenta=="AKIAI3EWXFJCOUXWRSPQ") //si es la cuenta de ELA , pongo el remitente hard coded
               $remitente = $info_boletin['bol_nombre_remitente'].'<info@ela.com.co>';
     
     /*chcm.  Abr 24 2015*/
        if ($cuenta=="AKIAJRSF6ZS24CKAHQYA") //si es la cuenta de CLAUDIA TOVAR  ,  pongo el remitente hard coded
         $remitente = $info_boletin['bol_nombre_remitente'].'<contacto@morenoconsultores.com.co>'; 
    
     /*chcm.  Sept 09 2015*/
        if ($cuenta=="AKIAJMRQWQ4WCYBPMG3Q") //si es la cuenta de JUANCHO CORRELON  ,  pongo el remitente hard coded
         $remitente = $info_boletin['bol_nombre_remitente'].'<envios@boletines.juanchocorrelon.com>'; 
    
    
    
    $resultado = $obj_correo->enviarCorreo($remitente, $destinatario, $asunto, $cuerpo, $array_adjuntos, $sender, $configuracion);
    
    if($resultado!=1){
  	 return 'EP-1';//error al enviar el correo de prueba
    }else{
      return 'EP-0';//el correo de prueba se envi� satisfactoriamente
    }//Fin de if($resultado!=1)    
  }//Fin de enviarCorreoPrueba($datos,$datos_boletin)
  
  
  function send_sms ($destino, $mensaje){
  	global $_URL_SMS,$_LOGIN_SMS,$_PASSWORD_SMS,$_CLIENTID_SMS,$_ACOUNTID_SMS,$_SEQ_SMS,$_SOURCE_SMS;

  	$URL = $_URL_SMS;
    $login = $_LOGIN_SMS;
    $password = $_PASSWORD_SMS;
    $clientID = $_CLIENTID_SMS;
    $accountID= $_ACOUNTID_SMS;
    $destination =  $destino;
    $text = $mensaje;
    $seq = $_SEQ_SMS;
    $source = $_SOURCE_SMS;
  
  	$ch = curl_init();
  	curl_setopt($ch, CURLOPT_URL,$URL);
  	curl_setopt($ch, CURLOPT_POST, 1);
  	curl_setopt($ch, CURLOPT_POSTFIELDS, "login=$login&password=$password&clientID=$clientID&accountID=$accountID&destination=$destination&text=$text&seq=$seq&source=$source");
  	curl_setopt($ch, CURLOPT_TIMEOUT, 90);
  	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  	$rs = curl_exec($ch);
  	curl_close ($ch);
  
  	return trim($rs);
  	//return 'OK';
  
  }
  

  
  /*
   * pgs - 30/08/2012
   * Funcion que se encarga en enviar un correo de prueba de un boletin
   */
  function enviarPendientes($datos){
    global $_obj_database,$_limite_correos,$_EMAIL_FROM,$_NOMBRE_FROM,$_PATH_WEB,$_limite_daemon_tiempo,$_PATH_LOCAL, $_PATH_LOCAL_ADJUNTOS;


    
    $res_html = '';
    $estado_envios = array();
         
     
    //jcb 02/03/2017 asignacion de contadores
    $count_env=0;
    $count_fallidos=0;
    

        //jcb - 02/03/2017 - se asignan cantidads por procesar, fallidos y pendientes;
       $total_pendientes=$this->obtenerCountEnviosPendientes();
       (isset($datos['cantidad_pendientes'])) ? $cantidad_pendientes = $datos['cantidad_pendientes'] :$cantidad_pendientes=(int)$total_pendientes[0]['pendientes'];
       (isset($datos['cantidad_enviados'])) ? $cantidad_enviados = $datos['cantidad_enviados'] :$cantidad_enviados=0;
       (isset($datos['cantidad_procesados'])) ? $cantidad_procesados = $datos['cantidad_procesados'] :$cantidad_procesados=0;
       (isset($datos['cantidad_fallidos'])) ? $cantidad_fallidos = $datos['cantidad_fallidos'] :$cantidad_fallidos=0;
      
      if(isset($datos['flag']) && $datos['flag']=='procesar'){
      	
      	//echo $datos['flag'];
      	
	      if((int)$total_pendientes[0]['pendientes'] > 0 ){
	
	      	
	      	$enviosPendientes = $this->obtenerEnviosPendientes();
	      	
	      	if(sizeof($enviosPendientes)>0){
	      		
	      		$aray_cantidad_procesados=array();
	      		
	      		foreach($enviosPendientes as $key => $envio){
	      			
	      			$count_env++;
	      			//array con procesados
	      			$aray_cantidad_procesados[]=$envio['envs_id'];
	      			//array con los id de los envios
	      			$estado_envios[] = $envio['envs_env_id'];
	      	
	      			$resultado = $this->send_sms($envio['envs_numero_destinatario'],$envio['envs_mensaje']);
	      			//jcb 01-05-2017 se actualizan estados de envios sms
	      			$env['tabla'] = "envio_sms";
	      			$env['condicion'] = "envs_id='".$envio['envs_id']."'";
	      			$env['envs_estado'] = 1;
	      	
	      			//pgs - 25/07/2012 - se actualiza la fecha en que se envio
	      			//$env['env_fecha_envio'] = 1; = date('Y-m-d H:i:s');
	      			$campos  = array("envs_estado");
	      			//jcb - 01/04/2017 - se captura error
	      			if(strpos($resultado, 'OK') !== false){
	      				$cantidad_enviados++;
	      			}
	      				else{
	      				if($envio['envs_intentos_fallidos'] <= 3){ ///Fin de if($resultado!=1)
	      					$env_error=$envio['envs_intentos_fallidos'];
	      					$env['envs_estado'] = 0;
	      					$env['envs_intentos_fallidos'] = $env_error +1;
	      					$campos[] = 'envs_intentos_fallidos';
	      					$count_fallidos ++;
	      				}
	      			}///Fin de (strpos($resultado, 'OK') !== false)
	      			
	      			$sql = $_obj_database->generarSQLActualizar($env,$campos);
	      			$resultado = $_obj_database->ejecutarSql($sql);
	      			
	      		}//Fin de foreach($envios as $key => $envio)
	         
	      	}else{
	      		$res_html .= 'No hay envios pendientes<br>';
	      	}//Fin de if(sizeof($envios)>0)
	      	
	      }else {
	      	
	      } //fin xif($this->obtenerCountEnviosPendientes() > 0 )
	      
	      
	      //jcb - 02/04/2017 Se revisan envios procesados para actualizar tabla envio
	      $envios_proesados= array_unique($estado_envios);
	      foreach($envios_proesados as $envio){
	        $fecha_actual = date('Y-m-d H:i:s');
	        
	        $sql = "SELECT count(env_id) as sms_enviados
	                FROM envio_sms
	                WHERE envs_enviado=1 and envs_intentos_fallidos < 3 and envs_env_id=$envio";
	        
	    		
	           
	    		$res = $_obj_database->obtenerRegistrosAsociativos($sql);
	    		
	    		$num_sms_enviados =  $res[0]['sms_enviados'];
	    		
	    		
	    		//Si no hay registros entonces el envio termino
	    		//pgs - 12/01/2012 - se a�ade que si se ha terminado de procesar el envio
	    		if(sms_enviados <= 0 ){
	    		  $datos['tabla'] = "envio";
		          $datos['condicion'] = 'env_id='.$envio;
		          $datos['env_fecha_envio'] = date('Y-m-d H:i:s');
		          $campos  = array("env_fecha_envio"
		                          );
	        					
	        	$sql = $_obj_database->generarSQLActualizar($datos,$campos);
	        	
	        	$resultado = $_obj_database->ejecutarSql($sql);
	    		}
	          
	          
	      }//Fin de foreach($estado_envios as $envio)
      	
  		} // fin if(isset($datos['flag']) && $datos['flag']=='procesar')
      //jcb 02/036/2017 se suma contador de enviados,fallidos por lote a cantidad total enviados.
      
      $cantidad_procesados=$_obj_database->obtenerCantidadResultados("select envs_id from envio_sms where (envs_estado != 0 or envs_intentos_fallidos != 0) and envs_procesado = 0");
      $cantidad_fallidos=$_obj_database->obtenerCantidadResultados("select envs_id from envio_sms where envs_intentos_fallidos = 3 and envs_procesado = 0");


      $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas."/enviar_pendientes.html");

      if((int)$total_pendientes[0]['pendientes'] !=0 && $count_env <= (int)$total_pendientes[0]['pendientes']){
      	$script="location.href='".$_PATH_WEB."index.php?m=envios&accion=enviar_pendientes&cantidad_procesados=$cantidad_procesados&cantidad_enviados=$cantidad_enviados&cantidad_pendientes=$cantidad_pendientes&cantidad_fallidos=$cantidad_fallidos&msg=E-1&flag=procesar'";
      	Interfaz::asignarToken("redireccion", 1, $contenido);
        Interfaz::asignarToken("ruta",$_PATH_WEB,$contenido);
        Interfaz::asignarToken("script",$script,$contenido);
      }
      else{
      	// jcb - 03/05/5017 - al terminar el proceso actualizo campo procesado
      	$datos2['tabla'] = "envio_sms";
      	$datos2['condicion'] = '1';
      	$datos2['envs_procesado'] = 1;
      	$campos2  = array("envs_procesado"
      	);
      	$sql2 = $_obj_database->generarSQLActualizar($datos2,$campos2);
      	$resultado2 = $_obj_database->ejecutarSql($sql2);
      	
      	
      	$_obj_mensajes=new Mensajes();
      	$opciones['ACCION'] = 5;
      	$opciones['CADENA'] = 'El Env&iacute;o';
      	$datos['mensaje_ok'] = $_obj_mensajes->crearMensaje('E-1', $opciones);

      	Interfaz::asignarToken("redireccion", '', $contenido);
      	Interfaz::asignarToken("mensaje_ok", $datos['mensaje_ok'], $contenido);
      }
      
      
      	
      	Interfaz::asignarToken("cantidad_procesados", $cantidad_procesados, $contenido);
      	Interfaz::asignarToken("cantidad_enviados", $cantidad_enviados, $contenido);
      	Interfaz::asignarToken("cantidad_pendientes", $cantidad_pendientes, $contenido);
      	Interfaz::asignarToken("cantidad_fallidos", $cantidad_fallidos, $contenido);
      	Interfaz::asignarToken("titulo_manual", "Proceso de env&iacute;o", $contenido);

      
    return $contenido;	
  }//Fin de enviarPendientes($datos,$datos_boletin)
  
  //chcm.  Mayo 03 2017.  Elimina todo el contenido de la tabla de env�os
  function vaciarTablaEnvios()
  {
         global $_obj_database;
         $resultado1 = $_obj_database->ejecutarSql("delete from envio");
         $resultado2 = $_obj_database->ejecutarSql("delete from envio_sms");
         
         
          $resultado1 = $resultado2 = 1;
         if($resultado1 && $resultado2)
         {
           header('Location: index.php?m=envios&accion=listado_envios&msg=E-2');exit;
         }
         
         	
         
  }
}//Fin de clase  Envios
?>
