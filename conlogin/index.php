<?php
require_once("Config.php");
require_once("Includes/index.php");  

if(!isset($_SESSION["ultimo_acceso"]) || $_SESSION["ultimo_acceso"] == "")
{
  $_SESSION["ultimo_acceso"] = date("Y-n-j H:i:s");
}
								
$_obj_database = new database($_host_db,$_user_db,$_password_db,$_db);
$_obj_interfaz = new Interfaz();
$_obj_mensajes = new Mensajes();
$_obj_herramientas = new Herramientas();

//Si no se definio un modulo toma por defecto usuarios
$datos['m'] = ( isset($datos['m']) ?  $datos['m'] : "usuarios");
//Convierte a mayuscula el primer caracter
$datos['m'] = ucfirst($datos['m']); 

/*chcm.  Julio 16/07/2012.  Para que traiga el limite de destinatarios por cliente, el valor en la sesi�n se graba en el momento de autenticarse.*/
/*if($_SESSION['$_limit_des_usuario']!="")
  $_limit_des_usuario=$_SESSION['$_limit_des_usuario'];*/

//DM - 2014-01-23 
//si el parametro no es un directorio valido deja por defecto el de usuario
if( !is_dir($datos['m']) )
{
    $datos[m] = "Usuarios";
} 
require_once($datos['m']."/index.php");
print_r($_obj_interfaz->crearInterfazGrafica($datos));

$_obj_database->desconectar();


?>
