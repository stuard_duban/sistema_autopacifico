<?php
/*
*	Nombre: Menu.php
*	Descripci�n: Clase que incluye el menu de la aplicaci�n
*	Autor: Christian Felipe Benitez Casta�o
*	E-mail:  pipeloco@gmail.com
*	Fecha de Creaci�n: 20-03-2007
*	Fecha de �ltima Modificaci�n: 20-03-2007
*/

class Menu
{
	function Menu()
	{

	}

	/********************************* CREAR MENU ************************************/
	/*
	* Utilidad:	Se encarga de crear el menu de la interfaz
	* Par�metros de entrada:
	*	void
	* Valores de retorno:
	*	void
	*/
	function crearMenu()
	{
		global $_PATH_WEB;
		global $_ANCHO_SITIO_WEB; 
     
		$menu = "";
		echo "
		<div id=menu>
			<table width=$_ANCHO_SITIO_WEB border=0 cellpadding=0 cellspacing=0 align='center'>
				<tr>
			  		<td colspan=4><div id=titulo>Sistema de env&iacute;o de boletines<br><br></div></td>
				</tr>
			  	<tr>
			    	<td><a href='$_PATH_WEB/Grupos/index.php?accion=mostrar_opciones'>Grupos</a></td>
			    	<td><a href='$_PATH_WEB/Destinatarios/index.php?accion=mostrar_opciones'>Destinatarios</a></td>
			    	<td><a href='$_PATH_WEB/Boletin/index.php?accion=administrar_boletines'>Boletines</a></td>
			    	<td><a href='$_PATH_WEB//index.php?accion=cerrar_sesion'>Cerrar Sesion</a></td>
				</tr>
			</table>
		</div>";
	}//Fin function crearMenu()
}//Fin class Menu
?>
