<?php
/*
*	Nombre: Encabezado.php
*	Descripci�n: Clase que incluye el encabezado de la aplicaci�n
*	Autor: Christian Felipe Benitez Casta�o
*	E-mail:  pipeloco@gmail.com
*	Fecha de Creaci�n: 20-03-2007
*	Fecha de �ltima Modificaci�n: 21-03-2007
*/

class Encabezado
{
	var $_javaScripts;
	function Encabezado()
	{
		$this->_javaScripts=array();
	}

	/********************************* CREAR ENCABEZADO ************************************/
	/*
	* Utilidad:	Se encarga de crear el encabezado de la interfaz
	* Par�metros de entrada:
	*	void
	* Valores de retorno:
	*	void
	*/
	function crearEncabezado()
	{
		global $_PATH_WEB;
		global $_TITULO_SITIO_WEB;
		$obj_Herramientas = new Herramientas();

		echo "
		<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">
			<html xmlns='http://www.w3.org/1999/xhtml'>
				<head>
				";
					if($this->_javaScripts != '')
					{
						foreach($this->_javaScripts as $nombre => $valor)
						{
							echo $valor;
						}
					}
					echo "
					<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' />
					<link rel='stylesheet' type='text/css' href='".$_PATH_WEB."/InterfazGrafica/css/hoja_estilos.css' />
					<script language=javascript type=text/javascript src='".$_PATH_WEB."/SistemaBase/Herramientas/validacionFormas.js'></script>
					<script language=javascript type=text/javascript src='".$_PATH_WEB."/SistemaBase/Herramientas/BrowserDetect.js'></script>

					<title>$_TITULO_SITIO_WEB</title>
				</head>
				<body  topmargin='0'>
						<table width='<?phpecho $_ANCHO_SITIO_WEB?>' border='0' cellspacing='0' cellpadding='0' align='center'>
						    <tr>
								<td>
							  		<img  src='".$_PATH_WEB."/InterfazGrafica/Imagenes/cabecera.jpg'>
							  	</td>
								
						    </tr>
						</table>

						<!--Fin de la cabecera-->";
		
	}

	/********************************* ADICIONAR JS ************************************/
	/*
	* Utilidad:	Se encarga de incluir la ruta de los scripts de las validaciones
	* Par�metros de entrada:
	*	$nombre_script -> Nombre del archivo JavaScript
	* Valores de retorno:
	*	void
	*/
	function adicionarJS($nombre_script)
	{
		global $_PATH_WEB;
		$this->_javaScripts[$nombre_script] = "<script language='JavaScript' type='text/JavaScript' src='".$_PATH_WEB."/SistemaBase/Herramientas/$nombre_script'></script>";
	}
}
?>
