<?php

require_once($_PATH_SERVIDOR . "/Includes/Herramientas.php");
require_once("Pagos.php");

$obj_pagos = new Pagos();

$_obj_interfaz->adicionarFooterJS("Miscelanea.js");
$_obj_interfaz->adicionarFooterJS("usuarios.js");
$_obj_interfaz->adicionarFooterJS("pagos.js");

$datos['accion'] = $datos['accion'].$_SESSION['tipo_usuario'] ;

//print_r($datos['accion']);

if(strpos($datos['accion'], 'Get.aspx?TicketOfficeID=')) {
  $accion = $datos['accion'];
  //$accion = str_replace("/","&",$datos['accion']);
  $datos['accion'] = 'reciboPSE';
  //$str = 'In My Cart : 11 12 items';

  preg_match_all('!\d+!', $accion, $matches);
  //print_r($matches);exit;

  $ticketOfficeID =  implode(' ', $matches[0]);
  //echo $ticketOfficeID;exit;
}
//print_r($datos['accion']);//exit;

switch($datos['accion'])
{
    case 'obtener_funcinalidades_paquete'.$obj_pagos->validarAcceso(array("R")):
        $funcionalidades = $obj_pagos->obtenerFuncionalidesPaqueteAjax($datos);
        print_r($funcionalidades);

        //DM - 2017-11-21 issue [10936]
        //Fin de registro de log
        $datos_log['log_termina_proceso'] = 1;
        logProceso($datos_log);

        exit;
        break;

	case 'gestion'.$obj_pagos->validarAcceso(array("H","R","AD","C")):    //Antes estaba asi: validarAcceso(array("C")), se cambio mientras se trabaja en la nueva gestion de usuarios

		$_SESSION['modulo_superior'] = "configuracion";
    $_SESSION['submodulo_superior'] = "";
		$_SESSION['modulo_activo'] = "conf_gestion";
		$opciones['ACCION']=$datos['tipo_gestion'];
		$opciones['CADENA']= $idi_despliegue['usuarios_1_cadena'];
		$datos['mensaje']= $_obj_mensajes->crearMensaje($datos['msg'],$opciones);
		$contenido = $obj_pagos->abrirFormularioGestion($datos);

		$valores = $obj_pagos->obtenerUsuariosGestion($datos);

		if ($_SESSION['tipo_usuario'] == 'R')
		{
			$campos = array("usu_id"=>$idi_despliegue['hue_id'],
							"usu_nombre"=>$idi_despliegue['label_nombre'],
							"usu_login"=>$idi_despliegue['usuario'],
							"usu_correo"=>$idi_despliegue['hue_email'],
							"cad_nombre"=>$idi_despliegue['cadena'],
							"est_nombre"=>$idi_despliegue['establecimiento'],
							"usu_per_id"=>"",
							"usu_usc_id"=>"");
			$campos_ordena = array("usu_id","usu_nombre","usu_login","usu_correo", "cad_nombre", "est_nombre");

            $opciones['login'] = "usuario_id=%usu_id%";
		}
		else
		{
			$campos = array("usu_id"=>"",
							"usu_nombre"=>$idi_despliegue['label_nombre'],
							"usu_login"=>$idi_despliegue['usuario'],
							"usu_correo"=>$idi_despliegue['hue_email'],
							"usu_per_id"=>"",
							"usu_usc_id"=>"");
			$campos_ordena = array("usu_nombre","usu_login","usu_correo");
		}

		//Septiembre 17 de 2011 YA NO Se restringe por ahora solo para perfil tipo cadena
		//$restriccionCadena = " AND per_id = '6' ";

		//El select de los tipos de usuario
		$opcionesTipoUsuario['nombre_campo']="usu_per_id";
        $opcionesTipoUsuario['sql']="select per_id , pid_nombre
								from perfiles, perfiles_idioma
								where 	per_id = pid_per_id AND
										pid_idi_id = '".$_SESSION['idi_id']."'
								".$restriccionCadena."
								AND per_id != 1 AND per_id != 3 AND per_id != 4 AND per_id != 5 AND per_id != 7
								order by per_nombre ASC";
        $opcionesTipoUsuario['atributos']['codigo']="per_id";
        $opcionesTipoUsuario['atributos']['mostrar']="pid_nombre";
        $opcionesTipoUsuario['valor']=$datos['usu_per_id'];
		$opcionesTipoUsuario['adicionales']='onChange="submit()"';
		//El select de las categorias de usuarios
		$opcionesCategoriaUsuario['nombre_campo']="usc_codigo";
        $opcionesCategoriaUsuario['sql']="select usc_id , uci_nombre
								from usuario_categoria, usuario_categoria_idioma
								where usc_id = uci_usc_id AND uci_idi_id = '".$_SESSION['idi_id']."'
								order by usc_nombre ASC";
		$opcionesCategoriaUsuario['atributos']['codigo']="usc_id";
        $opcionesCategoriaUsuario['atributos']['mostrar']="uci_nombre";
        $opcionesCategoriaUsuario['valor']=$datos['usc_codigo'];
        $opcionesCategoriaUsuario['adicionales']='onChange="submit()"';
		$opciones['tipoUsuario'] = Herramientas::crearSelectFormaPersonalizada($opcionesTipoUsuario,$idi_despliegue['tipo_usuario_select']);
		$opciones['categoriaUsuario'] = Herramientas::crearSelectFormaPersonalizada($opcionesCategoriaUsuario,$idi_despliegue['categoria']);

        if( $_SESSION[tipo_usuario]=="C")//El usuario es de tipo cadena %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		{
			$ids = array_keys( $_SESSION[cadena_est] );
			$hotel =  $_SESSION['cadena_est'][$ids[0] ];

			$datos['est_id'] = $datos['est_id'] == "" ? $hotel['est_id'] : $datos['est_id'];
		}
		$est_id = ($datos['est_id'] == '') ? $_SESSION['est_id'] : $datos['est_id'];
		$opciones['actualizar'] = "usc_codigo=%usu_usc_id%&est_id=".$est_id."&usu_per_id=%usu_per_id%&usu_id=%usu_id%&categoria_codigo=%usu_usc_id%&offset=".$datos['offset']."&usu_busqueda=".$datos['usu_busqueda']."&ordena=".$datos['ordena']."&tipo_ordena=".$datos['tipo_ordena'];
		$opciones['eliminar'] = "est_id=".$est_id."&usu_id=%usu_id%&usu_busqueda=".$datos['usu_nombre_cedula'];

		$arreglo_datos['TITULO'] = '';  //"Listado de usuarios";
		$arreglo_datos['CAMPOS_OPCIONES']=
					array("usu_cedula"=>'width="80" align="center"',
						"usu_nombre"=>'width="200" align="left"',
						"pid_nombre"=>'width="110" align="center"',
						"usu_login"=>'width="110" align="left"');

		$arreglo_datos['CAMPOS']=$campos;
		$arreglo_datos['CAMPOS_ORDENA']=$campos_ordena;
		$arreglo_datos['LISTADO']=$valores;
		$arreglo_datos['OPCIONES']=$opciones;
		$arreglo_datos['VOLVER']=true;
		$arreglo_datos['ACCION_LISTAR'] = "gestion&m=usuarios&usu_busqueda=".$datos['usu_busqueda'];
		$arreglo_datos['ACCION_ACTUALIZAR']="fm_editar";
		$arreglo_datos['ACCION_ELIMINAR']="eliminar";
        $arreglo_datos['ACCION_LOGIN']="login_usuario&m=usuarios";
		$arreglo_datos['DATOS']=$datos;


		if( isset($datos['msg']))
		{
			$opciones['ACCION']=$datos['tipo_gestion'];
			$opciones['CADENA']= $idi_despliegue['usuarios_1_cadena'];
			$arreglo_datos['MENSAJE'] = $_obj_mensajes->crearMensaje($datos['msg'],$opciones);
		}
		//amp - 12/8/15 - insertar migasd e pan
		$rastro = array( 'nivel_1' => array( "titulo"=>$idi_despliegue['label_menu_configuracion']),
						 'nivel_2' => array( "titulo"=>$idi_despliegue['gestion_usuarios'] , 'activo'=> 1));
		$_obj_interfaz->asignarMigas($rastro);

		$contenido .= $_obj_interfaz->crearListadoUsuarios($arreglo_datos);
		$_obj_interfaz->asignarContenido($contenido);
		break;


	case 'fm_editar'.$obj_pagos->validarAcceso(array("H","R","AD","C")):    //Antes estaba asi: validarAcceso(array("C")), se cambio mientras se trabaja en la nueva gestion de usuarios
        $_SESSION['modulo_activo']="editar";
		$valores = $obj_pagos->obtenerDatosUsuario($datos['usu_id']);
		if( !is_array($valores))
		{
			$opciones['CADENA']= $idi_despliegue['usuarios_1_cadena'];
			$opciones['ACCION']=0;
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($valores,$opciones);
		}
		$contenido = $obj_pagos->abrirFormularioEditar($datos,$valores);
		$_obj_interfaz->asignarContenido($contenido);
		break;

	case 'editar'.$obj_pagos->validarAcceso(array("C","H","R")):
		$resultado = $obj_pagos->actualizarUsuarioGestion($datos);
		if( $resultado==1)
		{
			$enlace = "index.php?m=usuarios&accion=gestion&tipo_gestion=2&usu_busqueda=".$datos['usu_busqueda']."&msg=".$resultado;
            //DM - 2014-07-29 issue 5040
            if( $datos['est_id'] != '')
            {
                $enlace .= '&est_id='.$datos['est_id'];
            }
			header('Location: '.$enlace);
            exit;
		}
		else
		{
			$opciones['ACCION']=2;
			$opciones['CADENA']= $idi_despliegue['usuarios_1_cadena'];
			$datos['mensaje']= $_obj_mensajes->crearMensaje($resultado,$opciones);
			$valores = $obj_pagos->obtenerDatosUsuario($datos['usu_id']);
			$contenido = $obj_pagos->abrirFormularioEditar($datos,$valores);
			$_obj_interfaz->asignarContenido($contenido);
		}
		break;

	//MAOH - 25 Sep 2012 - Incidencia 1761
	//Se quita el captcha del formulario y se agrega que solo puedan acceder los usuarios
	/*case 'getInfoPagosCedula':
	    //$_SESSION[inf_captcha] = "";
		$_obj_interfaz->adicionarFooterJS("Miscelanea.js");
		$contenido = $obj_pagos->abrirFormularioSoporte($datos);
    	//AMP Cambio a modal de INSPINIA
		 $_obj_interfaz->asignarContenido($contenido);
		$contenido = $obj_pagos->abrirFormularioEditar($datos,$valores);
		$_obj_interfaz->asignarContenido($contenido);
		break;   */
	case 'infoPagosCedula':
		$datos['plantilla'] = 'principal';
		$contenido = $obj_pagos->abrirFormularioInfoPagosCedula($datos);
    	//AMP Cambio a modal de INSPINIA
		 $_obj_interfaz->asignarContenido($contenido);
		break;
	case 'infoPagosPlaca':
		$datos['plantilla'] = 'principal';
		$contenido = $obj_pagos->abrirFormularioInfoPagosPlaca($datos);
    	//AMP Cambio a modal de INSPINIA
		 $_obj_interfaz->asignarContenido($contenido);
		break;

    case 'infoPagosPSE':
      $datos['plantilla'] = 'principal';
      $contenido = $obj_pagos->abrirFormularioPSE($datos, $ticketOfficeID);
        //AMP Cambio a modal de INSPINIA
       $_obj_interfaz->asignarContenido($contenido);
      break;

      case 'reciboPSE':
        $datos['plantilla'] = 'principal';
        $datos['m'] = array('TicketOfficeID' => $ticketOfficeID);

        $contenido = $obj_pagos->reciboPSE($datos);
          //AMP Cambio a modal de INSPINIA
         $_obj_interfaz->asignarContenido($contenido);
        break;

      case 'comprobarTransacciones':
        //$datos['plantilla'] = 'principal';
        $contenido = $obj_pagos->comprobarTransacciones();
          //AMP Cambio a modal de INSPINIA
         $_obj_interfaz->asignarContenido($contenido);
        break;

	//Formulario de autenticacion
	default:
            //AM - 2015-08-11
            //carga la plantilla de inicio de sesion sin menu
      $datos['plantilla'] = 'principal';
			$datos['mensaje']= $_obj_mensajes->crearMensaje($datos['msg']);
			$contenido = $obj_pagos->abrirFormularioHome($datos);
			$_obj_interfaz->asignarContenido($contenido);
		break;
}//Fin de switch($datos['accion'])
?>
