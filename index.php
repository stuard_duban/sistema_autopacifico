<?php
require_once("Config.php");
require_once("Includes/index.php");

$_obj_database = new database($_host_db,$_user_db,$_password_db,$_db);

$_obj_interfaz = new Interfaz();

$_obj_mensajes = new Mensajes();


$_obj_interfaz->adicionarCSS("style.css");
//$_obj_interfaz->adicionarFooterJS("config.js");

//Si no se definio un modulo toma por defecto usuarios
$datos['m'] = ( isset($datos['m']) ?  $datos['m'] : "pagos");

$datos['m'] = strtolower($datos['m']);

//si el parametro no es un directorio valido deja por defecto el de usuario
if( !is_dir($datos['m']) )
{
    $datos[m] = "Pagos";
}
require_once($datos['m']."/index.php");

print_r( $_obj_interfaz->crearInterfazGrafica($datos) );

$_obj_database->desconectar();

?>
